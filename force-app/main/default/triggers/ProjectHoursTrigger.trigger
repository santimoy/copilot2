trigger ProjectHoursTrigger on Project_Hours__c (before insert) {
    if(trigger.isBefore && trigger.isInsert){
        ProjectHoursTriggerHelper.HandleBeforeInsert(trigger.new);
    }
}