/**
*@name          : CampaignCompany_Trgr
*@developer     : 10K developer
*@date          : 2017-09-22
*@description   : Trigger for Campaign_Company__c object, dealing After insert,delete events!
                    CampaignCompanyHandler is a handler of this object, and will be dealing a process of creating automated child campagin & members!
                    
                    
Reason for Commenting out code: Due to requirement of Opportunity Automation for CampaignMember, there is no need of CampaignCompany automation so commenting out the obsolete code!
Code commented out on : 25th Oct 2018
**/
trigger CampaignCompany_Trgr on Campaign_Company__c (after insert, after delete) {
     //new CampaignCompanyHandler().run();
}