/**
* @Description Handles all triggers on the Lead object.
*
* @Author: Daniel Llewellyn
* @Date:   Aug 20, 2018
* @Note: DL 08/20/2018 Initial Draft
**/
trigger LeadTrigger on Lead (before insert, after insert)
{
    LeadTriggerHandler leadHandler = new LeadTriggerHandler();
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            leadHandler.onBeforeInsert(Trigger.new);
        }
    }
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            leadHandler.onAfterInsert(Trigger.new);
        }
    }
}