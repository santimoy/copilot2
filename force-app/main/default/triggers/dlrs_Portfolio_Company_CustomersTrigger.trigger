/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_Portfolio_Company_CustomersTrigger on Portfolio_Company_Customers__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(Portfolio_Company_Customers__c.SObjectType);
}