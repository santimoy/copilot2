/*
Author : Snehil Jaiswal
Description : Trigger on  Sourcing_Preference__c Object. Please define your event and write the function in Handler class.
Date Created : 13th March 2019
Change 1 : 
*/
trigger SourcingPreferenceTrigger on Sourcing_Preference__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
     if(Trigger.IsBefore && Trigger.IsInsert) {
        SourcingPreferenceTriggerHandler.triggerIsBeforeInsert(Trigger.New);
    }
    if(Trigger.IsBefore && Trigger.IsUpdate) {
        SourcingPreferenceTriggerHandler.triggerIsBeforeUpdate(Trigger.New);
    }
    if(Trigger.IsAfter && Trigger.IsInsert) {
        SourcingPreferenceTriggerHandler.triggerIsAfterInsert(Trigger.New);
    }
    if(Trigger.IsAfter && Trigger.IsUpdate) {
        SourcingPreferenceTriggerHandler.triggerIsAfterUpdate(Trigger.OldMap, Trigger.New);
    }
}