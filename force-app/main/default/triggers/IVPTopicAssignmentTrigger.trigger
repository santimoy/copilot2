/**
* @author 10k advisor
* @date 2018-07-05
* @TriggerName IVPTopicAssignmentTrigger
* @group IVPSmartDash
*
* @description Generally used to handle TopicAssignment Functionality
*/
trigger IVPTopicAssignmentTrigger on TopicAssignment (after insert,before delete) {
    if(Trigger.isInsert){
        IVPTopicAssignmentTriggerHandler.handlerAfterProcess(Trigger.New);
    }else if(Trigger.isDelete){
        IVPTopicAssignmentTriggerHandler.handlerBeforeDeleteProcess(Trigger.old);
    }
}