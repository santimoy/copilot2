trigger ResourceHoursTrigger on Resource_Hours__c ( before insert, after insert, after update, after delete ) {
    if(trigger.isBefore && trigger.isInsert){
        ResourceHoursTriggerHelper.HandleBeforeInsert(trigger.new);
    }
    if(trigger.isAfter && trigger.isInsert){
        ResourceHoursTriggerHelper.UpdateResourceNameOnProjectHours(trigger.new);
    }
    if(trigger.isAfter && trigger.isUpdate){
        ResourceHoursTriggerHelper.UpdateResourceNameOnProjectHours(trigger.new);
    }
    if(trigger.isAfter && trigger.isDelete){
        ResourceHoursTriggerHelper.UpdateResourceNameOnProjectHours(trigger.old);
    }
}