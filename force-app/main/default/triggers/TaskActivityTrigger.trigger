/*
*       Trigger on Task Object
* 
*       Author:   Wilson Ng
*       Date:     August 21, 2012
*
*/ 
trigger TaskActivityTrigger on Task (after delete, after insert, after undelete, after update, before delete, before update, before insert) {

	ActivityTriggerHandler objHandler = new ActivityTriggerHandler();
	
	if(Trigger.isAfter && Trigger.isInsert){
		objHandler.onAfterInsert(Trigger.new);
	}
	else if(Trigger.isAfter && Trigger.isUpdate){
		objHandler.onAfterUpdate(Trigger.new, Trigger.OldMap);
	}
	else if(Trigger.isAfter && Trigger.isDelete){
		objHandler.onAfterDelete(Trigger.old);
	}
	else if(Trigger.isAfter && Trigger.isUndelete){
		objHandler.onAfterUndelete(Trigger.new);
	}
	
	else if(Trigger.isBefore && Trigger.isDelete){
		objHandler.onBeforeTaskDelete(Trigger.old);
	} 
	else if(Trigger.isBefore && Trigger.isInsert){
        objHandler.onBeforeInsert(Trigger.new);
    }
    else if(Trigger.isBefore && Trigger.isUpdate){
        objHandler.onBeforeUpdate(Trigger.new, Trigger.OldMap);
    }
}