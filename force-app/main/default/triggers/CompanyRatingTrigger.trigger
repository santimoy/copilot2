/***********************************************************************************
 * @author      10k-Expert
 * @name        CompanyRatingTrigger
 * @date        2019-01-29
 * @description Trigger on Company Rating Object
 ***********************************************************************************/
trigger CompanyRatingTrigger on Company_Rating__c (before insert) {
    if(trigger.isBefore && trigger.isInsert){
        CompanyRatingTriggerHandler.onBeforeInsert(Trigger.new);
    }
}