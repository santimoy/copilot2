/**
*@name      	: CampaignInfluenceTrigger
*@developer 	: 10K developer
*@date      	: 2018-10-12
*@description   : Trigger is use to stop the autometic records creation of influence records when opportunity record type is ignite
**/
trigger OppAuto_CampaignInfluenceTrigger on CampaignInfluence (before insert) {
    OppAuto_CampaignInfluenceTriggerHandler.stopAutometicInfluenceRecordCreate(Trigger.new);
}