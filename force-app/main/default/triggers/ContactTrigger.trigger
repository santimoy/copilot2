/**
 * Contact trigger set all other contacts "key_contact" value to false
 *
 @author Thomas Lesnick
 @created 2015-10-01
 @version 1.0
 @since API 37.0 
 *
 Last Modified
 2018-01-16   Wilson NG   Bulkifed code and fixed "Duplicate id in list" error
 2019-01-15  Gordon A Exempted IR Contact record types from logic
 2019-10-01  Nimisha Jaiswal Shifted the trigger code to ContactTriggerHandler as well as merged the functionality of ContactSumTrigger as per the Don's comment on SFDC-160 ticket. 
 *
 */
trigger ContactTrigger on Contact ( after insert, after update, after delete, after undelete) {

    if(!checkRecursive.runOnce('ContactTrigger', trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete))
        return;

    ContactTriggerHandler objHandler = new ContactTriggerHandler();
    if( trigger.isAfter ) {

        if( trigger.isInsert ) {

            objHandler.onAfterInsert( trigger.new );
        }
        else if( trigger.isUpdate ) {

            objHandler.onAfterUpdate( trigger.new, trigger.oldMap );
        }
        else if ( trigger.isDelete ) {

            objHandler.onAfterDelete( trigger.old );
        }
        else if ( trigger.isUndelete ) {

            objHandler.onAfterUndelete( trigger.new );
        }
    }
    /*if(!checkRecursive.runOnce('ContactTrigger', trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete))
        return;

    set<Id> contactIds = new set<Id>();
    set<Id> accountIds = new set<Id>();
    for(Contact c : trigger.new) {
        contactIds.add(c.Id);
        if(c.Key_Contact__c==true && c.AccountId!=null)
            accountIds.add(c.AccountId);
    }
    
    if(accountIds.size() > 0) {
        system.debug('accountIds: ' + accountIds);
        system.debug('contactIds: ' + contactIds);
        
        list<Contact> pending = new list<Contact>();    
        list<Contact> others = [select id, Key_Contact__c from Contact where Id<>:contactIds and AccountId=:accountIds and Key_Contact__c=true and RecordType.Name !='IR Contact'];
        for(Contact o : others) {
            if(!contactIds.contains(o.Id)) {
                o.Key_Contact__c=false;
                pending.add(o);
                contactIds.add(o.Id);
            }
        }
    
        if(pending.size() > 0) {
            system.debug('update pending: ' + pending);
            update pending;
        }
    }*/   
}