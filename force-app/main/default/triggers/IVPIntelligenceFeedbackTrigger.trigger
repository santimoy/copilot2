trigger IVPIntelligenceFeedbackTrigger on Intelligence_Feedback__c (after update) {
    
    if(!checkRecursive.runOnce('IVPIntelligenceFeedbackTrigger', trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete))
		return;
    
    if(trigger.isAfter && trigger.isUpdate){
        IVPIntelligenceFeedbackTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
    }

}