/**
* @author Amjad-Concretio
* @date 2018-05-21
* @Trigger user Trigger
*
* @description trigger to handle user events
*/
trigger User on User (After Insert, After Update) {

    if(Trigger.isInsert){
       IVP_UserTrigger_Handler.createGroupOnInsert(Trigger.new);
    }
    if(Trigger.isUpdate){
        IVP_UserTrigger_Handler.createGroupOnUpdate(Trigger.new, Trigger.oldMap);
    }
}