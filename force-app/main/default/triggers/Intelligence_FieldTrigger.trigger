/*
*     Trigger on Intelligence_Field__c Object
*     Author:   Virendra
*     Date:   April 9 2018
*
*/ 

trigger Intelligence_FieldTrigger on Intelligence_Field__c (before insert, before update) {
	
	if( trigger.isBefore ) {
    	if( trigger.isInsert )
    	  Intelligence_FieldTriggerHandler.handleBeforeInsert( trigger.new );
    	else if( trigger.isUpdate )
      	  Intelligence_FieldTriggerHandler.handleBeforeUpdate( trigger.new, trigger.oldMap );
    }
    
}