trigger CompanyCompetitorTrigger on CompanyCompetitor__c (after delete, after insert) {

	CompanyCompetitorTriggerHandler triggerHandler = new CompanyCompetitorTriggerHandler();
	if(Trigger.isAfter && Trigger.isInsert){
		triggerHandler.onAfterInsert(Trigger.new);
	}
	else if(Trigger.isAfter && Trigger.isDelete){
		triggerHandler.onAfterDelete(Trigger.old);
	}
}