trigger CompanyBankTrigger on CompanyBank__c (after delete, after insert) {
	
	CompanyBankTriggerHandler triggerHandler = new CompanyBankTriggerHandler();
	if(Trigger.isAfter && Trigger.isInsert){
		triggerHandler.onAfterInsert(Trigger.new);
	}
	else if(Trigger.isAfter && Trigger.isDelete){
		triggerHandler.onAfterDelete(Trigger.old);
	}
}