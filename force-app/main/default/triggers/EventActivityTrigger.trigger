/*
*       Trigger on Event Object
* 
*       Author:   Wilson Ng
*       Date:     August 21, 2012
*
*/ 
trigger EventActivityTrigger on Event (after delete, after insert, after undelete, after update) {

	ActivityTriggerHandler objHandler = new ActivityTriggerHandler();
	
	if(Trigger.isAfter && Trigger.isInsert){
		objHandler.onAfterInsert(Trigger.new);
	}
	else if(Trigger.isAfter && Trigger.isUpdate){
		objHandler.onAfterUpdate(Trigger.new, Trigger.OldMap);
	}
	else if(Trigger.isAfter && Trigger.isDelete){
		objHandler.onAfterDelete(Trigger.old);
	}
	else if(Trigger.isAfter && Trigger.isUndelete){
		objHandler.onAfterUndelete(Trigger.new);
	}
	
}