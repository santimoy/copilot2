/***********************************************************************************
 * @author      10k-Expert
 * @name		OpportunityTrigger
 * @date		2018-11-14
 * @description	Trigger on Opportunity Object
 ***********************************************************************************/
trigger OpportunityTrigger on Opportunity (after update) {  //we are not dealing ->before insert, before update, 
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            // handling after update 
            OpportunityTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}