/***********************************************************************************
 * @author      10k-Expert
 * @name        CompanyRequestTrigger
 * @date        2018-11-14
 * @description Trigger on Company Request Object
 ***********************************************************************************/
trigger CompanyRequestTrigger on Company_Request__c (before insert, after insert, after update) {
    if(trigger.isBefore){//this thing is not needed => CompanyRequestTriggerHandler.IS_NOT_COMPANY_REQUEST_PROCESS
        if(trigger.isInsert){
            CompanyRequestTriggerHandler.onBeforeInsert(Trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
            CompanyRequestTriggerHandler.onAfterInsert(Trigger.new);
        }
        else if(trigger.isUpdate){
            CompanyRequestTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}