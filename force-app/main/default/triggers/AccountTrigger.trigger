/*
*      Trigger on Account Object
* 
*      Author:   Wilson Ng
*      Date:     Sept 12, 2012
*
*/ 
trigger AccountTrigger on Account( before insert, before delete, before update, after insert, after update )
{
    Boolean reTrigger = !Test.isRunningTest() && !checkRecursive.runOnce('AccountTrigger', trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete);
    System.debug('reTrigger>'+ reTrigger);
    /*if(reTrigger){
        if(trigger.isUpdate ){
            AccountTriggerHandler objHandler = new AccountTriggerHandler();
            if( trigger.isBefore ){
                system.debug('------->');
                objHandler.onBeforeUpdate( trigger.new, trigger.oldMap );
            }else{
                objHandler.onAfterUpdate( trigger.new, trigger.oldMap );
            }
        }
        return;
    }*/

    AccountTriggerHandler objHandler = new AccountTriggerHandler();
    AccountTriggerHandlerWOS objWOSHandler = new AccountTriggerHandlerWOS();
    
    if( trigger.isBefore )
    {
        if( trigger.isInsert && !reTrigger)
            objHandler.onBeforeInsert( trigger.new );
        else if( trigger.isUpdate ){
            // following method need to run again in the case of update
            objHandler.onBeforeUpdate( trigger.new, trigger.oldMap );
        }else if( trigger.isDelete && !reTrigger)
            objHandler.onBeforeDelete( trigger.old );
    }

    if( trigger.isAfter )
    {
        if( trigger.isInsert && !reTrigger)
        {
            objWOSHandler.onAfterInsert( trigger.new );
            objHandler.onAfterInsert( trigger.new );
        }
        else if( trigger.isUpdate )
        {
            if(!reTrigger){// following method not need to run again in the case of update
                objWOSHandler.onAfterUpdate( trigger.new, trigger.oldMap );
            }
            // following method need to run again in the case of update
            objHandler.onAfterUpdate( trigger.new, trigger.oldMap );
        }
    }
}