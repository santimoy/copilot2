/**
 * Created by Ranjeet Kumar on 11/5/2017.
 */

trigger InvoiceLItemTrigger on Krow__Invoice_Line_Item__c (after insert, after update, after delete ) {
    if(trigger.isAfter && trigger.isInsert){
        InvoiceLItemTriggerHelper.HandleAfterInsert(trigger.new);
    }
    if(trigger.isAfter && trigger.isUpdate){
        InvoiceLItemTriggerHelper.HandleAfterUpdate(trigger.oldMap, trigger.new);
    }
    if(trigger.isAfter && trigger.isDelete){
        InvoiceLItemTriggerHelper.HandleAfterDelete(trigger.old);
    }
    InvoiceLItemTriggerHelper.DebugLimits();
}