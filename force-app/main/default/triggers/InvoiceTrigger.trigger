trigger InvoiceTrigger on Krow__Invoice__c (after insert) {
    if(trigger.isAfter && trigger.isInsert){
        InvoiceTriggerHelper.CreateAdditionalInvoiceLineItem(trigger.new);
    }
}