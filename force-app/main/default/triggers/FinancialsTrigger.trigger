//
// 4-4-2018: TO BE DEPRECATED
//
trigger FinancialsTrigger on Financials__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

        FinancialsTriggerHandler handler= new FinancialsTriggerHandler();
        if (Trigger.isBefore) {
            
        
        } else if (Trigger.isAfter) {
            if(Trigger.isUpdate) handler.afterUpdate(Trigger.new, Trigger.oldMap);
            else if(Trigger.isInsert) handler.afterInsert(Trigger.new);
        
        }
}