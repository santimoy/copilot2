/**
*@name      	: Campaign_Trgr
*@developer 	: 10K developer
*@date      	: 2017-09-22
*@description   : This Trigger is on Campaign Object
						And handle the after update And before delete events
**/

trigger Campaign_Trgr on Campaign ( after Update, before delete ) {
	new CampaignHandler().run();
}