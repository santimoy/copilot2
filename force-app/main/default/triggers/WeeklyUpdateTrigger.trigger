trigger WeeklyUpdateTrigger on Weekly_Update__c (before update) {

	    WeeklyUpdateTriggerHandler objHandler = new WeeklyUpdateTriggerHandler();
    
	   if(Trigger.isBefore && Trigger.isUpdate){
	        objHandler.onBeforeUpdate(Trigger.new, Trigger.OldMap);
	    }
   

}