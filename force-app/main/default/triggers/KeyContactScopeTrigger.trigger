/*
Class Name   : KeyContactScopeTrigger
Description  : Triger on Key Contact scope object
Author       : Sukesh
Created Date : 26th August 2019
*/
trigger KeyContactScopeTrigger  on Key_Contact_Scope__c (after update, before update) {

    KeyContactScopeTriggerHandler objHandler = new KeyContactScopeTriggerHandler();

    if(Trigger.isAfter) {

        if(Trigger.isUpdate && KeyContactScopeTriggerHandler.runOnce) {

            KeyContactScopeTriggerHandler.runOnce = false;
            objHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }

    }

    if(Trigger.isBefore ) {

        if(Trigger.isUpdate && KeyContactScopeTriggerHandler.runOnce)
            objHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
}