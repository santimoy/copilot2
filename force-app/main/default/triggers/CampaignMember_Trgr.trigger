/**
*@name          : CampaignMember_trgr
*@developer     : 10K developer
*@date          : 2017-09-22
*@description   :This Trigger is on CampaignMember Object
                        And handle the after insert/update And before delete events
**/
trigger CampaignMember_Trgr on CampaignMember (before delete, after insert, after update, before insert, before update) {
    new CampaignMemberHandler().run();
}