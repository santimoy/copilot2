﻿! function(a) {
    var b = function(idOfMultiPick, c, d) {
		var picklistContainer = $('#'+idOfMultiPick);
        var e = d ? c[d] : c;
        a("<option value=\"" + e + '">'+e+'</option>').data("__ttmulti_data__", c).appendTo(picklistContainer); 
    };
	
	var getSelectedLst = function(idOfMultiPick)
	{
		var selected = [];
    		$('#' + idOfMultiPick + ' option').each(function(){
    			  selected.push($(this).val()); 
    		});
		
		return selected;
	};
	
    a.fn.typeaheadmulti = function(c, d, idOfMultiPick) {
		
        function e(c, d) { 
			
            var e = d ? d.display : void 0;
            this.each(function() {
                var f = a(this);
				
                f.typeahead(c, d).bind("typeahead:select", function(a, c) {
					var selectedItems = getSelectedLst(idOfMultiPick); 
					//console.log('===selectedItems===',selectedItems);
					var itemSelected = e ? c[e] : c; 
					//console.log('===g===',g); 
					
					var index = selectedItems.indexOf(itemSelected);
					//console.log('===itemSelected===',itemSelected,'====index====',index);
					if(index == -1)
					{
						b(idOfMultiPick, c, e); 
					}
					f.typeahead("val", "");
					
                })
            })
        }

        function f(b) {
            return this.first().parent().prev(".ttmulti-selections").find("li").map(function() {
                return a(this).data("__ttmulti_data__")
            })
        }
        return "val" === c ? f.call(this, [].slice.call(arguments, 1)) : e.call(this, c, d)
    }
}(jQuery);