({
    onInit: function(cmp, evt) {
        var action = cmp.get("c.getIVPId");
        //action.setParams({ firstName : cmp.get("v.firstName") });

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
                /*var recId = response.getReturnValue();
                var workspaceAPI = cmp.find("workspace");
                workspaceAPI.openTab({
                    focus: true,
                    recordId: recId
                }).then(function(response) {
                    workspaceAPI.openSubtab({
                        parentTabId: response,
                        url: '/lightning/r/Intelligence_User_Preferences__c/'+recId+'/view',
                        focus: true
                    });
                })
                .catch(function(error) {
                    console.log(error);
                });
                */
                /*var navEvt = $A.get("e.force:navigateToSObject");
                if(navEvt){
                    navEvt.setParams({
                        "recordId": response.getReturnValue()
                    });
                    navEvt.fire();
                }*/

                var recId = response.getReturnValue();

                var workspaceAPI = cmp.find("workspace");
                if (workspaceAPI) {
                    workspaceAPI.openTab({
                        url: '#/sObject/' + recId + '/view',
                        focus: true,
                        closeable: false
                    }).then(function(response) {
                        workspaceAPI.getTabInfo({
                            tabId: response
                        }).then(function(tabInfo) {
                            console.log(tabInfo);
                            window.localStorage.setItem('sdTabId', tabInfo.tabId);
                            console.log(window.localStorage.getItem('sdTabId'));
                            console.log('The recordId for this tab is:' + tabInfo.recordId);
                        });
                    }).catch(function(error) {
                        console.log(error);
                    });
                    /*workspaceAPI.getAllTabInfo().then(function(response) {
                        console.table(response);
                        for(var i in response){
                            var tab = response[i];
                            if(tab.url.includes("Smart_Dash")){
                                workspaceAPI.closeTab({tabId: tab.tabId});
                            }
                        }
                    })
                    .catch(function(error) {
                        console.log(error);
                    });*/

                    workspaceAPI.getFocusedTabInfo().then(function(response) {
                            var focusedTabId = response.tabId;
                            workspaceAPI.closeTab({ tabId: focusedTabId });
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                }
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
        });
        $A.enqueueAction(action);
    }
})