({
	init : function(component, event, helper) {
		var action = component.get("c.fetchInitData");

		action.setParams({
			recordId:component.get("v.recordId")
		});

		action.setCallback(this,function(response)
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
               
				var res = JSON.parse(response.getReturnValue());
				component.set("v.companyTeams",res.teamMembers);
				component.set("v.domainURL",res.domainName);
				
			}
			else if(state === "INCOMPLETE")
			{
			}
			else if(state === "ERROR")
			{
				var errors = response.getError();
				if(errors)
				{
					if(errors[0] && errors[0].message)
					{
						console.log("Error message: " + errors[0].message);
					}
				}else
				{
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	redirectToRecord : function(component, event, helper) {
		//debugger;
		debugger;
		var recordId = event.currentTarget.name;
		var urlEvent = $A.get("e.force:navigateToURL");
                      urlEvent.setParams({
                        "url": component.get("v.domainURL")+"/"+recordId,
                        "isredirect": "false"
                      });
                      urlEvent.fire();


        // var navEvt;
        // if(recordId) {
        //     navEvt = $A.get("e.force:navigateToSObject");
        //     navEvt.setParam("recordId", recordId);
        // } 
        // navEvt.fire();
	},
    
    viewMore : function(component, event, helper) {
		var noOfrecord = component.get("v.noOfRecordsToDisplay");
        component.set("v.noOfRecordsToDisplay", noOfrecord+100);
	}
})