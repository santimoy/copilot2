({
    doInit: function(component, event, helper) {
        helper.doInit(component);
    },
    handleCancel : function(component, event, helper) {
        var dashDockedAppEvent = $A.get("e.c:DashDockedAppEvent");
        dashDockedAppEvent.setParams({ 
            "dockedData" : {hideLog : true, refresh : false}
        });
        dashDockedAppEvent.fire();
    },
    handleSave : function(component, event, helper) {
        
        helper.handleSave(component);
    },
    handleTypeChange :function(component, event, helper) {
        var typeSelect = component.find('typeSelect');
        var sourceSelect = component.find("sourceSelect");
        if(typeSelect.get('v.value') === 'Email'){
            sourceSelect.set('v.value', '');
            sourceSelect.set('v.disabled', false);
        }else{
            sourceSelect.set('v.value', 'Inbound');
            sourceSelect.set('v.disabled', true);
            sourceSelect.set('v.value', '');
        }
    }
})