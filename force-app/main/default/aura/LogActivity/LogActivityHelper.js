({
    doInit : function(cmp){
        var data = {};
        data = cmp.get('v.data');
        if(!data){
            return;
        }

        var inputJson = {'inputJson':JSON.stringify({accId : data.companyId})};
        this.handleCalling(cmp, 'getInitData', inputJson, this.initCallback, this, {});
        
        var today = new Date();
        var todayVal = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();         
        var task = {
            Status : 'Completed',
            Priority : 'Normal',
            ActivityDate : todayVal,
            Next_Action__c: '',
            //Next_Action_Date__c : todayVal
        };
        task.WhatId = data.companyId;
        task.What = { Name : data.companyName };
        task.Type__c = data.type;
        task.Subject = data.subject;
        task.Company_Signal__c = data.companySignalId;
        task.Company_Signal__r = { Name: data.companySignalName};
        task.Description = data.comments;
        task.Signal_Action__c = data.action;
        task.Signal_Category__c = data.signalCategoryId;
        task.Signal_Category__r = { Name: data.signalCategoryName};
        task.Company_Signal_Headline__c = data.companySignalHeadline;
        task.Company_Signal_External_Id__c = data.companySignalExtId;
        if(data.action=='Log Activity'){
	        if(data.companyNextAct ){
	            task.Next_Action__c = data.companyNextAct;
	        }
	        if(data.companyNextActDate){
	            task.Next_Action_Date__c = data.companyNextActDate;
	        }
        }
        cmp.set('v.task', task);
        
        var opts = [
            { value: "Call", label: "Call" },
            { value: "Email", label: "Email" },
            { value: "Meeting", label: "Meeting" },
            { value: "Voicemail", label: "Voicemail" },
            { value: "Unassign", label: "Unassign" },
            { value: "Pass", label: "Pass" },
            { value: "Other", label: "Other" }
         ];

        cmp.set("v.typeOptions", opts);
	},
    initCallback : function(cmp, hlpr, response, callBackInfo){
        response = response.getReturnValue();
        if(response){
            cmp.set('v.user', response.currentUser);
            var accContacts = response.accContacts;
            var opts = [{ value: "", label: "--Select--" }];
            
            for(var index = 0, length = accContacts.length; index<length; index++){
                var contact = accContacts[index];
                opts.push({ class: "optionClass", value: contact.Id, label: contact.Name, 
                           selected: contact.Key_Contact__c });
            }
            cmp.set("v.nameOptions", opts);
        }
    },
    handleSave : function(cmp){
    	if(this.validate(cmp)){
	        var taskObj = cmp.get('v.task');
	
	        var data = cmp.get('v.data');
	        var result = {hideLog : true, refresh : true, recid : taskObj.WhatId, 
	                      type : data.type,
	                      signalAction : data.action,
	                      companySignalId : data.companySignalId, result:'request'};
	        this.handleCalling(cmp, 'prepareTask', {taskObj:JSON.stringify(taskObj)}, this.handleSaveCallBack, this, {data:data, result:result});
    	}
        
    },
    validate : function(cmp){
    	var isValid = true;
    	
		isValid = this.checkAndAddError(cmp, isValid, 'comments');
		isValid = this.checkAndAddError(cmp, isValid, 'inpSub');
		isValid = this.checkSourceField(cmp, isValid);
        return isValid;
    },
    checkAndAddError: function(cmp, isValid, fieldId){
    	
    	var inputCmp = cmp.find(fieldId);
        console.log(fieldId);
        if(inputCmp){
            console.log(fieldId,'found');
            var value = inputCmp.get("v.value");
            if(value){
                inputCmp.set("v.errors", null);
            }else{
                inputCmp.set("v.errors", [{message:"Complete this field."}]);
                isValid = false;
            }
        }
        
        return isValid;

    },
    checkSourceField: function(cmp, isValid){
    	var typeSelect = cmp.find('typeSelect');
        var sourceSelect = cmp.find("sourceSelect");

    	if(typeSelect.get('v.value') === 'Email'){

            if(sourceSelect.get('v.value') === ''){
            	this.triggerToast({
		            title: "Error",
		            message: "Please select the source!",
		            type: "Error"
		        });
		        sourceSelect.set("v.errors", [{message:"Complete this field."}]);
		        isValid = false;
            }
        }
        
    	var task = cmp.get('v.task');
    	
		var newActionField = cmp.find('newActionDate');
		var nexActionDate;
		if(newActionField){
		    nexActionDate = newActionField.get('v.value');
		    newActionField.set('v.errors', null);
		
    		if(nexActionDate){
    			var nexActionDateVar = new Date(nexActionDate);
    			var activityDateVar = new Date(task.ActivityDate);
    			isValid = this.checkAndAddError(cmp, isValid, 'nextAction');
    			
    	    	if(activityDateVar && (activityDateVar >= nexActionDateVar)){
    		        newActionField.set('v.errors',[{message:"Should be greater than Today!"}]);
    		        isValid = false;
    	    	}
    		}else{
    			var inputCmp = cmp.find('nextAction');
    			if(inputCmp){
    			    inputCmp.set('v.errors', null);
    			}
    		}
		}
        return isValid;

    },
    handleSaveCallBack : function(cmp, hlpr, response, callBackInfo){
        var responseData = response.getReturnValue();
        callBackInfo.result.taskId = responseData.taskId;
        callBackInfo.result.state = 'response';
        hlpr.handleDashDockEvent(cmp, hlpr, callBackInfo.data, callBackInfo.result);
    },
    handleDashDockEvent : function(cmp, hlpr, data, result){
        var dashDockedAppEvent = $A.get("e.c:DashDockedAppEvent");
        dashDockedAppEvent.setParams({ 
            "dockedData" : result
        });
        dashDockedAppEvent.fire();
        
        var info = {message: 'Task has been created!', // default message
                    title: "Success",
                    type: "Success"};
        
        if(data.type == 'Email'){
            info.message = $A.get("$Label.c.IVP_WD_Email_Success");
        }else if(data.type == 'Call'){
            info.message = $A.get("$Label.c.IVP_WD_Log_Activity_Success");
        }
        
        hlpr.showMessage(info);
        hlpr.handleSpinner(cmp, false);
    },
    handleCalling : function(cmp, method, params, callback, hlpr, callBackInfo){
        hlpr.handleSpinner(cmp, true);
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(cmp, hlpr, response, callBackInfo);
            }else if (state === "INCOMPLETE") {
                hlpr.showMessage({message:'Please refresh the page!', type:"info"});
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    hlpr.handleErrors(errors);
                }else {
                    alert("Unknown error");
                }
            }
            hlpr.handleSpinner(cmp, false);
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    
    handleSpinner : function(component, showSpinner){
        var spinner = component.find("lgSpinner");
        if(showSpinner){
            $A.util.removeClass(spinner, "slds-hide"); 
        } else {
            $A.util.addClass(spinner, "slds-hide");
        }
    },
    
    handleErrors : function(errors){
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams){
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    }
})