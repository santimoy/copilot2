({
	doInit : function(cmp) {
        this.handleCalling(cmp, 'getInitData', {}, this.initCallback, this, null);
        this.handleSpinner(cmp, false) ;
    },
    initCallback : function(cmp, hlpr, response, callbackInfo){
        var topicsMap = response.getReturnValue();
        
       	var opts = [{ "class": "optionClass", 
                       label: '--None--', 
                       value: ''}];
		var topics = topicsMap['lstOfTopics'];
        //opts=[];
        if(topics !== undefined){
	        for(var index=0, length=topics.length; index<length; index++){
	            var topic = topics[index];
	            opts.push({ "class": "optionClass", 
	                       label: topic.Name, 
	                       value: topic.Id});
	        }        
        	
        }
        var selectCmp = cmp.find("selectTags");
        selectCmp.set("v.options", opts);
        selectCmp.set("v.value", '');
        hlpr.enableCloseButon(cmp, true);
    },
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(cmp, hlpr, response, callbackInfo);
            }
            else if (state === "INCOMPLETE") {
            	hlpr.enableCloseButon(cmp, false);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
                hlpr.enableCloseButon(cmp, false);
            }
            
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    handleLoadWatchDog : function(cmp, selectedTag ){
        var ivpTaggingAppEvent = $A.get("e.c:IVPTaggingAppEvent");
        if(ivpTaggingAppEvent){
            ivpTaggingAppEvent.setParams({ 
                "formData" : {topic:selectedTag, source:'tag'}
            });
            ivpTaggingAppEvent.fire();
        }else{
            alert('CompanyListView: Please Contact to Administrator!');
        }
    },
    handleLoadView: function(cmp, data){
        var selectCmp = cmp.find("InputSelect");
        var options = selectCmp.get('v.options');
        for(var index=0, length=options.length; index < length; index++){
            var option = options[index];
            if(option.devValue!==undefined 
               &&((data.source === 'ListViewLoad' && option.value === data.viewId)
               || ( data.source === 'Chart' && option.devValue.toLowerCase() === data.viewName.toLowerCase()))){
                
                selectCmp.set("v.value", option.value);
                this.handleLoadWatchDog(cmp, option);
                this.updateViewName(cmp, option.devValue);
                break;
            }
        }
    },
    maintainTags : function(cmp, type){
        var params = {selectedTagId:'', selectedValue:'', source:"tag", sourceType:"simple", 
					filterScope:'Mine', chkFilter:false, fieldApi:'Tags__c'};
		
        this.enableCloseButon(cmp, true);
    	var selectCmp = cmp.find("selectTags");
        if(type == 'Applying'){
            
            var selectedTag = selectCmp.get("v.value");
            var options = selectCmp.get("v.options");
            if(selectedTag == ''){
                this.showMessage({title:'Error', message:'Please select Tag first!', type:'Error'});
                return;
            }
            this.handleSpinner(cmp, true) ;
            for(var index = 0,length = options.length; index<length; index++){
                if(selectedTag == options[index].value){
                    params.selectedTagId = selectedTag; 
                    params.selectedValue = options[index].label;
                    break;
                }
            }
            this.enableCloseButon(cmp, false);
	        this.handleCalling(cmp, 'maitainTags', {inputJson : JSON.stringify(params)}, this.maitainTagsCallback, this,
	                           {type:type});
        }else if(type ==  'Removing'){
        	var selectCmp = cmp.find("selectTags");
        	selectCmp.set("v.value", '');
        	this.maitainTagsCallback(cmp, this, {}, {});
        }
        
        //this.showMessage({title:'Info', message:type+' Tag filter!', type:'Info'});
    },
    
    maitainTagsCallback : function(cmp, hlpr, response, callbackInfo){
        var action = 'clear';
        var messageType = 'Removed';
        if((callbackInfo.type === 'Applying')){
            messageType = 'Applied';
            action = 'apply';
        }
        messageType+=' tag fiter!';
        var ivpTaggingAppEvent = $A.get("e.c:IVPTaggingAppEvent");
        if(ivpTaggingAppEvent){
            ivpTaggingAppEvent.setParams({ 
                "formData" : {refresh:true, source:'tag', action:action}
            });
            ivpTaggingAppEvent.fire();
        }else{
            alert('IVP Tagging: Please Contact to Administrator!');
        }
        hlpr.handleSpinner(cmp, false) ;
        hlpr.triggerToast({title:'Success', message:messageType, type:'Success'});
    },
    enableCloseButon: function(cmp, disabled){
        var btnClose = cmp.find('btnClose');
        btnClose.set('v.disabled',disabled);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success",
            duration: 1000
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        } 
        if(message.duration){
            toastParams.duration = message.duration;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleSpinner : function(component, showSpinner){
        var spinner = component.find("tagSpinner");
        if(spinner){
	        if(showSpinner){
	            $A.util.removeClass(spinner, "slds-hide"); 
	        } else {
	            $A.util.addClass(spinner, "slds-hide");
	        }
        }
    },
})