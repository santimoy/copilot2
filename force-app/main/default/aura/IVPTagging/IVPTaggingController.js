({
    doInit : function(component, event, helper) {
        helper.doInit(component);
    },
    onTagSelectChange: function(component, event, helper) {
        var selectCmp = component.find("selectTags");
        var selectedTag = selectCmp.get("v.value");
    },
    handleApplyTag: function(component, event, helper) {
        helper.maintainTags(component, 'Applying');
    },
    handleClearTag: function(component, event, helper) {
        helper.maintainTags(component, 'Removing');
    },
    handleIVPTaggingAppEvent: function(component, event, helper) {
        var formData = event.getParam('formData');
        if(formData && formData.source == 'list'){
            var selectCmp = component.find("selectTags");
            selectCmp.set("v.value","");
        }
    }
})