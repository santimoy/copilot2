({
    
    fetchRecHelper : function(component, recordId) {
        var action = component.get("c.fetchSourcingPreference");
        action.setParams({
            "recId": recordId
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS"){
                var res = response.getReturnValue();
                if(res !== undefined && res != null){
                    component.set("v.sourcingPrefRecord", res);
                } else {
                    $A.get("e.force:closeQuickAction").fire();
                    this.toastMsg(component, event, 'There is some technical issue while fetching the record.', 'error');
                }
            } else if (response.state === "ERROR") {
                $A.get("e.force:closeQuickAction").fire();
                this.toastMsg(component, event, 'Error message='+ JSON.stringify(response.error), 'error');
            }
        });
        $A.enqueueAction(action); 
    },
    
    saveRecHelper : function(component, saveRecord) {
        var action1 = component.get("c.saveSourcingPreference");
        action1.setParams({
            "recSave": saveRecord
        });
        action1.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS"){
                var res = response.getReturnValue();
                if(res == true){
                    $A.get("e.force:closeQuickAction").fire();
                    this.toastMsg(component, event, 'Record updated Successfully', 'success');
                    $A.get('e.force:refreshView').fire();
                } else {
                    $A.get("e.force:closeQuickAction").fire();
                    this.toastMsg(component, event, 'There is some technical issue while saving the record.', 'error');
                }
            } else if (response.state === "ERROR") {
                $A.get("e.force:closeQuickAction").fire();
                this.toastMsg(component, event, 'Error message='+ JSON.stringify(response.error), 'error');
            }
        });
        $A.enqueueAction(action1); 
    },
    
    toastMsg : function (component, event, msg, type ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": msg
        });
        toastEvent.fire();
    }
})