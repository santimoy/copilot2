({
	 selectChanged: function(cmp, evt) {
        var selected = evt.target.value;
        cmp.set("v.value", selected);
    }
})