({
    iniiLoadFilter:false,
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo, errorCB,isBackGround) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        let action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        
        //To send calling in background
        if(callback.name === "loadFiltersCB" || isBackGround){
            action.setBackground();
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            cmp.set('v.actionCnt', cmp.get('v.actionCnt')-1);
            let state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                if(callback){
                    callback(cmp, hlpr, response, callbackInfo);
                }
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({message:'Please refresh the page!', type:"error"});
            }else if (state === "ERROR") {
                let errors = response.getError();
                hlpr.handleErrors(errors);
                if(errorCB){
                    errorCB(cmp, hlpr, callbackInfo);
                }
            }
        })
        //this is actually use to call server side method
        cmp.set('v.actionCnt', cmp.get('v.actionCnt')+1);
        $A.enqueueAction(action);
    },
    doInit : function(cmp) {
        let self = this;
        let records = [];
        cmp.set('v.records', records);
        this.prepareFilters(cmp);
    },
    
    prepareFilters: function(cmp,formData){
    	console.log('A3');
        this.handleCalling(cmp, 'getWDFilters', {}, this.prepareFiltersCB, this, formData);
    },
    
    prepareFiltersCB: function(cmp, hlpr, response, callbackInfo){
        let settings = response.getReturnValue();
	    if(callbackInfo && callbackInfo.refreshFilters){
		    hlpr.iniiLoadFilter = true;
		    setTimeout(function(){
			    hlpr.loadCompanySignal(cmp, callbackInfo);
		    }, 2000);
	    }
	    hlpr.populateFilters(cmp, settings);
        hlpr.loadFilters(cmp, callbackInfo);
    },
    populateFilters: function(cmp, filters){
        let selectedCats = [];
        let resetSelect = false;
        let colorFilters = cmp.get('v.colorFilters');
        if(filters){
            if(filters.colours && filters.colours.length > 0){
                filters.colours.forEach(function(colour){
                    if(colorFilters[colour] != undefined){
                        colorFilters[colour] = true;
                    }
                })
                cmp.set("v.colorFilters", colorFilters);
                cmp.set('v.filterOn', true);
            }
            if(filters.categories && filters.categories.length > 0){
                filters.categories.forEach(function(cat){
                    selectedCats.push(cat);
                })
                cmp.set("v.selectedCats",selectedCats);
                resetSelect = true;
            }
        }
        if(resetSelect){
            cmp.set('v.filterOn', true);
            this.resetSelectsWithSelect(cmp);
        }

    },
    
    loadFilters : function(cmp, callbackInfo){
        let viewId = cmp.get('v.selectedView')
        this.handleCalling(cmp, 'getInitData', {inputJson: JSON.stringify({viewId:viewId,isLimit : true})}, this.loadFiltersCB, this, callbackInfo);
    },
    loadFiltersCB : function(cmp, hlpr, response, callbackInfo){
        let wdData = response.getReturnValue();
        let cats = wdData.signals;
        //('100');
        //console.log({cats});
        let catOptions = [];

	    /*For set My Dash Signal Preferences*/
	    for(let index=0,len=cats.length; index<len; index++){
		    let cat = cats[index];
		    //console.log('107>>>>: cat>>>>: '+JSON.stringify(cat));
		    catOptions.push({value:cat.Category__c, text:cat.External_Id__c,label:cat.Name, selected:false});
	    }
	    //console.log('109> catOptions>>>>: '+JSON.stringify(catOptions));
	    cmp.set('v.catOptions', catOptions);
	    let myDashPreferences = wdData.filters.myDashSignalPreferences;
	    
	    /*For set My Dash Signal Preferences and WD Filter Category(s)*/
	    /*let filterMyDashPreferences = [];
	    for(let index=0,len=cats.length; index<len; index++){
		    let cat = cats[index];
		    catOptions.push({value:cat.Category__c, text:cat.Name,label:cat.Name, selected:false});
		    for (let i = 0; i < wdData.filters.categories.length; i++) {
			    let lenElement = wdData.filters.categories[i];
			    console.log(lenElement);
			    console.log(cat.Name);
			    console.log(cat.Category__c);
			    if(lenElement === cat.Category__c) {
				    filterMyDashPreferences.push(cat.Name);
			    }
		    }
	    }
	    cmp.set('v.catOptions', catOptions);
		console.log('filterMyDashPreferences 1>>>: '+JSON.stringify(filterMyDashPreferences));
		console.log('wdData.filters.myDashSignalPreferences 1>>>: '+JSON.stringify(wdData.filters.myDashSignalPreferences));
	    filterMyDashPreferences = filterMyDashPreferences.concat(wdData.filters.myDashSignalPreferences);
	    filterMyDashPreferences = filterMyDashPreferences.filter( function( item, index, inputArray ) {
		    return inputArray.indexOf(item) == index;
	    });
	    console.log('filterMyDashPreferences 2>>>: '+JSON.stringify(filterMyDashPreferences));
	    let myDashPreferences = filterMyDashPreferences;*/
        cmp.set('v.DashSignalPreferencesList', myDashPreferences);



        let colorselect = cmp.find("cat-select");
        if(callbackInfo && callbackInfo.refreshFilters){
            colorselect.set('v.options',[]);
            colorselect.set('v.myDashSignalPreferencesList',[]);
            colorselect.initWithSelected();
        }
        //console.log('145> catOptions>>>>: '+JSON.stringify(catOptions));
        if(catOptions.length > 0 && colorselect){
            colorselect.set('v.options', catOptions);
            if(myDashPreferences.length > 0) {
                colorselect.set('v.myDashSignalPreferencesList',myDashPreferences);
                colorselect.initWithSelected();
            } else {
                colorselect.initWithSelected();
            }

        } else{
            colorselect.set('v.options', catOptions);
            colorselect.set('v.myDashSignalPreferencesList',myDashPreferences);
            colorselect.reInit();
        }
    },
    
    loadDefault : function(cmp){
        let selectedViewId = cmp.get('v.selectedView');
        let colorObject = cmp.get('v.colorObject');
        let colorFilters = cmp.get('v.colorFilters');
        
        let selCatsOps = cmp.get('v.selectedCats');
        let selectedClrs = [];
        let selectedCats = [];
        let filterOn = false;
        
        for(let key in colorFilters){
            if(colorFilters[key] != undefined && colorFilters[key]){
                selectedClrs.push(key);
                filterOn = true;
            }
        }
        if(selCatsOps && selCatsOps.length > 0){
            selCatsOps.forEach(function(ele){
                selectedCats.push(ele);
            });
            filterOn = true;
        }
        
        // cmp.set('v.filterOn', filterOn);
        
        let isInit = cmp.get('v.init');
        let params = {viewId : selectedViewId, selClrsOps : selectedClrs, selCatsOps : selectedCats, init : isInit, isLimit : false};
        
        cmp.set('v.init', false);
        //console.log('selectedViewId>>>: '+JSON.stringify(selectedViewId));
        if(selectedViewId === ''){
            this.handleCalling(cmp, 'getInitData', {inputJson: JSON.stringify(params)}, this.initCallback, this, 'initCategory');
        }else{
            this.loadCompanySignal(cmp, params);
        }
    },
    loadCompanySignal : function(cmp, params){
        //console.log('loadCompanySignal');
        let selectedViewId = cmp.get('v.selectedView');
        let selCatsOps = cmp.get('v.selectedCats');
        let colorObject = cmp.get('v.colorObject');
        let colorFilters = cmp.get('v.colorFilters');

        let selectedClrs = [];
        let selectedCats = [];
        let filterOn = false;

        for(let key in colorFilters){
            if(colorFilters[key] != undefined && colorFilters[key]){
                selectedClrs.push(key);
                filterOn = true;
            }
        }

        if(selCatsOps && selCatsOps.length > 0){
            filterOn = true;
        }

        cmp.set('v.filterOn', filterOn);
        let isInit = cmp.get('v.init');
        if(selectedViewId){
            params.viewId = selectedViewId;
        }
        params.selClrsOps = selectedClrs;
        params.selCatsOps = selCatsOps;
        params.init = isInit;
        cmp.set('v.compCount', 0);
        if(params.viewId && params.viewId!=''){
            cmp.set('v.selectedView', params.viewId);
        }
        let callbackInfo = {};
        //console.log('params.refreshFilters>>>>: '+JSON.stringify(params.refreshFilters));
        if(params.refreshFilters){
            callbackInfo.refreshFilters = params.refreshFilters;
        }
        //console.log('Params>>>>: '+JSON.stringify(params));
        this.handleCalling(cmp, 'getCompanySignals', {inputJson: JSON.stringify(params)}, this.initCallback, this, callbackInfo);
    },
    initCallback : function(cmp, hlpr, response, callbackInfo){
        let wdData = response.getReturnValue();
        //console.log('223');
        //console.log({wdData});
        // let settings = wdData.ivpSettings;
        // hlpr.populateFilters(cmp, settings);
        let colorObject = cmp.get('v.colorObject');
        //let companySignals = wdData.signals;
        let newCompanySignals = wdData.signals;
        //console.log({newCompanySignals});

        let ssselCatsOps = cmp.get('v.selectedCats');
        //console.log('234 ssselCatsOps>>>: '+JSON.stringify(ssselCatsOps));

        let companySignals = [];
        if(ssselCatsOps.length  === 0) {
            cmp.set('v.DashSignalPreferencesList',[]);
            companySignals = newCompanySignals;
        } else {
            newCompanySignals.forEach(function(ele) {
                ssselCatsOps.forEach(function (pre){
                    if(pre.trim() === ele.Category__r.Id.trim()) {
                        companySignals.push(ele);
                    }
                })
            })
        }
        //console.log('267');
        //console.log({companySignals});
        let records = [];
        cmp.set('v.records', records);
        let today = new Date();
        let groupWithIconMap = hlpr.groupWithIconMap();
        for(let index = 0, length = companySignals.length; index < length; index ++){
            let data = {};
            data.companySignal = companySignals[index];
            data.signal = companySignals[index].Category__r;
            data.company = companySignals[index].Company__r;
            data.actions = [];
            if(data.signal.Color_HEX__c){
                data.signal.Color_HEX__c= colorObject[data.signal.Color_HEX__c.toLowerCase()];
            }else{
                data.signal.Color_HEX__c= colorObject['default'];
            }
            data.groupIcon = 'utility:info_alt';
            data.hoverHeadline = '';
            if(data.companySignal.Headline__c){
                data.hoverHeadline = data.companySignal.Headline__c
            }
            if(data.companySignal.Snippets__c){
                data.hoverHeadline+= (data.hoverHeadline!='' ? '\n\n': '' )+data.companySignal.Snippets__c;
            }
            
            if(data.signal.Image_URL__c){
                data.groupIcon = data.signal.Image_URL__c;
            }
            if(data.signal.Options__c){
                let actions = data.signal.Options__c.split(';');
                actions.sort();
                for( let aindex=0, alength=actions.length; aindex < alength; aindex++){
                    let actionTitle = actions[aindex];
                    let errorcheck = '';
                    let actionIcon = '';
                    if(actionTitle == 'Email'){
                        continue;
                    }else if(actionTitle == 'Log Activity'){
                        actionTitle = $A.get("$Label.c.IVP_WD_Action_Log_Activity");
                        errorcheck='$Label.c.IVP_WD_Action_Log_Activity does not exist.';
                        actionIcon = 'note';
                    }else if(actionTitle == 'Examine Activity'){
                        actionTitle = $A.get("$Label.c.IVP_WD_Action_Examine_Activity");
                        errorcheck='$Label.c.IVP_WD_Action_Examine_Activity does not exist.';
                        actionIcon = 'task';
                    }
                    if(errorcheck === actionTitle){
                        actionTitle = actions[aindex];
                    }
                    data.actions.push({name: actions[aindex], title: actionTitle, icon: actionIcon});
                }
            }
            data.days = '';
            if(data.companySignal.Date__c){
                let dateParts = data.companySignal.Date__c.split('-');
                let dateValue = new Date(dateParts[0], dateParts[1]-1, dateParts[2]);
                if(data.companySignal.Date__c)
                    data.days = new timeago().format(data.companySignal.Date__c);
                
                let indexOf = data.days.indexOf('hours ago');
                if(indexOf > 0){
                    data.days = 'today';
                    let hours = today.getHours();
                    let dateHour = data.days.substring(0, indexOf-1);
                    if(dateHour < hours){
                        data.days = 'yesterday';
                    }
                }else if(data.days == '1 day ago'){
                    data.days = 'yesterday';
                }
            }
            data.hasActivity = false;
            data.ltaskId = '';
            data.etaskId = '';
            data.hasltask = false;
            data.hasetask = false;
            records.push(data);
        }
        cmp.set('v.records', records);
        //console.log('callbackInfo.refreshFilters>>>: '+JSON.stringify(callbackInfo.refreshFilters));
        if(callbackInfo.refreshFilters){
            let cats = wdData.signals;
            let catOptions = new Map();
            for(let index=0,len=cats.length; index<len; index++){
                let cat = cats[index];
                //console.log('cat 355>>>>: '+JSON.stringify(cat));
                catOptions.set(cat.Category__c, {value:cat.Category__c, text:cat.Category__r.External_Id__c,label:cat.Category__r.Name});
            }
            cmp.set('v.catOptions', Array.from(catOptions.values()));
            
            let catselect = cmp.find("cat-select");
            if(catOptions.size > 0 && catselect){
                catselect.set('v.options', Array.from(catOptions.values()));
                catselect.initWithSelected();
            }
            if(hlpr.iniiLoadFilter){
                hlpr.loadFilters(cmp, callbackInfo);
                hlpr.iniiLoadFilter = false;
            }
        }
        
        hlpr.checkTask(cmp, {init:true}, hlpr);
        
    },
    handleRedirection : function(recordId){
        let navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }else{
            window.open('/'+recordId);
        }
    },
    
    daysBetween : function(start, end) {
        let one_day = 1000*60*60*24;
        let start_dateOnly = new Date(start.getFullYear(), start.getMonth(), start.getDate());
        let end_dateOnly = new Date(end.getFullYear(), end.getMonth(), end.getDate());
        
        // Convert both dates to milliseconds
        let start_ms = start_dateOnly.getTime();
        let end_ms = end_dateOnly.getTime();
        
        // Calculate the difference in milliseconds
        let difference_ms = (end_ms > start_ms ? end_ms - start_ms : start_ms - end_ms);
        
        // Convert back to days and return
        let d = Math.round(difference_ms/one_day); 
        return d;
    },
    prepareCompanySignalAction : function( cmp, label, recId) {
        let data = {};
        let records = cmp.get('v.records');
        for(let index = 0,length=records.length; index<length; index++){
            let companySignal = records[index].companySignal;
            let signal = records[index].signal;
            let company = records[index].company;
            if(recId === companySignal.Id){
                data.companyName = company.Name;
                data.companyId = companySignal.Company__c;
                data.signalCategoryId = companySignal.Category__c;
                data.signalCategoryName = signal.Name                
                data.companySignalHeadline = companySignal.Headline__c;
                data.companySignalExtId = companySignal.External_ID__c;
                data.companySignalId = companySignal.Id;
                data.companySignalName = companySignal.Name;
                data.companySignalUrl = companySignal.URL__c;
                data.companyNextActDate = company.Next_Action_Date__c;
                data.companyNextAct = company.Next_Action__c;
                
            }
        }
        data.source = 'Watchdog';
        data.action = label;
        this.enableDocked(cmp, data);;
    },
    disableDocked: function(cmp){
        let data = {hideLog:true};
        let appEvent = $A.get("e.c:DashDockedAppEvent");
        if(appEvent){
            appEvent.setParams({ 
                "dockedData" : data
            });
            appEvent.fire();    
        }else{
            alert('WatchDog: Please Contact to Administrator!');
        }
    },
    enableDocked : function(cmp,data){
        if(data.action === 'Log Activity'){
            data.type='Call';
            data.subject='Logging Call';
        }else if(data.action === 'Email'){
            data.type='Email';
            data.subject='Logging Email';
            data.subject='Logging Email';
        }else if(data.action === 'Examine Activity'){
            data.type='Other';
            data.subject='Examine - ' +data.signalCategoryName;
            data.comments=data.companySignalHeadline;
            if(data.companySignalUrl){
                data.comments +='\n\n'+data.companySignalUrl;
            }
        }
        let appEvent = $A.get("e.c:DashDockedAppEvent");
        if(appEvent){
            appEvent.setParams({ 
                "dockedData" : data
            });
            appEvent.fire();    
        }else{
            alert('WatchDog: Please Contact to Administrator!');
        }
    },
    companySignalCallback : function(cmp, hlpr, response, callbackInfo){
        let state = response.getState();
        callbackInfo.recId = response.getReturnValue();
        hlpr.enableDocked(callbackInfo);
    },
    checkTask : function(cmp, data, hlpr){
        let init = false;
        let recid = '';
        let type = '';
        let signalAction = '';
        if(data){
            if(data.init){
                init = data.init;
            }
            if(data.companySignalId){
                recid = data.companySignalId;
            }
            if(data.type){
                type = data.type;
            }
            if(data.signalAction){
                signalAction = data.signalAction;
            }
        }
        
        let records = cmp.get('v.records');
        let accountIds = [];
        let noactivityCompanies = [];
        for(let index = 0, length = records.length; index < length; index++){
            if(accountIds.indexOf(records[index].companySignal.Company__c)<0){
                accountIds.push(records[index].companySignal.Company__c);
            }
        }
        
        if(!init){
            for(let index = 0, length = records.length; index < length; index++){
                
                if(recid === records[index].companySignal.Id){
                    
                    if(!records[index].hasActivity){
                        records[index].hasActivity = true; 
                    }
                    
                    if(records[index].hasActivity ){
                        if(signalAction == 'Log Activity'){
                            if(data.taskId){
                                records[index].ltaskId = data.taskId;
                                records[index].hasltask = true;
                            }
                        }
                        if(signalAction == 'Examine Activity'){
                            if(data.taskId){
                                records[index].etaskId = data.taskId;
                                records[index].hasetask = true;
                            }
                        }
                        
                    }
                }
                
                if(records[index].hasActivity ){
                    let eleIndex = accountIds.indexOf(records[index].companySignal.Company__c);
                    if (eleIndex > -1) {
                        accountIds.splice(eleIndex, 1);
                    }
                }
            }
        }
        if(!init){
            cmp.set('v.records', records);
        }else{
            if(init){
                let params = {accountIds: JSON.stringify(accountIds)};
                this.handleCalling(cmp, 'getTasks', params, hlpr.checkTaskCallback, hlpr, params);
                return;
            }
        }
        cmp.set('v.compCount', accountIds.length);
    },
    checkTaskCallback : function(cmp, hlpr, response, callbackInfo){
        let taskMap = response.getReturnValue();
        let records = cmp.get('v.records');
        let allAccountIds = [];
        let eventCompanies = [];
        
        for(let index = 0, length = records.length; index < length; index++){
            if(allAccountIds.indexOf(records[index].companySignal.Company__c)<0){
                allAccountIds.push(records[index].companySignal.Company__c);
            }
        }
        for(let index = 0, length = records.length; index < length; index++){
            let record = records[index];
            let companySignalId = record.companySignal.Id;
            if(taskMap[companySignalId]){
                let tasks = taskMap[companySignalId];
                for(let tIndex = 0, tLength=tasks.length; tIndex<tLength; tIndex++ ){
                    let task = tasks[tIndex];
                    
                    if(eventCompanies.indexOf(task.WhatId) < 0){
                        eventCompanies.push(task.WhatId)
                        let eleIndex = allAccountIds.indexOf(task.WhatId);
                        if (eleIndex > -1) {
                            allAccountIds.splice(eleIndex, 1);
                        }
                    }
                    records[index].hasActivity = true;
                    if(task.Signal_Action__c === 'Log Activity'){
                        record.ltaskId = task.Id;
                        records[index].hasltask = true;
                    }
                    if(task.Signal_Action__c === 'Examine Activity'){
                        record.etaskId = task.Id;
                        record.hasetask = true;
                    }
                    record.hasTask = true;
                }
            }
        }
        cmp.set('v.compCount', allAccountIds.length);
        cmp.set('v.records', records);
        hlpr.handleCalling(cmp, 'getKeyContacts', callbackInfo, hlpr.keyContactsCB, hlpr, {});
    },
    keyContactsCB : function(cmp, hlpr, response, callbackInfo){
        let accKeyConMap = response.getReturnValue();
        let records = cmp.get('v.records');
        let accIds = [];
        let eventCompanies = [];
        let conIds = [];
        let cadNames = []
        
        for(let accId in accKeyConMap){
            if(accIds.indexOf(accId) < 0){
                accIds.push(accId);
            }
            conIds = conIds.concat(accKeyConMap[accId]);
        }
        if(accIds.length == 0){
            return;
        }
        
        records.forEach(function(record){
            
            if(record.signal.Salesloft_Cadence__c){
                let lowerCadName = record.signal.Salesloft_Cadence__c.toLowerCase();
                let eDripInd= -1 ;
                if(record.actions){
                    for( let aInd = record.actions.length-1; aInd>0; aInd--){
                        if(record.actions[aInd].name == 'Email Drip'){
                            eDripInd = aInd;
                            break;
                        }
                    }
                }
                
                if(accIds.indexOf(record.company.Id) >= 0 && eDripInd < 0){
                    
                    let actionTitle = $A.get("$Label.c.IVP_WD_Action_Email_Drip");
                    if(actionTitle === '$Label.c.IVP_WD_Action_Email_Drip does not exist.'){
                        actionTitle = 'Email Drip';
                    }
                    if(cadNames.indexOf(lowerCadName)<0){
                        cadNames.push(lowerCadName);
                    }
                    record.actions.push({name: 'Email Drip', title: actionTitle, icon: 'adduser'});
                }
            }
        });
        cmp.set('v.records', records);
        if(cadNames.length > 0 && conIds.length > 0){
            //re-utilizing wrapper so using paramName
            let params = {selClrsOps:conIds,selCatsOps:cadNames};
            hlpr.handleCalling(cmp, 'getCadenceMemberDetails', {inputJson: JSON.stringify(params)}, hlpr.cadenceMemberCB, hlpr, null, true);
        }
    },
    
    cadenceMemberCB : function(cmp, hlpr, response, callbackInfo){
        
        let cadMembers = response.getReturnValue();
        let dataMap={};
        let cadences=[];
        cadMembers.forEach(function(cadMember){
            if(cadMember.Cadence__c){
                if(cadMember.Cadence__r)
                cadences.push(cadMember.Cadence__r.Name.toLowerCase());
                if(cadMember.Contact__c && cadMember.Contact__r ){
                    if(cadMember.Contact__r.AccountId ){
                        let cadNames = [];
                        if(dataMap[cadMember.Contact__r.AccountId]){
                            cadNames= dataMap[cadMember.Contact__r.AccountId];
                        }
                        if(cadNames.indexOf(cadMember.Cadence__r.Name.toLowerCase())<0){
                            cadNames.push(cadMember.Cadence__r.Name.toLowerCase())
                        }
                        dataMap[cadMember.Contact__r.AccountId]= cadNames;
                    }
                }
            }
        });
        let records = cmp.get('v.records');
        records.forEach(function(record){
            let cadName = '';
            if(record.signal.Salesloft_Cadence__c && record.signal.Salesloft_Cadence__c){
                cadName = record.signal.Salesloft_Cadence__c.toLowerCase();
            }
            if(dataMap[record.company.Id] || cadences.indexOf(cadName)<0){
                let remeoveEmailDrip = false;
                let emailDripIndex = -1;
                record.actions.forEach(function(action){
                    emailDripIndex++;
                    if(action.name ==='Email Drip'){
                        let cadenceNames = dataMap[record.company.Id];
                        if(cadences.indexOf(cadName) < 0 ||
                            (cadenceNames && cadenceNames.indexOf(record.signal.Salesloft_Cadence__c.toLowerCase())>-1)){
                            record.signal.Salesloft_Cadence__c = '';
                            remeoveEmailDrip = true; 
                            action.title=$A.get('$Label.c.IVP_WD_No_Email_Drip');
                            // remeoveEmailDrip = cadences.indexOf(cadName) < 0;// if need to show disabled button when cadencemember found
                        }
                    }
                })
                if(remeoveEmailDrip && emailDripIndex>-1){
                    record.actions.splice(emailDripIndex,1);
                }
            }
        });
        cmp.set('v.records',records);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    groupWithIconMap : function(){
        return {'Financial':'custom:custom17','Growth':'standard:investment_account','Recognition':'standard:reward',
                'People':'standard:groups','Events & Marketing':'standard:event','Corporate Update':'standard:case',
                'Negative News':'custom:custom82','Uncategorized News':'standard:news'};
    },
    extractLabelVal: function(label, defaultVal){
        let labVal = $A.get(label);
        if(labVal == label+" does not exist."){
            labVal = defaultVal;
        }
        return labVal;
    },
    applyFilter: function(cmp){
        let selClrsOps = []
        
        let colorObject = cmp.get('v.colorObject');
        let colorFilters = cmp.get('v.colorFilters');
        
        for(let key in colorFilters){
            if(colorFilters[key] != undefined && colorFilters[key]){
                selClrsOps.push(key);
            }
        }
        
        let selCatsOps = cmp.get('v.selectedCats');
        cmp.set('v.init', false);
        this.loadCompanySignal(cmp, {viewId:'', modifyMetaData:false});
        //this.maintainFilters(cmp, selClrsOps, selCatsOps);
    },
    maintainFilters: function(cmp, colours, cats){
        let params = {selClrsOps:colours, selCatsOps:cats,modifyMetaData:true};
        this.handleCalling(cmp, 'setIVPSDSettings', {inputJson: JSON.stringify(params)}, this.maintainFiltersCB, this, null);
    },
    maintainFiltersCB : function(cmp, hlpr, response, callbackInfo){
    },
    resetFilter: function(cmp){
        let colorFilters = cmp.get('v.colorFilters');
        for(let key in colorFilters){
            colorFilters[key] = false;
        }
        cmp.set("v.DashSignalPreferencesList",[]);
        cmp.set("v.colorFilters",colorFilters);
        cmp.set("v.selectedClrs","[]");
        this.resetSelects(cmp);
        this.loadDefault(cmp);
        this.maintainFilters(cmp,[],[]);
    },
    resetSelects: function(cmp){
        let colorSelect = cmp.find("cat-select");
        if(colorSelect){
            //console.log('793 options>>>>: '+JSON.stringify(cmp.get('v.options')));
            //colorSelect.set('v.options',[]);
            colorSelect.set('v.myDashSignalPreferencesList',[]);
            colorSelect.initWithSelected();
        }
    },
    resetSelectsWithSelect: function(cmp){
        let colorselect = cmp.find("cat-select");
        if(colorselect){
            colorselect.initWithSelected();
        }
    },
    
    handleEmailDrip: function(cmp, csignalId){
        let records = cmp.get('v.records');
        for(let rInd = records.length-1; rInd>-1; rInd--){
            let record = records[rInd];
            if(record.companySignal.Id === csignalId){
                let param={accId:record.companySignal.Company__c, salesloftCadence:record.signal.Salesloft_Cadence__c}
                record.signal.Salesloft_Cadence__c=''
                record.actions.forEach(function(action){
                    if(action.name == 'Email Drip'){
                        action.icon = 'spinner';
                    }
                });
                cmp.set('v.records',records)
                this.handleCalling(cmp, 'processKeyContact', 
                                    {inputJson:JSON.stringify(param)}, 
                                    this.processKeyContactCB, 
                                    this, 
                                    {signalId:record.signal.Id, cSignalId: record.companySignal.Id}, 
                                    this.processKeyContactErrorCB);
                break;
            }
        }
    },
    processKeyContactCB : function(cmp, hlpr, response, callbackInfo){
        let msg= $A.get('{!$Label.c.IVP_WD_Cadence_Update}');
        hlpr.showMessage({title:'Success', message:msg, type:'success'});
        hlpr.revertCadenceIcon(cmp, callbackInfo.cSignalId, false);
    },
    processKeyContactErrorCB : function(cmp, hlpr, callbackInfo){
        if(callbackInfo){
            hlpr.revertCadenceIcon(cmp, callbackInfo.cSignalId, true);
        }
    },
    revertCadenceIcon : function(cmp, csignalId, isError){
        if(csignalId){
            let records = cmp.get('v.records');
            records.forEach(function(record){
                if(record.companySignal.Id === csignalId){
                    record.actions.forEach(function(action){
                        if(action.name === 'Email Drip'){
                            record.signal.Salesloft_Cadence__c='';
                            action.icon='adduser';
                            // if(isError){
                                action.title=$A.get('$Label.c.IVP_WD_No_Email_Drip');
                            // } 
                        }
                    })
                }
            })
            cmp.set('v.records', records);
        }
    },
    initDefault : function(cmp){
    	console.log('A1');
        let colorFilters = {'red':false, 'green':false, 'yellow':false};
        let colorObject = {
            'red': $A.get('$Label.c.IVP_WD_Color_HEX_Red'),
            'green': $A.get('$Label.c.IVP_WD_Color_HEX_Green'), 
            'yellow': $A.get('$Label.c.IVP_WD_Color_HEX_Yellow'),
            'default': $A.get('$Label.c.IVP_WD_Color_HEX_Default')
        };
        cmp.set('v.colorObject', colorObject);
        cmp.set('v.colorFilters', colorFilters);
        
        cmp.set('v.selectedView','');
        cmp.set('v.actionCnt',0);
        cmp.set('v.init',true);
    },
    handleFocus: function(id){
        let ele = document.getElementById(id);
        if(ele){
            ele.focus();
        }
    },
    handleListViewChange : function(cmp,formData){
    	console.log('A2');
       	this.prepareFilters(cmp,formData);
       	//console.log('formData>>>: '+JSON.stringify(formData));
    },
})