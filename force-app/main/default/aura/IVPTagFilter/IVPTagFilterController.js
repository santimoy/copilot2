({
    scriptsLoaded : function(component, event, helper) {
        
        helper.doInit(component);
    },
    doInit : function(component, event, helper) {
        //helper.doInit(component);
    },
    onTagSelectChange: function(component, event, helper) {
        var selectCmp = component.find("selectTags");
        var selectedTag = selectCmp.get("v.value");
        cmp.set('v.typeValue','');
    },
    handleFilterChange : function(component, event, helper) {
    	component.set('v.chkFilter',!component.get('v.chkFilter'));
    },
    handleTypeChange: function(component, event, helper) {
    	helper.handleTypeChange(component, event);
    },
    handleApplyTag: function(component, event, helper) {
    	var value = cmp.get('v.selectedTags');
    },
    handleClearTag: function(component, event, helper) {
        helper.maintainTags(component, 'Removing');
    },
    
    handleIVPTaggingAppEvent: function(component, event, helper) {
        var formData = event.getParam('formData');
        if(formData ){
        	if(formData.source == 'list'){
	        	var selectedEle = $('.dropdown-chose-list .del');
	        	if(selectedEle){
	        		selectedEle.click();
	        	}
	        	var clearEle = $('.dropdown-clear-all');
	        	if(clearEle){
	        		if(clearEle.css){
	        			clearEle.css({'display':'none'});
	        		}
	        		if(clearEle.style){
	        			clearEle.style.display='none';
	        		}
	        	}
        	}else if(formData.source == 'change'){
        	    var userId = helper.getCurrentUserId();
        	    
        		if(formData.selectedLV){
        		    if(formData.selectedLV.viewName.indexOf(userId)===-1){
            		    console.log('done');
    	        		component.set('v.selectedLV', formData.selectedLV);
    	        	}
        		}
        	}
        }
    }
})