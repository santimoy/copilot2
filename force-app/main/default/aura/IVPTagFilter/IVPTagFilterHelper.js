({
	doInit : function(cmp) {
        cmp.set('v.isProcessed', true);
        
        this.handleCalling(cmp, 'getInitData', {}, this.initCallback, this, null);
        this.handleSpinner(cmp, false) ;
    },
    initCallback : function(cmp, hlpr, response, callbackInfo){
    	
        var topicsMap = response.getReturnValue();
        
       	var opts = [{ "class": "optionClass", 
                       label: '--None--', 
                       value: ''}];
		var topics = topicsMap['lstOfTopics'];
		var topicsOther = topicsMap['lstOfTopicsOthers'];
        var optss = [];
        var groupList = [];
        var groupName = $A.get("$Label.c.IVP_SD_Tag_DD_My_Group");
        if(groupName =='$Label.c.IVP_SD_Tag_DD_My_Group does not exist.'){
        	groupName = 'My';
        }
        
        groupList.push({groupName:groupName, groupId:1});
        groupName = $A.get("$Label.c.IVP_SD_Tag_DD_All_Group");
        if(groupName =='$Label.c.IVP_SD_Tag_DD_All_Group does not exist.'){
        	groupName = 'My';
        }
        groupList.push({groupName:groupName, groupId:2});
        console.log('topics',topics);
        var groupListIndex = 0;
        var topicCount = 0;
        if(topics){
	        for(var index =0, length=topics.length; index<length; index++){
	            var topic = topics[index];
	            opts.push({ "class": "optionClass", 
	                       label: topic.Name, 
	                       value: topic.Id});
               
                optss.push({id:topic.Id, groupName : groupList[groupListIndex].groupName,
                			groupId: groupList[groupListIndex].groupId,
                       		name :topic.Name})
           
	           	if(++topicCount == 50){
					groupListIndex = 1;
	           	}
	        }
        }
        console.log('topicsOther',topicsOther);
        if(topicsOther){
        	groupListIndex = 1;
        	for(var index =0, length=topicsOther.length; index<length; index++){
	            var topic = topicsOther[index];
	            opts.push({ "class": "optionClass", 
	                       label: topic.Name, 
	                       value: topic.Id});
               
                optss.push({id:topic.Id, groupName : groupList[groupListIndex].groupName,
                			groupId: groupList[groupListIndex].groupId,
                       		name :topic.Name})
	        }
        }
        
        cmp.set('v.tags', optss);
		cmp.set('v.isProcessed', false);
		hlpr.initPicklist(optss);
    },
    initPicklist : function(optss){
    	var placeholderText = $A.get("$Label.c.IVP_SD_TAG_Search_Place_Holder");
        
        if(placeholderText =='$Label.c.IVP_SD_TAG_Search_Place_Holder does not exist.'){
        	placeholderText='Change to search tags...';
        }
        
    	$('.inpTagsPanal').dropdown({
            // the maximum number of options allowed to be selected
            limitCount: 1,
            // search field
            input: '<input type="text" maxLength="20" placeholder="'+placeholderText+'" aura:id="inpSelectedTags">',
            // dynamic data here
            data: optss,
            
            placeholder: placeholderText,
            // is search able?
            searchable: true,
            limitCountError: 'sdsdss',
            // when there's no result
            searchNoData: '<li style="color:#ddd">No tag founds!</li>',
            // callback
            choice: function(){
                if($(this)[0].name.length >= 1){
                    var dropdownDisplay = $('.dropdown-display');
                    $('.dropdown-clear-all').html('');
                    var deleteIcon = '<button class="slds-button slds-button_icon-bare" type="button" title="Clear selection" data-aura-rendered-by="1152:0"><lightning-primitive-icon data-aura-rendered-by="1153:0" lightning-primitive-icon_primitive-icon=""><svg focusable="false" data-key="delete" aria-hidden="true" class="slds-button__icon slds-button__icon_small dark" lightning-primitive-icon_primitive-icon="">'
									+'<use xlink:href="/_slds/icons/utility-sprite/svg/symbols.svg?cache=9.8.0#close" lightning-primitive-icon_primitive-icon=""></use>'
									+'</svg></lightning-primitive-icon><span class="slds-assistive-text" data-aura-rendered-by="1155:0">Clear selection</span></button>'
									
                    $('.dropdown-clear-all').html(deleteIcon);
                    $('.dropdown-clear-all').css({'display':'none'});
                    $('#inpTagsPanal').find('.dropdown-clear-all').css({'display':'block','margin-right': '-.5rem','margin-top': '-.05rem'});
                }else{
                    $('#inpTagsPanal').find('.dropdown-clear-all').css({'display':'none'});
                    var elements = document.getElementsByClassName("dropdown-chose-list");
					elements[0].children[0].innerText='';
                } 
            }
        }, $('.inpTagsPanal'));
    },
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(cmp, hlpr, response, callbackInfo);
            }
            else if (state === "INCOMPLETE") {
            	hlpr.enableCloseButon(cmp, false);
            	hlpr.handleSpinner(cmp, false);
            }else if (state === "ERROR") {
                var errors = response.getError();
                hlpr.handleErrors(errors);
                /*if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }*/
                hlpr.enableCloseButon(cmp, false);
                hlpr.handleSpinner(cmp, false);
            }
            
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    
    handleTypeChange: function(cmp, evt){
        this.maintainTags(cmp, evt, 'Applying');
    },
    
    maintainTags : function(cmp, evt, type){
    	var selectedTags = '';
    	var elements = document.getElementsByClassName("dropdown-selected");
		
    	// var elements = document.getElementsByClassName("dropdown-display");
    	if(elements){
         	// selectedTags = elements[0].getAttribute('title'); 
         	if(elements[0]){
         		selectedTags = elements[0].innerText;
         	}
    	}
        var params = {selectedTagId:'', selectedValue:'', source:"tag", sourceType:"advanced", 
        			filterScope:'Mine', chkFilter:false, fieldApi:'Tags__c'};
        
        if(type == 'Applying'){
            var inpTagsPanal = cmp.find('inpTagsPanal');
            if(!selectedTags){
                this.showMessage({title:'Error', message:'Please select Tag first!', type:'Error'});
                return;
            }
            
            var source = evt.getSource();
	        var value = source.get('v.value');
            params.filterScope = value;
            params.filterScope = (params.filterScope == 'ALL' ? 'Everything' : params.filterScope);
            params.filterScope = (params.filterScope == 'MINE' ? 'Mine' : params.filterScope);

	        var chkValue = cmp.get('v.chkFilter');
            params.chkFilter = chkValue;
            params.selectedValue = selectedTags;
          	this.handleSpinner(cmp, true);
          	var selectedLV = cmp.get('v.selectedLV');
            if(selectedLV){
            	params.selectedLVId = selectedLV.viewId;
            }
            console.log('LV:params ',params);
            this.handleCalling(cmp, 'maitainTags', {inputJson : JSON.stringify(params)}, this.maitainTagsCallback, 
	        					this, {type:type});
			
        }else if(type ==  'Removing'){
        	var selectCmp = cmp.find("selectTags");
        	selectCmp.set("v.value", '');
        	this.maitainTagsCallback(cmp, this, {}, {});
        }
    },
    
    parseLinkValue :function(linkValue){
        var wrapperElement = document.createElement('div');
        wrapperElement.innerHTML = linkValue;
        var element = wrapperElement.childNodes[0];
        var attributes = element.attributes;
        var linkMap = {label: element.innerHTML};
        for (var i = 0; i < attributes.length; i++) {
            linkMap[attributes[i].name] = attributes[i].value;
        }
        
        if(linkMap.label){
	        var iIndex = linkMap.label.indexOf('<i');
	        if(iIndex>=0){
	        	var value = linkMap.label.substring(0,iIndex);
	        	linkMap.label = value;
	        }
        }
        return linkMap;
    },
    maitainTagsCallback : function(cmp, hlpr, response, callbackInfo){
    	var resultMap = response.getReturnValue();
    	var success = resultMap['success'];
    	if(success){
    		var action = 'clear';
	        var messageType = 'Removed';
	        if((callbackInfo.type === 'Applying')){
	            messageType = 'Applied';
	            action = 'apply';
	        }
	        messageType+=' tag fiter!';
	        var ivpTaggingAppEvent = $A.get("e.c:IVPTaggingAppEvent");
	        if(ivpTaggingAppEvent){
	            ivpTaggingAppEvent.setParams({ 
	                "formData" : {refresh:true, source:'tag', action:action}
	            });
	            ivpTaggingAppEvent.fire();
	        }else{
	            alert('IVP Tagging: Please Contact to Administrator!');
	        }
	        hlpr.handleSpinner(cmp, false);
	        hlpr.triggerToast({title:'Success', message:messageType, type:'Success'});
    	}else{
    		hlpr.handleSpinner(cmp, false);
    		var errorMessage = $A.get("$Label.c.IVP_SD_TAG_Too_Many_Fltr_Msg");
	        if(errorMessage =='$Label.c.IVP_SD_TAG_Too_Many_Fltr_Msg does not exist.'){
	        	errorMessage='We cannot include listview\'s filters because multiple filters are applied already!';
	        }
    		hlpr.triggerToast({title:'Error', message:errorMessage, type:'Error'});
    	}
    },
    handleLoadWatchDog : function(cmp, selectedTag ){
        var ivpTaggingAppEvent = $A.get("e.c:IVPTaggingAppEvent");
        if(ivpTaggingAppEvent){
            ivpTaggingAppEvent.setParams({ 
                "formData" : {topic:selectedTag, source:'tag'}
            });
            ivpTaggingAppEvent.fire();
        }else{
            alert('CompanyListView: Please Contact to Administrator!');
        }
    },
    handleLoadView: function(cmp, data){
        var selectCmp = cmp.find("InputSelect");
        var options = selectCmp.get('v.options');
        for(var index=0, length=options.length; index < length; index++){
            var option = options[index];
            if(option.devValue!==undefined 
               &&((data.source === 'ListViewLoad' && option.value === data.viewId)
               || ( data.source === 'Chart' && option.devValue.toLowerCase() === data.viewName.toLowerCase()))){
                
                selectCmp.set("v.value", option.value);
                this.handleLoadWatchDog(cmp, option);
                this.updateViewName(cmp, option.devValue);
                break;
            }
        }
    },
    handleProcess : function(cmp, value){
    	cmp.set('v.isProcessed', value);
    },
    enableCloseButon: function(cmp, disabled){
        var btnClose = cmp.find('btnClose');
        if(btnClose)
        	btnClose.set('v.disabled',disabled);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown Error",
            type: "error"
        };
        
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        toastParams.message+='\n OR try again by de-selecting include current ListView filters!';
        toastParams.message+='\nPlease Contact To Administrator!';
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success",
            duration: 1000
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        } 
        if(message.duration){
            toastParams.duration = message.duration;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleSpinner : function(component, showSpinner){
        var spinner = component.find("tagSpinner");
        if(spinner){
	        if(showSpinner){
	            $A.util.removeClass(spinner, "slds-hide"); 
	        } else {
	            $A.util.addClass(spinner, "slds-hide");
	        }
        }
    },
    getCurrentUserId: function(){
        return $A.get("$SObjectType.CurrentUser.Id");
    }
})