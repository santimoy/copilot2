({
	toggleFindEmailButton : function(component) {
		var firstName = component.get("v.firstName");
		var lastName = component.get("v.lastName");
		var domain = component.get("v.domain");

		var findEmailBtnCmp = component.find("findEmailBtn");
		if(!$A.util.isEmpty(firstName) && !$A.util.isEmpty(lastName) && !$A.util.isEmpty(domain)){
			findEmailBtnCmp.set("v.disabled", false);
			console.log('findEmailBtn enabled');
		} else {
			findEmailBtnCmp.set("v.disabled", true);
			console.log('findEmailBtn disabled');
		}
	}
})