({
	doInit : function(component, event, helper) {
		var action = component.get("c.initializeEmailDiscoverer");

		action.setParams({
			"recordId" : component.get("v.recordId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				var contactInfo = response.getReturnValue();
				component.set("v.firstName", contactInfo.firstName);
				component.set("v.lastName", contactInfo.lastName);
				component.set("v.domain", contactInfo.domain);
			} else {
				console.log('Problem getting contact info, response state: ' + state);
			}

			helper.toggleFindEmailButton(component);
		});
		$A.enqueueAction(action);

	},

	findEmails : function(component, event, helper) {
		component.set("v.isFinding", true);
		component.set("v.hasSuccessfulEmails", false);
		component.set("v.hasFindEmailFired", false);
		var action = component.get("c.findDiscovererEmails");

		action.setParams({
			"firstNameIn" : component.get("v.firstName"),
			"lastNameIn" : component.get("v.lastName"),
			"domainIn" : component.get("v.domain")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				var okEmailList = [];
				var badEmailList = [];
				response.getReturnValue().forEach(function(a){
					console.log(a.CheckerResults);
					if( a.CheckerResults.status === "Ok" ){
						var obj = { emailAddress: a.CheckerResults.emailAddressProvided, status: a.CheckerResults.status, additionalInfo: a.CheckerResults.additionalStatus };
						okEmailList.push(obj);
					} else {
						var obj = { emailAddress: a.CheckerResults.emailAddressProvided, status: a.CheckerResults.status, additionalInfo: a.CheckerResults.additionalStatus };
						badEmailList.push(obj);
					}
				});

				if( okEmailList.length != 0 ){
					component.set("v.hasSuccessfulEmails", true);

					var okFirstEmail = okEmailList[0].emailAddress;

					var okBCCEmails = '';
					if( okEmailList.length > 1 ){
						for(var i = 1; i < okEmailList.length; i++){
							okBCCEmails += okEmailList[i].emailAddress + ';';
						}
					}

					if( okBCCEmails ){
						component.set("v.validEmailAddresses", okFirstEmail + '?bcc=' + okBCCEmails);
					} else {
						component.set("v.validEmailAddresses", okFirstEmail);
					}
				} else {
					component.set("v.hasSuccessfulEmails", false);
				}
				console.log('valid first email address is' + okFirstEmail + 'valid bcc email address' + okBCCEmails);
                
				if( badEmailList.length != 0 ){
					var blasterFirstEmail = '';
					var blasterIndex = 1;
					if( okEmailList.length != 0 ){
						blasterFirstEmail = okEmailList[0].emailAddress;
						blasterIndex = 0;
					} else {
						blasterFirstEmail = badEmailList[0].emailAddress;
					}

					var blasterBCCEmails = '';
                    if(okEmailList.length>1){
                        for(var i = 1; i < okEmailList.length; i++){
							blasterBCCEmails += okEmailList[i].emailAddress + ';';
						}
                    }
                    
					if( badEmailList.length > blasterIndex + 1 ){
						for(var i = blasterIndex; i < badEmailList.length; i++){
							blasterBCCEmails += badEmailList[i].emailAddress + ';';
						}
					}

					if( blasterBCCEmails ){
						component.set("v.blasterEmailAddresses", blasterFirstEmail + '?bcc=' + blasterBCCEmails);
					} else {
						component.set("v.blasterEmailAddresses", blasterFirstEmail);
					}
				}

                console.log('blaster first email address is' + blasterFirstEmail + 'blaster bcc email address' + blasterBCCEmails);
                
				var allEmails = [];
				Array.prototype.push.apply(allEmails, okEmailList);
				Array.prototype.push.apply(allEmails, badEmailList);

				component.set("v.allEmails", allEmails);
				component.set("v.hasFindEmailFired", true);
				component.set("v.isFinding", false);
			} else {
				console.log('Problem getting contact info, response state: ' + state);
				component.set("v.isFinding", false);
			}
		});
		$A.enqueueAction(action);
	},

	handleRowAction : function(component, event, helper){
		component.set("v.isFinding", true);
		var selectedMenuItemValue = event.getParam("value");
		var index = event.getSource().get("v.tabindex");

		var allEmails = component.get("v.allEmails");

		switch(selectedMenuItemValue){
			case "update_key_contact":
				var updateKeyContactAction = component.get("c.updateKeyContact");

				updateKeyContactAction.setParams({
					"accountId" : component.get("v.recordId"),
					"emailAddressIn" : allEmails[index]["emailAddress"]
				});
		
				updateKeyContactAction.setCallback(this, function(response) {
					var state = response.getState();
					if(state === "SUCCESS") {
						component.set("v.isFinding", false);
					} else {
						console.log('Problem getting contact info, response state: ' + state);
					}
				});
				$A.enqueueAction(updateKeyContactAction);
				break;
			case "create_key_contact":
				var createKeyContactAction = component.get("c.createKeyContact");

				createKeyContactAction.setParams({
					"accountId" : component.get("v.recordId"),
					"firstNameIn" : component.get("v.firstName"),
					"lastNameIn" : component.get("v.lastName"),
					"emailAddressIn" : allEmails[index]["emailAddress"]
				});
		
				createKeyContactAction.setCallback(this, function(response) {
					var state = response.getState();
					if(state === "SUCCESS") {
						component.set("v.isFinding", false);
					} else {
						console.log('Problem getting contact info, response state: ' + state);
					}
				});
				$A.enqueueAction(createKeyContactAction);
				break;
			case "send_email":
				break;
		}
	},

	emailBlastAllValid : function(component, event, helper) {
        console.log('Inside method call');
		var validEmailAddresses = component.get("v.validEmailAddresses");
        console.log('Inside method call,,',validEmailAddresses);
		window.location.href = "mailto:" + validEmailAddresses;
	},
	
	emailBlastAll : function(component, event, helper) {
		var blasterEmailAddresses = component.get("v.blasterEmailAddresses");
		window.location.href = "mailto:" + blasterEmailAddresses;
	},

	emailFieldsChanged : function(component, event, helper) {
		helper.toggleFindEmailButton(component);

		component.set("v.hasFindEmailFired", false);
		component.set("v.hasSuccessfulEmails", false);
		component.set("v.validEmailAddresses", "");
		component.set("v.blasterEmailAddresses", "");
		component.set("v.allEmails", new Object());
	}
})