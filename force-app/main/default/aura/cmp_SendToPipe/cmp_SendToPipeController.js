({
	//if shipping city is blank
	//Remove shipping
	//create inbound field
	//dont need prior, current, next
	//spaces in locations

	fetchInit : function(component, event, helper) {
        
		helper.doInit(component, event, helper);

	},
	
	refreshTheViewOnlyOnSuccess : function(component, event, helper) {
	    var msg = event.getParam("message");
	    var type = event.getParam("type");
	    if(msg == "Successfull")
	    {
	        helper.doInit(component, event, helper); 
	    }
	},
	saveSendToPipe : function(component, event, helper) {
		$A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
		helper.validateRequiredFields(component,helper);
        
		//helper.saveSendToPipeData(component,helper);
	},
	checkModalOpen : function(component, event, helper) {
		if(component.get("v.openSendToPipeModal")==true)
		{
			component.set("v.locationEdit",false);
			var action = component.get("c.fetchInitData");

		action.setParams({
			recId:component.get("v.recordId")
		});

		action.setCallback(this,function(response) 
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
				var tempRes = response.getReturnValue();
				var res = JSON.parse(tempRes);
				var d = new Date();
                component.set("v.isHavingOpenOpp", res.isHavingOpenOpp);
				component.set("v.oppName",res.accountData.Name+' - '+(d.getMonth()+1)+'/'+d.getFullYear());
				component.set("v.oppDescription",res.accountData.Short_Description__c);
				if(res.accountData.ShippingCity ==''||res.accountData.ShippingCity ==null)
				{
					component.set("v.accShippingStreet",res.accountData.BillingStreet);
					component.set("v.accShippingCity",res.accountData.BillingCity);
					component.set("v.accShippingState",res.accountData.BillingStateCode);
					component.set("v.accShippingCountry",res.accountData.BillingCountry);
					component.set("v.accShippingPostalCode",res.accountData.BillingPostalCode);
				}
				else
				{
					component.set("v.accShippingStreet",res.accountData.ShippingStreet);
					component.set("v.accShippingCity",res.accountData.ShippingCity);
					component.set("v.accShippingState",res.accountData.ShippingStateCode);
					component.set("v.accShippingPostalCode",res.accountData.ShippingPostalCode);
					component.set("v.accShippingCountry",res.accountData.ShippingCountry);
				}
				var dFullYear = d.getFullYear();
                //Date logic
                component.set("v.priorPriorYear",(dFullYear-2));
                component.set("v.priorYear",(dFullYear-1));
                component.set("v.currentYear",dFullYear);
                component.set("v.nextYear",(dFullYear+1));

				component.set("v.priorPriorRevenueValue",res.objFinancialData.priorPriorRevenueValue);
				component.set("v.priorPriorEbitaValue",res.objFinancialData.priorPriorEbitaValue);
				component.set("v.priorRevenueValue",res.objFinancialData.priorRevenueValue);
				component.set("v.priorEbitaValue",res.objFinancialData.priorEbitaValue);
				component.set("v.currentRevenueValue",res.objFinancialData.currentRevenueValue);
				component.set("v.currentEbitaValue",res.objFinancialData.currentEbitaValue);
				component.set("v.nextRevenueValue",res.objFinancialData.nextRevenueValue);
				component.set("v.nextEbitaValue",res.objFinancialData.nextEbitaValue);
                
                component.set("v.priorPriorNetValue",res.objFinancialData.priorPriorNetValue);
				component.set("v.priorPriorGrossValue",res.objFinancialData.priorPriorGrossValue);
				component.set("v.priorNetValue",res.objFinancialData.priorNetValue);
				component.set("v.priorGrossValue",res.objFinancialData.priorGrossValue);
				component.set("v.currentNetValue",res.objFinancialData.currentNetValue);
				component.set("v.currentGrossValue",res.objFinancialData.currentGrossValue);
				component.set("v.nextNetValue",res.objFinancialData.nextNetValue);
				component.set("v.nextGrossValue",res.objFinancialData.nextGrossValue);

				if(!$A.util.isEmpty(res.objFinancialData.revenueType))
                {
                    component.set("v.revenueValue", res.objFinancialData.revenueType);
                }
                
				//Picklist Inits
				component.set("v.dealTypeOptions",res.dealTypeOptions);
				component.set("v.companyQualityOptions",res.companyQualityOptions);
				component.set("v.dealSizeOptions",res.dealSizeOptions);
				component.set("v.dealImminenceOptions",res.dealImminenceOptions);
				component.set("v.capitalHistoryOptions",res.capitalHistoryOptions);
				component.set("v.stageOptions",res.stageOptions);
				component.set("v.countryOptions",res.countryOptions);
				component.set("v.nextAction",res.accountData.Next_Action__c);
				component.set("v.nextActionDate",res.accountData.Next_Action_Date__c);
				component.set("v.dealTypeValue",res.accountData.Deal_Type__c);
				component.set("v.companyQualityValue",res.accountData.Company_Quality__c);
				component.set("v.dealSizeValue",res.accountData.Deal_Size__c);
				component.set("v.dealImminenceValue",res.accountData.Deal_Imminence__c);
				component.set("v.capitalHistoryValue",res.accountData.Capital_History__c);
				component.set("v.totalFundingRaisedValue",(res.accountData.Total_Funding_Raised__c!=null?res.accountData.Total_Funding_Raised__c:res.accountData.DWH_Total_Funding_Raised__c))
				component.set("v.countryToStates",res.countryToStates);
				if(component.get("v.accShippingCountry")!=null)
				{
					var countryConv = component.get("v.countryToStates");
					var countrySelect = component.get("v.accShippingCountry");
					component.set("v.stateOptions",countryConv[countrySelect]);
				}
				if(res.currentUser.Primary_Partner__c!=null)
				{
					var partnerObj = {rId:'',rName:''};
					partnerObj.rId = res.currentUser.Primary_Partner__c;
					partnerObj.rName = res.currentUser.Primary_Partner__r.Name;
					component.set("v.partnerObject",partnerObj);	
				}
				setTimeout(function() {
				    if(document.getElementById('strike-input-nextActionInput') != null && document.getElementById('strike-input-nextActionInput') != undefined)
					    document.getElementById('strike-input-nextActionInput').focus();
				}, 0);
			}
			else if(state === "INCOMPLETE")
			{
			}
			else if(state === "ERROR")
			{
				var errors = response.getError();
				if(errors)
				{
					if(errors[0] && errors[0].message)
					{
						console.log("Error message: " + errors[0].message);
					}
				}else
				{
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
		}
	},

	clickSendToPipe : function(component, event, helper) {
		component.set("v.openSendToPipeModal",true);
	},
	countryChange : function(component, event, helper) {
		var countryConv = component.get("v.countryToStates");
		var countrySelect = component.get("v.accShippingCountry");
		component.set("v.stateOptions",countryConv[countrySelect]);
		component.set("v.accShippingState",'-None-');
		//component.set("v.openSendToPipeModal",true);
	},
	closeModal :function(component){
		component.set("v.openSendToPipeModal",false);
	},
	toggleOpportunityInfo:function(component){
		var toggleVar = component.get("v.openOpportunityInfo");
		component.set("v.openOpportunityInfo",!toggleVar);
	},
	toggleDealInfo:function(component){
		var toggleVar = component.get("v.openDealInfo");
		component.set("v.openDealInfo",!toggleVar);
	},
	toggleHoringInfo:function(component){
		var toggleVar = component.get("v.horingInfo");
		component.set("v.horingInfo",!toggleVar);
	},
	toggleLeibsInfo:function(component){
		var toggleVar = component.get("v.openLeibsInfo");
		component.set("v.openLeibsInfo",!toggleVar);
	},
	toggleFinancials:function(component){
		var toggleVar = component.get("v.openFinancials");
		component.set("v.openFinancials",!toggleVar);
	},
	locationEditAction :function(component){
		component.set("v.locationEdit",true);
	},

	handleBubbling : function(cmp, evt, help)
	{
		help.lkupFunc(cmp,evt);
	}
})