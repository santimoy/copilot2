({
	lkupFunc : function(cmp,evt)
	{
		var srchVal = evt.getParam('lkupStr');
		var objAPIName = evt.getParam('objAPIName');

		var action = cmp.get("c.typeAheadFuncLtng");
		action.setParams({
												"rName":srchVal,
												"sObjName":objAPIName,
												"selIds":[],
												"filter":""

										 });

		action.setCallback(this,function(response)
		{
			var state = response.getState();

			if (state === "SUCCESS")
			{
				var res = response.getReturnValue();

				var tempRes = JSON.parse(JSON.stringify(res));

				evt.getSource().set('v.srchList',tempRes);
			}


			else if(state === "INCOMPLETE")
			{
			}
			else if(state === "ERROR")
			{
				var errors = response.getError();
				if(errors)
				{
						if(errors[0] && errors[0].message)
						{
								console.log("Error message: " + errors[0].message);
						}
				}else
				{
						console.log("Unknown error");
				}
			}
		});

		$A.enqueueAction(action);
	},
	validateRequiredFields: function(component,helper)
	{
	    component.set("v.isSavingSendToPipe", true);
		var error = false;
		if(component.get("v.partnerObject")==null)
		{
			error = true;
			var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
						title: 'Error',
						message: 'Please select a partner',
						type: 'error' ,
						 mode: 'dismissible',
						});
					toastEvent.fire();

		}
		if(component.get("v.dealTypeValue")==null||component.get("v.dealTypeValue")=='')
		{
			error = true;
			var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
						title: 'Error',
						message: 'Please select a deal type',
						type: 'error' ,
						 mode: 'dismissible',
						});
					toastEvent.fire();
		}
		if(!error)
		{
			helper.saveSendToPipeData(component,helper);
		}
		else
		{
		    component.set("v.isSavingSendToPipe", false);
			$A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
		}
	},
	saveSendToPipeData: function(component,helper)
	{
  		var action = component.get("c.saveData");
        var financialData = {};
        financialData.priorPriorRevenueValue = component.get("v.priorPriorRevenueValue");
        financialData.priorPriorEbitaValue = component.get("v.priorPriorEbitaValue");
        financialData.priorPriorNetValue = component.get("v.priorPriorNetValue");
        financialData.priorPriorGrossValue = component.get("v.priorPriorGrossValue");
        
        financialData.priorRevenueValue = component.get("v.priorRevenueValue");
        financialData.priorEbitaValue = component.get("v.priorEbitaValue");
        financialData.priorNetValue = component.get("v.priorNetValue");
        financialData.priorGrossValue = component.get("v.priorGrossValue");
        
        financialData.currentRevenueValue = component.get("v.currentRevenueValue");
        financialData.currentEbitaValue = component.get("v.currentEbitaValue");
        financialData.currentNetValue = component.get("v.currentNetValue");
        financialData.currentGrossValue = component.get("v.currentGrossValue");
        
        financialData.nextRevenueValue = component.get("v.nextRevenueValue");
        financialData.nextEbitaValue = component.get("v.nextEbitaValue");
        financialData.nextNetValue = component.get("v.nextNetValue");
        financialData.nextGrossValue = component.get("v.nextGrossValue");
        
		action.setParams({
			recordId:component.get("v.recordId"),
			oppName:component.get("v.oppName"),
			oppDescription:component.get("v.oppDescription"),
			accShippingStreet:component.get("v.accShippingStreet"),
			accShippingCity:component.get("v.accShippingCity"),
			accShippingState:component.get("v.accShippingState"),
			accShippingCountry:component.get("v.accShippingCountry"),
			accShippingPostalCode:component.get("v.accShippingPostalCode"),
			partnerObject:(component.get("v.partnerObject")!=null?component.get("v.partnerObject").rId:''),
			opportunityTeamMembers:JSON.stringify(component.get("v.opportunityTeamMembers")),
			nextAction:component.get("v.nextAction"),
			nextActionDate:component.get("v.nextActionDate"),
			dealTypeValue:component.get("v.dealTypeValue"),
			companyQualityValue:component.get("v.companyQualityValue"),
			dealSizeValue:component.get("v.dealSizeValue"),
			dealImminenceValue:component.get("v.dealImminenceValue"),
			pipeStatusValue:component.get("v.pipeStatusValue"),
			capitalHistoryValue:component.get("v.capitalHistoryValue"),
			stageValue:component.get("v.stageValue"),
			totalFundingRaisedValue:component.get("v.totalFundingRaisedValue"),
			inboundNameOfBank:component.get("v.inboundNameOfBank"),
			interest:component.get("v.interestValue"),
			numberOfEmployees:component.get("v.numberOfEmployeesValue"),
			otherKPI:component.get("v.otherKPIValue"),
			strRevenueType:component.get("v.revenueValue"),
			financialDataJSON : JSON.stringify(financialData),
			pipeStatus:component.get("v.pipeStatusValue"),  
			investors:component.get("v.investors")
		});

		action.setCallback(this,function(response)
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
				$A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
				component.set("v.openSendToPipeModal",false);
                
				$A.get("e.force:refreshView").fire();
                /* START: Created on 10/04/2019 by Sukesh. Hack to solve refresh related issue on opp creation from send to pipe */
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Success',
                    message: 'Successfull', 
                    type: 'success' ,
                    mode: 'dismissible',
                });
                toastEvent.fire();
                /* END: Created on 10/04/2019 by Sukesh. Hack to solve refresh related issue on opp creation from send to pipe */
			}
			else {

	            var toastEvent = $A.get("e.force:showToast");
	            var message = '';

	            if (state === "INCOMPLETE") {
	                message = 'Server could not be reached. Check your internet connection.';
	            } else if (state === "ERROR") {
	                var errors = response.getError();
	                if (errors) {
	                    for(var i=0; i < errors.length; i++) {
	                        for(var j=0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
	                            message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
	                        }
	                        if(errors[i].fieldErrors) {
	                            for(var fieldError in errors[i].fieldErrors) {
	                                var thisFieldError = errors[i].fieldErrors[fieldError];
	                                for(var j=0; j < thisFieldError.length; j++) {
	                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
	                                }
	                            }
	                        }
	                        if(errors[i].message) {
	                            message += (message.length > 0 ? '\n' : '') + errors[i].message;
	                        }
	                    }
	                } else {
	                    message += (message.length > 0 ? '\n' : '') + 'Unknown error';
	                }
	            }

	            toastEvent.setParams({
	                title: 'Error',
	                type: 'error',
	                message: message
	            });
	            $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');

	            toastEvent.fire();
	            // if(errorHandler) {
	            //     errorHandler(response);
	            // }
	            component.set("v.isSavingSendToPipe", false);
        }
		});
		$A.enqueueAction(action);
	},
	toastErrorMessage : function (messageTitle, messageBody, messageType ) {
		var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
			title: messageTitle,
			message: messageBody,
			type: messageType ,
			 mode: 'dismissible',
			});
		toastEvent.fire();
	},
	
	doInit : function(component, event, helper) {
        
		var action = component.get("c.fetchInitData");

		action.setParams({
			recId:component.get("v.recordId")
		});

		action.setCallback(this,function(response)
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
				var tempRes = response.getReturnValue();
				var res = JSON.parse(tempRes);
               
                component.set("v.isHavingOpenOpp", res.isHavingOpenOpp);
                
				var d = new Date();
				component.set("v.oppName",res.accountData.Name+' - '+(d.getMonth()+1)+'/'+d.getFullYear());
				component.set("v.oppDescription",res.accountData.Short_Description__c);
				if(res.accountData.ShippingCity ==''||res.accountData.ShippingCity ==null)
				{
					component.set("v.accShippingStreet",res.accountData.BillingStreet);
					component.set("v.accShippingCity",res.accountData.BillingCity);
					if(res.accountData.BillingState==null||res.accountData.BillingState=='')
					{
						component.set("v.accShippingState",'-None-');
					}
					else
					{
						component.set("v.accShippingState",res.accountData.BillingState);
					}
					component.set("v.accShippingCountry",res.accountData.BillingCountry);
					component.set("v.accShippingPostalCode",res.accountData.BillingPostalCode);
				}
				else
				{
					component.set("v.accShippingStreet",res.accountData.ShippingStreet);
					component.set("v.accShippingCity",res.accountData.ShippingCity);
					if(res.accountData.ShippingState==null||res.accountData.ShippingState=='')
					{
						component.set("v.accShippingState",'-None-');
					}
					else
					{
						component.set("v.accShippingState",res.accountData.ShippingState);
					}
					component.set("v.accShippingState",res.accountData.ShippingState);
					component.set("v.accShippingPostalCode",res.accountData.ShippingPostalCode);
					component.set("v.accShippingCountry",res.accountData.ShippingCountry);
				}
				//Date logic
				var dFullYear = d.getFullYear();
				component.set("v.priorPriorYear",(dFullYear-2));
                component.set("v.priorYear",(dFullYear-1));
                component.set("v.currentYear",dFullYear);
                component.set("v.nextYear",(dFullYear+1));

				component.set("v.priorPriorRevenueValue",res.objFinancialData.priorPriorRevenueValue);
				component.set("v.priorPriorEbitaValue",res.objFinancialData.priorPriorEbitaValue);
				component.set("v.priorRevenueValue",res.objFinancialData.priorRevenueValue);
				component.set("v.priorEbitaValue",res.objFinancialData.priorEbitaValue);
				component.set("v.currentRevenueValue",res.objFinancialData.currentRevenueValue);
				component.set("v.currentEbitaValue",res.objFinancialData.currentEbitaValue);
				component.set("v.nextRevenueValue",res.objFinancialData.nextRevenueValue);
				component.set("v.nextEbitaValue",res.objFinancialData.nextEbitaValue);
                
                component.set("v.priorPriorNetValue",res.objFinancialData.priorPriorNetValue);
				component.set("v.priorPriorGrossValue",res.objFinancialData.priorPriorGrossValue);
				component.set("v.priorNetValue",res.objFinancialData.priorNetValue);
				component.set("v.priorGrossValue",res.objFinancialData.priorGrossValue);
				component.set("v.currentNetValue",res.objFinancialData.currentNetValue);
				component.set("v.currentGrossValue",res.objFinancialData.currentGrossValue);
				component.set("v.nextNetValue",res.objFinancialData.nextNetValue);
				component.set("v.nextGrossValue",res.objFinancialData.nextGrossValue);

				if(!$A.util.isEmpty(res.objFinancialData.revenueType))
                {
                    component.set("v.revenueValue", res.objFinancialData.revenueType);
                }

				//Picklist Inits
				component.set("v.dealTypeOptions",res.dealTypeOptions);
				component.set("v.companyQualityOptions",res.companyQualityOptions);
				component.set("v.dealSizeOptions",res.dealSizeOptions);
				component.set("v.dealImminenceOptions",res.dealImminenceOptions);
				component.set("v.pipeStatusOptions",res.pipeStatusOptions);
				component.set("v.capitalHistoryOptions",res.capitalHistoryOptions);
				component.set("v.stageOptions",res.stageOptions);
				component.set("v.revenueOptions",res.lstRevenueTypeOptions);
				component.set("v.countryOptions",res.countryOptions);
				component.set("v.nextAction",res.accountData.Next_Action__c);
				component.set("v.nextActionDate",res.accountData.Next_Action_Date__c);
				component.set("v.dealTypeValue",res.accountData.Deal_Type__c);
				component.set("v.companyQualityValue",res.accountData.Company_Quality__c);
				component.set("v.dealSizeValue",res.accountData.Deal_Size__c);
				component.set("v.dealImminenceValue",res.accountData.Deal_Imminence__c);
				component.set("v.pipeStatusValue",res.accountData.Pipe_Status__c);
				component.set("v.capitalHistoryValue",res.accountData.Capital_History__c);
				component.set("v.totalFundingRaisedValue",(res.accountData.Total_Funding_Raised__c!=null?res.accountData.Total_Funding_Raised__c:res.accountData.DWH_Total_Funding_Raised__c))
				component.set("v.numberOfEmployeesValue",res.accountData.NumberOfEmployees);
				if(res.currentUser.Primary_Partner__c!=null)
				{
					var partnerObj = {rId:'',rName:''};
					partnerObj.rId = res.currentUser.Primary_Partner__c;
					partnerObj.rName = res.currentUser.Primary_Partner__r.Name;
					component.set("v.partnerObject",partnerObj);	
				}
			
            //$A.get('e.force:refreshView').fire();
            }
			else if(state === "INCOMPLETE")
			{
			}
			else if(state === "ERROR")
			{
				var errors = response.getError();
				if(errors)
				{
					if(errors[0] && errors[0].message)
					{
						console.log("Error message: " + errors[0].message);
					}
				}else
				{
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);

	},
})