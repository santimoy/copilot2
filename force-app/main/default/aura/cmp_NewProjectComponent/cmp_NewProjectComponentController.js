({
	init : function(component, event, helper) {
    
    helper.fetchPickListValCategory(component, 'Project_Category__c',  'Select2');
    
    var themeAction = component.get("c.fetchTheme");
    themeAction.setCallback(this, function(response) {
      var res =response.getReturnValue();
      if(res=='Theme4t')
      {
        component.set('v.sf1',true);
      }
      var domainAction = component.get("c.fetchDomain");
      domainAction.setCallback(this, function(response) {
        var res =response.getReturnValue();
         component.set("v.sysDomain",res);
         var customSettingAction = component.get("c.fetchKrowEndUrl");
              //set callback   
              customSettingAction.setCallback(this, function(response) {
                 if (response.getState() == "SUCCESS") {
                  component.set("v.endURL",response.getReturnValue());
                    document.getElementById('companySelectLookup').focus();
                    helper.getAllFields(component, event, helper);
                    helper.fetchPicklistValues(component, 'Project_Group__c', 'Project_Category__c');
                    helper.fetchPicklistValues2(component, 'Project_Category__c', 'Insight_Project_Type__c');
                    helper.fetchPicklistValues3(component, 'Category__c', 'Metric__c');
                 }
              });
              $A.enqueueAction(customSettingAction);
       
      });
      $A.enqueueAction(domainAction);
     
      

    });
    $A.enqueueAction(themeAction);
		
    
	},
    
    onPicklistChange: function(component, event, helper) {
       
       helper.addCategoryType(component, event, helper, event.getSource().get("v.value"));
    },
    
  addToProject: function(component, event, helper) {
    $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
    var addAction = component.get("c.addUserToProject");
    addAction.setParams({projectId:event.currentTarget.id,companyId:component.get('v.companyObject.rId')});
    addAction.setCallback(this, function(response) {
      if(response.getReturnValue()!='ERROR')
      {
        var res =JSON.parse(response.getReturnValue());
        component.set("v.companyProjects",res);
      }
      $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
    });
    $A.enqueueAction(addAction);
  },
  companyChange: function(component, event, helper) {
    if(component.get("v.companyObject")==null)
    {
         component.set("v.companySelected",false);
         component.set("v.companyProjects",[]);
    }
    else
    {
      var selCompanyAction = component.get("c.fetchCompanyProjects");
      selCompanyAction.setParams({companyId:component.get('v.companyObject.rId')});
      selCompanyAction.setCallback(this, function(response) {
        var res =JSON.parse(response.getReturnValue());
        component.set("v.companyProjects",res);
        helper.isJunctionCompany(component, event, helper);
      });
      $A.enqueueAction(selCompanyAction);
    }
  
  },
  startProjectCreation: function(component, event, helper) {
   component.set("v.companySelected",true);
    var opts = [];
      opts.push({"class": "optionClass", label: "--None--", value: "", selected: "true"});
      component.find("type1").set("v.options", opts);
  },
  
	onControllerFieldChange: function(component, event, helper) {
      // get the selected value
      var controllerValueKey = event.getSource().get("v.value");
 
      // get the map values   
      var Map = component.get("v.depnedentFieldMap");
 
      // check if selected value is not equal to None then call the helper function.
      // if controller field value is none then make dependent field value is none and disable field
      if (controllerValueKey != '--- None ---') {
 
         // get dependent values for controller field by using map[key].  
         // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
         // map['India'] = its return all dependent picklist values.
         var ListOfDependentFields = Map[controllerValueKey];
         helper.fetchDepValues(component, ListOfDependentFields,helper);
 
      } else {
         var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: '--- None ---'
         }];
         component.find('projectCategoryList').set("v.options", defaultVal);
         component.set("v.isDependentDisable", true);
      }
   },
   onControllerFieldChange2: function(component, event, helper) {
      // get the selected value
      var controllerValueKey = event.getSource().get("v.value");
 
      // get the map values   
      var Map = component.get("v.depnedentFieldMap2");
 
      // check if selected value is not equal to None then call the helper function.
      // if controller field value is none then make dependent field value is none and disable field
      if (controllerValueKey != '--- None ---') {
 
         // get dependent values for controller field by using map[key].  
         // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
         // map['India'] = its return all dependent picklist values.
         var ListOfDependentFields = Map[controllerValueKey];
         helper.fetchDepValues2(component, ListOfDependentFields);
 
      } else {
         var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: '--- None ---'
         }];
         component.find('insightProjectType').set("v.options", defaultVal);
         component.set("v.isDependentDisable2", true);
      }
      helper.onControllerFieldChange3(component,event,helper);
   },
  
 
   // function call on change tha Dependent field    
   onDependentFieldChange: function(component, event, helper) {
   },
	handleBubbling : function(cmp, evt, help)
	{
		help.lkupFunc(cmp,evt);
	},
	createProject : function(cmp, evt, help)
	{	
		$A.util.toggleClass(cmp.find("spinnerIcon"), 'slds-hide');
		help.validateFields(cmp,evt,help);
	},
	handleCompanyChange:function(cmp, ev, help)
	{
    // if(ev.getParam('selName')=='Insight Onsite - Portfolio Wide')
    // {
    //   cmp.set("v.junctionLookup",true);
    // }
    // else
    // {
    //   cmp.set("v.junctionLookup",false);
    // }
		// var fetchAction = cmp.get("c.fetchAccountTeam");
		// fetchAction.setParams({accId:ev.getParam('selId')});
  //       fetchAction.setCallback(this, function(response) {
  //       	var res =response.getReturnValue();
  //       	var lst = [];
  //       	for(var i = 0; i<res.length;i++)
  //       	{
  //       		lst[i] = {rId:res[i].UserId,rName:(res[i].TeamMemberRole=='Lead Partner'?'*Lead Partner* ':'')+res[i].User.Name,isLead:res[i].TeamMemberRole};
  //       	}
		// 	cmp.find("projectPartnerLookup").set("v.selValList",lst);
  //       });
  //       $A.enqueueAction(fetchAction);
	},
})