({
    loadTaskRecord : function(component, helper) {
        var action = component.get("c.fetchMeetingAttendees");
        console.log('ecordId: ',component.get('v.recordId'));
        action.setParams({
			taskID: component.get('v.recordId')
        });
        
		action.setCallback(this, function(response) {

            var state = response.getState();
            console.log('state: ',state);

            var res = response.getReturnValue();

            if (state === "SUCCESS") {
                console.log('res: ',res);
                component.set("v.taskRelationLst",res);

                var attendeesId = [];
                if(res != '' && res != undefined && res != null){
                    res.forEach(attendee => {
                        attendeesId.push(attendee.Relation.Name)
                    })
                }

                console.log('attendeesId: ',attendeesId);
                component.set("v.attendeeIds",attendeesId);
            }
        });
		$A.enqueueAction(action);
    },

    lkupFunc : function(component, event, helper)
    {
        var srchVal = event.getParam('lkupStr');
        var objAPIName = event.getParam('objAPIName');
        
        console.log('srchVal: ',srchVal);
        console.log('objAPIName: ',objAPIName);

        var recordData = {};
        
        var recordId = component.get("v.recordId"); 

        recordData.isTalentUser = true;
        recordData.recordId = recordId;

        console.log('recordData: ',JSON.stringify(recordData));

        // Need to add isTalent Param to setParams in other component where lkupFunc method used
        var action = component.get("c.typeAheadFuncLtng");
        action.setParams({
            "rName":srchVal,
            "sObjName":objAPIName,
            "filter":"",
            "recordData":JSON.stringify(recordData)
            
        });
        
        action.setCallback(this,function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS")
            {
                var res = response.getReturnValue();
                
                var tempRes = JSON.parse(JSON.stringify(res));
                
                event.getSource().set('v.srchList',tempRes);
            }
            
            
            else if(state === "INCOMPLETE")
            {
                this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },

    updateTaskComment : function(component, event, helper) {
        
        // var action = component.get("c.updateTaskComment");
        // action.setParams({
        //     objTaskComment: component.get('v.taskCommentRecord'),
        //     objTask: component.get('v.taskRecord')
        // });
        
		// action.setCallback(this, function(response) {
        //     component.set('v.showSpinner', false); 
        //     if (response.getState() === "SUCCESS") {
                
                $A.get("e.force:closeQuickAction").fire();
        //         $A.get('e.force:refreshView').fire();
        //         if(component.get("v.showRichText"))
        //             window.location.reload();
        //     }
        // });
        // component.set('v.showSpinner', true); 
		// $A.enqueueAction(action); 
    },
    
    updateTaskCommentWithPlainText : function(component,helper) {
        
        var action = component.get("c.fetchTaskPlainTextComment");
        action.setParams({
			taskID: component.get('v.recordId')
        });
        
        action.setCallback(this, function(response) {
            if(response.getReturnValue() != null) {
                component.set('v.taskRecord', response.getReturnValue());
            } 
        });
		$A.enqueueAction(action); 
    }
})