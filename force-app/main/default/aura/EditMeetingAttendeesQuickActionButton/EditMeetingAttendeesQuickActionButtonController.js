({
    onInit: function (component, event, helper) {
        helper.loadTaskRecord(component, helper);
    },

    handleBubbling : function(component, event, helper) {
        console.log('handleBubbling: ');
		helper.lkupFunc(component, event, helper);
    },

    clearRow : function(component, event, helper){
        console.log('clear RowId ',event.currentTarget.getAttribute("data-UserId")); 
        console.log('clear Row',event.currentTarget.getAttribute("data-UserName"));

        var taskRelationId = event.currentTarget.getAttribute("data-UserId");
        
        var removedAttendees = component.get("v.clearAttendees");
        removedAttendees.push(taskRelationId);
        component.set("v.clearAttendees", removedAttendees);

        console.log('clearAttendees: ',JSON.parse(JSON.stringify(component.get("v.clearAttendees"))));

        var taskRelationLst = component.get("v.taskRelationLst");
        var finalTaskRelations = [];

        console.log('taskRelationLst: ',JSON.parse(JSON.stringify(taskRelationLst)));

        //Removing the closed pill
        if (taskRelationLst.length != 0 && taskRelationId) {
            for (var i in taskRelationLst) {
                console.log('i: ',i);
                console.log('i Id: ',taskRelationLst[i].Id);

                if (
                    taskRelationLst[i].Id &&
                    taskRelationLst[i].Id != taskRelationId
                ) {
                    finalTaskRelations.push(taskRelationLst[i]);
                }
            }
        }

        console.log('finalTaskRelations: ',JSON.parse(JSON.stringify(finalTaskRelations)));
        
        component.set("v.taskRelationLst", finalTaskRelations);

    },
    
    closeEditTaskCommentModal: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    },

    saveTaskAttendees: function (component, event, helper) {
        
        //To Save MultiContact List
        console.log('ContactList Ids: ',component.find("contactAttendees"));

        var meetingAttendees = component.find("contactAttendees").get("v.selValList");

        console.log('meetingAttendees: ',meetingAttendees);
        var contactAttendeesList= [];
        var contactAttendeesNameList= [];
        
        if(meetingAttendees.length > 0){
            for(var i = 0; i<meetingAttendees.length;i++)
            {
                if(meetingAttendees[i].rId != null){
                    contactAttendeesList.push(meetingAttendees[i].rId);
                    console.log('Contact ' + meetingAttendees[i].rId + ' added to list');
                }
                if(meetingAttendees[i].rName != null){
                    contactAttendeesNameList.push(meetingAttendees[i].rName);
                    console.log('Contact ' + meetingAttendees[i].rName + ' added to list');
                }
                
            }
        }
        console.log('contactAttendeesList: ',contactAttendeesList);
        console.log('contactAttendeesNameList: ',contactAttendeesNameList);

        var attendeeIds = component.get('v.attendeeIds');

        var isExistingContact = false;
        for(var i = 0; i < attendeeIds.length; i++){
            console.log('attendeeIds[i]: ',attendeeIds[i]);
            for(var j = 0; j < contactAttendeesNameList.length; j++){
                console.log('contactAttendeesList[i]: ',contactAttendeesNameList[i]);
                if(attendeeIds[i] == contactAttendeesNameList[j]){
                    console.log('Updating with Duplicate Id >>> ');
                    isExistingContact = true;
                }
            }
        }

        if(!isExistingContact){
            console.log('clearAttendees: ',component.get("v.clearAttendees"));
            var action = component.get("c.clearTaskAttendees");
            console.log('ecordId: ',component.get('v.recordId'));

            action.setParams({
                taskID: component.get('v.recordId'),
                clearedTaskRelationsIds : component.get("v.clearAttendees"),
                meetingAttendeesList: contactAttendeesList
            });
            
            action.setCallback(this, function(response) {

                var state = response.getState();
                console.log('state: ',state);

                if (state === "SUCCESS") {
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
            });
            $A.enqueueAction(action);
        }else{
            alert('Selected Contact already existing in this Task');
        }
        
        
    }
})