({
    getAccounts :function(component, event, helper){
        var action = component.get("c.getTopicsList");        
        var accountIdsString = component.get("v.accountsIdsString");
        helper.startSpinner(component,event,helper);
        
        action.setParams({"accountIds":accountIdsString});
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if(accountIdsString == "Not Selected"){
                component.set("v.canTag","No");
                //helper.showToastMessageFail(component,event,"Please Select Companies!");
                helper.stopSpinner(component,event,helper);
            }
            if (state === "SUCCESS" && accountIdsString !== "Not Selected") {
                //
                var responseString = response.getReturnValue();
                if(responseString){
                    var canApplyTag = responseString['canApplyTag'];
                    var isException = responseString['exception'];
                    if(canApplyTag === "true" && isException === "false"){
                        component.set("v.canTag","Yes");
                        var topics = responseString['lstOfTopics'];
                        var lstOfTopicsOthers = responseString['lstOfTopicsOthers'];
                        var listTopicsRemove = responseString['lstOfTopicsRemove'];
                        component.set("v.topicsOthers",lstOfTopicsOthers);
                        component.set("v.listTopicsRemove",listTopicsRemove);
                        component.set("v.topics", topics);
                        var inputValue = component.get("v.inputValue",true);
                        component.set("v.selectedTopic",inputValue);                        
                        var selectedTopic = component.get("v.selectedTopic",true);
                        var topicsToPush =[];
                        var showTopicLimit = 0;
                        if(topics.length<5){
                            component.set("v.topicsToShow",topics); 
                        }else{
                            for(var i=0;i<5;i++){
                                topicsToPush.push(topics[i]);
                            }
                            component.set("v.topicsToShow",topicsToPush);
                        }
                        component.set("v.showTopics",true);
                    }else if(canApplyTag === "false" && isException === "false"){
                        component.set("v.canTag","No");
                    }else{
                        //Exception Code
                        component.set("v.canTag","No");
                    }
                }else{
                    
                }
            }
            helper.stopSpinner(component,event,helper);
        });
        $A.enqueueAction(action);
    },
    onKeyUp :function (component, event, helper){
        var message  = component.get("v.inputValue",true);
        if(message!= undefined && message.length > 0){
            component.set("v.selectedTopic",message);
            helper.setTopicsToShow(component,event, helper);
            component.set("v.showTopics",true);
        }else{
            component.set("v.selectedTopic","");
            helper.setTopicsToShow(component,event, helper);
            component.set("v.showTopics",true);
        }
    },
    handleError : function(component){
      var inputValue = component.find('inputBox');
        if(inputValue!== undefined){
            inputValue = inputValue.get("v.value");
            if(inputValue.length > 98){
                return false;
            }else{
                return true;
            }
        }
    },    
    onAddTagClick : function (component, event, helper){
        component.set("v.showMessageFail",false);
        component.set("v.showMessageSuccess",false);
        helper.startSpinner(component, event, helper);
        var selectedTopic = component.get("v.selectedTopic",true);
        var topics = component.get("v.topics",true); 
        var isFoundInTopics = false;
        var selectedTopicRecord = {};
        var accountIdsString = component.get("v.accountsIdsString");
        for(var i=0;i<topics.length;i++){
            if(topics[i].Name.toLowerCase() == selectedTopic.toLowerCase() ){
                isFoundInTopics=true;
                selectedTopicRecord = topics[i];
            }
        }
        if(this.handleError(component))
        {
            if(isFoundInTopics){
                var action = component.get("c.addTagToSelectedAccounts");          
                action.setParams({ "accountIds" : accountIdsString,"selectedTopicToTag" : selectedTopicRecord});
                action.setCallback(this, function(response) {
                    var state = response.getState();               
                    console.log("getReturnValue",response.getReturnValue());
                    if (state === "SUCCESS") {
                        var returnMessage = JSON.parse(response.getReturnValue());
                        var isSuccess = returnMessage.isSuccess
                        var countFailCompanies = returnMessage.countFailCompanies;
                        var countSuccessCompanies = returnMessage.countSuccessCompanies;
                        var topicApplied= $A.get("{!$Label.c.IVP_SD_Manage_Tag_Topic_Applied_To_n_Companies}");
                        var topicNotApplied= $A.get("{!$Label.c.IVP_SD_Manage_Tag_Topic_Not_Applied_To_n_Companies}");
                        if(countFailCompanies == 0){
                            helper.showToastMessageSuccess(component,event,topicApplied.replace(new RegExp('{!n}','g'), 'all'));
                        }else if(countSuccessCompanies == 0){
                            helper.showToastMessageFail(component,event, topicNotApplied.replace(new RegExp('{!n}','g'), 'all'));
                        }else{
                            var companyMsgSuccess = topicApplied.replace(new RegExp('{!n}','g'), countSuccessCompanies);
                            /*
                        if(countSuccessCompanies<=1){
                            companyMsgSuccess = "Topic applied successfully on "+countSuccessCompanies+  " company.";
                        }else{
                            companyMsgSuccess = "Topic applied successfully on "+countSuccessCompanies+  " companies.";
                        }
                        */
                        var companyMsgFail	  = topicNotApplied.replace(new RegExp('{!n}','g'), countFailCompanies);
                        /*
                        if(countFailCompanies<=1){
                            companyMsgFail = "Topic did not applied on "+countFailCompanies + " company.";
                        }else{
                            companyMsgFail = "Topic did not applied on "+countFailCompanies + " companies.";
                        }
                        */
                        helper.showToastMessageSuccess(component,event,companyMsgSuccess);
                        helper.showToastMessageFail(component,event,companyMsgFail);
                        //helper.showToastMessage(component,event,selectedTopicRecord.Name +" is successfully  tagged to "+ companyMsgSuccess +" but failed for "+companyMsgFail);
                    }
                }
                var isRemoveTagSetup = component.get("v.isRemoveTagSetup",true);
                if(isRemoveTagSetup){
                    helper.getRemoveTags(component,event,helper);
                }
                helper.initiateComponent(component,event,helper);
                helper.stopSpinner(component,event,helper);
            });
                $A.enqueueAction(action);  
            }else{
                var action = component.get("c.insertNewTopic");
                if(selectedTopic.trim().length<99){
                    action.setParams({ newTopicName : selectedTopic.trim() ,accountIds : accountIdsString});
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            var returnMessage = JSON.parse(response.getReturnValue());
                            var isSuccess = returnMessage.isSuccess
                            var countFailCompanies = returnMessage.countFailCompanies;
                            var countSuccessCompanies = returnMessage.countSuccessCompanies;
                            var topicApplied= $A.get("{!$Label.c.IVP_SD_Manage_Tag_Topic_Applied_To_n_Companies}");
                            var topicNotApplied= $A.get("{!$Label.c.IVP_SD_Manage_Tag_Topic_Not_Applied_To_n_Companies}");
                            if(countFailCompanies==0){
                                helper.showToastMessageSuccess(component,event,topicApplied.replace(new RegExp('{!n}','g'), 'all'));
                            }else if(countSuccessCompanies==0){
                                helper.showToastMessageFail(component,event, topicNotApplied.replace(new RegExp('{!n}','g'), 'all'));
                            }else{
                                var companyMsgSuccess = topicApplied.replace(new RegExp('{!n}','g'), countSuccessCompanies);
                                var companyMsgFail	  = topicNotApplied.replace(new RegExp('{!n}','g'), countFailCompanies);
                                
                                /*if(countSuccessCompanies<=1){
                                companyMsgSuccess = "Topic applied successfully on "+countSuccessCompanies+  " company.";
                            }else{
                                companyMsgSuccess = "Topic applied successfully on "+countSuccessCompanies+  " companies.";
                            }
                            if(countFailCompanies<=1){
                                companyMsgFail = "Topic did not applied on "+countFailCompanies + " company.";
                            }else{
                                companyMsgFail = "Topic did not applied on "+countFailCompanies + " companies.";
                            }*/
                            helper.showToastMessageSuccess(component,event,companyMsgSuccess);
                            helper.showToastMessageFail(component,event,companyMsgFail);
                            //helper.showToastMessage(component,event,selectedTopic +" is successfully  tagged to "+ companyMsgSuccess +" but failed for "+companyMsgFail);
                        }
                    }
                    var isRemoveTagSetup = component.get("v.isRemoveTagSetup",true);
                    if(isRemoveTagSetup){
                        helper.getRemoveTags(component,event,helper);
                    }
                    helper.initiateComponent(component,event,helper);
                    helper.stopSpinner(component,event,helper);
                });
                $A.enqueueAction(action);
            }
        }
        }else{
            helper.stopSpinner(component,event,helper);
            helper.showToastMessageFail(component,event,$A.get("$Label.c.IVP_MANAGE_TOPIC_Topic_length_Error_Msg"));
        }
    },
    itemSelected : function(component, event, helper) {
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex"); 
        
        component.set("v.selectedTopic",SelIndex.Name);
        if(SelIndex){
            var topicsToShow = component.get("v.topicsToShow");
            for(var j=0;j<topicsToShow.length;j++){
                if(topicsToShow[j].Id == SelIndex){
                    var selItem = topicsToShow[j];
                    component.set("v.selectedTopic",selItem.Name);
                    component.set("v.selItem",selItem);
                    component.set("v.showTopics",false);
                    component.set("v.inputValue",selItem.Name);
                }
            }
        }
    },    
    getIndexFrmParent : function(target,helper,attributeToFind){
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    startSpinner : function(component,event,helper){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner,"slds-hide");
        $A.util.addClass(spinner,"slds-show");
    },    
    stopSpinner : function(component,event,helper){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner,"slds-show");
        $A.util.addClass(spinner,"slds-hide");
    },
    setTopicsToShow : function(component,event,helper){
        var showTopicLimit = component.get("v.showTopicLimit",true);
        var selectedTopic = component.get("v.selectedTopic",true);
        var topics = component.get("v.topics",true);
        
        var topicsToPush =[];
        if(selectedTopic !== undefined && selectedTopic.trim().length>0){
            var showTopicLimit = component.get("v.showTopicLimit",true);
            selectedTopic = selectedTopic.toLowerCase();
            for(var i=0;i<topics.length;i++){
                if(topics[i].Name.toLowerCase().includes(selectedTopic)){
                    topicsToPush.push(topics[i]);
                }
            }
            component.set("v.topicsToShow",topicsToPush);  
        }else{
            component.set("v.topicsToShow",topics);
        }
    },
    removeTag : function(component,event,helper){
        var values = component.get('v.selectedTags');
        component.set("v.showMessageFail",false);
        component.set("v.showMessageSuccess",false);
        helper.startSpinner(component,event,helper);
        var selectedTagsObjList = [];
        for(var index = 0, length = values.length; index < length; index++){
            selectedTagsObjList.push(helper.parseLinkValue(values[index]));
        }
        if(selectedTagsObjList){
            var accountIdsString = component.get("v.accountsIdsString");
            var selectedTagsObjString = JSON.stringify(selectedTagsObjList);  
            var action = component.get("c.unTagAccounts");
            action.setParams({ selectedTags : selectedTagsObjString,accountIds : accountIdsString });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var returnMessage = JSON.parse(response.getReturnValue());
                    var isSuccess = returnMessage.isSuccess
                    var countFailCompanies = returnMessage.countFailCompanies;
                    var countSuccessCompanies = returnMessage.countSuccessCompanies;
                    var topicRemoved= $A.get("{!$Label.c.IVP_SD_Manage_Tag_Topic_Removed_To_n_Companies}");
                    var topicNotRemoved= $A.get("{!$Label.c.IVP_SD_Manage_Tag_Topic_Not_Removed_To_n_Companies}");
                    
                    if(countFailCompanies==0){
                        helper.showToastMessageSuccess(component,event, topicRemoved.replace(new RegExp('{!n}','g'), 'all'));
                    }else if(countSuccessCompanies==0){
                        helper.showToastMessageFail(component,event,  topicNotRemoved.replace(new RegExp('{!n}','g'), 'all'));
                    }else{
                        var companyMsgSuccess = topicRemoved.replace(new RegExp('{!n}','g'), countSuccessCompanies);
                        var companyMsgFail	  = topicNotRemoved.replace(new RegExp('{!n}','g'), countFailCompanies);
                        
                        /*if(countSuccessCompanies<=1){
                            companyMsgSuccess = "Topics removed successfully from "+countSuccessCompanies+  " company.";
                        }else{
                            companyMsgSuccess = "Topics removed successfully from "+countSuccessCompanies+  " companies.";
                        }
                        if(countFailCompanies<=1){
                            companyMsgFail = "Topics did not removed from "+countFailCompanies + " company.";
                        }else{
                            companyMsgFail = "Topics did not removed from "+countFailCompanies + " companies.";
                        }*/
                        helper.showToastMessageSuccess(component,event,companyMsgSuccess);
                        helper.showToastMessageFail(component,event,companyMsgFail)
                        //helper.showToastMessage(component,event,"Untagged Successfully for  "+companyMsgSuccess + " but failed for "+companyMsgFail);
                       
                    }
                    helper.getRemoveTags(component,event,helper);
                }
                //Clear DropDown
                
                var selectedEle = $('.dropdown-chose-list .del');
                if(selectedEle){
                    selectedEle.click();
                }
                var clearEle = $('.dropdown-clear-all');
                if(clearEle){
                    if(clearEle.css){
                        clearEle.css({'display':'none'});
                    }
                    if(clearEle.style){
                        clearEle.style.display='none';
                    }
                }
				component.set("v.selectedTags","");
                helper.getRemoveTags(component,event,helper);
                helper.initiateComponent(component,event,helper);
                helper.stopSpinner(component,event,helper);
                
            });
            $A.enqueueAction(action);
        }
    },    
    showToastMessageSuccess : function(component,event,message){
        component.set("v.messageSuccess",message);
        component.set("v.showMessageSuccess",true);
    },
    showToastMessageFail : function(component,event,message){
        component.set("v.messageFail",message);
        component.set("v.showMessageFail",true);
    },
    
    initiateComponent : function(component,event,helper){
        component.set("v.inputValue","");
        component.set("v.selectedTopic","");
        component.set("v.showTopics",true);
        var accountIdsString = component.get("v.accountsIdsString");
        if(accountIdsString != "Not Selected"){
            helper.getAccounts(component,event,helper);
            helper.startSpinner(component,event,helper);
            helper.getRemoveTags(component,event,helper);
            helper.stopSpinner(component,event,helper);
        }
    },
    gotoConsole : function (component, event, helper) {
        var spinner = component.find("mySpinnerMain");
         $A.util.addClass(spinner,"slds-show");
        $A.util.removeClass(spinner,"slds-hide");       
        helper.initiateComponent(component,event,helper);        
        var urlEvent = $A.get("e.force:navigateToURL");
        $A.util.removeClass(spinner,"slds-show");
        $A.util.addClass(spinner,"slds-hide");
        
        urlEvent.fire();
    },
    getFilteredTopics : function (component,event,helper,message){
        var action = component.get("c.getFilterTopics");
        helper.startSpinner(component,event,helper);
        action.setParams({ "searchString" : message });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseString = response.getReturnValue();
                console.log("filteredTopics",responseString['lstOfTopics']);
                component.set("v.topicsToShow", responseString['lstOfTopics']);
                component.set("v.showTopics", true);
            }
            helper.stopSpinner(component,event,helper);
        });
        $A.enqueueAction(action);        
    },
    onApplyTagClicked :  function(component,event,helper){},
    onRemoveTagClicked : function(component,event,helper){        
        var topics = component.get("v.listTopicsRemove",true);
        
        helper.setupDropDown(component,event,helper,topics);       
    },
    parseLinkValue :function(linkValue){
        var wrapperElement = document.createElement('div');
        wrapperElement.innerHTML = linkValue;
        var element = wrapperElement.childNodes[0];
        var linkMap = {};
        
        var topicName       = element.childNodes[0].nodeValue;
        var topicId         = element.childNodes[1].attributes[1].value;
        linkMap.topicName=topicName;
        linkMap.topicId=topicId;
        
        
        return linkMap;
    },
    getRemoveTags : function (component,event,helper){
        var action = component.get("c.getTopicsListForRemove");
        var accountIdsString = component.get("v.accountsIdsString");
        helper.startSpinner(component,event,helper);
        action.setParams({ "accountIds" : accountIdsString });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseString = response.getReturnValue();
                if(responseString){
                	helper.setupDropDown(component,event,helper,responseString);    
                }
            }
            helper.stopSpinner(component,event,helper);
        });
        $A.enqueueAction(action); 
    },
    setupDropDown : function(component,event,helper,topics){
        var opts = [{ "class": "optionClass", 
                     label: '--None--', 
                     value: ''}];
        component.set("v.selectedTags","");
        
        //var topics = topicsMap['lstOfTopics'];
        var optss = [];
        if(topics ){
            for(var index=0, length=topics.length; index<length; index++){
                var topic = topics[index];
                opts.push({ "class": "optionClass", 
                           label: topic.Name, 
                           value: topic.Id});
                optss.push({id:topic.Id,
                            name :topic.Name})
            }
        }
        $('.inpTagsPanal').dropdown({
            // the maximum number of options allowed to be selected
            limitCount: Infinity,
            // search field
            input: '<input type="text" maxLength="20" placeholder="Search">',
            // dynamic data here
            data: optss,
            // is search able?
            searchable: true,
            destroy:true,
            // when there's no result
            searchNoData: '<li style="color:#ddd">No topic founds!</li>',
            // callback
            choice: function(){
                console.log('Nothing Selected-1');
                if($(this)[0].name.length >= 1){
                    console.log($(this));
                    console.log($(this)[0]);                    
                    var dropdownDisplay = $('.dropdown-display');
                    component.set('v.selectedTags',$(this)[0].name);
                }else{
                    component.set("v.selectedTags","");
                    $('#sourceFilterSelectContainer').find('.dropdown-clear-all').css({'display':'none'});
                } 
            }
        });
    }
    
})