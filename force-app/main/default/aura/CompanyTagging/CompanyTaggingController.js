({
    doInit : function(component, event, helper) {
        helper.getAccounts(component, event, helper);
    },
    itemSelected : function(component, event, helper) {
        helper.itemSelected(component, event, helper);
    },
    filterTopics : function(component, event, helper) {
        helper.filterTopics(component, event, helper);
    },
    onAddTagClick : function(component, event, helper){
        helper.onAddTagClick(component, event, helper);
    },
    hideNotificationMessage: function (component, event, helper){
        
    },
    closeSuccessToast: function (component, event, helper){
        component.set("v.showMessageSuccess",false);
    },
    closeFailToast: function (component, event, helper){
        component.set("v.showMessageFail",false);
    },
    
    removeTag : function(component, event, helper){
        helper.removeTag(component, event, helper);
    },
    goBack :  function(component, event, helper){
        helper.gotoConsole(component, event, helper);
    },
    onKeyUp : function(component, event, helper){
        var isOthersTopicsAdded = component.get("v.isOthersTopicsAdded",true);    
        if(!isOthersTopicsAdded){
            component.set("v.isOthersTopicsAdded",true);
            var topics = component.get("v.topics",true);
            var topicsOthers = component.get("v.topicsOthers",true);
            topics = topics.concat(topicsOthers);
            component.set("v.topics",topics);
        }
        helper.onKeyUp(component, event, helper);
    },
    onFocusForTag : function (component,event, helper){
        var inputValue = component.get("v.inputValue",true);
        component.set("v.selectedTopic",inputValue);
        helper.setTopicsToShow(component,event, helper);  
        component.set("v.showTopics",true);
    },
    eventHandlerFromVF : function(component, event, helper) {
        //helper.getAccounts(component,event,helper);
    },
    onRemoveTagClicked:function(component, event, helper) {
        var isRemoveTagSetup = component.get("v.isRemoveTagSetup",true);
        component.set("v.selectedTab","removeTag");
        debugger;
        if(!isRemoveTagSetup){
            component.set("v.isRemoveTagSetup",true);
            setTimeout(function(){helper.onRemoveTagClicked(component,event,helper)},1000);    
        }else{
            helper.startSpinner(component,event,helper);
            helper.getRemoveTags(component,event,helper);
            helper.stopSpinner(component,event,helper);
        }
    },
	onApplyTagClicked:function(component, event, helper) {
        component.set("v.selectedTab","applyTag");
        setTimeout(function(){component.find("inputBox").getElement().focus();},100);
        helper.onApplyTagClicked(component,event,helper);
    },
})