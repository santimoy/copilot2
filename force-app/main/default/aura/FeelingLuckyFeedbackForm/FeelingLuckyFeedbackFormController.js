({
    doInit: function(component, event, helper) {
      
    },
	toggleModal: function(component, event, helper) {
		var isModalOpen = component.get("v.isModalOpen");
        var feedbackForm = component.find("feedback-form");
        if(!isModalOpen){
            $A.util.removeClass(feedbackForm, "slds-slide-up-saving"); 
        } else {
            $A.util.addClass(feedbackForm, "slds-slide-up-saving");
        }
	},
    handleCancel: function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    handleSubmit: function(component, event, helper) {
        component.set("v.isModalOpen", false);
        component.set("v.isSaveBtnClicked", true);
    },
    checkValidForm: function(component, event, helper) {
        var type = component.get('v.feedbackObj.Why_is_score_wrong__c');
		var feedback = component.get('v.feedbackObj.Comments__c');
		var inputCmp = component.find('formField');
		var commentIndex = -1;
		for(var index=0, length =inputCmp.length ; index<length; index++){
			var comments = inputCmp[index];
			if(comments.get('v.name') === 'comments'){
				commentIndex = index;
			}
		}
		if( index > 0){
	    	if(type === 'Other (Please fill out comments)' || type.toLowerCase().indexOf('other')>=0){
	    		inputCmp[commentIndex].set('v.required', true);
	    	}else{
				inputCmp[commentIndex].set('v.required', false);
	    	}
		}
		
        var validExpense = component.find('formField').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        // If we pass error checking, do some real work
        if(validExpense){
        	component.set("v.isFormValid", false);
        } else {
            component.set("v.isFormValid", true);
        }
    }
})