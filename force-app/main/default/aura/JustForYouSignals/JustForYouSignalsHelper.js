({
    iniiLoadFilter: false,
    handleCalling: function (cmp, method, params, callback, hlpr, callbackInfo, errorCB, isBackGround) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        //console.log('params121--->', params);
        //console.log('methodname-->' + method);
        let action = cmp.get("c." + method);
        if(action === undefined ){
            return;
        }
        // passing parameter while calling
        if (Object.keys(params)) {
            action.setParams(params);
        }

        //To send calling in background
        if (callback.name === "loadFiltersCB" || isBackGround) {
            action.setBackground();
        }
        // Create a callback that is executed after
        // the server-side action returns
        action.setCallback(this, function (response) {
            cmp.set('v.actionCnt', cmp.get('v.actionCnt') - 1);
            let state = response.getState();
            if (state === "SUCCESS") {
                // passing response to a callback method
                if (callback) {
                    callback(cmp, hlpr, response, callbackInfo);
                }
            } else if (state === "INCOMPLETE") {
                hlpr.showMessage({message: 'Please refresh the page!', type: "error"});
            } else if (state === "ERROR") {
                let errors = response.getError();
                hlpr.handleErrors(errors);
                if (errorCB) {
                    errorCB(cmp, hlpr, callbackInfo);
                }
            }
        });
        console.log('outside loop:'+cmp.get('v.actionCnt'));
        //this is actually use to call server side method
        cmp.set('v.actionCnt', cmp.get('v.actionCnt') + 1);
        $A.enqueueAction(action);
    },
    doInit: function (cmp) {
        let self = this;
        let records = [];
        cmp.set('v.records', records);
        
        this.prepareFilters(cmp, {init:cmp.get('v.init')});
    },

    prepareFilters: function (cmp, formData) {
        console.log('A3');
        //console.log('50 formData>>>>: ' + JSON.stringify(formData));
        this.handleCalling(cmp, 'getWDFilters', {}, this.prepareFiltersCB, this, formData);
    },

    prepareFiltersCB: function (cmp, hlpr, response, callbackInfo) {
        let settings = response.getReturnValue();
        //console.log('settings->'+JSON.stringify(settings));
        //console.log('56 callbackInfo->' + JSON.stringify(callbackInfo));
        if (callbackInfo && callbackInfo.refreshFilters) {
            hlpr.iniiLoadFilter = true;
            
        }
        hlpr.populateFilters(cmp, settings);
        hlpr.loadFilters(cmp, callbackInfo);
    },
    populateFilters: function (cmp, filters) {
        let selectedCats = [];
        let resetSelect = false;
        let colorFilters = cmp.get('v.colorFilters');
        if (filters) {
            if (filters.colours && filters.colours.length > 0) {
                filters.colours.forEach(function (colour) {
                    if (colorFilters[colour] != undefined) {
                        colorFilters[colour] = true;
                    }
                })
                cmp.set("v.colorFilters", colorFilters);
                cmp.set('v.filterOn', true);
            }
            if (filters.myDashSignalPreferences && filters.myDashSignalPreferences.length > 0) {
                filters.myDashSignalPreferences.forEach(function (cat) {
                    selectedCats.push(cat);
                })
                //console.log('selectedCats-->'+JSON.stringify(selectedCats));
                cmp.set("v.selectedCats", selectedCats);
                resetSelect = true;
            }
        }
        if (resetSelect) {
            cmp.set('v.filterOn', true);
            this.resetSelectsWithSelect(cmp);
        }

    },

    loadFilters: function (cmp, callbackInfo) {
        let viewId = cmp.get('v.selectedView');
        //console.log('getInitData');
        console.log({inputJson: JSON.stringify({viewId: viewId, isLimit: true})});
        let init = cmp.get('v.init');
        this.handleCalling(cmp, 'getInitData', {
            inputJson: JSON.stringify({
                viewId: viewId,
                isLimit: true,
                init:init
            })
        }, this.loadFiltersCB, this, callbackInfo);
    },
    loadFiltersCB: function (cmp, hlpr, response, callbackInfo) {
        let wdData = response.getReturnValue();
        //console.log('wdData loadFiltersCB');
        //console.log({wdData});
        let cats = wdData.signals.data;
        let preferencesCategories = wdData.categories;
        if(wdData.signals.error){
            let errorMsg = wdData.signals.error;
            let errorInd = errorMsg.indexOf('ERROR:');
            let jfyInd = errorMsg.indexOf('just_for_you');
            let uPreInd = errorMsg.indexOf('user_preferences');
            var dataExcpInd = errorMsg.indexOf('Hint: ');
            var doesnot = errorMsg.indexOf('does not exist');
            if(errorInd > -1 ){
                if(doesnot>-1){
                    errorInd+=6;
                    doesnot+=15;
                    errorMsg = errorMsg.substr(errorInd,(doesnot>errorInd ?doesnot: errorMsg.length)-errorInd);
                }else if(jfyInd > -1){
                    errorMsg = $A.get("$Label.c.IVP_JFY_Refresh_Tooltip");
                }else if(uPreInd>-1){
                    errorMsg = $A.get("$Label.c.IVP_Preview_Universe_Refresh_Tooltip");
                }else if (dataExcpInd>-1){
                    errorInd+=6;
                    errorMsg = errorMsg.substr(errorInd,(dataExcpInd>errorInd ?dataExcpInd-2: errorMsg.length)-errorInd);
                }
            }
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"info",
                "message":  '[JFY Signals] '+errorMsg
            });
            toastEvent.fire();
        }
        /*For set My Dash Signal Preferences*/
        let filterList = [];
        for (let index = 0, len = preferencesCategories.length; index < len; index++) {
            let cat = preferencesCategories[index].External_Id__c;
            filterList.push(cat);
        }

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }

        let finalUniqueList = filterList.filter(onlyUnique);
        //console.log('finalUniqueList>>>: '+JSON.stringify(finalUniqueList));

        let catOptions = [];
        for (let index = 0, len = finalUniqueList.length; index < len; index++) {
            let cat = finalUniqueList[index];
            //console.log('cat>>>>: '+JSON.stringify(cat[3]));
            catOptions.push({value: cat, text: cat, label: cat, selected: false});
        }
        //console.log('113> catOptions>>>>: ' + JSON.stringify(catOptions));
        cmp.set('v.catOptions', catOptions);
        let myDashPreferences = wdData.filters.myDashSignalPreferences;
        //console.log('myDashPreferences>>>>: '+JSON.stringify(myDashPreferences));
        cmp.set('v.DashSignalPreferencesList', myDashPreferences);
        let initSelected= [];
        myDashPreferences.forEach(function(ele){
            initSelected.push(ele);
        })
        cmp.set('v.initSelected',initSelected);
        let colorselect = cmp.find("cat-select");
        if (callbackInfo && callbackInfo.refreshFilters) {
            colorselect.set('v.options', []);
            colorselect.set('v.myDashSignalPreferencesList', []);
            colorselect.initWithSelected();
        }
        //console.log('151> catOptions>>>>: '+JSON.stringify(catOptions));
        if (catOptions.length > 0 && colorselect) {
            colorselect.set('v.options', catOptions);
            if (myDashPreferences.length > 0) {
                colorselect.set('v.myDashSignalPreferencesList', myDashPreferences);
                colorselect.initWithSelected();
            } else {
                colorselect.initWithSelected();
            }

        } else {
            colorselect.set('v.options', catOptions);
            colorselect.set('v.myDashSignalPreferencesList', myDashPreferences);
            colorselect.reInit();
        }
        if (catOptions.length == 0) {
            cmp.set('v.filterOn', true);
        }
        hlpr.initCallback(cmp, hlpr, response, callbackInfo);
    },

    loadDefault: function (cmp) {
        let selectedViewId = cmp.get('v.selectedView');
        let colorObject = cmp.get('v.colorObject');
        let colorFilters = cmp.get('v.colorFilters');

        let selCatsOps = cmp.get('v.selectedCats');
        let selectedClrs = [];
        let selectedCats = [];
        let filterOn = false;

        for (let key in colorFilters) {
            if (colorFilters[key] != undefined && colorFilters[key]) {
                selectedClrs.push(key);
                filterOn = true;
            }
        }
        if (selCatsOps && selCatsOps.length > 0) {
            selCatsOps.forEach(function (ele) {
                selectedCats.push(ele);
            });
            filterOn = true;
        }
        //console.log('selectedCats' + selectedCats);
        cmp.set('v.filterOn', filterOn);

        let isInit = cmp.get('v.init');
        let params = {
            viewId: selectedViewId,
            selClrsOps: selectedClrs,
            selCatsOps: selectedCats,
            init: isInit,
            isLimit: false
        };
        if (selectedViewId === '') {
            this.handleCalling(cmp, 'getInitData', {inputJson: JSON.stringify(params)}, this.initCallback, this, 'initCategory');
        } else {
            this.loadCompanySignal(cmp, params);
        }
    },
    loadCompanySignal: function (cmp, params) {
        let selectedViewId = cmp.get('v.selectedView');
        let selCatsOps = cmp.get('v.selectedCats');
        let colorObject = cmp.get('v.colorObject');
        let colorFilters = cmp.get('v.colorFilters');
        //console.log('colorFilters>>>: ' + JSON.stringify(colorFilters));

        let selectedClrs = [];
        let selectedCats = [];
        let filterOn = false;

        for (let key in colorFilters) {
            if (colorFilters[key] != undefined && colorFilters[key]) {
                selectedClrs.push(key);
                filterOn = true;
            }
        }

        if (selCatsOps && selCatsOps.length > 0) {
            filterOn = true;
        }
        let isInit = cmp.get('v.init');
        if (selectedViewId) {
            params.viewId = selectedViewId;
        }
        //console.log('218 params>>>: ' + JSON.stringify(params));
        //console.log('238 selectedClrs>>>: ' + JSON.stringify(selectedClrs));
        params.selClrsOps = selectedClrs;
        params.selCatsOps = selCatsOps;
        params.init = isInit;
        cmp.set('v.compCount', 0);
        if (params.viewId && params.viewId != '') {
            cmp.set('v.selectedView', params.viewId);
        }
        let callbackInfo = {};
        //console.log('params.refreshFilters>>>>: '+JSON.stringify(params.refreshFilters));
        if (params.refreshFilters) {
            callbackInfo.refreshFilters = params.refreshFilters;
        }
        //console.log('230 Params>>>>: ' + JSON.stringify(params));
        this.handleCalling(cmp, 'getCompanySignals', {inputJson: JSON.stringify(params)}, this.initCallback, this, callbackInfo);
        cmp.set('v.init',false);
    },
    initCallback: function (cmp, hlpr, response, callbackInfo) {
        try {
            let wdData = response.getReturnValue();
            //console.log('245');
            //console.log({wdData});
            let colorObject = cmp.get('v.colorObject');
            /*let apiResponse = wdData.signals.apiResponse.data_fields;
            let data_fields = [];
            if(apiResponse) {
                for (let i = 0; i < apiResponse.length; i++) {
                    let apiResEle = apiResponse[i].split(".");
                    if(apiResEle[apiResEle.length-1] != null) {
                        data_fields.push(apiResEle[apiResEle.length-1]);
                    }
                }
                console.log('data_fields');
                console.log(data_fields);
            }*/

            let newCompanySignals = wdData.signals.data;
            let preferencesList = wdData.categories;
            let ssselCatsOps = cmp.get('v.selectedCats');
            let companySignals = [];
            if (ssselCatsOps.length === 0) {
                cmp.set('v.DashSignalPreferencesList', []);
                companySignals = newCompanySignals;
            } else {
                if (!$A.util.isUndefinedOrNull(wdData.signals.errorInfo)) {
                    hlpr.showMessage({message: wdData.signals.errorInfo, type: "info"});
                } else if ($A.util.isUndefinedOrNull(wdData.signals.errorMessage)) {
                    if(!$A.util.isUndefinedOrNull(newCompanySignals) && !$A.util.isEmpty(newCompanySignals) 
                       && !$A.util.isUndefinedOrNull(ssselCatsOps) && !$A.util.isEmpty(ssselCatsOps)) {
                        newCompanySignals.forEach(function (ele) {
                            ssselCatsOps.forEach(function (pre) {
                                if (pre.trim() === ele[3].trim()) {
                                    companySignals.push(ele);
                                }
                            })
                        })
                    }
                } else {
                    let errorMessage = $A.get("$Label.c.IVP_JustForYou_Response_ERROR");
                    hlpr.showMessage({message: errorMessage, type: "error"});
                }

            }

            let colorFilters = cmp.get('v.colorFilters');
            let selectedClrs = [];
            for (let key in colorFilters) {
                if (colorFilters[key] != undefined && colorFilters[key]) {
                    selectedClrs.push(key);
                }
            }
            //console.log('selectedClrs>>>>>: ' + JSON.stringify(selectedClrs));

            //console.log('273');
            //console.log({companySignals});
            let records = [];
            cmp.set('v.records', records);
            let today = new Date();
            if (!$A.util.isUndefinedOrNull(companySignals) && !$A.util.isEmpty(companySignals)) {
                //console.log('292>>>: 
                for (let index = 0, length = companySignals.length; index < length; index++) {
                    let data = {};
                    let companySignalsObj = companySignals[index];
                    data.companySignal = companySignalsObj;
                    data.preferenceId = companySignalsObj[0];
                    if (!$A.util.isUndefinedOrNull(companySignalsObj[1]) && !$A.util.isEmpty(companySignalsObj[1])) {
                        data.preferencesf_Id = companySignalsObj[1];
                    } else {
                        data.preferencesf_Id = null;
                    }
                    data.preferenceName = companySignalsObj[3];
                    data.preferenceSnippets = companySignalsObj[5];
                    data.signalName = companySignalsObj[6];
                    data.preferenceUrl = companySignalsObj[7];
                    data.preferenceDf_id = companySignalsObj[8];
                    data.preferenceWebsite = companySignalsObj[9];
                    data.preferenceCt_id = companySignalsObj[10];
                    data.ivp_name = companySignalsObj[11];
                    data.actions = [];
                    data.groupIcon = 'utility:info_alt';
                    if (preferencesList) {
                        for (let i = 0; i < preferencesList.length; i++) {
                            const preferencesObj = preferencesList[i];
                            if (data.preferenceName === preferencesObj.External_Id__c) {
                                if(preferencesObj.Color_HEX__c){
                                    data.Color_HEX__c = colorObject[preferencesObj.Color_HEX__c.toLowerCase()];
                                    data.Home_Color_HEX__c = preferencesObj.Color_HEX__c.toLowerCase();
                                }
                                data.groupIcon = preferencesObj.Image_URL__c;
                            }
                        }
                    } else {
                        data.Color_HEX__c = colorObject['default'];
                        data.Home_Color_HEX__c = colorObject['default'];
                    }
                    data.hoverHeadline = companySignalsObj[4];
                    data.days = '';
                    if (companySignalsObj[2]) {
                        if (companySignalsObj[2])
                            data.days = new timeago().format(companySignalsObj[2]);

                        let indexOf = data.days.indexOf('hours ago');
                        if (indexOf > 0) {
                            data.days = 'today';
                            let hours = today.getHours();
                            let dateHour = data.days.substring(0, indexOf - 1);
                            if (dateHour < hours) {
                                data.days = 'yesterday';
                            }
                        } else if (data.days == '1 day ago') {
                            data.days = 'yesterday';
                        }
                    }
                    data.hasActivity = false;
                    data.ltaskId = '';
                    data.etaskId = '';
                    data.hasltask = false;
                    data.hasetask = false;
                    /*console.log('selectedClrs>>>>: ');
                    console.log({selectedClrs});
                    console.log('data.Color_HEX__c>>>>: ' + data.Home_Color_HEX__c);
                    console.log(selectedClrs.indexOf(data.Home_Color_HEX__c));*/
                    if(selectedClrs.length > 0) {
                        if (selectedClrs.includes(data.Home_Color_HEX__c)) {
                            records.push(data);
                        }
                    } else {
                        records.push(data);
                    }
                }
            }
            cmp.set('v.records', records);

            /*if (callbackInfo.refreshFilters) {
                let cats = wdData.signals.data;
                let filterList = [];
                if (!$A.util.isUndefinedOrNull(cats) && !$A.util.isEmpty(cats)) {
                    for (let index = 0, len = cats.length; index < len; index++) {
                        let cat = cats[index];
                        filterList.push(cat[3]);
                    }
                }

                function onlyUnique(value, index, self) {
                    return self.indexOf(value) === index;
                }

                let finalUniqueList = filterList.filter(onlyUnique);

                let catOptions = new Map();
                for (let index = 0, len = finalUniqueList.length; index < len; index++) {
                    let cat = finalUniqueList[index];
                    catOptions.set(cat, {value: cat, text: cat, label: cat});
                }
                cmp.set('v.catOptions', Array.from(catOptions.values()));

                let catselect = cmp.find("cat-select");
                if (catOptions.size > 0 && catselect) {
                    catselect.set('v.options', Array.from(catOptions.values()));
                    catselect.initWithSelected();
                }
                if (hlpr.iniiLoadFilter) {
                    hlpr.loadFilters(cmp, callbackInfo);
                    hlpr.iniiLoadFilter = false;
                }
            }

            hlpr.checkTask(cmp, {init: true}, hlpr);*/
        } catch (e) {
            console.log('ERROR' + e);
            console.log(e);
        }

    },
    handleRedirection: function (recordId) {
        let navEvt = $A.get("e.force:navigateToSObject");
        if (navEvt) {
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        } else {
            window.open('/' + recordId);
        }
    },

    daysBetween: function (start, end) {
        let one_day = 1000 * 60 * 60 * 24;
        let start_dateOnly = new Date(start.getFullYear(), start.getMonth(), start.getDate());
        let end_dateOnly = new Date(end.getFullYear(), end.getMonth(), end.getDate());

        // Convert both dates to milliseconds
        let start_ms = start_dateOnly.getTime();
        let end_ms = end_dateOnly.getTime();

        // Calculate the difference in milliseconds
        let difference_ms = (end_ms > start_ms ? end_ms - start_ms : start_ms - end_ms);

        // Convert back to days and return
        let d = Math.round(difference_ms / one_day);
        return d;
    },
    prepareCompanySignalAction: function (cmp, label, recId) {
        let data = {};
        let records = cmp.get('v.records');
        for (let index = 0, length = records.length; index < length; index++) {
            let companySignal = records[index].companySignal;
            let signal = records[index].signal;
            let company = records[index].company;
            if (recId === companySignal.Id) {
                data.companyName = company.Name;
                data.companyId = companySignal.Company__c;
                data.signalCategoryId = companySignal.Category__c;
                data.signalCategoryName = signal.Name
                data.companySignalHeadline = companySignal.Headline__c;
                data.companySignalExtId = companySignal.External_ID__c;
                data.companySignalId = companySignal.Id;
                data.companySignalName = companySignal.Name;
                data.companySignalUrl = companySignal.URL__c;
                data.companyNextActDate = company.Next_Action_Date__c;
                data.companyNextAct = company.Next_Action__c;

            }
        }
        data.source = '';
        data.action = label;
        this.enableDocked(cmp, data);
        ;
    },
    disableDocked: function (cmp) {
        let data = {hideLog: true};
        let appEvent = $A.get("e.c:DashDockedAppEvent");
        if (appEvent) {
            appEvent.setParams({
                "dockedData": data
            });
            appEvent.fire();
        } else {
            //alert('WatchDog: Please Contact to Administrator!');
        }
    },
    enableDocked: function (cmp, data) {
        if (data.action === 'Log Activity') {
            data.type = 'Call';
            data.subject = 'Logging Call';
        } else if (data.action === 'Email') {
            data.type = 'Email';
            data.subject = 'Logging Email';
        } else if (data.action === 'Examine Activity') {
            data.type = 'Other';
            data.subject = 'Examine - ' + data.signalCategoryName;
            data.comments = data.companySignalHeadline;
            if (data.companySignalUrl) {
                data.comments += '\n\n' + data.companySignalUrl;
            }
        }
        let appEvent = $A.get("e.c:DashDockedAppEvent");
        if (appEvent) {
            appEvent.setParams({
                "dockedData": data
            });
            appEvent.fire();
        } else {
            //alert('WatchDog: Please Contact to Administrator!');
        }
    },
    companySignalCallback: function (cmp, hlpr, response, callbackInfo) {
        let state = response.getState();
        callbackInfo.recId = response.getReturnValue();
        hlpr.enableDocked(callbackInfo);
    },
    checkTask: function (cmp, data, hlpr) {
        let init = false;
        let recid = '';
        let type = '';
        let signalAction = '';
        if (data) {
            if (data.init) {
                init = data.init;
            }
            if (data.companySignalId) {
                recid = data.companySignalId;
            }
            if (data.type) {
                type = data.type;
            }
            if (data.signalAction) {
                signalAction = data.signalAction;
            }
        }

        let records = cmp.get('v.records');
        let accountIds = [];
        let noactivityCompanies = [];
        for (let index = 0, length = records.length; index < length; index++) {
            if (records[index].companySignal && records[index].companySignal.Company__c &&
                 accountIds.indexOf(records[index].companySignal.Company__c) < 0) {
                accountIds.push(records[index].companySignal.Company__c);
            }
        }

        if (!init) {
            for (let index = 0, length = records.length; index < length; index++) {

                if (recid === records[index].companySignal.Id) {

                    if (!records[index].hasActivity) {
                        records[index].hasActivity = true;
                    }

                    if (records[index].hasActivity) {
                        if (signalAction == 'Log Activity') {
                            if (data.taskId) {
                                records[index].ltaskId = data.taskId;
                                records[index].hasltask = true;
                            }
                        }
                        if (signalAction == 'Examine Activity') {
                            if (data.taskId) {
                                records[index].etaskId = data.taskId;
                                records[index].hasetask = true;
                            }
                        }

                    }
                }

                if (records[index].hasActivity) {
                    let eleIndex = accountIds.indexOf(records[index].companySignal.Company__c);
                    if (eleIndex > -1) {
                        accountIds.splice(eleIndex, 1);
                    }
                }
            }
        }
        if (!init) {
            cmp.set('v.records', records);
        } else {
            if (init && accountIds && accountIds.length >0 ) {
                let params = {accountIds: JSON.stringify(accountIds)};
                this.handleCalling(cmp, 'getTasks', params, hlpr.checkTaskCallback, hlpr, params);
                return;
            }
        }
        cmp.set('v.compCount', accountIds.length);
    },
    checkTaskCallback: function (cmp, hlpr, response, callbackInfo) {
        let taskMap = response.getReturnValue();
        let records = cmp.get('v.records');
        let allAccountIds = [];
        let eventCompanies = [];

        for (let index = 0, length = records.length; index < length; index++) {
            if (records[index].companySignal && records[index].companySignal.Company__c &&
                allAccountIds.indexOf(records[index].companySignal.Company__c) < 0) {
                allAccountIds.push(records[index].companySignal.Company__c);
            }
        }
        for (let index = 0, length = records.length; index < length; index++) {
            let record = records[index];
            let companySignalId = record.companySignal.Id;
            if (taskMap[companySignalId]) {
                let tasks = taskMap[companySignalId];
                for (let tIndex = 0, tLength = tasks.length; tIndex < tLength; tIndex++) {
                    let task = tasks[tIndex];

                    if (eventCompanies.indexOf(task.WhatId) < 0) {
                        eventCompanies.push(task.WhatId)
                        let eleIndex = allAccountIds.indexOf(task.WhatId);
                        if (eleIndex > -1) {
                            allAccountIds.splice(eleIndex, 1);
                        }
                    }
                    records[index].hasActivity = true;
                    if (task.Signal_Action__c === 'Log Activity') {
                        record.ltaskId = task.Id;
                        records[index].hasltask = true;
                    }
                    if (task.Signal_Action__c === 'Examine Activity') {
                        record.etaskId = task.Id;
                        record.hasetask = true;
                    }
                    record.hasTask = true;
                }
            }
        }
        cmp.set('v.compCount', allAccountIds.length);
        cmp.set('v.records', records);
        hlpr.handleCalling(cmp, 'getKeyContacts', callbackInfo, hlpr.keyContactsCB, hlpr, {});
    },
    keyContactsCB: function (cmp, hlpr, response, callbackInfo) {
        let accKeyConMap = response.getReturnValue();
        let records = cmp.get('v.records');
        let accIds = [];
        let eventCompanies = [];
        let conIds = [];
        let cadNames = []

        for (let accId in accKeyConMap) {
            if (accIds.indexOf(accId) < 0) {
                accIds.push(accId);
            }
            conIds = conIds.concat(accKeyConMap[accId]);
        }
        if (accIds.length == 0) {
            return;
        }
        records.forEach(function (record) {
            // record. signal there then need to check salesloft
            if (record.signal && record.signal.Salesloft_Cadence__c) {
                let lowerCadName = record.signal.Salesloft_Cadence__c.toLowerCase();
                let eDripInd = -1;
                if (record.actions) {
                    for (let aInd = record.actions.length - 1; aInd > 0; aInd--) {
                        if (record.actions[aInd].name == 'Email Drip') {
                            eDripInd = aInd;
                            break;
                        }
                    }
                }

                if (accIds.indexOf(record.company.Id) >= 0 && eDripInd < 0) {

                    let actionTitle = $A.get("$Label.c.IVP_WD_Action_Email_Drip");
                    if (actionTitle === '$Label.c.IVP_WD_Action_Email_Drip does not exist.') {
                        actionTitle = 'Email Drip';
                    }
                    if (cadNames.indexOf(lowerCadName) < 0) {
                        cadNames.push(lowerCadName);
                    }
                    record.actions.push({name: 'Email Drip', title: actionTitle, icon: 'adduser'});
                }
            }
        });
        cmp.set('v.records', records);
        if (cadNames.length > 0 && conIds.length > 0) {
            //re-utilizing wrapper so using paramName
            let params = {selClrsOps: conIds, selCatsOps: cadNames};
            hlpr.handleCalling(cmp, 'getCadenceMemberDetails', {inputJson: JSON.stringify(params)}, hlpr.cadenceMemberCB, hlpr, null, true);
        }
    },

    cadenceMemberCB: function (cmp, hlpr, response, callbackInfo) {

        let cadMembers = response.getReturnValue();
        let dataMap = {};
        let cadences = [];
        cadMembers.forEach(function (cadMember) {
            if (cadMember.Cadence__c) {
                if (cadMember.Cadence__r)
                    cadences.push(cadMember.Cadence__r.Name.toLowerCase());
                if (cadMember.Contact__c && cadMember.Contact__r) {
                    if (cadMember.Contact__r.AccountId) {
                        let cadNames = [];
                        if (dataMap[cadMember.Contact__r.AccountId]) {
                            cadNames = dataMap[cadMember.Contact__r.AccountId];
                        }
                        if (cadNames.indexOf(cadMember.Cadence__r.Name.toLowerCase()) < 0) {
                            cadNames.push(cadMember.Cadence__r.Name.toLowerCase())
                        }
                        dataMap[cadMember.Contact__r.AccountId] = cadNames;
                    }
                }
            }
        });
        let records = cmp.get('v.records');
        records.forEach(function (record) {
            let cadName = '';
            if (record.signal.Salesloft_Cadence__c && record.signal.Salesloft_Cadence__c) {
                cadName = record.signal.Salesloft_Cadence__c.toLowerCase();
            }
            if (dataMap[record.company.Id] || cadences.indexOf(cadName) < 0) {
                let remeoveEmailDrip = false;
                let emailDripIndex = -1;
                record.actions.forEach(function (action) {
                    emailDripIndex++;
                    if (action.name === 'Email Drip') {
                        let cadenceNames = dataMap[record.company.Id];
                        if (cadences.indexOf(cadName) < 0 ||
                            (cadenceNames && cadenceNames.indexOf(record.signal.Salesloft_Cadence__c.toLowerCase()) > -1)) {
                            record.signal.Salesloft_Cadence__c = '';
                            remeoveEmailDrip = true;
                            action.title = $A.get('$Label.c.IVP_WD_No_Email_Drip');
                            // remeoveEmailDrip = cadences.indexOf(cadName) < 0;// if need to show disabled button when cadencemember found
                        }
                    }
                })
                if (remeoveEmailDrip && emailDripIndex > -1) {
                    record.actions.splice(emailDripIndex, 1);
                }
            }
        });
        cmp.set('v.records', records);
    },
    handleErrors: function (errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage: function (message) {
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if (message.title) {
            toastParams.title = message.title;
        }
        if (message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function (toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if (toastEvent) {
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert(toastParams.title + ': ' + toastParams.message);
        }
    },
    groupWithIconMap: function () {
        return {
            'Financial': 'custom:custom17', 'Growth': 'standard:investment_account', 'Recognition': 'standard:reward',
            'People': 'standard:groups', 'Events & Marketing': 'standard:event', 'Corporate Update': 'standard:case',
            'Negative News': 'custom:custom82', 'Uncategorized News': 'standard:news'
        };
    },
    extractLabelVal: function (label, defaultVal) {
        let labVal = $A.get(label);
        if (labVal == label + " does not exist.") {
            labVal = defaultVal;
        }
        return labVal;
    },
    applyFilter: function (cmp) {
        let selClrsOps = []

        let colorObject = cmp.get('v.colorObject');
        let colorFilters = cmp.get('v.colorFilters');
        //console.log('colorFilters>>>>: ' + colorFilters);

        for (let key in colorFilters) {
            if (colorFilters[key] != undefined && colorFilters[key]) {
                selClrsOps.push(key);
            }
        }
        let init = cmp.get('v.init');
        this.loadCompanySignal(cmp, {init:init,viewId: '', modifyMetaData: false});
        this.maintainFilters(cmp, selClrsOps, []);
    },
    maintainFilters: function (cmp, colours, cats) {
        let params = {selClrsOps: colours, selCatsOps: cats, modifyMetaData: true};
        this.handleCalling(cmp, 'setIVPSDSettings', {inputJson: JSON.stringify(params)}, this.maintainFiltersCB, this, null);

    },

    maintainFiltersCB: function (cmp, hlpr, response, callbackInfo) {
    },
    resetFilter: function (cmp, event, helper) {
        let colorselect = cmp.find("cat-select");
        // populating selected values and re initializing with default. 
        let initSelected = cmp.get('v.initSelected');
        let selectedCats=[];
        initSelected.forEach(function(ele){
            selectedCats.push(ele);
        })
        colorselect.set('v.myDashSignalPreferencesList', selectedCats);
        colorselect.initWithSelected();
        helper.loadCompanySignal(cmp, {init:false,viewId: '', modifyMetaData: false});
    },
    resetSelects: function (cmp) {
        let colorSelect = cmp.find("cat-select");
        if (colorSelect) {
            //console.log('793 options>>>>: '+JSON.stringify(cmp.get('v.options')));
            colorSelect.initWithSelected();
        }
    },
    resetSelectsWithSelect: function (cmp) {
        let colorselect = cmp.find("cat-select");
        if (colorselect) {
            colorselect.initWithSelected();
        }
    },

    handleEmailDrip: function (cmp, csignalId) {
        let records = cmp.get('v.records');
        for (let rInd = records.length - 1; rInd > -1; rInd--) {
            let record = records[rInd];
            if (record.companySignal.Id === csignalId) {
                let param = {
                    accId: record.companySignal.Company__c,
                    salesloftCadence: record.signal.Salesloft_Cadence__c
                }
                record.signal.Salesloft_Cadence__c = ''
                record.actions.forEach(function (action) {
                    if (action.name == 'Email Drip') {
                        action.icon = 'spinner';
                    }
                });
                cmp.set('v.records', records)
                this.handleCalling(cmp, 'processKeyContact',
                    {inputJson: JSON.stringify(param)},
                    this.processKeyContactCB,
                    this,
                    {signalId: record.signal.Id, cSignalId: record.companySignal.Id},
                    this.processKeyContactErrorCB);
                break;
            }
        }
    },
    processKeyContactCB: function (cmp, hlpr, response, callbackInfo) {
        let msg = $A.get('{!$Label.c.IVP_WD_Cadence_Update}');
        hlpr.showMessage({title: 'Success', message: msg, type: 'success'});
        hlpr.revertCadenceIcon(cmp, callbackInfo.cSignalId, false);
    },
    processKeyContactErrorCB: function (cmp, hlpr, callbackInfo) {
        if (callbackInfo) {
            hlpr.revertCadenceIcon(cmp, callbackInfo.cSignalId, true);
        }
    },
    revertCadenceIcon: function (cmp, csignalId, isError) {
        if (csignalId) {
            let records = cmp.get('v.records');
            records.forEach(function (record) {
                if (record.companySignal.Id === csignalId) {
                    record.actions.forEach(function (action) {
                        if (action.name === 'Email Drip') {
                            record.signal.Salesloft_Cadence__c = '';
                            action.icon = 'adduser';
                            // if(isError){
                            action.title = $A.get('$Label.c.IVP_WD_No_Email_Drip');
                            // }
                        }
                    })
                }
            })
            cmp.set('v.records', records);
        }
    },
    initDefault: function (component, event, helper, preferenceId) {
        
        let colorFilters = {'red': false, 'green': false, 'yellow': false};
        let colorObject = {
            'red': $A.get('$Label.c.IVP_WD_Color_HEX_Red'),
            'green': $A.get('$Label.c.IVP_WD_Color_HEX_Green'),
            'yellow': $A.get('$Label.c.IVP_WD_Color_HEX_Yellow'),
            'default': $A.get('$Label.c.IVP_WD_Color_HEX_Default')
        };
        component.set('v.colorObject', colorObject);
        component.set('v.colorFilters', colorFilters);

        component.set('v.selectedView', '');
        //component.set('v.actionCnt', 0);
        component.set('v.init', true);
        helper.handleIVPLoadSMWDAppEvent(component, event, helper, preferenceId);
    },

    handleIVPLoadSMWDAppEvent: function (component, event, helper, preferenceId) {
        helper.disableDocked(component);
        let formData = {};
        formData.label = '';
        formData.viewId = '';
        formData.viewName = '';
        formData.source = '';
        formData.refreshFilters = true;
        formData.preferenceId = preferenceId;
        formData.init = component.get('v.init');
        component.set('v.selectedView', formData.viewId);
        helper.handleListViewChange(component, formData);
        
    },

    handleFocus: function (id) {
        let ele = document.getElementById(id);
        if (ele) {
            ele.focus();
        }
    },
    handleListViewChange: function (cmp, formData) {
        console.log('A2');
        console.log('SPC Just for you Signals... ' + formData);
        this.prepareFilters(cmp, formData);
        //console.log('formData>>>: '+JSON.stringify(formData));
    },
    jsDeselectAll:function(cmp, evt, hlpr){
        let colorselect = cmp.find("cat-select");
        colorselect.set('v.myDashSignalPreferencesList', []);
        colorselect.initWithSelected();
        cmp.set('v.records',[]);
        colorselect.setFocus();
    },
    jsSelectAll:function(cmp, evt, hlpr){
        let selectCats = [];
        let colorselect = cmp.find("cat-select");
        let options = colorselect.get('v.options');
        let selectedCats = cmp.get('v.selectedCats');
        if(selectedCats.length===options.length){
            return;
        }
        options.forEach(function(ele){
            selectCats.push(ele.value);
        })
        
        colorselect.set('v.myDashSignalPreferencesList', selectCats);
        colorselect.initWithSelected();
        hlpr.loadCompanySignal(cmp, {init:false,viewId: '', modifyMetaData: false});
    }
})