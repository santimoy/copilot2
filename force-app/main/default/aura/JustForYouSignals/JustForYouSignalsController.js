({
    doInit : function(component, event, helper) {
        let workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) {
            component.set("v.isConsoleApp",response);
        })
            .catch(function(error) {
                console.log(error);
            });
        helper.initDefault(component, event, helper, null);
    },
    handleAction : function(component, event, helper) {
        let currentTarget = event.currentTarget;
        let dataSet = currentTarget.dataset;
        let src = event.getSource();
        let title = src.get("v.title");
        let name = src.get("v.name");
        let csignalid = src.get("v.value");
        let action = src.get("v.alternativeText");

        if(title === 'Email Drip' || name==='Email Drip'){
            helper.handleEmailDrip(component, csignalid);
        }else if(title !== 'Open Activity'){
            helper.prepareCompanySignalAction(component, title, csignalid);
        }else {
            let records = component.get('v.records');
            let taskId = '';
            for(let index = 0,length=records.length; index<length; index++){
                let record = records[index];
                let companySignal = record.companySignal;
                if(csignalid === companySignal.Id){
                    if(action === 'Examine Activity'){
                        taskId = record.etaskId;
                    }else{
                        taskId = record.ltaskId;
                    }
                    csignalid = companySignal.Id;
                    break;
                }
            }
            let result = {hideLog : true, refresh : false};
            let dashDockedAppEvent = $A.get("e.c:DashDockedAppEvent");
            dashDockedAppEvent.setParams({
                "dockedData" : result
            });
            dashDockedAppEvent.fire();

            helper.handleRedirection(taskId);
        }

    },
    gotoObject : function(component, event, helper) {

        let dataSet = event.currentTarget.dataset;
        if(dataSet.source === 'record'){
            window.open('/lightning/r/'+dataSet.link+'/view');
        }else{
            let url = dataSet.link;
            if(url.indexOf('http') < 0 ){
                url='http://'+url;
            }
            window.open(url);
        }
        
        /*let dataSet = event.currentTarget.dataset;
        if(dataSet.source === 'record'){
            if(component.get("v.isConsoleApp")){
                helper.handleRedirection(dataSet.recordId);
            }else{
                window.open('/lightning/r/'+dataSet.recordId+'/view');
            }
        }else if(dataSet.link){
            let url = dataSet.link;
            if(url.indexOf('http') < 0 ){
                url='http://'+url;
            }
            window.open(url);
        }*/
    },
   
    handleDashDockedAppEvent: function(component, event, helper){
        let dockedData = event.getParam('dockedData');
        dockedData.init = false;
        if(dockedData){
            if(dockedData.refresh){
                helper.checkTask(component, dockedData, helper);
                return;
            }
        }
    },
    /*handleIVPLoadSMWDAppEvent : function(component, event, helper) {
    	helper.disableDocked(component);
        //let formData = event.getParam('formData');
        let formData = '{"class":"optionClass","label":"My Watch Companies","value":"00B0W000006kcRtUAI","devValue":"My_Watch_Companies","selected":true}';
	    console.log('11 formData>>>: '+JSON.stringify(formData));
        if(formData ){
        	console.log('formData>>>: '+JSON.stringify(formData));
            if(formData.source === 'ListView'){
                if(formData.viewId){
                    component.set('v.selectedView', formData.viewId);
                }
                formData.refreshFilters = true;
                helper.handleListViewChange(component,formData);
            }
        }
    },*/
    handleSelectClrChangeEvent: function(component, event, helper) {
        helper.loadDefault(component);
    },
    handleSelectCatChangeEvent: function(component, event, helper) {
        let callOnOut = component.get('v.callOnOut');
        if(callOnOut){
            helper.applyFilter(component);
        }
    },
    applyFilter: function(component, event, helper) {
        helper.applyFilter(component);
    },
    resetFilter: function(component, event, helper) {
        helper.resetFilter(component, event , helper);
    },
    handleColorChange: function(component, event, helper) {
        let target = event.currentTarget;
        let val = target.value;
        let colorFilters = component.get('v.colorFilters');
        if(colorFilters[val] != undefined){
            colorFilters[val] = !colorFilters[val];
        }
        component.set('v.colorFilters',colorFilters);

        helper.applyFilter(component);
    },
    handleHidePanel: function(component, event, helper) {
        if(component.get('v.hidePanel')){
            helper.disableDocked(component);
        }
    },
    handleFocus: function(component, event, helper){
        if(component.get('v.isFocus')){
            component.find('signalPanel').getElement().scrollTop=0;
            helper.handleFocus('wdIcon');
            let records = component.get('v.records');
            if(records && records.length > 0 ){
                if(records[0].preferenceId){
                    let firstIcon = records[0].preferenceId+'_icon';
                    helper.handleFocus(firstIcon);
                }
            }
        }
        component.set('v.isFocus',false);
    },
    handleFocusOnClick: function(component, event, helper){
        let ele = event.currentTarget;
        if(ele && ele.dataset){
            helper.handleFocus(ele.dataset.icon);
        }
    },

    setPreferenceApi : function (c, e, h) {
        let preferenceRecordId = e.getParam("preferenceId");
        if(!$A.util.isUndefinedOrNull(preferenceRecordId) && !$A.util.isEmpty(preferenceRecordId)) {
            c.set('v.preferenceId', preferenceRecordId);
            h.initDefault(c, e, h, preferenceRecordId);
        } else {
            c.set('v.preferenceId', null);
        }
    },
    handleDeselectAll:function(cmp, evt, hlpr){
        hlpr.jsDeselectAll(cmp, evt, hlpr);
    },
    handleSelectAll:function(cmp, evt, hlpr){
        hlpr.jsSelectAll(cmp, evt, hlpr);
    }
})