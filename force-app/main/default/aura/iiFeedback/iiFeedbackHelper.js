({
    getCurUserId:function(cmp){
        let curUserId = cmp.get('v.userId');
        if(curUserId){
            return curUserId;
        }
        curUserId = $A.get("$SObjectType.CurrentUser.Id");
        cmp.set('v.userId',curUserId);
        return curUserId;
    },
    getCurUserEmail:function(cmp){
        let curUserEmail = cmp.get('v.userEmail');
        if(curUserEmail){
            return curUserEmail;
        }
        curUserEmail = $A.get("$SObjectType.CurrentUser.Email");
        cmp.set('v.userEmail',curUserEmail);
        return curUserEmail;
    },
    handleCalling : function(cmp, hlpr, method, params, callback, errorCB) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        let action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                if(callback){
                    callback(cmp, hlpr, response);
                }
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({message:'Please refresh the page!', type:"error"});
            }else if (state === "ERROR") {
                let errors = response.getError();
                hlpr.handleErrors(errors);
                if(errorCB){
                    errorCB(cmp, hlpr);
                }
            }
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    
    jsInit: function(cmp, hlpr) {
        let iiFeedback = cmp.get('v.iiFeedback');
        let key = Object.keys(iiFeedback);
        if(key && key.length > 0){
            hlpr.jsPrepareFeedbacks(cmp, hlpr);
        }
    },
    jsPrepareFeedbacks: function(cmp, hlpr) {
        let iconToShow = [];
        
        let actionMap = cmp.get('v.actionMap');
        
        let iconMap = cmp.get('v.iconMap');
        let iiFeedback = cmp.get('v.iiFeedback');
        let defaultres='{ ';
        for( let key in iiFeedback){
            let fBack = iiFeedback[key]
            let opacity = fBack.opacity!==undefined && fBack.opacity!==null ? fBack.opacity :100;
            let value = fBack.value;
            if(value ===null ||value === undefined ){
                if(key === "ResponsiveNess"){
                    value='null';
                }else{
                    value = 0;
                }
            }
            if(iconMap[key] && iconMap[key][value]){
                defaultres+=key+':'+value+', ';
                let icon=iconMap[key][value];
                //|| key==='PreferenceCenter' 
                let isAction = key==='InsightMatch' || key==='ThumbsUp' || key==='ThumbsDown';
                let isProgress = false;
                if(actionMap[key]!=undefined){
                    isProgress = actionMap[key];
                }
                let iconObj ={name:key, icon:icon,isAction:isAction,value:value,
                              isProgress:isProgress,
                              opacity:icon.Apply_Opacity__c === false || icon.Apply_Opacity__c === "false"? 100 : opacity};
                iconObj.hasLeftBar = key==='ThumbsUp';
                if(key==='ThumbsUp'){
                    if(value !== 0 ){
                        value = - value;
                    }
                    iconObj.value=value;
                    iconToShow.push(iconObj);
                    if(iconMap['ThumbsDown'] && iconMap['ThumbsDown'][value]){
                        let tIcon = iconMap['ThumbsDown'][value];
                        if(actionMap['ThumbsDown']!=undefined){
                            isProgress = actionMap['ThumbsDown'];
                        }
                        iconToShow.push({name:'ThumbsDown',isProgress:isProgress,
                                         opacity:tIcon.Apply_Opacity__c === false || tIcon.Apply_Opacity__c === "false"? 100 : opacity,
                                         isAction:isAction,
                                         icon:tIcon,value:value});
                        iiFeedback['ThumbsDown']=value;
                    }
                }else if(key!=='ThumbsDown'){
                    iconToShow.push(iconObj);
                }
            }
        }
        iconToShow.sort(function(a,b){
            return a.icon.Order__c - b.icon.Order__c;
        })
        defaultres = defaultres.substr(0,defaultres.lastIndexOf(','))+ ' }';
        cmp.set('v.defaultres', defaultres);
        cmp.set('v.iconToShow', iconToShow);
        cmp.set('v.actionMap',actionMap);
    },
    jsAction: function(cmp, evt, hlpr) {
        let action =evt.currentTarget.dataset.name;
        let iiFeedback = cmp.get('v.iiFeedback');
        let iconMap = cmp.get('v.iconMap');
        if(action==='ThumbsUp' || action==='ThumbsDown' || action==='InsightMatch'
           || action==='PreferenceCenter'){
            let key = action ==='ThumbsDown' ? 'ThumbsUp' : action;
            let val = iiFeedback[key];
            val = val.value ===null ||val.value === undefined? 0 : val.value;
            
            var newVal = iconMap[action][val].Toggle_Value__c !== undefined 
            ? iconMap[action][val].Toggle_Value__c
            : val === 1 ? 0 : 1;
            let data={key:key, 
                      action:action,
                      userId:hlpr.getCurUserId(cmp),
                      userEmail: hlpr.getCurUserEmail(cmp),
                      tool:cmp.get('v.tool'),
                      orgId:cmp.get('v.orgId'), 
                      reason:null,
                      newVal:newVal,
                      oldVal:val
                     };
            hlpr.showLoading(cmp, data.action);
            let actionMap = cmp.get('v.actionMap');
            actionMap[data.action] = true;
            cmp.set('v.actionMap',actionMap)
            hlpr.doAPICall(cmp, hlpr, data);
        }
    },
    showLoading:function(cmp, key){
        let iconToShow = cmp.get('v.iconToShow');
        iconToShow.forEach(function(icon){
            if(icon.name === key){
                icon.isProgress = !icon.isProgress;
            }
        })
        cmp.set('v.iconToShow',iconToShow);
    },
    doAPICall: function(cmp, hlpr, data) {
        var req = new XMLHttpRequest();
        var params={
            "org_id":cmp.get('v.orgId'),
            "user_id":hlpr.getCurUserId(cmp),
            "id_app_name":cmp.get('v.app_name'),
            "tool_name":cmp.get('v.tool')
        }
        var url = cmp.get('v.DWH_URL');
        var dwhKey = cmp.get('v.DWH_KEY');
        var erroMsg = $A.get('$Label.c.DWH_Feedback_Error_Message');
        if($A.util.isUndefinedOrNull(url) || $A.util.isEmpty(url) 
           || $A.util.isUndefinedOrNull(dwhKey) || $A.util.isEmpty(dwhKey) ){
            
            hlpr.displayMessage('info', 'Data Ware house details missing!');
            return;
        }
        url+='/feedback/sf/';
        let recId = cmp.get('v.recId');
        data.recId = recId;
        let recData ={
            "value": Number.parseInt(data.newVal),
            "reason": null
        };
        if(data.key === 'ThumbsUp'){
            url+='thumbs?';            
            params["thumbs"]={};
            params["thumbs"][recId]=recData;
        }else if(data.key === 'InsightMatch'){
            url+='insight_match?';
            params["match"]={};
            params["match"][recId]=recData;
        }
        req.open('POST', url);
        req.setRequestHeader ('Authorization',dwhKey);
        req.timeout = 30000;
        let logData={url:url, method:'POST', body:params}
        req.onreadystatechange = function() {
            cmp.set('v.isLoading', false);
            let actionMap = cmp.get('v.actionMap');
            actionMap[data.action] = false;
            cmp.set('v.actionMap', actionMap);
            if(req.readyState === 4 ) {
                if(req.status===200){
                    var res = JSON.parse(req.responseText);
                    hlpr.processResponse(cmp, hlpr, res, data);
                }else if(req.status === 404 ){
                    var res={'isError':true, 'dwhNoFound':true};
                    hlpr.processResponse(cmp, hlpr, res, data);
                    var toastEvent = $A.get("e.force:showToast");
                    if(toastEvent){
                        toastEvent.setParams({
                            "type":"info",
                            "message": erroMsg
                        });
                        toastEvent.fire();
                    }else{
                        alert(erroMsg);
                    }
                }else{
                    logData['responseText']=req.responseText;
                    logData['status']=req.status;
                    var res={'isError':true};
                    hlpr.processResponse(cmp, hlpr, res, data);
                    hlpr.jsLogAJAX(cmp, hlpr, logData);
                }
            }else if(req.readyState === 0){
                var res={'isError':true};
                hlpr.processResponse(cmp, hlpr, res, data);
                alert('Server not found!');
            }
        }
        try{
            cmp.set('v.isLoading', true);
            req.send(JSON.stringify(params));
        }catch(error){
            cmp.set('v.isLoading', false);
            console.log(error);
        }
    },
    processResponse:function(cmp, hlpr, res, data){
        if(res['isError'] ===true || res['dwhNoFound']===true){
            hlpr.showLoading(cmp, data.action);
        }else if(res[data.recId] !== undefined){
            let iiFeedback = cmp.get('v.iiFeedback');
            iiFeedback[data.key].value = data.newVal;
            cmp.set('v.iiFeedback', iiFeedback);
        }else if(res['error']){
            console.log('-', res);
        }
    },
    jsLogAJAX:function(cmp, hlpr, data){
        hlpr.handleCalling(cmp, hlpr, 'logAjax',{dataJSON:JSON.stringify(data)}, hlpr.jsLogAJAXCB);
    },
    jsLogAJAXCB:function(cmp, hlpr, response){
        hlpr.displayMessage('info', response.getReturnValue());
    },
    displayMessage : function(type,msg){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams({
                "type":type,
                "message":msg,
            });
            toastEvent.fire();
        }else{
            alert(msg);
        }
    },
})