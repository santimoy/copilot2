({
	handleInit : function(cmp, evt, hlpr) {
        hlpr.jsInit(cmp, hlpr);
    },
    handleFeedbackChange : function(cmp, evt, hlpr) {
		hlpr.jsPrepareFeedbacks(cmp, hlpr);
	},
    handleAction: function(cmp, evt, hlpr) {
        hlpr.jsAction(cmp, evt, hlpr);
    }
})