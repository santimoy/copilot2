({
    doInit : function(component,event,helper) {
        var action = component.get("c.fetchInitData");
        action.setParams({
            recordId:component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=== 'SUCCESS'){
                var res = response.getReturnValue();
                console.log('Result',res);
                component.set("v.isRecordOwner",res);
            }
            else if(state=== 'INCOMPLETE'){
                this.showMessage({title:'Validation',message:'Cannot Load!',type:'Error'})
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                this.handleErrors(errors);
            }

        });
        $A.enqueueAction(action);

    },
    isNull:function(value){
        return value==='' ||value===undefined || value===null ? true:false; 
    },
    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
        "title": "ERROR!",
        type : 'error',
        "message": "Please fill all the Required Fields!"
    });
    toastEvent.fire();
    }
})