({
    doInit : function(component, event, helper) {
        //var findButton = component.find("initButton").get("v.id");
        //console.log('findButton',findButton);
        helper.doInit(component,event,helper);
        window.addEventListener("keydown",function(event){
            if(event.key==='Escape'){
                component.set("v.showModal",false);
            }
        })
        var action = component.get("c.isEscalated");
        action.setParams({
            recordID:component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=== 'SUCCESS'){
                var res = response.getReturnValue();
                if(res){
                    component.set("v.buttonLabel",'De-Escalate')
                }
                else{
                    component.set("v.buttonLabel",'Escalate');
                }
                
                console.log('ResultButtonLabel',component.get("v.buttonLabel"));
                //component.set("v.isRecordOwner",res);
            }
            else if(state=== 'INCOMPLETE'){
                this.showMessage({title:'Validation',message:'Cannot Load!',type:'Error'})
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);

    },
    handleClick:function(component,event,helper){
        var findButton = event.getSource().get("v.name");
        console.log('findButton',findButton);
        if(findButton==='De-Escalate')
        {
            component.set("v.headerLabel",findButton);
            component.set("v.buttonSelect",findButton);
            component.set("v.showEscalateFields",false);
        }
        else{
            component.set("v.headerLabel",findButton);
            component.set("v.buttonSelect",findButton);
            component.set("v.showEscalateFields",true);
        }
        component.set("v.showModal",true);
    },
    closeModal:function(component,event,helper){
        component.set("v.showModal",false);
            component.set("v.EscalateNotes",'');
               component.set("v.selectedReason",'');
               component.set("v.commentValue",'');
    },
    saveEscalateDeEscalate:function(component,event,helper){
        //component.set("v.showModal",false);
        //component.set("v.showModal",false); 
        //component.set("v.showModal",false);
        console.log('>>>>>>>>>>>>BUTTON SELECTED',component.get("v.buttonSelect"));
        console.log('isNULL',helper.isNull(component.get("v.commentValue")));
        console.log('isNULL',helper.isNull(component.get("v.selectedReason")));
        console.log('isNULL',helper.isNull(component.get("v.selectedReason")));
        console.log('ISTRUE',(component.get("v.buttonSelect")=='De-Escalate' && helper.isNull(component.get("v.EscalateNotes"))));

        if(component.get("v.buttonSelect")=='De-Escalate' &&helper.isNull(component.get("v.EscalateNotes"))){
            helper.showToast(component,event,helper);
        }
        else if(component.get("v.buttonSelect")!='De-Escalate' && 
        (helper.isNull(component.get("v.EscalateNotes"))
        || helper.isNull(component.get("v.selectedReason")))){
            helper.showToast(component,event,helper);
        }
        else{
           
            component.set("v.showModal",false);
            console.log('>>>>>>>>>>>SelectedButton',component.get("v.buttonSelect"));
            component.get("v.buttonSelect")==='Escalate'? component.set("v.isEscalated",true):component.set("v.isEscalated",false);
           
           var action  = component.get("c.saveEscalateDeEscalte");
           action.setParams({
            recordID:component.get("v.recordId"),
            isEscalated :component.get("v.isEscalated")
           });
           if(component.get("v.buttonSelect")==='Escalate'){
            component.set("v.buttonLabel",'De-Escalate');
        }
        else{
         component.set("v.buttonLabel",'Escalate');
        }
           action.setCallback(this,function(response){
            var state = response.getState();
            if(state=== 'SUCCESS'){
                var res = response.getReturnValue();
               component.set("v.EscalateNotes",'');
               component.set("v.selectedReason",'');
               component.set("v.commentValue",'');
                //component.set("v.isRecordOwner",res);
            }
            else if(state=== 'INCOMPLETE'){
                this.showMessage({title:'Validation',message:'Cannot Load!',type:'Error'})
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                this.handleErrors(errors);
            }

        });
        $A.enqueueAction(action);
       }
    }
})