({
	myAction : function(cmp, ev, help)
	{
		var srchVal = ev.srcElement.value;
		var objAPIName = cmp.get('v.objAPIName');

		cmp.set('v.srchVal',srchVal);

		var compEvent = cmp.getEvent("lkupTypingEvt");
		compEvent.setParams({
													"lkupStr":srchVal,
													"objAPIName":objAPIName
												});
		compEvent.fire();
	},
	keyDownCheck :function(cmp,ev,help)
	{
		if(cmp.get("v.arrowControl"))
		{
		 	if (ev.keyCode == '38') {
		
        // up arrow

	        	var srchList = cmp.get('v.srchList');
	        	var keyIndex = cmp.get("v.arrowIndex");
	        	if(keyIndex==null)
	        	{
	        		keyIndex = 0;
	        	}
	        	else if(keyIndex!=0)
	        	{
	        		keyIndex--;
	        	}

					cmp.set("v.arrowIndex",keyIndex);
		    }
		    else if (ev.keyCode == '40') {
		    	var srchList = cmp.get('v.srchList');
	        	var keyIndex = cmp.get("v.arrowIndex");
	        	if(keyIndex==null)
	        	{
	        		keyIndex = 0;
	        	}
	        	else if(keyIndex!=srchList.length-1)
        		{
	        		keyIndex= keyIndex+1;
	        	}

					cmp.set("v.arrowIndex",keyIndex);
			//}
		    }
		    else  if (ev.keyCode == '13') 
		    {
		    	var srchList = cmp.get('v.srchList');
        		var keyIndex = cmp.get("v.arrowIndex");
        		var selVal = srchList[keyIndex];
        		if(selVal!=null)
        		{
        			var compEvent = cmp.getEvent("lkupSelEvt");
					compEvent.setParams({
																"selId":selVal.rId
															});
					compEvent.fire();
					if(cmp.get("v.inputLbl")=="Company")
					{
						var changeEvent = $A.get("e.c:evt_CompanyLookupChanged");;
						changeEvent.setParams({
																"selId":selVal.rId,
																"selName":selVal.rName
															});
						changeEvent.fire();
					}
					if(cmp.get("v.multiSelect")==true)
						{
							var lst =cmp.get('v.selValList');
							lst.push(selVal);
							cmp.set('v.selValList',lst);
							cmp.set("v.srchVal",'');
							cmp.set("v.srchList",null);
							document.getElementById(cmp.get('v.lkupElId')).value = '';
						}
						else
						{
							cmp.set('v.focVar',false);
							cmp.set('v.selVal',selVal);
							cmp.set('v.value',selVal.rId);
						}
						cmp.set("v.arrowIndex",0);
        		}
				
		    }
		}
	},
	blurFunc:function(cmp,ev,help)
	{
		cmp.set('v.focVar',false);
		cmp.set('v.arrowIndex',null);
	},

	focusFunc:function(cmp,ev,help)
	{
		cmp.set('v.focVar',true);
	},

	myClick:function(cmp, ev, help)
	{
		var indx = ev.currentTarget.dataset.selindx;
		var srchList = cmp.get('v.srchList');
		var selVal = srchList[indx];

		console.log('lkup srchList',srchList);
		console.log('lkup selVal.Id',selVal.Id);
		console.log('lkup selVal.Name',selVal.Name);

		var compEvent = cmp.getEvent("lkupSelEvt");
		compEvent.setParams({
													"selId":selVal.rId
												});
		compEvent.fire();
		if(cmp.get("v.inputLbl")=="Company")
		{
			var changeEvent = $A.get("e.c:evt_CompanyLookupChanged");;
			changeEvent.setParams({
													"selId":selVal.rId,
													"selName":selVal.rName
												});
			changeEvent.fire();
		}
		cmp.set('v.focVar',false);
		if(cmp.get("v.multiSelect")==true)
		{
			var lst =cmp.get('v.selValList');
			lst.push(selVal);
			cmp.set('v.selValList',lst);
			document.getElementById(cmp.get('v.lkupElId')).value = '';
		}
		else
		{
			cmp.set('v.selVal',selVal);
			cmp.set('v.value',selVal.rId);
		}

		console.log('lkup myClick selValList: ',cmp.get('v.selValList'));
	},

	clearSel:function(cmp, ev, help)
	{
		if(cmp.get("v.multiSelect")==true)
		{
			var removeVal = ev.currentTarget.id;
			var lst = cmp.get('v.selValList');
			for(var i = 0; i<lst.length;i++)
			{
				if(lst[i].rId==removeVal)
				{
					lst.splice(i, 1);
				}
			}
			cmp.set("v.selValList",lst);
		}
		else
		{
			cmp.set('v.selVal',null);
		}

		console.log('lkup clearSel selValList: ',cmp.get('v.selValList'))
	}
})