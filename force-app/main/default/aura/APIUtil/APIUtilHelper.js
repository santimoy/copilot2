({
    doAPICall: function(request, cmp, hlpr, callback, errorCb) {
        //Handle response when complete
        request.onreadystatechange = function(component) {
            if(this.readyState !== 0){
                if(callback){
                    callback(cmp, hlpr, request);
                }else{
                    console.log('API CB not defined');
                }
            }else if(this.readyState === 0){
                if(errorCb){
                    errorCb(cmp, hlpr, request);
                }else{
                    console.log('ERROR CB not defined');
                }
            }
        };
        
    }
})