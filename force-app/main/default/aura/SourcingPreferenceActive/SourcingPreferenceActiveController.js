({
	fetchRec : function(component, event, helper) {
        var recId = component.get("v.recordId");
        helper.fetchRecHelper(component, recId);
    },
     saveRec : function(component, event, helper) {
        var record = component.get("v.sourcingPrefRecord");
        if(record !== undefined && record != null) { 
            if(component.find("active").get("v.checked")){
                record.Status__c = "Active";
            } else {
                record.Status__c = "Inactive";
            }
            record.Sent_to_DWH__c = true;
            helper.saveRecHelper(component, record);
        }
    }
})