({
    scriptsLoaded: function (component, event, helper) {
        try{
            component.set("v.isSpinner", true);
            component.set("v.previewPopChange", false);
            if(component.get("v.setFilters")){
                helper.fetchFiltersList(component, event, helper);
            }else{
                helper.applyFilter(component, event, helper);
            }
            component.set("v.newRec.Advanced_Mode__c", false);
            component.set('v.validationStatusErrorMessage',null);
            var recId = component.get("v.recordId");
            if (recId !== undefined && recId != null) {
                helper.fetchRecord(component, event, helper, recId);
            }
            component.set("v.setFilters", false);
        }catch(e){
            console.log('ERROR'+e);
            console.log(e);
        }
    },
    

	onReset: function (component, event, helper) {
		if (component.get("v.newRec.Advanced_Mode__c")) {
			component.set("v.newRec.SQL_Where_Clause__c", '');
		} else {
			$('#builder').queryBuilder('reset');
			component.set("v.newRec.SQL_Where_Clause__c", '');
		}
	},

	closeModal: function (component, event, helper) {
		var divName = document.getElementById("ruleID").value;
		var output = $('#builder').queryBuilder('getRules');
		if (output !== undefined && output != null) {
			(function loop(rules) {
				rules.forEach(function (ruleOrGroup) {
					if ('rules' in ruleOrGroup) {
						loop(ruleOrGroup.rules);
					} else if (ruleOrGroup.id == 'Template' && ruleOrGroup.value == divName) {
						rules.pop(ruleOrGroup);
					}
				});
			}(output.rules));
			if (output.rules == null || output.rules == '') {
				$('#builder').queryBuilder('reset');
			} else {
				$('#builder').queryBuilder('setRulesFromMongo', output);
			}
		} else {
			$('#builder').queryBuilder('reset');
		}
        $('#builder').queryBuilder('removeFilter', 'Template');
		document.getElementById("TemplateModal").style.display = "none";
	},

	doSomething: function (component, event, helper) {
		var sel = component.find("mySelect").get("v.value");
		;
		component.set("v.newRec.Include_Exclude__c", sel);
	},


	onSave: function (component, event, helper) {

		var newRecord = component.get("v.newRec");
		if (newRecord.IncludeButton === true || newRecord.ExcludeButton === true) {
			// var newRecord = component.get("v.newRec");
			if (newRecord.Name__c != null && newRecord.Name__c.trim() != '' && newRecord.Include_Exclude__c != "") {
				var Jsonstr = helper.convertToLowerCase(component, event, helper); //$('#builder').queryBuilder('getRules');
                if (Jsonstr != undefined && Jsonstr != null){
                    $('#builder').queryBuilder('setRulesFromMongo', Jsonstr);
                }
				var Sql = $('#builder').queryBuilder('getSQL', false);
				if (Jsonstr != null && Sql != null || component.get("v.newRec.Advanced_Mode__c")) {
					if (component.get("v.isClone")) {
						newRecord.Id = null;
					}
					if (component.get("v.newRec.Advanced_Mode__c")) {
						newRecord.JSON_QueryBuilder__c = null;
					} else {
						var SqlString = Sql['sql'];
                        if(SqlString != null) {
                            if(SqlString.includes("\\")){
                                SqlString = SqlString.replace(/\\/g, "'");
                            }
                            if(SqlString.includes('iLIKE') ) {
                                SqlString = SqlString.replace(/iLIKE/g, "LIKE");
                            }
                            if(SqlString.includes('LIKE')) {
                                SqlString = SqlString.replace(/LIKE/g, "iLIKE");
                            }
                        }
						newRecord.JSON_QueryBuilder__c = JSON.stringify(Jsonstr);
						newRecord.SQL_Where_Clause__c = SqlString;
					}
					component.set('v.validationStatusErrorMessage',null);
					var ruleForKeyword = helper.checkForKeyword(component, event, helper);
					if (ruleForKeyword !== undefined && ruleForKeyword != null && ruleForKeyword.length > 0) {
						helper.getKeywordQuery(component, event, helper, ruleForKeyword, newRecord, null);
					} else {
						helper.saveSourcingRecord(component, event, helper, newRecord);
					}
				} else {
					component.find('recName').showHelpMessageIfInvalid();
					var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
						"type": 'error',
						"message": 'Please enter Name'
					});
					toastEvent.fire();
				}
			} else {
				component.find('recName').showHelpMessageIfInvalid();
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					"type": 'error',
					"message": 'Records are Empty'
				});
				toastEvent.fire();
			}
		} else {
			component.find('recName').showHelpMessageIfInvalid();
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
				"type": 'error',
				"message": 'Please Choose Include or Exclude Companies '
			});
			toastEvent.fire();
		}
	},

	onModalSave: function (component, event, helper) {
        var divName = document.getElementById("ruleID").value;
        
        var record = component.get("v.selectedLookUpRecord");
        if(record !== undefined && record !== null && record.JSON_QueryBuilder__c != null){
            var jsonOldRule = JSON.parse(record.JSON_QueryBuilder__c);
            delete jsonOldRule.valid;
            
            var jsonNewRule = $('#builder').queryBuilder('getRules');
            
            var flag = false;
            var output = $('#builder').queryBuilder('getRules');
            if (output !== undefined && output != null) {
                (function loop(rules) {
                    rules.forEach(function (ruleOrGroup) {
                        if (ruleOrGroup.id == 'Template' && ruleOrGroup.value == divName) {
                            if (divName == 'FirstRule') {
                                flag = true;
                            } else {
                                rules.pop(ruleOrGroup);
                                rules.push(jsonOldRule);
                            }
                        } else if ('rules' in ruleOrGroup) {
                            loop(ruleOrGroup.rules);
                        }
                    });
                }(output.rules));
                if (flag == true) {
                    jsonOldRule.valid = true;
                    $('#builder').queryBuilder('setRulesFromMongo', jsonOldRule);
                } else {
                    $('#builder').queryBuilder('setRulesFromMongo', output);
                }
            }
            $('#builder').queryBuilder('removeFilter', 'Template');
            document.getElementById("TemplateModal").style.display = "none";
        }
        
	},

	viewSQL: function (component, event, helper) {
		var rulesAfterCheck = helper.convertToLowerCase(component, event, helper);
		if (rulesAfterCheck !== undefined && rulesAfterCheck != null) {
			$('#builder').queryBuilder('setRulesFromMongo', rulesAfterCheck);

			var ruleForKeyword = helper.checkForKeyword(component, event, helper);
			if (ruleForKeyword !== undefined && ruleForKeyword != null && ruleForKeyword.length > 0) {
				helper.getKeywordQuery(component, event, helper, ruleForKeyword, null, null);
			} else {
				var result = $('#builder').queryBuilder('getSQL', false);
				if (result !== undefined && result != null) {
                    var SqlString = result.sql;
                    if(SqlString != null) {
                        if(SqlString.includes("\\")){
                            SqlString = SqlString.replace(/\\/g, "'");
                        }
                        if(SqlString.includes('iLIKE') ) {
                            SqlString = SqlString.replace(/iLIKE/g, "LIKE");
                        }
                        if(SqlString.includes('LIKE')) {
                            SqlString = SqlString.replace(/LIKE/g, "iLIKE");
                        }
                    }
					component.set("v.sqlString", SqlString);
					component.set("v.openViewSql", true);
				}
			}
		}
	},

	closeSQL: function (component, event, helper) {
		component.set("v.openViewSql", false);
	},

	cancel: function (component, event, helper) {
        component.set("v.isTableView", false);
	},

	hideBuild: function (component, event, helper) {
		if (component.get("v.newRec.Advanced_Mode__c")) {
			helper.handleShowModalFooter(component, event, helper);
		} else {
			component.set("v.newRec.Advanced_Mode__c", true);
			document.getElementById("builder").style.display = "none";

			var rulesAfterCheck = helper.convertToLowerCase(component, event, helper);
			if (rulesAfterCheck !== undefined && rulesAfterCheck != null) {
				$('#builder').queryBuilder('setRulesFromMongo', rulesAfterCheck);

				var result = $('#builder').queryBuilder('getSQL', false);
				if (result !== undefined && result != null) {
					var ruleForKeyword = helper.checkForKeyword(component, event, helper);
					if (ruleForKeyword !== undefined && ruleForKeyword != null && ruleForKeyword.length > 0) {
						helper.getKeywordQuery(component, event, helper, ruleForKeyword, null, true);
					} else {
                        var SqlString = result.sql;
                        if(SqlString != null) {
                            if(SqlString.includes("\\")){
                                SqlString = SqlString.replace(/\\/g, "'");
                            }
                            if(SqlString.includes('iLIKE') ) {
                                SqlString = SqlString.replace(/iLIKE/g, "LIKE");
                            }
                            if(SqlString.includes('LIKE')) {
                                SqlString = SqlString.replace(/LIKE/g, "iLIKE");
                            }
                        }
						component.set("v.newRec.SQL_Where_Clause__c", SqlString);
					}
				} else {
					component.set("v.newRec.SQL_Where_Clause__c", '');
				}
			} else {
				component.set("v.newRec.SQL_Where_Clause__c", '');
			}
		}
	},

	handleConfrimationEvent: function (component, event, helper) {
		var checkConfirmation = event.getParam("sqlEditor");
		if (checkConfirmation !== undefined && checkConfirmation != null) {
			if (checkConfirmation) {
				component.set("v.newRec.Advanced_Mode__c", false);
				document.getElementById("builder").style.display = "block";
			} else {
				component.set("v.newRec.Advanced_Mode__c", true);
				component.find("advanceCheck").set("v.checked", true);
			}
		}
	},

	onChangeIncludeRadioButton: function (c, e, h) {
		var newRecObj = c.get('v.newRec');
		if (!$A.util.isUndefinedOrNull(newRecObj) && !$A.util.isEmpty(newRecObj)) {
			if (newRecObj.IncludeButton === true) {
				c.set("v.newRec.IncludeButton", true);
				c.set("v.newRec.ExcludeButton", false);
			} else {
				c.set("v.newRec.IncludeButton", false);
				c.set("v.newRec.ExcludeButton", false);
			}
		}
	},

	onChangeExcludeRadioButton: function (c, e, h) {
		var newRecObj = c.get('v.newRec');
		if (!$A.util.isUndefinedOrNull(newRecObj) && !$A.util.isEmpty(newRecObj)) {
			if (newRecObj.ExcludeButton === true) {
				c.set("v.newRec.IncludeButton", false);
				c.set("v.newRec.ExcludeButton", true);
			} else {
				c.set("v.newRec.IncludeButton", false);
				c.set("v.newRec.ExcludeButton", false);
			}
		}
	},

})