({
    herokuTableName:'',
    getHerokuTableName: function(){
        if(this.herokuTableName){
	        return this.herokuTableName;
	    }else{
            var herokuTable = $A.get("$Label.c.IVP_Heroku_Data_Table_Name");
            this.herokuTableName = herokuTable;
            return herokuTable;
	    }
    },
    dwhViewName :'',
	getDWHViewName: function(){
	    if(this.dwhViewName){
	        return this.dwhViewName;
	    }else{
            var viewName = $A.get("$Label.c.IVP_DWH_Query_View_Name_To_Replace");
            this.dwhViewName = viewName;
            return viewName;
	    }
    },
    curUserId :'',
	currentUserId: function(){
	    if(this.curUserId){
	        return this.curUserId;
	    }else{
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            this.curUserId = userId;
            return userId;
	    }
    },
    currentUserFilter:'',
    getCurrentUserFilter: function(){
	    if(this.currentUserFilter){
	        return this.currentUserFilter;
	    }else{
            let curFilter= ' AND (user_sf_id = \''+this.currentUserId()+'\' or user_sf_id is null)';
            this.currentUserFilter = curFilter;
            return curFilter;
	    }
    },
    fetchFiltersList: function(component, event, helper) {
        try {
            component.set("v.isSpinner", true);
            var action1 = component.get("c.fetchFilters");
            action1.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var rec = response.getReturnValue();
                    if (rec !== undefined && rec != null) {
                        if (rec.picklistBody !== undefined && rec.picklistBody != null) {
                            if (rec.fetchPicklistErr) {
                                component.set('v.validationStatusErrorMessage', rec.picklistBody);
                            } else {
                                component.set("v.picklistBody", rec.picklistBody);
                            }
                        }
                        if (rec.fieldListObj !== undefined && rec.fieldListObj != null) {
                            let dwhViewName = helper.getDWHViewName();
                            rec.fieldListObj.forEach(function(iiField){
                                iiField.DB_Column__c = iiField.DB_Column__c.replace(iiField.DB_Table__c, 'ct.'+dwhViewName);
                            });
                            component.set("v.intFieldRecList", rec.fieldListObj);
                            helper.applyFilter(component, event, helper);
                        }
                    }
                } else if (response.state === "ERROR") {
                    this.toastMsg(component, event, 'Error message=' + JSON.stringify(response.error), 'error');
                }
            });
            $A.enqueueAction(action1);
        } catch (e) {
            console.log('ERROR' + e);
            console.log(e);
        }
    },

    applyFilter: function(component, event, helper) {
        component.set("v.isSpinner", true);
        //console.clear();
        console.log('typeof ', typeof $('#builder').queryBuilder);
        var funType = typeof $('#builder').queryBuilder;
        if (funType !== 'function' || funType == 'undefined') {
            component.set("v.isSpinner", false);
            this.toastMsg(component, event, 'Please refresh the page, try again!', 'info');
            return;
        }
        try {
            var body = component.get("v.picklistBody");
            var list = this.createFilters(component, component.get("v.picklistBody"));
            var self = this;
            if (list !== undefined && list != null && list.length > 0) {
                $('#builder').queryBuilder({
                    plugins: [
                        'sql-support'
                    ],
                    filters: list
                });
                $('#builder').queryBuilder('addFilter', {
                    id: 'ct.'+ self.getDWHViewName()+'.ivp_investor_list',
                    label: 'Investors',
                    type: 'string',
                    input: 'select',
                    multiple: false,
                    plugin: 'selectize',
                    plugin_config: {
                        valueField: 'picklist',
                        labelField: 'picklist',
                        searchField: 'picklist',
                        sortField: 'picklist',
                        options: [],
                        onInitialize: function() {
                            var that = this;
                            that.on("blur", function(e) {
                                that.clearOptions();
                            });
                        },
                        valueSetter: function(rule, value) {
                            rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                        },
                        load: function(query, callback) {
                            var that = this;
                            // that.clearOptions();
                            if (query && query.length > 1) {
                                //component.set("v.isSpinner", true);
                                var selectId = document.getElementById("selectID").value;
                                if(selectId !== undefined && selectId != null){
                                    var spinnerElement = document.getElementById(selectId);
                                    spinnerElement.setAttribute("style", "display: block;");
                                }
                                helper.searchRecords(component, query, that)
                                    .then(response => {
                                        if (response !== undefined && response != null && response.length > 0) {
                                        response.forEach(item => {
                                        that.addOption(item);
                                        //component.set("v.isSpinner", false);
                                        var selectId = document.getElementById("selectID").value;
                                        if(selectId !== undefined && selectId != null){
                                        var spinnerElement = document.getElementById(selectId);
                                        spinnerElement.setAttribute("style", "display: none;");
                                    	}
                                     });
                                        }else{
                                           // component.set("v.isSpinner", false);
                                            var selectId = document.getElementById("selectID").value;
                                            if(selectId !== undefined && selectId != null){
                                                var spinnerElement = document.getElementById(selectId);
                                                spinnerElement.setAttribute("style", "display: none;");
                                            }
                                        }
                                        //that.open();
                                        callback(response);
                                    });
                            } else {
                                that.clearOptions();
                                callback();
                                //that.close();
                            }
                        }
                    },
                    valueSetter: function(rule, value) {
                        rule.$el.find('.rule-value-container select')[0].selectize.addOption({
                            id: value,
                            picklist: value
                        });
                        rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
                    }
                });

                var newId = component.get("v.uniqueId");
                var newLabel = component.get("v.keywordLabel");
                $('#builder').queryBuilder('addFilter', {
                    id: newId,
                    label: newLabel,
                    type: 'string',
                    operators: ["contains", "not_contains"] 
                });

                this.createTypeaheadFilters(component, event, helper);
            
                component.set("v.isSpinner", false);
            } else {
                component.set('v.validationStatusErrorMessage', $A.get("$Label.c.IVP_Custom_Error"));
            }
        } catch (e) {
            console.log('ERROR' + e);
            console.log(e);
            var output = $('#builder').queryBuilder('getRules');
            if (output !== undefined && output != null) {
                component.set('v.validationStatusErrorMessage', $A.get("$Label.c.IVP_Custom_Error"));
            } else {
                component.set('v.validationStatusErrorMessage', null);
            }

        }
    },
    fetchRecord: function(component, event, helper, recoId) {
        var self = this;
        var dwhViewName = helper.getDWHViewName();
        try {
            component.set("v.isSpinner", true);
            var action = component.get("c.fetchRecord");
            action.setParams({
                "recId": recoId
            });
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var rec = response.getReturnValue();
                    if (rec !== undefined && rec != null) {
                        
                        if(rec.SQL_Where_Clause__c){
                            let curFilter = self.getCurrentUserFilter();
                            if(rec.SQL_Where_Clause__c.indexOf(curFilter)>-1){
                                //rec.SQL_Where_Clause__c=rec.SQL_Where_Clause__c.substr(0,rec.SQL_Where_Clause__c.indexOf(curFilter));
                            }
                        }
                        
                        component.set("v.newRec", rec);
                        if (component.get("v.isClone")) {
                            component.set("v.newRec.Name__c", rec.Name__c + ' ' + '(Copy)');
                            component.set("v.newRec.Cloned_From__c", component.get("v.recordId"));
                            component.set("v.newRec.External_Id__c", null);
                        } else {
                            component.set("v.newRec.Name__c", rec.Name__c);
                        }
                        //component.find("mySelect").set("v.value", rec.Include_Exclude__c);
                        if (!rec.Include_Exclude__c) {
                            component.set("v.newRec.ExcludeButton", false);
                            component.set("v.newRec.IncludeButton", false);
                        } else if (rec.Include_Exclude__c === 'Include') {
                            component.set("v.newRec.ExcludeButton", false);
                            component.set("v.newRec.IncludeButton", true);
                        } else {
                            component.set("v.newRec.ExcludeButton", true);
                            component.set("v.newRec.IncludeButton", false);
                        }
                        if (rec.Advanced_Mode__c) {
                            component.find("advanceCheck").set("v.checked", rec.Advanced_Mode__c);
                            document.getElementById("builder").style.display = "none";
                        }
                        if (!rec.Advanced_Mode__c && rec.JSON_QueryBuilder__c !== undefined && rec.JSON_QueryBuilder__c != null) {
                            try {
                            	rec.JSON_QueryBuilder__c = rec.JSON_QueryBuilder__c.replace(new RegExp('company_cross_data_view','g'), dwhViewName);
                                rec.JSON_QueryBuilder__c = rec.JSON_QueryBuilder__c.replace(new RegExp('companies_cross_data_full','g'), dwhViewName);
                                var JsonQb = this.checkForLower(component, event, helper, JSON.parse(rec.JSON_QueryBuilder__c));
                                if (JsonQb !== undefined && JsonQb !== null) {
                                    $('#builder').queryBuilder('setRulesFromMongo', JsonQb);
                                } else {
                                    component.set('v.validationStatusErrorMessage', $A.get("$Label.c.IVP_Custom_Error"));
                                }
                            } catch (e) {
                                component.set('v.validationStatusErrorMessage', $A.get("$Label.c.IVP_Custom_Error"));
                                console.log('ERROR' + e);
                                console.log(e);
                            }
                        }
                        component.set("v.isSpinner", false);
                    }
                } else if (response.state === "ERROR") {
                    component.set("v.isSpinner", false);
                    this.toastMsg(component, event, 'Error message=' + JSON.stringify(response.error), 'error');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            component.set("v.Spinner", false);
        }
    },

    checkForLower: function(component, event, helper, QbJson) {
        var listOfIntField = component.get("v.intFieldRecList");
        if (listOfIntField != undefined && listOfIntField != null && listOfIntField.length > 0) {
            if (QbJson !== undefined && QbJson != null) {
                (function loop(rules) {
                    rules.forEach(function(ruleOrGroup) {
                        if ('rules' in ruleOrGroup) {
                            loop(ruleOrGroup.rules);
                        } else {
                            var ruleid = ruleOrGroup.id;
                            if (ruleid.includes("LOWER(")) {
                                ruleid = ruleid.replace("LOWER(", "");
                                ruleid = ruleid.replace(")", "");
                                for (var i = 0; i < listOfIntField.length; i++) {
                                    if (ruleid == listOfIntField[i].DB_Column__c && listOfIntField[i].QBFlt_Lowercase__c == false) {
                                        ruleOrGroup.id = ruleid;
                                    }
                                }
                            } else {
                                for (var i = 0; i < listOfIntField.length; i++) {
                                    if (ruleOrGroup.id == listOfIntField[i].DB_Column__c && listOfIntField[i].QBFlt_Lowercase__c == true) {
                                        ruleOrGroup.id = 'LOWER(' + ruleid + ')';
                                    }
                                }
                            }
                        }
                    });
                }(QbJson.rules));
                return QbJson;
            }
        }
    },


    createFilters: function(component, body) {
        try {
            var listOfIntField = component.get("v.intFieldRecList");
            var list = [];
            var dataTypeMaping = new Map([
                ['text', 'string'],
                ['integer', 'integer'],
                ['double precision', 'double'],
                ['date', 'date'],
                ['time', 'timestamp without timezone'],
                ['boolean', 'boolean'],
                ['datetime', '']
            ]);

            var arr = [];
            var flag = false;
            if (body != undefined && body != null && body != '') {
                var jsonPicklist = JSON.parse(body);
                for (var i = 0; i < listOfIntField.length; i++) {
                    if (listOfIntField[i].QBFlt_LOVs__c != null) {
                        for (var x in jsonPicklist) {
                            if (jsonPicklist[x].hasOwnProperty(listOfIntField[i].QBFlt_LOVs__c)) {
                                for (var y in jsonPicklist[x]) {
                                    if (y == listOfIntField[i].QBFlt_LOVs__c) {
                                        var obj = jsonPicklist[x][y];
                                        var aMap = {};
                                        var listOfVal = [];
                                        for (var a in obj) {
                                            var aMap1 = {};
                                            var listOfVal1 = [];
                                            if (obj[a] instanceof Array) {
                                                flag = true;
                                                for (var z in obj[a]) {
                                                    var val1 = obj[a][z].value;
                                                    listOfVal1.push(val1);
                                                }
                                                var name = listOfIntField[i].QBFlt_LOVs__c
                                                aMap1[name] = listOfVal1;
                                                arr.push(aMap1);
                                            } else {
                                                flag = false;
                                                var val1 = obj[a].value;
                                                listOfVal.push(val1);
                                            }
                                        }
                                        if (flag === false) {
                                            var name = listOfIntField[i].QBFlt_LOVs__c
                                            aMap[name] = listOfVal;
                                            arr.push(aMap);
                                        }
                                        flag = false;
                                    }
                                }
                            }
                        }
                    }
                }
                console.log('arr=='+JSON.stringify(arr));
                component.set("v.arrayOfPicklist", arr);
            }


            for (var i = 0; i < listOfIntField.length; i++) {
                if (listOfIntField[i].QBFlt_Typeahead__c == false) {
                    var obj = {};
                    if (listOfIntField[i].QBFlt_Lowercase__c) {
                        obj.id = 'LOWER(' + listOfIntField[i].DB_Column__c + ')';
                    } else {
                        obj.id = listOfIntField[i].DB_Column__c;
                    }
                    obj.label = listOfIntField[i].UI_Label__c ? listOfIntField[i].UI_Label__c : 'NA';
                    obj.input = listOfIntField[i].QBFlt_Input__c;
                    if (obj.input == 'radio' || obj.input == 'checkbox' || obj.input == 'select') {
                        if (obj.input == 'select' && listOfIntField[i].QBFlt_Manual_LOVs__c !== undefined && listOfIntField[i].QBFlt_Manual_LOVs__c != null) {
                            obj.values = JSON.parse(listOfIntField[i].QBFlt_Manual_LOVs__c);
                        }
                        else if (obj.input == 'select' && arr !== undefined && arr != null) {
                            for (var key in arr) {
                                if (arr[key].hasOwnProperty(listOfIntField[i].QBFlt_LOVs__c)) {
                                    obj.values = arr[key][listOfIntField[i].QBFlt_LOVs__c];
                                }
                            }
                        }
                    }
                    
                    if (listOfIntField[i].QBFlt_Filter_Grouping__c) {
                        obj.optgroup = listOfIntField[i].QBFlt_Filter_Grouping__c;
                    }
                    obj.type = dataTypeMaping.get(listOfIntField[i].DWH_Data_Type__c); 
                    obj.value_separator = listOfIntField[i].QBFlt_Value_Separator__c;
                    obj.default_value = listOfIntField[i].QBFlt_Default_Value__c;
                    obj.size = listOfIntField[i].QBFlt_Size__c;
                    obj.rows = listOfIntField[i].QBFlt_Rows__c;
                    obj.multiple = listOfIntField[i].QBFlt_Multiple__c;
                    obj.placeholder = listOfIntField[i].QBFlt_Placeholder__c;
                    obj.vertical = listOfIntField[i].QBFlt_Vertical__c;
                    if (listOfIntField[i].QBFlt_Num_Min__c != null && listOfIntField[i].QBFlt_Num_Min__c != '' && listOfIntField[i].QBFlt_Num_Max__c != null && listOfIntField[i].QBFlt_Num_Max__c != '') {
                        var objValid = {};
                        objValid.min = listOfIntField[i].QBFlt_Num_Min__c;
                        objValid.max = listOfIntField[i].QBFlt_Num_Max__c;
                        obj.validation = objValid;
                    }
                    obj.operators = listOfIntField[i].QBFlt_Operators__c != null ? JSON.parse(listOfIntField[i].QBFlt_Operators__c) : null;
                    obj.default_operator = listOfIntField[i].QBFlt_Default_Operator__c;
                    obj.plugin = listOfIntField[i].QBFlt_Plugin_Name__c;
                    list.push(obj);
                } else {
                    if(listOfIntField[i].DB_Column__c == 'ct.'+this.getDWHViewName()+'.ivp_country') {
                        component.set('v.countryPosition', i-1);
                    }
                }
            }
            component.set("v.isSpinner", false);
            return list;
        } catch (e) {
            component.set('v.validationStatusErrorMessage', $A.get("$Label.c.IVP_Custom_Error"));
            console.log('ERROR' + e);
            console.log(e);
        }
    },
    createTypeaheadFilters: function(component, event, helper) {
        try {
            var arr = component.get("v.arrayOfPicklist");
            var listOfIntField = component.get("v.intFieldRecList");
            var verticalsVals = [];
            var obj = {};
            var self = this;
            for (var i = 0; i < listOfIntField.length; i++) {
                if (listOfIntField[i].QBFlt_Typeahead__c 
                    && listOfIntField[i].DB_Column__c != 'ct.'+self.getDWHViewName()+'.ivp_investor_list') {
                    for (var x = 0; x < arr.length; x++) {
                        for (var key in arr[x]) {
                            if (arr[x].hasOwnProperty(key)) {
                                if (key === listOfIntField[i].QBFlt_LOVs__c) {
                                    obj[listOfIntField[i].DB_Column__c] = arr[x][key];
                                }
                            }
                        }
                    }
                }
            }
            $('#builder').queryBuilder('addFilter', {
                id: 'ct.'+self.getDWHViewName()+'.ivp_industry_list',
                label: 'Verticals',
                type: 'string',
                input: 'select',
                multiple: false,
                plugin: 'selectize',
                operators : ["contains", "not_contains"],
                plugin_config: {
                    valueField: 'picklist',
                    labelField: 'picklist',
                    searchField: 'picklist',
                    sortField: 'picklist',
                    options: [],
                    onInitialize: function() {
                        var that = this;
                        that.on("blur", function(e) {
                            that.clearOptions();
                        });
                    },
                    valueSetter: function(rule, value) {
                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                    },
                    load: function(query, callback) {
                        var that = this;
                        // that.clearOptions();
                        if (query && query.length > 1) {
                            var selectId = document.getElementById("selectID").value;
                            if(selectId !== undefined && selectId != null){
                                var spinnerElement = document.getElementById(selectId);
                                spinnerElement.setAttribute("style", "display: block;");
                            }
                            if (obj != null) {
                                for (var key in obj) {
                                    if (obj.hasOwnProperty(key) 
                                        && key === 'ct.'+self.getDWHViewName()+'.ivp_industry_list') {
                                        verticalsVals = obj[key];
                                    }
                                }
                            }
                            helper.getTypeAheadVals(component, query, that, verticalsVals)
                                .then(response => {
                                    if (response !== undefined && response != null && response.length > 0) {
                                        response.forEach(item => {
                                            that.addOption(item);
                                    var selectId = document.getElementById("selectID").value;
                                    if(selectId !== undefined && selectId != null){
                                    var spinnerElement = document.getElementById(selectId);
                                    spinnerElement.setAttribute("style", "display: none;");
                                }                                        });
                        } else {
                            var selectId = document.getElementById("selectID").value;
                            if(selectId !== undefined && selectId != null){
                                var spinnerElement = document.getElementById(selectId);
                                spinnerElement.setAttribute("style", "display: none;");
                            }                                    
                        }
                                    callback(response);
                                });
                        } else {
                            that.clearOptions();
                            callback();
                            //that.close();
                        }
                    }
                },
                valueSetter: function(rule, value) {
                    rule.$el.find('.rule-value-container select')[0].selectize.addOption({
                        id: value,
                        picklist: value
                    });
                    rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
                }
            });
            $('#builder').queryBuilder('addFilter', {
                id: 'ct.'+self.getDWHViewName()+'.ivp_country',
                label: 'Country',
                type: 'string',
                input: 'select',
                multiple: false,
                optgroup: 'Location',
                plugin: 'selectize',
                plugin_config: {
                    valueField: 'picklist',
                    labelField: 'picklist',
                    searchField: 'picklist',
                    sortField: 'picklist',
                    options: [],
                    onInitialize: function() {
                        var that = this;
                        that.on("blur", function(e) {
                            that.clearOptions();
                        });
                    },
                    valueSetter: function(rule, value) {
                        rule.$el.find('.rule-value-container input')[0].selectize.setValue(value);
                    },
                    load: function(query, callback) {
                        var that = this;
                        // that.clearOptions();
                        if (query && query.length > 1) {
                            var selectId = document.getElementById("selectID").value;
                            if(selectId !== undefined && selectId != null){
                                var spinnerElement = document.getElementById(selectId);
                                spinnerElement.setAttribute("style", "display: block;");
                            }
                            if (obj != null) {
                                for (var key in obj) {
                                    if (obj.hasOwnProperty(key) 
                                        && key === 'ct.'+self.getDWHViewName()+'.ivp_country') {
                                        verticalsVals = obj[key];
                                    }
                                }
                            }
                            helper.getTypeAheadVals(component, query, that, verticalsVals)
                                .then(response => {
                                    if (response !== undefined && response != null && response.length > 0) {
                                        response.forEach(item => {
                                            that.addOption(item);
                                    var selectId = document.getElementById("selectID").value;
                                    if(selectId !== undefined && selectId != null){
                                    var spinnerElement = document.getElementById(selectId);
                                    spinnerElement.setAttribute("style", "display: none;");
                                }
                                      });
                        }else{
                            var selectId = document.getElementById("selectID").value;
                            if(selectId !== undefined && selectId != null){
                                var spinnerElement = document.getElementById(selectId);
                                spinnerElement.setAttribute("style", "display: none;");
                            }
                        }
                                    callback(response);
                                });
                        } else {
                            that.clearOptions();
                            callback();
                            //that.close();
                        }
                    }
                },
                valueSetter: function(rule, value) {
                    rule.$el.find('.rule-value-container select')[0].selectize.addOption({
                        id: value,
                        picklist: value
                    });
                    rule.$el.find('.rule-value-container select')[0].selectize.setValue(value);
                }
            }, component.get('v.countryPosition'));
        } catch (e) {
            component.set('v.validationStatusErrorMessage', null);
            console.log('ERROR' + e);
            console.log(e);
        }
    },

    searchRecords: function(component, searchInvestorString, selector) {
        try {
            return new Promise($A.getCallback(function(resolve, reject) {
                var action = component.get("c.searchInvestorRecords_Apex");
                action.setParams({
                    "searchString": searchInvestorString
                });
                action.setCallback(this, function(r) {
                    if (r.getState() === 'SUCCESS') {
                        var response = r.getReturnValue();
                        if (response !== undefined && response != null && response.length > 0) {
                            var newList = [];
                            for (var i = 0; i < response.length; i++) {
                                for (var x = 0; x < response[i].length; x++) {
                                    if (x == 1) {
                                        var obj = {};
                                        obj.id = i;
                                        obj.picklist = response[i][x];
                                        newList.push(obj);
                                    }
                                    if (newList.length === 10) {
                                        break;
                                    }
                                }
                            }
                        }
                        resolve(newList);
                    } else {
                        console.log('ERROR');
                        console.log(r.getError());
                        reject({
                            error: r.getError()
                        });
                    }
                });
                $A.enqueueAction(action);
            }));
        } catch (e) {
            console.log('ERROR' + e);
            console.log(e);
        }
    },

    getTypeAheadVals: function(component, searchInvestorString, selector, picklistVals) {
        try {
            return new Promise($A.getCallback(function(resolve, reject) {
                var newList = [];
                if (picklistVals != null && picklistVals.length > 0) {
                    for (var i = 0; i < picklistVals.length; i++) {
                        if (picklistVals[i].includes(searchInvestorString) || picklistVals[i].toLowerCase().includes(searchInvestorString) ||
                            picklistVals[i].includes(searchInvestorString.toLowerCase()) || picklistVals[i].toLowerCase().includes(searchInvestorString.toLowerCase())) {
                            var obj = {};
                            obj.id = i;
                            obj.picklist = picklistVals[i];
                            newList.push(obj);
                        }
                        if (newList.length === 10) {
                            break;
                        }
                    }
                    resolve(newList);
                } else {
                    reject({
                        error: 'Error'
                    });
                }
            }));
        } catch (e) {
            console.log('ERROR' + e);
            console.log(e);
        }
    },

    checkForKeyword: function(component, event, helper) {
        var output = $('#builder').queryBuilder('getRules');
        if (output !== undefined && output != null) {
            var ruleForKeyword = [];
            (function loop(rules) {
                rules.forEach(function(ruleOrGroup) {
                    if ('rules' in ruleOrGroup) {
                        loop(ruleOrGroup.rules);
                    } else if (ruleOrGroup.id == $A.get("$Label.c.IVP_Querybuilder_Keyword_Label")) {
                        ruleForKeyword.push(ruleOrGroup);
                    }
                });
            }(output.rules));
            return ruleForKeyword;
        }
    },

    convertToLowerCase: function(component, event, helper) {
        var output = $('#builder').queryBuilder('getRules');
        if (output !== undefined && output != null) {
            (function loop(rules) {
                rules.forEach(function(ruleOrGroup) {
                    if ('rules' in ruleOrGroup) {
                        loop(ruleOrGroup.rules);
                    } else if (ruleOrGroup.id.includes("LOWER") && ruleOrGroup.value) {
                        var value = ruleOrGroup.value;
                        ruleOrGroup.value = value.toLowerCase();
                    }
                });
            }(output.rules));
            return output;
        } else {
            return null;
        }
    },

    getKeywordQuery: function(component, event, helper, ruleForKeyword, newRecord, setSqlWhereClase) {
        var action1 = component.get("c.getKeywordQuery");
        var dwhViewName = helper.getDWHViewName();
        action1.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var res = response.getReturnValue();
                if (res !== undefined && res != null) {
                    var sqlRes = $('#builder').queryBuilder('getSQL', false);
                    var result = sqlRes.sql;
                    var intFiledList = res;
                    var query = '';
                    for (var i = 0; i < ruleForKeyword.length; i++) {
                        query = '';
                        for (var z = 0; z < intFiledList.length; z++) {

                            intFiledList[z].DB_Column__c = intFiledList[z].DB_Column__c.replace(intFiledList[z].DB_Table__c, 'ct.'+dwhViewName);

                            if (intFiledList[z].QBFlt_Lowercase__c) {
                                if (query != '') {
                                    query += ' OR LOWER(' + intFiledList[z].DB_Column__c + ')' + this.getOperatorValue(ruleForKeyword[i].value.toLowerCase(), ruleForKeyword[i].operator);
                                } else {
                                    query += 'LOWER(' + intFiledList[z].DB_Column__c + ')' + this.getOperatorValue(ruleForKeyword[i].value.toLowerCase(), ruleForKeyword[i].operator);
                                }
                            } else {
                                if (query != '') {
                                    query += ' OR ' + intFiledList[z].DB_Column__c + this.getOperatorValue(ruleForKeyword[i].value, ruleForKeyword[i].operator);
                                } else {
                                    query += intFiledList[z].DB_Column__c + this.getOperatorValue(ruleForKeyword[i].value, ruleForKeyword[i].operator);
                                }
                            }
                        }
                        
                        var abc = $A.get("$Label.c.IVP_Querybuilder_Keyword_Label") + this.getOperatorValue(ruleForKeyword[i].value, ruleForKeyword[i].operator);
                        query = '(' + query + ')';
                        if(result != null) {
                            if(result.includes('iLIKE') ) {
                                result = result.replace(/iLIKE/g, "LIKE");
                            }
                            if(result.includes('LIKE')) {
                                result = result.replace(/LIKE/g, "iLIKE");
                            }
                        }
                        result = result.replace(abc, query);
                    }
                    if (result !== undefined && result != null) {
                        if (newRecord !== undefined && newRecord != null) {
                            if (result != null && result.includes("\\")) {
                                result = result.replace(/\\/g, "'");
                            }
                            newRecord.SQL_Where_Clause__c = result;
                            this.saveSourcingRecord(component, event, helper, newRecord);
                        } else if (setSqlWhereClase !== undefined && setSqlWhereClase != null) {
                            if (result != null && result.includes("\\")) {
                                result = result.replace(/\\/g, "'");
                            }
                            component.set("v.newRec.SQL_Where_Clause__c", result);
                        } else {
                            if (result != null && result.includes("\\")) {
                                result = result.replace(/\\/g, "'");
                            }
                            component.set("v.sqlString", result);
                            component.set("v.openViewSql", true);
                        }
                    }
                }
            } else if (response.state === "ERROR") {
                this.toastMsg(component, event, 'Error message=' + JSON.stringify(response.error), 'error');
            }
        });
        $A.enqueueAction(action1);
    },

    getOperatorValue: function(value, operator) {
        var str = '';
        if (operator == 'equal') {
            str = ' = \'' + value + '\'';
        } else if (operator == 'not_equal') {
            str = ' != \'' + value + '\'';
        } else if (operator == 'in') {
            str = ' IN(\'' + value + '\')';
        } else if (operator == 'not_in') {
            str = ' NOT IN(\'' + value + '\')';
        } else if (operator == 'begins_with') {
            str = ' iLIKE(\'' + value + '%\')';
        } else if (operator == 'not_begins_with') {
            str = ' NOT iLIKE(\'' + value + '%\')';
        } else if (operator == 'contains') {
            str = ' iLIKE(\'%' + value + '%\')';
        } else if (operator == 'not_contains') {
            str = ' NOT iLIKE(\'%' + value + '%\')';
        } else if (operator == 'ends_with') {
            str = ' iLIKE(\'%' + value + '\')';
        } else if (operator == 'not_ends_with') {
            str = ' NOT iLIKE(\'%' + value + '\')';
        } else if (operator == 'is_empty') {
            str = ' = \'\'';
        } else if (operator == 'is_not_empty') {
            str = ' != \'\'';
        } else if (operator == 'is_null') {
            str = ' IS NULL';
        } else if (operator == 'is_not_null') {
            str = ' IS NOT NULL';
        }
        return str;
    },

    saveSourcingRecord: function(component, event, helper, saveRecordVal) {
        var self = this;
        try {
            component.set("v.isSpinner", true);
            var newRecUpdateObj = {};
            newRecUpdateObj.Id = saveRecordVal.Id;
            newRecUpdateObj.Name__c = saveRecordVal.Name__c;
            newRecUpdateObj.JSON_QueryBuilder__c = saveRecordVal.JSON_QueryBuilder__c;
            newRecUpdateObj.SQL_Where_Clause__c = saveRecordVal.SQL_Where_Clause__c;
            
            let curFilter = self.getCurrentUserFilter();
            if(newRecUpdateObj.SQL_Where_Clause__c.indexOf(curFilter)==-1){
                //newRecUpdateObj.SQL_Where_Clause__c+=curFilter;
            }
            newRecUpdateObj.Validation_Status__c = saveRecordVal.Validation_Status__c;
            newRecUpdateObj.Last_Validated__c = saveRecordVal.Last_Validated__c;
            newRecUpdateObj.Recent_Validation_Error__c = saveRecordVal.Recent_Validation_Error__c;
            newRecUpdateObj.Advanced_Mode__c = saveRecordVal.Advanced_Mode__c;
            newRecUpdateObj.External_Id__c = saveRecordVal.External_Id__c;
            newRecUpdateObj.Cloned_From__c = saveRecordVal.Cloned_From__c;
            newRecUpdateObj.Sent_to_DWH__c = true;
            if (saveRecordVal.IncludeButton === true) {
                newRecUpdateObj.Include_Exclude__c = 'Include';
            } else if (saveRecordVal.ExcludeButton === true) {
                newRecUpdateObj.Include_Exclude__c = 'Exclude';
            } else {
                newRecUpdateObj.Include_Exclude__c = undefined;
            }
            var action1 = component.get("c.saveSourcingPreference");
            action1.setParams({
                "saveRec": newRecUpdateObj
            });
            action1.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var res = response.getReturnValue();
                    console.log('res===' + JSON.stringify(res));
                    component.set("v.newValues", res);
                    if (res.isPass === true) {
                        component.set("v.SaveValues", res);
                        component.set("v.isCustom", false);
                        component.set("v.isClone", false);
                        if (res.validationStatus === 'Valid') {
                            component.set("v.isCloned", true);
                            component.set("v.previewPopChange", true);
                        } else {
                            if (res.validationStatus === 'Invalid'
                                || res.validationStatus === 'In Progress') {
                                component.set('v.validationStatusErrorMessage', res.recentValidationError);
                                
                                this.toastMsg(component, event,res.recentValidationError, 
                                                  res.validationStatus === 'In Progress' ? 'warning': 'error');
                                component.set("v.isTableView", false);
                            }
                        }
                        helper.fetchRecord(component, event, helper, res.newRecId);
                    } else {
                        this.toastMsg(component, event, 'There is some technical issue.', 'error');
                    }
                } else if (response.state === "ERROR") {
                    this.toastMsg(component, event, 'Error message=' + JSON.stringify(response.error), 'error');
                }
                component.set("v.isSpinner", false);
            });
            $A.enqueueAction(action1);
        } catch (e) {
            console.log('ERROR' + e);
            console.log(e);
        }
    },

    handleShowModalFooter: function(component, event, helper) {
        var modalBody;
        var modalFooter;
        $A.createComponents([
                ["c:ConfirmationModalContent", {}],
                ["c:ConfirmationModalFooter", {}]
            ],
            function(components, status) {
                if (status === "SUCCESS") {
                    modalBody = components[0];
                    modalFooter = components[1];
                    component.find('overlayLib').showCustomModal({
                        // header: "Test",
                        body: modalBody,
                        footer: modalFooter,
                        showCloseButton: false,
                        cssClass: "my-modal,my-custom-class,my-other-class",
                        closeCallback: function() {}
                    })
                }
            }
        );
    },


    toastMsg: function(component, event, msg, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": msg
        });
        toastEvent.fire();
    },
})