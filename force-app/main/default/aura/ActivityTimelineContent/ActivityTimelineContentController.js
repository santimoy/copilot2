({
	doInit : function(component, event, helper) {
		var today = new Date();
		console.log('today: ',today);
		var monthDigit = today.getMonth() + 1;
		if (monthDigit <= 9) {
			monthDigit = '0' + monthDigit;
		}
		component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate()); 
		console.log('today1111: ',component.get('v.today'));

		var key = component.get('v.activity.activityId');
		var map = component.get('v.mapTaskIdToRichTextComment');
		
		// var datefor = new Date(component.get('v.activity.activityDate'));

		// console.log('>>>14>>',datefor.getMonth());
		// console.log('>>>15>>',datefor.getMonth()+1);
		// var monthDigitAT = datefor.getMonth() + 1;
		// var dayDigitAT =  datefor.getDate();
		// if (monthDigitAT <= 9) {
		// 	monthDigitAT = '0' + monthDigitAT;
			
		// }
		// if ( dayDigitAT<=9) {
		
		// 	dayDigitAT = '0' + dayDigitAT;
		// }
		// console.log('dayDigitAT>>>26',dayDigitAT);
		// component.set('v.activity.activityDate', datefor.getFullYear() + "-" + monthDigitAT + "-" +dayDigitAT); 

		if(!$A.util.isEmpty(map) && map.hasOwnProperty(key)) {
			component.set('v.richTextTaskComment', map[key]);
		}
		helper.fetchTaskCommentBasedonId(component, event, helper);
	},

	toggleContentSection : function(component, event, helper) {
		var activity = component.get("v.activity");
		console.log( 'activity : ', activity );
		if(activity.isOpen){
			activity.isOpen = false;
		} else {
			activity.isOpen = true;
		}
		component.set("v.activity", activity);
	},

	redirectToRecord : function(component, event, helper) {
		var activityId = event.currentTarget.getAttribute("id");
		var navEvt = $A.get("e.force:navigateToSObject");
		console.log( 'navEvt : ', navEvt );
		navEvt.setParams({
			"recordId": activityId,
		});
		navEvt.fire();
	}
})