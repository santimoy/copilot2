({
	fetchTaskCommentBasedonId : function(component,helper) {
        var action = component.get("c.isRichTextCommentExist");
        console.log('action',action);
        action.setParams({
			strTaskId: component.get('v.activity.activityId')
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set('v.isRichTextComment', response.getReturnValue()); 
            }
        });
        $A.enqueueAction(action); 
	}
})