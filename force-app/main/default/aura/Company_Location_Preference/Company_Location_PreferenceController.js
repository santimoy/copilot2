({
    doInIt : function(component, event, helper) {
        console.log("Preference Set");
        return;
        var action = component.get("c.getSourcingPreferenceWithCount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                if(result && result.length > 0 && result[0].errorInfo){
                    let errorMsg = result[0].errorInfo;
                    let errorInd = errorMsg.indexOf('ERROR:');
                    errorMsg = errorInd >-1 ? errorMsg.substr(errorInd, 60) : errorMsg;
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type":"info",
                        "message":  errorMsg
                    });
                    toastEvent.fire();
                }
                component.set("v.activePreferenceList",result);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var toastEvent = $A.get("e.force:showToast");
                        if(toastEvent){
                            toastEvent.setParams({
                                "type":"error",
                                "title": "Error",
                                "message": errors[0].message
                            });
                            toastEvent.fire();
                        }else{
                            console.log("Error message: " +  errors[0].message);
                        }
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},
    setSelectedPreference :function(component, event, helper) {
        component.set("v.Spinner",true);
        var currentTarget = event.currentTarget;
        var hasGray = $A.util.hasClass(currentTarget, "gray");
        console.log('Has Gray >> '+hasGray);
        if(!hasGray){
            var preferenceList = document.getElementsByClassName("preference");
            for(var i=0;i<preferenceList.length;i++){
                preferenceList[i].classList.remove("gray"); 
            }
            var preferenceId = event.currentTarget.getAttribute("data-id");
            console.log('preferenceId'+preferenceId);
            $A.util.addClass(event.currentTarget, 'gray');
            var compEvent = component.getEvent("preferenceEvent");
            compEvent.setParams({"preferenceId" : preferenceId });
            console.log('Event Fire');
            compEvent.fire();
           
        }
        component.set("v.Spinner",false);
    },
    togglePreference : function(component, event, helper) {
        var preferenceBody = component.find("preferenceBody");
        $A.util.toggleClass(preferenceBody, 'toggleHide');
        component.set("v.isOpen",!component.get("v.isOpen"));
    }
})