({
	doInit : function(cmp){
		this.setDefaults(cmp);
        this.prepareOptions(cmp);
	},
    setDefaults: function(cmp){
        //cmp.set('v.isEmail', false);
    },
    prepareOptions : function(cmp){
        var opts = [
            //{ value: "Call", label: "Call", icon: "call" },
            // { value: "Email", label: "Email", icon: "email" },
            { value: "LogActivity", label: "Log", subLabel: 'Activity', icon:"note" },
            { value: "ExamineActivity", label: "Examine", subLabel: 'Activity', icon:"task" }
         ];
        cmp.set('v.dockOptions', opts);
    },
    panelUp : function(cmp){
        
        window.setTimeout(            
            $A.getCallback(function() {
                var panel = cmp.find('panel');
                $A.util.toggleClass (panel, 'bottom-up');
                
                window.setTimeout(            
                    $A.getCallback(function() {
                        var panel = cmp.find('panel');
                        $A.util.toggleClass (panel, 'bottom-up');

                    }), 200);
            }), 200
            
        );
    }
})