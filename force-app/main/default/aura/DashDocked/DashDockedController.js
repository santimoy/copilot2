({
    doInit : function(component, event, helper) {
        helper.doInit(component);
        var panel = component.find('panel');
        $A.util.addClass(panel, 'bottom-up');
    },
    handleClick : function(component, event, helper) {
        var current = component.get('v.selectedTab');
        var panalId = event.currentTarget.dataset.panalId;
        if(current == panalId){
            component.set('v.selectedTab', '');
        }else{
            component.set('v.selectedTab', '');
        }
	},
    
    handleDashDockedAppEvent : function(component, event, helper) {
        var dockedData = event.getParam('dockedData');
        component.set('v.selectedTab', '');
        // console.log(JSON.stringify(dockedData));
        if(dockedData){
            if(dockedData.hideLog){
                component.set('v.selectedTab', '');
                return;
            }
            
            if(dockedData.action === 'Log Activity'){
            	dockedData.secTitle = 'Log an Activity';
            }else if(dockedData.action === 'Examine Activity'){
            	dockedData.secTitle = 'Log an Examine';
            }
            component.set('v.data', dockedData);
            component.set('v.selectedTab', dockedData.action.replace(' ',''));
        }
        helper.panelUp(component);
    },
    handleClose : function(component, event, helper) {
        component.set('v.selectedTab', '');
    }
})