({
    doInit : function(cmp, event, helper) {
        helper.doInit(cmp);
    },

    onViewSelectChange: function(component, event, helper) {
        var selectCmp = component.find("InputSelect");
        var selectedViewId = selectCmp.get("v.value");
        helper.handleLoadView(component, {viewId:selectedViewId,source:'ListViewLoad'});
        helper.resetTagFilter(component);
    },
    handleIVPCompanyListViewEvent: function(component, event, helper) {
        var formData = event.getParam('formData');
        if(formData){
            if(formData.source === 'Chart' ){
                helper.maintainChartView(component, formData);
            }else if(formData.source === 'ChartReset'){
                var lastUpdateLV = component.get('v.lastUpdateLV');
                lastUpdateLV.source='ListViewLoad';
                if(lastUpdateLV && lastUpdateLV.devValue){
                    helper.handleLoadView(component, lastUpdateLV);
                }
            }
        }
    },
    handleIVPTaggingAppEvent: function(component, event, helper) {
        var formData = event.getParam('formData');
        if(formData && formData.source != 'list'
        	&& formData.source != 'change'){
        	component.set('v.filterType',formData.source);
            helper.refreshList(component, formData.action);
        }else if(formData.source === 'change'){
            var userId = helper.getCurrentUserId();
    	    if(formData.selectedLV){
    	        if(formData.selectedLV.viewName.indexOf(userId)===-1){
        		    component.set('v.selectedLV', formData.selectedLV);
            	}
    	    }
        }
    }
})