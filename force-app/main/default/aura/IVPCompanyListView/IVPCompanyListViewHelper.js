({
	doInit : function(cmp) {
        this.refreshList(cmp, 'apply');
        this.handleSpinner(cmp, false);
    },
    refreshList : function(cmp, action){
        this.handleCalling(cmp, 'getInitData', {}, this.initCallback, this, {action:action});
    },
    getSelectedOption : function(cmp){
    	var selectCmp = cmp.find("InputSelect");
    	var selectedViewId = selectCmp.get("v.value");
        var options = selectCmp.get('v.options');
        for(var index = 0, length = options.length; index < length; index++){
            var option = options[index];
            if(option.devValue && selectedViewId === option.value){
                return option;
            }
        }
        return null;
    },
    initCallback : function(cmp, hlpr, response, callbackInfo){
        var opts = [{ "class": "optionClass", 
                       label: '--None--', 
                       value: '', 
                       selected: true }];
        
        var defaultLVName = 'PUBLIC_Smart_Dash_Default';
        var defaultLVIndex = 0;
        
        var result = response.getReturnValue();
        var listViews = result.listViews;
        var ipvSDListView = result.ipvSDListView;
        var ipvOrgSDListView = result.ipvOrgSDListView;
        var defaultOrgLVName = ''
        var selectCmp = cmp.find("InputSelect");
        if(listViews.length > 0){
            opts = [];
            listViews.sort(function(a, b) {
                return a.label.toLowerCase().localeCompare(b.label.toLowerCase())});
            defaultLVName = listViews[0].developerName;
            defaultLVIndex = 0;
        }else{
            selectCmp.set('v.disabled', true);
        }
		
		if(ipvOrgSDListView){
            defaultLVName = ipvOrgSDListView.List_View_Name__c ? ipvOrgSDListView.List_View_Name__c : '';
        }
        
        if(ipvSDListView){
            defaultLVName = ipvSDListView.List_View_Name__c ? ipvSDListView.List_View_Name__c : '';
        }
        
        var filterType = cmp.get('v.filterType');
        
        if(filterType !== ''){
            filterType='ivp_sd_'+filterType;
        }
        if(callbackInfo.action === 'clear'){
            filterType = '';
        }

        var userId = hlpr.getCurrentUserId();//$A.get("$SObjectType.CurrentUser.id");
        var lIndex=0;
        
        for(var index=0, length=listViews.length; index<length; index++){
            var listView = listViews[index];

            if(listView.developerName.toLowerCase().indexOf('ivp_sd_') < 0 
              || (listView.developerName.toLowerCase().indexOf('ivp_sd_') >= 0 
                  && listView.developerName.indexOf(userId)>0)){
                
                if(defaultLVName.toLowerCase() == listView.developerName.toLowerCase()){
                    defaultLVIndex = lIndex;
                    defaultOrgLVName = '';
                }
                
                if(filterType !== '' && listView.developerName.toLowerCase().indexOf(filterType) >=0){
                    defaultLVIndex = lIndex;
                    defaultOrgLVName = '';
                    defaultLVName = '';
                }
                
                if( defaultOrgLVName !== '' && defaultOrgLVName.toLowerCase() == listView.developerName.toLowerCase()){
                	defaultLVIndex = lIndex;
                	defaultOrgLVName = '';
                    defaultLVName='';
                }
                lIndex++;
                opts.push({ "class": "optionClass", 
                       label: listView.label, 
                       value: listView.id, 
                       devValue: listView.developerName});
            }
        }
        
        selectCmp.set("v.options", opts);
        selectCmp.set("v.value", opts[defaultLVIndex].value);
        let viewName = opts[defaultLVIndex].devValue;
        if(viewName.indexOf('ivp_sd_') === -1){
	        cmp.set('v.lastUpdateLV', opts[defaultLVIndex]);
        }
        hlpr.handleLoadWatchDog(cmp, opts[defaultLVIndex]);

    },
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(cmp, hlpr, response, callbackInfo);
            }
            else if (state === "INCOMPLETE") {
                console.log('incomplete');
                //hlpr.showMessage({message:'Cannot laod!', type:"error"});
            }else if (state === "ERROR") {
                console.log('error');
                hlpr.showMessage({message:'Please refresh the page!', type:"Info"});
            }
            
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    handleLoadWatchDog : function(cmp, option ){
	    console.log('handleLoadWatchDog');
	    console.log('option>>>>: '+JSON.stringify(option));
        var ivpCompanyListViewEvent = $A.get("e.c:IVPLoadSMWDAppEvent");
        if(ivpCompanyListViewEvent){
            ivpCompanyListViewEvent.setParams({ 
                "formData" : {viewId:option.value, viewName:option.devValue, source:'ListView'}
            });

            ivpCompanyListViewEvent.fire();
        }else{
            alert('CompanyListView: Event not found!\\nPlease Contact to Administrator!');
        }
    },
    handleLoadView: function(cmp, data){
        var selectCmp = cmp.find("InputSelect");
        var options = selectCmp.get('v.options');
        var userId = this.getCurrentUserId();
        
        for(var index=0, length = options.length; index < length; index++){
            var option = options[index];
            if(option.devValue !== undefined 
               &&((data.source === 'ListViewLoad' && (option.value === data.viewId || option.value === data.value))
               || ( data.source === 'Chart' && option.devValue.toLowerCase() === data.viewName.toLowerCase()))){
                

                selectCmp.set("v.value", option.value);
                if(option.devValue.indexOf('ivp_sd_')==-1)
                    cmp.set('v.lastUpdateLV', option);
                this.handleLoadWatchDog(cmp, option);
                if(option.devValue.indexOf(userId) < 0 && data.source ==='ListViewLoad'){
                    this.updateViewName(cmp, option.devValue);
                }
                return;
            }
        }

        if(data.source === 'Chart'){
            this.handleLoadWatchDog(cmp, {label: '', 
                                          value: '', 
                                          devValue:''});
        }
    },
    updateViewName: function(cmp, viewName){
        this.handleCalling(cmp, 'maintainDefautListView', {viewName:viewName}, this.updateViewNameCallBack, this, {viewName:viewName});
    },
    updateViewNameCallBack : function(cmp, hlpr, response, callbackInfo){
        //cmp.set('v.lastUpdateLV', callbackInfo);
    },
    maintainChartView : function(cmp, data) {
    	this.handleSpinner(cmp, true);
    	var selectedLV = cmp.get('v.selectedLV');
    	if(selectedLV){
    		data.selectedLVId = selectedLV.viewId;
    	}
    	this.handleCalling(cmp, 'maintainChartListView', {inputJson:JSON.stringify(data)}, this.maintainCVCallBack, this, null);
    },
    maintainCVCallBack : function(cmp, hlpr, response, callbackInfo) {
        var resultMap = response.getReturnValue();
        if(resultMap['success']){
            cmp.set('v.filterType','chart');
            var filterTitle='';
            for(let key in resultMap){
                if(key!='success'){
                    let data = resultMap[key];
                    let value = data.value;
                    let fieldLabel = data.fieldLabel;
                    let fieldType = data.fieldType;
                    if(value.indexOf('ACCOUNT.')==0){
                        value = value.substring(8);
                    }
                    if(fieldType){
                        if(fieldType.toLowerCase()=='boolean' && value){
                            value = value==1;
                        }
                            
                    }
                    filterTitle += fieldLabel +'='+value+' AND ';
                }
            }
            if(filterTitle.length > 0){
                filterTitle = filterTitle.substring(0, filterTitle.length-5);
                hlpr.resetChart(cmp,{source:'RefreshTitle',filterTitle:filterTitle});
            }
            hlpr.resetTagFilter(cmp);
            hlpr.refreshList(cmp, 'chart');
        }else{
            if(resultMap['errors']){
                let msg = resultMap['errors'];
                var index = msg.indexOf('FIELD_INTEGRITY_EXCEPTION');
                if(index > -1){
                    let start = msg.indexOf('(', msg.indexOf('#'));
                    let end = msg.indexOf(' ', start);
                    msg = msg.substring(start+1,end) + ' has FIELD_INTEGRITY_EXCEPTION. Try again by de-selecting include this field via ListView filters! Please Contact To Administrator!';
                }else{
                    index = msg.indexOf('INVALID_CROSS_REFERENCE_KEY');
                    if(index>-1){
                        let start = msg.indexOf('field');
                        let end = msg.indexOf(' ', start+7);
                        if(start > -1 && end > -1){
                            msg = msg.substring(start+7, end) + ' has INVALID_CROSS_REFERENCE_KEY. Try again by de-selecting include this field via ListView filters! Please Contact To Administrator!';
                        }
                    }
                }
                var errors = [{message:msg}]
                
                hlpr.handleErrors(errors);
            }
        }
        hlpr.handleSpinner(cmp, false);
    },
    getCurrentUserId: function(){
        return $A.get("$SObjectType.CurrentUser.Id");
    },
    resetTagFilter : function(cmp){
        var ivpCompanyListViewEvent = $A.get("e.c:IVPTaggingAppEvent");
        if(ivpCompanyListViewEvent){
            ivpCompanyListViewEvent.setParams({ 
                "formData" : {source:'list'}
            });
            ivpCompanyListViewEvent.fire();
        }else{
            alert('CompanyListView: Event not found!\\nPlease Contact to Administrator!');
        }
    },
    resetChart : function(cmp,data){
        var eventIVP_Chart_Refresh = $A.get("e.c:IVP_Chart_Refresh");
        if(eventIVP_Chart_Refresh){
            eventIVP_Chart_Refresh.setParams({ 
                "formData" : data
            });
            eventIVP_Chart_Refresh.fire();
        }else{
            alert('CompanyListView: Event not found!\\nPlease Contact to Administrator!');
        }
    },
    handleSpinner : function(cmp, showSpinner){
        var spinner = cmp.find("clvSpinner");
        if(spinner){
	        if(showSpinner){
	            $A.util.removeClass(spinner, "slds-hide"); 
	        } else {
	            $A.util.addClass(spinner, "slds-hide");
	        }
        }
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown Error",
            type: "error"
        };
        
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message+'\n ';
        }
        toastParams.message+='OR try again by de-selecting include current ListView filters!';
        toastParams.message+='\nPlease Contact To Administrator!';
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    }
})