({
    handleCancel : function(component, event, helper) {
        var applicationEvent = $A.get("e.c:ConfirmationCheck");
        applicationEvent.setParams({"sqlEditor" : false})
        applicationEvent.fire();
        component.find("overlayLib").notifyClose();
    },
    
    handleOK : function(component, event, helper) {
        var applicationEvent = $A.get("e.c:ConfirmationCheck");
        applicationEvent.setParams({"sqlEditor" : true})
        applicationEvent.fire();
        component.find("overlayLib").notifyClose();
    }
})