({
    getActivities : function(component, activityFilter, dateFilter, selectedTalentGroupFilter) {
        var action = component.get("c.getAccountWithActivities");
        
        var cachedActivityFilterState = window.localStorage.getItem("activityFilterState");
        console.log('cachedActivityFilterState-----',cachedActivityFilterState)
        var afs = ( cachedActivityFilterState ) ? cachedActivityFilterState : activityFilter;
        console.log('cachedActivityFilterState-----',afs)
        component.set("v.selectedActivityFilter", afs);
        var activityFilterStr = ( afs === "All" ) ? "" : afs;
        
        var cachedDateFilterState = window.localStorage.getItem("dateFilterState");
        var dfs = ( cachedDateFilterState ) ? cachedDateFilterState : dateFilter;
        component.set("v.selectedDateFilter", dfs);
        var dateFilterStr = ( dfs === 'All' ) ? "" : dfs;

        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var cachedMyFilterValue = window.localStorage.getItem(userId);
        console.log('cachedMyFilterValue: ',cachedMyFilterValue);
        var mfs = ( cachedMyFilterValue != null && cachedMyFilterValue != '' &&  cachedMyFilterValue != undefined ) ? cachedMyFilterValue : selectedTalentGroupFilter;

        if(mfs != 'All' && cachedMyFilterValue != null){
        component.set("v.selectedTalentActivityFilter", mfs);
        console.log('Selected value from Attribute: ',selectedTalentGroupFilter);
        console.log('Selected value from canchec: ',mfs);
        }else if(cachedMyFilterValue == 'All'){
            component.set("v.selectedTalentActivityFilter", cachedMyFilterValue);
        }
        
        console.log('selectedTalentActivityFilter: ',component.get("v.selectedTalentActivityFilter"));
        console.log('activityFilterStr: ',activityFilterStr);
        console.log('dateFilterStr: ',dateFilterStr);
        console.log('selectedTalentGroupFilter: ',selectedTalentGroupFilter);

        action.setParams({
            "recordId" : component.get("v.recordId"),
            "activityFilter" : activityFilterStr,
            "dateFilter" : dateFilterStr,
            "selectedTalentGroupFilter" : component.get("v.selectedTalentActivityFilter")
        });
        
        var spinner = component.find("loading_spinner");
        $A.util.removeClass(spinner, "slds-hide");
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var activityTimeline = response.getReturnValue();
                console.log('activityTimeline0------>'+activityTimeline);
                 console.log('activityTimeline1------>'+activityTimeline.openActivities);
                 console.log('activityTimeline2------>'+activityTimeline.pastActivities);
                console.log('activityTimeline.pastActivities',activityTimeline.pastActivities);
                component.set("v.mapTaskIdToRichTextComment", activityTimeline.mapTaskIdToRichTextComment);
                component.set("v.openActivities", activityTimeline.openActivities);
                component.set("v.activityHistories", activityTimeline.pastActivities);
                component.set("v.numberOfPastActivitiesLoaded", activityTimeline.loadedNumOfPastActivities);
                component.set("v.totalNumOfPastActivities", activityTimeline.totalCountOfPastActivities);
                
                var expandAllState = window.localStorage.getItem("expandAllState");
                if(expandAllState === "expanded"){
                    this.expandAllEventHelper(component);
                } else if(expandAllState === "collapsed"){
                    this.collapseAllEventHelper(component);
                }
                $A.util.addClass(spinner, "slds-hide");
            } else {
                console.log('Problem getting account, response state: ' + state);
                $A.util.addClass(spinner, "slds-hide");
            }
        });
        $A.enqueueAction(action);
    },
    
    
    loadMorePastActivitiesHelper : function(component){
        var loadPastActivitiesAction = component.get("c.loadPastActivities");
        
        var activityFilter = component.get("v.selectedActivityFilter");
        var activityFilterStr = ( activityFilter === "All" ) ? "" : activityFilter;
        
        var dateFilter = component.get("v.selectedDateFilter");
        var dateFilterStr = ( dateFilter === "All" ) ? "" : dateFilter;
        
        var activityHistories = component.get("v.activityHistories");
        
        loadPastActivitiesAction.setParams({
            "recordId" : component.get("v.recordId"),
            "activityFilter" : activityFilterStr,
            "dateFilter" : dateFilterStr,
            "offsetValue" : activityHistories.length
        });
        
        // Configure response handler
        var spinner = component.find("loading_spinner");
        $A.util.removeClass(spinner, "slds-hide");
        loadPastActivitiesAction.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state === "SUCCESS") {
                var activities = response.getReturnValue();
                var isExpandedAllVal = component.get("v.isExpandedAll");
                var activityHistories = component.get("v.activityHistories");
                
                activities.forEach(function(activity){
                    if(isExpandedAllVal){
                        activity.isOpen = true;
                    }
                    activityHistories.push(activity);
                });
                component.set("v.activityHistories", activityHistories);
                component.set("v.numberOfPastActivitiesLoaded", activityHistories.length);
                $A.util.addClass(spinner, "slds-hide");
                console.log( 'action completed' );
            } else {
                console.log('Problem getting account, response state: ' + state);
                console.log( 'action failed' );
                $A.util.addClass(spinner, "slds-hide");
            }
        });
        $A.enqueueAction(loadPastActivitiesAction);
    },
    
    expandAllEventHelper : function(component){
        var openActivities = component.get("v.openActivities");
        if( openActivities.length > 0 ){
            openActivities.forEach(function(activity){
                activity.isOpen = true;
            });
            component.set("v.openActivities", openActivities);
        }
        
        var activityHistories = component.get("v.activityHistories");
        if( activityHistories.length > 0 ){
            activityHistories.forEach(function(activity){
                activity.isOpen = true;
            });
            component.set("v.activityHistories", activityHistories);
        }
        
        window.localStorage.setItem("expandAllState", "expanded");
        component.set("v.isExpandedAll", true);
    },
    
    collapseAllEventHelper : function(component){
        var openActivities = component.get("v.openActivities");
        if( openActivities.length > 0 ){
            openActivities.forEach(function(activity){
                activity.isOpen = false;
            });
            component.set("v.openActivities", openActivities);
        }
        
        var activityHistories = component.get("v.activityHistories");
        if( activityHistories.length > 0 ){
            activityHistories.forEach(function(activity){
                activity.isOpen = false;
            });
            component.set("v.activityHistories", activityHistories);
        }
        
        window.localStorage.setItem("expandAllState", "collapsed");
        component.set("v.isExpandedAll", false);
    },
    
    activityFilterSelectHandler : function(component, event){
        var selectedActivityFilter = event.getParam("value");
        console.log('selectedActivityFilter====164> ',selectedActivityFilter);
        window.localStorage.setItem("activityFilterState", selectedActivityFilter);
        
        var dateFilter;
        var cachedDateFilterState = window.localStorage.getItem("dateFilterState");
        console.log('cachedDateFilterState=====169> ',cachedDateFilterState);
        if( !cachedDateFilterState ){
            dateFilter = component.get("v.selectedDateFilter");
            console.log('dateFilter=====172> ',dateFilter);
        } else {
            dateFilter = cachedDateFilterState;
            console.log('dateFilter=====175> ',dateFilter);
        }
        console.log('dateFilter=====177> ',dateFilter);
        component.set("v.selectedActivityFilter", selectedActivityFilter);
        this.getActivities(component, selectedActivityFilter, dateFilter);
    },
    
    dateFilterSelectHandler : function(component, event, helper) {
        var selectedDateFilter = event.getParam("value");

        window.localStorage.setItem("dateFilterState", selectedDateFilter, null);
        
        var cachedActivityFilterState = window.localStorage.getItem("activityFilterState");
        var activityFilter;
        if( !cachedActivityFilterState ){
            activityFilter = component.get("v.selectedActivityFilter");
        } else {
            activityFilter = cachedActivityFilterState;
        }
        
        component.set("v.selectedDateFilter", selectedDateFilter);

        console.log('activityFilter: ',activityFilter);
        console.log('selectedDateFilter: ',selectedDateFilter);
        
        this.getActivities(component, activityFilter, selectedDateFilter, null);
    },

    getTalentGroupTasks : function(component, event, helper){
        var selectedTalentTeam = event.getParam("value");

        var userId = $A.get("$SObjectType.CurrentUser.Id");
        console.log('userId: ',userId);
        window.localStorage.setItem(userId, selectedTalentTeam);
        //test
        var dateFilter;
        var cachedDateFilterState = window.localStorage.getItem("dateFilterState");
        if( !cachedDateFilterState ){
            dateFilter = component.get("v.selectedDateFilter");
        } else {
            dateFilter = cachedDateFilterState;
        }
        
        var cachedActivityFilterState = window.localStorage.getItem("activityFilterState");
        var activityFilter;
        if( !cachedActivityFilterState ){
            activityFilter = component.get("v.selectedActivityFilter");
        } else {
            activityFilter = cachedActivityFilterState;
        }
        var cachedActivityFilterState = window.localStorage.getItem("activityFilterState");
        var activityFilter;
        if( !cachedActivityFilterState ){
            activityFilter = component.get("v.selectedActivityFilter");
        } else {
            activityFilter = cachedActivityFilterState;
        }
        
        //end
        console.log('Talent Slected Filter: ',selectedTalentTeam);
        
        component.set("v.selectedTalentActivityFilter", selectedTalentTeam);
        console.log('selectedTalentActivityFilter: ',component.get("v.selectedTalentActivityFilter"));

        this.getActivities(component, activityFilter, dateFilter, selectedTalentTeam);
    },
    
	// Added sukesh code
    getFilterMenuItems: function(component, fieldName) {
        var action = component.get("c.getButtonMenuItems");
        action.setParams({
            "objectType": component.get("v.objInfo"),
            "field": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "All",
                        value: "All"
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                console.log('opts====>266 ',opts);
                component.set("v.actions", opts);
            }
        });
        $A.enqueueAction(action);
    }
})