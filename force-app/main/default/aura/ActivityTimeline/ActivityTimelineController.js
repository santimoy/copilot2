({
    doInit : function(component, event, helper){
        helper.getActivities(component, "All", "All", false);
		//Added sukesh code
	helper.getFilterMenuItems(component, "Type__c");
    },
    
    closeActivityTimeline : function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    expandAllEvent : function(component, event, helper){
        helper.expandAllEventHelper(component);
    },
    
    collapseAllEvent : function(component, event, helper){
        helper.collapseAllEventHelper(component);
    },
    
    handleActivityFilterSelect : function(component, event, helper) {
        helper.activityFilterSelectHandler(component, event);
    },
    
    handleDateFilterSelect : function(component, event, helper) {
        helper.dateFilterSelectHandler(component, event);
    },
    
    loadMorePastActivities : function(component, event, helper) {
        helper.loadMorePastActivitiesHelper(component);
    },

    getTalentTask:function(component, event, helper) {
        component.set("v.activityTaskTalent",!component.get("v.activityTaskTalent"));
        helper.getActivities(component, "All", "All", component.get("v.activityTaskTalent"));
    },

    handleTalentGroupTasks:function(component, event, helper){
        helper.getTalentGroupTasks(component, event, helper);
    }
})