({
    doInit: function(component, event, helper) {
        component.set('v.dashTitle', component.get('v.title'));
    },
    refreshChart: function(component, event, helper) {
        var formData = event.getParam('formData');
        var fieldValues = component.get("v.fieldName").split(",");
        if(fieldValues.length > 0){
            component.set('v.dashTitle', component.get('v.title') + ' By ' + fieldValues[1]);
        }
        
        let updateValue = false;
        if(updateValue){
            updateValue = formData.updateValue;
        }
        
        let viewName = '';
        if(formData){
            if(formData.source === 'RefreshTitle' ){
                helper.prepareFilterList(component, formData.filterTitle);
                return;
            }else if(formData.source === 'LVChart'){
                viewName=formData.viewName;
            }
        }
        var createMonth = component.get("v.createMonth");
        helper.chartChange(component, fieldValues, createMonth, viewName, updateValue);
    },
    scriptsLoaded: function(component, event, helper) {
        helper.initialLoad(component);
    },
    handleValueChange: function(component, event, helper) {
        if (event.getParam("value") === '') {
            return;
        }

        var fieldData = component.get("v.fieldName").split(",");
        var includeFilter = component.get('v.includeFilter');
        component.set('v.includeFilter', true);
        helper.maintainFilterFields(component, fieldData);
        
        var ivpCompanyListViewEvent = $A.get("e.c:IVPCompanyListViewEvent");
        if (ivpCompanyListViewEvent) {
            let value = event.getParam("value");
            if(value === $A.get("$Label.c.IVP_SD_Chart_No_Label_Text")){
                value = '';
            }
            ivpCompanyListViewEvent.setParams({
                "formData": {
                    viewName: 'chart_' + event.getParam("value"),
                    selectedValue: value,
                    source: 'Chart',
                    fieldApi: fieldData[0],
                    chkFilter: includeFilter,
                    updateValue : true
                }
            });
            ivpCompanyListViewEvent.fire();
        } else {
            console.log('CompanyListView: Please Contact to Administrator!');
        }
        component.set("v.change", "");
    },
    changeField: function(component, event, helper) {
        var fieldData = event.getParam("value");
        var fieldValues = fieldData.split(",");
        var createMonth = component.get("v.createMonth");
        component.set("v.fieldName", fieldData);
        helper.chartChange(component, fieldValues, createMonth, '' , true);

    },

    getFieldList: function(component, event, helper) {
        helper.getFieldList(component, event);
    },
    handleFilterChange: function(component, event, helper){
        component.set('v.includeFilter', !component.get('v.includeFilter'));
    },
    resetFilter: function(cmp, event, helper){
        cmp.set('v.includeFilter', false);
        cmp.set('v.filterTitle', $A.get("$Label.c.IVP_SD_Chart_No_Filters"));
        cmp.set('v.filterFields','[]');
        cmp.set('v.filterList','[]')
        cmp.set('v.dashTitle', cmp.get('v.title') );
        helper.chartChange(cmp, cmp.get('v.fieldData'), cmp.get("v.createMonth"));
        helper.handleResetFilters(cmp);
    },
})