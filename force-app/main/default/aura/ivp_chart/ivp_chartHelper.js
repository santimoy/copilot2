({
    index : 0,
    colorsList : ['#FF9C79','#FFB665','#6DEBCA','#71D9E8','#CACACA','#8D4F95','#FFE0D4','#FFE7CC','#CEF8ED','#D0F2F7','#DBDBDB','#EDCEF1','#EDEDED'],
    prepareColors:function(colorsCount){
        let colors = this.colorsList;
        colorsCount-=colors.length;
        if(colorsCount > 0){
            for(let clrInd = 0; clrInd<colorsCount;){
                let newColor = this.getRandomColor();
                if(colors.indexOf(newColor)<0){
                    colors.push(newColor);
                    clrInd++;
                }
            }
            this.colorsList = colors;
        }
    },
    getRandomColor:function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },
    chartChange: function(component, fieldData, createMonth, viewName, updateValue) {
        component.set("v.showspinner", true);
        var action = component.get("c.getData");
        component.set('v.fieldData',fieldData);
        
        let includeFilter = component.get('v.includeFilter');
        let params = {
            "objectName": component.get("v.objectName"),
            "groupByFieldName": fieldData[0],
            "whereClause": component.get("v.whereClause"),
            "createdMonth": createMonth,
            "includeFilter":includeFilter,
            "updateValue":updateValue
        };
        if(viewName){
            params.viewName = viewName;
        }
        // console.log(JSON.stringify(params));
        action.setParams(params);
        
        //action.setStorable();
        this.selectMenuItem(component, fieldData[0]);
        this.maintainUserSetting(component, fieldData[0]);
        action.setCallback(this, function(response) {
            component.set("v.showspinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.dashTitle', component.get('v.title') + ' By ' + fieldData[1]);
                var resp = response.getReturnValue();
                component.set("v.total", resp.total);
                var data = resp.recs;
                if (data != 'null') {
                    var labels = [],
                        count = [],
                        totalCount = 0,
                        customLegends = [],
                        colors = this.getColors(),
                        colorsLength = colors.length;
                    this.prepareColors(data.length);
                    colors = this.colorsList;
                    colorsLength = colors.length;
                    for (var startIndex = 0; startIndex < data.length; startIndex++) {
                        labels[startIndex] = data[startIndex].key;
                        count[startIndex] = data[startIndex].value;
                        totalCount = totalCount + parseInt(data[startIndex].value);
                        let cusLegend ={key:data[startIndex].key, 
                                        value:data[startIndex].value, 
                                        color:'grey'};
                        if(startIndex<colorsLength){
                            cusLegend.color=colors[startIndex];
                        }
                        customLegends.push(cusLegend);
                    }
                    component.set('v.legends',customLegends);
                    if (component.chart != undefined) {
                        component.chart.data.datasets[0].backgroundColor = colors;
                        component.chart.data.datasets[0].data = count;
                        component.chart.data.labels = labels;
                        component.chart.data.datasets[0].label = fieldData[1];
                        component.chart.update();
                    }
                }
            }else if (state === "INCOMPLETE") {
                this.showMessage({message:'Please refresh the page!', type:"error"});
            }else if (state === "ERROR") {
                var errors = response.getError();
                this.handleErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    
    selectMenuItem: function(cmp, fieldName){
        let fieldList = cmp.get('v.fieldList');
        let filterFields = cmp.get('v.filterFields');
        fieldList.forEach(function(field){
            if(filterFields && filterFields.indexOf(field.key) >= 0){
                field.iconName = 'utility:filterList';
            }else{
                field.iconName = '';
            }
            if(field.key === fieldName){
                field.iconName = 'utility:check';
            }
        });
        cmp.set('v.fieldList',fieldList);
    },
    getFieldList: function(component) {
        var action = component.get("c.getGroupableFields");
        action.setParams({ "sObjectName": component.get("v.objectName") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() != '') {
                    
                    var pickListItems = response.getReturnValue();
                    component.set("v.fieldList", pickListItems);
                    component.set("v.fieldName", pickListItems[0].key + "," + pickListItems[0].value);
                    
                    component.set("v.createMonth", "99");
                    this.chartChange(component, [pickListItems[0].key, pickListItems[0].value], 99);
                }
            }else{
                this.showMessage({message:'Please refresh the page!', type:"error"})
            }
        });
        $A.enqueueAction(action);
        component.set("v.showspinner", false);
    },
   
    loadChart:  function(component, hlpr) {
        component.set("v.total", " ");
        var colors = this.getColors();
        var data = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: colors,
                label: ''
            }]
        };
        this.initChartDefault(component, data, hlpr);
        /*else{
            if(self.index<5){ 
                window.setTimeout(
                    $A.getCallback(function(){
                        self.index++;
                        self.loadChart(component);
                    }),2000
                );
            }
        }*/
    },
    initChartDefault : function(component, data, hlpr){
        var ctx = document.getElementById('chart'); //component.find("chart").getElement().getContext("2d");;
        var self = this;
        if (ctx ) {
            var cntxt = ctx.getContext('2d');
            cntxt.clearRect(0, 0, ctx.width, ctx.height);
            if(component.chart){
                component.chart.destroy();
            }
            component.chart = new Chart(ctx, {
                type: component.get("v.chartType"),
                data: data,
                options: {
                    elements:{
                        arc:{
                            borderWidth:0
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    },
                    legend: {
                        position: 'right',
                        display: !component.get('v.customLegends'),
                        labels : {
                            boxWidth : 25,
                            fontSize : 10
                        }
                    },
                    animation: false,
                    maintainAspectRatio: false,
                    onClick: function(event, helper) {
                        var elements = component.chart.getElementAtEvent(event);
                        if (elements && elements.length === 1) {
                            component.set("v.change", component.chart.data.labels[elements[0]._index]);
                        }
                    }
                }
            });
            Chart.pluginService.register({
                beforeDraw: function(chart) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = 'Arial';
                    var txt = '10000';
                    var color = '#000';
                    var sidePadding = 20;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;
                    
                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
                    
                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(20 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);
                    
                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);
                    
                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;
                    
                    //Draw text in center
                    if (component.get("v.total") > 0) {
                        ctx.fillText(component.get("v.total"), centerX, centerY);
                    }
                }
            });
        }
        if(!component.chart){
            hlpr.initChartDefault(component, data);
        }
    },
    /*loadChartWithTime : function(cmp){
        var ctx = document.getElementById('chart');
        var self = this;
        var index=0;
        window.setTimeout(
            $A.getCallback(function(){
				index++;
                var setIntervalId;
                setIntervalId = setInterval(function() {
                    if(ctx){
                        if(ctx.style.cssText){
                            clearInterval(setIntervalId);
                        }else{
                            clearInterval(setIntervalId);
                            self.loadChart(cmp);
                        }
                    }
                    index++;
                    if(index == 2){
                        clearInterval(setIntervalId);
                    }
                }, 1000);
            }), 1000
        );
    },*/
    maintainFilterFields: function(cmp, fieldData){
        let filterFields = cmp.get('v.filterFields');
        if(fieldData && fieldData.length > 0){
            filterFields.push(fieldData[0]);
            cmp.set('v.filterFields', filterFields);
        }
    },
    prepareFilterList : function(cmp, filtersVal){
        let filterVal = filtersVal.replace(new RegExp(' AND ', 'g'), ' , ');
        let filters = filterVal.split(',');
        let filterList = [];
        let noValueLable = $A.get("$Label.c.IVP_SD_Chart_No_Label_Text");
        filterVal ='';
        var fInd = filters.length-1;
        var rtfound = 0;
        // console.log(JSON.stringify(filters));
        if(fInd > -1){
            let fObj = this.extractFilter(filters[fInd], noValueLable);
            if(fObj.label.trim()!=='Company Record Type'){
                rtfound=1;
                filterList.push(fObj);
                filterVal+= fObj.label.trim()+' = '+fObj.value+'\n';
            }
        }
        for(var ind=0,filInd = fInd-rtfound; ind<filInd;ind++){
            let fObj = this.extractFilter(filters[ind], noValueLable);
            
            if(fObj.label.trim()!=='Company Record Type'){
                filterVal+=  fObj.label.trim()+' = '+fObj.value+'\n';
                filterList.push(fObj);
            }
        }
        let filterValLength = filterVal.length;
        if(filterValLength > 0){
	        filterVal = filterVal.substring(0, filterValLength-1);
        }
        cmp.set('v.filterList', filterList);
        cmp.set('v.filterTitle', filterVal);
    },
    extractFilter : function(filter, noValueLable){        
        let filterValue = filter.split('=');        
        let value = filterValue[1];
        if(value === '' || value.trim() === ''){
            value = noValueLable;
        }else{
            let startInd = value.indexOf('\'');
            if( startInd === 0){
                startInd+=1;
                let endingInd = value.indexOf('\'', startInd);
                endingInd = (endingInd ===-1 ? value.length : endingInd);
                value = value.substring(startInd, endingInd);
            }
        }
        return {'label':filterValue[0],'value':value};
    },
    handleResetFilters : function(cmp){
        var ivpCompanyListViewEvent = $A.get("e.c:IVPCompanyListViewEvent");
        if (ivpCompanyListViewEvent) {
            ivpCompanyListViewEvent.setParams({
                "formData": {
                    source: 'ChartReset',
                }
            });
            ivpCompanyListViewEvent.fire();
        } else {
            console.log('CompanyListView: Please Contact to Administrator!');
        }
    },
    
    initialLoad : function(cmp){
        let params = { "sObjectName": cmp.get("v.objectName") };
        this.handleCalling(cmp, 'getInitData', params, this.initCallback, this, {});
    },
    initCallback : function(cmp, hlpr, response, callbackInfo){
        var data = response.getReturnValue();
        let selectedInd = 0;
        let selectedFieldName='';
        if(data.ivpSetting && data.ivpSetting.Dash_BreakDown_Default_field__c ){
            selectedFieldName = data.ivpSetting.Dash_BreakDown_Default_field__c.toLowerCase();
        }
        
        for(let fInd=0, len = data.fields.length; fInd<len; fInd++){
            let field = data.fields[fInd];
            if(field.key.toLowerCase() == selectedFieldName){
                selectedInd = fInd;
                break;
            }
        }            
        cmp.set("v.fieldList", data.fields);
        cmp.set("v.fieldName", data.fields[selectedInd].key + "," + data.fields[selectedInd].value);
        cmp.set("v.createMonth", "99");
        
        hlpr.loadChart(cmp, hlpr);
        hlpr.chartChange(cmp, [data.fields[selectedInd].key, data.fields[selectedInd].value], 99);
        //hlpr.loadChartWithTime(cmp);
    },
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo) {
        cmp.set("v.showspinner", true);
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            cmp.set("v.showspinner", false);
            var state = response.getState();
            if (state === "SUCCESS"){
                if(callback)
                    callback(cmp, hlpr, response, callbackInfo);
            } else if (state === "INCOMPLETE") {
                hlpr.showMessage({message:'Please refresh the page!', type:"error"});
            } else if (state === "ERROR") {
                var errors = response.getError();
                hlpr.handleErrors(errors);
            }

        })
        $A.enqueueAction(action);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    maintainUserSetting : function(cmp, fieldName){
        //  this.handleCalling(cmp, 'maintainDefautDashField', {fieldName : fieldName}, null, this, {});
    },
    getColors:function(){
        let colors = [
            "rgb(255,156,121)",
            "rgb(255,182,101)",
            "rgb(109,235,202)",
            "rgb(113,217,232)",
            "rgb(202,202,202)",
            "rgb(141,79,149)",
            "rgb(255,224,212)",
            "rgb(255,231,204)",
            "rgb(206,248,237)",
            "rgb(208,242,247)",
            "rgb(219,219,219)",
            "rgb(237,206,241)",
            "rgb(237,237,237)"
        ];
        colors = colors.concat(['blue','brown','burlywood','cadetblue','chartreuse','coral','cornflowerblue',
                               'darkkhaki','darkolivegreen','darkseagreen','darksalmon','darkviolet','deeppink',
                               'ghostwhite','gold','greenyellow','green','indigo',
                               'lightpink','lightskyblue','midnightblue','red','rosybrown','salmon','tomato','violet','yellow']);
        return colors;
    }
})