({
    keyPress: function(component, event, helper) {
        var source= event.currentTarget;
        var val= source.value;
        helper.filterList(component,val.toLowerCase());
    },
    
    keyDown: function(component, event, helper) {
        var source= event.currentTarget;
        var val= source.value;
        helper.filterList(component,val.toLowerCase());
    },
    reInit: function(component, event, helper) {
        component.set("v.initialized",false);
        helper.init(component);
    },
    initWithSelected: function(component, event, helper) {
        component.set("v.initialized",false);
        component.set("v.initWithSelect",true);
        helper.init(component);
    },
    init: function(component, event, helper) {
        helper.init(component);
    },
    
    handleClick: function(cmp, evt, hlpr) {
        hlpr.jsClick(cmp, evt, hlpr);
    },
    
    handleSelection: function(component, event, helper) {
        var item = event.currentTarget;
        helper.handleSelection(component, item);
    },
    
    handleMouseLeave: function(component, event, helper) {
        component.set('v.callOnOut', false);
        component.set("v.dropdownOver", false);
        var mainDiv = component.find('main-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        let changed = component.get('v.optChanged');
        component.set('v.callOnOut', changed);
        document.getElementById('catInput').value = "";
    },
    
    handleMouseEnter: function(component, event, helper) {
        component.set("v.dropdownOver", true);
    },
    
    handleMouseOutButton: function(cmp, evt, hlpr) {
        hlpr.jsMouseOutButton(cmp, evt, hlpr);
    },
    removeSelection: function(component, event, helper) {
        var source = event.getSource();
        var val = source.get('v.value');
        if(val){
            var item= {dataset:{value:val,selected:"true"}};
            helper.handleSelection(component, item);
        }
    },
    handleOptionOnlyClick : function(cmp, evt, hlpr) {
		hlpr.jsOptionOnlyClick(cmp, evt, hlpr);
	},
    handleMasterChange : function(cmp, evt, hlpr) {
        hlpr.jsMasterChange(cmp, evt, hlpr);
    },
    handleFocus : function(cmp, evt, hlpr) {
        hlpr.jsFocus(cmp, evt, hlpr);
    }
})