({
    init: function(component) {

        //note, we get options and set options_
        //options_ is the private version and we use this from now on.
        //this is to allow us to sort the options array before rendering
        if (component.get("v.initialized")){
            return;
        }
        component.set("v.initialized",true);
        var initWithSelect=component.get("v.initWithSelect");
        var selectedOptions = [];
        if(!initWithSelect){
            component.set("v.selectedItems",[]);
        }else{
            //selectedOptions = component.get("v.selectedItems");
            selectedOptions = component.get("v.myDashSignalPreferencesList");
        }
        //console.log('selectedOptions>>>>: '+JSON.stringify(selectedOptions));
        var options = component.get("v.options");
        var dashSignalPreferencesList = component.get("v.myDashSignalPreferencesList");
        //console.log('dashSignalPreferencesList>>>>: '+JSON.stringify(dashSignalPreferencesList));
        //console.log('options>>>>: '+JSON.stringify(options));
        options.sort(function compare(a, b) {
            if (a.value == 'All') {
                return -1;
            } else if (a.value < b.value) {
                return -1;
            }
            if (a.value > b.value) {
                return 1;
            }
            return 0;
        });
        options.forEach(function(ele){
            ele.selected = false;
        })
        var selectAll = true;
        if(!$A.util.isUndefinedOrNull(dashSignalPreferencesList) && !$A.util.isEmpty(dashSignalPreferencesList)) {
            dashSignalPreferencesList.forEach(function (pre) {
                options.forEach(function(ele){
                    if(ele.text && pre.trim() === ele.text.trim()) {
                        ele.selected = true;
                    }else{
                        selectAll = false;
                    }
                })
            })
        } else {
            options.forEach(function(ele){
                ele.selected = false;
                selectAll = false;
            })
        }

        /*options.forEach(function(ele){
            ele.selected = false;
            if(selectedOptions.indexOf(ele.value) >=0){
                ele.selected = true;
            }
        })*/
        //console.log('56 >>>>> options>>>>: '+JSON.stringify(options));
        component.set("v.options_", options);
        component.set("v.initWithSelect",false);
        component.set("v.selectAll", selectAll);
        component.set("v.noItem", options.length == 0);
        var labels = this.getSelectedLabels(component);
        var values = this.getSelectedValues(component);
        component.set("v.selectedItems",values);
        this.setInfoText(component, labels);
        
    },  
    
    setInfoText: function(component, values) {
        
        if (values.length == 0) {
            component.set("v.selectedText","");
        }else {
            component.set("v.selectedText", values.length + " option(s) selected");
        }
    },
    
    getSelectedValues: function(component){
        var options = component.get("v.options_");
        var values = [];
        var selectAll = true;
        options.forEach(function(element) {
            if (element.selected) {
                values.push(element.value);
            }else{
                selectAll = false;
            }
        });
        
        component.set("v.selectAll", selectAll);
        return values;
    },
    
    getSelectedLabels: function(component){
        var options = component.get("v.options_");
        var labels = [];
        options.forEach(function(element) {
            if (element.selected) {
                labels.push(element.label);
            }
        });
        return labels;
    },
    /*
    despatchSelectChangeEvent: function(component,values){
        var compEvent = component.getEvent("onChangeSelection");
        if(compEvent){
            compEvent.setParams({ "values": values });
	        compEvent.fire();
        }
        //component.set('v.selectedItems',values);
    },
    */
    filterList: function(cmp,val){
        var options = cmp.get("v.options_");
        var noItem = false;
        var count=0;
        var hiddenCount=0;
        options.forEach(function(element) {
            element.hide = element.label.toLowerCase().indexOf(val)<0;
            if(element.hide){
                hiddenCount++;
            }
            count++;
        });
        
        noItem = hiddenCount == count;
        cmp.set("v.options_", options);
        cmp.set("v.noItem", noItem);
    },
    handleSelection: function(cmp, item){
        if (item && item.dataset) {
            cmp.set('v.optChanged',true);
            let selectAll = cmp.get('v.selectAll');
            var value = item.dataset.value;
            var selected = item.dataset.selected;
            var options = cmp.get("v.options_");
            options.forEach(function(element) {
                if (element.value == value) {
                    element.selected = selected == "true" ? false : true;
                }
                if(element.selected === false){
                    selectAll = false;
                }
            });
            cmp.set('v.selectAll', selectAll);
            cmp.set("v.options_", options);
            var values = this.getSelectedValues(cmp);
            var labels = this.getSelectedLabels(cmp);
            cmp.set("v.selectedItems",values);
            this.setInfoText(cmp, labels);
        }
    },
    jsOptionOnlyClick : function(cmp, evt, hlpr) {
        let opVal = evt.currentTarget.dataset.value;
        //hlpr.jsClick(cmp, evt, hlpr);
        if(opVal!=undefined){
            let options_ = cmp.get('v.options_');
            let selectAll = false;
            options_.forEach(function(option){
                option.selected = false;
            })
            cmp.set('v.options_', options_);
            var values = this.getSelectedValues(cmp);
            var labels = this.getSelectedLabels(cmp);
            cmp.set("v.selectedItems",values);
            this.setInfoText(cmp, labels);
            cmp.set("v.dropdownOver", false);
            cmp.set('v.optChanged', true);
        }
    },
    jsMasterChange : function(cmp, evt, hlpr) {
        let selectAll = !cmp.get('v.selectAll');
        let options_ = cmp.get('v.options_');
        options_.forEach(function(option){
            option.selected=selectAll;
        })
        cmp.set('v.selectAll', selectAll);
        cmp.set("v.options_", options_);
        var values = this.getSelectedValues(cmp);
        var labels = this.getSelectedLabels(cmp);
        cmp.set("v.selectedItems",values);
        this.setInfoText(cmp, labels);
        cmp.set("v.dropdownOver", false);
        cmp.set('v.optChanged', true);
        hlpr.jsMouseOutButton(cmp, evt, hlpr);
	},
    jsMouseOutButton: function(cmp, evt, hlpr) {
        window.setTimeout(
            $A.getCallback(function() {
                if (cmp.isValid()) {
                    //if dropdown over, user has hovered over the dropdown, so don't close.
                    if (cmp.get("v.dropdownOver")) {
                        return;
                    }
                    var mainDiv = cmp.find('main-div');
                    $A.util.removeClass(mainDiv, 'slds-is-open');
                    let changed = cmp.get('v.optChanged');
                    document.getElementById('catInput').value = "";
                    cmp.set('v.callOnOut', changed);
                    cmp.set('v.optChanged', false);
                    hlpr.filterList(cmp,'');
                }
            }), 200
        );
    },
    jsFocus : function(cmp, evt, hlpr) {
        document.getElementById('catInput').focus();
        hlpr.jsClick(cmp, evt, hlpr);
    },
    
    jsClick: function(cmp, evt, hlpr) {
        var mainDiv = cmp.find('main-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        cmp.set('v.optChanged', false);
    },
    
})