({
    saveIFLFeedback: function(component, params) {
        var self = this;
        component.set('v.feedbackObj', {'Accurate_IQ_Score__c': '',
                                        'Why_is_score_wrong__c': '',
                                        'Comments__c': ''});
        component.set("v.isProcessed", true);
        self.handleCalling(component, 'saveIFLFeedback', params, self.thumbsCallback, self, null);
    },
    showFeedbackModal: function(component, feedbackObj) {
        component.set("v.feedbackObj", feedbackObj);
        component.set("v.isModalOpen", true);
    },
    thumbsCallback: function(component, helper, response, callbackInfo){
        var records = component.get("v.allRecords");
        var updatedRecords = [];
        /*var feedback = response.getReturnValue();
        if(feedback.Accurate_IQ_Score__c == 'Accept'){
            var success = {message: 'Thanks for accpet this company!'};
            helper.showMessage(success);
        } else if(feedback.Accurate_IQ_Score__c == 'Reject') {
            var success = {message: 'Thanks for your valuable feedback!'};
            helper.showMessage(success);
        } else if(feedback.Accurate_IQ_Score__c == 'Ignore') {
            var success = {
                title: 'Info',
                message: 'Sorry this company has been assigned to someone else, we\'ve added a new Company to your list.',
                type: 'info'
            };
            helper.showMessage(success);    
        }*/
        var feedbackResponse = response.getReturnValue();
        var feedbackCode = feedbackResponse.code;
        var feedbackData = feedbackResponse.data;
        
        if(feedbackCode == 'Accept'){
            var success = {message: 'Thanks for accepting this company!'};
            helper.showMessage(success);
            helper.doRefeshListView();
        } else if(feedbackCode == 'Reject') {
            var success = {message: 'Thanks for your valuable feedback!'};
            helper.showMessage(success);
        } else if(feedbackCode == 'Ignore') {
            if(feedbackData.iflRecords.length > 0){
                var data = feedbackData.iflRecords;
                var fieldsAndLabels = JSON.parse(feedbackData.fieldsAndLabels);
                
                for(var i in data){
                    var subFields = [];
                    
                    for (var key in fieldsAndLabels) {
                        var obj = {};
                        if (fieldsAndLabels.hasOwnProperty(key)) {
                            var val;
                            if(key.includes('.')){
                                var keyArr = key.split('.');  
                                val = data[i][keyArr[0]][keyArr[1]]
                            } else {
                                val =  data[i][key];
                            }
                            var label;
                            var type;
                            for(var j in fieldsAndLabels[key]){
                                label = j;
                                type = val ? fieldsAndLabels[key][j] : '';
                            }
                            val = val ? val : 'No '+label;
                            obj = { label: label, value: val, type: type};
                            subFields.push(obj);
                        }
                    }
                    
                    data[i].subFields = subFields;
                }
                updatedRecords = data;
            }
            
            var success = {
                title: 'Info',
                message: 'Sorry this company has been assigned to someone else, we\'ve added a new Company to your list.',
                type: 'info'
            };
            helper.showMessage(success);    
        }
        if(feedbackCode == 'Accept' || feedbackCode == 'Reject'){
            for(var i in records){
                if(records[i].Id !=feedbackData.Id){
                    updatedRecords.push(records[i]);
                }
            }
        }
        component.set('v.isIFLFound', updatedRecords.length > 0);
        component.set('v.allRecords', updatedRecords);
        component.set("v.isProcessed", false);
    },
    handleCalling : function(component, method, params, callback, helper, callbackInfo) {
        // show spinner
        //this.showSpinner(component, true);
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            //hide spinner
            //this.showSpinner(component, false);
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(component, helper, response, callbackInfo);
            } else if (state === "INCOMPLETE") {
                var errors = [{message: 'Unknown error with '+ state +' state'}];
                helper.handleErrors(errors);
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors)
                if (errors) {
                    helper.handleErrors(errors);
                } else {
                    alert("Unknown error");
                }
            }
            
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    handleRedirection : function(recordId){
        
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }else{
            window.open('/'+recordId);
        }
    },
    omit: function(arr, obj) {
        var k, acc = {};
        for (k in obj) {
            if (obj.hasOwnProperty(k) && arr.indexOf(k) < 0) {
                acc[k] = obj[k];
            }
        }
        return acc;
    },
    handleErrors : function(errors) {
        component.set("v.isProcessed", false);
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    doRefeshListView: function(){
        console.log('doRefeshListView');
        var ivpCompanyListViewEvent = $A.get("e.c:IVPLoadSMWDAppEvent");
        if(ivpCompanyListViewEvent){
            ivpCompanyListViewEvent.setParams({ 
                "formData" : {source:'IFL'}
            });
            
            ivpCompanyListViewEvent.fire();    
        }else{
            var success = {
                title: 'Info',
                message: 'CompanyListView: Please Contact to Administrator!',
                type: 'info'
            };
            helper.showMessage(success);
        }
    }
})