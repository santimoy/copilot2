({
    doInit : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) {
            component.set("v.isConsoleApp",response);
            console.log('is console app',response);
        })
        .catch(function(error) {
            console.log(error);
        });
    },
	goToObject : function(component, event, helper) {
        console.log('chbsdmhfbsvdfkhjdsbfksbadhj',component.get("v.isConsoleApp"));
        if(component.get("v.isConsoleApp")){
            console.log(component.get("v.isConsoleApp"));
            var dataSet = event.currentTarget.dataset;
            helper.handleRedirection(dataSet.recordId);
        }
    },
    handleThumbsClick : function(component, event, helper) {
        var src = event.getSource();
        var feedbackType = src.get("v.title");
        var feedbackObj = src.get("v.value");
        
        if(feedbackType == 'Accept'){
            feedbackObj.isProcessed = true;
            feedbackObj.Accurate_IQ_Score__c = feedbackType;
            var toSend = helper.omit(['Company_Evaluated__r'],feedbackObj);
            var params = {feedbackObj: toSend};
            helper.saveIFLFeedback(component, params);
        } else if(feedbackType == 'Reject'){
            helper.showFeedbackModal(component, feedbackObj);
        }
	},
    handleRejectFeedback: function(component, event, helper) {
        console.log('handleRejectFeedback');
        if(component.get("v.isSaveBtnClicked")){
           component.set("v.isSaveBtnClicked", false);        
            var feedbackObj  = component.get('v.feedbackObj');
            feedbackObj.isProcessed = true;
            feedbackObj.Accurate_IQ_Score__c = 'Reject';
            var toSend = helper.omit(['Company_Evaluated__r'],feedbackObj);
            var params = {feedbackObj: toSend};
            helper.saveIFLFeedback(component, params); 
        }
    },
    handleIconClick: function(component, event, helper) {
        var src = event.getSource();
        var btnType = src.get("v.title");
        var btnURL = src.get("v.value");
        var regex = new RegExp('^https?:\/\/');
        if (!btnURL.match(regex)) {
            btnURL = 'http://' + btnURL;
        }
        
        window.open(btnURL);
    }
})