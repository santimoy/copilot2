({
    fetchRecordsList : function(component, event, helper) {
        helper.fetchRecordsListHelper(component, event);
    },
    save : function(component, event, helper) {
        try{
            var objs = []
            var signalCategoryMapVar = component.get("v.signalCategoryMap");
            console.log(signalCategoryMapVar);
            if(!$A.util.isUndefinedOrNull(signalCategoryMapVar) && !$A.util.isEmpty(signalCategoryMapVar)){
                for(var i = 0; i < signalCategoryMapVar.length; i++){
                    for(var key in signalCategoryMapVar[i].finalMapList) {
                        if(signalCategoryMapVar[i].finalMapList[key].Value){
                            objs.push(signalCategoryMapVar[i].finalMapList[key].ExternalId);
                        }
                    }
                }
            }
            console.log(objs);
            var objString = objs.join(", ");
            console.log(objString);
            helper.saveUserPref(component, objString);
        }catch(Ex){
            console.log('Exception occured at in allSelectedChange method-->'+Ex);
        }
        /*var objs = [];
        var CheckedItem = component.find("checkboxes");
        for (var i = 0; i < CheckedItem.length; i++) {
            if (CheckedItem[i].get("v.checked") == true) {
                var text = CheckedItem[i].get("v.value");
                objs.push(text);
            }
        }
        var objString = objs.join(", ");
        helper.saveUserPref(component, objString);*/
    },
    
    
    allSelectedChange : function(c, e, h){
        try{
            var signalCategoryMapVar = c.get("v.signalCategoryMap");
            console.log(signalCategoryMapVar);
            if(!$A.util.isUndefinedOrNull(signalCategoryMapVar) && !$A.util.isEmpty(signalCategoryMapVar)){
                for(var i = 0; i < signalCategoryMapVar.length; i++){
                    for(var key in signalCategoryMapVar[i].finalMapList) {
                        signalCategoryMapVar[i].finalMapList[key].Value = true;
                    }
                }
            }
            c.set('v.signalCategoryMap',signalCategoryMapVar);
        }catch(Ex){
            console.log('Exception occured at in allSelectedChange method-->'+Ex);
        }
    },
    allDeSelectedChange : function(c, e, h){
        try{
            var signalCategoryMapVar = c.get("v.signalCategoryMap");
            console.log(signalCategoryMapVar);
            if(!$A.util.isUndefinedOrNull(signalCategoryMapVar) && !$A.util.isEmpty(signalCategoryMapVar)){
                for(var i = 0; i < signalCategoryMapVar.length; i++){
                    for(var key in signalCategoryMapVar[i].finalMapList) {
                        signalCategoryMapVar[i].finalMapList[key].Value = false;
                    }
                }
            }
            c.set('v.signalCategoryMap',signalCategoryMapVar);
        }catch(Ex){
            console.log('Exception occured at in allSelectedChange method-->'+Ex);
        }
    },
})