({
    fetchRecordsListHelper : function(component, event) {
        var action = component.get("c.fetchSignals");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS"){
                var signalPrefControllerObj = response.getReturnValue();
                console.log(signalPrefControllerObj.signalCategoryList);
                if(signalPrefControllerObj !== undefined && signalPrefControllerObj != null) {
                    this.setValues(component, signalPrefControllerObj);
                }  
            }
        });
        $A.enqueueAction(action);
    },
    
    setValues : function(component, signalPrefControllerObj) {  
        var singnalPrefController = signalPrefControllerObj.userPrefObj;
        var signalCategoryMap = signalPrefControllerObj.signalCategoryList;
        var arrayWithCategory = [];
        var arrayOfSignalCat = [];
        var cmpTypeObj = component.get("v.cmpType");
        if(singnalPrefController !== undefined && singnalPrefController != null) {
            
            let selected =[]
            if(cmpTypeObj == "Sourcing_Signal_Preferences") {
                if(singnalPrefController.Sourcing_Signal_Category_Preferences__c !== undefined && singnalPrefController.Sourcing_Signal_Category_Preferences__c != null) {
                    selected = singnalPrefController.Sourcing_Signal_Category_Preferences__c.split(', ');
                }
            } else if (cmpTypeObj == "My_Smart_Dash_Preferences") {
                if (singnalPrefController.My_Dash_Signal_Preferences__c !== undefined && singnalPrefController.My_Dash_Signal_Preferences__c != null) {
                    selected = singnalPrefController.My_Dash_Signal_Preferences__c.split(', ');
                }
            }
            var sign =[];
            selected.forEach(function(signal){
                arrayOfSignalCat = arrayOfSignalCat.concat(signal.split(','));
            })
        }
        if(signalCategoryMap !== undefined && signalCategoryMap != null) {
            var isAllPreferencesTrue = false;
            for(var j in signalCategoryMap) {
                var signalCategoryList = signalCategoryMap[j]; 
                var catObj = {};
                var finalMap = [];
                catObj.Group = j;
                catObj.finalMapList = [];
                
                for(var i in signalCategoryList) {
                    var mapOfRec = {};
                    mapOfRec["Name"] = signalCategoryList[i].Name;
                    mapOfRec["Description"] = signalCategoryList[i].Description__c;
                    mapOfRec["ExternalId"] = signalCategoryList[i].External_Id__c;
                    if(arrayOfSignalCat.includes(signalCategoryList[i].External_Id__c)) {
                        mapOfRec["Value"] = true;
                        isAllPreferencesTrue = true;
                    } else {
                        mapOfRec["Value"] = false;
                    }
                    finalMap.push(mapOfRec);
                }
                catObj.finalMapList = finalMap;
                arrayWithCategory.push(catObj);
            }
            console.log(arrayWithCategory);
            component.set("v.signalCategoryMap", arrayWithCategory);
            //console.log('isAllPreferencesTrue>>>>: '+isAllPreferencesTrue);
            if(isAllPreferencesTrue === false) {
                component.set("v.signalCategoryMap", []);
                for (var i = 0; i < arrayWithCategory.length; i++) {
                    for(var key in arrayWithCategory[i].finalMapList) {
                        arrayWithCategory[i].finalMapList[key].Value = true;
                    }
                }
                //console.log('isAllPreferencesTrue');
                //console.log(finalMap);
                component.set("v.signalCategoryMap", arrayWithCategory);
            }
        }
        component.set("v.intelligenceUserPreference", singnalPrefController);
    },
    
    saveUserPref : function(component, objString) {
        var newIntUserPreference = component.get("v.intelligenceUserPreference");
        var cmpTypeObj = component.get("v.cmpType");
        //console.log('typeOfSignalPreference>>>>: '+typeOfSignalPreference);
        if(cmpTypeObj == "Sourcing_Signal_Preferences") {
            newIntUserPreference.Sourcing_Signal_Category_Preferences__c = objString;
        } else if (cmpTypeObj == "My_Smart_Dash_Preferences") {
            newIntUserPreference.My_Dash_Signal_Preferences__c = objString;
        }
        //console.log('newIntUserPreference>>>>: '+JSON.stringify(newIntUserPreference));
        var action = component.get("c.saveUserPreference");
        action.setParams({
            "userPrefObj": newIntUserPreference,
            "cmpTypeObj" : cmpTypeObj
        });
        action.setCallback(this, function(response) {
            var success = response.getReturnValue();
            if(success == true){
               // $A.get("e.force:refreshView").fire();  
                this.toastMsg(component, event, 'The record was saved.', 'success');
            } else {
                this.toastMsg(component, event, 'There is some technical issue.', 'error');
            }
        });
        $A.enqueueAction(action); 
    },
    
    toastMsg : function (component, event, msg, type ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": msg
        });
        toastEvent.fire();
    }
})