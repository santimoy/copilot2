({
	init : function(component,event,helper){
        component.set("v.Spinner", true); 
		helper.getCol(component,event,helper,''); 
        helper.getName(component,event,helper);
    },
    refreshData : function(component, event, helper) {
        component.set("v.Spinner", true); 
        helper.getCol(component,event,helper,''); 
    },
    openModal : function(component, event, helper) {
        $A.createComponent( 'c:Configure_Columns', {
            'isReferesh':component.getReference("v.isReferesh"),
            'callFrom':'Preview'
        },function(modalComponent, status, errorMessage) {
            if (status === "SUCCESS") {
                var body = component.find( 'showConfigureModal' ).get("v.body");
                body.push(modalComponent);
                component.find( 'showConfigureModal' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
                console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
                console.log('error');
            }
        });
	},
    showJson: function(c , e ,h){
        c.set("v.isJson",!c.get("v.isJson"));
    },
    sorting : function(component, event, helper) {
        component.set("v.Spinner", true); 
		var target = event.currentTarget;
        var columnName = target.dataset.id;
        console.log(columnName);
        component.set("v.currentColumn",columnName);
        var spanTag = document.getElementById(columnName+"-span");
        var hasBoth = $A.util.hasClass(spanTag, "both");
        var hasAsc = $A.util.hasClass(spanTag, "ascSort");
        var hasDesc = $A.util.hasClass(spanTag, "descSort");
        var orderSeq =  '';
        if(hasBoth){
            $A.util.removeClass(spanTag, "both");
            $A.util.addClass(spanTag, "ascSort");
        }        
        if(hasDesc){
            $A.util.removeClass(spanTag, "descSort");
            $A.util.addClass(spanTag, "ascSort");
            orderSeq = 'ASC';
        }
        if(hasAsc){
            $A.util.removeClass(spanTag, "ascSort");
            $A.util.addClass(spanTag, "descSort");
            orderSeq = 'DESC';
        }
        component.set("v.isAscDirection",!component.get("v.isAscDirection"));
        helper.getCol(component,event,helper,columnName,orderSeq);
        
	}
})