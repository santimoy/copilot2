({
    errorMsg:'',
    getCol : function(component,event,helper,columnName,orderSeq) {
        component.set("v.isDisplay",false);
        if(component.get("v.isAscDirection")){
            orderSeq = 'ASC';
        }else{
            orderSeq = 'DESC';
        }
        this.getData(component,event,helper,columnName,orderSeq);
    },
    getData : function(component,event,helper,columnName,orderSeq) {
        //console.log('result1=='+JSON.stringify(result));
        component.set("v.Spinner", true); 
        var pageName = component.get("v.pageName");
        var sourceId = component.get("v.sourceId");
        var formattedJson = new Object();
        console.log('sourceId >>'+sourceId);
        var action = component.get("c.fetchHerokuData");
        var maxText = component.get("v.maxTextDisplay");
        if(action){
            action.setParams({pageName:pageName,pageNo:'1',sourceId:sourceId,orderByColumn:columnName,orderType:orderSeq});
            action.setCallback(this, function(response) {
                component.set("v.Spinner",false);
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.isDisplay",true);
                    var result = new Object();
                    result = response.getReturnValue();
					//console.log(component.get("v.updateStatus"));
                    if(result.errorInfo){
                        helper.toastMsg(component, event, result.errorInfo, "info");
                    }else{
                        var jsonObj = JSON.parse(result.jsonData);
                        var fieldDataIndex = {};
                        delete  jsonObj.data;  
                        if(jsonObj.error || jsonObj.Error){
                            let errorMsg = jsonObj.error ? jsonObj.error : jsonObj.Error;
                            let errorInd = errorMsg.indexOf('ERROR:');
                            let uPreInd = errorMsg.indexOf('user_preferences');
                            var dataExcpInd = errorMsg.indexOf('Hint: ');
                            var doesnot = errorMsg.indexOf('does not exist');
                            let isReload = component.get('v.isRelaod');
                            if(errorInd > -1 ){
                                if(uPreInd>-1){
                                    errorMsg = $A.get("$Label.c.IVP_Preview_Universe_Refresh_Tooltip");
                                }else if (dataExcpInd>-1){
                                    errorInd+=6;
                                    errorMsg = errorMsg.substr(errorInd,(dataExcpInd>errorInd ?dataExcpInd-2: errorMsg.length)-errorInd);
                                }else if(doesnot>-1){
                                    errorInd+=6;
                                    doesnot+=15;
                                    errorMsg = errorMsg.substr(errorInd,(doesnot>errorInd ?doesnot+15: errorMsg.length)-errorInd);
                                }
                            }
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "type":"info",
                                "message":  errorMsg
                            });
                            if(isReload === false){
                                component.set("v.Spinner", true);
                                if(isReload === false){
                                    helper.toastMsg(component, event, errorMsg, "info");
                                }
	                            helper.errorMsg=errorMsg;
                                component.set('v.isRelaod', true);
                                window.setTimeout(
                                    $A.getCallback(function() {
                                        helper.getData(component,event,helper,columnName,orderSeq);
                                    }), 500
                                );
                                return;
                            }else{
                                if(helper.errorMsg!=errorMsg)
	                                helper.toastMsg(component,event,errorMsg,"error");
                            }
                            
                        }else{
                            /*if(component.get("v.updateStatus")) {
                                helper.updateSourcingPreference(component, event);
                                component.set("v.updateStatus", false);
                            }*/
                            component.set('v.isRelaod', false);
                            component.set('v.isColnWrap', result.isColnWrap);
                            component.set('v.colnWrapMaxLength', result.colnWrapMaxLength);
                            var descriptionIndex;
                            var columnArray = [];
                            var wrapColnIndex = [];
                            var colnWrapMaxLength = result.colnWrapMaxLength;
                            var sequenceField = jsonObj.apiResponse.data_fields;
                            for(var i=0;i<result.fieldsLabel.length;i++){
                                var col = new Object();
                                col.label = result.fieldsLabel[i];
                                col.fieldName = result.fieldsApi[i];
                                col.minWidth = result.colnWidthList[i]!=undefined && result.colnWidthList[i]!=undefined !=null 
                                				? result.colnWidthList[i]+'px':'';
                                col.type ='text';
                                if(col.label.trim() == 'Company Name'){
                                    col.label = 'Name';
                                }
                                if(col.label.trim() == 'Description'){
                                    descriptionIndex = i;
                                }
                                columnArray.push(col);
                                for(var key in sequenceField) {
                                    if(sequenceField[key].includes(col.fieldName)) {
                                        fieldDataIndex[i] = key;
                                    }
                                }
                                if(result.wrapColnList !== undefined && result.wrapColnList.length > 0){
                                    for(var q=0;q<result.wrapColnList.length;q++){
                                        if(result.wrapColnList[q].trim() === result.fieldsLabel[i].trim()){
                                            wrapColnIndex.push(i);
                                        }
                                    }
                                }
                            }
                            if(descriptionIndex){
                                component.set("v.descriptionIndex",descriptionIndex);
                            }
                            component.set("v.columns",columnArray);
                            var data = new Array();
                            for(var j=0;j<result.data.length;j++){
                                var obj = new Array();
                                //for(var i=0;i<result.data[j].length;i++){
                                for(var i in fieldDataIndex){
                                    var dat = new Object();
                                    dat.title = result.data[j][fieldDataIndex[i]];
                                    dat.displayData = result.data[j][fieldDataIndex[i]];
                                    
                                    if(wrapColnIndex !== undefined && wrapColnIndex.length > 0){
                                        var flag = 0;
                                        for(var q=0;q<wrapColnIndex.length;q++){
                                            var dLength = result.data[j][fieldDataIndex[i]].trim().length;
                                            if(wrapColnIndex[q] == i){
                                                flag = 1;
                                                if(colnWrapMaxLength!=undefined){
                                                    if(dLength>colnWrapMaxLength){
                                                        dat.displayData = result.data[j][fieldDataIndex[i]].slice(0,colnWrapMaxLength)+'...';
                                                    } else {
                                                        dat.displayData = result.data[j][fieldDataIndex[i]];
                                                    }
                                                } else {
                                                    dat.displayData = result.data[j][fieldDataIndex[i]];
                                                }
                                            }else{
                                                if(dLength > maxText && flag === 0){
                                                    dat.displayData = result.data[j][fieldDataIndex[i]].slice(0,maxText)+'...';
                                                }
                                            }
                                        }
                                    }else{
                                        if(result.data[j][fieldDataIndex[i]].length > maxText){
                                            dat.displayData = result.data[j][fieldDataIndex[i]].slice(0,maxText)+'...';
                                        }
                                    }
                                    
                                    /*if(result.data[j][i].length > maxText){
                                        dat.displayData = result.data[j][i].slice(0,maxText)+'...';
                                    }*/
                                    obj.push(dat); 
                                }
                                data.push(obj);
                            }
                            component.set("v.data",data);
                            if(result.data.length == 0){
                                component.set("v.noData",true);
                                //component.set("v.injectEditComponent",true);
                                //helper.toastMsg(component,event, 'No preview records are available for this record.', 'error');
                            }
                            
                        }
                        component.set("v.totalRecords",jsonObj.recordsTotal);
                        var jsonBeauty =  JSON.stringify(jsonObj,null,4);
                        component.set("v.formattedJson",jsonBeauty);
                    }
                    //console.log(result);
                    
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Unknown error"+errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
	},
    getName : function(component,event,helper){
        var sourceId = component.get("v.sourceId");
        if(sourceId !== undefined && sourceId != null && sourceId != ''){
            console.log('sourceId=='+sourceId);
            var action = component.get("c.fetchRecName");
            action.setParams({recId:sourceId});
            action.setCallback(this, function(response) {
                var state = response.getState();
                component.set("v.Spinner", false); 
                if (state === "SUCCESS") {
                     var result = response.getReturnValue();
                    if(result !== undefined && result != null){
                        component.set("v.recName",result.Name__c);
                        /*if(result.Validation_Status__c == 'Valid') {
                            component.set("v.updateStatus", false);
                        }*/
                    }
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Unknown error"+errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    updateSourcingPreference : function(component, event) {
		var action = component.get("c.updatePreference");
        action.setParams({
            "externalId" : component.get("v.sourceId")
        });
        action.setCallback(this, function(response) {
            
        });
        $A.enqueueAction(action);
    },
    
    toastMsg : function (c, e, msg, type ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": msg,
        });
        // we don't need stickness  
        // "mode": 'sticky'
        toastEvent.fire();
    },
})