({
    "getActivityHistory" : function(cmp) {
        var action = cmp.get("c.SelectTalentTasksForAccount");
        action.setParams({ "AccountId" : cmp.get("v.recordId") });

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('SUCCESS: ' + response);
            }
            else if (state === "INCOMPLETE") {
                console.log('INCOMPLETE: ' + response);
            }
            else if (state === "ERROR") {
                console.log('ERROR: ' + response);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})