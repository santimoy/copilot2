({
    navigate  : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": 'https://insightpartners.force.com/intranet/s/compliance'
        });
        urlEvent.fire();
    },

    navigate1  : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": 'https://insightpartners.force.com/intranet/s/human-resources'
        });
        urlEvent.fire();
    },

    navigate2  : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": 'https://insightpartners.force.com/intranet/s/marketing'
        });
        urlEvent.fire();
    },

    navigate3  : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": 'https://insightpartners.force.com/intranet/s/information-technology'
        });
        urlEvent.fire();
    }
})