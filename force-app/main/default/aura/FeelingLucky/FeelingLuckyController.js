({
	doInit : function(component, event, helper) {
        helper.doInit(component);
        helper.doCheckIsUserAdmin(component);
    },
    handleRefreshAction :  function(component, event, helper) {
        helper.doRefresh(component);
    }
})