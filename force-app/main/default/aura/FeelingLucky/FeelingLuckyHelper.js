({
    doInit : function(component) {
        var self = this;
        self.handleCalling(component, 'getFeelingLuckyList', {}, self.initCallback, self, null);
    },
    doCheckIsUserAdmin : function(component){
        var self = this;
        self.handleCalling(component, 'isUserSystemAdmin', {}, self.checkIsUserAdminCallback, self, null);
    },
    checkIsUserAdminCallback: function(component, helper, response, callbackInfo){
		component.set("v.isUserAdmin", response.getReturnValue());
    },
    doRefresh : function(component) {
        var self = this;
        self.handleCalling(component, 'getRefreshedFeelingLuckyList', {}, null, self, null);
    },
    initCallback : function(component, helper, response, callbackInfo){
        
        var records = response.getReturnValue();
        var feedbackPicklist = records.feedbackPicklist;
        var data = records.iflRecords;
        var fieldsAndLabels = JSON.parse(records.fieldsAndLabels);
        console.log('--=>', data.length);
        if(data && data.length == 0 ){
            component.set('v.isIFLFound', false);
        }
        for(var i in data){
            var subFields = [];
            
            for (var key in fieldsAndLabels) {
                var obj = {};
                if (fieldsAndLabels.hasOwnProperty(key)) {
                    var val;
                    if(key.includes('.')){
                        var keyArr = key.split('.');  
                        val = data[i][keyArr[0]][keyArr[1]]
                    } else {
                        val =  data[i][key];
                    }
                    var label;
                    var type;
                    for(var j in fieldsAndLabels[key]){
                        label = j;
                        type = val ? fieldsAndLabels[key][j] : '';
                    }
                    val = val ? val : 'No '+label;
                    obj = { label: label, value: val, type: type};
                    subFields.push(obj);
                }
            }
            
            data[i].subFields = subFields;
        }
        
        var feedbackPicklistOpts = [];
        for(var key in feedbackPicklist){
            feedbackPicklistOpts.push({label: feedbackPicklist[key], value:key});
        }

        console.log('feedbackPicklistOpts>>>: '+JSON.stringify(feedbackPicklistOpts));
        console.log('data>>>: '+JSON.stringify(data));
        console.log('FeelingLuckyHelper isIFLFound>>>: '+component.get('v.isIFLFound'));

        component.set("v.feedbackPicklist",feedbackPicklistOpts);
        component.set("v.records", data);
    },
    handleCalling : function(component, method, params, callback, helper, callbackInfo) {
        // show spinner
        //this.showSpinner(component, true);
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            //hide spinner
            //this.showSpinner(component, false);
            var state = response.getState();
            if (state === "SUCCESS"){
                if(callback != null){
                    // passing response to a callback method
                    callback(component, helper, response, callbackInfo);
                }
                
            }
            else if (state === "INCOMPLETE") {
                //hlpr.showMessage({message:'Cannot laod!', type:"error"});
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors)
                if (errors) {
                    helper.handleErrors(errors);
                } else {
                    alert("Unknown error");
                }
            }
            
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    showSpinner : function(component, showSpinner) {
        var spinner = component.find("spinner");
        if(showSpinner){
            $A.util.removeClass(spinner, "slds-hide"); 
        } else {
            $A.util.addClass(spinner, "slds-hide");
        }
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    }
})