({
	doInit : function(component, event, helper) {
		helper.prepareRows(component);
    },
	handleIVPLoadSMWDAppEvent : function(component, event, helper) {
        helper.handleLoadEvent(component, event);
    }
    
})