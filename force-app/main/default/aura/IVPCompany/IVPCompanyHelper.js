({
	loadListView : function(cmp, formData){
        cmp.set("v.selectedView", '');
        cmp.set('v.selectedView', formData.viewName);
    },
    
    handleLoadEvent: function(cmp, evt){
		
		var oldSelectedView = cmp.get("v.selectedView");
		this.prepareRows(cmp);
        cmp.set("v.selectedView", '');
        var formData = evt.getParam('formData');
        if(formData ){
        	this.resetTagFilter(cmp, formData);
            if(formData.source === 'ListView'){
                this.loadListView(cmp, formData);
                
                if(formData && formData.viewName && formData.viewName.indexOf('ivp_sd_chart')==0){
                    this.refreshChart(formData);
                }
            }else{
                cmp.set("v.selectedView", oldSelectedView);
                
                this.refreshChart();
            }
        }
    },
    prepareRows : function(cmp){
    	var rowCount = $A.get("$Label.c.IVP_SD_LIST_Record_Count");
		if( rowCount !== '$Label.c.IVP_SD_LIST_Record_Count does not exist.' && !isNaN(rowCount)){
			cmp.set('v.rowCount',parseInt(rowCount));
		}else{
			cmp.set('v.rowCount', 50);
		}
    },
    refreshChart : function(params){
        
        var appEvent = $A.get("e.c:IVP_Chart_Refresh");
        if(appEvent){
            if(params){
                params.source='LVChart';
                appEvent.setParams({ 
                    "formData" : params
                });
            }
            appEvent.fire();
        }else{
            console.log('IVP_Chart_Refresh not found');
        }
    },
    resetTagFilter : function(cmp, data){
        if(data.source && data.source === 'IFL'){
        	return;
        }
        var ivpCompanyListViewEvent = $A.get("e.c:IVPTaggingAppEvent");
        if(ivpCompanyListViewEvent){
            ivpCompanyListViewEvent.setParams({ 
                "formData" : {source:'change', selectedLV:data}
            });
            ivpCompanyListViewEvent.fire();
        }
    }
})