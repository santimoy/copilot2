({
    doInit : function(component, event, helper) {
        helper.doInit(component, helper);
  },
    hideInputBox : function(component, event, helper) {
        component.set("v.reason","");
        component.set("v.isAcceptPopoverId","");
        component.set("v.isRejectPopoverId","");
        component.set("v.isAccept",false);
        component.set("v.isReject",false);
  },
    showAcceptPopover : function(component, event, helper) {
        // unhighlight utility panel
        helper.setUtilityHighlighted(component, false);
        helper.setFocus(component, 'input');
        component.set("v.reason","");
    component.set("v.isAcceptPopoverId", event.target.dataset.name);
        component.set("v.isAccept",true);
        component.set("v.isReject",false);
  },
    /*hideAcceptPopover : function(component, event, helper) {
    component.set("v.isAcceptPopoverId","");
        component.set("v.isAccept",false);
        component.set("v.isReject",false);
  },*/
    showRejectPopover : function(component, event, helper) {
        // unhighlight utility panel
        helper.setUtilityHighlighted(component, false);
        helper.setFocus(component, 'input');
        component.set("v.reason","");
    component.set("v.isRejectPopoverId", event.target.dataset.name);
        component.set("v.isReject",true);
        component.set("v.isAccept",false);
  },
    /*hideRejectPopover : function(component, event, helper) {
    component.set("v.isRejectPopoverId","");
        component.set("v.isReject",false);
        component.set("v.isAccept",false);
  },*/
    onAccept : function(component, event, helper) {
        var note = component.get("v.reason");//document.getElementById('accept').value;
        helper.checkInput(component, helper);
        if(component.get("v.isChecked")){
            helper.changeRequestStatus(component, event, helper, 'Accepted',component.get('v.isAcceptPopoverId'), note);
        }
        component.set("v.isAcceptPopoverId","");
  },
    onReject : function(component, event, helper) {
        var note = component.get("v.reason");//document.getElementById('reject').value;
        helper.checkInput(component, helper);
        if(component.get("v.isChecked")){
            helper.changeRequestStatus(component, event, helper, 'Rejected', component.get('v.isRejectPopoverId'), note);
        }
        component.set("v.isRejectPopoverId","");
    },
    navigateToSObject : function(component, event, helper) {
        // unhighlight utility panel
        helper.setUtilityHighlighted(component, false);
    var navService = component.find("navService");
        var defaultUrl = "#";
        navService.generateUrl({"type": "standard__recordPage",
                                "attributes": {
                                    "recordId": event.target.dataset.name,
                                    "actionName": "view"
                                }})
        .then($A.getCallback(function(url) {
            window.open(url);
        }), $A.getCallback(function(error) {
            alert(error);
        }));
    },
    handleMessage : function(component, event, helper) {
    var param = event.getParam('message');
        if(param){
            helper.getCompanyRequest(component, helper, param);
        }
  },
    handleOnselect : function(component, event, helper) {
        // unhighlight utility panel
        helper.setUtilityHighlighted(component, false);
    },
    keyCheck : function(component, event, helper){
        if (event.which == 13){
            helper.checkInput(component, helper);
            if(component.get("v.isChecked")){
                var isReject = component.get("v.isReject");
                var isAccept = component.get("v.isAccept");
                var note = component.get("v.reason");
                if(isReject){
                    helper.changeRequestStatus(component, event, helper, 'Rejected', component.get('v.isRejectPopoverId'), note);
                }else if(isAccept){
                    helper.changeRequestStatus(component, event, helper, 'Accepted', component.get('v.isAcceptPopoverId'), note);
                }
            }  
        }
    },
    refresh : function(component, event, helper) {
        component.set("v.inboundCompanyRequests", []);
        component.set("v.outboundCompanyRequests", []);
        component.set("v.isRefresh", true);
        helper.doInit(component, helper);
    },
})