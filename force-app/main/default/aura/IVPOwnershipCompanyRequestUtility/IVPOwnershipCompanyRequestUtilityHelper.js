({
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo, errorCB) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                if(callback){
                    callback(cmp, hlpr, response, callbackInfo);
                }
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({title:'INCOMPLETE', message:'Cannot laod!', type:"error"});
            }else if (state === "ERROR") {
                var err = response.getError();
                hlpr.handleErrors(err);
                if(errorCB){
                    errorCB(cmp, hlpr, response, callbackInfo);
                }
            }
            
        });
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    showMessage : function(message){

        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            if(errors){
                if(errors.length>0 && errors[0]!=undefined && errors[0].pageErrors === undefined && errors[0].fieldErrors === undefined){
          toastParams.message = errors[0].message;
                }else if(errors[0] && errors[0].pageErrors && errors[0].pageErrors.length>0){
                    toastParams.message = errors[0].pageErrors[0].message;
                }else if(errors[0] && errors[0].fieldErrors){
                    toastParams.message = this.prepareFieldErrorMsg(errors[0].fieldErrors);
                }
            }else{
                alert('Unknown error');
            }
        }
        this.triggerToast(toastParams);
    },
    prepareFieldErrorMsg : function(fieldErrors){
        var keys = Object.keys(fieldErrors);
        var msg = 'Unknown error';
        if(keys.length > 0){
            msg='';
            for( var ind in keys){
                var fErrors = fieldErrors[keys[ind]];
                fErrors.forEach(function(fErr){
                    msg+= fErr.message+' \n';
                })
                
            }
        }
        return msg;
    },
    doInit : function(cmp, hlpr){
        cmp.set("v.showSpinner", true);
        hlpr.handleCalling(cmp, 'getByPassValue', {} , hlpr.setUtilityPanel, hlpr, null);
        hlpr.getCompanyRequests(cmp, hlpr);
    },
    /************
    * Checking if the user having bypass and if yes then we are getting the value and 
          setting it onto the attribute value
  ************/
    setUtilityPanel:function(cmp, hlpr, response, callbackInfo){
        var byPassOnUtility = response.getReturnValue();
        console.log('byPassOnUtility',byPassOnUtility);
        cmp.set('v.byPassOnUtility',byPassOnUtility);
    },
    //We don't need it
    /*getHolidays: function(cmp, hlpr){
        hlpr.handleCalling(cmp, 'getHolidays', {} , hlpr.getHolidaysCB, hlpr, null, hlpr.getHolidaysErrCB);
    },
    getHolidaysCB : function(cmp, hlpr, response, callbackInfo){
        cmp.set('v.holidays', response.getReturnValue());
        hlpr.getCompanyRequests(cmp, hlpr);
    },*/
    getCompanyRequests: function(cmp, hlpr){
        hlpr.handleCalling(cmp, 'getCompanyRequests', {} , hlpr.setData, hlpr, null);
    },
    setData : function(cmp, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        cmp.set("v.validationMap",validationMap);
        hlpr.setInboundAndOutboundData(cmp, hlpr, validationMap.requests, 'created');
        hlpr.getRemainingBalance(cmp, hlpr);
        cmp.set("v.isRefresh", false);
        hlpr.setFocus(cmp, 'tab2');
    },
    getRemainingBalance: function(cmp, hlpr){
        hlpr.handleCalling(cmp, 'getRemainingBalance', {} , hlpr.getRemainingBalanceCB, hlpr, null);
    },
    getRemainingBalanceCB : function(cmp, hlpr, response, callbackInfo){
        var validationWrapper = response.getReturnValue();
        var requestBalanceDetail = $A.get('$Label.c.IVP_Ownership_Remaining_Request_Balance')
        var requestBalanceDateDetail = $A.get('$Label.c.IVP_Ownership_Request_Balance_Update_Message')
        if(requestBalanceDetail.indexOf('{!n}') > -1){
            requestBalanceDetail = requestBalanceDetail.replace('{!n}', ' <b> '+validationWrapper.requestBalance+'</b> ');
        }else{
            requestBalanceDetail += ' ' + validationWrapper.requestBalance;
        }
        //may be we need in future
        /*if(requestBalanceDetail.indexOf('{!days}') > -1){
            requestBalanceDetail = requestBalanceDetail.replace('{!days}', ' <b> '+validationWrapper.ownershipRequestBalanceDays+'</b> ');
        }else{
            requestBalanceDetail += ' ' + validationWrapper.requestBalance;
        }
        if(requestBalanceDateDetail.indexOf('{!n}') > -1){
            requestBalanceDateDetail = requestBalanceDateDetail.replace('{!n}', ' <b> '+validationWrapper.ownershipRequestBalance+'</b> ');
        }else{
            requestBalanceDateDetail += ' ' + validationWrapper.requestBalanceUpdateDate;
        }*/
        if(requestBalanceDateDetail.indexOf('{!date}') > -1){
            requestBalanceDateDetail = requestBalanceDateDetail.replace('{!date}', ' <b> '+validationWrapper.requestBalanceUpdateDate+'<b/> ');
        }else{
            requestBalanceDateDetail += ' ' + validationWrapper.requestBalanceUpdateDate;
        }
        cmp.set('v.requestBalanceDetail', requestBalanceDetail);
        cmp.set('v.requestBalanceDateDetail', requestBalanceDateDetail);
        cmp.set("v.validationWrapper", validationWrapper);
        cmp.set("v.showSpinner", false);
    },
    setInboundAndOutboundData : function(cmp, hlpr, companyRequests, status){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var inboundCompanyRequests = cmp.get("v.inboundCompanyRequests");
        var outboundCompanyRequests = cmp.get("v.outboundCompanyRequests");
        var today = new Date();
        
        var holiDates = [];
        var holidays = cmp.get('v.holidays');
        holidays.forEach(function(holiday){
          holiDates.push(holiday.ActivityDate);
        })
        
        var requests = [];
        hlpr.prepareRequestData(companyRequests, requests, holiDates);
        hlpr.prepareInboundOutboundData(cmp, hlpr, status, requests, inboundCompanyRequests, outboundCompanyRequests);
        inboundCompanyRequests = inboundCompanyRequests.sort(function(a, b) {
            return new Date(b.CreatedDate).getTime() < new Date(a.CreatedDate).getTime() ?  -1 : 1;
        });
        outboundCompanyRequests = outboundCompanyRequests.sort(function(a, b) {
            return new Date(b.CreatedDate).getTime() < new Date(a.CreatedDate).getTime() ?  -1 : 1;
        });
        inboundCompanyRequests = inboundCompanyRequests.sort(function(a, b) {
            return (a.isPending ? ( b.isPending ? a.hours - b.hours : -1):0);
            //new Date(b.CreatedDate).getTime() > new Date(a.CreatedDate).getTime() ? (a.isPending ? (a.hours > b.hours ? 1 : -1) : 1) : (a.isPending ? (a.hours > b.hours ? 1 : -1) : 1);
        });
        outboundCompanyRequests = outboundCompanyRequests.sort(function(a, b) {
            return (a.isPending ? ( b.isPending ? a.hours - b.hours : -1):0);
            //new Date(b.CreatedDate).getTime() > new Date(a.CreatedDate).getTime() ? (a.isPending ? (a.hours > b.hours ? 1 : -1) : 1) : (a.isPending ? (a.hours > b.hours ? 1 : -1) : 1);
        });
        cmp.set("v.inboundCompanyRequests", inboundCompanyRequests);
        cmp.set("v.outboundCompanyRequests", outboundCompanyRequests);
    },
    prepareRequestData : function(companyRequests, requests, holiDates){
        var nowDate = new Date();
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        companyRequests.forEach(function(request){
            var rDate = new Date(request.Request_Expire_On__c);
          request.hours = request.Request_Processing_Hours__c;
            
            /*var tempStartDate = new Date(nowDate);
            var hours = (rDate - nowDate) / 36e5;
            var isReverse = hours < 0;
            hours = Math.abs(hours);
          for(;tempStartDate < rDate;){
            var tDate = tempStartDate.getFullYear()+'-'+(tempStartDate.getMonth()+1)+'-'+ ((tempStartDate.getDate()<10 ? '0':'')+tempStartDate.getDate());
            var tempDateDayFormat = days[tempStartDate.getDay()];
            var isIterate = true;
                do{
                    isIterate = false;
                    if(tempDateDayFormat == 'Sat' || tempDateDayFormat == 'Sun' || holiDates.indexOf(tDate)>-1){
                tempStartDate.setDate(tempStartDate.getDate()+1);
                tDate = tempStartDate.getFullYear()+'-'+(tempStartDate.getMonth()+1)+'-'+ ((tempStartDate.getDate()<10 ? '0':'')+tempStartDate.getDate());
                tempDateDayFormat = days[tempStartDate.getDay()];
                request.hours-=24;
                        isIterate = true;
                    }else{
                        tempStartDate.setDate(tempStartDate.getDate()+1);
                    }
                } while(isIterate);
            }*/
            request.hours = Math.round(request.hours);
            requests.push(request);
        })
    },
    prepareInboundOutboundData : function(cmp, hlpr, status, requests, inboundCompanyRequests, outboundCompanyRequests){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var pendingInboundCnt = cmp.get("v.pendingInboundCompanyRequests");
        var pendingOutboundCnt = cmp.get("v.pendingOutboundCompanyRequests");
        for(var i in requests){
            var companyRequest = requests[i];
            companyRequest['createDate'] = hlpr.changeFormat(companyRequest.CreatedDate);
            companyRequest['isPending'] = false;
            if(companyRequest.Request_By__c == userId){
                if(companyRequest.Status__c == 'Pending'){
                    companyRequest['timeLeft'] = companyRequest.hours + ' hours';
                    companyRequest['isPending'] = true;
                    pendingOutboundCnt += 1;
                }
                if(status == 'created'){
                    outboundCompanyRequests.push(companyRequest);
                }else{
                    pendingOutboundCnt -= 1;
                    var isAdded = hlpr.maintainRequestlist (outboundCompanyRequests, companyRequest);
                    
                    if(!isAdded){
                        outboundCompanyRequests.push(companyRequest);
                        hlpr.spiceRequestlist(inboundCompanyRequests, companyRequest);
                    }
                }
            }else{
                if(companyRequest.Status__c == 'Pending'){
                    companyRequest['timeLeft'] = companyRequest.hours + ' hours';
                    companyRequest['isPending'] = true;
                    pendingInboundCnt += 1;
                }
                if(status == 'created'){
                    inboundCompanyRequests.push(companyRequest);
                }else{
                    pendingInboundCnt -= 1;
                    var isAdded = hlpr.maintainRequestlist (inboundCompanyRequests, companyRequest);
                    
                    if(!isAdded){
                        inboundCompanyRequests.push(companyRequest);
                        hlpr.spiceRequestlist(outboundCompanyRequests, companyRequest);
                    }
                }
            }
        }
        if(pendingInboundCnt > 0){
            hlpr.setUtilityHighlighted(cmp, true);
        }else{
            hlpr.setUtilityHighlighted(cmp, false);
        }
        cmp.set("v.pendingInboundCompanyRequests", (pendingInboundCnt < 0 ? 0 :pendingInboundCnt));
        cmp.set("v.pendingOutboundCompanyRequests", (pendingOutboundCnt < 0 ? 0 :pendingOutboundCnt));
    },
    changeFormat : function(createDate){
        var d = new Date(createDate);
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var strDate = (d.getMonth()+1) + '/' + d.getDate() + '/' + d.getFullYear();
        var strTime = (hours>12 ? hours-12 : hours) + ':' + (minutes.toString().length == 1 ? '0'+minutes : minutes) + (hours>=12 ? 'pm' : 'am');
        return strDate + ' ' + strTime;
    },
    maintainRequestlist : function(requests, request){
        for(var i in requests){
            var cmpRqst = requests[i];
            if(cmpRqst.Id == request.Id){
                requests[i] = request;
                return true;
            }
        }
        return false;
    },
    spiceRequestlist : function(requests, request){
        for(var i in requests){
            var cmpRqst = requests[i];
            if(cmpRqst.Id == request.Id){
                requests.splice(i, 1);
                break;
            }
        }
    },
    changeRequestStatus : function(cmp, evt, hlpr, status, recId, note){
        cmp.set("v.showSpinner", true);
        cmp.set("v.isAccept",false);
        cmp.set("v.isReject",false);
        var inboundCompanyRequests = cmp.get('v.inboundCompanyRequests');
        for(var ind=0, len=inboundCompanyRequests.length; ind<len; ind++){
            if(inboundCompanyRequests[ind].Id==recId){
                recId = inboundCompanyRequests[ind].Company__c;
                break;
            }
        }
        var params={recordId:recId, status:status, note:note};
        hlpr.handleCalling(cmp, 'changeRequestStatus', params,
                           hlpr.changeRequestStatusCB, hlpr, 
                           params,
                           hlpr.changeRequestStatusErrorCB);
    },
    changeRequestStatusCB : function(cmp, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        cmp.set("v.validationMap",validationMap);
        var toastParams = {};
        if(validationMap.errorMsg != ''){
            toastParams = {
                title: "Validation",
                message: validationMap.errorMsg,
                type: "error"
            };
        }else if(validationMap.successMsg != ''){
            toastParams = {
                title: "Success",
                message: validationMap.successMsg,
                type: "success"
            };
        }
        hlpr.triggerToast(toastParams);
        cmp.set("v.showSpinner",false);
        cmp.set("v.isChecked",false);
    },
    changeRequestStatusErrorCB : function(cmp, hlpr, response, callbackInfo){
        cmp.set("v.showSpinner",false);
    },
    getCompanyRequest : function(cmp, hlpr, companyRequest){
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        if(companyRequest.sobject.Request_By__c == userId || companyRequest.sobject.Request_To__c == userId){
            cmp.set("v.showSpinner", true);
            $A.get("e.force:refreshView").fire();
            hlpr.handleCalling(cmp, 'getCompanyRequest', {recordId:companyRequest.sobject.Id} , hlpr.getCompanyRequestCB, hlpr, companyRequest.event);
        }
    },
    getCompanyRequestCB : function(cmp, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        cmp.set("v.validationMap",validationMap);
        hlpr.setInboundAndOutboundData(cmp, hlpr, validationMap.requests, callbackInfo.type);
        hlpr.getRemainingBalance(cmp, hlpr);
    },
    setUtilityHighlighted : function(cmp, isHighlight) {
        var utilityAPI = cmp.find("utilitybar");
        utilityAPI.setUtilityHighlighted({
            highlighted: isHighlight
        });
    },
    checkInput : function(cmp, hlpr) {
        var isRequired = cmp.find('input').get("v.required");
        var reason = cmp.get("v.reason");
        if (!isRequired || (isRequired && reason != '')) {
            cmp.set("v.isChecked",true);
        } else {
            var element = cmp.find('input');
            if(element){
                element.reportValidity();
            }else{
                hlpr.showMessage({
                    title: "Validation",
                    message: "Complete this field.",
                    type: "error"
                })
            }
        }
  },
    setFocus : function(cmp, eleId) {
        window.setTimeout(
            $A.getCallback(function() {
                var element = cmp.find(eleId);
                if(element){
                    element.focus();
                }
            }), 200
        );
    },
})