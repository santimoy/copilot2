({
    doInit: function(cmp, event, helper) {
	    console.log('DoInit isIFLLoad>>>: '+cmp.get('v.isIFLLoad'));
	    cmp.set('v.hidePanel',true);
        document.title = "Just For You";
        helper.handleShowPanel(cmp);
        helper.handleKeyPress(cmp);
        helper.setDefaultColor(cmp);
		helper.initializeJustForYou_Helper(cmp, event, helper, 'JustForYouCompanies');
		helper.isOnLoad = true;
    },
	closeNav : function(cmp, event, helper) {
		helper.handleOpenPanel(cmp,false, helper);
	},
    openNav : function(cmp, event, helper) {
        helper.handleOpenPanel(cmp, true, helper);
	},

	handleisRefreshJustForYouCompanies : function (cmp, event, helper) {
    	let isRefreshJustForYouCompanies = cmp.get('v.isRefreshJustForYouCompanies');
    	if(isRefreshJustForYouCompanies === true) {
		    helper.initializeJustForYou_Helper(cmp, event, helper, 'JustForYouCompanies');
	    }
	}
})