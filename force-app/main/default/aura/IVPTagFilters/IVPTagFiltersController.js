({
    scriptsLoaded : function(component, event, helper) {
        var funType = typeof $.fn.fastselect;
        if(funType !=='function' || funType=='undefined'){
            location.reload();
            return;
        }
        helper.doInit(component);
    },
    handleFilterChange : function(component, event, helper) {
    	
        component.set('v.chkFilter',!component.get('v.chkFilter'));
    },
    handleTypeChange: function(component, event, helper) {
    	helper.handleTypeChange(component, event);
    },
    handleClearTag: function(component, event, helper) {
        //helper.resetPicklist(component);
    },
    handleIVPTaggingAppEvent: function(component, event, helper) {
        var formData = event.getParam('formData');
        if(formData ){
        	if(formData.source == 'list'){
                //helper.resetPicklist(component);
        	}else if(formData.source == 'change'){
        	    var userId = helper.getCurrentUserId();
        	    
                if(formData.selectedLV){
        		    if(formData.selectedLV.viewName.indexOf(userId) === -1){
            		    
                        component.set('v.selectedLV', formData.selectedLV);
    	        	}
        		}
        	}
        }
    }
})