({
	doInit : function(cmp) {
        cmp.set('v.isProcessed', true);
        
        this.handleCalling(cmp, 'getInitData', {}, this.initCallback, this, null);
        this.handleSpinner(cmp, false) ;
    },
    
    initCallback : function(cmp, hlpr, response, callbackInfo){
    	
        var topicsMap = response.getReturnValue();
        
       	var opts = [{ "class": "optionClass", 
                       label: '--None--', 
                       value: ''}];
		var topics = topicsMap['lstOfTopics'];
		var topicsOther = topicsMap['lstOfTopicsOthers'];
        var optss = [];
        var groupList = [];
        var groupName = $A.get("$Label.c.IVP_SD_Tag_DD_My_Group");
        if(groupName =='$Label.c.IVP_SD_Tag_DD_My_Group does not exist.'){
        	groupName = 'My';
        }
        
        groupList.push({groupName:groupName, groupId:1});
        groupName = $A.get("$Label.c.IVP_SD_Tag_DD_All_Group");
        if(groupName =='$Label.c.IVP_SD_Tag_DD_All_Group does not exist.'){
        	groupName = 'My';
        }
        groupList.push({groupName:groupName, groupId:2});
        var groupListIndex = 0;
        var topicCount = 0;
        var myOptions = [];
        var allOptions = [];

        if(topics){
            for(var index =0, length=topics.length; index<length; index++){
                var topic = topics[index];
                if(topicCount<50){
                    myOptions.push({value:topic.Id, text:topic.Name});
                }else{
                    allOptions.push({value:topic.Id, text:topic.Name});
                }
                
	            opts.push({ "class": "optionClass", 
	                       label: topic.Name, 
	                       value: topic.Id});
               
                optss.push({id:topic.Id, groupName : groupList[groupListIndex].groupName,
                			groupId: groupList[groupListIndex].groupId,
                       		name :topic.Name})
           
	           	if(++topicCount == 50){
					groupListIndex = 1;
	           	}
	        }
        }
        if(topicsOther){
        	groupListIndex = 1;
        	for(var index =0, length=topicsOther.length; index<length; index++){
	            var topic = topicsOther[index];
                allOptions.push({value:topic.Id, text:topic.Name});
	            opts.push({ "class": "optionClass", 
	                       label: topic.Name, 
	                       value: topic.Id});
               
                optss.push({id:topic.Id, groupName : groupList[groupListIndex].groupName,
                			groupId: groupList[groupListIndex].groupId,
                       		name :topic.Name})
	        }
        }
        cmp.set('v.myOptions',myOptions);
        cmp.set('v.allOptions',allOptions);
        //cmp.set('v.tags', optss);
		cmp.set('v.isProcessed', false);
        cmp.set('v.showTag', true);
        var tagoptions = document.getElementById('tagoptions');
        tagoptions.className = tagoptions.className.replace('slds-hide','');
        if(tagoptions.children && myOptions.length ==0 && tagoptions.children.length > 0){
            tagoptions.children[0].remove();
        }
		hlpr.initPicklist(cmp,optss);
		
    },
    initPicklist : function(cmp,optss){
    	var placeholderText = $A.get("$Label.c.IVP_SD_TAG_Search_Place_Holder");
        
        if(placeholderText =='$Label.c.IVP_SD_TAG_Search_Place_Holder does not exist.'){
        	placeholderText='Select Topic';
        }
        var $j = jQuery.noConflict();
    	$j('.singleSelectGrouped').fastselect({
            noResultsText :'No topic founds',
            placeholder : placeholderText,
            searchPlaceholder : 'Search Topic',
            onItemSelect : function($item, model, fastsearchApi){
                var cmpTarget = cmp.find('clearBtn');
                if(cmpTarget){
                    $A.util.removeClass(cmpTarget, 'slds-hide');
                }
                cmp.set('v.selectedTags',model.text);
            }
        });
    },
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(cmp, hlpr, response, callbackInfo);
            }
            else if (state === "INCOMPLETE") {
            	hlpr.enableCloseButon(cmp, false);
            	hlpr.handleSpinner(cmp, false);
            }else if (state === "ERROR") {
                var errors = response.getError();
                hlpr.handleErrors(errors);
                hlpr.enableCloseButon(cmp, false);
                hlpr.handleSpinner(cmp, false);
            }
            
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    
    handleTypeChange: function(cmp, evt){
        this.maintainTags(cmp, evt, 'Applying');
    },
    
    maintainTags : function(cmp, evt, type){
    	var selectedTags = '';
    	var elements = document.getElementsByClassName("dropdown-selected");
		
    	// var elements = document.getElementsByClassName("dropdown-display");
    	/*if(elements){
         	// selectedTags = elements[0].getAttribute('title'); 
         	if(elements[0]){
         		selectedTags = elements[0].innerText;
         	}
    	}
        */
        selectedTags = cmp.get('v.selectedTags');
        if(selectedTags == '--none--'){
            selectedTags = '';
        }
        var params = {selectedTagId:'', selectedValue:'', source:"tag", sourceType:"advanced", 
        			filterScope:'Mine', chkFilter:false, fieldApi:'Tags__c'};
        
        if(type == 'Applying'){
            var inpTagsPanal = cmp.find('inpTagsPanal');
            if(!selectedTags){
                this.showMessage({title:'Error', message:'Please select Topic first!', type:'Error'});
                return;
            }
            
            var source = evt.getSource();
	        var value = source.get('v.value');
            params.filterScope = value;
            params.filterScope = (params.filterScope == 'ALL' ? 'Everything' : params.filterScope);
            params.filterScope = (params.filterScope == 'MINE' ? 'Mine' : params.filterScope);

	        var chkValue = cmp.get('v.chkFilter');
            params.chkFilter = chkValue;
            params.selectedValue = selectedTags;
          	this.handleSpinner(cmp, true);
          	var selectedLV = cmp.get('v.selectedLV');
            if(selectedLV){
            	params.selectedLVId = selectedLV.viewId;
            }
            console.log('params',params);
            this.handleCalling(cmp, 'maitainTags', {inputJson : JSON.stringify(params)}, this.maitainTagsCallback, 
	        					this, {type:type});
			
        }else if(type ==  'Removing'){
        	var selectCmp = cmp.find("selectTags");
        	selectCmp.set("v.value", '');
        	this.maitainTagsCallback(cmp, this, {}, {});
        }
    },
    
    parseLinkValue :function(linkValue){
        var wrapperElement = document.createElement('div');
        wrapperElement.innerHTML = linkValue;
        var element = wrapperElement.childNodes[0];
        var attributes = element.attributes;
        var linkMap = {label: element.innerHTML};
        for (var i = 0; i < attributes.length; i++) {
            linkMap[attributes[i].name] = attributes[i].value;
        }
        
        if(linkMap.label){
	        var iIndex = linkMap.label.indexOf('<i');
	        if(iIndex>=0){
	        	var value = linkMap.label.substring(0,iIndex);
	        	linkMap.label = value;
	        }
        }
        return linkMap;
    },
    maitainTagsCallback : function(cmp, hlpr, response, callbackInfo){
    	var resultMap = response.getReturnValue();
    	var success = resultMap['success'];
    	if(success){
    		var action = 'clear';
	        var messageType = 'Removed';
	        if((callbackInfo.type === 'Applying')){
	            messageType = 'Applied';
	            action = 'apply';
	        }
	        messageType+=' Topic fiter!';
	        var ivpTaggingAppEvent = $A.get("e.c:IVPTaggingAppEvent");
	        if(ivpTaggingAppEvent){
	            ivpTaggingAppEvent.setParams({ 
	                "formData" : {refresh:true, source:'tag', action:action}
	            });
	            ivpTaggingAppEvent.fire();
	        }else{
	            alert('IVP Tagging: Please Contact to Administrator!');
	        }
	        hlpr.handleSpinner(cmp, false);
	        hlpr.triggerToast({title:'Success', message:messageType, type:'Success'});
    	}else{
    		hlpr.handleSpinner(cmp, false);
    		var errorMessage = $A.get("$Label.c.IVP_SD_TAG_Too_Many_Fltr_Msg");
	        if(errorMessage =='$Label.c.IVP_SD_TAG_Too_Many_Fltr_Msg does not exist.'){
	        	errorMessage='We cannot include listview\'s filters because multiple filters are applied already!';
	        }
    		hlpr.triggerToast({title:'Error', message:errorMessage, type:'Error'});
    	}
    },
    handleLoadWatchDog : function(cmp, selectedTag ){
        var ivpTaggingAppEvent = $A.get("e.c:IVPTaggingAppEvent");
        if(ivpTaggingAppEvent){
            ivpTaggingAppEvent.setParams({ 
                "formData" : {topic:selectedTag, source:'tag'}
            });
            ivpTaggingAppEvent.fire();
        }else{
            alert('CompanyListView: Please Contact to Administrator!');
        }
    },
    handleLoadView: function(cmp, data){
        var selectCmp = cmp.find("InputSelect");
        var options = selectCmp.get('v.options');
        for(var index=0, length=options.length; index < length; index++){
            var option = options[index];
            if(option.devValue!==undefined 
               &&((data.source === 'ListViewLoad' && option.value === data.viewId)
               || ( data.source === 'Chart' && option.devValue.toLowerCase() === data.viewName.toLowerCase()))){
                
                selectCmp.set("v.value", option.value);
                this.handleLoadWatchDog(cmp, option);
                this.updateViewName(cmp, option.devValue);
                break;
            }
        }
    },
    resetPicklist :function(cmp){
        var placeholderText = $A.get("$Label.c.IVP_SD_TAG_Search_Place_Holder");
        
        if(placeholderText =='$Label.c.IVP_SD_TAG_Search_Place_Holder does not exist.'){
        	placeholderText='Select Tag';
        }
        var selectOption = $('#tagoptions').get(0);
        selectOption.value = '';
        
        var toggleBtn = $('.fstToggleBtn').get(0);
        toggleBtn.innerHTML = placeholderText;
        var cmpTarget = cmp.find('clearBtn');
        if(cmpTarget){
            $A.util.addClass(cmpTarget, 'slds-hide');
        }
        cmp.set('v.selectedTags',''); //resetting value;
    },
    enableCloseButon: function(cmp, disabled){
        var btnClose = cmp.find('btnClose');
        if(btnClose)
        	btnClose.set('v.disabled',disabled);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown Error",
            type: "error"
        };
        
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        toastParams.message+='\n OR try again by de-selecting include current ListView filters!';
        toastParams.message+='\nPlease Contact To Administrator!';
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success",
            duration: 1000
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        } 
        if(message.duration){
            toastParams.duration = message.duration;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleSpinner : function(component, showSpinner){
        var spinner = component.find("tagSpinner");
        if(spinner){
	        if(showSpinner){
	            $A.util.removeClass(spinner, "slds-hide"); 
	        } else {
	            $A.util.addClass(spinner, "slds-hide");
	        }
        }
    },
    getCurrentUserId: function(){
        return $A.get("$SObjectType.CurrentUser.Id");
    }
})