({
    doInit : function(component,evt,helper){
        
        //var localStorageValue=localStorage.getItem('NextActionRequired');
        component.set("v.renderValue",true);
        window.intialized = true; 
        helper.loadCache(component, event, helper); // Changes 1
        component.set('v.isInit', true);
        window.addEventListener('popstate', function (e) {
		    var state = e.state;
		    if (state !== null) {
                // refershing Account Deatil page on navigating back somewhere
                window.location.reload();
            }
        })

        var nextActionOwnerObject = {'rId':'','rName':''};
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
               
                window.localStorage.setItem('userId', storeResponse.Id);
                window.localStorage.setItem('userName', storeResponse.Name);
                nextActionOwnerObject.rId = storeResponse.Id;
                nextActionOwnerObject.rName = storeResponse.Name;
                component.set("v.nextActionOwnerObject",nextActionOwnerObject);
            }
        });
        $A.enqueueAction(action);

        helper.doInit(component,evt,helper);
            
        helper.fetchDataForFinancial(component,helper);
        
        window.addEventListener("keydown", function(event) {
            if (event.ctrlKey) {
                
                switch (String.fromCharCode(event.which).toLowerCase()) {
                
                    case 's':
                        // Set the initial focus to some default to solve unexpected issue from aura 
                        var inp = document.getElementById('solveUnexpectedIssueOfFocus');
                        if(inp != undefined)
                            inp.focus(); 
                        
                        event.preventDefault();
                        if(document.getElementById("taskSaveButton")!=null) {
                            var isSaving = component.get("v.isSaving");
                            if(!isSaving) {
                                helper.saveNewTaskModalData(component,evt,helper);
                            } 
                        }
                        if(document.getElementById("saveUnassignButton")!=null) {
                            var isSaving = component.get("v.isSavingUnAssigned");
                            if(!isSaving) {
                                helper.saveUnassignModalHelper(component,evt,helper);
                            } 
                        }
                        if(document.getElementById("saveSendToPipeButton")!=null) {
                            var isSaving = component.get("v.isSavingSendToPipe");
                            if(!isSaving) {
                                setTimeout(function() {
                                    document.getElementById("saveSendToPipeButton").click();
                                }, 100);
                            } 
                        }
                        if(document.getElementById("saveFinancialsButton")!=null) {
                            var isSaving = component.get("v.isSavingFinancial");
                            if(!isSaving) {
                                setTimeout(function() {
                                    document.getElementById("saveFinancialsButton").click();
                                }, 100);
                            } 
                        }
                        break;
                    }
                if(event.altKey) {
                    event.preventDefault();
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'c':
                            helper.displayCallPopupHelper(component,evt,helper);
                            break;
                        case 'm':
                            helper.displayMeetingPopupHelper(component,evt,helper);
                            break;
                        case 'v':
                            helper.displayVoicemailPopupHelper(component,evt,helper);
                            break;
                        case 'i':
                            helper.displayInboundEmailHelper(component,evt,helper);
                            break;
                        case 'o':
                            helper.displayOutboundEmailHelper(component,evt,helper);
                            break;
                        case 'e':
                            helper.displayExaminePopupHelper(component,evt,helper);
                            break;
                        case 'p':
                            var isHavingOpenOpp = component.get("v.isHavingOpenOpp");
                            
                            if(!isHavingOpenOpp) {
                                component.set("v.openSendToPipeModal",true);
                                setTimeout(function() {
                                    var inp = document.getElementById('strike-input-nextActionInput');
                                    if(inp != undefined)
                                        inp.focus();
                                }, 0);
                            }
                            else {
                                helper.sendToPipeGreyOutErrorMessage(component, helper);
                            }
                            break;
                        case 'f':
                            component.set("v.openFinancialsModal",true);
                            break;
                        case 'u':
                            if(component.get("v.assignStatus")=='assign') {
                                var isProspect = component.get('v.isProspect');
                                if(isProspect === true){
                                    var validationMap = component.get('v.validationMap'); 
                                    if(validationMap && validationMap.isOwnership === true){
                                        helper.shortcutErrorMessage(component, helper);
                                    }else{
                                        helper.handleChangeOwnership(component, helper);
                                    }
                                }else{
                                    helper.assignToCurrentUserHelper(component,helper);
                                }
                            }
                            else if(component.get("v.assignStatus")=='reassign')
                            {
                                helper.unAssignUserHelper(component);
                            }
                            break;
                        case 'r':
                            helper.reAssignUserHelper(component);
                            break;
                        case 'd':
                            var validationMap = component.get('v.validationMap');
                            var assignStatus = component.get('v.assignStatus');
                            if(assignStatus == 'reassign') {
                                if(validationMap && validationMap.showUntouchable) {
                                    if(validationMap.isUntouchable){
                                        helper.shortcutErrorMessage(component, helper);
                                    }
                                    else {
                                        helper.handleCompanyStatusChange(component, helper, $A.get("$Label.c.IVP_Ownership_Status_Two"));
                                    }
                                }
                                else if(validationMap && validationMap.showWatchlist ) {
                                    if(validationMap.isWatchlist) {
                                        helper.shortcutErrorMessage(component, helper);
                                    }
                                    else {
                                        helper.handleCompanyStatusChange(component, helper, $A.get("$Label.c.IVP_Ownership_Status_One"));
                                    }
                                }
                            }
                            break;
                        case 'w':
                            var validationMap = component.get('v.validationMap');
                            var assignStatus = component.get('v.assignStatus');
                            if(validationMap && validationMap.showRequest && assignStatus == 'otheruser') {
                                if(validationMap.isRequest) {
                                    helper.shortcutErrorMessage(component, helper);
                                }
                                else {
                                    component.set("v.reasonLabel",$A.get("$Label.c.IVP_Ownership_Request_Ownership_Description_label"));
                                    component.set("v.showInputBox", true);
                                    helper.setFocus(component, 'input');
                                }
                            }
                            break;
                        case 'a':
                            var validationMap = component.get('v.validationMap');
                            var assignStatus = component.get('v.assignStatus');
                            if(validationMap && validationMap.showAcceptAndReject && (assignStatus == 'reassign' 
                                || validationMap.assignStatus == 'openrequest')) {
                                component.set("v.reasonLabel",$A.get("$Label.c.IVP_Ownership_Accept_Request_Note_label"));
                                component.set("v.isAccept",true);
                                component.set("v.showInputBox", true);
                                helper.setFocus(component, 'input');
                            }
                            break;
                        case 'j':
                            var validationMap = component.get('v.validationMap');
                            var assignStatus = component.get('v.assignStatus');
                            if(validationMap && validationMap.showAcceptAndReject && (assignStatus == 'reassign' 
                                || validationMap.assignStatus == 'openrequest')) {
                                if(validationMap.isReject){
                                    helper.shortcutErrorMessage(component, helper);
                                }
                                else{
                                    component.set("v.reasonLabel",$A.get("$Label.c.IVP_Ownership_Reject_Request_Reason_label"));
                                    component.set("v.isReject",true);
                                    component.set("v.showInputBox", true);
                                    helper.setFocus(component, 'input');
                                }
                            }
                            break;
                    }
                }
            }
            else if(event.key === "Escape") {
                // Set the initial focus to some default to solve unexpected issue from aura 
                var inp = document.getElementById('solveUnexpectedIssueOfFocus');
                if(inp != undefined)
                    inp.focus();
                var isShowAlert = component.get("v.showAlert");
                if(!isShowAlert) {
                    component.set("v.taskSelect","");
                    helper.displayPrompt(component, evt, helper);
                }
                else {
                    inp = document.getElementById('strike-input-nextActionInput');
                    if(inp != undefined) 
                        inp.focus();
                    helper.closeAlertModal(component, evt, helper);//Added by V2Force
                }
                component.set("v.openAssignModal",false);
                component.set("v.lossDropDisable",false);
                component.set("v.lossDropDisable",false);
                component.set("v.openSendToPipeModal",false);
                component.set("v.openFinancialsModal",false);
            }
        }, true);
    },
    destroyComponent :function(component,event,helper){
        component.destroy();
    },
	assignToCurrentUser :function(component,event,helper){
		helper.assignToCurrentUserHelper(component,helper);
	},
	
	unAssignUser :function(component,event,helper){
		helper.unAssignUserHelper(component);
	},
	saveNewTaskModal :function(component,event ,helper){
        window.localStorage.setItem('runAutoSave', false);
        component.set('v.isRecursion', false);
		helper.saveNewTaskModalData(component,event,helper);
	},
	closeModal :function(component){
        component.set("v.showFollowUpDependentField",false);
		component.set("v.openAssignModal",false);
		component.set("v.taskSelect","");
		component.set("v.lossDropDisable",false);
	},
    
	saveUnassignModal :function(component,event ,helper){
        component.set("v.showFollowUpDependentField",false);
		helper.saveUnassignModalHelper(component,event,helper);
	},
	reAssignUser :function(component,event,helper){
		helper.reAssignUserHelper(component);
	},
	closeNewTaskModal : function(component, event, helper) {
        window.localStorage.setItem('runAutoSave', false);
        helper.displayPrompt(component, event, helper);
    },
    closeAlertModal : function(component, event, helper) {
        helper.closeAlertModal(component, event, helper);
    },
    saveFinancials : function(component,event,helper){
        component.set("v.isModalClass", false);
        component.set("v.showAlert",false);
        component.set("v.isWithoutFin", true);

        component.set("v.priorPriorRevenueValue", component.get("v.priorPriorRevenueValueCopy"));
        component.set("v.priorRevenueValue", component.get("v.priorRevenueValueCopy"));
        component.set("v.currentRevenueValue", component.get("v.currentRevenueValueCopy"));
        component.set("v.nextRevenueValue", component.get("v.nextRevenueValueCopy"));
        component.set("v.priorPriorEbitaValue", component.get("v.priorPriorEbitaValueCopy"));
        component.set("v.priorEbitaValue", component.get("v.priorEbitaValueCopy"));
        component.set("v.currentEbitaValue", component.get("v.currentEbitaValueCopy"));
        component.set("v.nextEbitaValue", component.get("v.nextEbitaValueCopy"));
        
        component.set("v.priorPriorNetValue", component.get("v.priorPriorNetValueCopy"));
        component.set("v.priorNetValue", component.get("v.priorNetValueCopy"));
        component.set("v.currentNetValue", component.get("v.currentNetValueCopy"));
        component.set("v.nextNetValue", component.get("v.nextNetValueCopy"));
        component.set("v.priorPriorGrossValue", component.get("v.priorPriorGrossValueCopy"));
        component.set("v.priorGrossValue", component.get("v.priorGrossValueCopy"));
        component.set("v.currentGrossValue", component.get("v.currentGrossValueCopy"));
        component.set("v.nextGrossValue", component.get("v.nextGrossValueCopy"));
        
        helper.saveNewTaskModalHelper(component,event,helper);
    },
	displayCallPopup : function(component,event,helper) {

        
        component.set('v.TaskCommentDraft',"");
        component.set('v.taskComments', "");
        //component.set("v.taskNextAction", ""); //today
        //component.set("v.taskNextActionDate", undefined);//todays
        //component.set("v.totalFundingRaisedValue",null); //today
        window.localStorage.setItem('runAutoSave', true);
        helper.displayCallPopupHelper(component,event, helper);
        helper.fetchDataForFinancial(component,helper);
    },
    displayMeetingPopup : function(component,event,helper) {

        window.localStorage.setItem('runAutoSave', true);
        //component.set("v.taskNextAction", ""); //today
        //component.set("v.taskNextActionDate", undefined);//todays
        //component.set("v.totalFundingRaisedValue",null); //today
        component.set('v.talentTypeComment', "");
    	helper.displayMeetingPopupHelper(component, event, helper);
        helper.fetchDataForFinancial(component,helper);
    },
    displayVoicemailPopup : function(component,event,helper) {
        window.localStorage.setItem('runAutoSave', true);
    	helper.displayVoicemailPopupHelper(component, event, helper);
    },
    displayInboundEmail : function(component,event,helper) {
        window.localStorage.setItem('runAutoSave', true);
    	helper.displayInboundEmailHelper(component, event, helper);
    },
    displayOutboundEmail : function(component,event,helper) {
        window.localStorage.setItem('runAutoSave', true);
    	helper.displayOutboundEmailHelper(component, event, helper);
    },
    displayExaminePopup : function(component,event,helper) {
        window.localStorage.setItem('runAutoSave', true);
    	helper.displayExaminePopupHelper(component, event, helper);
    },
    displayAssignPopup : function(component,event,helper) {
        
    	helper.displayAssignPopupHelper(component);
    },
    handleBubbling : function(cmp, evt, help) {
		help.lkupFunc(cmp,evt);
	},
    
    
    /*
     * Ownership changes
     * 
    */
    
    handleChangeOwnership : function(cmp, evt, hlpr){
        hlpr.handleChangeOwnership(cmp, hlpr);
    },
    showInputBox : function(component, event, helper) {
        component.set("v.reasonLabel",$A.get("$Label.c.IVP_Ownership_Request_Ownership_Description_label"));
        component.set("v.showInputBox", true);
        helper.setFocus(component, 'input');
	},
    hideInputBox : function(component, event, helper) {
        component.set("v.reason","");
        component.set("v.isAccept",false);
        component.set("v.isReject",false);
        component.set("v.showInputBox", false);
	},
    handleCompanyStatusChange : function(component, event, helper) {
        // var ownerStatus = event.getSource().get("v.name");
        var ownerStatus = event.target.name;
        console.log('ownerStatus--->'+ownerStatus);
        helper.handleCompanyStatusChange(component, helper, ownerStatus);
	},
    onAccept : function(component, event, helper) {
        component.set("v.reasonLabel",$A.get("$Label.c.IVP_Ownership_Accept_Request_Note_label"));
        component.set("v.isAccept",true);
        component.set("v.showInputBox", true);
        helper.setFocus(component, 'input');
	},
    handleAcceptRequest : function(component, event, helper) {
        helper.checkInput(component, helper);
        if(component.get("v.isChecked")){
            helper.handleAcceptRequest(component, helper);
        }
	},
    onReject : function(component, event, helper) {
        component.set("v.reasonLabel",$A.get("$Label.c.IVP_Ownership_Reject_Request_Reason_label"));
        component.set("v.isReject",true);
        component.set("v.showInputBox", true);
        helper.setFocus(component, 'input');
	},
    handleRejectRequest : function(component, event, helper) {
        helper.checkInput(component, helper);
        if(component.get("v.isChecked")){
            helper.handleRejectRequest(component, helper);
        }
	},
    handleRequestForOwnership : function(component, event, helper) {
        helper.checkInput(component, helper);
        if(component.get("v.isChecked")){
            component.set("v.showInputBox", false);
            helper.handleRequestForOwnership(component, helper);
        }
    },
    keyCheck : function(component, event, helper){
        if (event.which == 13){
            helper.checkInput(component);
            if(component.get("v.isChecked")){
                var isReject = component.get("v.isReject");
                var isAccept = component.get("v.isAccept");
                if(isReject){
                    helper.handleRejectRequest(component, helper);
                }else if(isAccept){
                    helper.handleAcceptRequest(component, helper);
                }else{
                    helper.handleRequestForOwnership(component, helper);
                }
            }  
        }
    },
    handleToast: function(component, event, helper){
        helper.doInit(component, event, helper);
    },
    toggleFinancials: function(component) {
    	var openFinancials = component.get("v.openFinancials");
        component.set("v.openFinancials", !openFinancials);
    },
    discardDraftComment: function(component, event, helper){
        helper.discardDraftComment(component, event, helper);
    },
    closePromptModal: function(component, event, helper) {
        var isNextActionRequired=component.get("v.isNextActionRequired")=='Yes'?true:false;
        if(isNextActionRequired){
            component.set("v.showFollowUpDependentField",true);
        }
        else{
            component.set("v.showFollowUpDependentField",false);
        }

        component.set('v.showPrompt', false);
        helper.autoSaveComment(component, event, helper);
    },

    closeTaskPromptModal : function(component, event, helper) {
        helper.closeTaskModal(component, event, helper);
    },

    displayModelPopup : function(component, event, helper) {
        
       
        component.set("v.isNextActionRequired",'No');
        component.set("v.showFollowUpDependentField",false);
        component.set("v.isSignificant",'No');
        component.set("v.talentTaskIntExtVal",'External');
        component.set("v.talentFlwUpIntExtVal",'');
        component.set("v.talentTypeActivityType",'');
        component.set("v.talentTypeTaskStatus",'');
        component.set("v.talentTypeNextAction",'');
        window.localStorage.setItem('runAutoSave', true);

        var nextActionOwnerObject = {'rId':'','rName':''};
        
        if(window.intialized != true) {
            return; 
        } 
        
        if(window.localStorage != null) {
            nextActionOwnerObject.rId = localStorage.getItem('userId');
            nextActionOwnerObject.rName = localStorage.getItem('userName');
            component.set("v.nextActionOwnerObject",nextActionOwnerObject);
        }

        component.set("v.talentTypeNextActionDate",null);
        console.log('title: ',event.currentTarget.id);
        component.set("v.currentBtnDetail", event.currentTarget.id);
        if(event.currentTarget.id == 'Inbound Email' || event.currentTarget.id == 'Outbound Email'){
            component.set("v.talentTypeSubject", event.currentTarget.id+' logged');
        }
        else{
            console.log('470=== ',event.currentTarget.id)
            component.set("v.talentTypeSubject",'');
        }
        
        component.set("v.TaskTypeLabel", event.currentTarget.id);
        component.set('v.isShowDefaultComment',true);
      
        helper.getCheckableFields(component, event, helper, null, false, true);
        helper.displayModelPopupHlpr(component, event, helper);
        helper.autoSaveTalentComment(component, event, helper);
        
        /*if(event.currentTarget.id === 'Call'){
            helper.displayCallPopupHelper(component,event, helper);
            helper.fetchDataForFinancial(component,helper);
        }else if(event.currentTarget.id === 'Meeting'){
            helper.displayMeetingPopupHelper(component, event, helper);
            helper.fetchDataForFinancial(component,helper);
        }else if(event.currentTarget.id === 'Voicemail'){
            helper.displayVoicemailPopupHelper(component, event, helper);
        }else if(event.currentTarget.id === 'Inbound Email'){
            helper.displayInboundEmailHelper(component, event, helper);
        }else if(event.currentTarget.id === 'Outbound Email'){
            helper.displayOutboundEmailHelper(component, event, helper);
        }else if(event.currentTarget.id === 'Examine'){
            helper.displayExaminePopupHelper(component, event, helper);
        }else if(event.currentTarget.id === 'Task'){
            
        }*/
        
    },
    closefieldModel : function(component, event, helper){
        component.set("v.showFollowUpDependentField",false);
        component.set("v.isAllSelected",false);
        
        window.localStorage.setItem('runAutoSave', false);

        //component.set("v.showfieldModelforTalent", false);
        helper.autoSaveTalentComment(component, event, helper);
        helper.displayPrompt(component, event, helper);
        //helper.autoSaveComment(component, event, helper);
        //helper.clearIntervalOnModalClose(component);
    },
    /*setfieldValues : function(component, event, helper){
        var updatedVal = event.getSource();
        var id = updatedVal.getLocalId();
        console.log('>>>>value>>>',updatedVal.get("v.value"));
        console.log('>>>>id>>>',id);

        /*if(updatedVal !== undefined){
            var record = component.get("v.record");
            var fieldName = component.get("v.col");
            record[fieldName] = updatedVal;
        }
    },*/

    saveTalentTask : function(component, event, helper){

        console.log('Start: saveTalentTask');
        var selectedValue =  component.get('v.isNextActionRequired');
        window.localStorage.setItem('runAutoSave', false);
        var taskLabelType = component.get("v.TaskTypeLabel");
        if(taskLabelType == 'Task'){
            component.set("v.talentTypeNextActionDate",null);
            component.set("v.talentTypeNextAction",null);
        }
      

        console.log('isNull talentTypeComment', helper.isNull(component.get("v.talentTypeComment")));
        console.log('isNull talentTypeActivityDate', helper.isNull(component.get("v.talentTypeActivityDate")));
        console.log('TaskTypeLabel', component.get("v.TaskTypeLabel"));
        console.log('isNull talentTypeNextAction', helper.isNull(component.get("v.talentTypeNextAction")));
        console.log('isNull talentTypeNextActionDate', helper.isNull(component.get("v.talentTypeNextActionDate")));
        console.log('isNull talentTypeSubject', helper.isNull(component.get("v.talentTypeSubject")));
        console.log('isNull talentTypeActivityType', helper.isNull(component.get("v.talentTypeActivityType")));
        console.log('isNull for Talent', (component.get("v.TaskTypeLabel") != 'Task' &&(helper.isNull(component.get("v.talentTypeNextAction")) ||
        helper.isNull(component.get("v.talentTypeNextActionDate")))));
        console.log('isNull Examine', ((component.get("v.TaskTypeLabel") != 'Examine')&&
        helper.isNull(component.get("v.talentTaskIntExtVal"))|| 
        helper.isNull(component.get("v.talentTypeActivityType"))));


        
        if(helper.isNull(component.get("v.talentTypeComment"))|| 
            helper.isNull(component.get("v.talentTypeActivityDate"))||
            (component.get("v.TaskTypeLabel") != 'Task' && selectedValue!= 'No' && 
            (helper.isNull(component.get("v.talentTypeNextAction"))||
            helper.isNull(component.get("v.talentTypeNextActionDate"))|| 
            helper.isNull(component.get("v.talentFlwUpIntExtVal"))))||
            helper.isNull(component.get("v.talentTypeSubject"))|| 
            ((component.get("v.TaskTypeLabel") != 'Examine')&&
            helper.isNull(component.get("v.talentTypeActivityType"))) ||
            ((component.get("v.TaskTypeLabel") == 'Examine')&& selectedValue!= 'No' &&
            helper.isNull(component.get("v.talentTypeActivityType"))))
            
            {
              
                    helper.showToast(component, event, helper);
                
                
        }else{
            
                var currentTasktypeLabel = component.get("v.TaskTypeLabel");
                console.log('currentTasktypeLabel>>>>>',currentTasktypeLabel)
                //To Save MultiContact List
                // console.log('ContactList Ids: ',component.find("contactAttendees"));

                // hiding meeting attendees for Examine and Task Model
                if(currentTasktypeLabel != 'Examine'
                    && currentTasktypeLabel != 'Task'){
                    var meetingAttendees = component.find("contactAttendees").get("v.selValList");

                    console.log('meetingAttendees: ',meetingAttendees);
                    var contactAttendeesList= [];
                    
                    if(meetingAttendees.length > 0){
                        for(var i = 0; i<meetingAttendees.length;i++)
                        {
                            if(meetingAttendees[i].rId != null){
                                contactAttendeesList.push(meetingAttendees[i].rId);
                                console.log('Contact ' + meetingAttendees[i].rId + ' added to list');
                            }
                            
                        }
                    }
                    console.log('contactAttendeesList: ',contactAttendeesList);

                    var contactAttendeesIdSet = removeDuplicates(contactAttendeesList);

                    function removeDuplicates(contactAttendeesList) {
                        return contactAttendeesList.filter((a, b) => contactAttendeesList.indexOf(a) === b)
                    };

                    console.log('contactAttendeesIdSet: ',contactAttendeesIdSet);
                    helper.saveNewTaskModalOfTalent(component, event, helper, contactAttendeesIdSet);

                }
                else{
                    var contactAttendeesIdSet = [];
                helper.saveNewTaskModalOfTalent(component, event, helper, contactAttendeesIdSet);
                }
            }
    },
    selectAllCheckBox : function(component, event, helper){
        
        console.log('isAllSelected: ',component.get('v.isAllSelected'));
        if(component.get('v.isAllSelected') == false) {
            component.set('v.isAllSelected', true);
            const myCheckboxes = component.find('myCheckboxes'); 
            let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
            chk.forEach(checkbox => checkbox.set('v.checked', component.get('v.isAllSelected')));
        }else{
            component.set('v.isAllSelected', false);
            const myCheckboxes = component.find('myCheckboxes'); 
            let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
            chk.forEach(checkbox => checkbox.set('v.checked', component.get('v.isAllSelected')));
        }
        
    },
    onChangeRequiredNextAction:function(component,event,helper){
        var selectedValue = component.find("creatFollowUp_Yes/No").get("v.value");
        
        console.log('isNextActionRequired: ',component.get("v.isNextActionRequired"));
        if(selectedValue==='Yes'){
            component.set("v.showFollowUpDependentField",true);
        }
        else if(selectedValue==='No'){
            component.set("v.showFollowUpDependentField",false);
        }

    },
     onCheck: function(cmp, evt) {
         console.log('checkCmp === ',cmp.get("v.checkedValue"));
         console.log('641',cmp.find("checkbox"));
		 var checkCmp = cmp.find("checkbox");
          if(cmp.get('v.checkedValue') == false) {
    		 cmp.set("v.checkedValue",true);
          }
         else{
              cmp.set("v.checkedValue",false);
         }
         console.log('checkCmp === ',cmp.get("v.checkedValue"));
		 //resultCmp = cmp.find("checkResult");
		 //resultCmp.set("v.value", ""+checkCmp.get("v.value"));

	 }
})