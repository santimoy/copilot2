({
    handleCalling : function(cmp, hlpr, method, params, callback, callbackInfo, errorCallback) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                callback(cmp, null, hlpr, response, callbackInfo);
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }else if (state === "ERROR") {
                var errors = response.getError();
                hlpr.handleErrors(errors);
                cmp.set("v.reason","");
                cmp.set("v.showInputBox", false);
                cmp.set("v.showSpinner",false);
                if(errorCallback){
                    errorCallback(cmp, hlpr, response, callbackInfo);
                }
            }
            
        });
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success",
            mode: 'pester',
            duration: '8000'
        };
        
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error",
            mode: 'dismissible'
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            if(errors){
                if(errors.length>0 && errors[0]!=undefined && errors[0].pageErrors === undefined && errors[0].fieldErrors === undefined){
                    toastParams.message = this.extractErrorMessage(errors[0].message);
                }else if(errors[0] && errors[0].pageErrors && errors[0].pageErrors.length>0){
                    toastParams.message = errors[0].pageErrors[0].message;
                }else if(errors[0] && errors[0].fieldErrors){
                    toastParams.message = this.prepareFieldErrorMsg(errors[0].fieldErrors);
                }
            }else{
                console.log("Unknown error");
            }
        }
        this.triggerToast(toastParams);
    },
    extractErrorMessage : function(msg){
        var ind= msg.indexOf('_EXCEPTION,') ;
        if(ind > -1 ){
            ind+='_EXCEPTION, '.length;
            var lastIndex = msg.indexOf(':', ind);
            lastIndex = lastIndex < 0 ? msg.length : lastIndex;
            msg = msg.substring(ind, lastIndex);
        }
        return msg;
    },
    prepareFieldErrorMsg : function(fieldErrors){
        var keys = Object.keys(fieldErrors);
        var msg = 'Unknown error';
        if(keys.length > 0){
            msg='';
            for( var ind in keys){
                var fErrors = fieldErrors[keys[ind]];
                fErrors.forEach(function(fErr){
                    msg+= fErr.message+' \n';
                })
                
            }
        }
        return msg;
    },
    doInit : function(component,event ,helper) {
        
        var initializing = component.get('v.initializing');
        if(initializing === true){
            // returning because initializing on going
            return;
        }
        var action = component.get("c.fetchInitData");
        action.setParams({
            recordId:component.get("v.recordId")
        });
        
        action.setCallback(this,function(response){
            
            
            
            component.set('v.initializing', false); 
            var state = response.getState();
            console.log('fetchInitData state: ',state);
            
            if (state === "SUCCESS"){
                var res = response.getReturnValue();
                console.log('fetchInitData res: ',JSON.parse(res));
                
                var tempRes = JSON.parse(res);
                console.log('tempRes: ',tempRes);
                if(tempRes.lossDropPicklist.length > 0){
                    tempRes.lossDropPicklist.sort();
                }
                component.set("v.taskRecordTypeId",tempRes.taskRecordId);
                component.set("v.todayDate",tempRes.todayDate);
                component.set("v.taskNextActionOnLoad",tempRes.taskNextActionOnLoad);
                component.set("v.taskNextActionDateOnLoad",tempRes.taskNextActionDateOnLoad);
                component.set("v.taskNextAction",tempRes.taskNextAction);
                component.set("v.taskNextActionDate",tempRes.taskNextActionDate);
                //component.set("v.taskActivityDate",tempRes.todayDate);
                component.set("v.currentUser",tempRes.currentUserId);
                component.set("v.assignStatus",tempRes.assignStatus);
                component.set("v.unAssignOptions",tempRes.lossDropPicklist);
                component.set("v.accountPriorityOptions",tempRes.accountPriorityPicklist);
                component.set("v.dynamicBtnDetail", tempRes.lstOfBtnData);
                component.set("v.IsTalentGroup", tempRes.isRoleTalentGroup);
                component.set("v.isUserInPicklistqueue",tempRes.isUserInPicklistqueue);
                tempRes.userqueuePicklist = 'This company is reserved for Team '+tempRes.userqueuePicklist+'. You can cannot assign this company now';
                component.set("v.teamQueuePicklistValues",tempRes.userqueuePicklist);
                component.set("v.preventCompanyRequest",tempRes.preventCompanyRequest);
                console.log('aab',component.get("v.teamQueuePicklistValues"))
                console.log('IsTalentGroup----->',component.get("v.IsTalentGroup"));
                console.log('dynamicBtnDetail----->',component.get("v.dynamicBtnDetail"));
                if(component.get("v.IsTalentGroup")=== true){
                    console.log('IsTalentGroup true');
                    helper.checkDraftCommentHT(component, event, helper);
                }else if(component.get("v.IsTalentGroup")=== false){
                    console.log('not talent');
                    helper.checkDraftCommentH(component, event, helper);
                }
                if(tempRes.isProspect === true){
                    var validationMap = tempRes.validationData;
                    //console.log('validationMap',validationMap); 
                    /**********************
                        * Calling a method from controller to check if user has bypass and
                        * If the user has bypass then we are setting the attribute value is Bypassed
                        and using it to check validations on various cases
                        **********************/
                    console.log('tempRes isBypassed ',tempRes.isBypassed);
                    component.set("v.isBypassed",tempRes.isBypassed);
                    component.set("v.previousOwnerCheckData", {isValid: tempRes.isPreviousOwnerCheck, errMsg: tempRes.isPreviousOwnerError, 
                                                               isWaitToRepriortizeCheck: tempRes.isWaitToRepriortizeCheck, isWaitToRepriortizeError: tempRes.isWaitToRepriortizeError});
                    this.handleWatchlistAndUntouchable(component, validationMap)
                    component.set("v.isProspect", tempRes.isProspect);
                }
                
            }else if(state === "INCOMPLETE"){
                this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }else if(state === "ERROR"){
                var errors = response.getError();
                this.handleErrors(errors);
            }
            
        });
        component.set('v.initializing', true);
        $A.enqueueAction(action);
    },
    
    saveNewTaskModalHelper :function(component,event ,helper){
        
        var comments = component.get("v.taskComments");
        var taskSubject = component.get("v.taskSubject");
        var tasktypeVal = component.get("v.taskType");
        if(component.get("v.openNewTaskModal"))
        {
            component.set("v.isSaving",true);
            $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
            var error = false;
            if($A.util.isEmpty(taskSubject))
            {
                error = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error',
                    message: 'PSubject is required',
                    type: 'error' ,
                    mode: 'dismissible',
                });
                toastEvent.fire();
            }
            
            if($A.util.isEmpty(comments))
            {
                error = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error',
                    message: 'Please enter Comments',
                    type: 'error' ,
                    mode: 'dismissible',
                });
                toastEvent.fire();
                
            }
            var taskActivityDateDOM = document.getElementById('taskActivityDate');
            
            if(taskActivityDateDOM && taskActivityDateDOM.value === '')
            {
                error = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: 'Error',
                    message: 'Activity Date is required',
                    type: 'error' ,
                    mode: 'dismissible',
                });
                toastEvent.fire();
                
            }
            //Added 4/23/2019
            if(tasktypeVal == "Call" || tasktypeVal == "Meeting" ){
                
                var isBlankFirst = false; 
                var isBlankSecond = false; 
                if(!$A.util.isEmpty(comments))
                {
                    if(comments.length > 100){
                        
                        var priorPriorRevenueValue = component.get("v.priorPriorRevenueValue");
                        var priorPriorEbitaValue = component.get("v.priorPriorEbitaValue");
                        var priorRevenueValue = component.get("v.priorRevenueValue");
                        var priorEbitaValue = component.get("v.priorEbitaValue"); 
                        var currentRevenueValue = component.get("v.currentRevenueValue");
                        var currentEbitaValue = component.get("v.currentEbitaValue"); 
                        var nextRevenueValue = component.get("v.nextRevenueValue"); 
                        var nextEbitaValue = component.get("v.nextEbitaValue"); 
                        
                        if($A.util.isEmpty(currentRevenueValue) && $A.util.isEmpty(currentEbitaValue) 
                           && $A.util.isEmpty(nextEbitaValue) && $A.util.isEmpty(nextEbitaValue)) {
                            isBlankFirst = true;
                        }
                    }
                    
                    /* Prior Year and Current Year Net and Gross retention (4 boxes) are all blank 
                        * 	when logging a Call or Meeting AND the Task.Description Contains any of the following words:
                        *  “retention", "renew", or "churn" (case insensitive)
                        */
                    var commentsLower = comments.toLowerCase();
                    if (commentsLower.indexOf("retention") != -1 || commentsLower.indexOf("renew") != -1 || commentsLower.indexOf("churn") != -1) {
                        var priorNetValue = component.get("v.priorNetValue");
                        var priorGrossValue = component.get("v.priorGrossValue"); 
                        var currentNetValue = component.get("v.currentNetValue");
                        var currentGrossValue = component.get("v.currentGrossValue"); 
                        if($A.util.isEmpty(priorNetValue) && $A.util.isEmpty(priorGrossValue) 
                           && $A.util.isEmpty(currentNetValue) && $A.util.isEmpty(currentGrossValue)) {
                            isBlankSecond = true;
                        }
                    }
                    
                    component.set("v.showRevEbtErrorMsg", isBlankFirst);
                    component.set("v.showNetGrossErrorMsg", isBlankSecond);
                    
                    var isWithoutFin = component.get("v.isWithoutFin");
                    if((isBlankFirst || isBlankSecond) && !isWithoutFin){
                        
                        $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                        helper.clearIntervalOnModalClose(component);//Added by V2Force
                        component.set("v.showAlert",true);
                        error = true; 
                    }
                    else {
                        
                        component.set("v.showAlert",false);
                        error = false;
                        component.set("v.isWithoutFin", false);
                    }
                } 
                else
                {
                    component.set("v.isModalClass", false);
                }
            }
            
            if(!error)
            {
                var taskData = {};
                taskData.taskType = tasktypeVal;
                taskData.taskSubject = component.get("v.taskSubject");
                taskData.taskComments = component.get("v.taskComments");
                taskData.taskActivityDate = document.getElementById("taskActivityDate").value;
                taskData.taskNextAction = component.get("v.taskNextAction");
                var taskNextActionOnLoad = component.get("v.taskNextActionOnLoad");
                taskData.taskNextActionDate = document.getElementById("nextActionDate").value;
                var taskNextActionDateOnLoad = component.get("v.taskNextActionDateOnLoad");
                taskData.accountPriority = component.get("v.accountPriorityValue");
                taskData.clearAccountNextAction = false;
                taskData.clearAccountNextActionDate = false;
                
                if((taskData.taskNextAction == taskNextActionOnLoad 
                    && taskData.taskNextActionDate == taskNextActionDateOnLoad)) {
                    
                    taskData.taskNextAction = null;
                    taskData.taskNextActionDate = null;
                }
                else if(taskData.taskNextAction != taskNextActionOnLoad  
                        || taskData.taskNextActionDate != taskNextActionDateOnLoad) {
                    
                    if(!$A.util.isEmpty("taskNextActionOnLoad")
                       && $A.util.isEmpty(taskData.taskNextAction))
                        taskData.clearAccountNextAction = true;
                    if(!$A.util.isEmpty("taskNextActionDateOnLoad")
                       && $A.util.isEmpty(taskData.taskNextActionDate))
                        taskData.clearAccountNextActionDate = true;
                }
                
                var taskJSONData = JSON.stringify(taskData);
                if(tasktypeVal == 'Call'||tasktypeVal == 'Meeting')
                {
                    helper.saveTaskAndFinancialsData(component, helper, taskJSONData);
                }
                else
                {
                    helper.saveTaskDataOnly(component, helper, taskJSONData);
                }
                
            }
            else
            {
                component.set("v.isSaving",false);
                
                if(!component.get("v.showAlert")){
                    $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                    component.set("v.isModalClass", false);//Added by v2force to resolve the Activity date is required issue
                }
            }
        }
       
    },
    saveUnassignModalHelper :function(component,event ,helper){
        if(component.get("v.openAssignModal"))
        {
            component.set("v.isSavingUnAssigned", true);
            $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
            
            var error = false;
            if(component.get("v.reassignType")=='reassign')
            {
                if(component.get("v.userObject")==null)
                {
                    error = true;
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        message: 'Please select a user',
                        type: 'error' ,
                        mode: 'dismissible',
                    });
                    toastEvent.fire();
                }
            }
            else 
            {
                if(component.get("v.lossDropReasonValue")==''||component.get("v.lossDropReasonValue")==null)
                {
                    error = true;
                    //helper.toastErrorMessage('Error' , 'Please select a reason', 'error');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        message: 'Please select a reason',
                        type: 'error' ,
                        mode: 'dismissible',
                    });
                    toastEvent.fire();
                }
                else if(component.get("v.lossDropReasonValue")=='Other'&&component.get("v.lossDropNotesValue")=='')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        message: 'Please enter notes',
                        type: 'error' ,
                        mode: 'dismissible',
                    });
                    toastEvent.fire();
                    error = true;
                }
            }
            if(!error)
            {
                var action =component.get("c.reassignCurrentUser");
                action.setParams({
                    recordId:component.get("v.recordId"),
                    lossDropSelect:component.get("v.lossDropReasonValue"),
                    lossDropNotes:component.get("v.lossDropNotesValue"),
                    newUserSelect:(component.get("v.userObject")!=null?component.get("v.userObject").rId:"")
                });
                
                action.setCallback(this,function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS")
                    {
                        component.set("v.assignStatus","");
                        component.set("v.lossDropReasonValue","");
                        component.set("v.lossDropNotesValue","");
                        component.set("v.openAssignModal",false);
                        $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                        if(component.get("v.userObject")==null ||component.get("v.userObject").rId==null)
                        {
                            component.set("v.assignStatus","assign");
                        }
                        else
                        {
                            component.set("v.userObject",null);
                        }
                        component.set("v.lossDropDisable",false);
                        
                        $A.get("e.force:refreshView").fire();
                        helper.doInit(component,event ,helper);
                        helper.sendReassignEvent(component);
                    }
                    else if(state === "INCOMPLETE")
                    {
                        helper.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
                    }
                        else if(state === "ERROR")
                        {
                            var errors = response.getError();
                            //handling errors 
                            this.handleErrors(errors);
                            //hiding spinner
                            $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                        }
                    component.set("v.isSavingUnAssigned", false);
                });
                $A.enqueueAction(action);
            }
            else
            {
                component.set("v.isSavingUnAssigned", false);
                $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                
            }
        }
    },
    displayCallPopupHelper : function(component,event, helper) {

        console.log('let see',component.get("v.taskNextAction"));
        component.set("v.taskType",'Call');
        component.set("v.accountPriorityValue","");
        component.set("v.taskSelect","Call");
        
        if(!component.get('v.isRecursion')) {
            component.set("v.taskSubject","Call logged");
            component.set('v.isInit', false);
            helper.checkDraftCommentH(component, event, helper);
        }else{
            component.set('v.isRecursion', false);
        }
        component.set("v.openNewTaskModal",true);
        helper.focusOnCommentField(component,event, helper);

    },
    displayMeetingPopupHelper : function(component, event, helper) {

        component.set("v.taskType",'Meeting');
        component.set("v.accountPriorityValue","");
        component.set("v.taskSelect","Meeting");
        if(!component.get('v.isRecursion')) {
            component.set("v.taskSubject","Meeting");
            component.set('v.isInit', false);
            helper.checkDraftCommentH(component, event, helper);
        }else{
            component.set('v.isRecursion', false);
        }
        component.set("v.openNewTaskModal",true);
        helper.focusOnCommentField(component,event, helper);
    },
    displayVoicemailPopupHelper : function(component, event, helper) {
        component.set("v.taskType",'Voicemail');
        component.set("v.accountPriorityValue","");
        component.set("v.openNewTaskModal",true);
        //for setting voicemail in task type
        component.set("v.taskSelect","Voicemail");
        if(!component.get('v.isRecursion')) {
            component.set("v.taskSubject","Left voicemail");
            component.set('v.isInit', false);
            helper.checkDraftCommentH(component, event, helper);
        }else{
            component.set('v.isRecursion', false);
        }
        helper.focusOnCommentField(component,event, helper);
    },
    displayInboundEmailHelper : function(component, event, helper) {
        component.set("v.taskType",'Inbound Email');
        component.set("v.accountPriorityValue","");
        component.set("v.taskSelect","Inbound Email");
        if(!component.get('v.isRecursion')) {
            component.set("v.taskSubject","Inbound email");
            component.set('v.isInit', false);
            helper.checkDraftCommentH(component, event, helper);
        }else{
            component.set('v.isRecursion', false);
        }
        component.set("v.openNewTaskModal",true);
        helper.focusOnCommentField(component,event, helper);
    },
    displayOutboundEmailHelper : function(component, event, helper) {
        component.set("v.taskType",'Outbound Email');
        component.set("v.accountPriorityValue","");
        component.set("v.taskSelect","Outbound Email");
        
        if(!component.get('v.isRecursion')) {
            component.set("v.taskSubject","Outbound email");
            component.set('v.isInit', false);
            helper.checkDraftCommentH(component, event, helper);
        }else{
            component.set('v.isRecursion', false);
        }
        component.set("v.openNewTaskModal",true);
        helper.focusOnCommentField(component,event, helper);
    },
    displayExaminePopupHelper : function(component, event, helper) {
        component.set("v.taskType",'Examine');
        component.set("v.accountPriorityValue","");
        component.set("v.taskSelect","Examine");
        if(!component.get('v.isRecursion')) {
            component.set("v.taskSubject","Examine");
            component.set('v.isInit', false);
            helper.checkDraftCommentH(component, event, helper);
        }else{
            component.set('v.isRecursion', false);
        }
        component.set("v.openNewTaskModal",true);
        helper.focusOnCommentField(component,event, helper);
    },
    displayAssignPopupHelper : function(component) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Task",
            "recordTypeId" : component.get("v.taskRecordTypeId"),
            "defaultFieldValues": {
                
            }
        });
        createRecordEvent.fire();
    },
    reAssignUserHelper :function(component){
        component.set("v.openAssignModal",true);
        component.set("v.lossDropDisable",true);
        component.set("v.reassignType","reassign");
        component.set("v.lossDropReasonValue","Transfer");
        component.set("v.lossDropNotesValue","");
        
    },
    assignToCurrentUserHelper :function(component, helper){
        var action = component.get("c.assignCurrentUser");
        
        action.setParams({
            recordId:component.get("v.recordId")
        });
        
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               if (state === "SUCCESS")
                               {
                                   component.set("v.assignStatus","reassign");
                                   $A.get("e.force:refreshView").fire();
                                   helper.sendReassignEvent(component);
                               }
                               else if(state === "INCOMPLETE")
                               {
                                   this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
                               }
                                   else if(state === "ERROR")
                                   {
                                       var errors = response.getError();
                                       if(errors)
                                       {
                                           if(errors[0] && errors[0].message)
                                           {
                                               console.log("Error message: " + errors[0].message);
                                           }
                                       }else
                                       {
                                           console.log("Unknown error");
                                       }
                                   }
                           });
        $A.enqueueAction(action);
    },
    unAssignUserHelper :function(component){
        component.set("v.openAssignModal",true);
        component.set("v.lossDropReasonValue","");
        component.set("v.lossDropNotesValue","");
        component.set("v.reassignType","unAssign");
        setTimeout(function() {
            document.getElementById('lossDropSelectId').focus();
        }, 0);
    },
    toastErrorMessage : function (messageTitle, messageBody, messageType ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: messageTitle,
            message: messageBody,
            type: messageType ,
            mode: 'dismissible',
        });
        toastEvent.fire();
    },
    /*
        * ownership
    */
    handleWatchlistAndUntouchable : function(cmp, validationMap){
        var accDetails = validationMap.companyOwnerWithCurrentStatus.split('___');
        var watchList = validationMap.isCompanyOwnerAndUserSame && accDetails[1] && accDetails[1] == $A.get("$Label.c.IVP_Ownership_Status_One");
        var untouchable = validationMap.isCompanyOwnerAndUserSame && accDetails[1] && accDetails[1] == $A.get("$Label.c.IVP_Ownership_Status_Two");
        
        /**********************
                * Calling a method from controller to check if user has bypass and
                * If the user has bypass then we are setting the attribute value is Bypassed
                and using it to check validations on various cases
            **********************/
        
        var previousOwnerCheckData = cmp.get('v.previousOwnerCheckData');
        var isBypassOwnershipRequest = cmp.get('v.isBypassed');
        console.log('isBypassOwnershipRequest ',isBypassOwnershipRequest);
        
        //For collectiong error message
        var errorMsg = '';
        
        validationMap.showUntouchable = !validationMap.hasOpenRequest && watchList;
        validationMap.isUntouchable = false;
        validationMap.showWatchlist = !validationMap.hasOpenRequest && untouchable;
        validationMap.isWatchlist = false;
        //--> we were having f
        validationMap.watchlistMsg = 'Ctrl+Alt+d';//$A.get("$Label.c.IVP_Ownership_Demote_to_Watch_Button_label")
        if(untouchable === false){
            errorMsg = '';
            if(validationMap.untouchableCount <= 0){
                /**********************
                    * Updated the previous flow and checking if the user has bypass
                    * If the user has bypass then we are allowing the end user to make
                    a request even though the untouchable count is 0
                    **********************/
                if(!isBypassOwnershipRequest)
                {
                    validationMap.isUntouchable = true;
                    errorMsg += $A.get("$Label.c.IVP_Ownership_Priority_Balance_Error");
                }
            }
            if(errorMsg != ''){
                validationMap.untouchableMsg = errorMsg;
            }else{
                //--> we were having f
                validationMap.untouchableMsg = 'Ctrl+Alt+d';//$A.get("$Label.c.IVP_Ownership_Promote_to_Priority_Button_label")
            }
        }
        
        validationMap.showRequest = !validationMap.isCompanyOwnerAndUserSame && !(accDetails[0] == "true");
        if(accDetails.length > 1 
           && accDetails[1] == $A.get("$Label.c.IVP_Ownership_Status_Two")){
            validationMap.showRequest = false;
        }
        validationMap.isRequest = true;
        validationMap.assignStatus = cmp.get('v.assignStatus');
        errorMsg = '';
        if(validationMap.hasOpenRequest){
            if(validationMap.currentUserIsRequester){
                errorMsg += (errorMsg != '' ? '\n' : '') + $A.get("$Label.c.IVP_Ownership_Company_Request_Exist_Error").replace('{!user}', 'yourself');
            }else{
                errorMsg += (errorMsg != '' ? '\n' : '') + $A.get("$Label.c.IVP_Ownership_Company_Request_Exist_Error").replace('{!user}', 'another user');
            }
            validationMap.assignStatus = 'openrequest';
        }
        if(accDetails[1] == $A.get("$Label.c.IVP_Ownership_Status_Two")){
            errorMsg += (errorMsg != '' ? '\n' : '') + $A.get("$Label.c.IVP_Ownership_User_Company_Priority_Error");
        }
        if(!(validationMap.requestBalance > 0)){
            /**********************
            * Updated the previous flow and checking if the user has bypass
            * If the user has bypass then we are allowing the end user to make
            a request even though the requestBalance is 0
            **********************/
            if(!isBypassOwnershipRequest)
            {
                errorMsg += (errorMsg != '' ? '\n' : '') + $A.get("$Label.c.IVP_Ownership_Request_Balance_Error");
            }
        }
        if(!(validationMap.untouchableCount > 0)){
            /**********************
                * Updated the previous flow and checking if the user has bypass
                * If the user has bypass then we are allowing the end user to make
                * a request even though the untouchable count is 0
                *********************/
            if(!isBypassOwnershipRequest)
            {
                errorMsg += (errorMsg != '' ? '\n' : '') + $A.get("$Label.c.IVP_Ownership_Priority_Balance_Error");
            }
        }
        if( validationMap.watchlistCount <= 0){
            /**********************
                * Updated the previous flow and checking if the user has bypass
                * If the user has bypass then we are allowing the end user to make
                * a request even though the watch List count is 0
                **********************/
            if(!isBypassOwnershipRequest)
            {
                errorMsg += (errorMsg != '' ? '\n' : '') + $A.get("$Label.c.IVP_Ownership_Total_Balance_Error");
            }
        }
        if(errorMsg != ''){
            validationMap.requestMsg = errorMsg;
        }else{
            validationMap.isRequest = false;
            validationMap.requestMsg = 'Ctrl+Alt+w';//$A.get("$Label.c.IVP_Ownership_Request_Ownership_Button_label");
        }
        console.log('alidationMap.isCompanyOwnerAndUserSame1',validationMap.isCompanyOwnerAndUserSame);
        console.log('alidationMap.isCompanyOwnerAndUserSame2',validationMap.hasOpenRequest);
        console.log('alidationMap.isCompanyOwnerAndUserSame3',accDetails[1]); 
        console.log('alidationMap.isCompanyOwnerAndUserSame3',accDetails[1] == $A.get("$Label.c.IVP_Ownership_Status_Two"));
        console.log('alidationMap.isCompanyOwnerAndUserSame36',validationMap.showAcceptAndReject);  
        validationMap.showAcceptAndReject = !(accDetails[1] == $A.get("$Label.c.IVP_Ownership_Status_Two")) && validationMap.isCompanyOwnerAndUserSame && validationMap.hasOpenRequest;
        console.log('alidationMap.isCompanyOwnerAndUserSame36',validationMap.showAcceptAndReject);  
        if(validationMap.untouchableCount > 0){
            validationMap.isReject = false;
            validationMap.rejectMsg = 'Ctrl+Alt+j';//$A.get("$Label.c.IVP_Ownership_Reject_Request_Button_label");
        }else{
            /**********************
                * Updated the previous flow and checking if the user has bypass
                * If the user has bypass then we are allowing the end user to make
                * a request even though the untouchable count is 0
                **********************/
            if(!isBypassOwnershipRequest)
            {
                validationMap.isReject = true;
                validationMap.rejectMsg = $A.get("$Label.c.IVP_Ownership_Priority_Balance_Error");
            }
            else
            {
                validationMap.isReject = false;
                validationMap.rejectMsg = 'Ctrl+Alt+j';   
            }
        }
        
        validationMap.showOwnership = accDetails[0] == "true";
        //validationMap.isOwnership = validationMap.watchlistCount > 0 ? false : true;
        //validationMap.ownershipMsg = validationMap.watchlistCount > 0 ? "Ctrl+Alt+u": $A.get("$Label.c.IVP_Ownership_Total_Balance_Error");
        /**********************
            * Updated the previous flow and checking if the user has bypass
            * If the user has bypass then we are allowing the end user to make
            * a request even though the untouchable count is 0
            **********************/
        validationMap.isOwnership = validationMap.watchlistCount > 0 ? false : isBypassOwnershipRequest?false:true;
        validationMap.ownershipMsg = validationMap.watchlistCount > 0 ? "Ctrl+Alt+u": (isBypassOwnershipRequest? "Ctrl+Alt+u" : $A.get("$Label.c.IVP_Ownership_Total_Balance_Error"));
        if(previousOwnerCheckData.isValid === true){
            // drop/tranfer by current so have to restrict current user
            
            // disabling request ownership button
            validationMap.requestMsg = this.mergeMessage(
                validationMap.isRequest===false ? '': validationMap.requestMsg, previousOwnerCheckData.errMsg);
            validationMap.isRequest = true;
            
            // disabling assign button            
            validationMap.ownershipMsg = this.mergeMessage(
                validationMap.isOwnership===false ? '': validationMap.ownershipMsg, previousOwnerCheckData.errMsg);
            validationMap.isOwnership = true;
        }
        if(previousOwnerCheckData.isWaitToRepriortizeCheck === true){
            // disabling assign button 
            validationMap.untouchableMsg  = this.mergeMessage(
                validationMap.isUntouchable===false ? '': validationMap.untouchableMsg, previousOwnerCheckData.isWaitToRepriortizeError);
            validationMap.isUntouchable = true;
        }
        cmp.set("v.validationMap",validationMap);
    },
    mergeMessage : function(oldMsg, newMsg){
        return  (oldMsg ? oldMsg + '\n' : '') + newMsg;
    },
    handleChangeOwnership : function(cmp, hlpr){
        cmp.set("v.showSpinner",true);
        hlpr.handleCalling(cmp, hlpr, 'changeOwnership', {recordId : cmp.get("v.recordId")} , hlpr.changeOwnershipCB, null);
    },
    changeOwnershipCB : function(cmp, event, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        var validationWrapperMap = cmp.get("v.validationMap");
        // var watch = validationWrapperMap.watchlistCount;
        var data = hlpr.extractDataFromResponse(validationMap);
        if(data.isValid === true){
            //validationWrapperMap.showOwnership
            $A.get("e.force:refreshView").fire();
            hlpr.doInit(cmp, event, hlpr);
            cmp.set('v.assignStatus','reassign');
            hlpr.sendReassignEvent(cmp);
        }
        if(data.toastParams){
            hlpr.triggerToast(data.toastParams);
        }
        cmp.set("v.showSpinner",false);
    },
    extractDataFromResponse : function(result){
        var data={};
        if(result.errorMsg != ''){
            data.toastParams = {
                title: "Validation",
                message: result.errorMsg,
                type: "error"
            };
            data.isValid = false;
        }else if(result.successMsg != ''){
            data.toastParams = {
                title: "Success",
                message: result.successMsg,
                type: "success"
            };
            data.isValid = true;
        }
        return data;
    },
    handleRequestForOwnership : function(cmp, hlpr){
        cmp.set("v.showSpinner",true);
        var reason = cmp.get("v.reason");
        hlpr.handleCalling(cmp, hlpr, 'requestForOwnership', {recordId : cmp.get("v.recordId"), reason : reason} , hlpr.requestOwnershipCB, null);
        cmp.set("v.showInputBox", false);
    },
    requestOwnershipCB : function(cmp, event, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        
        var data = hlpr.extractDataFromResponse(validationMap);
        if(data.isValid === true){
            var validationWrapperMap = cmp.get("v.validationMap");
            validationWrapperMap.requestBalance = validationWrapperMap.requestBalance - 1;
            validationWrapperMap.hasOpenRequest = true;
            hlpr.handleWatchlistAndUntouchable(cmp, validationWrapperMap);
            cmp.set("v.reason", "");
            cmp.set("v.isChecked", false);
            $A.get("e.force:refreshView").fire();
            hlpr.doInit(cmp, event, hlpr);
        }
        if(data.toastParams){
            hlpr.triggerToast(data.toastParams);
        }
        cmp.set("v.showSpinner",false);
    },
    handleAcceptRequest : function(cmp, hlpr){
        cmp.set("v.showSpinner",true);
        var reason = cmp.get("v.reason");
        hlpr.handleCalling(cmp, hlpr, 'acceptRequest', {recordId : cmp.get("v.recordId"), status : 'Accepted', reason : reason} , hlpr.acceptRequestCB, null);
        cmp.set("v.showInputBox", false);
    },
    acceptRequestCB : function(cmp, event, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        var data = hlpr.extractDataFromResponse(validationMap);
        if(data.isValid === true){
            var validationWrapperMap = cmp.get("v.validationMap");
            validationWrapperMap.hasOpenRequest = false;
            hlpr.handleWatchlistAndUntouchable(cmp, validationWrapperMap);
            $A.get("e.force:refreshView").fire();
            cmp.set('v.assignStatus', 'assignStatus');
            hlpr.doInit(cmp, event, hlpr);
        }
        if(data.toastParams){
            hlpr.triggerToast(data.toastParams);
        }
        
        cmp.set("v.reason","");
        cmp.set("v.isChecked", false);
        cmp.set("v.isAccept", false);
        cmp.set("v.showSpinner", false);
    },
    handleRejectRequest : function(cmp, hlpr){
        cmp.set("v.showSpinner",true);
        var reason = cmp.get("v.reason");
        hlpr.handleCalling(cmp, hlpr, 'rejectRequest', {recordId : cmp.get("v.recordId"), reason : reason} , hlpr.rejectRequestCB, null);
        cmp.set("v.showInputBox", false);
    },
    rejectRequestCB : function(cmp, event, hlpr, response, callbackInfo){
        var validationMapWrap = response.getReturnValue();
        var toastParams = {};
        if(validationMapWrap.errorMsg != ''){
            toastParams = {
                title: "Validation",
                message: validationMapWrap.errorMsg,
                type: "error"
            };
        }else if(validationMapWrap.successMsg != ''){
            toastParams = {
                title: "Success",
                message: validationMapWrap.successMsg,
                type: "success"
            };
            $A.get("e.force:refreshView").fire();
            var validationMap = cmp.get("v.validationMap");
            validationMap.hasOpenRequest = false;
            hlpr.handleWatchlistAndUntouchable(cmp, validationMap);
            hlpr.doInit(cmp, event, hlpr);
        }
        hlpr.triggerToast(toastParams);
        cmp.set("v.reason","");
        cmp.set("v.isChecked",false);
        cmp.set("v.isReject",false);
        cmp.set("v.showSpinner",false);
    },
    checkInput : function(cmp, hlpr) {
        var isRequired = cmp.find('input').get("v.required");
        var reason = cmp.get("v.reason");
        if (!isRequired || (isRequired && reason != '')) {
            cmp.set("v.isChecked",true);
        } else {
            var element = cmp.find('input');
            if(element){
                element.reportValidity();
            }else{
                hlpr.showMessage({
                    title: "Validation",
                    message: "Complete this field.",
                    type: "error"
                })
            }
        }
    },
    handleCompanyStatusChange : function(cmp, hlpr, ownerStatus){
        cmp.set("v.showSpinner",true);
        var id = cmp.get("v.recordId");
        hlpr.handleCalling(cmp, hlpr, 'changeCompanyStatus', {recordId : id, status : ownerStatus} , hlpr.changeCompanyStatusCB, {Status : ownerStatus});
    },
    changeCompanyStatusCB : function(cmp, event, hlpr, response, callbackInfo){
        var validationMap = response.getReturnValue();
        var data = hlpr.extractDataFromResponse(validationMap);
        if(data.isValid === true){
            var validationWrapperMap = cmp.get("v.validationMap");
            validationWrapperMap.companyOwnerWithCurrentStatus = '___' + callbackInfo.Status;
            hlpr.handleWatchlistAndUntouchable(cmp, validationWrapperMap);
            $A.get("e.force:refreshView").fire();
            hlpr.doInit(cmp, event, hlpr);
        }
        if(data.toastParams){
            hlpr.triggerToast(data.toastParams);
        }
        cmp.set("v.showSpinner",false);
    },
    setFocus : function(cmp, eleId) {
        window.setTimeout(
            $A.getCallback(function() {
                var element = cmp.find(eleId);
                if(element){
                    element.focus();
                }
            }), 200
        );
    },
    shortcutErrorMessage : function(cmp, hlpr) {
        let toastParams = {
            title: "Error",
            message: $A.get("$Label.c.IVP_Ownership_Disable_Button_Error_Message"),
            type: "error"
        };
        hlpr.triggerToast(toastParams);
    },
    
    sendToPipeGreyOutErrorMessage : function(cmp, hlpr) {
        let toastParams = {
            title: "Error",
            message: $A.get("$Label.c.IVP_Send_To_Pipe_Disable_Button_Error_Message"),
            type: "error"
        };
        hlpr.triggerToast(toastParams);
    },
    
    fetchDataForFinancial: function(component, helper) {
        var action = component.get("c.fetchInitDataFinancials");
        
        action.setParams({
            strRecId:component.get("v.recordId")
        });
        
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var tempRes = response.getReturnValue();
                var res = JSON.parse(tempRes);
                var d = new Date();
                var dFullYear = d.getFullYear();
                //Date logic
                component.set("v.priorPriorYear",(dFullYear-2));
                component.set("v.priorYear",(dFullYear-1));
                component.set("v.currentYear",dFullYear);
                component.set("v.nextYear",(dFullYear+1));
                
                 component.set("v.priorPriorRevenueValue",res.priorPriorRevenueValue);
                 component.set("v.priorPriorEbitaValue",res.priorPriorEbitaValue);
                 component.set("v.priorRevenueValue",res.priorRevenueValue);
                 component.set("v.priorEbitaValue",res.priorEbYearValue);
                 component.set("v.currentRevenueValue",res.currentRevenueValue);
                 component.set("v.currentEbitaValue",res.currentEbYearValue);
                 component.set("v.nextRevenueValue",res.nextRevenueValue);
                 component.set("v.nextEbitaValue",res.nextEbYearValue);
                
                 component.set("v.priorPriorNetValue",res.priorPriorNetValue);
                 component.set("v.priorPriorGrossValue",res.priorPriorGrossValue);
                 component.set("v.priorNetValue",res.priorNetValue);
                 component.set("v.priorGrossValue",res.priorGrossValue);
                 component.set("v.currentNetValue",res.currentNetValue);
                 component.set("v.currentGrossValue",res.currentGrossValue);
                 component.set("v.nextNetValue",res.nextNetValue);
                 component.set("v.nextGrossValue",res.nextGrossValue);
                component.set("v.revEbtErrorMsg",res.revEbtErrorMsg);
                component.set("v.netGrossErrorMsg",res.netGrossErrorMsg);
                
                //Picklist Inits
                component.set("v.revenueOptions",res.lstRevenueTypeOptions);
                //component.set("v.totalFundingRaisedValue",null);
                component.set("v.totalFundingRaisedValue",(res.accountData.Total_Funding_Raised__c!=null?res.accountData.Total_Funding_Raised__c:res.accountData.DWH_Total_Funding_Raised__c))
                
                if(!$A.util.isEmpty(res.revenueType))
                {
                    component.set("v.revenueValue", res.revenueType);
                }
            }
            else if(state === "INCOMPLETE")
            {
            }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    saveTaskDataOnly: function(component, helper, taskDataJSONString)
    {
        helper.clearIntervalOnModalClose(component);
        var action = component.get("c.createNewTask");  
        
        action.setParams({
            recordId:component.get("v.recordId"),
            taskDataJSON: taskDataJSONString,
            objTaskCommentDraft: component.get('v.TaskCommentDraft')//added by V2Force for autosave functionality
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                component.set("v.taskSubject","");
                component.set("v.taskActivityDate","");
                component.set("v.taskNextAction", component.get("v.taskNextActionOnLoad"));
                component.set("v.taskNextActionDate", component.get("v.taskNextActionDateOnLoad"));
                //component.set("v.taskNextAction", ""); //today
                //component.set("v.taskNextActionDate", undefined);//todays
                //component.set("v.totalFundingRaisedValue",null); //today
                component.set("v.taskSelect","");
                component.set("v.accountPriorityValue","");
                component.set("v.openNewTaskModal",false);
                component.set("v.TaskCommentDraft",null);//added by V2force
                component.set('v.TaskCommentDraft',"");
                $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                $A.get("e.force:refreshView").fire();
                component.set("v.isSaving",false);
            }
            else if(state === "INCOMPLETE")
            {
                helper.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    component.set("v.isSaving",false);
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].pageErrors)
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: 'Error',
                                message: errors[0].pageErrors[0].message,
                                type: 'error' ,
                                mode: 'dismissible',
                            });
                            toastEvent.fire();
                            console.log("Error message: " + errors[0].pageErrors[0].message);
                        }
                        else if(errors[0] && errors[0].fieldErrors)
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: 'Error',
                                message: errors[0].fieldErrors[0].message,
                                type: 'error' ,
                                mode: 'dismissible',
                            });
                            toastEvent.fire();
                            console.log("Error message: " + errors[0].fieldErrors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                    $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                }
        });
        $A.enqueueAction(action);
        component.set("v.isModalClass", false);
    },
    
    saveTaskAndFinancialsData: function(component, helper, taskDataJSONString) {
       
        helper.clearIntervalOnModalClose(component);

        component.set("v.isModalClass", false);
        var action = component.get("c.saveData");  
        action.setParams({
            recordId:component.get("v.recordId"),
            taskDataJSON: taskDataJSONString,
            priorRevenueValue:component.get("v.priorRevenueValue"),
            currentRevenueValue:component.get("v.currentRevenueValue"),
            nextRevenueValue:component.get("v.nextRevenueValue"),
            priorEbitaValue:component.get("v.priorEbitaValue"),
            currentEbitaValue:component.get("v.currentEbitaValue"),
            nextEbitaValue:component.get("v.nextEbitaValue"),
            totalFundingRaisedValue:component.get("v.totalFundingRaisedValue"),
            priorPriorRevenueValue:component.get("v.priorPriorRevenueValue"),
            priorPriorEbitaValue:component.get("v.priorPriorEbitaValue"),
            strRevenueType:component.get("v.revenueValue"),
            priorNetValue:component.get("v.priorNetValue"),
            currentNetValue:component.get("v.currentNetValue"),
            nextNetValue:component.get("v.nextNetValue"),
            priorPriorNetValue:component.get("v.priorPriorNetValue"),
            priorGrossValue:component.get("v.priorGrossValue"),
            currentGrossValue:component.get("v.currentGrossValue"),
            nextGrossValue:component.get("v.nextGrossValue"),
            priorPriorGrossValue:component.get("v.priorPriorGrossValue"),
            objTaskCommentDraft: component.get("v.TaskCommentDraft")//added by V2Force for autosave functionality
        });
        
        action.setCallback(this,function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {  

                
                component.set("v.taskSubject","");
                //component.set("v.taskComments","");
                component.set("v.taskActivityDate","");
                component.set("v.taskNextAction", component.get("v.taskNextActionOnLoad"));
                component.set("v.taskNextActionDate", component.get("v.taskNextActionDateOnLoad"));
                //component.set("v.taskNextAction", ""); //today
                //component.set("v.taskNextActionDate",undefined);//todays
                //component.set("v.totalFundingRaisedValue",null); //today
                component.set("v.taskSelect","");
                component.set("v.accountPriorityValue","");
                component.set("v.openNewTaskModal",false);
                component.set("v.TaskCommentDraft",null);//added by V2force
                component.set("v.TaskCommentDraft","");
                $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                $A.get("e.force:refreshView").fire();
                component.set("v.isSaving",false);
                component.set("v.isTaskSaved",true);
            }
            else {
                
                var toastEvent = $A.get("e.force:showToast");
                var message = '';
                
                if (state === "INCOMPLETE") {
                    message = 'Server could not be reached. Check your internet connection.';
                } else if (state === "ERROR") {
                    component.set("v.isSaving",false);
                    var errors = response.getError();
                    if (errors) {
                        for(var i=0; i < errors.length; i++) {
                            for(var j=0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
                                message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
                            }
                            if(errors[i].fieldErrors) {
                                for(var fieldError in errors[i].fieldErrors) {
                                    var thisFieldError = errors[i].fieldErrors[fieldError];
                                    for(var j=0; j < thisFieldError.length; j++) {
                                        message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
                                    }
                                }
                            }
                            if(errors[i].message) {
                                message += (message.length > 0 ? '\n' : '') + errors[i].message;
                            }
                        }
                    } else {
                        message += (message.length > 0 ? '\n' : '') + 'Unknown error';
                    }
                }
                
                toastEvent.setParams({
                    title: 'Error',
                    type: 'error',
                    message: message
                });
                $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    saveNewTaskModalData :function(component,event ,helper){
        component.set("v.isModalClass", true);
        component.set("v.priorPriorRevenueValueCopy", component.get("v.priorPriorRevenueValue"));
        component.set("v.priorRevenueValueCopy", component.get("v.priorRevenueValue"));
        component.set("v.currentRevenueValueCopy", component.get("v.currentRevenueValue"));
        component.set("v.nextRevenueValueCopy", component.get("v.nextRevenueValue"));
        component.set("v.priorPriorEbitaValueCopy", component.get("v.priorPriorEbitaValue"));
        component.set("v.priorEbitaValueCopy", component.get("v.priorEbitaValue"));
        component.set("v.currentEbitaValueCopy", component.get("v.currentEbitaValue"));
        component.set("v.nextEbitaValueCopy", component.get("v.nextEbitaValue"));
        
        component.set("v.priorPriorNetValueCopy", component.get("v.priorPriorNetValue"));
        component.set("v.priorNetValueCopy", component.get("v.priorNetValue"));
        component.set("v.currentNetValueCopy", component.get("v.currentNetValue"));
        component.set("v.nextNetValueCopy", component.get("v.nextNetValue"));
        component.set("v.priorPriorGrossValueCopy", component.get("v.priorPriorGrossValue"));
        component.set("v.priorGrossValueCopy", component.get("v.priorGrossValue"));
        component.set("v.currentGrossValueCopy", component.get("v.currentGrossValue"));
        component.set("v.nextGrossValueCopy", component.get("v.nextGrossValue"));
        helper.saveNewTaskModalHelper(component,event,helper);
        
    },
    
    /**
        * @description Function to call check for draft comment and prepopulate it if present.
        * @param component
        * @param helper
        * @Author V2Force
        * @date 19/07/2019
    */
    checkDraftCommentH : function (component, event, helper){    
        
        var action = component.get("c.checkDraftComment");
        if(action != null || action != undefined){
            action.setParams({
                accountID: component.get('v.recordId')
            });
            
            action.setCallback(this, function(response) {
                
                component.set("v.taskActivityDate", component.get("v.todayDate"));
                
                if(response.getState() == 'SUCCESS'
                   && response.getReturnValue() != null) {
                    
                    var objTaskCommentDraft = response.getReturnValue();
                    
                    
                    
                    console.log('objTaskCommentDraft: ',objTaskCommentDraft);
                    
                    component.set('v.TaskCommentDraft', objTaskCommentDraft);
                    if(!component.get('v.isTaskSaved')){
                        component.set('v.taskComments', objTaskCommentDraft.Comment__c);
                        component.set("v.taskSubject",objTaskCommentDraft.Task_Comment_Subject__c);
                        if(objTaskCommentDraft.Activity_Date__c != null 
                            && objTaskCommentDraft.Activity_Date__c != '' 
                            && objTaskCommentDraft.Activity_Date__c != undefined){
                             
                             component.set("v.taskActivityDate", objTaskCommentDraft.Activity_Date__c);
                         }
                    }
                    else{

                        component.set('v.taskComments', '');
                        
                    }
                    
                    //component.set('v.TaskCommentDraft', "asdasd");
                    //component.set('v.taskComments', "");
                    //component.set("v.taskSubject",objTaskCommentDraft.Task_Comment_Subject__c);
                    console.log('>>>>>>>?????',component.get("v.taskSubject"));
                    component.set('v.discardButtonVisibility', true);
                    
                    if(component.get('v.isInit')) {
                        component.set('v.isInit', false);
                        
                        if(objTaskCommentDraft.Task_Type__c == 'Call') {
                            
                            component.set('v.isRecursion', true);
                            helper.displayCallPopupHelper(component, event, helper);
                        }
                        else if(objTaskCommentDraft.Task_Type__c == 'Meeting') {
                            
                            component.set('v.isRecursion', true);
                            helper.displayMeetingPopupHelper(component, event, helper);
                        }
                            else if(objTaskCommentDraft.Task_Type__c == 'Voicemail') {
                                
                                component.set('v.isRecursion', true);
                                helper.displayVoicemailPopupHelper(component, event, helper);
                            }
                                else if(objTaskCommentDraft.Task_Type__c == 'Inbound Email') {
                                    
                                    component.set('v.isRecursion', true);
                                    helper.displayInboundEmailHelper(component, event, helper);
                                }
                                    else if(objTaskCommentDraft.Task_Type__c == 'Outbound Email') {
                                        
                                        component.set('v.isRecursion', true);
                                        helper.displayOutboundEmailHelper(component, event, helper);
                                    }
                                        else if(objTaskCommentDraft.Task_Type__c == 'Examine') {
                                            
                                            component.set('v.isRecursion', true);
                                            helper.displayExaminePopupHelper(component, event, helper);
                                        }
                    }
                }
                else if(response.getState() == 'SUCCESS'
                        && response.getReturnValue() == null) {
                    
                    component.set('v.TaskCommentDraft', null);
                    component.set('v.taskComments', "");
                    component.set('v.discardButtonVisibility', false);
                    //component.set("v.taskNextAction", ""); //today
                    //component.set("v.taskNextActionDate",undefined); //today
                    //component.set("v.totalFundingRaisedValue",null); //today
                }
                if(!component.get('v.isInit')) {
                    helper.autoSaveComment(component, event, helper);
                }
            });
            $A.enqueueAction(action); 
        }
    },
    
    checkDraftCommentHT : function (component, event, helper){    
        
        var action = component.get("c.checkDraftComment");

        
        if(action != null || action != undefined){
            action.setParams({
                accountID: component.get('v.recordId')
            });
            
            action.setCallback(this, function(response) {
                component.set("v.talentTypeActivityDate", component.get("v.todayDate"));
                
                console.log('checkDraftCommentHT response: ',response.getState());
                console.log('checkDraftCommentHT getReturnValue: ',response.getReturnValue());

                if(response.getState() == 'SUCCESS'
                   && response.getReturnValue() == null) {
                        // component.set('v.isShowDefaultComment',true);
                        helper.prePopulateCommentValue(component, event, helper);
                }
                else if(response.getState() == 'SUCCESS'
                   && response.getReturnValue() != null) {
                    
                    component.set('v.isShowDefaultComment',false);
                    var objTaskCommentDraft = response.getReturnValue();
                    console.log('objTaskCommentDraft>>>',objTaskCommentDraft);
                    console.log('checkDraftCommentHT objTaskCommentDraft: ',objTaskCommentDraft.Activity_Date__c);
                    
                    if(objTaskCommentDraft.Activity_Date__c != null 
                       && objTaskCommentDraft.Activity_Date__c != '' 
                       && objTaskCommentDraft.Activity_Date__c != undefined){
                        
                        component.set("v.talentTypeActivityDate", objTaskCommentDraft.Activity_Date__c);
                    }
                    
                    component.set('v.TaskCommentDraft', objTaskCommentDraft);
                    component.set('v.talentTypeComment', objTaskCommentDraft.Comment__c);
                    console.log('Talent Comment in draft Comment: ', component.get("v.talentTypeComment"));
                    component.set("v.talentTypeSubject",objTaskCommentDraft.Task_Comment_Subject__c);
                    component.set("v.talentTypeActivityType",objTaskCommentDraft.Activity_Type__c);
                    component.set("v.talentTypeActivityDate",objTaskCommentDraft.Activity_Date__c);
                    component.set("v.talentTypeNextActionDate",objTaskCommentDraft.Next_Action_Date__c);
                    component.set("v.talentTypeNextAction",objTaskCommentDraft.Next_Action__c);

                    component.set("v.talentTaskIntExtVal",objTaskCommentDraft.Task_Int_Ext__c);
                    component.set("v.talentFlwUpIntExtVal",objTaskCommentDraft.FlwUp_Int_Ext__c);

                    console.log('isNextActionRequired: ',objTaskCommentDraft.isNextActionRequired__c);

                    var isNextActionRequiredValue = objTaskCommentDraft.isNextActionRequired__c ? 'Yes' : 'No';
                    component.set("v.isNextActionRequired",isNextActionRequiredValue);
                    component.set("v.showFollowUpDependentField",objTaskCommentDraft.isNextActionRequired__c);
            
                    component.set("v.isSignificant",objTaskCommentDraft.Significant__c);
                    component.set("v.talentTypeTaskStatus",objTaskCommentDraft.Next_Action_Status__c);
                    component.set('v.discardButtonVisibility', true);

                    var talentDiscussedPoints = [objTaskCommentDraft.Activity_Sales__c,objTaskCommentDraft.Activity_Marketing__c,
                        objTaskCommentDraft.Activity_Customer_Success__c,objTaskCommentDraft.Activity_Research_Development__c,
                        objTaskCommentDraft.Activity_Operations__c ];

                    
                    console.log('checkDraftCommentHT talentDiscussedPoints: ',talentDiscussedPoints);

                    if(component.get('v.isInit')) {
                        component.set('v.isInit', false);
                        helper.getCheckableFields(component, event, helper, talentDiscussedPoints, true);
                        helper.displayModelPopupHlpr(component, event, helper);                        
                    }

                    console.log('dynamicCheckBoxfields: ',component.get("v.dynamicCheckBoxfields"));

                    console.log('talentTypeActivityType: ',component.get("v.talentTypeActivityType"));
                    console.log('talentTaskIntExtVal: ',component.get("v.talentTaskIntExtVal"));
                    console.log('talentFlwUpIntExtVal: ',component.get("v.talentFlwUpIntExtVal"));
                    
                }
                else if(response.getState() == 'SUCCESS'
                        && response.getReturnValue() == null) {
                            
                    component.set('v.TaskCommentDraft', null);
                    component.set('v.talentTypeComment', "");
                    component.set('v.discardButtonVisibility', false);
                }
                if(!component.get('v.isInit')) {
                    
                    console.log('calling feom checkDraftCommentHT');
                    helper.autoSaveTalentComment(component, event, helper);
                }
            });
            $A.enqueueAction(action); 
        }
    },

    prePopulateCommentValue : function(component, event, helper){
        var action = component.get("c.getCommentValueToPrePopulate");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var currentTasktypeLabel = component.get("v.TaskTypeLabel");
                var res = response.getReturnValue();
                console.log('resultof prepopulate',res);
                component.set("v.talentTypeComment",res.Default_Comment_Text__c);
                if(currentTasktypeLabel == 'Examine' || currentTasktypeLabel == 'Task'){
                    component.set("v.talentTypeComment",'');
                }
                console.log('Talent Comment in PrePopulate: ', component.get("v.talentTypeComment"));
            }
            else if(state === "INCOMPLETE")
            {
                this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    
    /**
        * @description Function to call saveCommentAsDraft every 30 secs
        * @param component
        * @param event
        * @param helper
        * @Author V2Force
        * @date 19/07/2019
    */
    autoSaveComment: function (component, event, helper) {
        
        var action = component.get("c.fetchInterval");
        var refreshInterval;
        action.setCallback(this, function(response) {
            
            refreshInterval = response.getReturnValue();
            //execute saveCommentAsDraft() again after 30 sec each
            var intervalId = window.setInterval(
                $A.getCallback(function() { 
                    helper.saveCommentAsDraft(component,helper);
                }), refreshInterval
            );     
            component.set('v.intervalId', intervalId);
        });
        $A.enqueueAction(action); 
    },
    
    /**
        * @description Function to call saveCommentAsDraft apex method to insert or update draft comment.
        * @param component
        * @param helper
        * @Author V2Force
        * @date 19/07/2019
    */
    saveCommentAsDraft : function (component, helper){
        
        var status = navigator.onLine;
        if (window.localStorage != null && localStorage.getItem('runAutoSave') != 'false')
        {
            console.log('we can work with this')
        
        if (status) {
            
            var action = component.get("c.saveCommentAsDraft");
            if(action != null || action != undefined){
                var taskData = {};
                taskData.taskComments = component.get("v.taskComments");
                var tskActivityDate = document.getElementById("taskActivityDate");
                if(tskActivityDate != undefined)
                    taskData.taskActivityDate = tskActivityDate.value;
                taskData.taskNextAction = component.get("v.taskNextAction");
                var nextActionDate = document.getElementById("nextActionDate");
                if(nextActionDate != undefined || nextActionDate != null)
                    taskData.taskNextActionDate = nextActionDate.value;
                taskData.taskSubject = component.get("v.taskSubject");
                taskData.taskType = component.get("v.taskType");
                
                var taskJSONData = JSON.stringify(taskData);
                helper.updateFinancialData(component, helper);
                
                console.log('Saving in taskJSONData: ',taskJSONData);
                console.log('Saving in TaskCommentDraft: ',component.get('v.TaskCommentDraft'));
                
                action.setParams({
                    accountID: component.get("v.recordId"),
                    objTaskCommentDraft: component.get('v.TaskCommentDraft'),
                    taskId: null,
                    taskDataJSON: taskJSONData,
                    talentTaskData:null
                });
                
                action.setCallback(this, function(response) {

                    if(response.getReturnValue() != null
                       && response.getReturnValue().objDraftTaskComment != null 
                       && response.getReturnValue().state == 'SUCCESS') {
                        component.set('v.TaskCommentDraft', response.getReturnValue().objDraftTaskComment);
                        component.set('v.discardButtonVisibility', true);
                        helper.showMessage({title: "Draft Saved", message: $A.get("$Label.c.AutoSave_Success_Message"), type: "success" });
                    }
                    else if(response.getReturnValue() != null
                            && response.getReturnValue().state == 'ERROR')
                        helper.showMessage({title: "Draft Save Failed", message: "The comment failed to save as draft.", type: "error" });
                });
                $A.enqueueAction(action);
            }
        } else {
            helper.showMessage({title: "Draft Save Failed", message: $A.get("$Label.c.Network_Connectivity_Failure"), type: "error" });
        }
    } 
    },


    
    
    /**
        * @description Function to clear the interval.
        * @param component
        * @Author V2Force
        * @date 19/07/2019
    */
    clearIntervalOnModalClose : function (component){    

        var intervalId = component.get('v.intervalId');
        if(!$A.util.isEmpty(intervalId)) {     
            window.clearInterval(intervalId); 
        }
        
    },
    
    /**
        * @description Function to discard the draft comment.
        * @param component
        * @param event
        * @param helper
        * @Author V2Force
        * @date 22/07/2019
    */
    discardDraftComment: function(component, event, helper){
        var action = component.get("c.discardDraftTaskComment");
        console.log('dicard',component.get('v.TaskCommentDraft'));
        if(action != null || action != undefined){
            action.setParams({
                objTaskCommentDraft: component.get('v.TaskCommentDraft'),
            });
            
            action.setCallback(this, function(response) {
                if(response.getReturnValue() != null) {
                    component.set("v.taskComments","");
                    component.set('v.TaskCommentDraft', null);
                    component.set("v.taskNextAction", component.get("v.taskNextActionOnLoad"));
                    component.set("v.taskNextActionDate", component.get("v.taskNextActionDateOnLoad"));
                    
                    //component.set("v.taskActivityDate", component.get("v.todayDate"));
                    component.set('v.discardButtonVisibility', false);
                    // Talent
                    component.set('v.talentTypeActivityType', '');
                    component.set('v.talentTypeComment', '');
                    component.set('v.talentTypeNextAction', '');
                    component.set('v.talentTypeNextActionDate', '');
                    
                }
            });
            $A.enqueueAction(action); 
        }
       
    },
    
    
    /**
        * @description Function to close the prompt and new task modal.
        * @param component
        * @param event
        * @param helper
        * @Author V2Force
        * @date 23/07/2019
    */
    closeTaskModal: function(component, event, helper) {
        component.set('v.showPrompt', false);
        component.set('v.showfieldModelforTalent', false);
        helper.clearIntervalOnModalClose(component);
        helper.discardDraftComment(component, event, helper);
        component.set("v.lossDropDisable",false);
        component.set("v.openNewTaskModal",false);
        //component.set("v.taskNextAction", ""); //added today
        //component.set("v.taskNextActionDate",undefined); //added today
        //component.set("v.totalFundingRaisedValue",null); //added today
    },
    
    /**
        * @description Function to display prompt before closing the modal, if a draft comment exist.
        * @param component
        * @param event
        * @param helper
        * @Author V2Force
        * @date 07/08/2019
    */
    displayPrompt: function(component, event, helper) {
        var discardButtonVisibility = component.get('v.discardButtonVisibility');       
        if(discardButtonVisibility) {
            component.set('v.showPrompt', true);
            helper.clearIntervalOnModalClose(component);
        }
        else
            helper.closeTaskModal(component, event, helper);
    },
    
    /**
        * @description Function to open task modal and restart the autosave on closing the financial modal.
        * @param component
        * @param event
        * @param helper
        * @Author V2Force
        * @date 07/08/2019
    */
    closeAlertModal : function(component, event, helper) {
        component.set("v.isModalClass", false);
        component.set("v.showAlert",false);
        component.set("v.openNewTaskModal",true);
        helper.autoSaveComment(component, event, helper);
    },
    
    updateFinancialData : function(component, helper) {
        
        var action = component.get("c.updateFinancialData");
        
        if(action != null && action != undefined) {
            
            action.setParams({
                recordId:component.get("v.recordId"),
                priorRevenueValue:component.get("v.priorRevenueValue"),
                currentRevenueValue:component.get("v.currentRevenueValue"),
                nextRevenueValue:component.get("v.nextRevenueValue"),
                priorEbitaValue:component.get("v.priorEbitaValue"),
                currentEbitaValue:component.get("v.currentEbitaValue"),
                nextEbitaValue:component.get("v.nextEbitaValue"),
                totalFundingRaisedValue:component.get("v.totalFundingRaisedValue"),
                priorPriorRevenueValue:component.get("v.priorPriorRevenueValue"),
                priorPriorEbitaValue:component.get("v.priorPriorEbitaValue"),
                priorNetValue:component.get("v.priorNetValue"),
                currentNetValue:component.get("v.currentNetValue"),
                nextNetValue:component.get("v.nextNetValue"),
                priorPriorNetValue:component.get("v.priorPriorNetValue"),
                priorGrossValue:component.get("v.priorGrossValue"),
                currentGrossValue:component.get("v.currentGrossValue"),
                nextGrossValue:component.get("v.nextGrossValue"),
                strRevenueType:component.get("v.revenueValue"),
                priorPriorGrossValue:component.get("v.priorPriorGrossValue")
            });
            
            action.setCallback(this,function(response) {
                
            });
            $A.enqueueAction(action);
        }
        
    },
    
    focusOnCommentField : function(component, event, helper) {

        
        console.log('focusOnCommentField: ',component.find("taskCommentsId"));
        setTimeout(function() {
            if(component.find("taskCommentsId") != undefined && component.find("taskCommentsId") != null &&
            component.find("taskCommentsId") != ''){
//DK 1/30: commented out during debugging                
component.find("taskCommentsId").focus();
// document.getElementById('taskCommentsId').focus();
            }
        }, 1000);

        // window.setTimeout(
        //     $A.getCallback(function () {
        //         if(component.find("taskCommentsId") != undefined)
        //         component.find("taskCommentsId").focus();
        //     }), 2000
        // );
    },

    lkupFunc : function(cmp,evt)
    {
        var srchVal = evt.getParam('lkupStr');
        var objAPIName = evt.getParam('objAPIName');
        
        console.log('srchVal: ',srchVal);
        console.log('objAPIName: ',objAPIName);

        var recordData = {};

        var isTalentUser = cmp.get("v.IsTalentGroup");
        var recordId = cmp.get("v.recordId"); 

        recordData.isTalentUser = isTalentUser;
        recordData.recordId = recordId;

        console.log('recordData: ',JSON.stringify(recordData));

        // Need to add isTalent Param to setParams in other component where lkupFunc method used
        var action = cmp.get("c.typeAheadFuncLtng");
        action.setParams({
            "rName":srchVal,
            "sObjName":objAPIName,
            "filter":"",
            "recordData":JSON.stringify(recordData)
            
        });
        
        action.setCallback(this,function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS")
            {
                var res = response.getReturnValue();
                
                var tempRes = JSON.parse(JSON.stringify(res));
                
                evt.getSource().set('v.srchList',tempRes);
            }
            
            
            else if(state === "INCOMPLETE")
            {
                this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },

    autoSaveTalentComment: function (component, event, helper) {
        
        var action = component.get("c.fetchInterval");
        var refreshInterval;

        action.setCallback(this, function(response) {
            
            refreshInterval = response.getReturnValue();
            //execute saveCommentAsDraft() again after 30 sec each
            var intervalId = window.setInterval(
                $A.getCallback(function() { 
                    console.log('autoSaveTalentComment: ');
                    helper.saveTalentCommentAsDraft(component,helper);
                }), refreshInterval
            );     
            component.set('v.intervalId', intervalId);
        });
        $A.enqueueAction(action); 
    },

    loadCache: function(component, event, helper) {

        if(window.intialized != true) {
            return; 
        } 
        
        if(window.localStorage != null) {
            console.log('TaskType lexKey>>>> ',localStorage.getItem('TaskType'));
            component.set("v.TaskTypeLabel",localStorage.getItem('TaskType'));
            component.set("v.currentBtnDetail",localStorage.getItem('TaskType'));
        }
    },

    saveTalentCommentAsDraft : function (component, helper){
        var talentData ={};
        var isTaskBtn = component.get("v.TaskTypeLabel"); 

        console.log('>>>>isTaskBtn: ',isTaskBtn);

        if(window.intialized != true) {
            return; 
        } 
        console.log('>>>>runAutoSave>>>>',localStorage.getItem('runAutoSave'));

        var status = navigator.onLine;
        console.log('>>>>status>>>>',status);
        if (window.localStorage != null && localStorage.getItem('runAutoSave') != 'false') {
            if (status) {    
                var action = component.get("c.saveCommentAsDraft");
                if(action != null && action != undefined){
                    var taskData = {};

                    taskData.taskSubject = component.get("v.talentTypeSubject");
                    taskData.taskComments = component.get("v.talentTypeComment");
                    taskData.taskActivityDate = component.get("v.talentTypeActivityDate");
                    talentData.activityType = component.get("v.talentTypeActivityType");

                    taskData.taskNextAction = component.get("v.talentTypeNextAction");
                    taskData.taskNextActionDate = component.get("v.talentTypeNextActionDate");
                    
                    const myCheckboxes = component.find('myCheckboxes'); 
                    talentData.taskType = component.get("v.currentBtnDetail");

                    talentData.talentTaskNextAction = component.get("v.talentTypeNextAction");
                    talentData.talentTaskNextActionDate = component.get("v.talentTypeNextActionDate");

                    talentData.taskIntExtVal = component.get("v.talentTaskIntExtVal");
                    talentData.flwUpIntExtVal = component.get("v.talentFlwUpIntExtVal");

                    console.log('isNextActionRequired: ',component.get("v.isNextActionRequired"));
                    talentData.isNextActionRequired = component.get("v.isNextActionRequired") == 'Yes' ? true : false;
                    talentData.isSignificant = component.get("v.isSignificant");
                    talentData.nextActionStatus = component.get("v.talentTypeTaskStatus");
                    if(talentData.taskType != 'Text'){
                        window.localStorage.setItem('TaskType', talentData.taskType);
                    }

                    if(component.get("v.TaskTypeLabel") != "" && talentData.taskType == 'Text'){
                        talentData.taskType = component.get("v.TaskTypeLabel");
                    }
                    
                    if(myCheckboxes != undefined && myCheckboxes != null &&  myCheckboxes != '' ){
                        let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
                        for(var i=0 ;i<myCheckboxes.length;i++){

                            console.log('>>chk>>>',chk[i].get('v.checked'));
                            console.log('>>chk label>>>',chk[i].get('v.value'));

                            if(chk[i].get('v.value') == 'Sales__c'){
                                talentData.activitySales = chk[i].get('v.checked');
                            }
                            if(chk[i].get('v.value') == 'Customer_Success__c'){
                                talentData.activityCustomerSuccess = chk[i].get('v.checked');
                            }
                            if(chk[i].get('v.value') == 'Research_Development__c'){
                                talentData.activityResearchDevelopment = chk[i].get('v.checked');
                            }
                            if(chk[i].get('v.value') == 'Marketing__c'){
                                talentData.activityMarketing = chk[i].get('v.checked');
                            }
                            if(chk[i].get('v.value') == 'Operations__c'){
                                talentData.activityOperations = chk[i].get('v.checked');
                            }
                            
                        }
                    }
                   if(component.get("v.TaskTypeLabel") == 'Examine' && component.get("v.isNextActionRequired") != 'Yes'){
                                talentData.activitySales = false;
                                talentData.activityCustomerSuccess=false;
                                talentData.activityResearchDevelopment=false;
                                talentData.activityMarketing=false;
                                talentData.activityOperations=false;
                    }
                    
                    console.log('taskData after set----->',JSON.stringify(taskData));
                    console.log('TaskCommentDraft after set----->',JSON.stringify(component.get('v.TaskCommentDraft')));
                    var taskJSONData = JSON.stringify(taskData);
                    //helper.updateFinancialData(component, helper);
                    console.log('talentData----->',talentData);
                    console.log('talentData----->',JSON.stringify(talentData));
                    
                    action.setParams({
                        accountID: component.get('v.recordId'),
                        objTaskCommentDraft: component.get('v.TaskCommentDraft'),
                        taskId: null,
                        taskDataJSON: taskJSONData,
                        talentTaskData:JSON.stringify(talentData)
                    });
                    
                    action.setCallback(this, function(response) {
                        var state = response.getState();

                        console.log('saveTalentCommentAsDraft-State--->',state);
                        console.log('saveTalentCommentAsDraft---res->',response.getReturnValue());

                        console.log('1631TaskCommentDraft---->'+component.get('v.TaskCommentDraft'));
                        if(response.getReturnValue() != null
                        && response.getReturnValue().objDraftTaskComment != null 
                        && response.getReturnValue().state == 'SUCCESS') {
                            console.log('>>>>Success>>>');
                            component.set('v.TaskCommentDraft', response.getReturnValue().objDraftTaskComment);
                            component.set('v.discardButtonVisibility', true);
                            helper.showMessage({title: "Draft Saved", message: $A.get("$Label.c.AutoSave_Success_Message"), type: "success" });
                        }
                        else if(response.getReturnValue() != null
                                && response.getReturnValue().state == 'ERROR'){
                            console.log('>>>>Success>>>');
                            helper.showMessage({title: "Draft Save Failed", message: "The comment failed to save as draft.", type: "error" });
                        }
                        
                    });
                    $A.enqueueAction(action);
                }
            } else {
                helper.showMessage({title: "Draft Save Failed", message: $A.get("$Label.c.Network_Connectivity_Failure"), type: "error" });
            }
        }
    },
    
    saveNewTaskModalOfTalent : function(component, event, helper, selecetdMeetingAttendees){

        
		console.log('checkedValue: ',component.get("v.checkedValue"));
        var checkedValue = component.get("v.checkedValue");
        
        console.log('selecetdMeetingAttendees: ',selecetdMeetingAttendees);
        var spinner = component.find('initSpinner');
        console.log('>>spinner>>>',spinner);
        
        if(spinner != undefined && spinner != null && spinner != ''){
            $A.util.removeClass(spinner, "slds-hide"); 
        }
            
        helper.clearIntervalOnModalClose(component);
        component.set("v.isSaving",true);
        var taskData = {};
        var salecheckBox;
        var cusSucCheckBox;
        var resAndDevCheckBox;
        var marketingCheckBox;
        var OperationCheckBox;
        const myCheckboxes = component.find('myCheckboxes'); 
        console.log('>>myCheckboxes>>>',myCheckboxes);
        
        if(myCheckboxes != undefined && myCheckboxes != null && myCheckboxes != ''){
            let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
            console.log('>>chk>>>',chk);
            for(var i=0 ;i<myCheckboxes.length;i++){
            console.log('>>chk>>>',chk[i].get('v.checked'));
            console.log('>>chk label>>>',chk[i].get('v.value'));
            if(chk[i].get('v.value') == 'Sales__c'){
                salecheckBox = chk[i].get('v.checked');
            }
            if(chk[i].get('v.value') == 'Customer_Success__c'){
                cusSucCheckBox = chk[i].get('v.checked');
            }
            if(chk[i].get('v.value') == 'Research_Development__c'){
                resAndDevCheckBox = chk[i].get('v.checked');
            }
            if(chk[i].get('v.value') == 'Marketing__c'){
                marketingCheckBox = chk[i].get('v.checked');
            }
            if(chk[i].get('v.value') == 'Operations__c'){
                OperationCheckBox =  chk[i].get('v.checked');
            }
            
        }
        }

        console.log('Saving Tast Type: ',component.get("v.TaskTypeLabel"));
        //chk.forEach(checkbox => checkbox.set('v.checked', component.get('v.isAllSelected')));
        var selectedTaskType = component.get("v.currentBtnDetail");

        console.log('selectedTaskType: ',selectedTaskType);

        taskData.taskType = component.get("v.TaskTypeLabel");

        taskData.taskSubject = component.get("v.talentTypeSubject");
        taskData.taskComments = component.get("v.talentTypeComment");
        taskData.taskActivityType = component.get("v.talentTypeActivityType");
        taskData.taskActivityDate = component.get("v.talentTypeActivityDate");
        taskData.taskNextAction = component.get("v.talentTypeNextAction");
        taskData.objTaskCommentDraft= component.get('v.TaskCommentDraft');
        taskData.taskNextActionDate = component.get("v.talentTypeNextActionDate");
        taskData.salecheckBox =  salecheckBox; 
        taskData.cusSucCheckBox =  cusSucCheckBox;
        taskData.resAndDevCheckBox =  resAndDevCheckBox ;
        taskData.marketingCheckBox =  marketingCheckBox ;
        taskData.OperationCheckBox = OperationCheckBox;
        taskData.taskIntExtVal = component.get("v.talentTaskIntExtVal");
        taskData.flwUpIntExtVal = component.get("v.talentFlwUpIntExtVal");
        
        var nextActionOwnerObj = component.get("v.nextActionOwnerObject");
        var nextActionOwnerId = nextActionOwnerObj != null ? nextActionOwnerObj.rId : null;
        var nextActionOwnerName = nextActionOwnerObj!= null? nextActionOwnerObj.rName : null;
        console.log('nextActionOwnerObj: ',nextActionOwnerObj);
        console.log('talentStatus: ',component.get("v.talentTypeTaskStatus"));

        console.log('saveNewTaskModalOfTalent: ',JSON.stringify(taskData));
        
        if(component.get("v.talentTypeTaskStatus") =='' || component.get("v.talentTypeTaskStatus")== undefined ){

            component.set("v.talentTypeTaskStatus",'Not Started');
        }
        var action = component.get("c.createTalentNewTask");  
        
        action.setParams({
            recordId:component.get("v.recordId"),
            taskDataJSON: JSON.stringify(taskData),
            salecheckBox:    salecheckBox ,
            cusSucCheckBox:     cusSucCheckBox ,
            resAndDevCheckBox:   resAndDevCheckBox ,
            marketingCheckBox:  marketingCheckBox ,
            OperationCheckBox:   OperationCheckBox ,
            objTalentTaskCmntAsDraft : component.get('v.TaskCommentDraft'),
            meetingAttendeesList: selecetdMeetingAttendees,
            taskIntExt : component.get("v.talentTaskIntExtVal"),
            flwUpIntExtVal : component.get("v.talentFlwUpIntExtVal"),
            nextActionOwnerId : nextActionOwnerId,
            isNextActionRequired : component.get("v.isNextActionRequired") == 'Yes'?true:false,
            isSignificant : component.get("v.isSignificant") ,
            nextActionStatus : component.get("v.talentTypeTaskStatus"),
            nextActionOwnerName : nextActionOwnerName,
            checkedValue : checkedValue
        });
        // objTaskCommentDraft: component.get('v.TaskCommentDraft')//added by V2Force for autosave functionality
        action.setCallback(this,function(response){ 
            var state = response.getState();
            console.log('saveNewTaskModalOfTalent State: ',state);  

            if (state === "SUCCESS")
            {
                component.set("v.showFollowUpDependentField",false);
                console.log('inside save success');
                component.set("v.currentBtnDetail","");
                component.set("v.talentFlwUpIntExtVal","");
                
                component.set("v.talentTypeSubject","");
                component.set("v.talentTypeComment",""); 
                component.set("v.taskcomments","");                      
                component.set("v.talentTypeActivityType","");
                component.set("v.taskNextActionDate",undefined);
                component.set("v.talentTypeActivityDate","");
                component.set("v.talentTypeNextAction","");
                component.set("v.showfieldModelforTalent",false);
                component.set("v.isSaving",false);
                component.set("v.isAllSelected",false);
                // $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                $A.get("e.force:refreshView").fire();
                component.set("v.isSaving",false);
                component.set("v.checkedValue",false);
                $A.util.addClass(spinner, "slds-hide");
            }
            else if(state === "INCOMPLETE")
            {
                helper.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    component.set("v.isSaving",false);
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].pageErrors)
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: 'Error',
                                message: errors[0].pageErrors[0].message,
                                type: 'error' ,
                                mode: 'dismissible',
                            });
                            toastEvent.fire();
                            console.log("Error message: " + errors[0].pageErrors[0].message);
                        }
                        else if(errors[0] && errors[0].fieldErrors)
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: 'Error',
                                message: errors[0].fieldErrors[0].message,
                                type: 'error' ,
                                mode: 'dismissible',
                            });
                            toastEvent.fire();
                            console.log("Error message: " + errors[0].fieldErrors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                    $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
                }
        });
        $A.enqueueAction(action);
    },

    displayModelPopupHlpr : function(component, event, helper){        
        component.set("v.showfieldModelforTalent", true);
        var action = component.get("c.getFieldsDetail");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                component.set("v.dynamicfields", response.getReturnValue());

                 if(component.get("v.isShowDefaultComment")){
                     helper.prePopulateCommentValue(component, event, helper);
                }
                
                helper.focusOnCommentField(component, event, helper);
            }
            else if(state === "INCOMPLETE")
            {
                this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },

    getCheckableFields : function(component, event, helper, getCheckableFields, isFromDraftTask){
        var arrayForIntExt = [];
        var action = component.get("c.getCheckableFields");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var res = response.getReturnValue();
                console.log('resValues',res);
                console.log('getCheckableFields res: ',res.lstOfCheckBoxes);
                var checkListVal = res.lstOfCheckBoxes;
                
                if(isFromDraftTask){
                    for(var i = 0; i <  checkListVal.length; i++){
                            checkListVal[i].fieldValue = getCheckableFields[i] == true ? true : false;
                    }
                }
                else{
                    for(var i = 0; i <  checkListVal.length; i++){
                        checkListVal[i].fieldValue = false;
                    }
                }
                for(var i=1;i<res.internalExternalValues.length;i++){
                    arrayForIntExt.push(res.internalExternalValues[i]);
                }
                console.log('followUpIntExtValuesarrayForIntExt',arrayForIntExt);
                console.log('getCheckableFields checkListVal: ',checkListVal);
                component.set("v.dynamicCheckBoxfields", checkListVal);
                component.set("v.activityTypeValues", res.activityType);
                component.set("v.activitystatusValues", res.activitystatus);
                component.set("v.taskIntExtValues",arrayForIntExt);
                console.log('taskIntExtValues', component.get("v.taskIntExtValues"));

                component.set("v.followUpIntExtValues",res.internalExternalValues);
                console.log('followUpIntExtValues', component.get("v.followUpIntExtValues"));
                component.set("v.significantValues",res.significantValues);
                //component.set("v.defaultValueForNextAction",res.followUpIntExt[1]);

                console.log('getCheckableFields dynamicCheckBoxfields: ',component.get("v.dynamicCheckBoxfields"));
            }
            else if(state === "INCOMPLETE")
            {
                this.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }
                else if(state === "ERROR")
                {
                    var errors = response.getError();
                    if(errors)
                    {
                        if(errors[0] && errors[0].message)
                        {
                            console.log("Error message: " + errors[0].message);
                        }
                    }else
                    {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    
    removeFocus : function(component){
        setTimeout(function() {
            if(component.find("taskCommentsId") != undefined)
                component.find("taskCommentsId").blur();
        }, 0);
    },

    isNull : function(value){
        console.log('value: ',value);
        return value == null || value == undefined || value == '' ? true : false; 
    },
    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
        "title": "ERROR!",
        type : 'error',
        "message": "Please fill all the Required Fields!"
    });
    toastEvent.fire();
    },
    sendReassignEvent : function(component) {
        //Fire Application Event to signal Ownership change
        console.log('sending ownership change event');
        var messageEvent = $A.get("e.c:MessageEvent");
        if(messageEvent!=null) {
            if(component.get("v.assignStatus") == "reassign"){
                messageEvent.setParam("message", "COMPANY_ASSIGNED");
            }
            else {
                messageEvent.setParam("message", "COMPANY_UNASSIGNED");
            }
            console.log('sending ownership change event; assignStatus = ' + component.get("v.assignStatus"));
            messageEvent.fire();                            
        }
    }
    
})