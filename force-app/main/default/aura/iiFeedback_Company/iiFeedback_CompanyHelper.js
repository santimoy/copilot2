({
	getCurUserId:function(cmp){
        let curUserId = cmp.get('v.userId');
        if(curUserId){
            return curUserId;
        }
        curUserId = $A.get("$SObjectType.CurrentUser.Id");
        cmp.set('v.userId',curUserId);
        return curUserId;
    },
    getCurUserEmail:function(cmp){
        let curUserEmail = cmp.get('v.userEmail');
        if(curUserEmail){
            return curUserEmail;
        }
        curUserEmail = $A.get("$SObjectType.CurrentUser.Email");
        cmp.set('v.userEmail',curUserEmail);
        return curUserEmail;
    },
    handleCalling : function(cmp, hlpr, method, params, callback, errorCB) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        let action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                if(callback){
                    callback(cmp, hlpr, response);
                }
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({message:'Please refresh the page!', type:"error"});
            }else if (state === "ERROR") {
                let errors = response.getError();
                hlpr.handleErrors(errors);
                if(errorCB){
                    errorCB(cmp, hlpr);
                }
            }
        })
        //this is actually use to call server side method
        $A.enqueueAction(action);
    },
    
    jsInit:function(cmp, evt, hlpr){
        hlpr.handleCalling(cmp, hlpr, 'getFeedbackData',{}, hlpr.jsInitCB);
    },
    jsInitCB:function(cmp, hlpr, response){
        let result = response.getReturnValue();
        let icons = result.icons;
        let iconMap = {};
        let srcPath = cmp.get('v.srcPath');
        cmp.set('v.dwhSetting', result.dwhSetting);
        cmp.set('v.orgId', result.org.Id);
        
        icons.forEach(function(icon){
            if(icon.Icon__c)
                icon.iconPath = srcPath+'/'+icon.Record_Level_Icon__c;
            if(!iconMap[icon.Indicator__c]){
                iconMap[icon.Indicator__c] ={};
            }
            iconMap[icon.Indicator__c][icon.Value__c] = icon;
        });
        
        let iiFeedback = hlpr._defaultFeedback();
        cmp.set('v.iconMap', iconMap);
        cmp.set('v.iiFeedback', iiFeedback);
        hlpr.doAPICall(cmp, hlpr);
    },
    doAPICall: function(cmp, hlpr) {
        var req = new XMLHttpRequest();
        let dwhSetting = cmp.get('v.dwhSetting');
        let dwhKey = dwhSetting.Auth_Key__c;
        var url = dwhSetting.Endpoint__c;
        if($A.util.isUndefinedOrNull(url) || $A.util.isEmpty(url) 
           || $A.util.isUndefinedOrNull(dwhKey) || $A.util.isEmpty(dwhKey) ){
            
            hlpr.displayMessage('info', 'Data Ware house details missing!');
            return;
        }
        url+='/feedback/sf/info?';
        url +='user_id='+hlpr.getCurUserId(cmp)
        +'&company_id='+cmp.get('v.recordId')
        +'&id_app_name=SF&org_id='+cmp.get('v.orgId');
        req.open('GET', url);
        req.setRequestHeader ('Authorization', dwhKey);
        req.timeout = 30000;
        let logData={url:url, method:'GET', body:{},timeout:30000}
        req.onreadystatechange = function() {
            if(req.readyState === 4 ) {
                cmp.set('v.isLoading', false);
                if(req.status == 200){
                    cmp.set('v.showIcons', true);
                    var res = JSON.parse(req.responseText);
                    hlpr.processResponse(cmp, hlpr, res);
                }else if(req.status === 404 ){
                    cmp.set('v.showIcons',false);
                }else{
                    hlpr.jsLogAJAX(cmp, hlpr, logData);
                }
            }else if(req.readyState === 0){
                cmp.set('v.isLoading', false);
            }
        }
        cmp.set('v.isLoading', true);
        try{
            req.send();
        }catch(error){
            console.log(error);
        }
    },
    processResponse:function(cmp, hlpr, res){
        let data = res;
        let iiFeedback = cmp.get('v.iiFeedback');
        if(data.thumbs_info){
            iiFeedback.ThumbsUp.value = data.thumbs_info.value;
            iiFeedback.ThumbsUp.opacity = data.thumbs_info.fade_level;
        }
        if(data.likely_to_raise){
            iiFeedback.LikelyToRaise.value = data.likely_to_raise.value;
        }
        if(data.responsive_data){
            iiFeedback.ResponsiveNess.value = data.responsive_data.value;
            iiFeedback.ResponsiveNess.opacity = data.responsive_data.fade_level;
        }
        if(data.insight_match){
            iiFeedback.InsightMatch.value = data.insight_match.value;
        }
        if(data.last_week_activity){
            iiFeedback.SignalAction.value = data.last_week_activity.value;
        }
        if(data.preferred_company){
            iiFeedback.PreferenceCenter.value= data.preferred_company.value;
        }
        cmp.set('v.iiFeedback', iiFeedback);
    },
    _defaultFeedback:function(){
        return{
            'ResponsiveNess':{value:null},'ThumbsUp':{value:0}, 'SignalAction' :{value:0},
            'InsightMatch':{value:0},'LikelyToRaise':{value:0},'PreferenceCenter':{value:0} 
        };
    },
    jsLogAJAX:function(cmp, hlpr, data){
        hlpr.handleCalling(cmp, hlpr, 'logAjax',{dataJSON:JSON.stringify(data)}, hlpr.jsLogAJAXCB);
    },
    jsLogAJAXCB:function(cmp, hlpr, response){
        hlpr.displayMessage('info', response.getReturnValue());
    },
    displayMessage : function(type,msg){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams({
                "type":type,
                "message":msg,
            });
            toastEvent.fire();
        }else{
            alert(msg);
        }
    },
})