({
    
    loadTaskComment : function(component, helper) {
        var action = component.get("c.fetchTaskComment");
        action.setParams({
			taskID: component.get('v.recordId')
        });
        
		action.setCallback(this, function(response) {
		    if(!$A.util.isEmpty(response.getReturnValue().Id)) {
            
                component.set('v.showRichText', true);
                component.set('v.taskCommentRecord', response.getReturnValue());
            } 
            else{
                component.set('v.taskCommentRecord', null);
                component.set('v.showRichText', false);
            }
        });
		$A.enqueueAction(action);
    },
    updateTaskComment : function(component, event, helper) {
        
        var action = component.get("c.updateTaskComment");
        action.setParams({
            objTaskComment: component.get('v.taskCommentRecord'),
            objTask: component.get('v.taskRecord')
        });
        
		action.setCallback(this, function(response) {
            component.set('v.showSpinner', false); 
            if (response.getState() === "SUCCESS") {
                
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
                if(component.get("v.showRichText"))
                    window.location.reload();
            }
        });
        component.set('v.showSpinner', true); 
		$A.enqueueAction(action); 
    },
    
    updateTaskCommentWithPlainText : function(component,helper) {
        // var tempArray = {};
        var action = component.get("c.fetchTaskPlainTextComment");
        action.setParams({
			taskID: component.get('v.recordId')
        });
        
        action.setCallback(this, function(response) {

            if(response.getReturnValue() != null) {
                component.set('v.taskRecord', response.getReturnValue());
            } 

        //    var  tempResults = response.getReturnValue();
        //    for(var i=0;i< tempResults.length;i++){
        //     tempArray.isTalentUser = tempResults[i].isTalentUser;
        //     tempArray.Description = tempResults[i].updatedTaskComment.Description;

        //    }
        //    console.log('tempArray.Description',tempArray.Description);

        //     if(response.getReturnValue() != null && tempArray.isTalentUser==true) {
        //         component.set('v.taskRecord',tempArray.Description);
        //         component.set('v.showRichText', tempArray.isTalentUser);
                
        //     }
        //     else if(response.getReturnValue() != null && tempArray.isTalentUser==false){
        //         component.set('v.taskRecord',tempArray.Description);
        //         component.set('v.showRichText', tempArray.isTalentUser);
        //     }
             
        });
		$A.enqueueAction(action); 
    }
})