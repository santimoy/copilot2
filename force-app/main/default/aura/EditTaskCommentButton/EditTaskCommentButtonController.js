({
    onInit: function (component, event, helper) {
      
        helper.loadTaskComment(component, helper);
        helper.updateTaskCommentWithPlainText(component,helper);
    },
    closeEditTaskCommentModal: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    },
    saveTaskComment: function (component, event, helper) {
        helper.updateTaskComment(component, event, helper);
    }
})