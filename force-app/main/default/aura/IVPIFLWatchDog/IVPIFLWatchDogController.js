({
	handleAccordion: function(component, event, helper) {
        event.currentTarget.classList.toggle('slds-is-open');
        let accOpen = component.get('v.accOpen');
        component.set('v.accOpen', !accOpen);
    }
})