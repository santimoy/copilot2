({
    scriptsLoaded : function(component,event,helper){
        component.set("v.Spinner", true);
        helper.getData(component,event,helper);
    },
    refresh : function(component,event,helper){
        component.set("v.showTable",false);
        component.set("v.Spinner", true);
        component.set("v.displayDT", false);
        component.set("v.isActivePreference",true);
        // to destroy jsDatatable
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.displayDT", true);
            }), 500
        );
        // to re-initialize jsDatatable
        window.setTimeout(
            $A.getCallback(function() {
                helper.configDatatable(component,event,helper);
            }), 1000
        );
    },
    doInit : function(component,event,helper){
        document.title = "Just For You";
    },
    openModal : function(component, event, helper) {
        component.set("v.Spinner", true);
        $A.createComponent( 'c:Configure_Columns', {
            'callFrom':'JustForYou',
            'isReferesh':component.getReference("v.isReferesh")
        },function(modalComponent, status, errorMessage) {
            if (status === "SUCCESS") {
                var body = component.find( 'showConfigureModal' ).get("v.body");
                body.push(modalComponent);
                component.find( 'showConfigureModal' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
                console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
                console.log('error');
            }
        });
        component.set("v.Spinner", false);
    },
    setPreferenceApi : function(component, event, helper){       
        var preferenceId = event.getParam("preferenceId");
        component.set("v.preferenceId",preferenceId);
        component.set("v.showTable",false);
        component.set("v.Spinner", true);
        component.set("v.displayDT", false);
        let noactivePreference = component.find('noactivePreference');
        let hideTable = false;
        // to destroy jsDatatable
        if(preferenceId){
            component.set("v.isAllPreference", false);
            hideTable = true;
        }else{
            component.set("v.isAllPreference", true);
        }
        if(noactivePreference){
            if(hideTable){
                $A.util.addClass(noactivePreference,'slds-hide');
            }else{
                $A.util.removeClass(noactivePreference,'slds-hide');
            }
        }
        // to destroy jsDatatable
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.displayDT", true);
            }), 500
        );
        // to re-initialize jsDatatable
        window.setTimeout(
            $A.getCallback(function() {
                helper.setPreferenceApi(component, event, helper);
            }), 1000
        );
    },
})