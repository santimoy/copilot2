({
    getSorryCSS:function(cmp){
        cmp.set('v.customCSS','')
    },
    getCurUserId:function(cmp){
        let curUserId = cmp.get('v.userId');
        if(curUserId){
            return curUserId;
        }
        curUserId = $A.get("$SObjectType.CurrentUser.Id");
        cmp.set('v.userId',curUserId);
        return curUserId;
    },
    getCurUserEmail:function(cmp){
        let curUserEmail = cmp.get('v.userEmail');
        if(curUserEmail){
            return curUserEmail;
        }
        curUserEmail = $A.get("$SObjectType.CurrentUser.Email");
        cmp.set('v.userEmail',curUserEmail);
        return curUserEmail;
    },
    removeFeedbackCols:function(cmp, hlpr, data){
        var feedbackData = hlpr.feedbackInitCols();
        var feedbackIIFields = feedbackData.feedbackIIFields 
        var feedbackSource=feedbackData.feedbackSource 
        for(let fKey in feedbackIIFields){
            let fInd = data.fApiList.indexOf(fKey);
            if(fInd > -1){
                data.fApiList.splice(fInd,1);
                data.fLabelList.splice(fInd,1);
            }
        }
    },
    getData : function(component,event,helper) {
        var action = component.get("c.getColumnList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldsData = result.fieldList;
                let nameAPiIndex = fieldsData.fApiList.indexOf('ivp_name');
                if(nameAPiIndex==-1){
                    nameAPiIndex = fieldsData.fLabelList.indexOf('Name');
                }
                if(nameAPiIndex>-1){
                    fieldsData.fApiList.splice(nameAPiIndex,1);
                    fieldsData.fLabelList.splice(nameAPiIndex,1)  
                }
                fieldsData.fApiList = ["ivp_name"].concat(fieldsData.fApiList);
                fieldsData.fLabelList = ["Name"].concat(fieldsData.fLabelList);
                // removing feedback cols as we are adding them default
                helper.removeFeedbackCols(component,helper, fieldsData);
                
                component.set("v.wrapColnName",result.wrapColName);
                component.set("v.fieldList",fieldsData);
                component.set("v.userId",result.loginUserId);
                component.set("v.orgId",result.loginOrgId);
                component.set("v.hostName",result.apiHostName);
                component.set("v.orderBy",result.orderBy);
                component.set("v.hideFields",result.hidefields);
                component.set("v.daysRequested",parseInt(result.daysRequested));
                component.set("v.UserName",result.loginUserName);
                component.set("v.isActivePreference",result.isActivePreference);
                component.set("v.excludeLabelList",result.excludeLabelList);
                component.set("v.excludeApiList",result.excludeApiList);
                component.set("v.colMinWidthMap",result.colMinWidth);
                component.set("v.isColnWrap",result.isColnWrap);
                component.set('v.colnWrapMaxLength',result.colnWrapMaxLength);
                component.set("v.feedbackItems",result.feedbackItems);
                helper.prepareIconMap(component,result);
                let jfyDataError = false;
                let jfyData = result.jfyData;
                let errorMsg ='';
                let isReload = false;
                if(jfyData && jfyData.length > 0 && jfyData[0].errorInfo){
                    jfyDataError = true;
                    errorMsg = jfyData[0].errorInfo;
                    let errorInd = errorMsg.indexOf('ERROR:');
                    let jfyInd = errorMsg.indexOf('just_for_you_most_recent');
                    let uPreInd = errorMsg.indexOf('user_preferences');
                    var dataExcpInd = errorMsg.indexOf('Hint: ');
                    var doesnot = errorMsg.indexOf('does not exist');

                    if(errorInd > -1 ){
                        if(jfyInd > -1){
                            isReload = true;
                            errorMsg = $A.get("$Label.c.IVP_JFY_Refresh_Tooltip");
                        }else if(uPreInd>-1){
                            isReload = true;
                            errorMsg = $A.get("$Label.c.IVP_Preview_Universe_Refresh_Tooltip");
                        }else if (dataExcpInd>-1){
                            isReload = true;
                            errorInd+=6;
                            errorMsg = errorMsg.substr(errorInd,(dataExcpInd>errorInd ?dataExcpInd-2: errorMsg.length)-errorInd);
                        }else if(doesnot>-1){
                            isReload = true;
                            errorInd+=6;
                            doesnot+=15;
                            errorMsg = errorMsg.substr(errorInd,(doesnot>errorInd ?doesnot+15: errorMsg.length)-errorInd);
                        }
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type":"info",
                        "message":  errorMsg
                    });
                }
                if(result.isActivePreference && jfyDataError === false){
                    var compLoc = component.find('compLoc');
                    if(compLoc){
                        compLoc.set('v.activePreferenceList', jfyData);
                    }
                    isReload = false;
                    component.set("v.Spinner", false);
                    component.set('v.isRefresh', false);
                    component.set('v.isRefreshMsg', false);
                    helper.ProcessBefore(component, event, helper);
                }else{
                    component.set('v.isRefresh', true);
                    if(result.isActivePreference === false){
                        errorMsg = $A.get("$Label.c.IVP_Just_For_You_Error");
                        component.set("v.Spinner", false);
                    }
                    helper.inActiveInfo(component,event,helper, errorMsg);
                    if(isReload === true){
                        component.set('v.isReload', isReload );
                        helper.reload(component, event, helper);
                    }
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayMessage("error",errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    reload : function(component,event,helper){
        window.setTimeout(
            $A.getCallback(function() {
                helper.getData(component,event, helper);
            }), 500
        );
    },
    checkStatus :function(component, helper){
        var action = component.get("c.getSourcingPreferenceWithCount");
        if(action){
            action.setParams({
                'fetchData':true,
                'sourcingPreferenceList':null,
                'justForYouSetting':null
            });        
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    let jfyDataError = false;
                    let jfyData = result.jfyData;
                    let errorMsg ='';
                    if(jfyData && jfyData.length > 0 && jfyData[0].errorInfo){
                        errorMsg = jfyData[0].errorInfo;
                        let errorInd = errorMsg.indexOf('ERROR:');
                        if(errorInd > -1){
                            errorMsg = $A.get("$Label.c.IVP_JFY_Refresh_Tooltip");
                        }
                        helper.reload(component, event, helper);
                    }else{
                        component.set('v.isReload', false);
                        var compLoc = component.find('compLoc');
                        if(compLoc){
                            compLoc.set('v.activePreferenceList', jfyData);
                        }
                        component.set("v.Spinner", false);
                        helper.ProcessBefore(component, event, helper);
                    }
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            var toastEvent = $A.get("e.force:showToast");
                            if(toastEvent){
                                toastEvent.setParams({
                                    "type":"error",
                                    "title": "Error",
                                    "message": errors[0].message
                                });
                                toastEvent.fire();
                            }else{
                                console.log("Error message: " +  errors[0].message);
                            }
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    inActiveInfo : function(component,event,helper, msg){
        var hidefields = component.get("v.hideFields");
        var fieldsData = component.get("v.fieldList");
        var colLabelList = new Array();
        for(var i=0;i<fieldsData.fLabelList.length;i++){
            if(!hidefields.includes(fieldsData.fLabelList[i])){
                colLabelList.push(fieldsData.fLabelList[i]);
            }
        }
        
        fieldsData.fLabelList = colLabelList;
        component.set("v.fieldList",fieldsData);
        if(component.get('v.isRefreshMsg') === false){
            helper.displayMessage("Info",msg);
            component.set('v.isRefreshMsg', true);
        }
    },
    ProcessBefore : function(component,event,helper){ 
        var fieldList = component.get("v.fieldList");
        var pageLength  = component.get("v.pageLength");
        var wrapColnList = component.get("v.wrapColnName");
        var colMinWidth = component.get("v.colMinWidthMap");
        var targetForWrap = [];
        
        component.set("v.Spinner", true);
        // we need to exclude these two fields if selecting specific preference.
        var excludeColumnLabel = component.get("v.excludeLabelList");
        var excludeColumnApi = component.get("v.excludeApiList");
        var preferenceId = component.get("v.preferenceId");
        var daysRequested = component.get("v.daysRequested");
        var alldataList = new Object();
        var preferencedataList = new Object();
        var ajaxUrl = component.get("v.hostName");
        ajaxUrl += '/preference_user/'+component.get("v.userId")+'/companies';
        alldataList.fields = fieldList.fApiList;
        alldataList.order_by = component.get("v.orderBy"); //"ct.companies_cross_data_full.iq_score asc NULLS LAST";
        alldataList.limit = pageLength;
        alldataList.days_requested = daysRequested;
        alldataList.user_id = component.get("v.userId");
        alldataList.sf_org_id = component.get("v.orgId");
        Object.assign(preferencedataList, alldataList);
        var prefApi = new Array();
        for(var i=0;i<alldataList.fields.length;i++){
            if(!excludeColumnApi.includes(alldataList.fields[i])){
                prefApi.push(alldataList.fields[i]);
            }
        }
        
        preferencedataList.fields = prefApi;
        var allcolumnArray = new Array();
        var preferenceColumnArray = new Array();
        var colmnsOrder = new Array();
        var iqColumn =0;
        var mdIndex =0;
        var isFilter = component.get("v.filter");
        
        if(fieldList.fLabelList.length > 0){
            for(var i=0;i<fieldList.fLabelList.length;i++){
                var col = new Object();
                col.title = fieldList.fLabelList[i];
                if(col.title.trim() == 'Name'){
                    col.title = 'Company Name';
                }
                
                if(wrapColnList !== undefined && wrapColnList !== null && wrapColnList.length>0){
                    for(var j=0; j< wrapColnList.length; j++){
                        if(wrapColnList[j].trim() === fieldList.fLabelList[i].trim()){
                            targetForWrap.push(i);
                        }
                    }
                }
                
                col.defaultContent = '';
                allcolumnArray.push(col);
                if(!excludeColumnLabel.includes(fieldList.fLabelList[i])){
                    preferenceColumnArray.push(col);
                }
                if(col.title.trim() == 'IQ'){
                    iqColumn = i;
                }
                if(col.title.trim() === 'Most Recent Matched Date'){
                    mdIndex = i;
                }
            }
        }
        let feedbackItems = component.get('v.feedbackItems');
        let allFeedbackMap={}, preFeedbackMap = {};
        if(feedbackItems){
            for(let fkey in feedbackItems){
                allFeedbackMap[fkey]=alldataList.fields.length;
                preFeedbackMap[fkey]=preferencedataList.fields.length;
                preferencedataList.fields.push(fkey);
                alldataList.fields.push(fkey);
            }
            component.set('v.allFeedbackMap',allFeedbackMap);
            component.set('v.preFeedbackMap',preFeedbackMap);
        }
        component.set("v.isAllPreference",true);
        component.set("v.currentTableName","allPreference");
        component.set("v.ajaxApiUrl",ajaxUrl);
        component.set("v.allDataSet",alldataList);
        component.set("v.preferenceDataSet",preferencedataList);
        component.set("v.preferenceAjaxColumn",preferenceColumnArray);
        component.set("v.iqIndex",iqColumn);
        component.set("v.mdIndex",mdIndex);
        component.set("v.ajaxData",alldataList);
        component.set("v.allAjaxColumn",allcolumnArray);        
        component.set("v.ajaxColumn",allcolumnArray);
        component.set("v.wrapTarget",targetForWrap);
        helper.configDatatable(component,event,helper);
    },
    configDatatable : function(component,event,helper) {
        component.set("v.Spinner", true);
        var tableId = component.get("v.currentTableName");
        
        var targetWrap = component.get("v.wrapTarget");
        var wrapColnList = component.get("v.wrapColnName");
        var colMinWidth = component.get("v.colMinWidthMap");
        var wrapColnName = component.get("v.wrapColnName");
        var colnWrapMaxLength = component.get("v.colnWrapMaxLength");
        colnWrapMaxLength = colnWrapMaxLength!==undefined ? colnWrapMaxLength : 250;
        var ajaxUrl = component.get("v.ajaxApiUrl");
        var dataList = component.get("v.ajaxData");
        var columnArray = component.get("v.ajaxColumn");
        var iqColumn = component.get("v.iqIndex");
        var mdIndex = component.get("v.mdIndex");
        var pageLength  = component.get("v.pageLength");
        var feedbackSource = helper._feedbackSource(component, helper);
        var preferenceId = component.get("v.preferenceId");
        component.set("v.showTable",true);
        if(preferenceId) {
            mdIndex -= 1;
        }
        if(typeof $.fn.DataTable != 'undefined'){
            /*no need of following code as it will automatically destroy with lightning rendering
            if ($.fn.DataTable.isDataTable('#'+tableId) ) {
                $('#'+tableId).DataTable().destroy();
            };
            **/
            $('#'+tableId).DataTable( {
                columns:columnArray,
                columnDefs: [
                    {
                        render: function (data, type, full, meta) {
                            return "<div class='text-wrap'>" + data + "</div>";
                        },
                        targets: targetWrap
                    }
                ],
                order: [[mdIndex,'desc']],
                dom: '<"top"ilp>rt<"bottom"ilp>',
                //scrollY:        "1000px",
                lengthMenu: [[25, 50, 100, 200, 500],[25, 50, 100, 200, 500]],
                scrollX:true,
                deferRender: true,
                pageLength:pageLength,
                serverSide:true,
                bStateSave: false,
                ajax:{
                    url : ajaxUrl, // json datasource
                    type: "POST",
                    data: dataList,
                    timeout:60000,
                    
                    error: function (xhr, error, thrown) {
                        component.set("v.Spinner", false);
                        component.set("v.showTable","false");
                        component.set("v.isActivePreference",false);
                        helper.logApiResponse(component ,event,helper,dataList,"Post",ajaxUrl,xhr.statusText);
                
                    },
                    beforeSend: function () {
                        component.set('v.apiCallError', true);
                    },
                    complete:function(){
                        helper.drawFeedbacks(component,helper);
                    }
                },
                createdRow: function ( nRow, data, index) {
                    helper.tableRowsFormatting(component,nRow, data, index,columnArray,wrapColnList,feedbackSource);
                }
            } );
            $('#'+tableId+' tbody').on('click', 'td', function (evt) {
                var isNew = $(evt.target).is('.addNew');
                var isUser = $(evt.target).is('.unassigned');
                if(isNew){
                    component.set("v.Spinner", true);
                    var dataFields = component.get("v.ajaxData")['fields'];
                    var ctIdCol = '';var cNameCol= '';var websiteCol = '';
                    for(var i=0;i<columnArray.length;i++){
                        if(columnArray[i].title.trim() == 'ct_id'){
                            ctIdCol = i;
                            let ctidColInd = dataFields.indexOf('ct_id');
                            if(ctIdCol!==ctidColInd){
                                ctIdCol = ctidColInd;
                            }
                        }
                        if(columnArray[i].title.trim() == 'Company Name'){
                            cNameCol = i;
                            let nameColInd = dataFields.indexOf('ivp_name');
                            if(cNameCol!==nameColInd){
                                cNameCol = nameColInd;
                            }
                        }
                        if(columnArray[i].title.trim() == 'Website'){
                            websiteCol = i;
                            let websiteColInd = dataFields.indexOf('website');
                            if(websiteCol!==websiteColInd){
                                websiteCol = websiteColInd;
                            }
                        }
                    }
                    var currentRow = $(this).parent();
                    var table = $('#'+tableId).DataTable();
                    var ctId = table.row( currentRow ).data()[ctIdCol];
                    var website = table.row( currentRow ).data()[websiteCol];
                    var company = table.row( currentRow ).data()[cNameCol];
                    helper.processCompany(component,event,helper,ctId,website,company,currentRow,tableId);
                }
                if(isUser){
                    component.set("v.Spinner", true);
                    var sfIdCol = '';var ownerCol ='';
                    for(var i=0;i<columnArray.length;i++){
                        if(columnArray[i].title.trim() == 'sf_id'){
                            sfIdCol = i;
                        }
                        if(columnArray[i].title.trim() == 'Owner'){
                            ownerCol = i;
                        }
                    }
                    var currentRow = $(this).parent();
                    var table = $('#'+tableId).DataTable();
                    var sfId = table.row( currentRow ).data()[sfIdCol];
                    window.setTimeout( $A.getCallback(function() { 
                        helper.processUser(component,event,helper,sfId,currentRow,ownerCol); 
                    }), 500 );
                    //helper.processUser(component,event,helper,sfId,currentRow,ownerCol);
                }
            });
            var table = $('#'+tableId).DataTable();
            $(table.table().header() ).addClass( 'adjustData' );
            helper.tableColumnsFormatting(component,event,helper,columnArray);
            
            $('#'+tableId).on('error.dt', function (e, settings, techNote, message) {
                component.set('v.apiCallError', true);
                component.set("v.showTable","false");
                component.set("v.isActivePreference",false);
                let errorInd = message.indexOf('ERROR:');
                let jfyInd = message.indexOf('just_for_you_most_recent');
                let uPreInd = message.indexOf('user_preferences');
                var dataExcpInd = message.indexOf('Hint: ');
                var doesnot = message.indexOf('does not exist');
                
                if(errorInd > -1 ){
                    if(jfyInd > -1){
                        message = $A.get("$Label.c.IVP_JFY_Refresh_Tooltip");
                    }else if(uPreInd>-1){
                        message = $A.get("$Label.c.IVP_Preview_Universe_Refresh_Tooltip");
                    }else if (dataExcpInd>-1){
                        errorInd+=6;
                        message = message.substr(errorInd,(dataExcpInd>errorInd ?dataExcpInd-2: message.length)-errorInd);
                    }else if(doesnot>-1){
                        errorInd+=6;
                        doesnot+=15;
                        message = message.substr(errorInd,(doesnot>errorInd ?doesnot: message.length)-errorInd);
                    }
                }
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"info",
                    "message":  message
                });
                toastEvent.fire();
                dataList['response_message']=message.length>1000 ? message.substr(0,1000) : message;
                if(dataList && settings && settings.jqXHR) {
                    helper.logApiResponse(component ,event,helper,dataList,"Post",ajaxUrl,settings.jqXHR.status);
                }
                component.set("v.Spinner", false);
            })
            
            // $('thead > tr> th:nth-child(0)').css({ 'min-width':'175px'});
            // $('tbody > tr> td:nth-child(0)').css({ 'min-width':'175px'});
            $('thead > tr> th:nth-child(1)').css({ 'min-width':'125:px'});
            if(component.get("v.isColnWrap")){
                
                for(var i=0;i<columnArray.length;i++){
                    var x = i+1;
                    let colLabel = String(columnArray[i].title).trim();
                    let colIndex = wrapColnList.indexOf(colLabel);
                    if(colIndex>-1){
                        let colLMinWidth = colnWrapMaxLength;
                        if(colMinWidth[colLabel]!==undefined){
                            colLMinWidth = colMinWidth[colLabel]
                        }
                        $('thead > tr> th:nth-child('+x+')').css({ 'min-width':colLMinWidth+'px '});
                    }
                    /*
                    if(String(columnArray[i].title).trim() in colMinWidth){
                        //$('thead > tr> th:nth-child('+x+')').css({ 'min-width':colMinWidth[String(columnArray[i].title).trim()]+'px '});
                    }else if(String(columnArray[i].sTitle).trim() in colMinWidth){
                        //$('thead > tr> th:nth-child('+x+')').css({ 'min-width':colMinWidth[String(columnArray[i].sTitle).trim()]+'px '});
                    }
                    */
                }
            }
        }else{
            location.reload();
        }
    },
    logApiResponse : function(component,event,helper,rBody,methodName,url,status){
        var action = component.get("c.logAjax");
        if(action === undefined){
            return;
        }
        action.setParams({dbody:JSON.stringify(rBody),methodName:methodName,endPoint:url,status:status});
        action.setCallback(this, function(response) {
            component.set("v.Spinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.displayMessage("info",response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayMessage("error",errors[0].message);
                    }
                } else {
                    helper.displayMessage("error","Unknown Error");
                }
            }
        });
        component.set("v.Spinner", true); 
        $A.enqueueAction(action);  
    },
    tableRowsFormatting : function(cmp, nRow, data, index,columnArray,wrapColnList,feedbackSource){
        let dataWithFeedback = cmp.get('v.dataWithFeedback');
        var dataFields = cmp.get("v.ajaxData")['fields'];
        if(!dataWithFeedback){
            dataWithFeedback={};
        }
        let wrapColnName = cmp.get('v.wrapColnName');
        let colnWrapMaxLength = cmp.get('v.colnWrapMaxLength');
        var websiteCoulumn; var EES6MColumn; var linkedInColumn;var EESColumn;var ownerColumn;var companyColumn;var sfIdColumn;
        var ownerStatusColumn; var ct_id;
        for(var i=0;i<columnArray.length;i++){
            if((String(columnArray[i].title)).trim() == 'Company Name'){
                companyColumn = i;
                let nameColInd = dataFields.indexOf('ivp_name');
                if(companyColumn!==nameColInd){
                    companyColumn = nameColInd;
                }
                $(nRow).find('td:eq('+companyColumn+')').addClass("i-f-w");
            }
            if((String(columnArray[i].title)).trim() == 'Website'){
                websiteCoulumn = i;
                let websiteColInd = dataFields.indexOf('Website');
                if(websiteCoulumn!==websiteColInd){
                    websiteCoulumn = websiteColInd;
                }
            }
            if((String(columnArray[i].title)).trim() == 'EES 6M %'){
                EES6MColumn = i;
                let ees6mColInd = dataFields.indexOf('ivp_6m_employeecount_growth');
                if(EES6MColumn!==ees6mColInd){
                    EES6MColumn = ees6mColInd;
                }
            }
            if((String(columnArray[i].title)).trim() == 'LinkedIn URL'){
                linkedInColumn = i;
                let linkedColInd = dataFields.indexOf('ivp_linkedin');
                if(linkedInColumn!==linkedColInd){
                    linkedInColumn = linkedColInd;
                }
                
            }
            if((String(columnArray[i].title)).trim() == 'EES'){
                EESColumn = i;
                let eesColInd = dataFields.indexOf('ivp_employee_count');
                if(EESColumn!==eesColInd){
                    EESColumn = eesColInd;
                }
            }
            if((String(columnArray[i].title)).trim() == 'Owner'){
                ownerColumn = i;
                let ownerColInd = dataFields.indexOf('salesforce_owner_name');
                if(ownerColumn!==ownerColInd){
                    ownerColumn = ownerColInd;
                }
            }
            if((String(columnArray[i].title)).trim() == 'sf_id'){
                sfIdColumn = i;
                let sfidColInd = dataFields.indexOf('sf_id');
                if(sfIdColumn!==sfidColInd){
                    sfIdColumn = sfidColInd;
                }
            }
            if((String(columnArray[i].title)).trim() == 'Ownership Status'){
                ownerStatusColumn = i;
                let ownerStatusInd = dataFields.indexOf('ownership_status');
                if(ownerStatusColumn!==ownerStatusInd){
                    ownerStatusColumn = ownerStatusInd;
                }
            }            
            if((String(columnArray[i].title)).trim() == 'ct_id'){
                ct_id = i;
                let ctidColInd = dataFields.indexOf('ct_id');
                if(ct_id!==ctidColInd){
                    ct_id = ctidColInd;
                }
            }
            if(data[i] && isNaN(data[i]) === true){
                let dLength = data[i].trim().length;
                if(wrapColnList.includes(String(columnArray[i].title).trim())){
                    $(nRow).find('td:eq('+i+')').addClass("text-wrap");
                    $(nRow).find('td:eq('+i+')').attr("title",data[i]);

                    if(colnWrapMaxLength!=undefined){
                        if(dLength>colnWrapMaxLength){
                            $(nRow).find('td:eq('+i+')').html((data[i].substring(0,colnWrapMaxLength))+'...'); 
                        }
                    }
                }else{
                    if(dLength > 50){
                        $(nRow).find('td:eq('+i+')').html((data[i].substring(0,50))+'...'); 
                    }
                    //$(nRow).find('td:eq('+i+')').width = "250px";
                    $(nRow).find('td:eq('+i+')').addClass("adjustData");
                    $(nRow).find('td:eq('+i+')').attr("title",data[i]);
                }
            }
        }
        // Preparing Feedback 
        let ifLtngFeedback={ 
            "ResponsiveNess":{value:null},
            "ThumbsUp":{value:0},
            "SignalAction":{value:0},
            "InsightMatch":{value:0},
            "LikelyToRaise":{value:0}
        };
        for(let fKey in feedbackSource){
            if(feedbackSource[fKey].valueIndex>-1){
                ifLtngFeedback[fKey].value=data[feedbackSource[fKey].valueIndex];
            }
            if(feedbackSource[fKey].fade_levelIndex>-1){
                ifLtngFeedback[fKey].opacity=data[feedbackSource[fKey].fade_levelIndex];
            }
        }
        dataWithFeedback['ifLtng'+index]={};
        dataWithFeedback['ifLtng'+index]['app_name']='DWH';
        dataWithFeedback['ifLtng'+index]['feedback']=ifLtngFeedback;
        // formatting for Company Name
        if(websiteCoulumn && columnArray.length > websiteCoulumn){
            var webSiteLink = 'http://'+data[websiteCoulumn];
            $(nRow).find('td:eq('+companyColumn+')').html('<a title='+webSiteLink+' href='+webSiteLink+' target="_blank">'+$(nRow).find('td:eq('+companyColumn+')').html()+'</a>');
            
            if(data[sfIdColumn]){
                $(nRow).find('td:eq('+companyColumn+')').append('<br/><a target="_blank" href="/'+data[sfIdColumn]+'"><img src="/resource/Datatable/Datatable/images/company_60.png" class="slds-icon--x-small" /></a>&nbsp;');
                dataWithFeedback['ifLtng'+index]['app_name']='SF';
                dataWithFeedback['ifLtng'+index]['recId']=data[sfIdColumn];
            }else{
                $(nRow).find('td:eq('+companyColumn+')').append('<br/><a><img src="/resource/Datatable/Datatable/images/addNew.png" class="slds-icon--x-small addNew" /></a>&nbsp;');
                dataWithFeedback['ifLtng'+index]['recId']=data[ct_id];
            }
            //Formatting website
            $(nRow).find('td:eq('+websiteCoulumn+')').html('<a title='+webSiteLink+' href='+webSiteLink+' target="_blank">'+$(nRow).find('td:eq('+websiteCoulumn+')').html()+'</a>');
        }
        $(nRow).find('td:eq('+companyColumn+')').append('<div class="ifLtng" id="ifLtng'+index+'" data-index="'+index+'"> </div>');
        cmp.set('v.dataWithFeedback',dataWithFeedback);
        // formatting for LinkedIn Column
        if(linkedInColumn && columnArray.length > linkedInColumn){
            var linkedInLink = data[linkedInColumn];
            $(nRow).find('td:eq('+EESColumn+')').html('<a title='+linkedInLink+' href='+linkedInLink+' target="_blank">'+$(nRow).find('td:eq('+EESColumn+')').html()+'</a>');
            $(nRow).find('td:eq('+linkedInColumn+')').html('<a title='+linkedInLink+' href='+linkedInLink+' target="_blank">'+$(nRow).find('td:eq('+linkedInColumn+')').html()+'</a>');
        }
        // Formatting for EES 6M %
        if(EES6MColumn && columnArray.length > EES6MColumn){
            if(data[EES6MColumn] && parseInt(data[EES6MColumn]) > 0){
                $(nRow).find('td:eq('+EES6MColumn+')').prepend('<span class="EESUp" style="padding-right: 1rem;"></span>');
                
            }
            if(data[EES6MColumn] && parseInt(data[EES6MColumn]) < 0){
                $(nRow).find('td:eq('+EES6MColumn+')').prepend('<span class="EESDown" style="padding-right: 1rem;"></span>');
                
            }
        }
        // Formatting for Owner
        if(ownerColumn && columnArray.length > ownerColumn){
            if(String(data[ownerColumn]) == ('Unassigned') || String(data[ownerStatusColumn]) == ('Unassigned')){
                $(nRow).find('td:eq(' +ownerColumn+')').prepend('<span><img  src="/resource/Datatable/Datatable/images/changeOwner.svg" class="slds-icon--x-small unassigned"/></span>&nbsp;&nbsp;');
                
                $(nRow).addClass('Unassigned');
            }else if(data[sfIdColumn] && String(data[ownerColumn]) != ('Unassigned') && String(data[ownerColumn])){
                $(nRow).addClass('Assigned');
            }
            if(String(data[ownerStatusColumn]) == ('Watch') ){
                $(nRow).find('td:eq(' +ownerColumn+')').prepend('<span><img src="/resource/Datatable/Datatable/images/assigned.png" class="slds-icon--x-small"/></span>&nbsp;&nbsp;');
            }
        }
        
    },
    tableColumnsFormatting : function(component,event,helper,columnArray){
        var tableId = component.get("v.currentTableName");
        //var excludeColumnLabel = ['All Matched Preferences','Most Recent Matched Preference'];
        var table = $('#'+tableId).DataTable();
        var pageNo = component.get("v.CurrentPage");
        var hideFields = component.get("v.hideFields");
        var websiteIndex;var linkedinIndex;var rId;var nameIndex;var sfIdIndex;var ctIdIndex;
        var ownerStatusIndex;var ownerIndex;
        var dataFields = component.get("v.ajaxData")['fields'];
        
        for(var i=0;i<columnArray.length;i++){
            if((String(columnArray[i].title)).trim() == 'Website'){
                websiteIndex = i;
                /*
                let websiteColInd = dataFields.indexOf('website');
                if(websiteIndex!==websiteColInd){
                    websiteIndex = websiteColInd;
                }
                */
            }
            if((String(columnArray[i].title)).trim() == 'Company Name'){
                nameIndex = i;
                /*
                let nameColInd = dataFields.indexOf('ivp_name');
                if(nameIndex!==nameColInd){
                    nameIndex = nameColInd;
                }
                */
            }
            if(String(columnArray[i].title).trim() == 'Id'){
                rId = i;
            }
            if(String(columnArray[i].title).trim() == 'LinkedIn URL'){
                linkedinIndex = i;
                /*
                let linkedColInd = dataFields.indexOf('ivp_linkedin');
                if(linkedinIndex!==linkedColInd){
                    linkedinIndex = linkedColInd;
                }
                */
            }
            if(String(columnArray[i].title).trim() == 'sf_id'){
                sfIdIndex = i;
                /*
                let sfidColInd = dataFields.indexOf('sf_id');
                if(sfIdIndex!==sfidColInd){
                    sfIdIndex = sfidColInd;
                }
                */
            }
            if(String(columnArray[i].title).trim() == 'ct_id'){
                ctIdIndex = i;
                /*
                let ctidColInd = dataFields.indexOf('ct_id');
                if(ctIdIndex!==ctidColInd){
                    ctIdIndex = ctidColInd;
                }
                */
            }
            if(String(columnArray[i].title).trim() == 'Ownership Status'){
                ownerStatusIndex = i;
                /*
                let ownerStatusInd = dataFields.indexOf('ownership_status');
                if(ownerStatusIndex!==ownerStatusInd){
                    ownerStatusIndex = ownerStatusInd;
                }
                */
            }
            if(String(columnArray[i].title).trim() == 'Owner'){
                ownerIndex = i;
                /*
                let ownerColInd = dataFields.indexOf('salesforce_owner_name');
                if(ownerIndex!==ownerColInd){
                    ownerIndex = ownerColInd;
                }
                */
            }
        }
        if(hideFields.includes('Ownership Status')){
            var ownerStatusColumn = table.column(ownerStatusIndex);
            ownerStatusColumn.visible(false);
        }
        if(hideFields.includes('Owner')){
            var ownerColumn = table.column(ownerIndex);
            ownerColumn.visible(false);
        }
        if(hideFields.includes('Website')){
            var websiteColumn = table.column(websiteIndex);
            websiteColumn.visible(false);
        }
        if(hideFields.includes('LinkedIn URL')){
            var linkedinColumn = table.column(linkedinIndex);
            linkedinColumn.visible(false);
        }
        if(hideFields.includes('sf_id')){
            var sfIdColumn = table.column(sfIdIndex);
            sfIdColumn.visible(false);
        }
        if(hideFields.includes('ct_id')){
            var ctIdColumn = table.column(ctIdIndex);
            ctIdColumn.visible(false);
        }
        $('#'+tableId).on( 'processing.dt', function ( e, settings, processing ) {
            if(processing){
                component.set("v.Spinner", true);
            }else{
                component.set("v.Spinner", false);
            }
        });
        
    },
    processCompany : function(component,event,helper,externalId,website,company,currentRow,tableId){
        component.set("v.Spinner", true);
        var http = new XMLHttpRequest();
        var url = component.get("v.hostName")+'/company_searches/fetch?datatables=1';
        var params = 'json_filters={"record_ids":"'+externalId+'"}';
        http.open('POST', url, true);
        http.timeout = 30000;
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.onreadystatechange = function() {
            if(http.status == 200 && http.readyState===4 ) {
                var response = JSON.parse(http.responseText);
                var columsList = new Array();
                var dataList = new Array();
                if(response.columns){
                    for(var i=0;i<response.columns.length;i++){
                        var col = new Object();
                        col.title = response.columns[i].title;
                        col.fieldName = '';
                        col.data = response.data[0][i];
                        if(response.columns[i].lookup){
                            if(response.columns[i].lookup.target_field__c != null){
                                col.fieldName = response.columns[i].lookup.target_field__c;
                            }
                        }
                        columsList.push(col);
                    }
                }
                window.setTimeout( $A.getCallback(function() { 
                    helper.sendAccountInfo(component,event,helper,columsList,currentRow,tableId,website,company);
                }), 500 );
                
            }else if(http.readyState === 0){
                component.set("v.Spinner", false);
                helper.logApiResponse(component ,event,helper,JSON.parse(http.responseText),"Post",url);
            }
        }
        http.send(params);
    },
    sendAccountInfo : function(component,event,helper,columsList,currentRow,tableId,website,company){
        var columnArray = component.get("v.ajaxColumn");
        var ownerCol;var companyCol;var webCol;
        for(var i=0;i<columnArray.length;i++){
            if(columnArray[i].title.trim() == 'Owner'){
                ownerCol = i;
            }
            if(columnArray[i].title.trim() == 'Company Name'){
                companyCol = i;
            }
        }
        
        var action = component.get("c.createCompany");
        action.setParams({ companyDataList : columsList, compName:company, website:website});
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.Spinner", false);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.message =='Exist'){
                    $(currentRow).find('td:eq(' + ownerCol + ')').html(result.acc.Owner.Name);
                    $(currentRow).css({'background-color': $A.get("$Label.c.DW_ERROR_COLOR")});
                    if(result.acc.Ownership_Status__c == 'Watch'){
                        $(currentRow).find('td:eq(' + ownerCol + ')').prepend('<span><img src="/resource/Datatable/Datatable/images/assigned.png" class="slds-icon--x-small"/></span>&nbsp;&nbsp;');
                    }
                    helper.displayMessage('info','It looks like you\'re creating a duplicate record. We recommend you use an existing record instead.');
                }else{
                    $(currentRow).find('td:eq(' + ownerCol + ')').html(result.acc.Owner.Name);
                    $(currentRow).css({'background-color': $A.get("$Label.c.DW_ASSIGNED_COLOR")});
                    if(result.acc.Ownership_Status__c == 'Watch'){
                        $(currentRow).find('td:eq(' + ownerCol + ')').prepend('<span><img src="/resource/Datatable/Datatable/images/assigned.png" class="slds-icon--x-small"/></span>&nbsp;&nbsp;');
                    }
                    helper.displayMessage('Success','New Company added');
                }
                let rindex=$(currentRow).index();
                let fIconKey = 'ifLtng'+rindex;
                let iconInnerHTML = document.getElementById(fIconKey).innerHTML;
                
                $(currentRow).find('td:eq(' + companyCol + ')').html('<a  href="http://'+result.acc.Website+'" target="_blank">'+result.acc.Name+'</a>');
                $(currentRow).find('td:eq(' + companyCol + ')').append('<br/><a target="_blank" href="/'+result.acc.Id+'"> <img src="/resource/Datatable/Datatable/images/company_60.png" class="slds-icon--x-small"/></a>&nbsp;');
                $(currentRow).find('td:eq(' + companyCol + ')').append('<div class="ifLtng" id="ifLtng'+rindex+'" data-index="'+rindex+'"> </div>');
                var dataWithFeedback = component.get('v.dataWithFeedback');
                let iconMap = component.get('v.iconMap');
                if(dataWithFeedback){
                    let data= dataWithFeedback[fIconKey].feedback;
                    let iconToShow = helper.jsPrepareFeedbacks(data,iconMap);
                    dataWithFeedback[fIconKey].feedback = data;
                    helper.jsPrepareIcons(fIconKey, dataWithFeedback[fIconKey],iconToShow, component.get('v.mapDWHAction'));
                }

                helper.enableFeedbackAction(component, helper);
                //helper.configDatatable(component,event,helper);
            }
            else if (state === "ERROR") {
                helper.displayMessage('error','failed to create account');
                $(currentRow).css({'background-color': $A.get("$Label.c.DW_ERROR_COLOR")});
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayMessage("error",errors[0].message);
                    }
                } else {
                    helper.displayMessage("error","Unknown Error");
                }
            }
        });
        component.set("v.Spinner", true);
        $A.enqueueAction(action);
    },
    processUser : function(component,event,helper,sfId,currentRow,ownerCol){
        var action = component.get("c.processAssgnUser");
        action.setParams({ companyId : sfId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner", false);
                var result = response.getReturnValue();
                component.set("v.Spinner", false);
                helper.displayMessage(result.taostType, result.message);
                if(result.types == 'Success'){
                    $(currentRow).find('td:eq('+ownerCol+')').find("span").remove();
                    $(currentRow).addClass("Assigned");
                    $(currentRow).css({'background-color': $A.get("$Label.c.DW_ASSIGNED_COLOR")});
                    //helper.configDatatable(component,event,helper);
                }else{
                    $(currentRow).css({'background-color': $A.get("$Label.c.DW_ERROR_COLOR")});
                }
                if(result.acc){
                    $(currentRow).find('td:eq(' + ownerCol + ')').html(result.acc.Owner.Name);
                    if(result.acc.Ownership_Status__c == 'Watch'){
                        $(currentRow).find('td:eq(' + ownerCol + ')').prepend('<span><img src="/resource/Datatable/Datatable/images/assigned.png" class="slds-icon--x-small"/></span>&nbsp;&nbsp;');
                    }
                }
            }
            else if (state === "ERROR") {
                component.set("v.Spinner", false);
                $(currentRow).css({'background-color': $A.get("$Label.c.DW_ERROR_COLOR")});
                var errors = response.getError();
                if (errors) {
                    var msg='nvalid Process';
                    if(errors[0].message){
                        msg = errors[0].message;
                    }
                    $(currentRow).find('td:eq('+ownerCol+')').html( 'Error: <h1 style="color:' + $A.get("$Label.c.DW_ERROR_COLOR") + '">' + msg + '<h1>');
                    helper.displayMessage("error",msg);
                } else {
                    $(currentRow).find('td:eq('+ownerCol+')').html( 'Error: <h1 style="color:' + $A.get("$Label.c.DW_ERROR_COLOR") + '">' + 'Unknown Error' + '<h1>');
                    helper.displayMessage("error","Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    displayMessage : function(mtype,sMessage){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type":mtype,
            "message":sMessage,
        });
        // we don't need stickness  
        // "mode":"sticky"
        toastEvent.fire();
    },
    setPreferenceApi : function(component, event, helper){
        var preferenceId = component.get("v.preferenceId");
        if(preferenceId){
            /*no need of following code as it will automatically destroy with lightning rendering
            if ($.fn.DataTable.isDataTable('#preferenceTable') ) {
                $('#preferenceTable').DataTable().destroy();
            };
            */
            var ajaxUrl = component.get("v.hostName");
            ajaxUrl += '/preference_user/'+preferenceId+'/preview';
            component.set("v.ajaxApiUrl",ajaxUrl);
            component.set("v.currentTableName","preferenceTable");
            component.set("v.ajaxColumn",component.get("v.preferenceAjaxColumn"));
            component.set("v.ajaxData",component.get("v.preferenceDataSet"));
            // no need following
            //component.set("v.sf_org_id",component.get("v.orgId"));
        }else{
            /*no need of following code as it will automatically destroy with lightning rendering
            if ($.fn.DataTable.isDataTable('#allPreference') ) {
                $('#allPreference').DataTable().destroy();
            };
            */
            var ajaxUrl = component.get("v.hostName");
            ajaxUrl += '/preference_user/'+component.get("v.userId")+'/companies';
            component.set("v.ajaxApiUrl",ajaxUrl);
            component.set("v.currentTableName","allPreference");
            component.set("v.ajaxColumn",component.get("v.allAjaxColumn"));
            component.set("v.ajaxData",component.get("v.allDataSet"));
            component.set("v.sf_org_id",component.get("v.orgId"));
            // no need following
            //alldataList.sf_org_id = component.get("v.orgId");
        }
        helper.configDatatable(component,event,helper);
    },
    _feedbackSource:function(cmp, hlpr){
        let feedbackMap = {};
        let isAllPreference = cmp.get('v.isAllPreference');
        if(isAllPreference){
            feedbackMap = cmp.get('v.allFeedbackMap');
        }else{
            feedbackMap = cmp.get('v.preFeedbackMap');
        }
        var feedbackData = hlpr.feedbackInitCols();
        var feedbackIIFields = feedbackData.feedbackIIFields /*{
            'thumbs_value':'ThumbsUp',
            'thumbs_fade_level':'ThumbsUp',
            'responsive_value':'ResponsiveNess',
            'responsive_fade_level':'ResponsiveNess',
            'likely_to_raise_value':'LikelyToRaise',
            'last_week_activity_value':'SignalAction',
            'insight_match_value':'InsightMatch'
        }*/
        var feedbackSource=feedbackData.feedbackSource /*{
            'ThumbsUp':{
                'thumbs_value':'value', valueIndex:-1,'thumbs_fade_level':'fade_level',fade_levelIndex:-1
            },
            'ResponsiveNess':{
                'responsive_value':'value', valueIndex:-1
            },
            'LikelyToRaise':{
                'likely_to_raise_value':'value', valueIndex:-1
            },
            'SignalAction':{
                'last_week_activity_value':'value', valueIndex:-1
            },
            'InsightMatch':{
                'insight_match_value':'value', valueIndex:-1
            }
        }*/
        for(let key in feedbackMap){
            if(feedbackIIFields[key]){
                if(feedbackSource[feedbackIIFields[key]]){
                    let field = feedbackSource[feedbackIIFields[key]] 
                    if(field && field[key]){
                        field[field[key]+'Index']=feedbackMap[key];
                    }
                }
            }
        }
        return feedbackSource;
	},
    feedbackInitCols:function(){
        var feedbackIIFields = {
            'thumbs_value':'ThumbsUp',
            'thumbs_fade_level':'ThumbsUp',
            'responsive_value':'ResponsiveNess',
            'responsive_fade_level':'ResponsiveNess',
            'likely_to_raise_value':'LikelyToRaise',
            'last_week_activity_value':'SignalAction',
            'insight_match_value':'InsightMatch'
        }
        var feedbackSource={
            'ThumbsUp':{
                'thumbs_value':'value', valueIndex:-1,'thumbs_fade_level':'fade_level',fade_levelIndex:-1
            },
            'ResponsiveNess':{
                'responsive_value':'value', valueIndex:-1
            },
            'LikelyToRaise':{
                'likely_to_raise_value':'value', valueIndex:-1
            },
            'SignalAction':{
                'last_week_activity_value':'value', valueIndex:-1
            },
            'InsightMatch':{
                'insight_match_value':'value', valueIndex:-1
            }
        }
        return {feedbackIIFields:feedbackIIFields,feedbackSource:feedbackSource};
    },
    drawFeedbacks:function(cmp,hlpr){
        var dataWithFeedback = cmp.get('v.dataWithFeedback');
        var mapDWHAction = cmp.get('v.mapDWHAction');
        let iconMap = cmp.get('v.iconMap');
        for(let key in dataWithFeedback){
            let data= dataWithFeedback[key].feedback;
            let iconToShow = hlpr.jsPrepareFeedbacks(data,iconMap);
            dataWithFeedback[key].feedback = data;
            $('#'+key).html('feedbackIcons');
            hlpr.jsPrepareIcons(key, dataWithFeedback[key],iconToShow, mapDWHAction);
        }
        cmp.set('v.dataWithFeedback',dataWithFeedback);
        cmp.set('v.lastDWHAction','');
        hlpr.enableFeedbackAction(cmp, hlpr);
    },
    enableFeedbackAction: function(cmp, hlpr){
        window.setTimeout(
            $A.getCallback(function() {
                $('a').each(function () {
                    var $this = $(this);
                    
                    if($this.attr('id')=='fbaction'){
                        let lastDWHAction = cmp.get('v.lastDWHAction')
                        let mapDWHAction =  cmp.get('v.mapDWHAction')
                        let key = $this.data('key');
                        let action = $this.data('name');
                        let setClick = lastDWHAction=='';// || lastDWHAction== ;
                        if(mapDWHAction && mapDWHAction[key]!=undefined){
                            setClick = mapDWHAction[key].indexOf(action)==-1;
                        }
                        if(setClick ){
                            $this.on("click", function () {
                                var dataWithFeedback = cmp.get('v.dataWithFeedback');
                                let action = $(this).data('name');
                                let key = $(this).data('key');
                                $(this).addClass('action slds-m-left_xx-small ');
                                var acclass='slds-spinner_container actionSpinner slds-m-left_small slds-m-right_x-small';
                                if(action=='ThumbsUp'){
                                    $(this).addClass('slds-border_left')
                                }
                                var spanId = 'action_'+key+action;
                                var smallSpinner = '<div Id="'+spanId+'" style="display: inline;"> <span class="'+acclass+'" style="height: 1rem;position:relative;background-color: transparent;"><div  aria-hidden="false" role="alert" class="slds-spinner--brand slds-spinner slds-spinner_x-small"> <span class="slds-assistive-text">Creating...</span><div class="slds-spinner__dot-a"></div><div class="slds-spinner__dot-b"></div>  </div></span></div>';
                                $(this).html(smallSpinner);
                                let actionData={action:action,key:key,data:dataWithFeedback[key]};
                                hlpr.jsAction(cmp, hlpr,actionData);
                            });
                        }
                    }
                });
            }), 200
        );
    },
    jsAction: function(cmp, hlpr, actionData) {
        let data = actionData.data;
        let iiFeedback = data.feedback;
        let iconMap = cmp.get('v.iconMap');
        let action = actionData.action;
        if(action==='ThumbsUp' || action==='ThumbsDown' || action==='InsightMatch' ){
            let key = action ==='ThumbsDown' ? 'ThumbsUp' : action;
            let val = iiFeedback[key];
            val = val.value ===null ||val.value === undefined? 0 : val.value;
            var newVal = iconMap[action][val].Toggle_Value__c !== undefined 
            ? iconMap[action][val].Toggle_Value__c
            : val === 1 ? 0 : 1;
            let fdata={key:key, 
                       action:action,
                       recId:data.recId,
                       id_app_name:data.app_name,
                       user_id:hlpr.getCurUserId(cmp),
                       userEmail: hlpr.getCurUserEmail(cmp),
                       tool:'Just For You',
                       org_id:cmp.get('v.orgId'), 
                       newVal:newVal,
                       oldVal:val
                      };
            hlpr.doAPICall(cmp, hlpr, fdata,actionData);
        }
    },
    doAPICall: function(cmp, hlpr, data, actionData) {
        var req = new XMLHttpRequest();
        var params={
            "org_id":data.org_id,
            "user_id":data.user_id,
            "id_app_name":data.id_app_name,
            "tool_name":data.tool
        }
        let dwhSetting = cmp.get('v.dwhSetting');
        var url = ''
        var dwhKey = '';
        if(dwhSetting){
            url = dwhSetting.Endpoint__c;
            dwhKey = dwhSetting.Auth_Key__c;
        }
        if($A.util.isUndefinedOrNull(url) || $A.util.isEmpty(url) 
           || $A.util.isUndefinedOrNull(dwhKey) || $A.util.isEmpty(dwhKey) ){
            hlpr.displayMessage('error', 'Data Ware house details missing!');
            return;
        }
        url+='/feedback/sf/';
        let recData ={
            "value": Number.parseInt(data.newVal),
            "reason": null
        };
        if(data.key === 'ThumbsUp'){
            url+='thumbs?';            
            params["thumbs"]={};
            params["thumbs"][data.recId]=recData;
        }else if(data.key === 'InsightMatch'){
            url+='insight_match?';
            params["match"]={};
            params["match"][data.recId]=recData;
        }
        req.open('POST', url);
        req.setRequestHeader ('Authorization',dwhKey);
        req.timeout = 30000;
        req.onreadystatechange = function() {
            //cmp.set("v.Spinner", false);
            if(req.readyState === 4 ) {
                let mapDWHAction = cmp.get('v.mapDWHAction');
                if(mapDWHAction&& mapDWHAction[actionData.key]){
                    let actionIndex = mapDWHAction[actionData.key].indexOf(data.action);
                    if(actionIndex>-1){
                        mapDWHAction[actionData.key].splice(actionIndex,1);
                        cmp.set('v.mapDWHAction',mapDWHAction);
                    }
                }
                if(req.status===200){
                    var res = JSON.parse(req.responseText);
                    hlpr.processResponse(cmp, hlpr, res, data, actionData);
                    hlpr.enableFeedbackAction(cmp, hlpr);
                }else if(req.status === 404 ){
                    var res = {'isError':true,'dwhNoFound':true};
                    hlpr.processResponse(cmp, hlpr, res, data, actionData);
                    hlpr.enableFeedbackAction(cmp, hlpr);
                } else {
                    var res = {'isError':true};
                    hlpr.processResponse(cmp, hlpr, res, data, actionData);
                    hlpr.enableFeedbackAction(cmp, hlpr);
                    hlpr.logApiResponse(cmp ,null,hlpr,params,"Post",url,req.status);
                }
                console.timeEnd('action'+data.key);
            }else if(req.readyState === 0){
                alert('Server not found!');
                var res = {'isError':true};
                hlpr.processResponse(cmp, hlpr, res, data, actionData);
                hlpr.enableFeedbackAction(cmp, hlpr);
                console.timeEnd('action'+data.key);
            }
        }
        let mapDWHAction = cmp.get('v.mapDWHAction');
        if(!mapDWHAction){
            mapDWHAction={};
        }
        if(!mapDWHAction[actionData.key]){
            mapDWHAction[actionData.key]=[]
        }
        if(mapDWHAction[actionData.key].indexOf(data.action)==-1){
            mapDWHAction[actionData.key].push(data.action);
            cmp.set('v.mapDWHAction',mapDWHAction);
        }else{
            return;
        }
        cmp.set('v.lastDWHAction',actionData.key);
        try{
            console.time('action'+data.key);
            //cmp.set("v.Spinner", true);
            req.send(JSON.stringify(params));
        }catch(error){
            component.set("v.Spinner", false);
            console.log(error);
        }
    },
    processResponse:function(cmp, hlpr, res, data, actionData){
        let isError = res['isError'] ===true ;
        let dwhNoFound = res['dwhNoFound'] ===true ;
        let updateValue = res[actionData.data.recId]!==undefined || isError;
        var erroMsg = $A.get('$Label.c.DWH_Feedback_Error_Message');
        if( updateValue){
            let iiFeedback = actionData.data.feedback;
            let key = actionData.action ==='ThumbsDown' ? 'ThumbsUp' : actionData.action;
            iiFeedback[key].value = data.newVal;
            if(isError){
                iiFeedback[key].value = data.oldVal
            }
            
            let dataWithFeedback = cmp.get('v.dataWithFeedback');
            dataWithFeedback[actionData.key].feedback = iiFeedback;
            
            let iconMap = cmp.get('v.iconMap');
            let iconToShow = hlpr.jsPrepareFeedbacks(iiFeedback, iconMap);
            dataWithFeedback[actionData.key].feedback = iiFeedback;
            var mapDWHAction = cmp.get('v.mapDWHAction');
            hlpr.jsPrepareIcons(actionData.key, dataWithFeedback[actionData.key],iconToShow,mapDWHAction);
            cmp.set('v.dataWithFeedback', dataWithFeedback);
            if(dwhNoFound){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":"info",
                    "message": erroMsg
                });
                toastEvent.fire();
            }
        }else if(res['error']){
            console.log('-', res);
        }
    },
    prepareIconMap:function(cmp, result){
        let srcPath = cmp.get('v.srcPath');
        let icons = result.icons;
        let iconMap = {};
        icons.forEach(function(icon){
            if(icon.Icon__c)
                icon.iconPath = srcPath+'/'+icon.JFY_Icon__c;
            if(!iconMap[icon.Indicator__c]){
                iconMap[icon.Indicator__c] ={};
            }
            iconMap[icon.Indicator__c][icon.Value__c] = icon;
        });
        
        cmp.set('v.dwhSetting', result.dwhSetting);
        cmp.set('v.orgId', result.org.Id);
        cmp.set('v.iconMap', iconMap);
    },
    jsPrepareFeedbacks: function(iiFeedback, iconMap) {
        var iconToShow=[];
        for( let key in iiFeedback){
            let fBack = iiFeedback[key]
            let value = fBack.value;
            if(value ===null ||value === undefined ){
                if(key === "ResponsiveNess"){
                    value='null';
                }else{
                    value = 0;
                }
            }
            //value = value ===null ||value === undefined ? 0 : value;
            if(iconMap[key] && iconMap[key][value]){
                let icon=iconMap[key][value];
                //|| key==='PreferenceCenter' 
                let isAction = key==='InsightMatch' || key==='ThumbsUp' || key==='ThumbsDown';
                let iconObj ={name:key, icon:icon,isAction:isAction,value:value};
                iconObj.hasLeftBar = key==='ThumbsUp';
                if(key==='ThumbsUp'){
                    if(value !== 0 ){
                        if(value !== "0" ){
                            value = - value;
                        }
                    }
                    iconObj.value=value;
                    iconToShow.push(iconObj);
                    if(iconMap['ThumbsDown'] && iconMap['ThumbsDown'][value]){
                        let tIcon = iconMap['ThumbsDown'][value];
                        iconToShow.push({name:'ThumbsDown', isAction:isAction,
                                         icon:tIcon,value:value});
                        iiFeedback['ThumbsDown']=value;
                    }
                }else if(key!=='ThumbsDown'){
                    iconToShow.push(iconObj);
                }
            }
        }
        iconToShow.sort(function(a,b){
            return a.icon.Order__c - b.icon.Order__c;
        })
        return iconToShow;
    },
    jsPrepareIcons:function(key, data, iconsToShow,mapDWHAction){
        let feedback = data['feedback'];
        if(!iconsToShow){
            for(let fKey in feedback){
                iconsToShow.push(feedback[fKey]);
            }
            iconsToShow.sort(function(a,b){
                return a.order - b.order;
            })
        }
        var iconRaw = '';
        var forTesting = false;
        if(forTesting){
            return;
        }
        for(let ind=0, len=iconsToShow.length; ind<len;ind++){
            let icon = iconsToShow[ind];
            let imgClass='slds-m-left_xx-small slds-m-right_xx-small';
            let aClass='';
            if(icon.hasLeftBar){
                imgClass='slds-border_left slds-m-left_xx-small slds-m-right_xx-small';
            }
            let iconVal ='<img src="'+icon.icon.iconPath+'" title="'+(icon.icon.Hover_Text__c?icon.icon.Hover_Text__c:'')+'" '
            	+'class="'+imgClass+'"/>';
            
            if(mapDWHAction && mapDWHAction[key] && mapDWHAction[key].indexOf(icon.name)>-1){
                iconVal = $('#action_'+key+icon.name).html();
            }
            if(icon.isAction){
                iconVal='<a href="javascript:void(0);" id="fbaction"'
                		+' data-name="'+icon.name+'" data-key="'+key+'"'
               			+' class="'+aClass+'">'+iconVal+'</a>';
            }
            iconRaw+=iconVal;
        }
        $('#'+key).html(iconRaw);
    }
})