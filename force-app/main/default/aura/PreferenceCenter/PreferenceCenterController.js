({
    changeTab1 : function(component, event, helper) {
        component.set("v.tab1",true);
        component.set("v.tab2",false);
        component.set("v.tab3",false);
    },
    changeTab2 : function(component, event, helper) {
        component.set("v.tab2",true);
        component.set("v.tab1",false);
        component.set("v.tab3",false);
    },
    changeTab3 : function(component, event, helper) {
        component.set("v.tab3",true);
        component.set("v.tab1",false);
        component.set("v.tab2",false);
    },
    checkForJquery : function (component) {
        var funType = typeof $.fn.queryBuilder;
        if(funType !=='function' || funType=='undefined'){
            component.set("v.isSpinner", false);
            location.reload();
            //this.toastMsg(component, event, 'Please refresh the page, try again!', 'info');
            return;
            //location.reload();
        }
    }
})