({
	fetchInit : function(component, event, helper) {
		helper.fetchData(component, event, helper);
	},	

	checkModalOpen : function(component, event, helper) {
        if(component.get("v.openFinancialsModal") == true)
        {
			helper.fetchData(component, event, helper);
        }
	},	
	
	saveFinancials : function(component, event, helper) {
	    component.set("v.isSavingFinancial", true);
		$A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
		helper.saveFinancialsData(component,helper);
	},

	clickFinancials : function(component, event, helper) {
		component.set("v.openFinancialsModal",true);
	},

	toggleFinancials:function(component){
		var toggleVar = component.get("v.openFinancials");
		component.set("v.openFinancials",!toggleVar);
	},

	closeModal :function(component){
		component.set("v.openFinancialsModal",false);
	},
})