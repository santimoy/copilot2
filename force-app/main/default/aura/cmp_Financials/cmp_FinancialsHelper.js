({
	saveFinancialsData: function(component,helper)
	{
  		var action = component.get("c.saveData");

		action.setParams({
			recordId:component.get("v.recordId"),
			priorRevenueValue:component.get("v.priorRevenueValue"),
			currentRevenueValue:component.get("v.currentRevenueValue"),
			nextRevenueValue:component.get("v.nextRevenueValue"),
			priorEbitaValue:component.get("v.priorEbitaValue"),
			currentEbitaValue:component.get("v.currentEbitaValue"),
			nextEbitaValue:component.get("v.nextEbitaValue"),
			totalFundingRaisedValue:component.get("v.totalFundingRaisedValue"),
			priorPriorRevenueValue:component.get("v.priorPriorRevenueValue"),
			priorPriorEbitaValue:component.get("v.priorPriorEbitaValue"),
			strRevenueType:component.get("v.revenueValue"),
            priorNetValue:component.get("v.priorNetValue"),
            currentNetValue:component.get("v.currentNetValue"),
            nextNetValue:component.get("v.nextNetValue"),
            priorPriorNetValue:component.get("v.priorPriorNetValue"),
            priorGrossValue:component.get("v.priorGrossValue"),
            currentGrossValue:component.get("v.currentGrossValue"),
            nextGrossValue:component.get("v.nextGrossValue"),
            priorPriorGrossValue:component.get("v.priorPriorGrossValue"),
		});

		action.setCallback(this,function(response)
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
				$A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');
				component.set("v.openFinancialsModal",false);
				$A.get("e.force:refreshView").fire();
			}
			else {

	            var toastEvent = $A.get("e.force:showToast");
	            var message = '';

	            if (state === "INCOMPLETE") {
	                message = 'Server could not be reached. Check your internet connection.';
	            } else if (state === "ERROR") {
	                var errors = response.getError();
	                if (errors) {
	                    for(var i=0; i < errors.length; i++) {
	                        for(var j=0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
	                            message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
	                        }
	                        if(errors[i].fieldErrors) {
	                            for(var fieldError in errors[i].fieldErrors) {
	                                var thisFieldError = errors[i].fieldErrors[fieldError];
	                                for(var j=0; j < thisFieldError.length; j++) {
	                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
	                                }
	                            }
	                        }
	                        if(errors[i].message) {
	                            message += (message.length > 0 ? '\n' : '') + errors[i].message;
	                        }
	                    }
	                } else {
	                    message += (message.length > 0 ? '\n' : '') + 'Unknown error';
	                }
	            }

	            toastEvent.setParams({
	                title: 'Error',
	                type: 'error',
	                message: message
	            });
	            $A.util.toggleClass(component.find("spinnerIcon"), 'slds-hide');

	            toastEvent.fire();
	            // if(errorHandler) {
	            //     errorHandler(response);
	            // }
	            component.set("v.isSavingFinancial", false);  
        }
		});
		$A.enqueueAction(action);
	},

	toastErrorMessage : function (messageTitle, messageBody, messageType ) {
		var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
			title: messageTitle,
			message: messageBody,
			type: messageType ,
			 mode: 'dismissible',
			});
		toastEvent.fire();
	},
    
    fetchData : function(component, event, helper) {
		var action = component.get("c.fetchInitData");

		action.setParams({
			strRecId:component.get("v.recordId")
		});

		action.setCallback(this,function(response)
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
				var tempRes = response.getReturnValue();
				var res = JSON.parse(tempRes);
				var d = new Date();
				component.set("v.oppName",res.accountData.Name+' - '+(d.getMonth()+1)+'/'+d.getFullYear());
				var dFullYear = d.getFullYear();
                //Date logic
                component.set("v.priorPriorYear",(dFullYear-2));
                component.set("v.priorYear",(dFullYear-1));
                component.set("v.currentYear",dFullYear);
                component.set("v.nextYear",(dFullYear+1));

				component.set("v.priorPriorRevenueValue",res.priorPriorRevenueValue);
				component.set("v.priorPriorEbitaValue",res.priorPriorEbitaValue);
				component.set("v.priorRevenueValue",res.priorRevenueValue);
				component.set("v.priorEbitaValue",res.priorEbYearValue);
				component.set("v.currentRevenueValue",res.currentRevenueValue);
				component.set("v.currentEbitaValue",res.currentEbYearValue);
				component.set("v.nextRevenueValue",res.nextRevenueValue);
				component.set("v.nextEbitaValue",res.nextEbYearValue);
                
                component.set("v.priorPriorNetValue",res.priorPriorNetValue);
				component.set("v.priorPriorGrossValue",res.priorPriorGrossValue);
				component.set("v.priorNetValue",res.priorNetValue);
				component.set("v.priorGrossValue",res.priorGrossValue);
				component.set("v.currentNetValue",res.currentNetValue);
				component.set("v.currentGrossValue",res.currentGrossValue);
				component.set("v.nextNetValue",res.nextNetValue);
				component.set("v.nextGrossValue",res.nextGrossValue);

				//Picklist Inits
				component.set("v.revenueOptions",res.lstRevenueTypeOptions);
				component.set("v.totalFundingRaisedValue",(res.accountData.Total_Funding_Raised__c!=null?res.accountData.Total_Funding_Raised__c:res.accountData.DWH_Total_Funding_Raised__c))
				
                if(!$A.util.isEmpty(res.revenueType))
                {
                    component.set("v.revenueValue", res.revenueType);
                }
			}
			else if(state === "INCOMPLETE")
			{
			}
			else if(state === "ERROR")
			{
				var errors = response.getError();
				if(errors)
				{
					if(errors[0] && errors[0].message)
					{
						console.log("Error message: " + errors[0].message);
					}
				}else
				{
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	}
})