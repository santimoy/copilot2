({
    getCurrentRecordInfo : function(component,event,helper) {

        var userId = $A.get("$SObjectType.CurrentUser.Id");

        var action = component.get("c.fetchInitData");
        
        action.setParams({
            recordId : component.get("v.recordId")
        });
        
        action.setCallback(this,function(response){

            var state = response.getState();

            if(state === 'SUCCESS'){
                var res = response.getReturnValue();

                console.log('res: ',res);
                if(res.objAcc.OwnerId == userId){
                    component.set("v.isRecordOwner",true);
                     component.set("v.escalateReasonDescriptions",res.escalateReasonMdtList);
                }else{
                    component.set("v.isRecordOwner",false); 
                }

                if(res.objAcc.Escalated__c){
                    component.set("v.buttonLabel",'De-Escalate');
                }else{
                    component.set("v.buttonLabel",'Escalate')
                }
                
            }
            else if(state=== 'INCOMPLETE'){
                this.showMessage({title:'Validation',message:'Cannot Load!',type:'Error'})
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                console.log('errors',errors);
            }

        });
        $A.enqueueAction(action);

    },
    isNull:function(value){
        return value==='' ||value===undefined || value===null ? true:false; 
    },
    showToast : function( titleType, displayMessage, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
        "title": titleType,
        type : type,
        "message": displayMessage
    });
    toastEvent.fire();
    },

    getPickListValues:function(component,event,helper){
        var action = component.get("c.getPickListValues");
        action.setCallback(this,function(response){
            var state = action.getState();
            if(state === 'SUCCESS'){
                var result = response.getReturnValue();
                component.set("v.EscalateReason",result);
            }
        })
        $A.enqueueAction(action);
    },

    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            if(errors){
                if(errors.length>0 && errors[0]!=undefined && errors[0].pageErrors === undefined && errors[0].fieldErrors === undefined){
                    toastParams.message = this.extractErrorMessage(errors[0].message);
                }else if(errors[0] && errors[0].pageErrors && errors[0].pageErrors.length>0){
                    toastParams.message = errors[0].pageErrors[0].message;
                }else if(errors[0] && errors[0].fieldErrors){
                    toastParams.message = this.prepareFieldErrorMsg(errors[0].fieldErrors);
                }
            }else{
                console.log("Unknown error");
            }
        }
        this.triggerToast(toastParams);
    },
    extractErrorMessage : function(msg){
        var ind= msg.indexOf('_EXCEPTION,') ;
        if(ind > -1 ){
            ind+='_EXCEPTION, '.length;
            var lastIndex = msg.indexOf(':', ind);
            lastIndex = lastIndex < 0 ? msg.length : lastIndex;
            msg = msg.substring(ind, lastIndex);
        }
        return msg;
    },
    prepareFieldErrorMsg : function(fieldErrors){
        var keys = Object.keys(fieldErrors);
        var msg = 'Unknown error';
        if(keys.length > 0){
            msg='';
            for( var ind in keys){
                var fErrors = fieldErrors[keys[ind]];
                fErrors.forEach(function(fErr){
                    msg+= fErr.message+' \n';
                })
                
            }
        }
        return msg;
    },

    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success",
            mode: 'pester',
            duration: '8000'
        };
        
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
})