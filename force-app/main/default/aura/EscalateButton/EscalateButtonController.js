({
    doInit : function(component, event, helper) {
        //var findButton = component.find("initButton").get("v.id");
        //console.log('findButton',findButton);
        
        helper.getCurrentRecordInfo(component,event,helper);
        var EscalationTime = ['Never','In 3 Months', 'In 6 Months'];
        component.set("v.EscalationTime",EscalationTime);

        // To close the Model popup with ESC key
        window.addEventListener("keydown",function(event){
            if(event.key==='Escape'){
                component.set("v.showModal",false);
            }
        })

    },

    handleClick:function(component,event,helper){

        helper.getPickListValues(component,event,helper);
        //var findButton = event.getSource().get("v.name");
        
        var findButton = event.target.name;
        console.log('findButton: ',findButton);
        if(findButton==='De-Escalate')
        {
            component.set("v.SelectedEscalationDate",'');//merged code
            component.set("v.headerLabel",findButton);
            component.set("v.buttonSelect",findButton);
            component.set("v.showEscalateFields",false);
        }
        else{
            component.set("v.headerLabel",findButton);
            component.set("v.buttonSelect",findButton);
            component.set("v.showEscalateFields",true);
        }
        component.set("v.showModal",true);
    },

    closeModal:function(component,event,helper){
        component.set("v.showModal",false);
        component.set("v.EscalateNotes",'');
        component.set("v.selectedReason",'');
        component.set("v.commentValue",'');
        //component.find("option").set("v.value",'In 3 Months');
    },
    
    saveEscalateDeEscalate:function(component,event,helper){
            
        var errorMessage = "Please fill all the Required Fields!";
        var successMessage = "Record Updated Successfully!";
        // validating escalated noted when De-Escalate is clicked 
        if(component.get("v.buttonSelect")=='De-Escalate' && helper.isNull(component.get("v.EscalateNotes"))){
            helper.showToast("ERROR!",errorMessage,'error');
        }
        // Validating escalated Notes and escalate reason when escalate button is clicked
        else if(component.get("v.buttonSelect")!='De-Escalate'   && 
                (helper.isNull(component.get("v.EscalateNotes"))  || 
                helper.isNull(component.get("v.selectedReason")))){
 
                helper.showToast("ERROR!",errorMessage,'error');
        }
        // Saving Escalate or DeEscalate after successfully validating
        else{
            
            component.get("v.buttonSelect")==='Escalate'? component.set("v.isEscalated",true):component.set("v.isEscalated",false);
            
            var action  = component.get("c.saveRecord");
            
            var spinner = component.find('initSpinner');
            $A.util.removeClass(spinner, "slds-hide");
            
            action.setParams({
                recordID    :   component.get("v.recordId"),
                isEscalated :   component.get("v.isEscalated"),
                EscalatedNotes  :   component.get("v.EscalateNotes"),
                EscalatedReason :   component.get("v.selectedReason") ,
                SelectedEscalationDate : component.get("v.SelectedEscalationDate")
            });
            
            action.setCallback(this,function(response){
                var state = response.getState();

                console.log('Escalate state: ',state);

                if(state=== 'SUCCESS'){
                    component.set("v.showModal",false);

                    var res = response.getReturnValue();
                    console.log('Escalate res: ',res);

                    component.set("v.EscalateNotes",'');
                    component.set("v.selectedReason",'');
                    component.set("v.commentValue",'');
                    //component.set("v.isRecordOwner",res);


                    var btnLabel = component.get("v.buttonSelect") == 'Escalate' ? 'De-Escalate' : 'Escalate';
                    component.set("v.buttonLabel",btnLabel);

                    var successMessage = 'Company '+res.Name;
                    if(res.Escalated__c){
                        successMessage += ' Escalated ';
                    }else{
                        successMessage += ' De-Escalated ';
                    }
                    successMessage += 'Successfully!';

                    $A.util.addClass(spinner, "slds-hide");
                    $A.get("e.force:refreshView").fire();
                    helper.showToast('Success',successMessage,'success');
                    //helper.showMessage({title:'Success',message:successMessage,type:'success'})
                }
                else if(state=== 'INCOMPLETE'){
                    helper.showToast('Validation','Cannot Load!','Error');
                    //helper.showMessage({title:'Validation',message:'Cannot Load!',type:'Error'})
                    $A.util.addClass(spinner, "slds-hide");
                }
                else if(state === 'ERROR'){
                    var errors = response.getError();
                    console.log('errors: ',errors);
                    helper.handleErrors(errors);
                    $A.util.addClass(spinner, "slds-hide");
                }
            });

            $A.enqueueAction(action);
       }
    },
    //merged code
    handleMessageEvent : function(cmp, event, helper) {
        console.log('handleEvent started');
        var message = event.getParam("message");
        console.log('Lightning Event received: payload = ' + message);
        //Inspect the message and refresh if needed
        if(message == 'COMPANY_ASSIGNED'){
            cmp.set("v.isRecordOwner",true);
        }
        if(message == 'COMPANY_UNASSIGNED'){
            cmp.set("v.isRecordOwner",false);
        }
    }
})