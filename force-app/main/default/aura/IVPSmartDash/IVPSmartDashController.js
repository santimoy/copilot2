({
    doInit: function(cmp, event, helper) {
        document.title="Smart Dash";
        helper.handleShowPanel(cmp);
        helper.handleKeyPress(cmp);
        helper.setDefaultColor(cmp);
        helper.isOnLoad = true;
    },
	closeNav : function(cmp, event, helper) {
		helper.handleOpenPanel(cmp,false, helper);
	},
    openNav : function(cmp, event, helper) {
        helper.handleOpenPanel(cmp, true, helper);
	},
})