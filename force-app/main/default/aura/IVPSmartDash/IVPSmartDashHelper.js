({
    isOnLoad:false,
	setDefaultColor: function(cmp){
	    var color = $A.get("$Label.c.IVP_SD_WD_Tab_Color_Hex");
        if(color !== '$Label.c.IVP_SD_WD_Tab_Color_Hex does not exist.'){
            cmp.set('v.isWDBackColor', color);
        }
	},
	addClass : function(cmp, domId, cls) {
		let dom = cmp.find(domId);
		if(dom){
            $A.util.addClass(dom, cls);
        }
	},
    removeClass : function(cmp, domId, cls) {
		let dom = cmp.find(domId);
		if(dom){
            $A.util.removeClass(dom, cls);
		}
	},

    handleShowPanel : function(cmp){
        this.handleCalling(cmp, 'getTodayVisitedWatchDog', {'isDML':true}, this.handleShowPanelCB, this, null);
    },
    handleShowPanelCB : function(cmp, hlpr, response, callbackInfo){
        var isVisited = response.getReturnValue();
        hlpr.handleOpenPanel(cmp, !isVisited, hlpr);
    },
    handleCalling : function(cmp, method, params, callback, hlpr, callbackInfo) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after
        // the server-side action returns
        action.setCallback(this, function(response){
            cmp.set('v.actionCnt', cmp.get('v.actionCnt')-1);
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                if(callback){
                    callback(cmp, hlpr, response, callbackInfo);
                }
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({message:'Please refresh the page!', type:"error"});
            }else if (state === "ERROR") {
                var errors = response.getError();
                hlpr.handleErrors(errors);
            }
        })
        //this is actually use to call server side method
        cmp.set('v.actionCnt', cmp.get('v.actionCnt')+1);
        $A.enqueueAction(action);
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            toastParams.message = errors[0].message;
        }
        this.triggerToast(toastParams);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleOpenPanel : function(cmp, isOpen, hlpr){

        var param = 'c__SignalsPopout';

        var isOpenedFromDailyEmail=decodeURIComponent

       ((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;

        isOpen = ((hlpr.isOnLoad && isOpenedFromDailyEmail == 'true') || isOpen) ? true : false;

        var body = document.body, html = document.documentElement;
        cmp.set('v.hidePanel', !isOpen);
        if(isOpen){
            // var height = Math.max( body.scrollHeight, body.offsetHeight,
            // cmp.set('v.panelheight',(height -150)+'px');
            //                       html.clientHeight, html.scrollHeight, html.offsetHeight );
            let handleOpenPanelCall = hlpr.handleOpenPanel;
            body.onkeydown =  function(e){if(e.which===27){handleOpenPanelCall(cmp,false, hlpr)}};
            cmp.find('wdComp').set('v.isFocus',true);
        }else{
            cmp.find('wdComp').set('v.isFocus',false);
            body.onkeydown =  null;
        }
        hlpr.isOnLoad = false;
    },
    handleKeyPress : function(cmp){
        var doc = document;
        let handleOpenPanelCall = this.handleOpenPanel;
        let hlpr = this;
        doc.onkeydown =  function(e){
            if( e.ctrlKey && e.shiftKey ){
                if(e.key === 'ArrowLeft' ){
                    handleOpenPanelCall(cmp, true, hlpr);
                    cmp.find('wdComp').set('v.isFocus',true);
                }else if(e.key === 'ArrowRight'){
                    handleOpenPanelCall(cmp, false, hlpr);
                }
            }
        };
    },
})