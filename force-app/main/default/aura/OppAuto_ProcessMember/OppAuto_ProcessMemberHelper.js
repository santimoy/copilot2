({
	doInit : function(cmp) {
        var action = cmp.get("c.ExecuteProfitProcessData");
        action.setParams({ campId : cmp.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Campaign Member Process Batch Executed Successfuly',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                
            }else if (state === "ERROR") {
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Please contact administrator!',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
				var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();                
        });
		$A.enqueueAction(action);
    }
})