({
    doInit : function (c, e, h) {
        h.initializePreferencePreview_Helper (c, e, h, 'Preference_Preview', c.get('v.PreviewType'), c.get('v.recordId'));
    },

    closeModel: function (c, e, h) {
        c.set('v.isCustom',false);
        c.set('v.previewPopChange',false);
        c.set('v.injectEditComponent',false);
    },
    ActivateRecord: function (c, e, h) {
        c.set("v.isCloned",false);
        c.set('v.isTableView',false);
        c.set("v.isActiveRecord",true);
        c.set('v.previewPopChange',false);
    },
    editData : function(c, e, h){
        console.log('Edit button pressed');
        c.set("v.injectEditComponent",true);
    },
    /*saveData: function (c, e, h) {
        console.log('In saveData:');
    },*/
    cancel : function(c ,e ,h){
    try{
    console.log('Inside cancel button Controller');
        c.set("v.isCloned",false);
        c.set("v.isTableView", false);
    }catch(ex){
    console.log('Exception ---> '+ex);
    }

    }
})