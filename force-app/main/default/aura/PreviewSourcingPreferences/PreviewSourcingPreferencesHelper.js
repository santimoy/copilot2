({
	initializePreferencePreview_Helper : function(c, e, h, cmpName, PreviewType, recordId) {
		try{
			$A.createComponent('c:'+cmpName, {
				'pageName' : PreviewType,
				'sourceId' : recordId,
				'injectEditComponent' : c.getReference('v.injectEditComponent')
			}, function (contentComponent, status, error) {
				if (status === "SUCCESS") {
					c.set('v.preferencePreviewBody', contentComponent);
				} else {
					console.log('FAIL');
					throw new Error(error);
				}
			})
		}catch(e){
			console.log('ERROR'+e);
			console.log(e);
		}
	}
})