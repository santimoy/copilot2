({
	doInit : function(component, event, helper) {
		var action = component.get("c.getAllEmails");
		action.setParams({
			"recordId" : component.get("v.recordId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				console.log( 'SUCCESSFUL' );
				var allEmails = response.getReturnValue();
				if( allEmails ){
					var firstEmail = allEmails[0];
					component.set("v.firstEmail", firstEmail);

					var bccEmails = '';
					for(var i = 1; i < allEmails.length; i++){
						bccEmails += allEmails[i] + ';';
					}

					if( bccEmails ){
						var blasterEmails = firstEmail + '?bcc=' + bccEmails;
						component.set("v.blasterEmails", blasterEmails);
					} else {
						component.set("v.blasterEmails", '');
					}
				} else {
					component.set("v.firstEmail", '');
					component.set("v.blasterEmails", '');
				}
			} else {
				console.log('Problem getting account, response state: ' + state);
			}
		});
		$A.enqueueAction(action);
	}
})