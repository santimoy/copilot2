({
    doInIt : function(component, event, helper) {
        var action = component.get('c.getFieldList'); 
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                var options = new Array();
                var values = new Array();
                var result  = a.getReturnValue();
                console.log(result);
                for(var i=0;i<result.fieldList.length;i++){
                    var opt = new Object();
                    opt.label = result.fieldList[i];
                    opt.value = result.fieldList[i];
                    options.push(opt);
                }
                console.log(result.requiredList);
                component.set('v.values', result.selectedList);
                component.set('v.isWrapColn', result.isWrapColn);
                component.set('v.options', options);
                var reqList = new Array();
                for(var i=0;i<result.requiredList.length;i++){
                    reqList.push(result.requiredList[i]);
                }
                console.log(reqList);
                component.set("v.requiredOptions", reqList );
            }
        });
        $A.enqueueAction(action);
	},
    handleChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        var selectedOptionValue = event.getParam("value");
        var selected = selectedOptionValue.toString();
        console.log(cmp.get("v.requiredOptions"));
        selected = selected.replace("Company Name,","");
        console.log(selected);
        cmp.set("v.SelectedOption",selected);
    },
    saveData : function(component, event, helper){
        var callFrom = component.get("v.callFrom");
        var action = component.get('c.saveColumns'); 
        action.setParams({
            "columns" : component.get('v.SelectedOption'),
            "isWrapColn" : component.get('v.isWrapColn')
        });
        var dataPromise = helper.executeAction(component, action);
        console.log('Data Promise');
        
        dataPromise.then(
            function(res) {
                console.log('result');
                console.log(callFrom);
                if(callFrom == 'JustForYou'){
                    location.reload();
                }else{
                    component.set("v.isReferesh",!component.get("v.isReferesh"));
                    component.destroy();
                }
            }
        )
        .catch(
            function(error) {
                console.log(error);
            }
        );
    },
    closeModal : function(component, event, helper) {
		// when a component is dynamically created in lightning, we use destroy() method to destroy it.
		component.destroy();
	},
    onResetDefault : function(cmp, evt, hlpr){
        hlpr.onResetDefault(cmp);
    }
})