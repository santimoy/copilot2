({
    executeAction: function(component, action) {
        return new Promise(function(resolve, reject) {
            action.setCallback(this, function(a){
                var state = a.getState(); // get the response state
                if(state == 'SUCCESS') {
                    resolve('Success');
                }
            });
            $A.enqueueAction(action);
        });
    },
    onResetDefault : function(cmp){
        this.handleCalling(cmp, this, 'resetDefault', {} , this.onResetFilterCB, null);
    },
    onResetFilterCB : function(cmp, hlpr, response, callbackInfo){
        location.reload();
    },
    handleCalling : function(cmp, hlpr, method, params, callback, callbackInfo, errorCallback) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = cmp.get("c." + method);
        // passing parameter while calling
        if(Object.keys(params) ) {
            action.setParams(params);
        }
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response){
            cmp.set('v.actionCnt', cmp.get('v.actionCnt')-1);
            var state = response.getState();
            if (state === "SUCCESS"){
                // passing response to a callback method
                if(callback){
                    callback(cmp, hlpr, response, callbackInfo);
                }
            }
            else if (state === "INCOMPLETE") {
                hlpr.showMessage({title: "Validation", message: "Cannot laod!", type: "error" });
            }else if (state === "ERROR") {
                var errors = response.getError();
                hlpr.handleErrors(errors);
                cmp.set("v.reason","");
                cmp.set("v.showInputBox", false);
                cmp.set("v.showSpinner",false);
                if(errorCallback){
                    errorCallback(cmp, hlpr, response, callbackInfo);
                }
            }
            
        });
        //this is actually use to call server side method
        cmp.set('v.actionCnt', cmp.get('v.actionCnt')+1);
        $A.enqueueAction(action);
    },
    showMessage : function(message){
        // Configure success toast
        let toastParams = {
            title: "Success",
            message: "Success", // Default message
            type: "success"
        };
        // Pass the success message if any
        toastParams.message = message.message;
        if(message.title) {
            toastParams.title = message.title;
        }
        if(message.type) {
            toastParams.type = message.type;
        }
        this.triggerToast(toastParams);
    },
    triggerToast: function(toastParams) {
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert( toastParams.title + ': ' + toastParams.message );
        }
    },
    handleErrors : function(errors) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (errors && Array.isArray(errors) && errors.length > 0) {
            if(errors){
                if(errors.length>0 && errors[0]!=undefined && errors[0].pageErrors === undefined && errors[0].fieldErrors === undefined){
					toastParams.message = this.extractErrorMessage(errors[0].message);
                }else if(errors[0] && errors[0].pageErrors && errors[0].pageErrors.length>0){
                    toastParams.message = errors[0].pageErrors[0].message;
                }else if(errors[0] && errors[0].fieldErrors){
                    toastParams.message = this.prepareFieldErrorMsg(errors[0].fieldErrors);
                }
            }else{
                console.log("Unknown error");
            }
        }
        this.triggerToast(toastParams);
    },
    extractErrorMessage : function(msg){
        var ind= msg.indexOf('_EXCEPTION,') ;
        if(ind > -1 ){
            ind+='_EXCEPTION, '.length;
            var lastIndex = msg.indexOf(':', ind);
            lastIndex = lastIndex < 0 ? msg.length : lastIndex;
            msg = msg.substring(ind, lastIndex);
        }
        return msg;
    },
})