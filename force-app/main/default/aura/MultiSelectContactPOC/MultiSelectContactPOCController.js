({
    doInit : function(cmp,evt)
	{
        var recordId = cmp.get("v.recordId");
        console.log('recordId: ',recordId);
    },
    
	handleBubbling : function(component, event, helper)
	{
		helper.lkupFunc(component, event);
    },
    
    handleSave : function(component, event, helper){
        helper.saveContacts(component, event);
    },
})