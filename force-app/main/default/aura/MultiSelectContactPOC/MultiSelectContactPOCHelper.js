({
	lkupFunc : function(cmp,evt)
	{
		var srchVal = evt.getParam('lkupStr');
		var objAPIName = evt.getParam('objAPIName');
		var recordId = cmp.get("v.recordId");

        console.log('Received lkupStr: ' + srchVal + '; objAPIName ' + objAPIName);

		var action = cmp.get("c.typeAheadFuncLtng");
		action.setParams({
												"rName":srchVal,
												"sObjName":objAPIName,
												"selIds":[],
												"filter":"",
												"recordId":recordId

										 });

		action.setCallback(this,function(response)
		{
			var state = response.getState();
			if (state === "SUCCESS")
			{
				var res = response.getReturnValue();
				var tempRes = JSON.parse(JSON.stringify(res));
                console.log('Search Return val: ' + tempRes);
				evt.getSource().set('v.srchList',tempRes);
			}

			else if(state === "INCOMPLETE")
			{
			}
			else if(state === "ERROR")
			{
				var errors = response.getError();
				if(errors)
				{
						if(errors[0] && errors[0].message)
						{
								console.log("Error message: " + errors[0].message);
						}
				}else
				{
						console.log("Unknown error");
				}
			}
		});

		$A.enqueueAction(action);
    },
    
	saveContacts : function(component,event)
	{

        var contactAttendees = component.find("contactAttendees").get("v.selValList");
        var contactAttendeesList= [];
        for(var i = 0; i<contactAttendees.length;i++)
        {
            contactAttendeesList.push(contactAttendees[i].rId);
            console.log('Contact ' + contactAttendees[i].rId + ' added to list');
        }

        //save contact list to controller
	},
})