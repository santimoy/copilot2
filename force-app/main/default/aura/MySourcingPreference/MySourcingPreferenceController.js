({
    doInit : function (c, e, h) {
        c.set("v.Spinner", true);
        h.doInit_Helper (c, e, h);
    },

    BackToMyPreferences : function(c , e ,h){
        if(c.get('v.isTableView')){
             c.set('v.isTableView',false);
        }

    },

    openPreviewPopup : function(c, e, h){
         console.log('openPreviewPopup---> ');
        if(c.get("v.previewPopChange") === true){
            var sourcingPreferenceListObj = c.get('v.sourcingPreferenceList');
            var currentRecordId = c.get("v.recordId");
             var isCloned = c.get("v.isCloned");
            var externalId = null;
            if(!$A.util.isUndefinedOrNull(sourcingPreferenceListObj) && !$A.util.isEmpty(sourcingPreferenceListObj)) {
                for (var i = 0; i < sourcingPreferenceListObj.length; i++) {
                    var sourcingPreferenceEle = sourcingPreferenceListObj[i];
                    if(sourcingPreferenceEle.Id === currentRecordId || sourcingPreferenceEle.External_Id === currentRecordId){
                        externalId = sourcingPreferenceEle.External_Id;
                    }
                }
            }
            if(externalId && isCloned == false) {
                console.log('externalId=='+externalId);
                h.initializeSourcingPreference_Helper(c, e, h, 'PreviewSourcingPreferences', externalId, false, c.get('v.previewModalHeader'), 'Preview');
            } else {
                console.log('else>>>: ');
                let SaveValuesObj = c.get('v.SaveValues');
                if(!$A.util.isUndefinedOrNull(SaveValuesObj) && !$A.util.isEmpty(SaveValuesObj)) {
                    console.log('SaveValuesObj[0].newRecId=='+SaveValuesObj[0].newRecId);
                    let newRecSpID = SaveValuesObj[0].newRecId;
                    if(!$A.util.isUndefinedOrNull(newRecSpID) && !$A.util.isEmpty(newRecSpID)) {
                        h.getRelatedRecords_Helper(c, e, h, newRecSpID, 'Preview');
                    } else {
                        h.toastMsg(c, e, 'There is some technical issue while Preview the record.', 'error');
                    }
                }
            }
        }
    },

    injectEditComponent : function(c, e, h){
        console.log('injectEditComponent===');
        if(c.get("v.injectEditComponent") === true){
            var sourcingPreferenceListObj = c.get('v.sourcingPreferenceList');
            var externalRecordId = c.get("v.recordId");
            var isCloned = c.get("v.isCloned");
            var currentRecordId = null;
            if(!$A.util.isUndefinedOrNull(sourcingPreferenceListObj) && !$A.util.isEmpty(sourcingPreferenceListObj)) {
                for (var i = 0; i < sourcingPreferenceListObj.length; i++) {
                    var sourcingPreferenceEle = sourcingPreferenceListObj[i];
                    if(sourcingPreferenceEle.External_Id === externalRecordId  || sourcingPreferenceEle.Id === externalRecordId){
                        currentRecordId = sourcingPreferenceEle.Id;
                    }
                }
            }
           // console.log('currentRecordId>>>: '+currentRecordId);
            if(currentRecordId && isCloned == false) {
                h.initializeSourcingPreference_Helper(c, e, h, 'QueryBuilderComponent', currentRecordId, false, c.get('v.editModalHeader'));
            } else {
                console.log('else>>>: ');
                c.set("v.isCloned",false);
                let SaveValuesObj = c.get('v.SaveValues');
                if(!$A.util.isUndefinedOrNull(SaveValuesObj) && !$A.util.isEmpty(SaveValuesObj)) {
                     //console.log('SaveValuesObj[0] Inject=='+SaveValuesObj[0].newRecId);
                    let newRecordId = SaveValuesObj[0].newRecId;
                    if(!$A.util.isUndefinedOrNull(newRecordId) && !$A.util.isEmpty(newRecordId)) {
                        h.initializeSourcingPreference_Helper(c, e, h, 'QueryBuilderComponent', newRecordId, false, c.get('v.editModalHeader'));
                    } else {
                        h.toastMsg(c, e, 'There is some technical issue while Edit the record.', 'error');
                    }
                }
            }
            c.set("v.injectEditComponent",false);
        }
    },

    activeRecordAction : function(c, e, h){
        if(c.get("v.isActiveRecord") === true){
            var sourcingPreferenceListObj = c.get('v.sourcingPreferenceList');
            var externalRecordId = c.get("v.recordId");
            var currentRecordId = null;
            if(!$A.util.isUndefinedOrNull(sourcingPreferenceListObj) && !$A.util.isEmpty(sourcingPreferenceListObj)) {
                for (var i = 0; i < sourcingPreferenceListObj.length; i++) {
                    var sourcingPreferenceEle = sourcingPreferenceListObj[i];
                    if(sourcingPreferenceEle.External_Id === externalRecordId){
                        currentRecordId = sourcingPreferenceEle.Id;
                    }
                }
            }
            if(currentRecordId) {
                h.objectChanged_helper(c, e, h, currentRecordId, true);
            } else {
                let SaveValuesObj = c.get('v.SaveValues');
                if(!$A.util.isUndefinedOrNull(SaveValuesObj) && !$A.util.isEmpty(SaveValuesObj)) {
                    let newRecordId = SaveValuesObj[0].newRecId;
                    if(!$A.util.isUndefinedOrNull(newRecordId) && !$A.util.isEmpty(newRecordId)) {
                        h.objectChanged_helper(c, e, h, newRecordId, true);
                    } else {
                        h.toastMsg(c, e, 'There is some technical issue while Activate the record.', 'error');
                    }
                }
            }
            c.set("v.isActiveRecord",false);
        }
    },

    handleSelect : function (c, e, h) {
        var recordId = e.getSource().get('v.value');
        var buttonName = e.getSource().get('v.name');
        c.set("v.recordId",recordId);
        //console.log('recordId: '+recordId);
        //console.log('buttonName: '+buttonName);
        if(buttonName === 'Preview') {
            h.initializeSourcingPreference_Helper(c, e, h, 'PreviewSourcingPreferences', recordId, false, c.get('v.previewModalHeader'),'Preview');
        } else if(buttonName === 'Edit') {
            h.initializeSourcingPreference_Helper(c, e, h, 'QueryBuilderComponent', recordId, false, c.get('v.editModalHeader'));
        } else if(buttonName === 'Clone') {
            h.initializeSourcingPreference_Helper(c, e, h, 'QueryBuilderComponent', recordId, true, c.get('v.cloneModalHeader'));
        } else if(buttonName === 'Delete') {
            c.set('v.DeletePreferenceDetail',recordId);
            c.set('v.isDeletePreferenceConfirmationPopup', true);
       // }else if(buttonName === 'Archive') {
         //   h.initializeSourcingPreference_Helper(c, e, h, 'SourcingPreferenceArchive', recordId, true, '');
        } else {
            this.toastMsg(c, e, 'There is some technical issue while deleting the record.', 'error');
        }
    },

    openPreviewUniverse : function(c, e, h){
        h.initializeSourcingPreference_Helper(c, e, h, 'PreviewSourcingPreferences', '', false, c.get('v.previewModalHeader'),'Preview Universe');
    },

    handleRejectRadioClick : function (c, e, h){
        var rejectOption = e.getSource().get('v.value');
        if(rejectOption ==='Yes'){
            var DeletePreferenceDetail = c.get('v.DeletePreferenceDetail');
            if(!$A.util.isUndefinedOrNull(DeletePreferenceDetail) && !$A.util.isEmpty(DeletePreferenceDetail)){
                h.deletePreferenceDetail_Helper(c, e, h, DeletePreferenceDetail);
            }
        }else{
            c.set('v.DeletePreferenceDetails',null);
            c.set('v.isDeletePreferenceConfirmationPopup',false);
        }
    },

    addNewPreference : function (c, e, h) {
        h.initializeSourcingPreference_Helper(c, e, h, 'QueryBuilderComponent', null, false, c.get('v.newModalHeader'));
    },

    objectChanged: function (c, e, h) {
        c.set("v.Spinner", true);
        var recordId = e.getSource().get('v.value');
        var buttonName = e.getSource().get('v.name');//get true or false value.
        if(!$A.util.isUndefinedOrNull(recordId) && !$A.util.isEmpty(recordId)
            && !$A.util.isUndefinedOrNull(buttonName) && !$A.util.isEmpty(buttonName)) {
            h.objectChanged_helper(c, e, h, recordId, buttonName);
        }
    }
})