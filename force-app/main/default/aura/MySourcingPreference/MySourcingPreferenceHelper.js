({
    doInit_Helper : function (c, e, h) {
        try{
            var action = c.get("c.MySourcingPreferencesRecords_Apex")
            action.setCallback(this, function (r) {
                if(r.getState() === 'SUCCESS') {
                    var storedResponse = r.getReturnValue();
                    let iaActivePreference = false; 
                    if(!$A.util.isUndefinedOrNull(storedResponse) && !$A.util.isEmpty(storedResponse)) {
                        var newPreferenceList = [];
                        for (var i = 0; i < storedResponse.length; i++) {
                            var rEle = storedResponse[i];
                            var newEle = {};
                            newEle.Id = rEle.Id;
                            newEle.Name = rEle.Name;
                            newEle.Name__c = rEle.Name__c;
                            newEle.Include_Exclude__c = rEle.Include_Exclude__c;
                            newEle.Validation_Status__c = rEle.Validation_Status__c;
                            newEle.Status__c = rEle.Status__c;
                            newEle.External_Id = rEle.External_Id__c;
                            newEle.Preference_Status__c = rEle.Preference_Status__c;
                            if(rEle.Status__c === 'Active'){
                                if(iaActivePreference === false){
                                    iaActivePreference = newEle.Validation_Status__c === 'Valid' && rEle.Include_Exclude__c === 'Include';
                                }
                                newEle.isStatus = true;
                            } else {
                                newEle.isStatus = false;
                            }
                            newPreferenceList.push(newEle);
                        }
                        //console.log('newPreferenceList >>'+JSON.stringify(newPreferenceList));
                        c.set('v.sourcingPreferenceList',newPreferenceList);
                        c.set("v.Spinner", false);
                    } else {
                        c.set('v.sourcingPreferenceList',[]);
                        c.set("v.Spinner", false);
                    }
                    c.set('v.iaActivePreference', iaActivePreference);
                } else {
                    c.set("v.Spinner", false);
                    console.log('ERROR');
                    console.log(r.getError());
                }
            });
            $A.enqueueAction(action);
        }catch(e){
            c.set("v.Spinner", false);
            console.log('ERROR'+e);
            console.log(e);
        }
    },

    openPopup_Helper: function(c, e, h, cmpName, isOpenPopup){
        try{
            $A.createComponent('c:'+cmpName, {
                'isOpened' : isOpenPopup
            }, function (contentComponent, status, error) {
                if (status === "SUCCESS") {
                    //console.log('SUCCESS');
                    c.set('v.previewSourcingPreferences', contentComponent);
                } else {
                    c.set('v.isOpened', false);
                    console.log('FAIL');
                    throw new Error(error);
                }
            })
        }catch(e){
            console.log('ERROR'+e);
            console.log(e);
        }
    },

    initializeSourcingPreference_Helper : function (c, e, h, cmpName, recordId, isClone, modalHeader, btnCmp) {
        try{
           
            c.set('v.isCustom', true);
            $A.createComponent('c:'+cmpName, {
                'recordId' : recordId,
                'isClone' : isClone,
                'modalHeader' : modalHeader,
                'isCustom' : c.getReference('v.isCustom'),
                'previewPopChange' : c.getReference('v.previewPopChange'),
                'isCloned' : c.getReference('v.isCloned'),
                'injectEditComponent' : c.getReference('v.injectEditComponent'),
                'isActiveRecord' : c.getReference('v.isActiveRecord'),
                'isTableView' : c.getReference('v.isTableView'),
                'PreviewType' : btnCmp,
                'SaveValues' : c.getReference('v.SaveValues'),
                'isSpinner' : c.getReference('v.isSpinner'),
                'picklistBody' : c.getReference('v.picklistBody'),
                'intFieldRecList' : c.getReference('v.intFieldRecList'),
                'setFilters' : c.getReference('v.setFilters')
            }, function (contentComponent, status, error) {
                if (status === "SUCCESS") {
                    //console.log(contentComponent);
                    c.set('v.sourcingPreferencesBody', contentComponent);
                    c.set('v.isTableView', true);
                } else {
                    c.set('v.isCustom', false);
                    console.log('FAIL');
                    throw new Error(error);
                }
            })
        }catch(e){
            console.log('ERROR'+e);
            console.log(e);
        }
    },

    deletePreferenceDetail_Helper : function (c, e, h, preferenceRecordId) {
        try {
            if(preferenceRecordId) {
                var action = c.get("c.DeletePreferenceDetail_Apex");
                action.setParams({
                    "preferenceRecordId": preferenceRecordId
                });
                action.setCallback(this, function (r) {
                    if(r.getState() === 'SUCCESS') {
                        var storedResponse = r.getReturnValue();
                        if(storedResponse === 'Success'){
                            c.set('v.DeletePreferenceDetails',null);
                            c.set('v.isDeletePreferenceConfirmationPopup',false);
                            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                            dismissActionPanel.fire();
                            this.toastMsg(c, e, 'Record deleted Successfully', 'success');
                            $A.get('e.force:refreshView').fire();
                        } else {
                            this.toastMsg(c, e, 'There is some technical issue while deleting the record.', 'error');
                        }
                    } else {
                        this.toastMsg(c, e, 'Error message='+ JSON.stringify(r.error), 'error');
                    }
                });
                $A.enqueueAction(action);
            } else {
                this.toastMsg(c, e, 'There is some technical issue while saving the record.', 'error');
            }
        } catch (ex) {
            console.log(ex);
        }
    },

    objectChanged_helper :function(c, e, h, recordId, isAction) {
        try{
            var action1 = c.get("c.saveSourcingPreference");
            action1.setParams({
                "recSave": recordId, //JSON.stringify(obj)
                "action": isAction
            });
            action1.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS"){
                    var res = response.getReturnValue();
                    if(res == true){
                        //this.toastMsg(c, e, 'Record updated Successfully', 'success');
                        h.doInit_Helper (c, e, h);
                        //$A.get('e.force:refreshView').fire();
                    } else {
                        this.toastMsg(c, e, 'There is some technical issue while saving the record.', 'error');
                    }
                } else if (response.state === "ERROR") {
                    this.toastMsg(c, e, 'Error message='+ JSON.stringify(response.error), 'error');
                }
            });
            $A.enqueueAction(action1);
        }catch(e){
            console.log('ERROR'+e);
            console.log(e);
        }
    },

    toastMsg : function (c, e, msg, type ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": msg
        });
        toastEvent.fire();
    },

    getRelatedRecords_Helper : function (c, e, h, newRecordId, modelType) {
        try{
            let action = c.get("c.getCurrentRecordDetails_Apex");
            action.setParams({
                "newRecordId": newRecordId
            });
            action.setCallback(this, function (r) {
                if(r.getState() === 'SUCCESS') {
                    let storedResponse = r.getReturnValue();
                    let externalId = storedResponse.External_Id__c;
                    if(modelType === 'Preview') {
                        h.initializeSourcingPreference_Helper(c, e, h, 'PreviewSourcingPreferences', externalId, false, c.get('v.previewModalHeader'), 'Preview');
                    }
                } else {
                    console.log('ERROR');
                    console.log(r.getError());
                }
            });
            $A.enqueueAction(action);
        }catch(e){
            console.log('ERROR'+e);
            console.log(e);
        }
    }
})