@isTest
private class IVP_CompanyTaggingVFCtrlTest {

	private static testMethod void testCompanyTagging() {
	    Test.startTest();
	    
	    List<Account> listAccount = new List<Account>();
	    listAccount.add(new Account(Name='Test-Account-1'));
	    listAccount.add(new Account(Name='Test-Account-1'));
	    
        insert listAccount;
        
        ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(listAccount);
        sc1.setSelected(listAccount);
        
        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(new List<Account>());
        
        
        IVP_CompanyTaggingVFCtrl companyTaggingController1 = new IVP_CompanyTaggingVFCtrl( sc1);
        IVP_CompanyTaggingVFCtrl companyTaggingController2 = new IVP_CompanyTaggingVFCtrl( sc2);
        Test.stopTest();
        
        System.assertEquals(true,companyTaggingController1.accountIds!= null);
        System.assertEquals('Not Selected',companyTaggingController2.accountIds);
        
	}
	
}