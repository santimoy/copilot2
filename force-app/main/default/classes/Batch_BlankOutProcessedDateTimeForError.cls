/*
Class Name   :  Batch_BlankOutProcessedDateTimeForError
Description  :  To Blank out Processed Date Time where isError is true
Author       :  Sukesh
Created On   :  October 14 2019
*/

global with sharing class Batch_BlankOutProcessedDateTimeForError implements Database.Batchable<sObject> {

    String strQuery = 'SELECT Id, Callout_Response__c,Trimmed_Callout_response__c FROM Key_Contact_Scope__c WHERE isError__c=true'; //AND Id=\'a6Mm00000004r7Q\'

    global Batch_BlankOutProcessedDateTimeForError() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Key_Contact_Scope__c> lstKeyContactScope) {

        List<Key_Contact_Scope__c> lstKeyContactScopeToUpdate = new List<Key_Contact_Scope__c>();

        for(Key_Contact_Scope__c objKeyContactScope : lstKeyContactScope) {

            lstKeyContactScopeToUpdate.add(new Key_Contact_Scope__c(Id=objKeyContactScope.Id,
                                                                    Processed_Date_Time__c = null));
        }

        update lstKeyContactScopeToUpdate;
    }

    global void finish(Database.BatchableContext BC) {

    }
}