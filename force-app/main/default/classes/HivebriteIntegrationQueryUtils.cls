/**
 * @File Name          : HivebriteIntegrationQueryUtils.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 1/11/2019, 2:52:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/11/2019   Anup Kage     Initial Version
**/
public with sharing class HivebriteIntegrationQueryUtils {
    private static Map<String, Map<String, Recordtype>> recordTypeByObjectName;
    private static Map<String, Integration_API_Object__mdt> apiObjectByLabel;
    private static Map<String, Integration_API_Request__mdt> apiRequestByLabel;
    private static Map<String, List<Integration_API_Field__mdt>> apiFieldByLabel;
    private static Map<String, Integration_API_Field__mdt> externalIdFieldByLabel;
    private static Map<String, List<Integration_API_Rule__mdt>> apiRulesByLabel;

    /**
    * It will return the recordtypes of object.
    * @Param ObjectName
    * @return Map<String, Recordtype> RecordTypeByName
    */
    public static Map<String, Recordtype> getRecordTypeValue(String objectName){
        if(recordTypeByObjectName == null){
            recordTypeByObjectName = new Map<String, Map<String, Recordtype>>();
        }
        if(!recordTypeByObjectName.containsKey(objectName)){
            Map<String, Recordtype> recordTypeByName = new Map<String, RecordType>();
            for(Recordtype rt : [SELECT Id, Name, DeveloperName FROM Recordtype WHERE SObjectType =: objectName ] ){
                recordTypeByName.put(rt.Name, rt);
            }
            recordTypeByObjectName.put(objectName, recordTypeByName);
        }
        return recordTypeByObjectName.get(objectName);
    }

    /**
    * it will return api object record based on the Label
    * @param apiObjectLabel
    * @return Integration_API_Object__mdt
    */
    public static Integration_API_Object__mdt getApiObject(String apiObjectLabel){
        if(apiObjectByLabel == null){
            apiObjectByLabel = new Map<String, Integration_API_Object__mdt>();
        }
        if(!apiObjectByLabel.containsKey(apiObjectLabel)){            
            for(Integration_API_Object__mdt api :[ SELECT Id, Label, Integration_API__c, Integration_Object_Name__c, Object_API_Name__c, Object_Label__c 
                                                    FROM Integration_API_Object__mdt
                                                    WHERE Label =: apiObjectLabel])
            
            {
                apiObjectByLabel.put(api.label, api);
            }
        }
        return apiObjectByLabel.get(apiObjectLabel);
    }
   
   /**
    * It will return api request based on the Label
    * @Param apiRequestLabel
    * @return Integration_API_Request__mdt
    */ 
    public static Integration_API_Request__mdt getApiRequest(String apiRequestLabel){
        if(apiRequestByLabel == null){
            apiRequestByLabel = new Map<String, Integration_API_Request__mdt>();
        }
        if(!apiRequestByLabel.containsKey(apiRequestLabel)){
            for(Integration_API_Request__mdt apiReq : [ SELECT Integration_API__r.Base_URL__c, Path__c, label,Verb__c 
                                                        FROM Integration_API_Request__mdt 
                                                        WHERE Label =: apiRequestLabel ])
            {
                apiRequestByLabel.put(apiReq.Label, apiReq);
            }
        }
        return apiRequestByLabel.get(apiRequestLabel);
    }

    /**
     * It will return api field based on the api Label 
     * @Param apiObjectLabel
     * @return list of Integration_API_Field__mdt
     */
    public static List<Integration_API_Field__mdt> getMapingFieldsRecords(String apiObjectLabel){
      
        if(apiFieldByLabel == null){
            apiFieldByLabel = new Map<String,List<Integration_API_Field__mdt>>();
        } 
        if(!apiFieldByLabel.containsKey(apiObjectLabel)){
           List<Integration_API_Field__mdt> lstApiFld = new List<Integration_API_Field__mdt>
                                                     ([ SELECT ID, Data_Type__c, Integration_Field_Name__c, Precision__c, Size__c, Field_API_Name__c, External_Id__c, Required_Field__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Label =: apiObjectLabel ]);
            apiFieldByLabel.put(apiObjectLabel, lstApiFld);
        }
        return apiFieldByLabel.get(apiObjectLabel);
   }
   /** 
   get Exteranl FieldAPi name from the Integration_API_Field__mdt 
   @ param apiObjectLabel
    */
    public static Integration_API_Field__mdt getExternalIdField(String apiObjectLabel){
        
        if(externalIdFieldByLabel == null){
            externalIdFieldByLabel = new Map<String, Integration_API_Field__mdt>();
        } 
        if(!externalIdFieldByLabel.containsKey(apiObjectLabel)){
            Integration_API_Field__mdt extrenalIdFld = [ SELECT ID, Data_Type__c, Integration_Field_Name__c, Precision__c, Size__c, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Label =: apiObjectLabel and External_Id__c = true  limit 1];
            externalIdFieldByLabel.put(apiObjectLabel, extrenalIdFld);
        }
        return externalIdFieldByLabel.get(apiObjectLabel);
    }
    /**
    * @description 
    * @author Anup Kage | 11/12/2019 
    * @param apiObjectLabel 
    * @return List<Integration_API_Rules__mdt> 
    **/
    public static List<Integration_API_Rule__mdt> getApiRules(String apiObjectLabel) {
        if(apiRulesByLabel == null){
             apiRulesByLabel = new Map<String, List<Integration_API_Rule__mdt>>();
        }
        if(!apiRulesByLabel.containsKey(apiObjectLabel)){
            List<Integration_API_Rule__mdt>  apiRulesList = [ SELECT Id, Field__c, Operator__c, Order__c, Value__c ,Integration_API_Object__r.Filter_Logic__c
                                                            FROM Integration_API_Rule__mdt 
                                                            WHERE Integration_API_Object__r.Label =: apiObjectLabel ];
            apiRulesByLabel.put(apiObjectLabel, apiRulesList);
        }
        return apiRulesByLabel.get(apiObjectLabel);
    }
}