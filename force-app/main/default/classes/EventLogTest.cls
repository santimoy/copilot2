@isTest(SeeAllData=true)
Public class EventLogTest  { 

//BigObjectexception
static testmethod void testBigObjectException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try {
        throw new BigObjectException('failed');
    } catch (BigObjectException e) {
            EventLog.generateLog(request, response, className, methodName, e);
        EventLog.generateLog(request, className, methodName, e);
    }
}  
    
//Externalobjectexception
static testmethod void testExternalObjectException() {
        List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try {
        throw new ExternalObjectException('failed');
    } catch (ExternalObjectException e) {
            EventLog.generateLog(request, response, className, methodName, e);
        EventLog.generateLog(request, className, methodName, e);
    }
}

//exception
static testmethod void testexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        CalloutException e = new CalloutException();
        //  e.setMessage('This is a constructed exception!');
        throw e;
    }catch (exception exp) {
        EventLog.generateLog(request, response, className, methodName, exp);
        EventLog.generateLog(request, className, methodName, exp);
    }
}  

// AsyncException
static testmethod void testAsyncException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        System.enqueueJob(new Event_Log__c());
    }catch (AsyncException asyneExp) {
        EventLog.generateLog(request, response, className, methodName, asyneExp); 
            EventLog.generateLog(request,  className, methodName, asyneExp); 
    }
}

// CalloutException    
static testmethod void testcalloutexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    Http http = new Http();
    request.setMethod('GET');
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        if(Test.isRunningTest()) {
        CalloutException e = new CalloutException();
    e.setMessage('This is a constructed exception for testing and code coverage');
    throw e;
        }
    }catch (CalloutException callOutExp){
        EventLog.generateLog(request, response, className, methodName, callOutExp); 
            EventLog.generateLog(request,  className, methodName, callOutExp); 
    }  
}

// EmailException
static testmethod void testemailexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }catch (EmailException emailExp) {
        EventLog.generateLog(request, response, className, methodName, emailExp); 
            EventLog.generateLog(request,  className, methodName, emailExp); 
    } 
}

// DMLException
static testmethod void testdmlexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
            
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try{
        Event_Log__c obj = new Event_Log__c();
        insert obj;
        
    }catch (DmlException dmlExp) {
        EventLog.generateLog(request, response, className, methodName, dmlExp); 
        EventLog.generateLog(request, className, methodName, dmlExp); 
    }
}

// JSONException
static testmethod void testjsonexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try{
        Decimal n = (Decimal)JSON.deserialize('00/', Decimal.class);
    }catch (JSONException jsonExp) {
        EventLog.generateLog(request, response, className, methodName, jsonExp); 
        EventLog.generateLog(request, className, methodName, jsonExp); 
    }
}  

// //LimitException
// static testmethod void testLimitException(){
//     List<Event_Log__c> lstobj = new List<Event_Log__c>();
//     HttpRequest request = new HttpRequest(); 
//     HttpResponse response = new HttpResponse();
//     String className;
//     String methodName;
    
//     Event_Log__c objEventLog;
//     String lineNumber;
//     String getMessage;
//     try {
//             throw new LimitException('failed');
//     } catch (LimitException e) {
//         EventLog.generateLog(request, response, className, methodName, e); 
//         EventLog.generateLog(request, className, methodName, e);
//     }
// } 

    //SearchException
static testmethod void testSearchException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;

    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try {
        throw new SearchException('failed');
    } catch (SearchException e) {
            EventLog.generateLog(request, response, className, methodName, e); 
        EventLog.generateLog(request, className, methodName, e); 
    }
} 

//SecurityException
static testmethod void testSecurityException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try {
        throw new SecurityException('failed');
    } catch (SecurityException e) {
            EventLog.generateLog(request, response, className, methodName, e); 
        EventLog.generateLog(request, className, methodName, e); 
    }
} 

//ListException
static testmethod void testlistexception(){
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    try{
        List<Integer> lst = new List<Integer>();
        lst.add(15);
        Integer i1 = lst[0]; 
        Integer i2 = lst[1]; 
    }catch (ListException lstExp) {
        EventLog.generateLog(request, response, className, methodName, lstExp);
            EventLog.generateLog(request,  className, methodName, lstExp); 
    }
} 

// MathException
static testmethod void testmathexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        Integer x = 4/0 ;
    }catch (MathException mathExp) {
        EventLog.generateLog(request, response, className, methodName, mathExp); 
            EventLog.generateLog(request, className, methodName, mathExp); 
    } 
}  

// NoAccessException
static testmethod void testnoaccessexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Note nt = new Note();
        nt.Title = 'Test Account Note';
        nt.Body = 'Test Account Note Body.';
        nt.ParentId = acc.Id;
        insert nt;
        Test.setCreatedDate(acc.Id, DateTime.now().addMonths(-6));
        Test.setCreatedDate(nt.Id, DateTime.now().addMonths(-6));
    }catch (NoAccessException noAccessxExp) {
        EventLog.generateLog(request, response, className, methodName, noAccessxExp); 
            EventLog.generateLog(request,  className, methodName, noAccessxExp); 
    }
}  


//NullPointerException
static testmethod void testnullpointerexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        String s;
        s.toLowerCase();
        System.debug('>>>>>>> TRY NullPointerException');
    }catch (NullPointerException nullPointExp) {
        System.debug('>>>>>>>>>>>>NullPointerException>>>>>>>>>>');
        EventLog.generateLog(request, response, className, methodName, nullPointExp); 
        EventLog.generateLog(request,  className, methodName, nullPointExp); 
    }
}

//QueryException
static testmethod void testqueryexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        obj = [SELECT Name FROM Event_Log__c WHERE Name='ABC'];
    }catch (QueryException qryExp) {
        EventLog.generateLog(request, response, className, methodName, qryExp);
        EventLog.generateLog(request,  className, methodName, qryExp);  
    }
}  

//RequiredFeatureMissingException
static testmethod void testRequiredFeatureMissingException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    obj.Type__c='Error';
    try {
        throw new RequiredFeatureMissingException('failed');
    } catch (RequiredFeatureMissingException e) {
            EventLog.generateLog(request, response, className, methodName, e); 
        EventLog.generateLog(request,className, methodName, e); 
    }
}  

//StringException
static testmethod void teststringexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        Event_Log__c elObj = new Event_Log__c(id='abcd');
    }catch (StringException strExp) {
        EventLog.generateLog(request, response, className, methodName, strExp); 
        EventLog.generateLog(request, className, methodName, strExp); 
    }
}

// SObjectException 
static testmethod void sObjExpTest(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    final String BAD_FIELD_NAME = 'namexxxxx';
    try{
        (new Event_Log__c()).get(BAD_FIELD_NAME);
        
    }catch (SObjectException sObjExp) {
        EventLog.generateLog(request, response, className, methodName, sObjExp); 
            EventLog.generateLog(request,  className, methodName, sObjExp); 
    }
}

//TypeException
static testmethod void testtypeexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        String s='a';  
        Integer i=Integer.valueOf(s);        
    }catch (TypeException typeExp) {
        EventLog.generateLog(request, response, className, methodName, typeExp); 
            EventLog.generateLog(request,  className, methodName, typeExp); 
    }  
} 

// XmlException
static testmethod void testxmlexception(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try{
        String bodyXMLFinal  ='';
        DOM.Document doc = new DOM.Document();
        String toParse = bodyXMLFinal ;
        doc.load(toParse);
    }catch (XmlException xmlExp) {
        EventLog.generateLog(request, response, className, methodName, xmlExp); 
            EventLog.generateLog(request,  className, methodName, xmlExp); 
    }
}
            
//Nodatafoundexception
    static testmethod void testNoDataFoundException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try {
        throw new NoDataFoundException();
    } catch (NoDataFoundException e) {
        EventLog.generateLog(request, response, className, methodName, e); 
            EventLog.generateLog(request,  className, methodName, e); 
    }
    
}


//NoSuchElementException
    static testmethod void testNoSuchElementException(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try {
        throw new NoSuchElementException('failed');
    } catch (NoSuchElementException e) {
            EventLog.generateLog(request, response, className, methodName, e); 
            EventLog.generateLog(request,  className, methodName, e); 
    }
    
}

//serializationexception
    static testmethod void testSerializationException() {
        List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    
    Event_Log__c obj = new Event_Log__c();
    try {
        throw new SerializationException('failed');
    } catch (SerializationException e) {
        EventLog.generateLog(request, response, className, methodName, e); 
            EventLog.generateLog(request,  className, methodName, e); 
    }
    
}

//VisualforceException
    static testmethod void testVisualforceException() {
        List<Event_Log__c> lstobj = new List<Event_Log__c>();
    HttpRequest request = new HttpRequest(); 
    HttpResponse response = new HttpResponse();
    String className;
    String methodName;
    
    Event_Log__c objEventLog;
    String lineNumber;
    String getMessage;
    try {
        throw new VisualforceException('failed');
    } catch (VisualforceException e) {
            EventLog.generateLog(request, response, className, methodName, e); 
            EventLog.generateLog(request,  className, methodName, e); 
    }
}
    
static testmethod void testCommit(){
    List<Event_Log__c> lstobj = new List<Event_Log__c>();
    try {
        insert lstobj;
        
    } catch (DmlException dmlexp) {
        System.debug('Error On inserting ErrorLogs: '+dmlexp.getCause() + ' '+dmlexp.getMessage()); 
    }
    EventLog.commitLogs();
}


    static testmethod void testEventlog(){
    Account a = new Account();
    Account a2 = new Account(Name = 'Test');
    Eventlog.generateLog('EventLog','testclass', Database.upsert(new List<Account>{a, a2}, false));
    Eventlog.generateLog('EventLog','testclass', Database.insert(new List<Account>{a, a2}, false));
    Eventlog.generateLog('EventLog','testclass', Database.upsert(a, false));
    Eventlog.generateLog('EventLog','testclass', Database.insert(a, false));
}
}