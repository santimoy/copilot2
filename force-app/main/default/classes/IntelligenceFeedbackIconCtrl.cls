public class IntelligenceFeedbackIconCtrl {
    
    @RemoteAction 
    @AuraEnabled 
    public static FeedbackData getFeedbackData(){
        FeedbackData fData= new FeedbackData();
        fData.icons = [SELECT Id, DeveloperName, MasterLabel, Label, 
                Value__c, Indicator__c, Field_Name__c, Icon__c,Record_Level_Icon__c, 
                Apply_Opacity__c, Toggle_Icon__c, Opacity_Field_Name__c, Hover_Text__c,
                Toggle_Value__c, Order__c
                FROM Intelligence_Feedback_Icons__mdt];
        fData.iiFields = [SELECT Id, DB_Column__c, DB_Table__c, Static_Selection__c, Order__c, DB_Field_Name__c 
                          FROM Intelligence_Field__c 
                          WHERE DB_Table__c='ct.company_data'
                          AND Active__c=true 
                          ORDER by Order__c];
        fData.dwhSetting = DWH_Settings__c.getInstance(UserInfo.getUserId());
        fData.org = [SELECT Id FROM Organization LIMIT 1];
        return fData;
    }
    @AuraEnabled
    public static String logAjax(String dataJSON){
        Logger.push('IntelligenceFeedbackIcon','logAjax');
        Datetime start = system.now();
		Logger.debugException(dataJSON);
        Logger.pop();
        Datetime ends = system.now();
        List<Log__c> logs = [SELECT Id,Name FROM Log__c 
                             WHERE CreatedById=:UserInfo.getUserId()
                             AND CreatedDate>=:start AND CreatedDate<=:ends];
        
        String result = Label.DWH_API_Time_Out ;
        if(!logs.isEmpty()){
            System.debug(logs[0].Name);
            result ='refId: '+String.escapeSingleQuotes(logs[0].Name)+', ' +result;
        }
        return result;
    }
    
    public class FeedbackData{
        @AuraEnabled public List<Object> icons{get;set;}
        @AuraEnabled public List<Object> iiFields{get;set;}
        @AuraEnabled public Object dwhSetting{get;set;}
        @AuraEnabled public Organization org{get;set;}
    }
}