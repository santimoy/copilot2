public class financialsController {
  
  // Constructor
 public financialsController(ApexPages.StandardController controller) {
  this.acct = (Account)controller.getSubject();
  
     this.getCurrentFinancials();
 }
 
 //Method to get current list
 private void getCurrentFinancials() { 
  
     this.financials = [ SELECT 
      d.SystemModstamp, d.Name, 
      d.LastModifiedDate, d.LastModifiedById, d.Id, d.CreatedDate, d.CreatedById, 
      d.Account__c, d.Year_Text__c, d.EBITDA__c, d.Revenue__c, d.Cash__c, d.COGS__c, d.Debt__c, d.Gross_Margin__c, d.Head_Count__c, d.Customers__c, d.OpEx__c,
      d.IVP_EBITDA_Forecast__c, d.IVP_Revenue_Forecast__c,d.Account__r.OwnerId
      FROM 
      Financials__c d
      WHERE 
      d.account__c = :acct.id
      order By d.Year_Text__c Nulls last];
    newFinancials = new list<financials__c>();
 }
 
 // Action Method called from page button
 public pagereference saveChanges() {
  upsert this.financials;
  if (newFinancials.size()>0) upsert newFinancials;
  return null;
 }
 
 public PageReference addFinacials(){
     Financials__c newFin = new Financials__c(account__c = acct.Id);
     financials.add(newFin);
     newFinancials.add(newFin);
     return null;
 }
 
 // Action Method called from page link
 public pagereference newFinancials() { 
  financials__c d = new financials__c();
  d.account__c = this.acct.id; 
  financials.add( d );
  return null;
 }
 
  
  // public Getter to provide table headers 
 public string[] getheaders() { return new string [] 
  {'Year','Revenue','EBITDA', 'Gross Margin', 'Head Count', 'Customers'} ; }
   
 
 //public Getter to list financials
 public financials__c[] getFinancials() { 
  return this.financials; 
 } 
 
   // cancel action button
    public PageReference Cancel() {
    this.getCurrentFinancials();
    return null;
}
    
    
 //class variables
 Account acct;
 list<financials__c> financials;
 list<financials__c> newFinancials; 
}