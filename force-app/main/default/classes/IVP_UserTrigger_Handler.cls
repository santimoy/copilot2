/**
* @author Amjad-Concretio
* @date 2018-05-21
* @class IVP_UserTrigger_Handler
* @description handler to handle user trigger events
*/
public class IVP_UserTrigger_Handler {
    public static String analystFullProfileId{
        get{
            if(analystFullProfileId == null){
            	analystFullProfileId = '';
                List<Profile> profiles = [Select Id from Profile where name = 'Analyst Full'];
                if(profiles != null && !profiles.isEmpty()){
                    analystFullProfileId = profiles.get(0).Id;
                }
            }
            return analystFullProfileId;
        }set;
    }
    
    public static void createGroupOnInsert(List<User> users){
        checkActiveAnalystAndCreateGroup(users, new Map<Id, User>(), true, false);
    }
    
    public static void createGroupOnUpdate(List<User> users, Map<Id,User> oldMap){
        checkActiveAnalystAndCreateGroup(users, oldMap, false, true);
    }
    private static void checkActiveAnalystAndCreateGroup(List<User> users, Map<Id,User> oldMap, Boolean isInsert, Boolean isUpdate){
    	Map<Id,String> usersToCreateGroup = new Map<Id,String>();
        for(User usr : users){
        	
            if(usr.profileId == analystFullProfileId && usr.IsActive 
            	&& (isInsert 
            		|| (isUpdate 
		            	&& (oldMap.get(usr.Id).profileId != analystFullProfileId
	            			|| oldMap.get(usr.Id).IsActive != usr.IsActive)))){
    			
                usersToCreateGroup.put(usr.Id, prepareName(usr));
            }
        }
        if(!usersToCreateGroup.isEmpty()){
            createGroups(usersToCreateGroup);
        }
    }
    private static String prepareName(User userObj){
		String userName = userObj.LastName;
		userName = (String.isNotBlank(userObj.FirstName) ? userObj.FirstName +' ' :'' ) + userName;
		return userName;
    }
    public static void createGroups(Map<Id,String> userIds){
        Set<String> existingGroupsSet = new Set<String>();
        String grpNameLike = IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL + '005%';
        for(Group grp : [Select Id, DeveloperName From Group WHERE DeveloperName like :grpNameLike Limit 2000]){
            existingGroupsSet.add(grp.DeveloperName);
        }
        List<Group> groupsToInsert = new List<Group>();
        
        for(Id key : userIds.keyset()){
            if(!existingGroupsSet.contains(IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL +key)){
                String groupName = IVPSDLVMaintainVisibilityService.IVP_SDLV_LABEL_INITIAL+userIds.get(key);
                if(groupName.length() > 40){
	            	groupName = groupName.substring(0, 37)+'...';
	            }
            	groupsToInsert.add(new Group(Name = groupName,
                                 DeveloperName = IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL +key));
            }
        }
        
        if(!groupsToInsert.isEmpty()){
            insert groupsToInsert;
            
            Map<Id,String> usrToGroupId = new Map<Id,String>();
            List<GroupMember> groupMembers = new List<GroupMember>();
            
            for(Group grp : groupsToInsert){
                usrToGroupId.put(grp.DeveloperName.substringAfter(IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL) ,grp.Id);
            }
            for(Id key : userIds.keyset()){
                if(usrToGroupId.containsKey(key)){
                	groupMembers.add(new GroupMember(GroupId = usrToGroupId.get(key), UserOrGroupId = key));
                }
            }
            
            if(!groupMembers.isEmpty()){
                insert groupMembers;
            }
        }
    }
}