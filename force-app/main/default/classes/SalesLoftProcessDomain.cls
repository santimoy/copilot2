/**
 * @author  : 10k-Advisor
 * @name    : SalesLoftProcessDomain 
 * @date    : 2018-08-10
 * @description   : The class user as an interface that will implement by each Service to process related Salesloft object.
**/
public interface SalesLoftProcessDomain {
    // to process HTTPResponse with respected object 
    void processHTTPResponse(HttpResponse response);
}