/**
 * @author  : 10k-Advisor
 * @name    : SalesloftPeopleSyncService 
 * @date    : 2018-08-10
 * @description   : The class used as service to handle the process related to Salesloft people object
**/
public class SalesloftPeopleSyncService implements SalesLoftProcessDomain{
    
    public void processHTTPResponse(HttpResponse response){
        if (response == NULL){
            return;
        }
        Salesloft.PeopleResponse peopleData = (Salesloft.PeopleResponse)JSON.deserialize(
                                                    response.getBody(), 
                                                    Salesloft.PeopleResponse.Class);
        if(peopleData!=NULL){
            Integer next_page;
            if(peopleData.metadata!=NULL){
                if(peopleData.metadata.paging!=NULL){
                    next_page = peopleData.metadata.paging.next_page = peopleData.metadata.paging.next_page;
                }
            }
            if(peopleData.data!=NULL){
                processData(peopleData.data);
            }
            if(next_page!=null){
                System.enqueueJob(new SalesLoftSyncQ('people', Salesloft.PEOPLE_END_POINT, next_page));
            }else{
                SalesloftUtility.removeSetting(new Set<String>{'salesloft-job-running'});
                if(!Test.isRunningTest()){
                    //initiating Cadence Syncing.
                    System.enqueueJob(new SalesLoftSyncQ('cadence', Salesloft.CADENCE_END_POINT, 1, SalesLoftSyncQ.startSyncDateTime));
                }
            }
        }
    }
    
    public static Map<String, String> ProcessData(List<Salesloft.People> lstPeople){
        Map<String, String> emailWithPeopleId = new Map<String, String>();
        for(Salesloft.People pple : lstPeople){
            String email = pple.email_address;
            if(String.isBlank(email)){
                System.debug(pple);
                continue;
            }
            email = email.toLowerCase();
            if(!emailWithPeopleId.containsKey(email)){
                emailWithPeopleId.put(email, pple.id);
            }else{
                System.debug(pple);
            }
        }
        
        List<Contact> contactToUpdates = new List<Contact>();
        Map<String, String> sfConIdWithId= new Map<String, String>();
        for(List<Contact> contacts : [SELECT Id, Email FROM Contact WHERE Email in : emailWithPeopleId.keySet() AND Salesloft_Id__c = NULL]){
            for(Contact con : contacts){
                String email = con.Email.toLowerCase();
                String sfId = emailWithPeopleId.get(email);
                contactToUpdates.add(new Contact(Id = con.Id, Salesloft_Id__c = sfId));
                sfConIdWithId.put(sfId, con.Id);
            }
        }
        try{
            if(!contactToUpdates.isEmpty()){
                update contactToUpdates;
            }
        }catch(Exception ex){
            SalesloftUtility.notifyToUser('Salesloft', ex);
        }
        return sfConIdWithId;
    }
}