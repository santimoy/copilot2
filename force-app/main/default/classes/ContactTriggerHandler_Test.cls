/**
 * Test class coverage for ContactTriggerHandler
 *
 *@author V2Force
 *@created 2019-10-01
 *
 */
 @isTest
 public with sharing class ContactTriggerHandler_Test {

    @isTest
    public static void testUpdateKeyContact(){

		Account objAccount = new Account(Name='Test Account');
		objAccount.Website__c = 'http://saasbydesign.com';
		objAccount.OwnerId = UserInfo.getUserId();
		insert objAccount;

		list<Contact> lstContacts = new list<Contact>();
        for(Integer i = 1; i <= 5; i++) {

            lstContacts.add(new Contact(Key_Contact__c = false,
                                        FirstName = 'Troy ' + i,
                                        LastName = 'Milton Wing',
                                        Email = 'thomas.lesnick@insightpartners.com',
                                        AccountId = objAccount.id));
        }
		insert lstContacts;

		// reset the recursive trigger flag
		checkRecursive.resetAll();

		Test.startTest();
		lstContacts[1].Key_Contact__c = true;	// only 2nd contact is the key contact
		update lstContacts[1];
		Test.stopTest();

		List<Contact> lstKeyContacts = [SELECT Id, FirstName, LastName
                                               FROM Contact
                                               WHERE AccountId =: objAccount.Id
                                               AND Key_Contact__c = true];
		system.assertEquals(1, lstKeyContacts.size(), 'Total key contacts rows should return exactly 1 row.');
		system.assertEquals(lstContacts[1].Id, lstKeyContacts[0].Id, 'Incorrect key contact record returned.');

        delete lstKeyContacts;
        undelete lstKeyContacts;
	}

    @isTest
    public static void testUpdateContactSumOnAccount_Insert(){

        Id CONTACT_RECORDTYPE_ID = [ SELECT Id FROM RecordType
                                               WHERE SObjectType = 'Contact'
                                               AND DeveloperName = 'IR_Contact' ].Id;
		Account objAccount = new Account(Name='Test Account');
		objAccount.Website__c = 'http://saasbydesign.com';
		objAccount.OwnerId = UserInfo.getUserId();
		insert objAccount;
		Test.startTest();
		list<Contact> lstContacts = new list<Contact>();
        for(Integer i = 1; i <= 5; i++) {

            lstContacts.add(new Contact(Key_Contact__c = false,
                                        FirstName = 'Troy ' + i,
                                        LastName = 'Milton Wing',
                                        Email = 'thomas.lesnick@insightpartners.com',
                                        AccountId = objAccount.id,
                                        RecordTypeId = CONTACT_RECORDTYPE_ID));
        }
		insert lstContacts;
        Test.stopTest();
        system.assertEquals(5, [SELECT Id, Number_of_Contacts__c FROM Account][0].Number_of_Contacts__c);
	}

    @isTest
    public static void testUpdateContactSumOnAccount_Delete(){

        Id CONTACT_RECORDTYPE_ID = [ SELECT Id FROM RecordType
                                               WHERE SObjectType = 'Contact'
                                               AND DeveloperName = 'IR_Contact' ].Id;
		Account objAccount = new Account(Name='Test Account');
		objAccount.Website__c = 'http://saasbydesign.com';
		objAccount.OwnerId = UserInfo.getUserId();
		insert objAccount;
		Test.startTest();
		list<Contact> lstContacts = new list<Contact>();
        for(Integer i = 1; i <= 5; i++) {

            lstContacts.add(new Contact(Key_Contact__c = false,
                                        FirstName = 'Troy ' + i,
                                        LastName = 'Milton Wing',
                                        Email = 'thomas.lesnick@insightpartners.com',
                                        AccountId = objAccount.id,
                                        RecordTypeId = CONTACT_RECORDTYPE_ID));
        }
		insert lstContacts;
		delete lstContacts[1];
        Test.stopTest();
        system.assertEquals(4, [SELECT Id, Number_of_Contacts__c FROM Account][0].Number_of_Contacts__c);
	}

    @isTest
    public static void testUpdateContactSumOnAccount_Undelete(){

        Id CONTACT_RECORDTYPE_ID = [ SELECT Id FROM RecordType
                                               WHERE SObjectType = 'Contact'
                                               AND DeveloperName = 'IR_Contact' ].Id;
		Account objAccount = new Account(Name='Test Account');
		objAccount.Website__c = 'http://saasbydesign.com';
		objAccount.OwnerId = UserInfo.getUserId();
		insert objAccount;
		Test.startTest();
		list<Contact> lstContacts = new list<Contact>();
        for(Integer i = 1; i <= 5; i++) {

            lstContacts.add(new Contact(Key_Contact__c = false,
                                        FirstName = 'Troy ' + i,
                                        LastName = 'Milton Wing',
                                        Email = 'thomas.lesnick@insightpartners.com',
                                        AccountId = objAccount.id,
                                        RecordTypeId = CONTACT_RECORDTYPE_ID));
        }
		insert lstContacts;
        Contact objCon = lstContacts[1];
		delete lstContacts[1];
        undelete objCon;
        Test.stopTest();
        system.assertEquals(5, [SELECT Id, Number_of_Contacts__c FROM Account][0].Number_of_Contacts__c);
	}
}