@isTest
private class JustForYouCompaniesController_test {

    @TestSetup static void testSetup(){
        Account acc = new Account();
        acc.Name  = 'Test Company';
        acc.website = 'www.google.com';
        insert acc;
        
        Test.startTest();

        Sourcing_Preference__c spcobj = new Sourcing_Preference__c();
        spcobj.Validation_Status__c = 'valid'; 
        spcobj.Status__c = 'Active';
        spcobj.Name__c = 'Test Sourcing';
        spcobj.External_Id__c = 'ABC1234568';
        spcobj.Include_Exclude__c = 'Include';
        spcobj.Last_Validated__c = Date.newInstance(2019, 1, 25);
        spcobj.Recent_Validation_Error__c = 'Status Unavailable';
        insert spcobj;

        List<Sourcing_Preference__c> spList = new List<Sourcing_Preference__c>();
        Sourcing_Preference__c sp = new Sourcing_Preference__c();
        sp.Validation_Status__c = 'valid';
        sp.Status__c = 'Active';
        sp.Name__c = 'Test Sourcing';
        sp.Cloned_From__c = spcobj.Id;
        //sp.Owner_Username__c = Userinfo.getName();
        sp.External_Id__c = 'ABC12345';
        sp.Include_Exclude__c = 'Exclude';
        sp.JSON_QueryBuilder__c = '{"condition":"AND","rules":[{"id":"LOWER(ct.companies_cross_data_full.ivp_description)","field":"LOWER(ct.companies_cross_data_full.ivp_description)","type":"string","input":"text","operator":"equal","value":"4444"}],"valid":true}';
        sp.Last_Validated__c = Date.newInstance(2019, 1, 20);
        sp.Recent_Validation_Error__c = 'Status Unavailable';
        spList.add(sp);
        insert spList;

        List<Sourcing_Preference__c> updateList = new List<Sourcing_Preference__c>();
        String getCurrentUserDbCol = 'Testing Purpose';

        for (Sourcing_Preference__c a : spList) {

            Sourcing_Preference__c sourcingPrefRec = a.clone(true, true, true, true);
            sourcingPrefRec.Cloned_From__c = spcobj.Id;
            sourcingPrefRec.External_Id__c = a.External_Id__c;
            sourcingPrefRec.Include_Exclude__c = a.Include_Exclude__c;
            sourcingPrefRec.JSON_QueryBuilder__c = a.JSON_QueryBuilder__c;
            sourcingPrefRec.Last_Validated__c = a.Last_Validated__c;
            sourcingPrefRec.Name__c = a.Name__c;
            sourcingPrefRec.Recent_Validation_Error__c = a.Recent_Validation_Error__c;
            sourcingPrefRec.Status__c = a.Status__c;
            sourcingPrefRec.SQL_Where_Clause__c = a.SQL_Where_Clause__c;
            sourcingPrefRec.Validation_Status__c = a.Validation_Status__c;
            sourcingPrefRec.SQL_Query__c = a.SQL_Query__c;
            sourcingPrefRec.Serialized__c = '';
            string recUrl = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/'+sourcingPrefRec.Id+' ';
            String newSubStringOfDbCol = '';
            String newSqlQuery = sourcingPrefRec.SQL_Query__c;
            updateList.add(sourcingPrefRec);
        }

        if(updateList.size() > 0) {
            update updateList;
        }
        
        List<Just_For_You_Setting__c> jfysList = new List<Just_For_You_Setting__c>();
        Just_For_You_Setting__c jfys = new Just_For_You_Setting__c();
        jfys.Name = 'Test CS2';
        jfys.JFY_Companies_Last_N_Days__c = 2;
        jfysList.add(jfys);
        insert jfysList;

        List<IVP_Call_Params__c> ivpcpList = new List<IVP_Call_Params__c>();
        IVP_Call_Params__c ivpcp = new IVP_Call_Params__c();
        ivpcp.Name = 'justforyou';
        ivpcp.Just_For_You_OrderBy__c = 'Name';
        ivpcpList.add(ivpcp);
        insert ivpcpList;
        List<Intelligence_Field__c> iiFields = new List<Intelligence_Field__c>();
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='all_matched_preferences',
            UI_Label__c='All Matched Preferences', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=false,
            Is_All_Preferences_Static_Field__c=true));
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='most_recent_matched_preference',
            UI_Label__c='Most Recent Matched Preference', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=false,
            Is_All_Preferences_Static_Field__c=true));
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='most_recent_appearance_reason',
            UI_Label__c='Most Recent Matched Reason', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=true,
            Is_All_Preferences_Static_Field__c=true));
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='most_recent_appearance_date',
            UI_Label__c='Most Recent Matched Date', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=true,
            Is_All_Preferences_Static_Field__c=true));
        insert iiFields;
        Test.stopTest();

    }
    @isTest public static void test1(){
        String companyId = '';String externalId = '';
        List<JustForYouCompaniesController.companyClass> companyList = new List<JustForYouCompaniesController.companyClass>();
        List<Sourcing_Preference__c> spList = [Select Id,External_Id__c,Name__c From Sourcing_Preference__c Where Name__c='Test Sourcing'];
        system.assertEquals(spList[0].Name__c,'Test Sourcing');
        List<Account> accList = [Select Id,Name From Account Where Name='Test Company' limit 1];
        
        if(accList.size()>0){
            companyId = accList[0].Id;
            system.assertEquals(accList[0].Name,'Test Company');
        }
        
        
        if(spList.size()>0){
            externalId = spList[0].External_Id__c;
        }
        
        JustForYouCompaniesController.companyClass ccObj = new JustForYouCompaniesController.companyClass();
        ccObj.title = 'Name';
        ccObj.fieldName = 'Name';
        ccObj.data = 'Test';
        companyList.add(ccObj);
        
        JustForYouCompaniesController.getSourcePreference();
        PreferencePreviewController.getDisplayColumn(spList[0].Id, true);
        JustForYouCompaniesController.getColumnList();
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        JustForYouCompaniesController.createCompany(companyList, 'test','test.com');
        JustForYouCompaniesController.createCompany(companyList, 'test','test.com');
        JustForYouCompaniesController.processAssgnUser(companyId);
        test.stopTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        JustForYouCompaniesController.callCompanyInfo(externalId,'Test Sourcing','www.google.com');
        JustForYouCompaniesController.getSourcingPreferenceWithCount(true, null, null);
    }
}