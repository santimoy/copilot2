/**
 * @author MukeshKS-Concretio
 * @date 2018-05-04
 * @className IVPUserService
 * @group IVP
 *
 * @description contains methods related with user object
 */
public class IVPUserService {
	
    /**
	 * @description this is used for to get list of public groups
	 * @param List<String> groupNames contains Developer name of public groups
	 * @return List<Group> groups
	 * @author Mukesh-concretio
	 */
    public static List<Group> getUserGroups(List<String> groupNames) {
        List<Group> groups = new List<Group>();
        
        String groupQuery = 'SELECT Id FROM Group WHERE DeveloperName IN :groupNames';
        
        for( Group groupObj: ( List<Group> ) Database.query(groupQuery) ) {
            groups.add(groupObj);
        }
        return groups;
    }
    
    /**
	 * @description this is used for to get list members (users) ids of public groups
	 * @param List<Group> groups contains list of groups
	 * @return Set<Id> groupMemberIds contains IDs of group members (users)
	 * @author Mukesh-concretio
	 */
    public static Set<Id> getGroupMembersIDs(List<Group> groups) {
        Set<Id> groupMembersIDs = new Set<Id>();
        
        Set<Id> groupIDs = ( new Map<Id, Group>(groups) ).keySet(); 
        
        String groupMemberQuery = 'SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN :groupIDs';
        
        for(GroupMember member: ( List<GroupMember> ) Database.query(groupMemberQuery)){
           groupMembersIDs.add(member.UserOrGroupId);
        }
        
        return groupMembersIDs;
    }
    
    /**
	 * @description this is used for to get list users by groups
	 * @param List<String> groups contains Developer name of public groups
	 * @return List<User> users of passed groupNames
	 * @author Mukesh-concretio
	 */
    public static List<User> getUsersByGroup(List<String> groupNames) {
        List<User> users = new List<User>();
        
        Set<Id> groupUserIds = getGroupMembersIDs(getUserGroups(groupNames));
        
        String userQuery = 'SELECT Id FROM User WHERE IsActive = True AND Id IN :groupUserIds';
        
        for( User user: ( List<User> ) Database.query(userQuery) ) {
            users.add(user);
        }
		return users;
    }

    /**
     * to check current user is system administrator or not
     * @param void
     * @return Boolean returns true if current user is system administrator 
     * @author Mukesh-ConcretIO
     */
    public static Boolean isCurrentUserSystemAdmin(){
        Id profileId = Userinfo.getProfileId();
        
        Integer profileCount = [SELECT COUNT() FROM Profile WHERE Id = :profileId AND Name = 'System Administrator'];
        return (  profileCount > 0 ) ? True : False ;
    }
}