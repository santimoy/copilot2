/** 
* \arg ClassName    : HttpUtils
* \arg JIRATicket   : 
* \arg CreatedOn    : 14/Aug/2019
* \arg LastModifiedOn : 
* \arg CreatededBy : Anup 
* \arg ModifiedBy : 
* \arg Description : generic http Utility class
*/
public with sharing class HttpUtils {
    private static String lOG_LEVEL{get{
        if(lOG_LEVEL == null){
            Integration_API_Settings__c userSetting = Integration_API_Settings__c.getOrgDefaults();
            if(userSetting.Event_Log_Level__c != null){
                lOG_LEVEL = userSetting.Event_Log_Level__c.toLowerCase();
            }else{
                lOG_LEVEL = 'error';
            }
        }
        return lOG_LEVEL;
    }set;}
    private static HttpRequest request;
    

    public static HttpResponse doCallout(HttpRequest reqData){
        HttpResponse res;
        try{

            request = reqData;
            http req = new Http();
            res = req.send(reqData);
            if(lOG_LEVEL == 'info' || lOG_LEVEL == 'debug'){
                EventLog.generateLog(reqData, res, 'HttpUtils', 'doCallout', lOG_LEVEL);
            }
            
        }catch(CalloutException ex){
            EventLog.generateLog(reqData, 'HttpUtils', 'doCallout', ex);
        }
        return res;
    }
    /**
    apiLabel : labe for Integration_API__mdt. used in soql query
    requestLabel : label for Integration_API_Request__mdt. used in soql query
    isCredRequired : used to call oAuth method with user name and password
     */
    public static void handleOAuth(String apiLabel, String requestLabel,Boolean isCredRequired){
        System.debug('inside repert part');
       HttpRequest req = dooAuth(apiLabel, requestLabel, isCredRequired);
       HttpResponse res = doCallout(req);
       if(res.getStatusCode() == 200){
           getAccessToken(res);
       }else if( !isCredRequired){
           EventLog.generateLog(req, res, 'HttpUtils', 'doCallout', 'INFO');
            handleOAuth(apiLabel, requestLabel, !isCredRequired);
       }else{
           EventLog.generateLog(req, res, 'HttpUtils', 'doCallout', 'INFO');
       }
    }
     /**
    apiLabel : label for Integration_API__mdt. used in soql query
    requestLabel : label for Integration_API_Request__mdt. used in soql query
    isCredRequired : used to call oAuth method with user name and password
     */
    private static HttpRequest dooAuth(String apiLabel, String requestLabel, Boolean isCredRequired){
        HttpRequest request = new HttpRequest();
        try{
            Integration_API__mdt ApiBaseURL = [SELECT Id, Label, QualifiedApiName, Base_URL__c, Description__c, Authentication_Method__c, Client_Id__c, Client_Secret__c
                                    FROM Integration_API__mdt WHERE Label =: apiLabel Limit 1 ];

            Integration_API_Request__mdt req = [SELECT Integration_API__r.Base_URL__c, Path__c, Verb__c 
                                                FROM Integration_API_Request__mdt WHERE Label = :requestLabel limit 1];
            request.setEndpoint(req.Integration_API__r.Base_URL__c + req.Path__c);
            request.setMethod(req.Verb__c);
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
            // request.setTimeout(30000);
            Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();            
            if(accToken.Request_timeOut__c != null){
                request.setTimeout(Integer.valueOf(accToken.Request_timeOut__c));  
            }else{
                request.setTimeout(30000);
            }

            Map<String, String> bodyValueByKey = new Map<String, String>();
            
            bodyValueByKey.put('client_id',ApiBaseURL.Client_Id__c);
            bodyValueByKey.put('client_secret',ApiBaseURL.Client_Secret__c);
            bodyValueByKey.put('scope','admin');
            // Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
            if(ApiBaseURL.Authentication_Method__c == 'OAuth 2.0'){
                
                if(accToken.Refresh_Token__c != null && !isCredRequired){

                    bodyValueByKey.put('grant_type','refresh_token');
                    bodyValueByKey.put('refresh_token', accToken.Refresh_Token__c);
                }else{
                    bodyValueByKey.put('grant_type','password');

                    bodyValueByKey.put('password',accToken.Integration_Password__c);
                    bodyValueByKey.put('admin_email',accToken.Integration_Username__c);

                }
            
            }else if(ApiBaseURL.Authentication_Method__c ==  'Username / Password'){
                // ! How we will handle UserName and password
                /*Blob headerValue = Blob.valueOf(username + ':' + password); 
                String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                req.setHeader('Authorization', authorizationHeader); */
            /*} */
            
            //return null;
            }
            String bodyText ='';
            for(String key : bodyValueByKey.keySet()){
                bodyText+= key + '=' + bodyValueByKey.get(key)+ '&';
            }
            bodyText = bodyText.removeEnd('&');
            if(String.isNotBlank(bodyText)){
                request.setBody(bodyText);
            }
        } catch(Exception e) {
            EventLog.generateLog(request, 'HttpUtils', 'dooAuth', e);                                 
        }
        return request;
    }
    @TestVisible
    private static void getAccessToken(HttpResponse response){
        try{
            Map<String, Object> body = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
            accToken.Access_Token__c =  String.valueOf(body.get('access_token'));
            accToken.Refresh_Token__c = String.valueOf(body.get('refresh_token'));
             // accToken.Token_Expiration__c = System.now().addSeconds(Integer.valueOf(body.get('expires_in'))/1000));
            accToken.Token_Expiration__c = System.now().addSeconds(Integer.valueOf(body.get('expires_in')));

            UPSERT accToken;
        } catch(Exception e) {
            EventLog.generateLog(request, response, 'HttpUtils', 'dooAuth', e);                                 
        }
    }

    public static HttpRequest getCallOutData(Integration_API__mdt ApiBaseURL, Integration_API_Request__mdt reqData ){
        HttpRequest request = new HttpRequest();
        request.setEndpoint(ApiBaseURL.Base_URL__c + reqData.Path__c);
        request.setMethod(reqData.Verb__c);        
        // request.setTimeout(30000);
        //String body = '';
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        if(accToken.Access_Token__c != null && accToken.Token_Expiration__c > System.now() ){
            request.setHeader('Authorization', 'Bearer ' + accToken.Access_Token__c);
        }
        if(reqData.Verb__c == 'POST' ||  reqData.Verb__c == 'PUT' ){
            request.setHeader('Content-Type','application/json');
        }
        if(accToken.Request_timeOut__c != null){
            request.setTimeout(Integer.valueOf(accToken.Request_timeOut__c));  
        }else{
             request.setTimeout(30000);
        }
        return request;
    }
}
// request.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
// handle 
/**   
 */