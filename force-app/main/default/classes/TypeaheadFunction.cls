public class TypeaheadFunction
{

  public class recordData{
    @testVisible
    Boolean isTalentUser;
    @testVisible
    String recordId;
  }

  public static TypeAheadRes[] srch(String rName, String sObjName, String filterStr,String filterField, String recordData)
  {

    recordData objCmpnyRecordData = (recordData) JSON.deserialize(recordData, recordData.class);

    System.debug('objCmpnyRecordData: '+objCmpnyRecordData);

    Set<Id> cmpnyIdSet =  new Set<Id>();

    if(objCmpnyRecordData != null && objCmpnyRecordData.isTalentUser && sObjName != 'User'){
      try{   
          for(PartnerCompanyIds__mdt partnerId : [SELECT companyId__c
                                            FROM PartnerCompanyIds__mdt]){
                
                System.debug('companyId__c OUT  Ifff: '+partnerId.companyId__c);
                String partnerIdString = String.valueof(partnerId.companyId__c);
                if(partnerIdString != null && partnerIdString.startsWith('001') && partnerIdString.length() <= 18){
                    System.debug('companyId__c in  Ifff: '+partnerId.companyId__c);
                    cmpnyIdSet.add(partnerId.companyId__c);
                }
          }
          if(objCmpnyRecordData.recordId != null){
            String currentRecordId = (objCmpnyRecordData.recordId).startsWith('00T') ? getParentId(objCmpnyRecordData.recordId) : objCmpnyRecordData.recordId;
            System.debug('recordId currentRecordId: '+currentRecordId);
            cmpnyIdSet.add(currentRecordId);
          }
          System.debug('cmpnyIdSet: '+cmpnyIdSet);
          //lstOfFields.add(new WrapFieldsDetail('Checkbox', 'Checkbox', '', true));
      }
      catch(Exception e)
      {
          throw new AuraHandledException(e.getMessage());
      }
    }
    
    if(sObjName=='User')
    {
      filterStr = 'IsActive = true';
    }
    TypeAheadRes[] resList = new TypeAheadRes[]{};
    if(sObjName=='Account')
    {
       String sanitizedSearchString = String.escapeSingleQuotes(rName);
      String sanitizedSObjectAPIName = String.escapeSingleQuotes(sObjName);
   
      String searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN NAME FIELDS RETURNING ' + sanitizedSObjectAPIName + '(Id,Name) Limit 15'; 

      List<List<SObject>> searchList = search.query(searchQuery);

      for (SObject so : searchList[0]) {
          resList.add(new TypeAheadRes(so.Id, (String)so.get('Name')));
          //results.add(new Result((String)so.get('Name'), so.Id));
      }
    }
    else
    {
      rName = rName.replaceAll('\'', '\\\\\'');
      System.debug('filterStr: '+filterStr);

      String query = 'Select Id,Name From '+
                                          sObjName +
                                          ' WHERE Name Like \'%' + rName + '%\''+
                                          ((filterStr!=null&&filterStr!='')?(' AND '+ filterStr):'')+
                                          ((objCmpnyRecordData.isTalentUser && !cmpnyIdSet.isEmpty()) ? ' AND AccountId IN: cmpnyIdSet ' : '' )+
                                          // ((talentFilter!=null&&talentFilter!='')?(' AND '+ talentFilter):'')+
                                          ' LIMIT 5';

      System.debug('Final QUERY String: '+query);

      sObject[] tempList = Database.query(query);

      for(sObject s : tempList)
      {
        String realName = (String)s.get('Name');

        resList.add(new TypeAheadRes((Id)s.get('Id'), realName));
      }
    }
   

    return(resList);
  }

  public Static String getParentId(String currentTaskId){

    return [SELECT WhatId FROM Task WHERE Id =:  currentTaskId].WhatId;
  }
}