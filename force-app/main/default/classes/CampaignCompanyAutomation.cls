/*
 * 
 *@name      : CampaignCompanyAutomation
 *@developer : 10K developer
 *@date      : 2017-09-26
 *@description   : Base class for the functionality of automatically creating Campaign, CampaignMember whenever a CampaignCompany is created.
					It will also provid methods to maintaining the consistancy for the same after CampaignCompany is created! Like modifying Campaign/Member of parent should reflect in child records!
 *
*/
public class CampaignCompanyAutomation {
    
    public CampaignCompanyAutomation(){
        
    }
    //public static boolean AllowChildCampaignAndMemberUpdate = false;
    
	/*
	 * This method is for CampaignMemberHandler
	 *  	Whenever a new CampaignMember is insert same CampaignMember should be insert for all child campaign's
	*/
    
    /*public static void insertChildCampaignMembersFromParentCampaignMembers(List<CampaignMember> parentCampaignMembers){
        try{
            List<CampaignMember> campMemberListToInsert = new List<CampaignMember>();
            Map<Id,List<CampaignMember>> campIdWithMemberMap = new Map<Id,List<CampaignMember>>();
            for(CampaignMember campMember : parentCampaignMembers){
                if(campIdWithMemberMap.containsKey(campMember.CampaignId)){
                    campIdWithMemberMap.get(campMember.CampaignId).add(campMember);
                }else{
                    campIdWithMemberMap.put(campMember.CampaignId,new List<CampaignMember>{campMember});
                }
            }
            Map<ID,set<ID>> parentIdChildIdsList = new Map<ID,set<ID>>();
            for(Campaign camp : [SELECT parentId,Id from Campaign where parentId in :campIdWithMemberMap.keySet()]){
                if(parentIdChildIdsList.containsKey(camp.ParentId)){
                    parentIdChildIdsList.get(camp.ParentId).add(camp.Id);
                }else{
                    parentIdChildIdsList.put(camp.ParentId,new set<Id>{camp.Id});
                }    
            }
            // We are prepareing this map because in trigger we can't access parent object fields 
			//	so we will be use this map to create CampaignMemberStatus
			
            Map<Id,Id> childCampaignWithParent = new Map<Id,Id>();
            for(Id campaignId : parentIdChildIdsList.keySet()){
                for(Id campId : parentIdChildIdsList.get(campaignId)){
                    childCampaignWithParent.put(campId, campaignId);
                    for(CampaignMember campMember : campIdWithMemberMap.get(campaignId)){
                        CampaignMember campMemToInsert = campMember.clone();
                        campMemToInsert.CampaignId = campId;
                        campMemberListToInsert.add(campMemToInsert);
                    }
                }
            }
            
            if(!campMemberListToInsert.isEmpty()){
                // Creating CampaignMemberStatus list to insert for child campaign's
                Set<Id> campaignIds = new Set<Id>();
                campaignIds.addAll(childCampaignWithParent.keySet());
                campaignIds.addAll(childCampaignWithParent.values());
                Map<Id, Map<String, CampaignMemberStatus>> campaignIdWithMembersStatus = getCampaignIdWithMemberStatusMap(campMemberListToInsert,campaignIds);
                
                List<CampaignMemberStatus> campMembersStatus = new List<CampaignMemberStatus>();
                for(CampaignMember campMember : campMemberListToInsert){
                    //Checking that CampaignMemberStatus should not be already exists
                    if(campMember.Status != null && campaignIdWithMembersStatus.get(campMember.CampaignId) != null && 
                       !campaignIdWithMembersStatus.get(campMember.CampaignId).containsKey(campMember.Status)){
                           
                           CampaignMemberStatus statusRec =  campaignIdWithMembersStatus.get(childCampaignWithParent.get(campMember.CampaignId)).get(campMember.Status);
                           if(statusRec != null)
                               campMembersStatus.add(new CampaignMemberStatus(CampaignId = campMember.CampaignId, 
                                                                              Label = statusRec.Label,
                                                                              IsDefault = statusRec.IsDefault,
                                                                              HasResponded = statusRec.HasResponded));
                       }       
                           
                } 
                if(!campMembersStatus.isEmpty())
                    insert campMembersStatus;
                AllowChildCampaignAndMemberUpdate = true;
                insert campMemberListToInsert;
                AllowChildCampaignAndMemberUpdate = false;
            }
          
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    /*
     * This method is for CampaignMemberHandler
     * Update Child Campaign's members whenever any Member of parent Campaign is changed! 
	*/
    /*public static void updateChildCampaignMembersFromParentCampaignMembers(List<CampaignMember> parentCampaignMembers){
        try{
            Map<Id,Map<Id,sObject>> campIdWithContactIdandMemberMap = new Map<Id,Map<Id,sObject>>();
            Set<Id> contactIds = new Set<Id>();
            Set<Id> leadIds    = new Set<Id>();
            for(CampaignMember campMember : parentCampaignMembers){
                if(!campIdWithContactIdandMemberMap.containsKey(campMember.CampaignId)){
                    campIdWithContactIdandMemberMap.put(campMember.CampaignId, new Map<Id,CampaignMember>());
                }
                if(campMember.ContactId != NULL){
                    campIdWithContactIdandMemberMap.get(campMember.CampaignId).put(campMember.ContactId,campMember);
                    contactIds.add(campMember.ContactId);
                }else if(campMember.leadId != NULL){
                    campIdWithContactIdandMemberMap.get(campMember.CampaignId).put(campMember.LeadId,campMember);
                    leadIds.add(campMember.leadId);
                }
            }
         
            if(!campIdWithContactIdandMemberMap.isEmpty()){
				List<CampaignMember> membersToUpdate = [SELECT Id,Status,CampaignId,campaign.parentId,ContactId,LeadId 
                                                          	FROM CampaignMember 
                                                          	WHERE Campaign.ParentId in :campIdWithContactIdandMemberMap.keyset() 
                                                          	AND (contactId in :contactIds
                                                           	OR leadId in :leadIds)];
                //We are sending CampaignMember List and getting campaign Id with CampaignMember 
                //	Status Map for inserting campaignMember status for child campaign 
				
                Map<Id, Map<String, CampaignMemberStatus>> campaignIdWithMembersStatus = getCampaignIdWithMemberStatusMap(membersToUpdate,new Set<Id>());
                
                List<CampaignMemberStatus> campMembersStatus = new List<CampaignMemberStatus>();
                // Getting here campaign members fields instead of below because this method will be call for every campaignMembers.
                List<String> campMemberFields = CampaignUtil.getObjectFields('CampaignMember', true).split(',');
                for(CampaignMember camMember : membersToUpdate){
                    if(camMember.getSObject(CampaignMember.CampaignId) != null && camMember.getSObject(CampaignMember.CampaignId).get('parentId') != null){
                        sObject oldMember;
                        if((Id)camMember.get('ContactId') != NULL){
                            oldMember = campIdWithContactIdandMemberMap .get((Id)camMember.getSObject(CampaignMember.CampaignId)
                                                .get('parentId')).get((Id)camMember.get('ContactId'));
                        }else{
                            oldMember = campIdWithContactIdandMemberMap .get((Id)camMember.getSObject(CampaignMember.CampaignId)
                                                .get('parentId')).get((Id)camMember.get('LeadId'));
                        }
                        
                        if(oldMember!=NULL){
                            for(String field : campMemberFields){
								camMember.put(field, oldMember.get(field));    
                            }
                            //Checking that CampaignMemberStatus should not be already exists.
                            if(oldMember.get('Status') != null 
                               && campaignIdWithMembersStatus.get(camMember.CampaignId) != null
                               && !campaignIdWithMembersStatus.get(camMember.CampaignId).containsKey((String)oldMember.get('Status'))){
                                   
                                   CampaignMemberStatus statusRec = campaignIdWithMembersStatus.get(camMember.Campaign.ParentId).get((String)oldMember.get('Status'));
                                  
                                   campMembersStatus.add(new CampaignMemberStatus(CampaignId = camMember.CampaignId, 
                                                                                  Label = statusRec.Label,
                                                                                  IsDefault = statusRec.IsDefault,
                                                                                  HasResponded = statusRec.HasResponded));
                                   campaignIdWithMembersStatus.get(camMember.CampaignId).put( statusRec.Label , statusRec);  
                                   
                               }
                        }
                    }
                }
                campMemberFields = NULL;
                AllowChildCampaignAndMemberUpdate = true;
                if(!campMembersStatus.isEmpty())
                	insert campMembersStatus;
                update membersToUpdate;
                AllowChildCampaignAndMemberUpdate = false;
           }
        }catch(Exception e){
         	System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    /* Calling - CampaignCompanyAutomation,CampaignUtil.
     * 
     * Getting CampaignMemberList and creating Map of campaignId and CampaignMemberStatus. 
     * Note :- For checking that CampaignMemberStatus should not exists already we are creating this map.
	*/
    /*public Static Map<Id, Map<String, CampaignMemberStatus>> getCampaignIdWithMemberStatusMap(List<CampaignMember> campaignMembers, Set<Id> campaignIds){
        
        Map<Id, Map<String, CampaignMemberStatus>> campaignIdWithMembersStatus = new Map<Id, Map<String, CampaignMemberStatus>>();
        if(campaignIds.isEmpty()){
            for(CampaignMember campMember : campaignMembers){
                campaignIds.add(campMember.CampaignId);
                campaignIds.add(campMember.Campaign.ParentId);
            }
        }
        
        for(CampaignMemberStatus campMemberStatus : [SELECT Id,CampaignId,Label,IsDefault,HasResponded 
                                                     	FROM CampaignMemberStatus 
                                                     	WHERE CampaignId IN :campaignIds]){
			if(campaignIdWithMembersStatus.containsKey(campMemberStatus.CampaignId)){
                campaignIdWithMembersStatus.get(campMemberStatus.CampaignId).put(campMemberStatus.Label, campMemberStatus);
            }else{
                campaignIdWithMembersStatus.put(campMemberStatus.CampaignId, new Map<String, CampaignMemberStatus>{campMemberStatus.Label => campMemberStatus});
            }
        }
        return campaignIdWithMembersStatus;
    }*/
    /*
     * This method is for CampaignMemberHandler
     * Delete Child Campaign's members whenever any Member of parent Campaign is delete! 
	*/
    /*public static void deleteChildCampaignMembersWhenParentCampaignMemberDeleted(List<CampaignMember> parentCampaignMembers){
        try{
            Set<Id> campaignIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            Set<Id> leadIds = new Set<Id>();
            
            for(CampaignMember campMember : parentCampaignMembers){
                campaignIds.add(campMember.campaignID);
                if(campMember.contactId != null)
                    contactIds.add(campMember.contactId);
                else if(campMember.leadId != null)
                    leadIds.add(campMember.leadId);
                
            }
           
            if(!campaignIds.isEmpty()){
                List<CampaignMember> cmpaignMemberList = [SELECT Id FROM CampaignMember WHERE Campaign.ParentId in :campaignIds AND (contactId in:contactIds OR leadId in:leadIds)];
                if(!cmpaignMemberList.isEmpty()){
                    AllowChildCampaignAndMemberUpdate = true;
                    delete cmpaignMemberList;
                    AllowChildCampaignAndMemberUpdate = false;
                }
            }
        }catch(Exception e){
         	System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    /*
     * This method is for CampaignHandler
     * Delete child Campaigns whenever parent campaign delete
	*/
    /*public static void deleteChildCampaignsWhenParentCampaignDeleted(List<Campaign> campList){
        try{
            Set<Id> campIdSet = new Set<ID>();
            for(Campaign campObj: campList){
                campIdSet.add(campObj.Id);
            }
            List<Campaign> campaignList = [SELECT Id FROM Campaign WHERE parentID in :campIdSet];
            
            if(!campaignList.isEmpty()){
                TriggerHandler.bypass('CampaignHandler');
                delete campaignList;
            }
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    /*
     * This method is for CampaignHandler
     * Update child Campaigns whenever parent campaign update
	*/
    /*public static void updateChildCampaignsWhenParentCampaignUpdated(List<Campaign> campaignList){
        try{ 
           	if(!campaignList.isEmpty()){
                Map<Id,Campaign> parentCampaignMap = new Map<ID,Campaign>(campaignList);
                Set<Id> setOfId = parentCampaignMap.keySet();
                List<Campaign> campListToUpdate =  Database.query('SELECT Portfolio_Company__r.Name, '+ CampaignUtil.getObjectFields('Campaign', false) + ' FROM Campaign WHERE '
                                                                    +'ParentId in :setOfId');
                for(Campaign childCamp :campListToUpdate){
                    
                    for(String field : CampaignUtil.getObjectFields('Campaign', true).split(',')){
                        if(!(field.equalsIgnoreCase('ParentId') || field.equalsIgnoreCase('RecordTypeId'))){
                            childCamp.put(field,parentCampaignMap.get((Id)childCamp.get('ParentId')).get(field));
                        }
                    }
					
                    childCamp.put('name', childCamp.Portfolio_Company__r.Name + ' | ' +parentCampaignMap.get((Id)childCamp.get('ParentId')).get('name'));

                }
                TriggerHandler.bypass('CampaignHandler');
                update campListToUpdate;
            }
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    /*
     * This method is for CampaignCompanyHandler 
     * Whenever new company inserts child campaign and campaignmbers should be insert
	*/
    /*public static void insertChildCampaignsAndMembersFromParentCampaignWhenNewCompanyInserted(List<Campaign_Company__c> campCompanyList){
        try{
            set<ID> parentCampaignIds = new set<ID>();
            set<ID> companyIds = new set<ID>();
            
            for(Campaign_Company__c campCompanyObj : campCompanyList){
                parentCampaignIds.add(campCompanyObj.campaign__c);
                companyIds.add(campCompanyObj.Company__c);
            }
			
            List<Campaign> campListToInsert = new List<Campaign>();
            Map<Id,Account> companyMap = new Map<Id,Account>([SELECT Id, Name from Account where id in :companyIds]);
			Map<Id, Campaign> parentCampaigns = new Map<Id, Campaign> ((List<Campaign>)CampaignUtil.getAllRecords(' WHERE ID in :setOfId', 'Campaign', parentCampaignIds,false));
            for(Campaign_Company__c campCompanyObj : ((Map<Id,Campaign_Company__c>)Trigger.newMap).values()){
                if(!parentCampaigns.containsKey(campCompanyObj.campaign__c))
                    continue;
                Campaign parentCampaign 			= parentCampaigns.get(campCompanyObj.campaign__c);
                Campaign campClonedObj 				= parentCampaign.clone();
                campClonedObj.Portfolio_Company__c 	= campCompanyObj.Company__c;
                campClonedObj.Name 					= companyMap.get(campCompanyObj.Company__c).Name + ' | ' + campClonedObj.Name;
                campClonedObj.ParentId 				= parentCampaign.Id;
                campListToInsert.add(campClonedObj);
            }
            if(!campListToInsert.isEmpty()){
                
                AllowChildCampaignAndMemberUpdate = true;
                insert campListToInsert;
				
                Map<Id,Set<Id>> parentIDWithChildId = new Map<Id,Set<Id>>();
                for(Campaign camp : campListToInsert){
                    if(parentIDWithChildId.containsKey(camp.ParentId))
                        parentIDWithChildId.get(camp.ParentId).add(camp.Id);
                    else
                        parentIDWithChildId.put(camp.ParentId, new set<Id>{camp.Id} );
                }
                CampaignUtil.insertChildCampMember(parentIDWithChildId);
            }
            
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    /*
	  *This method is for CampaignCompanyHandler
	  * Whenever new company deletes child campaign and campaignmbers should be delete		
	*/
    /*public static void deletedChildCampaignsAndMembersFromParentCampaignWhenNewCompanyDeleted(List<Campaign_Company__c> campCompanyList){
        try{
            Set<String> campNames = new Set<String>();
            Set<Id> companyIds = new Set<Id>();
            Set<Id> campaignIds = new Set<Id>();
            
            for(Campaign_Company__c campCompany : campCompanyList){
                 companyIds.add(campCompany.Company__c);
                 campaignIds.add(campCompany.Campaign__c);
            }
            Map<Id,Account> companyMap = new Map<Id,Account>([SELECT Id, Name from Account where id in :companyIds]);
            Map<Id,Campaign> IdWithCampaign = new Map<Id,Campaign>([SELECT Id, Name from Campaign where Id  in :campaignIds]); 
            
            for(Campaign_Company__c campCompany : (List<Campaign_Company__c>)Trigger.old){
                 campNames.add(companyMap.get(campCompany.Company__c).Name+' | '+IdWithCampaign.get(campCompany.Campaign__c).Name);
            }
            
			TriggerHandler.bypass('CampaignHandler');
            CampaignUtil.deleteChildCampaign(campNames);
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
    }*/
    
    /*
     * Method will return child campaigns set from the all campaign
	*/
    /*public static List<CampaignMember> applyChildMemberCheckAndGetParentCampaignMembers(List<CampaignMember> members, String triggerEvent){
        List<CampaignMember> membersToProcess = new List<CampaignMember>();
        try{
            Set<Id> campaignIds = new Set<Id>();
            for( CampaignMember member: members){
                campaignIds.add( member.CampaignId );
            }
            
            Set<Id> parentCampaignIds = new Set<Id>();
            Map<Id,String> campIdWithRecordType = new Map<Id,String>();
            
            for(Campaign camp : [ SELECT Id, ParentId, RecordType.Name FROM Campaign WHERE Id in: campaignIds]){
                if(camp.ParentId == null){
                    parentCampaignIds.add(camp.Id);
                }else{
                	campIdWithRecordType.put(camp.Id, (camp.RecordType.Name != null ? camp.RecordType.Name : '') );
                }
            }
            for( CampaignMember member: members){
                if( !AllowChildCampaignAndMemberUpdate && !parentCampaignIds.contains(member.CampaignId) 
                   	&& (campIdWithRecordType.get(member.CampaignId) != '' 
					&& campIdWithRecordType.get(member.CampaignId).equalsIgnoreCase('Ignite Campaign'))){
                        
                    member.addError('Can\'t '+ triggerEvent +' child campaign\'s member!');
                    
                } else {
                    membersToProcess.add( member);
                }
            }
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
        
        return membersToProcess;
    }*/
    
    /*
     * Method will return child campaigns set from the all campaign
	*/
    /*public static List<Campaign> applyChildCampaignCheckAndGetParentCampaigns(List<Campaign> campaigns, String triggerEvent){
        List<Campaign> campaignToProcess = new List<Campaign>();
        try{
            Map<Id,RecordType> idWithRecordTypeMap = new Map<Id,RecordType>([SELECT Id,Name FROM RecordType WHERE sObjectType = 'Campaign']);
            
            Map<Id,Campaign> oldMapOfCampaign= new Map<Id,Campaign>();
            if(triggerEvent.equalsIgnoreCase('modify'))
                oldMapOfCampaign = new Map<Id,Campaign>((List<Campaign>)Trigger.old);
            
            for( Campaign camp: campaigns){
                
                Id recordTypeId = (triggerEvent == 'modify' ? oldMapOfCampaign.get(camp.Id).RecordTypeId : camp.RecordTypeId);
                if( !AllowChildCampaignAndMemberUpdate && camp.ParentId != null 
                   && idWithRecordTypeMap.get(recordTypeId).Name.equalsIgnoreCase('Ignite Campaign') ){
                       
                   camp.addError('Can\'t '+ triggerEvent +' child campaign!');
                } else {
                    campaignToProcess.add( camp);
                }
            }
            
        }catch(Exception e){
            System.debug('exception ' +e.getMessage() +'\n'+ e.getStackTraceString());
        }
        
        return campaignToProcess;
    }*/
}