@isTest
public class ActivityViewerControllerTest {




@isTest static void Test_ActivityViewController(){

    Account acct = new Account(Name='testacct');
    acct.Website__c='http://saasbydesign.com';

    acct.OwnerId=UserInfo.getUserId();

    insert acct;
    Contact c1 = new Contact(Key_Contact__c=true,firstname='Troy',lastname='Milton Wing',Email='thomas.lesnick@insightpartners.com',AccountId=acct.id);
    insert c1;
    Investment_Opportunity__c oppy= new Investment_Opportunity__c(Name='inv oppy1', Company__c=acct.Id);
    insert oppy;

     String tId;
    Task t1 =new Task(Subject='Email:',Description='TO:ThomasLesnick@insightpartners.com\nSubject: Email\nBody: Information',
              ActivityDate=System.today(),whatId=acct.id,whoid=c1.id,Status='Completed');
    insert t1;
    tId=t1.id;
     t1=[Select Id,AccountId,Subject,Description,CreatedByID,CreatedDate,LastModifiedDate,LastModifiedById,ActivityDate,Type__c,Owner.FirstName,Owner.LastName,WhatId,Source__c,LastModifiedBy.LastName
     FROM Task where id=:tId];
      Task t2 =new Task(Subject='Email:',Description='TO:ThomasLesnick@insightpartners.com\nSubject: Email\nBody: Information',
              ActivityDate=System.today(),whatId=oppy.id,Status='Completed');
    insert t2;
     tId=t2.id;
      t2=[Select Id,AccountId,Subject,Description,CreatedByID,CreatedDate,LastModifiedDate,LastModifiedById,ActivityDate,Type__c,Owner.FirstName,Owner.LastName,WhatId,Source__c,LastModifiedBy.LastName
     FROM Task where id=:tId];
     
     Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
     Attachment a1= new Attachment(OwnerId=UserInfo.getUserId(),ParentId=acct.Id,Name='FileName.pdf',Body=bodyBlob);
     insert a1;
     tId=a1.Id;
     a1=[select CreatedDate,Id,OwnerId,Owner.FirstName,Owner.LastName,ParentId,Name,Body from Attachment where id=:tId];

     
    Test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController( acct );
            ActivityViewerController ctrl = new ActivityViewerController( std );
            ctrl.fillColorDict();
            ctrl.createWrapper(t2);
            Date attDate = Date.newInstance(a1.CreatedDate.year(), a1.CreatedDate.month(),a1.CreatedDate.day());
            ctrl.createAttachmentWrapper(a1,  attDate);
            ctrl.InitializeViewer();
              Task t3=new Task(Subject='Email:',Description='TO:ThomasLesnick@insightpartners.com\nSubject: Email\nBody: Information',
              ActivityDate=System.today(),whatId=acct.id,whoid=c1.id,Status='Completed');
              insert t3;
             tId=t3.id;
              t3=[Select Id,AccountId,Subject,Description,CreatedByID,CreatedDate,LastModifiedDate,LastModifiedById,ActivityDate,Type__c,Owner.FirstName,Owner.LastName,WhatId,Source__c
     FROM Task where id=:tId];
              ctrl.cleanTask (t3);
      

    List<Account> listAccounts=[SELECT Name,(Select Id,AccountId,Subject,Description,CreatedByID,CreatedDate,LastModifiedDate,LastModifiedById,ActivityDate,Type__c,Owner.FirstName,Owner.LastName,WhatId,Source__c
         FROM Tasks where IsClosed=true ORDER BY ActivityDate DESC, LastModifiedDate DESC LIMIT 499) from Account  WHERE Id=:acct.Id];
           System.assertNotEquals(listAccounts.get(0).Tasks.size(),0,'Error:no Activities');
  
    System.assertEquals(-1,t3.Subject.indexOf('Email:'),'Error: Task not cleaned');
   // System.assertEquals('Milton Wing',ctrl.KeyContact.LastName,'Error: No Key Contact');
     //   System.assertNotEquals(ctrl.getAccountActivities().size(),0,'Error:no Activities');

     ctrl.KeyContact=c1;
     ctrl.setAllEmails();


  Test.stopTest();

}

@isTest static void Test_sendErrorEmail(){
  ActivityViewerController.sendErrorEmail('Test Error', 'sendErrorEmail apex text');
}

   @isTest static void Test_updateKeyContact() {
  Account acct = new Account(Name='testacct');
    acct.OwnerId=UserInfo.getUserId();
    insert acct;
    Contact c1 = new Contact(Key_Contact__c=true,firstname='Troy',lastname='Wing',Email='thomas.lesnick@insightpartners.com',AccountId=acct.id);
    insert c1;

        
        test.startTest();
        String cId=c1.Id;
        String ret=ActivityViewerController.updateKeyContact('t@test.com',c1.Id);
          test.stopTest();
          c1=[select Email from Contact where id=:cId];
           system.assertEquals('t@test.com', c1.Email,'Error:Invalid Email');
   }


      @isTest static void Test_emailFinder2() {
      	
      	Test.setMock(HttpCalloutMock.class, new ActivityViewerControllerMockHttpResponseGenerator());
      	
        // create dummy data
       //List<EmailFinder.EmailDefinition> findEmails(String domain, String first, String last,String emailGroup)
         // in the query below, the order by should make the Production url first and the Sandbox url last

         Email_Finder_Settings__c settings=new Email_Finder_Settings__c(Name='email-checker.com',Overall_Timeout__c='10000',
                                        Request_Timeout__c='30000',
                                        API_Endpoint__c='https://api.emailverifyapi.com/api/a/v1?key={!APIKEY}&email={!EMAIL}',
                                        API_Key__c='EE9C41A1',Is_Active__c=true
      );
         insert settings;
         Email_Format_Template__c template= new Email_Format_Template__c(Name='FInit.Last@webdomain.com',Enabled__c=true);
         Email_Format_Template__c template2= new Email_Format_Template__c(Name='First.Last@webdomain.com',Enabled__c=true);
         insert template;
         insert template2;
        test.startTest();
         List<EmailFinder.EmailDefinition>  emails =ActivityViewerController.findEmails('insightpartners.com','Thomas','Lesnick','first');
          List<EmailFinder.EmailDefinition>  second =ActivityViewerController.findEmails('insightpartners.com','Thomas','Lesnick','second');
          test.stopTest();

           system.assertNotEquals(0, emails.size(),'Error:Invalid Find');
   }

   @isTest static void Test_emailFinder() {
        // create dummy data
       
         // in the query below, the order by should make the Production url first and the Sandbox url last

         Email_Finder_Settings__c settings=new Email_Finder_Settings__c(Name='email-checker.com',Overall_Timeout__c='10000',
                                        Request_Timeout__c='30000',
                                        API_Endpoint__c='https://api.emailverifyapi.com/api/a/v1?key={!APIKEY}&email={!EMAIL}',
                                        API_Key__c='EE9C41A1',Is_Active__c=true
      );
         insert settings;
         Email_Format_Template__c template= new Email_Format_Template__c(Name='FInit.Last@webdomain.com',Enabled__c=true);
         insert template;
        test.startTest();
          List<String> emails =ActivityViewerController.emailBlastList('insightpartners.com','Thomas','Lesnick');
          test.stopTest();

           system.assertEquals(2, emails.size(),'Error:Invalid Combinations');
   }

    @isTest static void Test_toggleOwner() {
        // create dummy data
        Account acct = new Account(Name='testacct');
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'twing', Email='troy.wing@saasbydesign.com',
                          EmailEncodingKey='UTF-8', LastName='Troy', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='troy.wing@saasbydesign.com.fullcopy1');
        User u2= new User(Alias = 'twing', Email='troy.wing@saasbydesign.com',
                          EmailEncodingKey='UTF-8', LastName='UNASSIGNED', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p2.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='troy.wing@saasbydesign.com.fullcopy2');
        insert u2;
        Id userId=UserInfo.getUserId();
        acct.OwnerId=userId;
        insert acct;
        // in the query below, the order by should make the Production url first and the Sandbox url last
        
        test.startTest();
        List<String> res =ActivityViewerController.toggleOwner(acct.id);
        System.assertEquals(3, res.size(),'Error:Invalid toggle result');
        system.assertEquals(res.get(2), 'unassign','Error:Invalid toggle result');
        System.runAs(u) {
            res =ActivityViewerController.toggleOwner(acct.id);
            System.assertEquals(3, res.size(),'Error:Invalid toggle result');
            System.assertEquals(res.get(2), 'cannot','Error:Invalid toggle result');
            
        }
        Test.stopTest();
        /*
        Account acct2 = new Account(Name='testacct');
        acct2.OwnerId=u2.Id;
        insert acct2;
        System.runAs(u2) {
            res =ActivityViewerController.toggleOwner(acct2.id);
            System.assertEquals(3, res.size(),'Error:Invalid toggle result');
            System.assertEquals(res.get(2), 'changed','Error:Invalid toggle result');
        }
*/
    }
   
   // implementing a mock http response generator for activity viewer
	public class ActivityViewerControllerMockHttpResponseGenerator implements HttpCalloutMock {
		public HTTPResponse respond(HTTPRequest req) {
			EmailFinder.EmailJson mjson = new EmailFinder.EmailJson();
			mjson.status = 'Ok';
			mjson.additionalStatus = '';
			mjson.Message = '';
			mjson.emailAddressChecked='dummy.user@test.com';
			mjson.emailAddressSuggestion='dummy.user@test.com';
			mjson.emailAddressProvided='dummy.user@test.com';
			String mjsonString = JSON.serialize(mjson);				
			System.debug(mjsonString);
			// Create a fake response
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setBody(mjsonString);
			res.setStatusCode(200);
			return res;
		}
	}
  
}