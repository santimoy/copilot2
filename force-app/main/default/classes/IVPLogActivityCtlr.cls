/**
 * @author Dharmendra Karamchandani
 * @date 2018-04-16
 * @className IVPLogActivityCtlr
 * @description contains methods to handle LogActivity component
 */
public with sharing class IVPLogActivityCtlr {
    /*******************************************************************************************************
     * @description used as a wrapper to hold data to initialize LogActivity component
     * 				having list of listviews of Contact and current User record
     * @className ActivityData
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    public class ActivityData{
        @AuraEnabled public User currentUser;
        @AuraEnabled public Contact[] accContacts;
        
        public ActivityData(User currentUser, Contact[] accContacts ){
            this.currentUser = currentUser;
            this.accContacts = accContacts;
        }
    }
    
    /*******************************************************************************************************
     * @description used to return Custom data Wrapper 
     * @return ActivityData
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
     
    @AuraEnabled
    public static ActivityData getInitData(String inputJson){
        Map<String, Object> inputValues = (Map<String, Object>)JSON.deserializeUntyped(inputJson);
        String accId = '';
        if(inputValues.containskey('accId')){
            accId=(String)inputValues.get('accId');
        }

        return new ActivityData([SELECT Id, Name FROM USER WHERE Id=:UserInfo.getUserId() LIMIT 1],
                            [SELECT Id, Name, Key_Contact__c FROM Contact WHERE AccountId=:accId 
                             ORDER BY LastModifieDdate DESC LIMIT 10000]);

    }
    
    /*******************************************************************************************************
     * @description used prepare Task sobject on the basis of specified field and create a task record
     * @return Map: having task's id with Account's id
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    @AuraEnabled
    public static Map<String, Object> prepareTask(String taskObj){
        Map<String, Object> keyValues = (Map<String, Object>)JSON.deserializeUntyped(taskObj);
        Task taskToInsert = new Task();
        for(String key: new List<String>{'WhatId','WhoId','Subject','Type__c','Status','Description',
            'Priority','Next_Action_Date__c','Next_Action__c','ActivityDate','Source__c','Signal_Action__c',
            'Signal_Category__c', 'Company_Signal_Headline__c', 'Company_Signal__c'}){
        	
            if(keyValues.containsKey(key)){
            	object value = keyValues.get(key);
                String strValue = (String)value;
            	if(value != NULL && String.isNotEmpty(strValue)){
	                
	                if(key == 'Next_Action_Date__c' || key == 'ActivityDate'){
	                    List<String> dateParts = strValue.split('-');
	                    taskToInsert.put(key, Date.newInstance(Integer.valueOf(dateParts[0]), 
	                                                           Integer.valueOf(dateParts[1]), 
	                                                           Integer.valueOf(dateParts[2])));
	                }else{
	                    taskToInsert.put(key, value);
	                }
            	}
            }
        }
        taskToInsert.ActivityDate = System.today();
        insert taskToInsert;

        return new Map<String, Object> {'taskId'=>taskToInsert.Id, 'accId'=>taskToInsert.WhatId};
    }
}