/**
 * @author 10k-Expert
 * @name IVPOwnEmailNotificationBtchTest
 * @description Test class for IVPOwnEmailNotificationBtch class
**/

@isTest
private class IVPOwnEmailNotificationBtchTest {

	@istest private static void test() {
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        List<Company_Request__c> requests = testData.companyRequests;
        
        requests[0].Request_expire_on__c = System.now().addHours(IVPOwnershipService.secondEmailNotifyHourGap);
        requests[1].Request_expire_on__c = System.now().addHours(IVPOwnershipService.finalEmailNotifyHourGap);
        update requests;
        Test.startTest();
        System.schedule('ivp-Email-notification', '00 00 * * * ?', new IVPOwnEmailNotificationBtch());
        Test.stopTest();
	}

}