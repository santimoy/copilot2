/**
 * @author 10k-Advisor
 * @date 2018-04-16
 * @className IVP_RedirectToDB_Ctrl
 * @group IVPSmartDash / tagging
 *
 * @description contains methods to return Smart Dash Intelligence_User_Preferences user
 */
public with sharing class IVP_RedirectToDB_Ctrl {
	
	@AuraEnabled
    public static String getIVPId(){
        String loggedInUserId 	= UserInfo.getUserId();
        
        List<Intelligence_User_Preferences__c> lstPref = [SELECT Id, Name
                                                          FROM Intelligence_User_Preferences__c 
                                                          WHERE Name = 'Smart Dash'
                                                          LIMIT 1];
        if(!lstPref.isEmpty()){
            return lstPref[0].Id;
        }else{
            lstPref = [SELECT Id
                          FROM Intelligence_User_Preferences__c 
                          WHERE User_Id__c =: UserInfo.getUserId()
                          LIMIT 1];
            
            if(!lstPref.isEmpty()){
                return lstPref[0].Id;
            }
            
            Intelligence_User_Preferences__c ivp = new Intelligence_User_Preferences__c();
            ivp.Name							 = 'Smart Dash';
            ivp.User_Id__c 						 = loggedInUserId;
            insert ivp;
            return ivp.Id;
        }
    }
}