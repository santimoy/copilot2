/***********************************************************************************
* @author      10k-Expert
* @name    IVPOwnProcessExpiredRequestBtchTest
* @date    2019-01-21
* @description  To test IVPOwnProcessExpiredRequestBtch
***********************************************************************************/
@isTest
public class IVPOwnProcessExpiredRequestBtchTest {
    @isTest
    public static void unitTest(){
        IVPOwnershipTestFuel testData = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        //Datetime previousDateTime = System.now().addDays(-1);
        List<Company_Request__c> companyRequests = testData.companyRequests;
        for(Company_Request__c crObj : companyRequests){
            crObj.Request_Expire_On__c = System.now().addDays(-1);
        }
        update companyRequests;
        Test.startTest();
        System.schedule('test-IVP Process expired requests', '00 00 * * * ?', new IVPOwnProcessExpiredRequestBtch());
        IVPOwnProcessExpiredRequestBtch batchObj = new IVPOwnProcessExpiredRequestBtch();
        Database.executeBatch(batchObj);
        Test.stopTest();
        List<Company_Request__c> crList = [Select Id,Status__c From Company_Request__c];
        for(Company_Request__c cr : crList){
            System.assert(cr.Status__c == 'Transfer - No Response');
    }
    }
}