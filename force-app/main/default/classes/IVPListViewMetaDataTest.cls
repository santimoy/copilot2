@isTest
private class IVPListViewMetaDataTest {
	private static testMethod void testParsing() {
        List<ListView> listViewRec=[SELECT Id FROM ListView LIMIT 1];
        String viewId=listViewRec[0].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new IVPWatchDogMock(false));
        String jsonResponse=IVPUtils.fetchListViewDetailById(viewId);
        IVPListViewMetaData metaData=new IVPListViewMetaData();
        IVPMetadataService.ListView lv=metadata.parse(jsonResponse);
       	System.assertEquals(10,lv.columns.size());
        Test.stopTest();
	}
    private static testMethod void testTypecasting(){
        List<ListView> listViewRec=[SELECT Id FROM ListView LIMIT 1];
        String viewId=listViewRec[0].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new IVPWatchDogMock(false));
        String jsonResponse=IVPUtils.fetchListViewDetailById(viewId);
      	IVPListViewMetaData listViewMetaData=(IVPListViewMetaData)JSON.deserialize(jsonResponse, IVPListViewMetaData.class);
      	System.assertEquals(15,listViewMetaData.columns.size());
        Test.stopTest();
    }

}