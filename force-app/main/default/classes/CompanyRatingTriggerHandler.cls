/***********************************************************************************
 * @author      10k-Expert
 * @name		companyRatingTriggerHandler
 * @date		2018-11-14
 * @description	TriggerHandler of companyRatingTrigger
 ***********************************************************************************/
public class CompanyRatingTriggerHandler {
    /*************************************************************************************
     * @author 		10K-Expert
     * @description handling before insert event of Company Rating
     * @param		List of new company request
     * @return 		void
     *************************************************************************************/
    public static void onBeforeInsert(List < Company_Rating__c > companyRatings){
        Set<Id> userIds = new Set<Id>();
        // getting requester
        for (Company_Rating__c companyRating: companyRatings) {
            if(companyRating.OwnerId != NULL){
                userIds.add(companyRating.OwnerId);
            }
        }
        // getting checking and populating Requestor_Primary_Partner with requester's Primary_Partner value.
        if(!userIds.isEmpty()){
            Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Primary_Partner__c 
                                                        FROM User 
                                                        WHERE Id in :userIds AND Primary_Partner__c != NULL]);
            
            for (Company_Rating__c companyRating: companyRatings) {
                if(companyRating.OwnerId != NULL && userMap.containsKey(companyRating.OwnerId)){
                    companyRating.Primary_Partner_Lookup__c = userMap.get(companyRating.OwnerId).Primary_Partner__c;
                }
            }
        }
    }
}