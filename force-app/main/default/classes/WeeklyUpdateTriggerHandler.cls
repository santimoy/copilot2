/*
*	WeeklyUpdateTriggerHandler is used to perform logic for WeeklyUpdateTrigger
*
*	Author: Troy Wing
*	Date:  August 13 2015
*
*/
public with sharing class WeeklyUpdateTriggerHandler {
	public WeeklyUpdateTriggerHandler() {
		
	}


	public void onBeforeUpdate(List<Weekly_Update__c> updatedWeeklyUpdateList, Map<Id, Weekly_Update__c> originalRecordMap) {
	System.debug('Starting onBeforeUpdate');

	// 20150908 Commented out as no longer needed
/*
		if(!wu_checkRecursive.runOnce()) return;
		Map<Id, Weekly_Update__c> changedSnippet = new Map<Id, Weekly_Update__c>();
		for(Weekly_Update__c updatedRecord : updatedWeeklyUpdateList) {
			Weekly_Update__c originalRecord = originalRecordMap.get(updatedRecord.Id);
		
			if((updatedRecord.Snippets__c !=null &&  originalRecord.Snippets__c==null && updatedRecord.Snippets__c != originalRecord.Snippets__c)) {
				changedSnippet.put(updatedRecord.Id, updatedRecord);
			}	
				
		}
		if(changedSnippet.size()>0){
			checkFutureWeek(changedSnippet);
		}
*/
	}

/*
	public void checkFutureWeek(Map<Id, Weekly_Update__c> changedSnippet ){
			Id userId= UserInfo.getUserId();
			Date futureDate;
			List<Id> projects= new List<Id>();
			List<Date> futureDates= new List<Date>();
			for(Weekly_Update__c wu:changedSnippet.values()){

				projects.add(wu.Project__c);
				futureDates.add(wu.Week_Ending__c.addDays(7));
			}
			List<Weekly_Update__c> future=[Select Last_Week__c,Id,Week_Ending__c,Project__c,Resource__c from Weekly_Update__c where  Resource__c=:userId and Project__c in :projects and  Week_Ending__c in :futureDates];

			List<Weekly_Update__c> pending = new List<Weekly_Update__c>();
			for(Weekly_Update__c wu:changedSnippet.values()){
				for(Weekly_Update__c fu:future) {
					if(fu.Project__c==wu.Project__c && wu.Week_Ending__c== fu.Week_Ending__c.addDays(-7) && wu.Resource__c==fu.Resource__c) {
						if(fu.Last_Week__c==null){
							fu.Last_Week__c=wu.Snippets__c;
							if(!changedSnippet.containsKey(fu.id)) pending.add(fu);
							
						}
						break;
					}
				}
			}
			update pending;

	}
	*/
}