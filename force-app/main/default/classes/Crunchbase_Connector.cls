//
// 3-30-2018 TO BE DEPRECATED
//
public with sharing class Crunchbase_Connector {
    private static final String INSIGHT_CBAPI_KEY_PARAM = 'api_key';
    private static final String INSIGHT_CBAPI_KEY_VALUE = 'jp9gv9r8mtw6vcw9n2dyxxbe';
    
    public static string testResponse='';
    
    public Crunchbase_Connector(){
    }
    
    public Crunchbase_Data.cbCompany getCbData(string cbUrl){
        HttpRequest req = new HttpRequest();
        
        string targetUrl = cbUrl;
        if (targetUrl.contains('?')) targetUrl += '&';
        else targetUrl += '?';
        targetUrl += INSIGHT_CBAPI_KEY_PARAM + '='+ INSIGHT_CBAPI_KEY_VALUE;
        
        req.setEndpoint(targetUrl);
        req.setMethod('GET');
        
        Http http = new Http();
        string respBody;
        
        if (Test.isRunningTest()){
            respBody = testResponse;    
        }
        else{
            HttpResponse resp = http.send(req);
        
            if (resp.getStatusCode()!=200){
                throw new Crunchbase_Exception( String.format('An error occured while calling the Crunchbase API. Status Code: {0} {1}', 
                    new string[]{ String.valueOf(resp.getStatusCode()), resp.getStatus() }) ); 
            }
            respBody = resp.getBody();
        }
        //system.Debug('\n---VERBOSE---respBody:'+respBody);
        
        Crunchbase_Data.cbCompany cbResp = (Crunchbase_Data.cbCompany)JSON.deserialize(respBody, Crunchbase_Data.cbCompany.class);
        system.debug('\n---cbResp:'+cbResp);
        
        return cbResp;
    }
    
    // ----------------------------------------------------------------------------------------------------------------------------------
    // Inner Classes
    public class Crunchbase_Exception extends Exception{}

    // ----------------------------------------------------------------------------------------------------------------------------------
    // Test Methods
    static testMethod void testThisClass(){
        Crunchbase_Connector testConnector = new Crunchbase_Connector();
        Crunchbase_Connector.testResponse = '{"name": "Punchey","permalink": "punchey","crunchbase_url": "http://www.crunchbase.com/company/punchey","homepage_url": "http://www.punchey.com","blog_url": "","blog_feed_url": "","twitter_username": "puncheyinc","category_code": null,"number_of_employees": null,"founded_year": 2011,"founded_month": null,"founded_day": null,"deadpooled_year": null,"deadpooled_month": null,"deadpooled_day": null,"deadpooled_url": null,"tag_list": "","alias_list": "","email_address": "info@punchey.com","phone_number": "800Punchey","description": "","created_at": "Fri Aug 17 12:46:10 UTC 2012","updated_at": "Sat Aug 18 01:20:40 UTC 2012","overview": "Punchey is the best way for local merchants to get discovered and accept payments. \\n\\nThe company provides an end-to-end payment processing and marketing solution for small and medium-sized merchants. Get paid wherever, whenever and bring more customers in with Punchey Mobile and Punchey Countertop. ","image":{"available_sizes":[[[150,45],"assets/images/resized/0020/6573/206573v1-max-150x150.jpg"],[[193,59],"assets/images/resized/0020/6573/206573v1-max-250x250.jpg"],[[193,59],"assets/images/resized/0020/6573/206573v1-max-450x450.jpg"]],"attribution": null},"products":[],"relationships":[{"is_past": false,"title": "CEO Founder","person":{"first_name": "Nathaniel","last_name": "Stevens","permalink": "nathaniel-stevens","image":{"available_sizes":[[[90,150],"assets/images/resized/0001/0294/10294v1-max-150x150.jpg"],[[120,200],"assets/images/resized/0001/0294/10294v1-max-250x250.jpg"],[[120,200],"assets/images/resized/0001/0294/10294v1-max-450x450.jpg"]],"attribution": null}}},{"is_past": false,"title": "Director of Business Development ","person":{"first_name": "Yuliya","last_name": "Sychikova","permalink": "yuliya-sychikova-2","image": null}}],"competitions":[],"providerships":[],"total_money_raised": "$1.7M","funding_rounds":[{"round_code": "unattributed","source_url": "http://www.finsmes.com/2012/08/punchey-raises-1-7m.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+finsmes%2FcNHu+%28FinSMEs%29","source_description": "Punchey Raises $1.7M","raised_amount": 1700000.0,"raised_currency_code": "USD","funded_year": 2012,"funded_month": 8,"funded_day": 17,"investments":[{"company": null,"financial_org":{"name": "Stevens Ventures","permalink": "stevens-ventures","image":{"available_sizes":[[[150,74],"assets/images/resized/0020/6576/206576v1-max-150x150.jpg"],[[250,123],"assets/images/resized/0020/6576/206576v1-max-250x250.jpg"],[[305,151],"assets/images/resized/0020/6576/206576v1-max-450x450.jpg"]],"attribution": null}},"person": null}]}],"investments":[],"acquisition": null,"acquisitions":[],"offices":[{"description": "","address1": "31 State Street","address2": "","zip_code": "02109","city": "Boston","state_code": "MA","country_code": "USA","latitude": null,"longitude": null}],"milestones":[],"ipo": null,"video_embeds":[],"screenshots":[{"available_sizes":[[[150,109],"assets/images/resized/0020/6574/206574v1-max-150x150.jpg"],[[250,183],"assets/images/resized/0020/6574/206574v1-max-250x250.jpg"],[[450,329],"assets/images/resized/0020/6574/206574v1-max-450x450.jpg"]],"attribution": null}],"external_links":[]}';
        
        Crunchbase_Data.cbCompany cbCompany = testConnector.getCbData('http://api.crunchbase.com/v/1/company/punchey.js');
        list<Crunchbase_Data.cbFundingRound> fundingList = cbCompany.funding_rounds;
        system.assertEquals(1, fundingList.size());
        
        Crunchbase_Data.cbFundingRound funding = fundingList.get(0);
        system.assertEquals('unattributed', funding.round_code);
        system.assertEquals('http://www.finsmes.com/2012/08/punchey-raises-1-7m.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+finsmes%2FcNHu+%28FinSMEs%29', funding.source_url);
        system.assertEquals(1700000, funding.raised_amount);
        system.assertEquals('USD', funding.raised_currency_code);
        system.assertEquals(2012, funding.funded_year);
        system.assertEquals(8, funding.funded_month);
        system.assertEquals(17, funding.funded_day);
        system.assertEquals(1, funding.investments.size());
        Crunchbase_Data.cbFundingRound_Investor investor = funding.investments.get(0); 
        system.assertEquals('Stevens Ventures', investor.financial_org.name);
        system.assertEquals('stevens-ventures', investor.financial_org.permalink);
    }
}