/**
 * @author 10k-Advisor
 * @date 2018-04-16
 * @className IVPCompanyListViewCtlr
 * @group IVPWatchDog
 *
 * @description contains methods to handle IVPWatchDog component
 */
public with sharing class IVPWatchDogCtlr {
    public class IVPWatchDogCtlrException extends Exception{}
    
    public class InputData{
        public String viewId;
        public Boolean modifyMetaData;
        public Boolean init;
        public List<String> selClrsOps;
        public List<String> selCatsOps;
    }
    
    public class IVPWDData{
        @AuraEnabled 
        public List<Company_Signal__c> signals;
        @AuraEnabled
        public Map<String, List<String>> filters;
        @AuraEnabled
        public List<Signal_Category__c> categories;
        
        public IVPWDData(){
            signals = new List<Company_Signal__c>();
            filters = new Map<String, List<String>> ();
            categories = new List<Signal_Category__c>();
        }
    }
    
    private static String fetchLVQuery(String viewId){
    	String jsonResponse = IVPUtils.fetchListViewDetailById(viewId);
        Map<String, Object> responseValue = new Map<String, Object>();
        try{
        	object response = JSON.deserializeUntyped(jsonResponse);
        	if(response instanceof Map<String, Object>){
            	responseValue = (Map<String, Object>)JSON.deserializeUntyped(jsonResponse);
        	}
        }catch(Exception excp){
        	throw new IVPWatchDogCtlrException(excp.getMessage());
        }
        // System.debug(JSON.serialize(responseValue));
        if(responseValue.containsKey('query')){
            return (String)responseValue.get('query');
        }
        return '';
    }
    
    private static List<sObject> fetchCompanySignals(String soqlQuery, List<Object> colors, List<Object> categories, Boolean isLimit){
    	List<Sobject> companySignalDataLst = new List<Sobject>();
    	Integer endIndex = soqlQuery.indexOfIgnoreCase('ORDER BY');
        endIndex = endIndex >=0 ? endIndex : soqlQuery.length();
        // System.debug('Vikas fetch Limit::'+isLimit);
        // String scop = 'USING SCOPE mine ';
        String innerSOQL = 'SELECT Id ' + soqlQuery.substring(soqlQuery.indexOfIgnoreCase('FROM'), endIndex);

        String usingScope = IVPUtils.extractSOQLScope(innerSOQL);
		innerSOQL = innerSOQL.replace(usingScope, '');
		String lastNDays = Label.IVP_WD_Last_X_Days;
		String lastNDaysFitlter = '';
		if(lastNDays.isNumeric()){
			lastNDaysFitlter = ' AND Date__c >=LAST_N_DAYS:'+lastNDays;
		}
        //If isLimit variable is true 
        //Then we have to query on 50000 records 
        //And fetch only signal category
		String wdRecLimit = isLimit ? Label.IVP_WD_Category_Record_Limit : Label.IVP_WD_Record_Limit;
		Integer recordLimit = 100;
		if(wdRecLimit.isNumeric()){
			recordLimit = Integer.valueOf(wdRecLimit);
		}
        // System.debug('Vikas::'+isLimit);
        // System.debug('Vikas::'+recordLimit);
		Integer validQryRows = LIMITS.getLimitQueryRows() - LIMITS.getQueryRows();
		if(recordLimit > validQryRows){
		    recordLimit = validQryRows;
		}
		String filters = (categories != NULL && categories.size() > 0 ? 'Category__c in : categories ':''); 
		filters += (colors != NULL && colors.size() > 0 ? (filters!='' ? ' OR ':'') + 'Category__r.Color_HEX__c in : colors ':'');
		
        String fields = isLimit ? 'Category__c, Category__r.Name, Category__r.External_Id__c ' : 'Id, Name, Company__c, Category__c, Date__c,'
            +'URL__c, Headline__c, Snippets__c,'
            +'Company__r.Name, Company__r.Next_Action_Date__c, Company__r.Next_Action__c,'
            +'Category__r.Name, Category__r.Image_URL__c, Category__r.External_Id__c, Category__r.Description__c, Category__r.Options__c,'
            +'Category__r.Color_HEX__c, Category__r.Salesloft_Cadence__c, '
            +'CreatedDate, LastModifiedDate ';
        
        String companySignalQuery = 'SELECT '+ fields
            +'FROM Company_Signal__c '
            + usingScope
            +' WHERE Company__c in ('+innerSOQL+') '
            +lastNDaysFitlter
            +' AND Category__c != NULL AND Category__r.Active__c = TRUE '
            +(filters!='' ?'AND ('+filters+')':'')
            + (isLimit ? 'GROUP BY Category__c, Category__r.Name, Category__r.External_Id__c ' : 'ORDER BY Date__c DESC NULLS LAST, Category__r.Priority__c NULLS LAST LIMIT '+ recordLimit);
        
        System.debug('=====> companySignalQuery: '+companySignalQuery);
        for(List<Sobject> compSignals : Database.query(companySignalQuery) ){
            companySignalDataLst.addAll(compSignals);
        }
        return companySignalDataLst;
    }
    
    @AuraEnabled
    public static IVPWDData getInitData(String inputJson){
        // System.debug('Vikas Input::'+inputJson);
        Map<String, Object> inputMap = (Map<String, Object>)Json.deserializeUntyped(inputJson);
        Boolean isLimit = (Boolean)inputMap.get('isLimit');
        // System.debug('Vikas Limit::'+isLimit);
        String viewId = '';
        if(inputMap.containsKey('viewId')){
            viewId = (String)inputMap.get('viewId');
        }
        List<Sobject> companySignalDataLst = new List<Sobject>();
    	IVP_SD_ListView__c currentUserSetting = IVPUtils.currentUserSetting();
    	List<String> colours = new List<String>();
    	List<String> cats = new List<String>();
    	Map<String, List<String> > filters = getWDFilters();
		List<ListView> listViews = new List<Listview>();
    	if(String.isNotBlank(viewId)){
    	    listViews = [SELECT Id,DeveloperName FROM ListView WHERE Id=:viewId];
    	}
    	if(String.isNotEmpty(currentUserSetting.List_View_Name__c)){
            // System.debug('Vikas ListView Name::'+currentUserSetting.List_View_Name__c);
            if(listViews.isEmpty()){
		        listViews = [SELECT Id,DeveloperName FROM ListView WHERE DeveloperName=:currentUserSetting.List_View_Name__c];
            }
    		if(listViews.isEmpty()){
    		    currentUserSetting = IVPUtils.currentOrgSetting();
    		    if(String.isNotEmpty(currentUserSetting.List_View_Name__c)){
    		        listViews = [SELECT Id,DeveloperName FROM ListView WHERE DeveloperName=:currentUserSetting.List_View_Name__c];
    		    }
    		}
    	}
		if(!listViews.isEmpty()){
    		String soqlQuery = fetchLVQuery(listViews[0].DeveloperName);
    		if(soqlQuery != ''){
	            companySignalDataLst = fetchCompanySignals(soqlQuery, colours, cats, isLimit);
	        }
		}
    	IVPWDData data = new IVPWDData();
    	data.signals = companySignalDataLst;
    	data.filters = filters;
        /*Set<Signal_Category__c> setCategorys= new Set<Signal_Category__c>();
        for(Company_Signal__c cs : data.signals){
            setCategorys.add(cs.Category__r);
        }
        data.categories = new List<Signal_Category__c>(setCategorys);*/
    	return data;
    }
    
    /*******************************************************************************************************
     * @description returning List of Company_Signal__c for specified list-view whose Id is being passed in the parameter
     * @return Company_Signal__c[]
     * @author Dharmendra Karamchandani
     * @param String inputJson	JSON value of input parameters
     * @note Don't change value of this property.
     */
     
    @AuraEnabled
    public static IVPWDData getCompanySignals(String inputJson){
        
        InputData inputValues = (InputData)JSON.deserialize(inputJson, InputData.class);
        List<Sobject> companySignalDataLst = new List<Sobject>();
        
        if(String.isNotBlank(inputValues.viewId)){
            if(inputValues.selClrsOps == NULL){
                inputValues.selClrsOps = new List<String>();
            }
            if(inputValues.selCatsOps == NULL){
                inputValues.selCatsOps = new List<String>();
            }
            Set<String> setSelCatsOps = new Set<String>();
            if(inputValues.init!=NULL && inputValues.init){
                Map<String, List<String>> filters = getWDFilters();
                inputValues.selClrsOps = filters.get('colours');
                setSelCatsOps.addAll(inputValues.selCatsOps);
                setSelCatsOps.addAll(filters.get('categories'));
                inputValues.selCatsOps = new List<String>(setSelCatsOps);
            }
            
            String soqlQuery = fetchLVQuery(inputValues.viewId);
	        if(soqlQuery != ''){
	            companySignalDataLst = fetchCompanySignals(soqlQuery, inputValues.selClrsOps, inputValues.selCatsOps, false);
	        }
        }
        IVPWDData data = new IVPWDData();
    	data.signals = companySignalDataLst;
    	return data;
    }
    
        /*******************************************************************************************************
     * @description returning map of task by taking key as Company_Signal__c value for selected accounts
     * @return object
     * @author Dharmendra Karamchandani
     * @param accountIds json values of account ids 
     * @note Don't change value of this property.
     */
    @AuraEnabled
    public static object getTasks(String accountIds){
        Set<String> ids = new Set<String>();
        Map<String, List<Task>> taskMap = new Map<String, List<Task>>();
        List<String> accIdsLst = (List<String>)System.JSON.deserialize(accountIds, List<String>.Class);
        
        Map<String, Signal_Category__c> signalCategoriesMap = new Map<String, Signal_Category__c>();
        
        for(Task taskObj : [SELECT Id, WhatId, Status, Company_Signal__c, Signal_Category__c, Type__c, Signal_Action__c
                            FROM Task
                            WHERE WhatId in :accIdsLst
                            AND Company_Signal__c!=NULL]){
        	
			ids.add(taskObj.WhatId);
			if(!taskMap.containsKey(taskObj.Company_Signal__c)){
				taskMap.put(taskObj.Company_Signal__c, new List<Task>());
			}
			taskMap.get(taskObj.Company_Signal__c).add(taskObj);

        }
        return (object)taskMap;
    }
    
    /*******************************************************************************************************
     * @description 
     * @return object
     * @author Dharmendra Karamchandani
     * @param accountIds json values of account ids 
     * @note Don't change value of this property.
     */
    @AuraEnabled
    public static object getKeyContacts(String accountIds){
        Map<String, List<Id>> accKeyConMap = new Map<String, List<Id>>();
        List<String> accIdsLst = (List<String>)System.JSON.deserialize(accountIds, List<String>.Class);
        
        Map<String, Signal_Category__c> signalCategoriesMap = new Map<String, Signal_Category__c>();
        
        for(Contact conObj : [SELECT Id, AccountId
                            FROM Contact
                            WHERE AccountId in :accIdsLst
                            AND Key_Contact__c = TRUE
                            AND Salesloft_Id__c != NULL]){
        	
			if(!accKeyConMap.containsKey(conObj.AccountId)){
				accKeyConMap.put(conObj.AccountId, new List<Id>());
			}
			accKeyConMap.get(conObj.AccountId).add(conObj.Id);

        }
        return (object)accKeyConMap;
    }
    
    @AuraEnabled
    public static object[] getCategories(){
    	return [SELECT id FROM Signal_category__c];
    }
    
    @AuraEnabled
    public static Map<String, List<String>> getWDFilters(){
        Map<String, List<String>> wdFilterMap = new Map<String, List<String>>{
            'colours'=>new List<String>(),
            'categories'=>new List<String>(),
            'myDashSignalPreferences'=>new List<String>()
        };
        List<Intelligence_User_Preferences__c> uPres = [SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c
                                                        FROM Intelligence_User_Preferences__c
                                                        WHERE User_Id__c=:UserInfo.getUserId()];
        if(uPres.isEmpty()) {
            Intelligence_User_Preferences__c IntelligenceUser = new Intelligence_User_Preferences__c();
            IntelligenceUser.Name = UserInfo.getName();
            IntelligenceUser.User_Id__c = UserInfo.getUserId();
            if(!Test.isRunningTest()){
                insert IntelligenceUser;
            }
            List<Intelligence_User_Preferences__c> uPresNewUser = [
                    SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c
                    FROM Intelligence_User_Preferences__c
                    WHERE User_Id__c = :UserInfo.getUserId()
            ];
            uPres.addAll(uPresNewUser);
        }
        if(!uPres.isEmpty()){
            if(String.isNotBlank(uPres[0].WD_Filter_Colour__c)){
                wdFilterMap.put('colours', uPres[0].WD_Filter_Colour__c.split(';'));
            }
            if(String.isNotBlank(uPres[0].WD_Filter_Category__c)){
                wdFilterMap.put('categories', uPres[0].WD_Filter_Category__c.split(';'));
            }
            if(String.isNotBlank(uPres[0].My_Dash_Signal_Preferences__c)){
                wdFilterMap.put('myDashSignalPreferences', uPres[0].My_Dash_Signal_Preferences__c.split(','));
            }
        }
        
        return wdFilterMap;
    }
    
    @AuraEnabled
    public static void setIVPSDSettings(String inputJson){
        try{
            InputData inputValues = (InputData)JSON.deserialize(inputJson, InputData.class);
            
            if(inputValues.modifyMetaData != NULL && inputValues.modifyMetaData){
                setWDFilters(inputValues.selClrsOps, inputValues.selCatsOps);
            }   
        }catch(Exception ex){
            System.debug(ex.getLineNumber());
            System.debug(ex.getLineNumber());
            System.debug(ex.getStackTraceString());
        }
    }
    
    @AuraEnabled
    public static void setWDFilters(List<String> colours, List<String> cats){
        Intelligence_User_Preferences__c usrPreference;
        for(Intelligence_User_Preferences__c uPre : [SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c
                                                        FROM Intelligence_User_Preferences__c
                                                        WHERE User_Id__c=:UserInfo.getUserId()]){
            usrPreference = uPre;
        }
        if(usrPreference == NULL){
            usrPreference = new Intelligence_User_Preferences__c(User_Id__c=UserInfo.getUserId(), Name=UserInfo.getName());
        }
        usrPreference.WD_Filter_Colour__c = String.join(colours,';');
        usrPreference.WD_Filter_Category__c = String.join(cats,';');
        upsert usrPreference;
    }
    
    @AuraEnabled
    public static void processKeyContact(String inputJson){
        Map<String, Object> inputMap = (Map<String, Object>)Json.deserializeUntyped(inputJson);
        String accId = inputMap.get('accId')==null ? '' : (String) inputMap.get('accId');
        String salesloftCadence = inputMap.get('salesloftCadence')==null ? '' : (String) inputMap.get('salesloftCadence');
        if(accId == '')
            return;
        List<Contact> contacts = [SELECT Id FROM Contact
                                    WHERE AccountId = :accId
                                    AND Key_Contact__c=TRUE
                                    ORDER BY LastModifiedDate DESC LIMIT 1];
        if(!contacts.isEmpty()){
            try{
                SalesloftCadenceMemberSyncService.pushCadenceMember(contacts[0].Id, salesloftCadence);
            }Catch(Exception ex){
                System.debug(ex.getStackTraceString());
                System.debug(ex.getMessage());
                throw new AuraHandledException(ex.getMessage());
            }
        }
    }
    
    @AuraEnabled
    public static List<Salesloft_Cadence_Member__c> getCadenceMemberDetails(String inputJson){
        InputData data = (InputData)JSON.deserialize(inputJson, InputData.Class);
        List<String> conIds = data.selClrsOps;
        Set<String> cadNames = new Set<String>();
        for(String cadName: data.selCatsOps){
            cadNames.add(cadName.toLowerCase());
        }
        System.debug('=>'+cadNames);
        List<Salesloft_Cadence_Member__c> cadMemberDetails = new List<Salesloft_Cadence_Member__c>();
        for(List<Salesloft_Cadence_Member__c> cadMembers: [SELECT Contact__c,Contact__r.AccountId, Cadence__c, Cadence__r.Name 
                                                            FROM Salesloft_Cadence_Member__c
                                                            WHERE Contact__c in : conIds AND Cadence__r.Name in : cadNames]){
            
            for(Salesloft_Cadence_Member__c cadMember: cadMembers){
                cadNames.remove(cadMember.Cadence__r.Name.toLowerCase());
                cadMemberDetails.add(cadMember);
            }
        }
        System.debug('=>'+cadNames);
        if(!cadNames.isEmpty()){
            for(List<Salesloft_Cadence__c> cadences: [SELECT Id, Name 
                                                        FROM Salesloft_Cadence__c
                                                        WHERE Name in : cadNames]){
                for(Salesloft_Cadence__c cadence : cadences){
                    Salesloft_Cadence_Member__c cadMember = new Salesloft_Cadence_Member__c(Cadence__c = cadence.Id);
                    cadMember.Cadence__r = cadence;
                    cadMemberDetails.add(cadMember);
                }
            }
        }
        return cadMemberDetails;
    }
}