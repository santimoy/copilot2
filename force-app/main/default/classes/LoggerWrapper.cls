public class LoggerWrapper {
    public Boolean isApiError {get; set;}
    public Boolean readTimeout {get; set;}
    public String apiError {get; set;}
    public String responseBody {get; set;}
    public Integer responseCode {get; set;}
    public LoggerWrapper(){
        readTimeout = false;
    }
}