/**
* @author 10k advisor
* @date 2018-06-14
* @className IVP_IFL_MarkFeedbackIgnoreBatch_Test
*
* @description Test class for IFL to set Ignore if they Expires 
*/
@isTest
public class IVP_IFL_MarkFeedbackIgnoreBatch_Test {
    
    /**
     * @description : This method will test with 'IQ_Score' Record-Type instead of Im_Feeling_Lucky Here 0 records will process in batch
	*/
    @isTest
    static void testFeedbackWithOtherRecordType() {
        Id currentUserId = UserInfo.getUserId();        
        Id unassignedUserId = IVPUtils.unassignedUserId;
        
        List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);        
        List<Intelligence_Feedback__c> testIFLRecords = new List<Intelligence_Feedback__c>();
        Id iqScoreRecordTypeId = IVPUtils.getRecordTypeId('IQ Score','Intelligence_Feedback__c');
        for(Account companyObj:testCompanies){
            testIFLRecords.add(new Intelligence_Feedback__c(
                Company_Evaluated__c = companyObj.Id,
                User__c = currentUserId,
                Sequence__c = companyObj.IFL_Sequence__c + 1,
                Assignment_Date_Time__c = System.now(),
                IQ_Score_at_Creation__c =   companyObj.IQ_Score__c,
                RecordTypeId = iqScoreRecordTypeId,
                Accurate_IQ_Score__c = null
            ));
        }
        insert testIFLRecords;
        Test.startTest();
        
        String CRON_EXP = '0 0 0 15 3 ? *';
        System.schedule('IVP_IFL_MarkFeedbackIgnoreBatch',  CRON_EXP, new IVP_IFL_MarkFeedbackIgnoreBatch());
        
        Test.stopTest();
        List<Intelligence_Feedback__c> listIFL = [Select Id,Name,Accurate_IQ_Score__c from Intelligence_Feedback__c where Accurate_IQ_Score__c='Ignore'];
        System.assertEquals(0, listIFL.size());
	}
    
    /**
     * @description : This method will test with 'IM_Felling_Lucky'
	*/
    @isTest
    static void testFeedbackWithIAmFeelingLuckyRT() {
        Id currentUserId = UserInfo.getUserId();        
        Id unassignedUserId = IVPUtils.unassignedUserId;
        
        List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);        
        List<Intelligence_Feedback__c> testIFLRecords = new List<Intelligence_Feedback__c>();
        for(Account companyObj:testCompanies){
            testIFLRecords.add(new Intelligence_Feedback__c(
                Company_Evaluated__c = companyObj.Id,
                User__c = currentUserId,
                Sequence__c = companyObj.IFL_Sequence__c + 1,
                Assignment_Date_Time__c = System.now()-10,
                IQ_Score_at_Creation__c =   companyObj.IQ_Score__c,
                RecordTypeId = IVPUtils.iFLRecordTypeId,
                Accurate_IQ_Score__c = null
            ));
        }
        
        insert testIFLRecords;
        
        Test.startTest();
        String CRON_EXP ='0 0 0 3 9 ? 2018';
        System.schedule('IVP_IFL_MarkFeedbackIgnoreBatch',  CRON_EXP, new IVP_IFL_MarkFeedbackIgnoreBatch());
        Database.executeBatch(new IVP_IFL_MarkFeedbackIgnoreBatch());
        
        Test.stopTest();

        List<Intelligence_Feedback__c> listIFL = [Select Id,Name,Accurate_IQ_Score__c from Intelligence_Feedback__c where Accurate_IQ_Score__c='Ignore'];
        System.assertEquals(5, listIFL.size());
	}
}