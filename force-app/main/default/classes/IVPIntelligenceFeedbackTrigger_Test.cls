@isTest
public class IVPIntelligenceFeedbackTrigger_Test {
    private static testMethod void test1(){
        List<Intelligence_Feedback__c> IntelligenceFeedback=new List<Intelligence_Feedback__c>();
        IVPTestFuel test=new IVPTestFuel();
        List<Account> acc=test.companies;
        
        system.assertNotEquals(null,acc.size());
        
        IntelligenceFeedback=test.IntelligenceFeedback;
        for(Intelligence_Feedback__c intelligence:IntelligenceFeedback){
            intelligence.Accurate_IQ_Score__c='Accept';
        }
        update IntelligenceFeedback;
    }
}