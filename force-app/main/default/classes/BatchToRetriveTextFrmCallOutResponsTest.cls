/*
Class Name   :  BatchToRetriveTextFromCallOutResponseTest
Description  :  Test Fetching first 255 characters from Callout Response field of Key Contact Scope and store in Trimmed Callout response field
Author       :  Sukesh
Created On   :  October 14 2019
*/

@isTest
private class BatchToRetriveTextFrmCallOutResponsTest {

    @isTest
    static void testKeyContactScopeErrorRecords(){

        List<Key_Contact_Scope__c> lstKeyContactScope = new List<Key_Contact_Scope__c>();

        for(Integer intCount = 1; intCount<=200; intCount++) {

            lstKeyContactScope.add(new Key_Contact_Scope__c(Callout_Response__c = 'Test Response "data"=>[]'));
        }

        insert lstKeyContactScope;

        Test.startTest();
         Database.executeBatch(new BatchToRetriveTextFromCallOutResponse());
        Test.stopTest();

        Database.executeBatch(new Batch_BlankOutProcessedDateTimeForError());
    }
}