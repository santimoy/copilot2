/**
*@name      : CampaignHandler
*@developer : 10K developer
*@date      : 2017-09-21
*@description   : The class used as handler of Campaign_Trgr
						Whenever a new campaign is updated/deleted need to do same with his child campaign's
**/

public class CampaignHandler extends TriggerHandler {
    public CampaignHandler (){
        
    }
    /*
     * This method will be called for After Delete event of Campaign, it will delete it's child Campaigns!
     * Child Campaign_Company is having master detail relationship so it will automatically be deleted!
	*/
    /*public override void beforeDelete(){
    	CampaignCompanyAutomation.deleteChildCampaignsWhenParentCampaignDeleted(
            CampaignCompanyAutomation.applyChildCampaignCheckAndGetParentCampaigns ( Trigger.old, 'delete' )
        );
    }*/
    /*
     * This method will call after update campaign and update all the child campaigns  records 
	*/
    /*public override void afterUpdate(){
        CampaignCompanyAutomation.updateChildCampaignsWhenParentCampaignUpdated(
            CampaignCompanyAutomation.applyChildCampaignCheckAndGetParentCampaigns ( Trigger.new, 'modify' )
        );
    }*/
    public override void afterUpdate(){
        Set<Id> campaignIds = new Set<Id>();
        Set<Id> campaignId = new Set<Id>();
        List<CampaignMember> campMembersToUpdate = new List<CampaignMember>();
        
        for(Campaign campObj : (List<Campaign>)Trigger.New){
            campaignIds.add(campObj.Id);
        }
        
        for(CampaignMember campMember : [Select Id,Status,Opportunity__c,Ready_For_Processing_On__c,CampaignId from CampaignMember where CampaignId in :campaignIds ]){
            if( campMember.Opportunity__c == NULL && campMember.Ready_For_Processing_On__c  != NULL ){
                campMembersToUpdate.add(campMember);
                campaignId.add(campMember.CampaignId);
            }
        }
        CampaignMemberHandler.updateCampaignMemberProcessingOnDate_static(campaignId,campMembersToUpdate); 
        update campMembersToUpdate;
    }
}