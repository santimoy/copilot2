/**
* @author 10k advisor
* @date 2018-07-05
* @className IVPTopicAssignmentTriggerHandler_Test
* @group IVPSmartDash
*
* @description contains methods to Test IVPTopicAssignmentTriggerHandler
*/
@isTest
public class IVPTopicAssignmentTriggerHandler_Test {
    @isTest static void testTopic(){
        List<Topic> topics = new List<Topic>();
        List<Account> acts = new List<Account>();
        List<TopicAssignment> topicAssignments = new List<TopicAssignment>();
        topics.add(new Topic(Name='Demo Topic'));
        topics.add(new Topic(Name='Demo Topic For testing purpose'));
        insert topics;
        acts.add(new Account(Name='Account For topic'));
        acts.add(new Account(Name='Account For topic Demo'));
        insert acts;
        TopicAssignment topicAssignment1 = new TopicAssignment(EntityId=acts[0].Id,TopicId=topics[0].Id);
        insert topicAssignment1;
        system.assertEquals(topicAssignment1.Topic.Name, acts[0].Tags__c);
        delete topicAssignment1;
        
        TopicAssignment topicAssignment2 = new TopicAssignment(EntityId=acts[0].Id,TopicId=topics[1].Id);
        insert topicAssignment2;
        system.assertEquals(topicAssignment2.Topic.Name, acts[0].Tags__c);
        delete topicAssignment2;
        
        TopicAssignment topicAssignment3 = new TopicAssignment(EntityId=acts[1].Id,TopicId=topics[0].Id);
        insert topicAssignment3;
        system.assertEquals(topicAssignment3.Topic.Name, acts[0].Tags__c);
        delete topicAssignment3;
        
        TopicAssignment topicAssignment4 = new TopicAssignment(EntityId=acts[1].Id,TopicId=topics[1].Id);
        insert topicAssignment4;
        system.assertEquals(topicAssignment4.Topic.Name, acts[1].Tags__c);
        delete topicAssignment4;
    }
    
    @isTest static void testBulkTopic(){
        List<Topic> topics = new List<Topic>();
        List<Account> acts = new List<Account>();
        List<TopicAssignment> tpicAssignts = new List<TopicAssignment>();
        
        for(Integer i=0;i<4;i++){
            topics.add(new Topic(Name='Demo Topic'+i));
        }
        insert topics;
        
        for(Integer i=0;i<2;i++){
            acts.add(new Account(Name='Account For topic'+i));
        }
        insert acts;
        
        for(Integer i=0;i<4;i++){
            if(i<2){
            	tpicAssignts.add(new TopicAssignment(EntityId=acts[0].Id,TopicId=topics[i].Id));    
            }
            tpicAssignts.add(new TopicAssignment(EntityId=acts[1].Id,TopicId=topics[i].Id));
        }
        insert tpicAssignts;
        system.assertEquals(tpicAssignts[0].Topic.Name, acts[0].Tags__c);
        system.assertEquals(tpicAssignts[1].Topic.Name, acts[1].Tags__c);
        delete tpicAssignts;
    }
}