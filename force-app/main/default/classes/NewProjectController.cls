public with sharing class NewProjectController {

	public NewProjectController() {
		
	}
  @AuraEnabled
  public static String fetchTheme()
  {
    return UserInfo.getUiTheme();
  }
  @AuraEnabled
  public static String fetchDomain()
  {
    String domainString = System.URL.getSalesforceBaseUrl().getHost();
    String trimmedString = (domainString).substring(0,domainString.length()-18);
    if(trimmedString.indexOf('.')==-1)
    {
      List<Organization> org = [Select Id,InstanceName from Organization limit 1];
      if(!org.isEmpty())
      {
        trimmedString += '--krow.'+org[0].InstanceName;
      }
    }
    else
    {
      trimmedString = trimmedString.replace('.','--krow.');
    }
    return trimmedString;
  }
  static public String protocolAndHost {
    get {
        if (protocolAndHost == null) {
            //memoize
            String orgId = UserInfo.getOrganizationId();
            String userId = UserInfo.getUserId();
            String sessionId = UserInfo.getSessionId();

            //use getSalesforceBaseUrl within batches and schedules (not Visualforce), and fix inconsistent protocol
            if (sessionId == null) return Url.getSalesforceBaseUrl().toExternalForm().replace('http:', 'https:');

            PageReference pr = new PageReference('/id/' + orgId + '/' + userId);
            pr.getParameters().put('oauth_token', sessionId);
            pr.getParameters().put('format', 'json');

            //within test context use url class, else derive from identity api
            String data = Test.isRunningTest() ? '{"urls": {"rest": "' + Url.getSalesforceBaseUrl().toExternalForm() + '"}}' : pr.getContent().toString();
            Map<String,Object> result = (Map<String,Object>)Json.deserializeUntyped(data);
            Map<String,Object> urls = (Map<String,Object>)result.get('urls');

            //compose pod from the REST endpoint
            Url rest = new Url((String)urls.get('rest'));
            protocolAndHost = rest.getProtocol() + '://' + rest.getHost();
        }

        return protocolAndHost;
    }
}
  @AuraEnabled
  public static Boolean checkJunctionCustomSetting(String projName)
  {
    if([Select Id from Related_Companies_Setting__c where Name =: projName].size()>0)
    {
      return true;
    }
    return false;
  }
  @AuraEnabled
  public static String fetchCompanyProjects(String companyId)
  {
    return Json.serialize([Select Id,Name,Active_WG_Members__c,Project_Start_Date_Formatted__c, Project_Category__c,Insight_Project_Type__c,Krow__Project_Start_Date__c,(Select Id from Krow__ProjectAssignments__r where Krow__User__c =: UserInfo.getUserId()),Krow__Total_Hours_Logged__c from Krow__Project__c where Krow__Account__c =:companyId and Krow__Project_Status__c='Active' order By Krow__Project_Start_Date__c DESC]);
  }
    @AuraEnabled
  public static Boolean fetchKrowSettings()
  {
    Krow__Krow_User_Configuration__c mhc = Krow__Krow_User_Configuration__c.getInstance(UserInfo.getUserId());
    return mhc.Krow__Disable_New_Task_Creation__c;
  }
  @AuraEnabled
  public static String fetchKrowEndUrl()
  {
    Krow_URLs__c mhc = Krow_URLs__c.getOrgDefaults();
    return mhc.EndUrl__c;
  }
  @AuraEnabled
  public static String addUserToProject(String projectId,String companyId)
  {
      Krow__Project_Resources__c projResource = [Select Id from Krow__Project_Resources__c where Krow__User__c =:UserInfo.getUserId() limit 1];
      if(projResource!=null)
      {
        Krow__Project_Assignment__c assign = new Krow__Project_Assignment__c(Krow__Status__c='Booked',Krow__Start_Date__c=Date.today(),Krow__Project_Resource__c=projResource.Id,Krow__Krow_Project__c=projectId);
        insert assign;
        return NewProjectController.fetchCompanyProjects(companyId);
      }
      return 'ERROR';

  }
	@AuraEnabled
	public static String fetchFields()
	{
		List<Schema.FieldSetMember> fieldsSetList = fetchFieldSet();
		FieldSetObject fieldObject = new FieldSetObject();
		fieldObject.fieldSet=fieldsSetList;
		Map<String,List<String>> picklistVals = new Map<String,List<String>>();
		Map<String,FieldLabelName> lookupVals = new Map<String,FieldLabelName>();
		for(Schema.FieldSetMember field:fieldsSetList)
		{
			if(field.getType()==Schema.DisplayType.Picklist)
			{
				picklistVals.put(field.getFieldPath(),NewProjectController.getPicklistValues('Krow__Project__c',field.getFieldPath()));
			}
			else if(field.getType()==Schema.DisplayType.Reference)
			{
				List<Schema.sObjectType> parentObj = NewProjectController.getLookupField('Krow__Project__c',field.getFieldPath());
				if(parentObj.size()>1)
				{
					FieldLabelName fieldLblName = new FieldLabelName();
					fieldLblName.apiName = 'User';
					fieldLblName.label = 'User';
					lookupVals.put(field.getFieldPath(),fieldLblName);
				}
				else
				{
					FieldLabelName fieldLblName = new FieldLabelName();
					fieldLblName.apiName = parentObj[0].getDescribe().getName();
					fieldLblName.label = parentObj[0].getDescribe().getLabel();
					lookupVals.put(field.getFieldPath(),fieldLblName);
				}
			}
		}
		fieldObject.picklistValues= picklistVals;
		fieldObject.lookupValues = lookupVals;
		return JSON.serialize(fieldObject);
	}
	@AuraEnabled
	public static List<AccountTeamMember> fetchAccountTeam(String accId)
	{
    List<New_Project_Account_Team__c> projTeamRoles = New_Project_Account_Team__c.getall().values();
    List<String> projRoleNames = new List<String>();
    for(New_Project_Account_Team__c role:projTeamRoles)
    {
      projRoleNames.add(role.Name);
    }
		return [Select Id,UserId,User.Name,TeamMemberRole from AccountTeamMember where AccountId =: accId and TeamMemberRole in:projRoleNames];
	}
  @AuraEnabled
  public static User fetchOwnerId()
  {
    return [Select Id,Name from User where Id=:UserInfo.getUserId() limit 1];
  }
 //************************************* 
 @AuraEnabled
 public static List <String> getselectOptions(sObject objObject, string fld) {
  
      List < String > allOpts = new list < String > ();
      // Get the object type of the SObject.
      Schema.sObjectType objType = objObject.getSObjectType();
     
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
     
      // Get a map of fields for the SObject
      map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
     
      // Get the list of picklist values for this field.
      list < Schema.PicklistEntry > values =
       fieldMap.get(fld).getDescribe().getPickListValues();
     
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a: values) {
       allOpts.add(a.getValue());
      }
     
      return allOpts;
 }
 
  @AuraEnabled
  public static String fetchCategoryType(String fieldApiName){
      if(String.isNotBlank(fieldApiName))
      {
          String FieldAPIName1 = fieldApiName.replaceAll('\\s+', '_'); 
          String type;
          List<Krow__Team__c> lstKrowTeam = new List<Krow__Team__c>();
         
          List<Krow__Project_Resources__c> lstProjectResource = [Select Id, Krow__User__c, Krow__Team__c FROM Krow__Project_Resources__c WHERE Krow__User__c  =: UserInfo.getUserId() LIMIT 1];
          
          if(lstProjectResource[0].Krow__Team__c != null && lstProjectResource[0].Krow__User__c != null) {
              
            String query = 'Select Id,'+FieldAPIName1+'__c FROM Krow__Team__c Where Id = \''+lstProjectResource[0].Krow__Team__c+'\'';
            lstKrowTeam = Database.query(query);
            type = (String)lstKrowTeam[0].get(FieldAPIName1+'__c');
            return type;
          }
      }
      
      return '';
      
  }
  //************************************* 
	public static List<Schema.FieldSetMember> fetchFieldSet() {
        return SObjectType.Krow__Project__c.FieldSets.New_Project_Fieldset.getFields();
    }
    public class FieldSetObject{
    	public List<Schema.FieldSetMember> fieldSet;
    	public Map<String,List<String>> picklistValues;
    	public Map<String,FieldLabelName> lookupValues;

    }
    public class FieldLabelName{
    	public String apiName;
    	public String label;
    }

    public static List<Schema.sObjectType> getLookupField(String ObjectApi_name,String Field_name)
    {
    	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
	  	Sobject Object_name = targetType.newSObject();
	  	Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
	    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
	    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
	    List<Schema.sObjectType> lookupValues = field_map.get(Field_name).getDescribe().getReferenceTo(); //grab lookup value
    	return lookupValues;
    }
    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
	  	List<String> lstPickvals=new List<String>();
	  	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
	  	Sobject Object_name = targetType.newSObject();
	  	Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
	    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
	    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
	    List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
	    for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
	      lstPickvals.add(a.getValue());//add the value  to our final list

   		}
  		return lstPickvals;
	}
	@AuraEnabled
    public static String createProjectController(String jsonString,List<String> projectAssignments,/*String projectGroup,*/ String projectCategory, String insightProjectType, List<String> junctionAccts,List<String> metrics, Decimal metricDelay)
    {
      List<fieldObj> fieldsList = (List<fieldObj>) System.JSON.deserialize(jsonString,List<fieldObj>.class);
      Krow__Project__c createKrowProject = new Krow__Project__c();
      for(fieldObj field:fieldsList)
      {
      	if(field.value!=null)
      	{
      		if(field.fieldtype=='DATE')
      		{
            if(field.value!=''&&field.value!=null)
            {
              createKrowProject.put(field.apiName,Date.parse(NewProjectController.preParseDate(field.value)));
            }
      		}
          else if(field.fieldtype=='BOOLEAN')
          {
            createKrowProject.put(field.apiName,Boolean.valueOf(field.value));
          }
      		else
      		{
      			createKrowProject.put(field.apiName,field.value);
      		}
      	}
      }
      /*System.debug('whatisproejctgroup====='+projectGroup+'=====');
      if(projectGroup!='--- None ---')
      {
        createKrowProject.Project_Group__c=projectGroup;
      }*/
      if(projectCategory!='--- None ---')
      {
        createKrowProject.Project_Category__c=projectCategory;
      }
      if(insightProjectType!='--- None ---')
      {
        createKrowProject.Insight_Project_Type__c=insightProjectType;
      }
      
      List<Krow__Project_Resources__c> lstProjectResource = [Select Id, Krow__User__c, Krow__Team__c FROM Krow__Project_Resources__c WHERE Krow__User__c  =: UserInfo.getUserId() LIMIT 1];
      if(lstProjectResource[0].Krow__Team__c != null) {
          
          createKrowProject.Team__c = lstProjectResource[0].Krow__Team__c;
      }
      System.debug('metricdelay==='+metricDelay);
      createKrowProject.Delay_Before_Measuring_Impact__c = metricDelay;
      try
      {
        insert createKrowProject;
        List<Krow__Project_Assignment__c> newProjectAssignment = new List<Krow__Project_Assignment__c>();
        for(String projAssign:projectAssignments)
        {
            newProjectAssignment.add(new Krow__Project_Assignment__c(Krow__Project_Resource__c=projAssign,Krow__Krow_Project__c=createKrowProject.Id));
        }
        //List<Project_Partner__c> newProjectPartners = new List<Project_Partner__c>();
        //for(String projPart:projectPartners)
        //{
        //  PartnerPerson part = (PartnerPerson)JSON.deserialize(projPart, PartnerPerson.class);
        //  if(part.lead=='Lead Partner')
        //  {
        //    newProjectPartners.add(new Project_Partner__c(Partner__c=part.Id,Project__c=createKrowProject.Id,Lead_Partner__c=true));
        //  }
        //  else
        //  {
        //    newProjectPartners.add(new Project_Partner__c(Partner__c=part.Id,Project__c=createKrowProject.Id ));
        //  }
        //}
        insert newProjectAssignment;
        //insert newProjectPartners;
        List<Project_Metric__c> projMetrics = new List<Project_Metric__c>();
        if(metrics.size()>0)
        {
          for(String metricStr:metrics)
          {
            projMetrics.add(new Project_Metric__c(Project__c=createKrowProject.Id,Metric__c=metricStr,Category__c=projectCategory));
          }
        }
        insert projMetrics;

        CreateObjectReturn ret = new CreateObjectReturn();
        ret.status = 'SUCCESS';
        ret.retId = createKrowProject.Id;
                System.debug('success');
        if(junctionAccts.size()>0)
        {
          List<Project_Company__c> juncList = new List<Project_Company__c>();
          for(String acc:junctionAccts)
          {
            Project_Company__c junc = new Project_Company__c();
            junc.Krow_Project__c=ret.retId;
            junc.Account__c = acc;
            juncList.add(junc);
          }
          insert juncList;
        }
        return JSON.serialize(ret);
      }
      catch(DmlException e)
      {
        System.debug('error');
        CreateObjectReturn ret = new CreateObjectReturn();
        ret.status = 'ERROR';
        ret.errorMsg = 'ERROR: '+e.getDmlMessage(0);
        return JSON.serialize(ret);
      }
      
      return '';
    }
    public static String preParseDate(String dateVal)
    {
      if(dateVal!= null && dateVal!='')
      {

        List<String> dateParts = dateVal.split('-');
        if(dateParts.size()>1)
        {
          return dateParts[1]+'/'+dateParts[2]+'/'+dateParts[0];
        }
      }
      return dateVal;
    }
    public class fieldObj
    {
    	public String apiName;
    	public String value;
    	public String fieldtype;
    }
    public class CreateObjectReturn
    {
      public String status;
      public String retId;
      public String errorMsg;
    }
    @AuraEnabled
    public static TypeAheadRes[] typeAheadFuncLtng(String rName, String sObjName,String filter, String recordData)
    {
        return TypeaheadFunction.srch(rName,sObjName,filter,'', recordData);
    }

// **********************************************************************************************************************************
// ********** GetDependentOptions ***************************************************************************************************
// **********************************************************************************************************************************
// Map<String,List<String>> GetDependentOptions (String objApiName, String contrfieldApiName, String depfieldApiName)
// Returns: Map of "contrfieldApiName" picklist values and their corresponding "depfieldApiName" dependent option values.
// **********************************************************************************************************************************

// Converts a base64 string into a list of integers representing the encoded bytes
public static List<Integer> B64ToBytes (String sIn) {
  Map<Integer,Integer> base64 = new Map<Integer,Integer>{65=>0,66=>1,67=>2,68=>3,69=>4,70=>5,71=>6,72=>7,73=>8,74=>9,75=>10,76=>11,77=>12,78=>13,79=>14,80=>15,81=>16,82=>17,83=>18,84=>19,85=>20,86=>21,87=>22,88=>23,89=>24,90=>25
                               ,97=>26,98=>27,99=>28,100=>29,101=>30,102=>31,103=>32,104=>33,105=>34,106=>35,107=>36,108=>37,109=>38,110=>39,111=>40,112=>41,113=>42,114=>43,115=>44,116=>45,117=>46,118=>47,119=>48,120=>49,121=>50,122=>51
                               ,48=>52,49=>53,50=>54,51=>55,52=>56,53=>57,54=>58,55=>59,56=>60,57=>61,43=>62,47=>63};

  List<Integer> lstOut = new List<Integer>();
  if ( sIn == null || sIn == '' ) return lstOut;
  
  sIn += '='.repeat( 4 - Math.mod( sIn.length(), 4) );

  for ( Integer idx=0; idx < sIn.length(); idx += 4 ) {
    if ( base64.get(sIn.charAt(idx+1)) != null ) lstOut.add( (base64.get(sIn.charAt(idx)) << 2) | (base64.get(sIn.charAt(idx+1)) >>> 4) );
    if ( base64.get(sIn.charAt(idx+2)) != null ) lstOut.add( ((base64.get(sIn.charAt(idx+1)) & 15)<<4) | (base64.get(sIn.charAt(idx+2)) >>> 2) );
    if ( base64.get(sIn.charAt(idx+3)) != null ) lstOut.add( ((base64.get(sIn.charAt(idx+2)) & 3)<<6) | base64.get(sIn.charAt(idx+3)) );
  }

  //System.Debug('B64ToBytes: [' + sIn + '] = ' + lstOut);
  return lstOut;
}//B64ToBytes
public static List<Integer> BlobToBytes (Blob input) {
  return B64ToBytes( EncodingUtil.base64Encode(input) );
}//BlobToBytes

// Converts a base64 string into a list of integers indicating at which position the bits are on
public static List<Integer> cnvBits (String b64Str) {
  List<Integer> lstOut = new List<Integer>();
  if ( b64Str == null || b64Str == '' ) return lstOut;

  List<Integer> lstBytes = B64ToBytes(b64Str);

  Integer i, b, v;
  for ( i = 0; i < lstBytes.size(); i++ ) {
    v = lstBytes[i];
    //System.debug ( 'i['+i+'] v['+v+']' );
    for ( b = 1; b <= 8; b++ ) {
      //System.debug ( 'i['+i+'] b['+b+'] v['+v+'] = ['+(v & 128)+']' );
      if ( ( v & 128 ) == 128 ) lstOut.add( (i*8) + b );
      v <<= 1;
    }
  }

  //System.Debug('cnvBits: [' + b64Str + '] = ' + lstOut);
  return lstOut;
}//cnvBits

public class TPicklistEntry{
  public string active {get;set;}
  public string defaultValue {get;set;}
  public string label {get;set;}
  public string value {get;set;}
  public string validFor {get;set;}
  public TPicklistEntry(){}
}//TPicklistEntry

    @AuraEnabled  
    public static Map<String,List<String>> getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
      Map<String,List<String>> mapResults = new Map<String,List<String>>();

      //verify/get object schema
      Schema.SObjectType pType = Schema.getGlobalDescribe().get(objApiName);
      if ( pType == null ) return mapResults;
      Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();

      //verify field names
      if (!objFieldMap.containsKey(contrfieldApiName) || !objFieldMap.containsKey(depfieldApiName)) return mapResults;     

      //get the control & dependent values   
      List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(contrfieldApiName).getDescribe().getPicklistValues();
      List<Schema.PicklistEntry> dep_ple = objFieldMap.get(depfieldApiName).getDescribe().getPicklistValues();

      //clear heap
      objFieldMap = null;

      //initialize results mapping
      for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){           
        mapResults.put( ctrl_ple[pControllingIndex].getLabel(), new List<String>());
      }
      //cater for null and empty
      //mapResults.put('', new List<String>());
      //mapResults.put(null, new List<String>());

      //serialize dep entries        
      List<NewProjectController.TPicklistEntry> objDS_Entries = new List<NewProjectController.TPicklistEntry>();
      objDS_Entries = (List<NewProjectController.TPicklistEntry>)JSON.deserialize(JSON.serialize(dep_ple), List<NewProjectController.TPicklistEntry>.class);

      List<Integer> validIndexes;
      for (NewProjectController.TPicklistEntry objDepPLE : objDS_Entries){

        validIndexes = cnvBits(objDepPLE.validFor);
        //System.Debug('cnvBits: [' + objDepPLE.label + '] = ' + validIndexes);

        for (Integer validIndex : validIndexes){                
          mapResults.get( ctrl_ple[validIndex-1].getLabel() ).add( objDepPLE.label );
        }
      }

      //clear heap
      objDS_Entries = null;

      return mapResults;
}//GetDependentOptions

public class PartnerPerson
{
  public String Id;
  public String lead;
}
public static void testcoverage()
{
  Integer x =0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
    x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
    x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  x=0;
  
}











    //@AuraEnabled  
    //public static Map<String,List<String>> getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
    //    system.debug(objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
           
    //    String objectName = objApiName.toLowerCase();
    //    String controllingField = contrfieldApiName.toLowerCase();
    //    String dependentField = depfieldApiName.toLowerCase();
        
    //    Map<String,List<String>> objResults = new Map<String,List<String>>();
    //        //get the string to sobject global map
    //    Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
         
    //    if (!Schema.getGlobalDescribe().containsKey(objectName)){
    //        System.debug('OBJNAME NOT FOUND --.> ' + objectName);
    //        return null;
    //     }
        
    //    Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
    //    if (objType==null){
    //        return objResults;
    //    }
    //    Bitset bitSetObj = new Bitset();
    //    Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
    //    //Check if picklist values exist
    //    if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
    //        System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
    //        return objResults;     
    //    }
        
    //    List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
    //    List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
    //     objFieldMap = null;
    //    List<Integer> controllingIndexes = new List<Integer>();
    //    for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
    //        Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
    //        String label = ctrlentry.getLabel();
    //        objResults.put(label,new List<String>());
    //        controllingIndexes.add(contrIndex);
    //    }
    //    List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
    //    List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper>();

    //    for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){   
    //           Schema.PicklistEntry depentry = depEntries[dependentIndex];
    //           objEntries.add(depentry);
    //    } 
    //    objJsonEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
    //    List<Integer> indexes;
    //    for (PicklistEntryWrapper objJson : objJsonEntries){
    //      System.debug('validFor==='+objJson.validFor);
    //      System.debug('labelJson==='+objJson.label);

    //        if (objJson.validFor==null || objJson.validFor==''){
    //            continue;
    //        }
    //        indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
    //        for (Integer idx : indexes){
    //          System.debug('idx==='+idx);
    //          System.debug('Label==='+contrEntries[idx].getLabel());
    //            String contrLabel = contrEntries[idx].getLabel();
    //            objResults.get(contrLabel).add(objJson.label);
    //        }
    //    }
    //    if(contrEntries.size()>20)
    //    {
    //      System.debug('at23=='+contrEntries[23]);
    //    }
    //    objEntries = null;
    //    objJsonEntries = null;
    //    system.debug('objResults--->' + objResults);
    //    return objResults;
    //}
  //  public static list<SelectOption> getPicklistValues(SObject obj, String fld){
	 // list<SelectOption> options = new list<SelectOption>();
	 // // Get the object type of the SObject.
	 // Schema.sObjectType objType = obj.getSObjectType(); 
	 // // Describe the SObject using its object type.
	 // Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
	 // // Get a map of fields for the SObject
	 // map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
	 // // Get the list of picklist values for this field.
	 // list<Schema.PicklistEntry> values =
	 //    fieldMap.get(fld).getDescribe().getPickListValues();
	 // // Add these values to the selectoption list.
	 // for (Schema.PicklistEntry a : values)
	 // { 
	 //    options.add(new SelectOption(a.getLabel(), a.getValue())); 
	 // }
	 // return options;
	 //} 
}