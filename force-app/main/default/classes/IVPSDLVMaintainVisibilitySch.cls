/**
 * @author Dharmendra Karamchandani
 * @date 2018-05-09
 * @className IVPSDLVMaintainVisibilitySch
 * @group IVPCompanyListView
 *
 * @description contains methods to handle IVPCompanyListView component
 * 				This is used as Schedulable and Batchable too
 * 				This is used pick listviews and create for that owner if not existing and share the same with that user's group
 * @note we are using the job in two phases
 * 			1. Checking the group and member and creating the same for the users if not exists
 * 			2. Once group and members are established then sharing listview with the respected group
 */
public class IVPSDLVMaintainVisibilitySch implements Schedulable, Database.Batchable <sObject>, 
													Database.AllowsCallouts, Database.Stateful {
	
	public class IVPSDLVMaintainVisibilitySchException extends Exception {}
	
	String jobStartTime;
	Boolean prepareMember;
	
	/*******************************************************************************************************
     * @description used to get listiviews according to criteria and check does any listview available 
     * 				for sharing with that user's owner if found then executing the job
     * 				
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
	public void checkListViewAndExecuteBatch(){
		jobStartTime = String.valueOf(System.now().getTime());
		List<ListView> listviews = Database.query(IVPSDLVMaintainVisibilityService.soqlQeury + ' LIMIT  1');
           	if(!listviews.isEmpty()){
    		Database.executeBatch(this, 40);
    	}
	}
	
	/*******************************************************************************************************
     * @description This is the method which is calling in the Schedulable context
     * 				
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    public void execute(SchedulableContext sc) {
    	prepareMember = false;
    	checkListViewAndExecuteBatch();
    }
	
	/*******************************************************************************************************
     * @description This is the main start method which is used to get the listview for every chunks of the job
     * 				
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
	public Database.QueryLocator start(Database.BatchableContext BC) {
		if(prepareMember == NULL)
			prepareMember = false;
        return Database.getQueryLocator(IVPSDLVMaintainVisibilityService.soqlQeury + ' ORDER BY LastModifiedDate DESC'
        								+ (Test.isRunningTest() ? ' LIMIT 20 ' : ''));
    }
    
    /* @description This is the method which is resposible for every chunks of the job and process the records
     * 				
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    public void execute(Database.BatchableContext BC, List<ListView> scope) {
    	try{
    		Map<Id, String> userIdWithGroupName = IVPSDLVMaintainVisibilityService.prepareUserWithGroupNameMap(scope);
	    	if(prepareMember){
	    		IVPSDLVMaintainVisibilityService.shareListViews(userIdWithGroupName, scope, 'Account');
	    	}
    	}catch(Exception excp){
			System.debug(excp.getStacktraceString() + '='+ excp.getMessage());
    	}
    }
    
    /* @description This is the method which is going to being called when the job is completed
    				in this we are checking if members are being prepared for the users so we are calling this again
					for sharing listviews
     * 				
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    public void finish(Database.BatchableContext BC) {
		if(!prepareMember){
			prepareMember = true;
			Database.executeBatch(this, 40);
		}else if(String.isEmpty(jobStartTime)){
			IVP_General_Config__c ivpGeneralConfig = IVP_General_Config__c.getValues('last-sync-time');
			if(ivpGeneralConfig == NULL){
				ivpGeneralConfig = new IVP_General_Config__c(Name='last-sync-time');
			}
			ivpGeneralConfig.value__c = jobStartTime;
			upsert ivpGeneralConfig;
		}
    }
    
}