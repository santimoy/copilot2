global class temp_AccountWebsiteUpdate implements Database.Batchable<sObject> {
	
	String query = 'Select Id,Website,Website_Domain__c from Account where Website!=null';
	
	global temp_AccountWebsiteUpdate() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Account> accList = (List<Account>)scope;
		List<Account> accountsToUpdate = new List<Account>();
		for( Account a : accList )
        {
                if( String.isNotBlank( a.Website ) )
                {
                    System.Url websiteURL = new System.Url( ( a.Website.startsWithIgnoreCase( 'http' ) ? '' : 'http://' ) + a.Website );
                    String hostURL = websiteURL.getHost();
                    //Strip out www.
                    if(a.Website_Domain__c!=hostURL.toLowerCase().remove( 'www.' ))
                    {
                    	a.Website_Domain__c = hostURL.toLowerCase().remove( 'www.' );
                    	accountsToUpdate.add(a);
                    }
                }
                //else
                //{
                //    a.Website_Domain__c = '';
                //}
        }
		Database.SaveResult[] srList =Database.update(accountsToUpdate, false);
		List<Failed_Website_Updates__c> failedupdates = new List<Failed_Website_Updates__c>();
		// Iterate through each returned result
		for (Database.SaveResult sr : srList) {
		    if (sr.isSuccess()) {
		        // Operation was successful, so get the ID of the record that was processed
		    }
		    else {
		        // Operation failed, so get all errors                
		        for(Database.Error err : sr.getErrors()) {
					Failed_Website_Updates__c failedUp = new Failed_Website_Updates__c(Name=sr.getId());
					String reason = err.getStatusCode() + ': ' + err.getMessage();
					failedUp.Reason__c = (reason.length()>255?reason.substring(0,255):reason);	
					failedupdates.add(failedUp);     
		        }
		    }
		}
		if(!failedupdates.isEmpty())
		{
			insert failedupdates;
		}
		//update accList;
		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}