/*
*	ClearGeoPointeTripFields_Test class : Test class coverage for InvocableMethod class/method ClearGeoPointeTripFields
*
*	Author: Wilson Ng
*	Date:   May 04, 2018
*
*/
@isTest
public class ClearGeoPointeTripFields_Test {
    
    static testMethod void clear_geopointe_tripfields_test() {
        
        // create test users
        Profile p = [ SELECT Id FROM Profile WHERE Name = 'Analyst Full' ];
		User u = new User( Alias = 'standt1',
						   Email='standarduser1@testorg.dev',
						   EmailEncodingKey = 'UTF-8',
						   LastName = 'Testing1',
						   LanguageLocaleKey='en_US',
						   LocaleSidKey = 'en_US',
						   ProfileId = p.Id, 
						   TimeZoneSidKey = 'America/Los_Angeles',
						   UserName = 'analystuser1@testorg.dev' );
						   
		System.runAs( u ) {
		
	        // create test data
	        Id currentUserId = UserInfo.getUserId();
	        list<Account> accts = new list<Account>();
	        for(integer i=0; i<10; i++)
	        	accts.add(new Account(Name='test account'+i, OwnerId=currentUserId, Geopointe_Trip_Message__c='testmessage'+i, Geopoint_Short_Message__c='testshort'+i));
	        insert accts;
			      
	        list<Id> acctIds = new list<Id>();
	        for(Account a : accts)
	        	acctIds.add(a.Id);
	        
	        Test.startTest();
	        	
				ClearGeoPointeTripFields.ClearGeoPointeTripFields(acctIds);
	            
	    	Test.stopTest();
	    	
	    	list<Account> checkaccts = [select Id, Geopointe_Trip_Message__c, Geopoint_Short_Message__c from Account where id=:accts];
	    	system.assertEquals(10, checkaccts.size(), 'Assert query returned incorrect number of account records.');
	    	for(Account a : checkaccts) {
		    	system.assertEquals(null, a.Geopointe_Trip_Message__c, 'Geopointe trip message field did not clear correctly.');
		    	system.assertEquals(null, a.Geopoint_Short_Message__c, 'Geopointe short message field did not clear correctly.');
	    	}
		}
    }
}