/*
Author : Snehil Jaiswal
Description : This class will be used for writing all the functionalities related to Sourcing_Preference__c object. 
Date Created : 13th March 2019
Change 1 : 
*/
public class SourcingPreferenceTriggerHandler {
    
    public static boolean isUpdate = false; 
    //public static boolean isInsert = true; 
    
    // Method for before Insert event
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered
    // @return void.  
    public static void triggerIsBeforeInsert(List<Sourcing_Preference__c> newTriggerList) {
        SourcingPreferenceHelper.setExternalId(newTriggerList);
        SourcingPreferenceHelper.updateIsTemplate(newTriggerList);
    }
    
    
    // Method for before Update event
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered
    // @return void.  
     public static void triggerIsBeforeUpdate(List<Sourcing_Preference__c> newTriggerList) {
        SourcingPreferenceHelper.updateIsTemplate(newTriggerList);
    }
    
    // Method for after Insert event
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered, oldTriggerMap is map of id to offer list before update
    // @return void.  
    public static void triggerIsAfterInsert(List<Sourcing_Preference__c> newTriggerList) {
        if(!isUpdate) {
            isUpdate = true;
            //system.debug(isUpdate);
            SourcingPreferenceHelper.updateSerializedField(null, newTriggerList, true);
        }
    }
    
    // Method for after update event
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered, oldTriggerMap is map of id to offer list before update
    // @return void.  
    public static void triggerIsAfterUpdate(Map<Id,Sourcing_Preference__c> oldTriggerMap, List<Sourcing_Preference__c> newTriggerList) {
        if(!isUpdate) {
            //system.debug(isUpdate);
            isUpdate = true;
            SourcingPreferenceHelper.updateSerializedField(oldTriggerMap, newTriggerList, false);
        }
    }
    
    
}