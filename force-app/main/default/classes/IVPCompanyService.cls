public class IVPCompanyService {
    
    public class IVPCompanyServiceException extends Exception {}
    
	public static void changeAccountOwner(Map<Id, Id> mapOfAccountAndNewOwnerIds){
        Set<Id> accountIds = mapOfAccountAndNewOwnerIds.keySet();
        
    	String query = 'SELECT Id FROM Account WHERE Id IN :accountIds';
        
        List<Account> accounts = new List<Account>();
        
        for(Account accountObj:( List<Account> ) Database.query(query)){
            accountObj.OwnerId = mapOfAccountAndNewOwnerIds.get(accountObj.Id);
            accounts.add(accountObj);
        }
        
        List<Database.SaveResult> accountsSaveResult = Database.update(accounts);
        
        if(!accountsSaveResult[0].isSuccess()){
            throw new IVPCompanyServiceException('An error occurred attempting to change account owner: ' + accountsSaveResult[0].getErrors()[0].getMessage());
        }
    }
}