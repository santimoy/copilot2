/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Krow_Timesheet_DetailTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Krow_Timesheet_DetailTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Krow__Timesheet_Detail__c());
    }
}