/**
* @author 10k advisor
* @date 2018-06-14
* @className IVP_IFL_MarkFeedbackIgnoreBatch
*
* @description Batch & Schedule class to set Accurate_IQ_Score to 'Ingore' of IFL Feedback if they expired 
*/
public class IVP_IFL_MarkFeedbackIgnoreBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{
    private Id recordTypeId;
    public  IVP_IFL_MarkFeedbackIgnoreBatch(){
        List<RecordType> recordTypes = [SELECT 
                                        		Id,Name,DeveloperName 
                                        FROM 	RecordType 
                                        WHERE 	sObjectType='Intelligence_Feedback__c' AND 
                                        		DeveloperName = 'Im_Feeling_Lucky'];
        if(!recordTypes.isEmpty()){
            recordTypeId =recordTypes[0].Id;
        }
    }
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(recordTypeId ==null ){
            return null;
        }
        String query = 'SELECT '+
                            'Id,Name,RecordTypeId,Is_Expired__c,Accurate_IQ_Score__c '+ 
                       'FROM Intelligence_Feedback__c '+
                       'WHERE '+ 
            				'RecordTypeId =:recordTypeId AND '+
                            'Is_Expired__c =true AND '+
                            'Accurate_IQ_Score__c = null';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Intelligence_Feedback__c> scope) {
        for(Intelligence_Feedback__c objIntelligenceFeedback : scope){
 			objIntelligenceFeedback.Accurate_IQ_Score__c = 'Ignore';
        }
        Database.update(scope);
    }  
    
    public void execute(SchedulableContext sc){
        Database.executebatch(new IVP_IFL_MarkFeedbackIgnoreBatch (),2000);
    }
    
    public void finish(Database.BatchableContext BC) {}
}