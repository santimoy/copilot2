@isTest
private class SendToPipeTest {
	
	@isTest static void test_method_one() {
		Account a = new Account(Name ='Testaccount');
		insert a;
        
        SendToPipeController.FinancialData objFData = new SendToPipeController.FinancialData();
        objFData.priorPriorRevenueValue = 1;
        objFData.priorRevenueValue = 1;
		objFData.currentRevenueValue = 1;
		objFData.nextRevenueValue = 1;
		objFData.priorPriorEbitaValue = 1;
        objFData.priorEbitaValue = 1;
		objFData.currentEbitaValue = 1;
		objFData.nextEbitaValue = 1;
		objFData.priorPriorNetValue = 1;
    	objFData.priorNetValue = 1;
		objFData.currentNetValue = 1;
		objFData.nextNetValue = 1;
		objFData.priorPriorGrossValue = 1;
    	objFData.priorGrossValue = 1;
		objFData.currentGrossValue = 1;
		objFData.nextGrossValue = 1;
		SendToPipeController.saveData(a.Id,
			'test opp',
			'test descrip',
			'test street',
			'test city',
			'NJ',
			'United States',
			'10003',
			null,//partnerObject
			null,//oppTeam
			'test',//next action
			null,//nextActionDate
			null,//dealType
			null,//companyquality
			null,//dealsize
			null,//immience
			null,//campitalhistory
			'Prospect',
			JSON.serialize(objFData),
			20,
			'test',
			'12',
			1,
			'Test',
			'ARR',
			'',
			'');
		String accountStr2 = SendToPipeController.fetchInitData(a.Id);
		SendToPipeController.saveData(a.Id,
			'test opp',
			'test descrip',
			'test street',
			'test city',
			'NJ',
			'United States',
			'10003',
			null,//partnerObject
			null,//oppTeam
			'test',//next action
			null,//nextActionDate
			null,//dealType
			null,//companyquality
			null,//dealsize
			null,//immience
			null,//campitalhistory
			'Prospect',
			JSON.serialize(objFData),
			20,
			'test',
			'12',
			1,
			'Test',
			'ARR',
			'',
			'');

		/*saveData(String recordId,String oppName,
    	String oppDescription,String accShippingStreet,String accShippingCity,
    	String accShippingState,String accShippingCountry,String accShippingPostalCode,String partnerObject,
    	String opportunityTeamMembers,String nextAction,String nextActionDate,
    	String dealTypeValue,String companyQualityValue,String dealSizeValue,String dealImminenceValue,
    	String capitalHistoryValue,String stageValue,Decimal priorRevenueValue,
    	Decimal currentRevenueValue,Decimal nextRevenueValue,Decimal priorEbitaValue,
    	Decimal currentEbitaValue,Decimal nextEbitaValue,Decimal totalFundingRaisedValue,
    	String inboundNameOfBank,String interest,Decimal numberOfEmployees,String otherKPI, Decimal priorPriorRevenueValue, Decimal priorPriorEbitaValue,String strRevenueType)*/

	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}