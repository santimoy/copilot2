/**
 *@name : IVPOwnProcessExpiredRequestBtch
 *@description : Process nearby expired company request
**/
public class IVPOwnProcessExpiredRequestBtch implements Database.Batchable<sObject>, Schedulable{
    
    public void execute(SchedulableContext sc){
        Logger.push('Schedulable.execute','IVPOwnProcessExpiredRequestBtch');
        Database.executeBatch(new IVPOwnProcessExpiredRequestBtch());
        Logger.pop();
        
    }
    
    /***********************************************************************
     * @author 10K-Expert
     * @description prepare filters and return Database.QueryLocator with respect to Company_Request__c records
     * @return Database.QueryLocator
     * @param nothing
     ***********************************************************************/
    public Database.QueryLocator start(Database.BatchableContext BC){
        Datetime currentDateTime = System.now();
        String queryStr = 'SELECT Id, Company__c, Request_By__c, Request_To__c, Request_Expire_On__c, Status__c, Comment_By_Owner__c, Comment_By_Requester__c '
            +'FROM Company_Request__c WHERE Request_Expire_On__c <= :currentDateTime AND Status__c = \'Pending\'';
        return Database.getQueryLocator(queryStr);
    }
    
    
    /***********************************************************************
     * @author 10K-Expert
     * @description basic chunk executer of the job,
     *      process requests and changes status with Tansfer - no Response
     * @return void
     * @param nothing
     ***********************************************************************/
    public void execute(Database.BatchableContext BC, List<Company_Request__c> companyRequestLst){
        Logger.push('Batchable.execute','IVPOwnProcessExpiredRequestBtch');
        processRecords(companyRequestLst);
        Logger.pop();
    }
    
    /***********************************************************************
     * @author 10K-Expert
     * @description process Company_Request record and change status to Tansfer - No Response
     * @return void
     * @param List Company_Request record
     ***********************************************************************/
     private static void processRecords(List<Company_Request__c> companyRequestLst){
         Logger.push('processRecords','IVPOwnProcessExpiredRequestBtch');
         /************
        * Checking if the user having bypass and if yes then we are bypassing the No Action Required
          by not changing the Status.
    ************/
         Set<Id> userIds = new Set<Id>();
         for( Company_Request__c companyRequest : companyRequestLst ){
             userIds.add(companyRequest.Request_To__c);
         }
         List<Company_Request__c> newCompanyList =  new List<Company_Request__c>();
         //Checking if the users having bypass
         Map<Id,boolean> usersWithBypass = IVPOwnershipService.checkUserHavingBypass(userIds);
         for( Company_Request__c companyRequest : companyRequestLst ){
             if(usersWithBypass.containsKey(companyRequest.Request_To__c) && usersWithBypass.get(companyRequest.Request_To__c)!=Null)
             {
                 if(usersWithBypass.get(companyRequest.Request_To__c))
                     continue;
                 else
                 {
                     companyRequest.Status__c = 'Transfer - No Response';
                     newCompanyList.add(companyRequest);
                 }
             }
             
             else
             {
                 companyRequest.Status__c = 'Transfer - No Response';
                 newCompanyList.add(companyRequest);
             }
                 
         }
        /*for( Company_Request__c companyRequest : companyRequestLst ){
            companyRequest.Status__c = 'Transfer - No Response';
    }*/
        Boolean isPopLogger = false;
        if(!newCompanyList.isEmpty()){
            List<Database.SaveResult> results = Database.update(newCompanyList,false);
            System.debug(results);
            for(Integer ind = 0, len=results.size(); ind < len; ind++){
                if(!results[ind].isSuccess()){
                    isPopLogger = true;
                    Logger.debug('Request:'+newCompanyList[ind].Id+',Message:'+results[ind].getErrors()[0].getMessage());
                }
            }
        }
        if(isPopLogger){
            System.debug(isPopLogger);
            Logger.pop();
        }
    }
    
    public void finish(Database.BatchableContext BC){
        
    }

}