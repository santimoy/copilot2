/**
 * @author MukeshKS-Concretio
 * @date 2018-05-18
 * @className IVPFeelingLuckyService_Test
 * @group FeelingLucky
 *
 * @description contains test cases
 */
@isTest
public class IVPFeelingLuckyService_Test {
    
    private static testmethod  void testGetFeelingLuckyList(){

		Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
		
		List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);
        
		Test.startTest();
            
            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new IVPFeelingLuckyMock());
        
			IVPFeelingLuckyWrapper feelingLuckyData = IVPFeelingLuckyService.getFeelingLuckyList(true);

			List<Intelligence_Feedback__c> testUserIFLRecords = feelingLuckyData.iflRecords;

			System.assertEquals(5, testUserIFLRecords.size());

		Test.stopTest();
	}
	
	private static testmethod void testSaveIFLFeedbackForAccept(){
	    
	    Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
        
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);

		Test.startTest();
		    
		    List<Intelligence_Feedback__c> testUserIFLRecords = [SELECT Id, Accurate_IQ_Score__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE User__c = :currentUserId AND Accurate_IQ_Score__c = null];
		    
		    System.assertEquals(5, testUserIFLRecords.size());
		    System.assertEquals(null, testUserIFLRecords[0].Accurate_IQ_Score__c);
		    System.assertNotEquals(null, testUserIFLRecords[0].Company_Evaluated__r.IFL_Load_Date__c);
		    System.assertEquals(False, testUserIFLRecords[0].Is_Expired__c);
		    System.assertEquals(currentUserId, testUserIFLRecords[0].User__c);
		    System.assertEquals(IVPUtils.iFLRecordTypeId, testUserIFLRecords[0].RecordTypeId);
		    System.assertEquals(unassignedUserId, testUserIFLRecords[0].Company_Evaluated__r.OwnerId);
		    
		    // reset IVPIntelligenceFeedbackTrigger runonce 
		    checkRecursive.reset('IVPIntelligenceFeedbackTrigger');
		    
            testUserIFLRecords[0].Accurate_IQ_Score__c = 'Accept';
			IVPResponse testResponse = IVPFeelingLuckyService.saveIFLFeedback(testUserIFLRecords[0]);

		Test.stopTest();
		
		List<Intelligence_Feedback__c> testUserIFLRecords2 = [SELECT Id, Accurate_IQ_Score__c,IQ_Score_at_Update__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Company_Evaluated__r.IQ_Score__c,Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE Id = :testUserIFLRecords[0].Id]; 
		    
	    System.assertEquals('Accept', testUserIFLRecords2[0].Accurate_IQ_Score__c);
	    System.assertEquals(currentUserId, testUserIFLRecords2[0].Company_Evaluated__r.OwnerId);
	    System.assertEquals(testUserIFLRecords2[0].Company_Evaluated__r.IQ_Score__c , testUserIFLRecords2[0].IQ_Score_at_Update__c);
        
		System.assertEquals('Accept', testResponse.code);
	}
	
	private static testmethod void testSaveIFLFeedbackForReject(){
	    
	    Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
	    
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);

		Test.startTest();
		    
		    List<Intelligence_Feedback__c> testUserIFLRecords = [SELECT Id, Accurate_IQ_Score__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE User__c = :currentUserId AND Accurate_IQ_Score__c = null];
		    
		    System.assertEquals(5, testUserIFLRecords.size());
		    System.assertEquals(null, testUserIFLRecords[0].Accurate_IQ_Score__c);
		    System.assertNotEquals(null, testUserIFLRecords[0].Company_Evaluated__r.IFL_Load_Date__c);
		    System.assertEquals(False, testUserIFLRecords[0].Is_Expired__c);
		    System.assertEquals(currentUserId, testUserIFLRecords[0].User__c);
		    System.assertEquals(IVPUtils.iFLRecordTypeId, testUserIFLRecords[0].RecordTypeId);
		    System.assertEquals(unassignedUserId, testUserIFLRecords[0].Company_Evaluated__r.OwnerId);
		    
		    // reset IVPIntelligenceFeedbackTrigger runonce 
		    checkRecursive.reset('IVPIntelligenceFeedbackTrigger');
		    
            testUserIFLRecords[0].Accurate_IQ_Score__c = 'Reject';
			IVPResponse testResponse = IVPFeelingLuckyService.saveIFLFeedback(testUserIFLRecords[0]);
            
            List<Intelligence_Feedback__c> testUserIFLRecords2 = [SELECT Id, Accurate_IQ_Score__c,IQ_Score_at_Update__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Company_Evaluated__r.IQ_Score__c,Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE Id = :testUserIFLRecords[0].Id]; 
		    
		    System.assertEquals('Reject', testUserIFLRecords2[0].Accurate_IQ_Score__c);
		    System.assertEquals(unassignedUserId, testUserIFLRecords2[0].Company_Evaluated__r.OwnerId);
		    System.assertEquals(testUserIFLRecords2[0].Company_Evaluated__r.IQ_Score__c , testUserIFLRecords2[0].IQ_Score_at_Update__c);
            
			System.assertEquals('Reject', testResponse.code);
			

		Test.stopTest();
	}
	
	private static testmethod void testSaveIFLFeedbackForIgnoreIfUserAccept(){
	    
	    Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(10, unassignedUserId);
	    
	    List<Account> testCompaniesToAssign = new List<Account>();
	    
	    for(Integer i = 0; i < 5; i++ ) {
	        testCompaniesToAssign.add(testCompanies.get(i));
	    }
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompaniesToAssign, currentUserId);
	    
	    // update company owner to another user
	    IVPTestFuel.updateCompaniesOwner(new List<Account>{testCompaniesToAssign[0]}, currentUserId);

		Test.startTest();
		    
		    List<Intelligence_Feedback__c> testUserIFLRecords = [SELECT Id, Accurate_IQ_Score__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE User__c = :currentUserId AND Accurate_IQ_Score__c = null];
		    
		    System.assertEquals(5, testUserIFLRecords.size());
		    System.assertEquals(null, testUserIFLRecords[0].Accurate_IQ_Score__c);
		    System.assertNotEquals(null, testUserIFLRecords[0].Company_Evaluated__r.IFL_Load_Date__c);
		    System.assertEquals(False, testUserIFLRecords[0].Is_Expired__c);
		    System.assertEquals(currentUserId, testUserIFLRecords[0].User__c);
		    System.assertEquals(IVPUtils.iFLRecordTypeId, testUserIFLRecords[0].RecordTypeId);
		    System.assertNotEquals(unassignedUserId, testUserIFLRecords[0].Company_Evaluated__r.OwnerId);
		    
		    // reset IVPIntelligenceFeedbackTrigger runonce 
		    checkRecursive.reset('IVPIntelligenceFeedbackTrigger');
		    
            testUserIFLRecords[0].Accurate_IQ_Score__c = 'Accept';
			IVPResponse testResponse = IVPFeelingLuckyService.saveIFLFeedback(testUserIFLRecords[0]);
            
            List<Intelligence_Feedback__c> testUserIFLRecords2 = [SELECT Id, Accurate_IQ_Score__c,IQ_Score_at_Update__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Company_Evaluated__r.IQ_Score__c,Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE Id = :testUserIFLRecords[0].Id]; 
		    
		    System.assertEquals('Ignore', testUserIFLRecords2[0].Accurate_IQ_Score__c);
		    System.assertNotEquals(unassignedUserId, testUserIFLRecords2[0].Company_Evaluated__r.OwnerId);
		    System.assertEquals(null , testUserIFLRecords2[0].IQ_Score_at_Update__c);
            
			System.assertEquals('Ignore', testResponse.code);
			
			IVPFeelingLuckyWrapper iFLData =  (IVPFeelingLuckyWrapper) testResponse.data;
			List<Intelligence_Feedback__c> remaniningIFLRecords = iFLData.iflRecords;
			System.assertEquals(5, remaniningIFLRecords.size());
			

		Test.stopTest();
	}
	
	private static testmethod void testSaveIFLFeedbackForIgnoreIfUserReject(){
	    
	    Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(10, unassignedUserId);
	    
	    List<Account> testCompaniesToAssign = new List<Account>();
	    
	    for(Integer i = 0; i < 5; i++ ) {
	        testCompaniesToAssign.add(testCompanies.get(i));
	    }
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompaniesToAssign, currentUserId);
	    
	    // update company owner to another user
	    IVPTestFuel.updateCompaniesOwner(new List<Account>{testCompaniesToAssign[0]}, currentUserId);

		Test.startTest();
		    
		    List<Intelligence_Feedback__c> testUserIFLRecords = [SELECT Id, Accurate_IQ_Score__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE User__c = :currentUserId AND Accurate_IQ_Score__c = null];
		    
		    System.assertEquals(5, testUserIFLRecords.size());
		    System.assertEquals(null, testUserIFLRecords[0].Accurate_IQ_Score__c);
		    System.assertNotEquals(null, testUserIFLRecords[0].Company_Evaluated__r.IFL_Load_Date__c);
		    System.assertEquals(False, testUserIFLRecords[0].Is_Expired__c);
		    System.assertEquals(currentUserId, testUserIFLRecords[0].User__c);
		    System.assertEquals(IVPUtils.iFLRecordTypeId, testUserIFLRecords[0].RecordTypeId);
		    System.assertNotEquals(unassignedUserId, testUserIFLRecords[0].Company_Evaluated__r.OwnerId);
		    
		    // reset IVPIntelligenceFeedbackTrigger runonce 
		    checkRecursive.reset('IVPIntelligenceFeedbackTrigger');
		    
            testUserIFLRecords[0].Accurate_IQ_Score__c = 'Reject';
			IVPResponse testResponse = IVPFeelingLuckyService.saveIFLFeedback(testUserIFLRecords[0]);
            
            List<Intelligence_Feedback__c> testUserIFLRecords2 = [SELECT Id, Accurate_IQ_Score__c,IQ_Score_at_Update__c, Company_Evaluated__c,Company_Evaluated__r.OwnerId,
		     Company_Evaluated__r.IFL_Load_Date__c, Company_Evaluated__r.IQ_Score__c,Is_Expired__c, RecordTypeId, User__c FROM Intelligence_Feedback__c 
		    WHERE Id = :testUserIFLRecords[0].Id]; 
		    
		    System.assertEquals('Ignore', testUserIFLRecords2[0].Accurate_IQ_Score__c);
		    System.assertNotEquals(unassignedUserId, testUserIFLRecords2[0].Company_Evaluated__r.OwnerId);
		    System.assertEquals(null , testUserIFLRecords2[0].IQ_Score_at_Update__c);
            
			System.assertEquals('Ignore', testResponse.code);
			
			IVPFeelingLuckyWrapper iFLData =  (IVPFeelingLuckyWrapper) testResponse.data;
			List<Intelligence_Feedback__c> remaniningIFLRecords = iFLData.iflRecords;
			System.assertEquals(5, remaniningIFLRecords.size());
			

		Test.stopTest();
	}
	
	private static testmethod void testRunIFLBatch() {
	    IVPTestFuel obj = new IVPTestFuel();
        User[] users = obj.users;
        System.runAs(users[1]) {
            Id unassignedUserId = IVPUtils.unassignedUserId;
            
            List<Account> testCompanies =  IVPTestFuel.createTestCompanies(25, unassignedUserId);
            
            User testUser1 = IVPTestFuel.createTestUser('TestLName-1', true);
            User testUser2 = IVPTestFuel.createTestUser('TestLName-2', true);
            User testUser3 = IVPTestFuel.createTestUser('TestLName-3', true);
            User testUser4 = IVPTestFuel.createTestUser('TestLName-4', true);
            
            Set<Id> testUserIds = new Set<Id>{testUser1.Id, testUser2.Id,testUser3.Id,testUser4.Id};
            
            //System.debug('testUserIds>>'+testUserIds);
            
            IVPTestFuel.assignTestUsersToTestPublicGroup(IVPTestFuel.createTestPublicGroup('Test IVP IFL User Group'), testUserIds);
            //System.debug('testUsers>>'+testUsers);
	       
            Test.startTest();
            
                IVPFeelingLuckyService.runIFLBatch();
                
            Test.stopTest();   
            Id iFLRecordTypeId = IVPUtils.iFLRecordTypeId;
            
            Map<Id, Integer> testUserIFLCountMap = new Map<Id, Integer>();
            for(Intelligence_Feedback__c testIFLRecord:[SELECT Id, User__c  
                                                            FROM Intelligence_Feedback__c 
                                                            WHERE Is_Expired__c = False AND User__c IN :testUserIds 
                                                            AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId
                                                            AND Company_Evaluated__r.OwnerId = :unassignedUserId]) {
                
                if(testUserIFLCountMap.containsKey(testIFLRecord.User__c)) {
                    Integer testIFLCount = testUserIFLCountMap.get(testIFLRecord.User__c);
                    testUserIFLCountMap.put(testIFLRecord.User__c, testIFLCount+1 );
                } else {
                    testUserIFLCountMap.put(testIFLRecord.User__c, 1);
                } 
                                                                
            }
            System.assertEquals(5, testUserIFLCountMap.get(testUser1.Id));
            System.assertEquals(5, testUserIFLCountMap.get(testUser2.Id));
            System.assertEquals(5, testUserIFLCountMap.get(testUser3.Id));
            System.assertEquals(5, testUserIFLCountMap.get(testUser4.Id));
	    }
	}
	
	private static testmethod void testRunIFLBatchAfterUsersRejectIFLs() {
	    
        IVPTestFuel obj = new IVPTestFuel();
        User[] users = obj.users;
        System.runAs(users[1]) {
            Id unassignedUserId = IVPUtils.unassignedUserId;
            List<Account> testNewCompanies =  IVPTestFuel.createTestCompanies(30, unassignedUserId);
            
            User testUser1 = IVPTestFuel.createTestUser('TestLName-1', true);
            User testUser2 = IVPTestFuel.createTestUser('TestLName-2', true);
            User testUser3 = IVPTestFuel.createTestUser('TestLName-3', true);
            User testUser4 = IVPTestFuel.createTestUser('TestLName-4', true);
            User testUser5 = IVPTestFuel.createTestUser('TestLName-5', true);
            
            Set<Id> testUserIds = new Set<Id>{testUser1.Id, testUser2.Id,testUser3.Id,testUser4.Id,testUser5.Id};
            
            //System.debug('testUserIds>>'+testUserIds);
            
            IVPTestFuel.assignTestUsersToTestPublicGroup(IVPTestFuel.createTestPublicGroup('Test IVP IFL User Group'), testUserIds);
            
            Id currentUserId = UserInfo.getUserId();
	    
    	    
    	    
    	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
            
    	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, testUser1.Id);
    	    
    	    List<Account> testCompanies2 = IVPTestFuel.createTestCompanies(5, unassignedUserId);
            
    	    List<Intelligence_Feedback__c> testIFLRecords2 = IVPTestFuel.createTestIFLRecords(testCompanies2, testUser2.Id);
    	    
    	    //List<Account> testCompanies3 = IVPTestFuel.createTestCompanies(5, unassignedUserId);
    	    //List<Intelligence_Feedback__c> testIFLRecords3 = IVPTestFuel.createTestIFLRecords(testCompanies3, testUser3.Id);
    	    
    	    Id iFLRecordTypeId = IVPUtils.iFLRecordTypeId;
                    
            List<Intelligence_Feedback__c> testIFLRecordsToUpdate = new List<Intelligence_Feedback__c>();
           
            
            Integer i = 0; Integer j = 0; 
            for(Intelligence_Feedback__c testIFLRecord:[SELECT Id, User__c  
                                                            FROM Intelligence_Feedback__c 
                                                            WHERE Is_Expired__c = False AND User__c IN :testUserIds 
                                                            AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId
                                                            AND Company_Evaluated__r.OwnerId = :unassignedUserId]) {
                
                
                testIFLRecord.Accurate_IQ_Score__c = 'Reject';
                
                testIFLRecordsToUpdate.add(testIFLRecord); 
                i++;
                j++;
            }
            
            update testIFLRecordsToUpdate;
            
            
	       
            Test.startTest();
            
                IVPFeelingLuckyService.runIFLBatch();
                
            Test.stopTest();   
            
            Map<Id, Integer> testUserIFLCountMap = new Map<Id, Integer>();
            for(Intelligence_Feedback__c testIFLRecord:[SELECT Id, User__c  
                                                            FROM Intelligence_Feedback__c 
                                                            WHERE Is_Expired__c = False AND User__c IN :testUserIds 
                                                            AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId
                                                            AND Company_Evaluated__r.OwnerId = :unassignedUserId]) {
                
                if(testUserIFLCountMap.containsKey(testIFLRecord.User__c)) {
                    Integer testIFLCount = testUserIFLCountMap.get(testIFLRecord.User__c);
                    testUserIFLCountMap.put(testIFLRecord.User__c, testIFLCount+1 );
                } else {
                    testUserIFLCountMap.put(testIFLRecord.User__c, 1);
                } 
                                                                
            }
            System.assertEquals(5, testUserIFLCountMap.get(testUser1.Id));
            System.assertEquals(5, testUserIFLCountMap.get(testUser2.Id));
            //System.assertEquals(5, testUserIFLCountMap.get(testUser3.Id));
        }
	}
	
	private static testmethod void testRunIFLBatchAfterUserAcceptANDRejectIFLs() {
	    
        IVPTestFuel obj = new IVPTestFuel();
        User[] users = obj.users;
        System.runAs(users[1]) {
            Id unassignedUserId = IVPUtils.unassignedUserId;
            List<Account> testNewCompanies =  IVPTestFuel.createTestCompanies(25, unassignedUserId);
            
            User testUser1 = IVPTestFuel.createTestUser('TestLName-1', true);
            User testUser2 = IVPTestFuel.createTestUser('TestLName-2', true);
            User testUser3 = IVPTestFuel.createTestUser('TestLName-3', true);
            User testUser4 = IVPTestFuel.createTestUser('TestLName-4', true);
            
            Set<Id> testUserIds = new Set<Id>{testUser1.Id, testUser2.Id,testUser3.Id,testUser4.Id};
            
            //System.debug('testUserIds>>'+testUserIds);
            
            IVPTestFuel.assignTestUsersToTestPublicGroup(IVPTestFuel.createTestPublicGroup('Test IVP IFL User Group'), testUserIds);
            
            Id currentUserId = UserInfo.getUserId();
	    
    	    
    	    
    	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
            
    	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, testUser1.Id);
    	    
    	    List<Account> testCompanies2 = IVPTestFuel.createTestCompanies(5, unassignedUserId);
            
    	    List<Intelligence_Feedback__c> testIFLRecords2 = IVPTestFuel.createTestIFLRecords(testCompanies2, testUser2.Id);
    	    
    	    //List<Account> testCompanies3 = IVPTestFuel.createTestCompanies(5, unassignedUserId);
    	    //List<Intelligence_Feedback__c> testIFLRecords3 = IVPTestFuel.createTestIFLRecords(testCompanies3, testUser3.Id);
    	    
    	    Id iFLRecordTypeId = IVPUtils.iFLRecordTypeId;
                    
            List<Intelligence_Feedback__c> testIFLRecordsToUpdate = new List<Intelligence_Feedback__c>();
           
            
            Integer i = 0; Integer j = 0; 
            for(Intelligence_Feedback__c testIFLRecord:[SELECT Id, User__c  
                                                            FROM Intelligence_Feedback__c 
                                                            WHERE Is_Expired__c = False AND User__c IN :testUserIds 
                                                            AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId
                                                            AND Company_Evaluated__r.OwnerId = :unassignedUserId]) {
                
                if(Math.mod(i, 4) == 0 && i > 0) {
                    j = 0;
                    continue;
                }
                
                String feedback = ( Math.mod(i, 2) == 0 ) ? 'Accept' : 'Reject';
                
                testIFLRecord.Accurate_IQ_Score__c = feedback;
                
                testIFLRecordsToUpdate.add(testIFLRecord); 
                i++;
                j++;
            }
            
            update testIFLRecordsToUpdate;
            
            
	       
            Test.startTest();
            
                IVPFeelingLuckyService.runIFLBatch();
                
            Test.stopTest();   
            
            Map<Id, Integer> testUserIFLCountMap = new Map<Id, Integer>();
            for(Intelligence_Feedback__c testIFLRecord:[SELECT Id, User__c  
                                                            FROM Intelligence_Feedback__c 
                                                            WHERE Is_Expired__c = False AND User__c IN :testUserIds 
                                                            AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId
                                                            AND Company_Evaluated__r.OwnerId = :unassignedUserId]) {
                
                if(testUserIFLCountMap.containsKey(testIFLRecord.User__c)) {
                    Integer testIFLCount = testUserIFLCountMap.get(testIFLRecord.User__c);
                    testUserIFLCountMap.put(testIFLRecord.User__c, testIFLCount+1 );
                } else {
                    testUserIFLCountMap.put(testIFLRecord.User__c, 1);
                } 
                                                                
            }
            System.assertEquals(5, testUserIFLCountMap.get(testUser1.Id));
            System.assertEquals(5, testUserIFLCountMap.get(testUser2.Id));
            //System.assertEquals(5, testUserIFLCountMap.get(testUser3.Id));
        }
	}
	
    private static testmethod void testIsValidIFLRecordForValid() {
        
        Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
	    
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);
	    Test.startTest();
	        Boolean isValid = IVPFeelingLuckyService.isValidIFLRecord(testIFLRecords[0].Id);
	    Test.stopTest();
	    System.assert(isValid);
    }
    
    private static testmethod void testIsValidIFLRecordInvalid() {
        
        Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
	    
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);
	    testIFLRecords[0].Accurate_IQ_Score__c = 'Reject';
	    update testIFLRecords[0];
	    
	    Test.startTest();
	        Boolean isValid = IVPFeelingLuckyService.isValidIFLRecord(testIFLRecords[0].Id);
	    Test.stopTest();
	    System.assertEquals(False, isValid);
    }
    
    private static testmethod void testIsValidIFLRecordInvalidIfIFLRecord7DaysOlder() {
        
        Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
	    
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);
	    testIFLRecords[0].Assignment_Date_Time__c = Date.today().addDays(-7);
	    update testIFLRecords[0];
	    
	    Test.startTest();
	        Boolean isValid = IVPFeelingLuckyService.isValidIFLRecord(testIFLRecords[0].Id);
	    Test.stopTest();
	    
	    System.assertEquals(False, isValid);
	    Intelligence_Feedback__c testIFLRecord = [SELECT Id, Is_Expired__c FROM Intelligence_Feedback__c WHERE Id = :testIFLRecords[0].Id];
	    System.assertEquals(true, testIFLRecord.Is_Expired__c);
    }
    
    private static testmethod void testIsValidIFLRecordInvalidIfIFLRecordAlreadyResponded() {
        
        Id currentUserId = UserInfo.getUserId();
	    
	    Id unassignedUserId = IVPUtils.unassignedUserId;
	    
	    List<Account> testCompanies = IVPTestFuel.createTestCompanies(5, unassignedUserId);
	    
	    
	    List<Intelligence_Feedback__c> testIFLRecords = IVPTestFuel.createTestIFLRecords(testCompanies, currentUserId);
	    
	    Map<Id, Intelligence_Feedback__c> testIFLRecordsToUpdateMap = new Map<Id, Intelligence_Feedback__c>();
	    
	    testIFLRecords[0].Accurate_IQ_Score__c = 'Reject';
	    testIFLRecordsToUpdateMap.put(testIFLRecords[0].Id ,testIFLRecords[0]);
	    
	    testIFLRecords[1].Accurate_IQ_Score__c = 'Accept';
	    testIFLRecordsToUpdateMap.put(testIFLRecords[1].Id ,testIFLRecords[1]);
	    
	    testIFLRecords[2].Accurate_IQ_Score__c = 'Ignore';
	    testIFLRecordsToUpdateMap.put(testIFLRecords[2].Id ,testIFLRecords[2]);
	    
	    update testIFLRecordsToUpdateMap.values();
	    
	    Test.startTest();
	        Boolean isValid0 = IVPFeelingLuckyService.isValidIFLRecord(testIFLRecords[0].Id);
	        Boolean isValid1 = IVPFeelingLuckyService.isValidIFLRecord(testIFLRecords[1].Id);
	        Boolean isValid2 = IVPFeelingLuckyService.isValidIFLRecord(testIFLRecords[2].Id);
	    Test.stopTest();
	    
	    System.assertEquals(False, isValid0);
	    System.assertEquals(False, isValid1);
	    System.assertEquals(False, isValid2);
	    
	    List<Intelligence_Feedback__c> testIFLRecordsToAssert = [SELECT Id, Accurate_IQ_Score__c FROM Intelligence_Feedback__c WHERE Id = :testIFLRecordsToUpdateMap.keySet()];
	    System.assertNotEquals(null, testIFLRecordsToAssert[0].Accurate_IQ_Score__c);
	    System.assertNotEquals(null, testIFLRecordsToAssert[1].Accurate_IQ_Score__c);
	    System.assertNotEquals(null, testIFLRecordsToAssert[2].Accurate_IQ_Score__c);
    }
    
    
}