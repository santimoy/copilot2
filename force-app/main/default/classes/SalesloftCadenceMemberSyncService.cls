/**
 * @author  : 10k-Advisor
 * @name    : SalesloftCadenceMemberSyncService 
 * @date    : 2018-08-10
 * @description   : The class used as service to handle the process related to Salesloft CadenceMember object
**/
public class SalesloftCadenceMemberSyncService implements SalesLoftProcessDomain{
    
    public class SalesloftCadenceMemberSyncServiceException extends Exception{}
    
    public void processHTTPResponse(HttpResponse response){
        Salesloft.CadenceMemberResponse cmemberData = (Salesloft.CadenceMemberResponse)JSON.deserialize(
                                                    response.getBody(), 
                                                    Salesloft.CadenceMemberResponse.Class);
        if(cmemberData!=NULL){
            Integer next_page;
            if(cmemberData.metadata!=NULL){
                if(cmemberData.metadata.paging!=NULL){
                    next_page = cmemberData.metadata.paging.next_page = cmemberData.metadata.paging.next_page;
                }
            }
            if(cmemberData.data!=NULL){
                processData(cmemberData.data);
            }
            if(next_page!=null){
                System.enqueueJob(new SalesLoftSyncQ('cadencemember', Salesloft.CADENCE_MEMBER_END_POINT, next_page, SalesLoftSyncQ.startSyncDateTime));
            }else{
                SalesloftUtility.prepareLastSyncDate(SalesLoftSyncQ.startSyncDateTime);
                SalesloftUtility.removeSetting(new Set<String>{'salesloft-job-running'});
                // need to execute SalesloftCadenceMemberSyncSchd job to sync CadenceMember with no contact/Cadence values
                if(!Test.isRunningTest()){
                    //querying CadenceMember to check any CadenceMember is exist without contact/cadence data
                    List<Salesloft_Cadence_Member__c> cadMembers = [SELECT Id FROM Salesloft_Cadence_Member__c WHERE Contact__c = NULL OR Cadence__c = NULL LIMIT 1];
                    if(!cadMembers.isEmpty()){
                	    Database.executeBatch(new SalesloftCadenceMemberSyncSchd('people'),100);
            	    }
                }
            }
        }
    }
    
    public void processData(List<Salesloft.CadenceMember> cadenceMembers){
        Set<String> personIds = new Set<String>();
        Set<String> cadenceIds = new Set<String>();
        
        for(Salesloft.CadenceMember cMember : cadenceMembers){
            if(cMember.person !=NULL){
                personIds.add(cMember.person.id);
            }
            if(cMember.cadence !=NULL){
                cadenceIds.add(cMember.cadence.id);
            }
        }
        
        Map<String, String> conWithSFIdMap = new Map<String, String>();
        Map<String, String> cadWithSFIdMap = new Map<String, String>();
        
        for(List<Contact> contacts : [SELECT Id, Salesloft_Id__c FROM Contact WHERE Salesloft_Id__c in :personIds]){
            for(Contact con: contacts){
                if(!conWithSFIdMap.containsKey(con.Salesloft_Id__c)){
                    conWithSFIdMap.put(con.Salesloft_Id__c, con.Id);
                }
            }
        }
        
        for(List<Salesloft_Cadence__c> cadences : [SELECT Id, Salesloft_Id__c FROM Salesloft_Cadence__c 
                                                        WHERE Salesloft_Id__c in :cadenceIds]){
            for(Salesloft_Cadence__c cad: cadences){
                if(!cadWithSFIdMap.containsKey(cad.Salesloft_Id__c)){
                    cadWithSFIdMap.put(cad.Salesloft_Id__c, cad.Id);
                }
            }
        }
        
        Map<String, Salesloft_Cadence_Member__c> cMembersToInsert = new Map<String, Salesloft_Cadence_Member__c>();
        
        for(Salesloft.CadenceMember cMember : cadenceMembers){
            String personId = '';
            String cadenceId = '';
            
            Salesloft_Cadence_Member__c tempCadMember = new Salesloft_Cadence_Member__c(Salesloft_Id__c = cMember.id);
            
            if(cMember.person!=NULL){
                personId = cMember.person.id;
                if(conWithSFIdMap.containsKey(personId)){
                    tempCadMember.Contact__c = conWithSFIdMap.get(personId);
                }
            }
            
            if(cMember.cadence!=NULL){
                cadenceId = cMember.cadence.id;
                if(cadWithSFIdMap.containsKey(cadenceId)){
                    tempCadMember.Cadence__c = cadWithSFIdMap.get(cadenceId);
                }
            }
            if(personId!='' && cadenceId!=''){
                tempCadMember.Salesloft_People_Id__c = personId;
                tempCadMember.Salesloft_Cadence_Id__c = cadenceId;
                cMembersToInsert.put(cMember.id, tempCadMember);
            }
        }
        try{
            if(!cMembersToInsert.isEmpty()){
                upsert cMembersToInsert.values() Salesloft_Id__c;
            }
        }catch(Exception ex){
            SalesloftUtility.notifyToUser('Salesloft', ex);
        }
    }
    
    Public static void pushCadenceMember(String contactId, String cadenceName){
        cadenceName = cadenceName.trim();
        List<Salesloft_Cadence_Member__c> cadMembers = [SELECT Id FROM Salesloft_Cadence_Member__c
                                                            WHERE Cadence__r.Name=:cadenceName
                                                        AND Contact__c=:contactId];
        if(!cadMembers.isEmpty()){
            throw new SalesloftCadenceMemberSyncServiceException(Label.IVP_Salesloft_Cadence_Member_Exist_Message);
        }
        String cadenceId = '';
        String personId = '';
        String ownerId = '';
        List<Contact> contacts = [SELECT Id, Salesloft_Id__c FROM Contact
                                    WHERE id =:contactId AND Salesloft_Id__c!=NULL ];
        
        if(!contacts.isEmpty()){
            Salesloft_Cadence_Member__c cadMember = new Salesloft_Cadence_Member__c();
            cadMember.Contact__c = contacts[0].Id;
            cadMember.Salesloft_People_Id__c = contacts[0].Salesloft_Id__c;
            personId = contacts[0].Salesloft_Id__c;
            List<Salesloft_Cadence__c> cadences = [SELECT Id, Salesloft_Id__c FROM Salesloft_Cadence__c
                                                    WHERE Name =:cadenceName  AND Salesloft_Id__c!=NULL];

            Map<String, String> sfConIdWithId = new Map<String, String>();
            
            if(!cadences.isEmpty()){
                sfConIdWithId.put(cadences[0].Salesloft_Id__c, cadences[0].Id);
                cadenceId = cadences[0].Salesloft_Id__c;
                cadMember.Cadence__c = cadences[0].Id;
                cadMember.Salesloft_Cadence_Id__c = cadences[0].Salesloft_Id__c;
            }else{
                throw new SalesloftCadenceMemberSyncServiceException(Label.IVP_Salesloft_Cadence_Not_Exist_Message);
            }
            if(cadenceId != ''){
                if(Test.isRunningTest()){
                    Test.setMock(HttpCalloutMock.class, new SalesloftMock());
            	}
                String cadIdEndPoint = Salesloft.CADENCE_ID_END_POINT.replace('{id}',cadenceId);
                HttpResponse response = SalesLoft.getAPIResponse(cadIdEndPoint, 'GET', null);
                Salesloft.CadenceData cadData = (Salesloft.CadenceData)JSON.deserialize(response.getBody(), Salesloft.CadenceData.Class);
                if(cadData != NULL){
                    cadenceId = cadData.data.id;
                    ownerId = cadData.data.owner.id;
                }
            }
            if(personId!='' && cadenceId != ''){
                Map<String, String> params = new Map<String, String>{
                                                'person_id'=>personId,
                                                'cadence_id'=>cadenceId,
                                                'user_id'=>ownerId};
            
                HttpResponse response = Salesloft.getAPIResponse(Salesloft.CADENCE_MEMBER_END_POINT, 'POST', params);
                if(response!=NULL){
                    if(response.getStatus() == 'Created' && response.getStatusCode() ==201){
                        Salesloft.CadenceMemberData cadMemberData = (Salesloft.CadenceMemberData)
                            JSON.deserialize(response.getBody(), Salesloft.CadenceMemberData.Class);
                        
                        cadMember.Salesloft_Id__c = cadMemberData.data.id;
                        insert cadMember;
                    }else{
                        String responseBody = response.getBody();
                        Map<String, Object> responseData = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
                        if(responseData.containsKey('errors')){
                            String errMsg = '';
                            Map<String, Object> msgMap = (Map<String, Object>)responseData.get('errors');
                            for(String key :msgMap.keySet()){
                                String keyMsg = String.valueOf(msgMap.get(key));
                                if(key=='cadence_id'){
                                    key = cadenceName;
                                }
                                errMsg+=key+':'+keyMsg+'\n';
                            }
                            throw new SalesloftCadenceMemberSyncServiceException(errMsg);
                        }
                    }
                }
            }
        }else{
            throw new SalesloftCadenceMemberSyncServiceException(Label.IVP_Salesloft_Person_Not_Exist_Message);
        }
    }
}