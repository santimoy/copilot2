@isTest
public class IVPFeelingLuckyMock  implements HttpCalloutMock {
	
	public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        Id iFLRecordTypeId = IVPUtils.iFLRecordTypeId;
        
        String host = System.Url.getSalesforceBaseURL().toExternalForm();
        String endpoint = host + '/services/data/v42.0/ui-api/object-info/Intelligence_Feedback__c/picklist-values/'+iFLRecordTypeId+'/Why_is_score_wrong__c'; 
        
        System.assertEquals(endpoint, req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"label-1":"value-1"},{"label-2":"value-2"},{"label-3":"value-3"}');
        res.setStatusCode(200);
        return res;
    }
}