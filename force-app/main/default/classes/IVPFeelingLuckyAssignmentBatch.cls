/**
 * @author MukeshKS-Concretio
 * @date 2018-05-02
 * @className IVPFeelingLuckyAssignmentBatch
 * @group IVP
 *
 * @description contains methods to run batch over companies to assign IFL records to users
 * @TODO 
 * 1. If user is already having any active IFL then on batch execution assign then only the needed IFL
 * Ex: user has 2 active IFL, so batch should assign him only 3 more! - SOQL - Active Feedback of all users!
 *
 */
public class IVPFeelingLuckyAssignmentBatch 
    implements Database.Batchable<sObject>, 
        Database.Stateful, 
        Schedulable{

    public class IVPFeelingLuckyAssignmentBatchException extends Exception{}
    
    /**
     * @description returns index of users list
     * @return Integer
     * @author Mukesh-Concretio
     */
    public Integer index;

    /**
     * @description returns Id of I'm Feeling Lucky record type id from Intelligence Feedback object
     * @return Id
     * @author MukeshKS-Concretio
     */
    private static Id iFLRecordTypeId = IVPUtils.iFLRecordTypeId;
    
    /**
     * @description returns max count to assign company
     * @return Integer
     * @author Mukesh-Concretio
     */
    public static Integer maxCompanyToAssign  = IVPUtils.maxCompanyToAssign;
    
    /**
     * @description returns max reject count of IFL record
     * @return Integer
     * @author Mukesh-Concretio
     */
    public static Integer maxIFLRejectAttempts  = IVPUtils.maxIFLRejectAttempts;

    /**
     * @description returns map of user and assigned record count
     * @return Map<Id, Integer>
     * @author Mukesh-Concretio
     */
    public Map<Id, Integer> usersActiveIFLCountMap  { get; set;} 

    /**
     * @description returns max reject count of IFL record
     * @return List<Id>
     * @author Mukesh-Concretio
     */
    public List<Id> skippedUsersToAssignIFL  { get; set;} 
    
    /**
     * @description contains list of active users Id to assign IFL records
     * @return List<Id>
     * @author MukeshKS-concretio
    */
    public List<Id> userIds {
        get {
            if(userIds == null) {
                Map<Id, User> usersMap = new Map<Id, User>( IVPUserService.getUsersByGroup(IVPUtils.iFLUserGroups) );
                userIds = new List<Id>(usersMap.keySet());
            }
            return userIds;
        } set;
    }
    
    /**
     * @description returns Id of Unassigned User (Username is Unassigned.)
     * @return Id
     * @author MukeshKS-Concretio
     */
    private static Id unassignedUserId;

    /**
     * this method is used for schedule batch
     * @param SchedulableContext SC object of SchedulableContext
     * @return void
     * @author Mukesh-Concretio
     */
    public void execute(SchedulableContext SC) {
        Database.executeBatch(new IVPFeelingLuckyAssignmentBatch());
    }

    /**
     * @description this method is used for start batch
     * @param Database.BatchableContext BC object of Database.BatchableContext
     * @return Database.QueryLocator
     * @author Mukesh-Concretio
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        //System.debug('Start Batch with users :'+userIds);
        //System.debug('IVPUtils.iFLUserGroups:' + IVPUtils.iFLUserGroups);
        
        unassignedUserId = IVPUtils.unassignedUserId;
        skippedUsersToAssignIFL = new List<Id>();
        usersActiveIFLCountMap = IVPFeelingLuckyService.getUsersActiveIFLCountMap(userIds);
        system.debug('usersActiveIFLCountMap ' + usersActiveIFLCountMap);
        index = 0;
        
        String query = 'SELECT Id, IQ_Score__c, IFL_Sequence__c '
                     + 'FROM Account '
                     + 'WHERE OwnerId = :unassignedUserId '
                     + 'AND IFL_Load_Date__c != null AND IFL_Is_Assignment_Ready__c = True '
                     + 'AND ( IFL_Reject_Count__c < :maxIFLRejectAttempts OR IFL_Reject_Count__c = null ) '
                     + 'AND Id NOT IN '
                     + '(SELECT Company_Evaluated__c FROM Intelligence_Feedback__c '
                     + 'WHERE Is_Expired__c = False AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId ) ' // Accurate_IQ_Score__c is a response
                     + 'AND Id NOT IN '
                     + '(SELECT Company_Evaluated__c FROM Intelligence_Feedback__c '
                     + 'WHERE Accurate_IQ_Score__c = \'Accept\' AND RecordTypeId = :iFLRecordTypeId ) '
                     + 'ORDER BY IFL_Load_Date__c DESC';
        
        return Database.getQueryLocator(query);
    }

    /**
     * @description this method is used for execute batch
     * @param Database.BatchableContext BC object of Database.BatchableContext
     * @param List<Account> companies contains list of companies
     * @return void
     * @author Mukesh-Concretio
     */
    public void execute(Database.BatchableContext BC, List<Account> companies) {
        try{
            assignIFLRecords(companies);
        }catch(Exception e){
            System.debug(e.getStackTraceString()+ '\n' + e.getLineNumber()+'\n');
        }
    }

    /**
     * @description this method is used for finish batch
     * @param Database.BatchableContext BC object of Database.BatchableContext
     * @return void
     * @author Mukesh-Concretio
     */
    public void finish(Database.BatchableContext BC) {
        //TODO: handle error reporting and success
    }   
    
    /**
     * @description this method is used for execute batch
     * @param void
     * @param List<Account> companies contains list of companies
     * @return void
     * @author Mukesh-Concretio
     */
    public void assignIFLRecords(List<Account> companies){


        //system.debug('skippedUsersToAssignIFL ' +skippedUsersToAssignIFL);
        //system.debug('usersActiveIFLCountMap ' +usersActiveIFLCountMap);

        List<Id> userIds = new List<Id>(usersActiveIFLCountMap.keySet());

        //System.debug('companies'+companies);
        //System.debug('userIds'+userIds);
        
        Map<Id, set<id> > userCompanyMap = IVPFeelingLuckyService.getUserCompanySet(companies, userIds);

        //@todo - #1

        //@todo - QUery Feedbacks for scope companies and prepare map<CompanyId, Set<UserIds>>
        
        List<Intelligence_Feedback__c> iFLRecords = new List<Intelligence_Feedback__c>();


        //System.debug('Start Loop over companies: ' + companies);

        for(Account companyObj:companies){
            System.debug('Index: ' +index);

            Id userId = null;// = userIds.get(index++);


            //Loop over the skipped user's list and if applicable assign them a Company! 
            //Finded applicable user could be stored in variable, and after the loop we can remove that from list!
            //If company assigned then continue the loop
            //Increase the User's assignment By 1 
            
            //System.debug('called: getNextAssignee123(0, ' + skippedUsersToAssignIFL.size() + ', '+userCompanySet+', '+skippedUsersToAssignIFL + ', ' + companyObj.Name +')');

            Integer skippedUserIndex = getNextAssignee(0, skippedUsersToAssignIFL.size(), userCompanyMap, skippedUsersToAssignIFL, companyObj.Id);

            System.debug('skippedUserIndex: '+skippedUserIndex);

            if( skippedUserIndex != null) {

                System.debug('Skipped User found.');

                userId = skippedUsersToAssignIFL.get(skippedUserIndex);

                System.debug('remove skipped user from list');
                skippedUsersToAssignIFL.remove(skippedUserIndex); 
                
            }

            if(userId == null) {
                System.debug('Skipped user not found.');

                if(index >= userIds.size()){
                    index = 0;
                }
                
                System.debug('Index: ' +index);

                //System.debug('call: getNextAssignee('+ index +', ' + userIds.size() + ', '+userCompanySet+', '+userIds + ', ' + companyObj.Name +')');

                Integer newIndex = getNextAssignee(index, userIds.size(), userCompanyMap, userIds, companyObj.Id);

                System.debug('newIndex: ' +newIndex);
                
                if(newIndex == null || newIndex == userIds.size()){ 
                    System.debug('user not found. loop users from 0 to '+index);
                    //System.debug('call: getNextAssignee(0, ' + index + ', '+userCompanySet+', '+userIds + ', ' + companyObj.Name +')');

                    newIndex = getNextAssignee(0, index, userCompanyMap, userIds, companyObj.Id);

                     System.debug('newIndex: ' +newIndex);
                }
                if(newIndex != null){
                    userId = userIds.get(newIndex);

                    System.debug('user found. '+userId);
                }
                index = (newIndex == null) ? index+1 : newIndex+1;
                System.debug('final new index '+index);
            }

            
            System.debug('Valid User Id to assign:' +userId);
            Boolean iFLHasToCreateForUser = false;
            
            if(userId != null){

                System.debug('User Id found to assign IFL record: '+userId);

                if(usersActiveIFLCountMap.containsKey(userId)){

                    Integer recordCount = usersActiveIFLCountMap.get(userId);

                    if(recordCount < maxCompanyToAssign){

                        iFLHasToCreateForUser = true;
                        usersActiveIFLCountMap.put(userId, recordCount + 1 );

                    }

                } else {

                    usersActiveIFLCountMap.put(userId, 1);
                    iFLHasToCreateForUser = true;

                }

                if( usersActiveIFLCountMap.get(userId) == maxCompanyToAssign ){
                    

                    Integer indexOfUser = userIds.indexOf(userId);
                    System.debug('indexOfUser>>' + indexOfUser);
                    // check user id in userIds list
                    if(indexOfUser >= 0){
                        // if yes, then remove user id from userIds
                        userIds.remove(indexOfUser);
                    }
                    System.debug('Remaining Users : ' + userIds.size());

                    usersActiveIFLCountMap.remove(userId);

                }

                System.debug('iFLHasToCreateForUser: '+iFLHasToCreateForUser);

            }

            System.debug('iFLHasToCreateForUser: '+iFLHasToCreateForUser);

            if(iFLHasToCreateForUser){
                iFLRecords.add(new Intelligence_Feedback__c(
                        Company_Evaluated__c = companyObj.Id,
                        User__c = userId,
                        Sequence__c = ((companyObj.IFL_Sequence__c != null) ? (companyObj.IFL_Sequence__c + 1) : 1),
                        Assignment_Date_Time__c = System.now(),
                        IQ_Score_at_Creation__c =   ((companyObj.IQ_Score__c != null) ? companyObj.IQ_Score__c : 0),
                        RecordTypeId = IVPUtils.iFLRecordTypeId
                    ));
            }
                        
        }
        System.debug('iFLRecords'+iFLRecords);
        if(!iFLRecords.isEmpty()){
            
            List<Database.SaveResult> iFLRecordsSaveResult = Database.insert(iFLRecords);
            
            if(!iFLRecordsSaveResult[0].isSuccess()){
                System.debug('Exception '+iFLRecordsSaveResult);
                throw new IVPFeelingLuckyAssignmentBatchException('An error occurred attempting to assign new IFL records while running batch: ' 
                                                       + iFLRecordsSaveResult[0].getErrors()[0].getMessage());
            }   
        }
        system.debug('After skippedUsersToAssignIFL ' +skippedUsersToAssignIFL);
        system.debug('After usersActiveIFLCountMap ' +usersActiveIFLCountMap);
    }

    /**
     * @description GetNextAssignee - this method id used for to get userId to whom assign IFL company
     * @param  Integer startIndex loop start from
     * @param  Integer endIndex loop end on
     * @param  Map<Id,Set<Id>> userCompanyMap map of userId and set of companyIds
     * @param  List<Id> userIds list of userIDs
     * @param  Id companyId companyID to assign
     * @return Integer index of userId
     * @author Mukesh-concretio
     */
    public Integer getNextAssignee(Integer startIndex, Integer endIndex, Map<Id, Set<Id>> userCompanyMap, List<Id> userIds, Id companyId){
        
        Integer nextIndex;

        for(integer i=startIndex; i< endIndex ; i++){
            Id userId = userIds.get(i);
            //Skip user?
            if( userCompanyMap.containsKey( userId )  ){
                if( userCompanyMap.get( userId ).contains(companyId) ){
                    // user exists in Skipped list?
                    if(skippedUsersToAssignIFL.indexOf(userId) == -1){
                        System.debug('Put in Skipped list : '+userId);
                        skippedUsersToAssignIFL.add( userId );
                    }
                    continue;
                }
                
            }
            System.debug('Next assignee index : '+i);
            //userId = userIds[i];
            nextIndex = i;
            break;
        }

        return nextIndex;
    }      
}