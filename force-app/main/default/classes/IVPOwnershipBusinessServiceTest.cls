/***********************************************************************************
 * @author      10k-Expert
 * @name		IVPOwnershipBusinessServiceTest
 * @date		2019-01-18
 * @description	To test IVPOwnershipBusinessService
 ***********************************************************************************/
@isTest
public class IVPOwnershipBusinessServiceTest{
    @isTest
    public static void prepareNextWorkingDayTest(){
        //List<Holiday> holidayList = [SELECT Id, ActivityDate, StartTimeInMinutes, EndTimeInMinutes, IsAllDay FROM Holiday  ORDER BY ActivityDate];
        //System.debug('holidayList :: '+holidayList.size());
        //List<Holiday> hList = new List<Holiday>();
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        //testdata.populateIVPGeneralConfig();
        DateTime sDate = System.now();
        Date pastDate = System.now().date();
        
        
        Test.startTest();
        	List<Holiday> hList = testData.holidays;
            IVPOwnershipBusinessService.BusinessDaysWrapper bdwObj = new IVPOwnershipBusinessService.BusinessDaysWrapper(sDate, 2);
            
            List<IVPOwnershipBusinessService.BusinessDaysWrapper> businessDateLst = new List<IVPOwnershipBusinessService.BusinessDaysWrapper>();
            businessDateLst.add(bdwObj);
        
        	DateTime businessDate = bdwObj.businessDate;
            IVPOwnershipBusinessService.prepareNextWorkingDay(bdwObj);
        
        	// interval has been reset to 0 while processing the request in IVPOwnershipBusinessService
        	System.assert(bdwObj.interval == 0);
        	// businessdate has been increased while preparing the next working day in IVPOwnershipBusinessService
        	System.assert(businessDate != bdwObj.businessDate);
        	
        
        
        	//System.debug('before businessDateLst :: '+businessDateLst);
            //IVPOwnershipBusinessService.prepareNextWorkingDayWithPastHoliday(businessDateLst , pastDate);
        	//System.debug('after businessDateLst :: '+businessDateLst);
        
        	//System.debug('*****before businessDateLst :: '+businessDateLst);
        
            //IVPOwnershipBusinessService.prepareWorkingDays(businessDateLst);
        	//System.debug('after businessDateLst :: '+businessDateLst);
        
        Test.stopTest();
    }
    
	@isTest
    public static void prepareNextWorkingDayWithPastHolidayTest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        //testdata.populateIVPGeneralConfig();
        DateTime sDate = System.now();
        Date pastDate = System.now().date();
        
        
        Test.startTest();
        	List<Holiday> hList = testData.holidays;
            IVPOwnershipBusinessService.BusinessDaysWrapper bdwObj2 = new IVPOwnershipBusinessService.BusinessDaysWrapper(sDate);
        	IVPOwnershipBusinessService.BusinessDaysWrapper bdwObj = new IVPOwnershipBusinessService.BusinessDaysWrapper(sDate, 2);
            List<IVPOwnershipBusinessService.BusinessDaysWrapper> businessDateLst = new List<IVPOwnershipBusinessService.BusinessDaysWrapper>();
            businessDateLst.add(bdwObj);
        	DateTime businessDate = bdwObj.businessDate;
        
        	//System.debug('before businessDateLst :: '+businessDateLst);
            IVPOwnershipBusinessService.prepareNextWorkingDayWithPastHoliday(businessDateLst , pastDate);
        	//System.debug('after businessDateLst :: '+businessDateLst);
        
        	// interval has been reset to 0 while processing the request in IVPOwnershipBusinessService
        	System.assert(bdwObj.interval == 0);
        	// businessdate has been increased while preparing the next working day in IVPOwnershipBusinessService
        	System.assert(businessDate != bdwObj.businessDate);
        
        Test.stopTest();
    }
    @isTest
    public static void prepareWorkingDaysTest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        //testdata.populateIVPGeneralConfig();
        DateTime sDate = System.now();
        Date pastDate = System.now().date();
        
        
        Test.startTest();
        	List<Holiday> hList = testData.holidays;
            IVPOwnershipBusinessService.BusinessDaysWrapper bdwObj = new IVPOwnershipBusinessService.BusinessDaysWrapper(sDate, 2);
            List<IVPOwnershipBusinessService.BusinessDaysWrapper> businessDateLst = new List<IVPOwnershipBusinessService.BusinessDaysWrapper>();
            businessDateLst.add(bdwObj);
        	DateTime businessDate = bdwObj.businessDate;
            IVPOwnershipBusinessService.prepareWorkingDays(businessDateLst);
        
        	// interval has been reset to 0 while processing the request in IVPOwnershipBusinessService
        	System.assert(bdwObj.interval == 0);
        	// businessdate has been increased while preparing the next working day in IVPOwnershipBusinessService
        	System.assert(businessDate != bdwObj.businessDate);
        
        Test.stopTest();
    }
}