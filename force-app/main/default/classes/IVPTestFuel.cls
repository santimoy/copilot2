@isTest 
public class IVPTestFuel {
    private String namePrefix;
    
    public IVPTestFuel(){  namePrefix = 'tst';}
    
    public IVPTestFuel(String namePrefix){
        
        this.namePrefix = namePrefix;
    }

    public void populateLVSettings(Boolean forUserAlso, Boolean forOrgAlso){
      System.runAs(users[1]){
            List<IVP_SD_ListView__c> ivpSDListViews= new List<IVP_SD_ListView__c>();
            if(forUserAlso){
              ivpSDListViews.add(IVPUtils.currentusersetting());
          }
          if(forOrgAlso){
            ivpSDListViews.add(IVPUtils.currentOrgSetting());
            
          }
          if(!ivpSDListViews.isEmpty()){
            List<ListView> listviews = [SELECT Id, DeveloperName FROM ListView 
                          WHERE DeveloperName in ('My_Recently_Unassigned_Public','My_Smartdash_Public') 
                          ORDER BY DeveloperName desc
                          LIMIT 1];
        if(!listviews.isEmpty()){
                for(IVP_SD_ListView__c rec:ivpSDListViews){
                    rec.List_View_Name__c = listviews[0].DeveloperName;
                }
                upsert ivpSDListViews;
        }
          }
        }
    }
    
    public void populateLVSettings(){
        populateLVSettings(true, true);
    }

    public User[] users{
        get {
            if (users == NULL) {
                List<Profile> profiles = [SELECT Id FROM PROFILE 
                                          WHERE Name IN ('System Administrator','Analyst Full') ORDER By Name ASC];
                
                users = new User[]{
                    new User(alias = namePrefix+'1', email=namePrefix+'Unit.Test@unittest.com',
                             emailencodingkey='UTF-8', firstName='First2', lastname=namePrefix+'Last2', languagelocalekey='en_US',
                             localesIdkey='en_US', profileId = profiles[0].Id,
                             timezonesIdkey='Europe/Berlin', username=namePrefix+'Unit.Test1@unittest.com'),
                        
                    new User(alias = namePrefix+'2', email=namePrefix+'Unit.Test2@unittest.com',
                             emailencodingkey='UTF-8', firstName='First2', lastname='Last2', languagelocalekey='en_US',
                             localesIdkey='en_US', profileId = profiles[1].Id,
                             timezonesIdkey='Europe/Berlin', username=namePrefix+'Unit.Test3@unittest.com')   
                    };
                insert users;
            }
            return users;
        }
        set;
    }
    
    public Account[] companies{
        get {
            String rtId = prospectRTId;
            if(companies == NULL) {
                companies = new Account[]{
                    new Account(Name = '#Test Account Name 1', BillingCountry='Croatia'),
                        new Account(Name = '#Test Account Name 2', Type ='Partner',BillingCountry='Croatia'),
                        new Account(Name = '#Test Account Name 3',BillingCountry='Croatia')
                        };
                if(rtId != ''){
                    for(Account acc: companies){
                        acc.RecordTypeId = rtId;
                    }
                }
                insert companies;
            }
            return companies;
        }
        set;
    }
    public Contact[] contacts{
        get {
            if(contacts == NULL) {
                contacts = new Contact[]{
                    new Contact(FirstName ='Test 1', LastName = '#Test Contact Name 1', AccountId = companies[0].Id, Key_Contact__c=true, MailingCountry='Croatia',
                                Email = 'test1@test1.com'),
                        new Contact(FirstName ='Test 2', LastName = '#Test Contact Name 2', AccountId = companies[1].Id, Key_Contact__c=true, MailingCountry='Croatia',
                                    Email = 'test2@test2.com'),
                        new Contact(FirstName ='Test 3', LastName = '#Test Contact Name 3', AccountId = companies[2].Id, MailingCountry='Croatia')
                        };
                insert contacts;
            }
            return contacts;
        }
        set;
    }
    public static String prospectRTId{
        get{
            if(prospectRTId == NULL){
                prospectRTId = '';
                List<RecordType> rts = [SELECT Id, Name, DeveloperName FROM RecordType 
                                        WHERE SobjectType='ACCOUNT' AND DeveloperName='Prospect' LIMIT 1];
                if(!rts.isEmpty()){
                    prospectRTId = rts[0].Id;
                }
            }
            return prospectRTId;
        }set;
        
    }
    public Signal_Category__c[] signalCategories{
        get{
            if(signalCategories == NULL){
                signalCategories = new Signal_Category__c[]{
                    new Signal_Category__c(Active__c = true, Description__c='Test Data',Group__c='Financial',Options__c='Log Activity',
                                           Priority__c=2,Source__c='',External_ID__c='Test_signal1'),
                    new Signal_Category__c(Active__c = true, Description__c='Test Data',Group__c='Financial',Options__c='Log Activity',
                                           Priority__c=2,Source__c='',External_ID__c='Test_signal1',
                                           Do_not_display__c=true)
                    };
                insert signalCategories;
            }
            return signalCategories;
        }set;
    }
    public Company_Signal__c[] companySignals{
        get{
            if(companySignals == NULL){
                companySignals = new Company_Signal__c[]{
                    new Company_Signal__c(Category__c = signalCategories[0].Id, Company__c = companies[0].Id,
                                          Date__c = System.Today(), External_ID__c = 'Test_signal1', Headline__c = '',
                                          Source__c = '', Type__c = 'object'),
                    new Company_Signal__c(Category__c = signalCategories[1].Id, Company__c = companies[1].Id,
                                          Date__c = System.Today(), External_ID__c = 'Test_signal1', Headline__c = '',
                                          Source__c = '', Type__c = 'object'),
                    new Company_Signal__c(Category__c = signalCategories[0].Id,Company__c = companies[1].Id,
                                          Date__c = System.Today(), External_ID__c = 'Test_signal1', Headline__c = '',
                                          Source__c = '', Type__c = 'object'),
                    new Company_Signal__c(Category__c = signalCategories[1].Id, Company__c = companies[1].Id,
                                          Date__c = System.Today(), External_ID__c='Test_signal1', Headline__c = '',
                                          Source__c ='', Type__c = 'object')
                    
                    };
              insert companySignals;
            }
            return companySignals;
        }set;
    }
    public Intelligence_Feedback__c[] IntelligenceFeedback{
        get{
          if(IntelligenceFeedback == NULL){
              IntelligenceFeedback=new Intelligence_Feedback__c[]{
                  new Intelligence_Feedback__c(Company_Evaluated__c= companies[0].Id,Accurate_IQ_Score__c='Yes'),
                  new Intelligence_Feedback__c(Company_Evaluated__c= companies[1].Id,Accurate_IQ_Score__c='Yes'),
                  new Intelligence_Feedback__c(Company_Evaluated__c= companies[2].Id,Accurate_IQ_Score__c='Yes')
                  };
              insert IntelligenceFeedback;
          }
            return IntelligenceFeedback;
        }
        set;
    }
    public String inputJson{
        get{
            if(inputJson == NULL){
                return '{ "WhatId":"'+companies[0].Id+'", "WhoId":"", "Subject":"type" ,"Type__c":"'+companies[0].type+'"'+
                    ',"Status":"completed","Description":"Test Data","Priority":"Normal"'+
                    ',"Next_Action_Date__c":"'+(System.now().year()+1)+'-7-7","ActivityDate":"'+System.now().year()+'2018-6-6"'+
                    ',"Source__c":"","signalCategories__c":"'+signalCategories[0].Id+'","Company_Signal_External_Id__c":"Test_signal1"'+
                    ',"Company_Signal_Headline__c":"'+companySignals[0].Headline__c+'","Company_Signal__c":"'+companySignals[0].Id+'"}';
            }
            return inputJson;
        }
        set;
    }
    
    public Group[] groups{
        get{
            if(groups == NULL){
                String groupName=IVPSDLVMaintainVisibilityService.IVP_SDLV_LABEL_INITIAL + UserInfo.getName();
                // groupName=(groupName.length()>40?groupName.subString(0,39):groupName);
                groups = new Group[]{
                    new Group(Name =groupName, 
                              DeveloperName = IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL + UserInfo.getUserId()+'1'),
                    new Group(Name =groupName, 
                              DeveloperName = IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL + UserInfo.getUserId()+'2')
                    };
                insert groups;
            }
            return groups;
        }
        set;
    }
    
    public GroupMember[] groupMembers{
        get{
            if(groupMembers == NULL){
                groupMembers = New GroupMember[]{
                    new GroupMember(GroupId = groups[0].Id, UserOrGroupId = UserInfo.getUserId())
                };
                insert groupMembers;
            }
            return groupMembers;
        }
        set;
    }
    
    public Topic[] topicList{
        get{
            if(topicList == NULL){
                topicList = new Topic[]{
                  new Topic(Name=namePrefix+'test')
                };
              insert topicList;
            }
            return topicList;
        }set;   
    }
    public static void updateCurrentUserSetting(){
        List<ListView> listviewrec=[SELECT Name,DeveloperName,CreatedById,CreatedDate FROM ListView WHERE SobjectType='Account' LIMIT 1];
        IVPCompanyListViewCtlr.maintainDefautListView(listviewrec[0].DeveloperName);
    }
    
    /**
  * createTestUser - this method is used for to create test user
  * @param String lName - last name of user
  * @param Boolean doAssignPermissionSet - set true if want to assign IVP_Smart_Dash PermissionSet
  * @return User tuser - test user data
  * @author Mukesh-concretio
  */
    public static User createTestUser(String lName, Boolean doAssignPermissionSet) {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'Analyst Full'];
        
        UserRole ur = new UserRole(Name = 'Analyst');
        insert ur;
        
        
        User tuser = new User(
            lastName = lName,
            email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',
            EmailEncodingKey = 'ISO-8859-1',
            Alias = uniqueName.substring(18, 23),
            TimeZoneSidKey = 'America/New_York',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            ProfileId = pf.Id,
            UserRoleId = ur.Id
        );
        
        insert tuser;
        
        if(doAssignPermissionSet) {
            PermissionSet myPermissionSetId = [Select Id From PermissionSet WHERE Name='IVP_Smart_Dash'];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = myPermissionSetId.Id, AssigneeId = tuser.Id);
            insert psa;
        }
        
        return tuser;
    }
    
    /**
  * createTestCompanies - this method is used for to create test companies
  * @param Integer limt - how many records to create
  * @param Id ownerId - owner of the companies
  * @return List<Account> testCompanies - list of companies
  * @author Mukesh-concretio
  */
    public static List<Account> createTestCompanies(Integer limt, Id ownerId) {
        Id recordTypeId = prospectRTId;
        List<Account> testCompanies = new List<Account>();
        
        for( Integer i = 0; i < limt; i++ ){
            testCompanies.add( new Account(
                Name = 'Test Company ' + i,
                IFL_Sequence__c = 0,
                IQ_Score__c = 2.4,
                Website__c = 'http://example.com/',
                IFL_Load_Date__c = System.today(),
                OwnerId = ownerId,
                RecordTypeId=recordTypeId
            ) );
        }
        
        insert testCompanies;
        
        return testCompanies;
    }
    
    /**
  * updateCompaniesOwner - this method is used for to update owner of test companies
  * @param List<Account> testCompanies - list of companies
  * @param Id ownerId user id
  * @return List<Account> testCompanies - list of companies
  * @author Mukesh-concretio
  */
    public static List<Account> updateCompaniesOwner(List<Account> companies, Id ownerId) {
        List<Account> testCompanies = new List<Account>();
        
        for(Account obj:companies) {
            obj.OwnerId = ownerId;
            testCompanies.add(obj);
        }
        
        update testCompanies;
        
        return testCompanies;
    }
    
    /**
  * createTestIFLRecords - this method is used for to create IFL (I'm Feeling Lucky) records for user to perform thumbs up/down
  * @param List<Account> unassignedCompanies - list of companies whose owner is unassigned user
  * @param Id userId to whom create IFL records
  * @return List<Intelligence_Feedback__c> testIFLRecords - list of IFL records
  * @author Mukesh-concretio
  */
    public static List<Intelligence_Feedback__c> createTestIFLRecords(List<Account> unassignedCompanies, Id userId) {
        
        List<Intelligence_Feedback__c> testIFLRecords = new List<Intelligence_Feedback__c>();
        
        for(Account companyObj:unassignedCompanies){
            
            testIFLRecords.add(new Intelligence_Feedback__c(
                Company_Evaluated__c = companyObj.Id,
                User__c = userId,
                Sequence__c = companyObj.IFL_Sequence__c + 1,
                Assignment_Date_Time__c = System.now(),
                IQ_Score_at_Creation__c =   companyObj.IQ_Score__c,
                RecordTypeId = IVPUtils.iFLRecordTypeId,
                Accurate_IQ_Score__c = null
            ));
            
        }
        
        insert testIFLRecords;
        
        return testIFLRecords;
        
    }
    public Task[] tasks{
        get{
            if(tasks == NULL){
                tasks=new Task[]{
                    new Task(WhatId=companies[0].Id,Status='In Progress',Company_Signal__c=companySignals[0].Id,
                             Signal_Category__c=signalCategories[0].Id,Type__c='Log Activity'),
                    new Task(WhatId=companies[1].Id,Status='In Progress',Company_Signal__c=companySignals[1].Id,
                             Signal_Category__c=signalCategories[0].Id,Type__c='Log Activity'),
                    new Task(WhatId=companies[0].Id,Status='In Progress',Company_Signal__c=companySignals[2].Id,
                             Signal_Category__c=signalCategories[1].Id,Type__c='Examine Activity'),
                    new Task(WhatId=companies[1].Id,Status='In Progress',Company_Signal__c=companySignals[3].Id,
                             Signal_Category__c=signalCategories[1].Id,Type__c='Examine Activity')
                    };
                insert tasks;
            }
            return tasks;  
        }set;
    }
    
    /**
  * createTestPublicGroup - this method is used for to create test public group
  * @param String testGroupName
  * @return Group testGroup
  * @author Mukesh-concretio
  */
    public static Group createTestPublicGroup(String testGroupName){
        Group testGroup = new Group(Name=testGroupName);
        insert testGroup;
        
        return testGroup;
    }
    
    /**
  * assignTestUsersToTestPublicGroup - this method is used for to create test public group
  * @param Group testGroup
  * @param Set<Id> testUserIds
  * @return void
  * @author Mukesh-concretio
  */
    public static void assignTestUsersToTestPublicGroup(Group testGroup, Set<Id> testUserIds){
        List<GroupMember> testGroupMembers = new List<GroupMember>();
        
        for(Id testUserId:testUserIds){
            GroupMember testGroupMember = new GroupMember();
            
            testGroupMember.UserOrGroupId = testUserId;
            testGroupMember.GroupId = testGroup.Id;
            
            testGroupMembers.add(testGroupMember);
        }
        
        insert testGroupMembers;
    }
    
    /*
    */
    public List<Sourcing_Preference__c> sourcingPreferences{
        get{
            if(sourcingPreferences==null){
                sourcingPreferences = new List<Sourcing_Preference__c>{
                    new Sourcing_Preference__c(
                        Name__c = 'Demo', External_Id__c = 'hdjb',
                        Sent_to_DWH__c = true, Is_Template__c = true,
                        JSON_QueryBuilder__c = sourcingPreferenceJSON),
                    new Sourcing_Preference__c(
                        Name__c = 'Demo', External_Id__c = 'hdjb2',
                        Sent_to_DWH__c = true, Is_Template__c = false,
                        JSON_QueryBuilder__c=sourcingPreferenceJSON)
                };
                insert sourcingPreferences;
            }
            return sourcingPreferences;
        }set;
    }
    
    public static String sourcingPreferenceJSON{
        get{
            return '{"condition":"AND","rules":[{"id":"LOWER(ct.companies_cross_data_full.ivp_country)","field":"LOWER(ct.companies_cross_data_full.ivp_country)","type":"string","input":"select","operator":"equal","value":"united states"}],"valid":true}';
        }
    }
    
    
    /**
     * Create Salesloft
     * 
    **/
    
     public List<Salesloft_Cadence__c> salesloftCadences{
         get{
             if(salesloftCadences==null){
                 salesloftCadences = new List<Salesloft_Cadence__c>{
                     new Salesloft_Cadence__c(
                         Name='Test1'),
                     new Salesloft_Cadence__c(
                         Name='Test2')
                 };
                 insert salesloftCadences;
             }
             return salesloftCadences;
         }set;
     }
    
    /**
     * Create Salesloft
     * 
    **/
    
     public List<Salesloft_Cadence_Member__c> salesloftCadenceMembers{
         get{
             if(salesloftCadenceMembers==null){
                 salesloftCadenceMembers = new List<Salesloft_Cadence_Member__c>{
                     new Salesloft_Cadence_Member__c(cadence__c = salesloftCadences[0].Id,
                          Contact__c = contacts[1].Id),
                     new Salesloft_Cadence_Member__c(cadence__c = salesloftCadences[1].Id,
                          Contact__c = contacts[0].Id)
                 };
                 insert salesloftCadenceMembers;
             }
             return salesloftCadenceMembers;
        }set;
    }
    
    public List<Intelligence_Field__c> iiFields{
         get{
             if(iiFields==null){
                 iiFields = new List<Intelligence_Field__c>{
                     new Intelligence_Field__c(DB_Field_Name__c='apptopia_1m_total_revenue_growth',
                         Default_display_field__c=false, UI_Label__c='App Revenue 1M %', 
                         Active__c=true, Static_Selection__c=false, 
                         DB_Column__c='ct.companies_cross_data_full.apptopia_1m_total_revenue_growth',
                         Optional_display_Field__c=true),
                     new Intelligence_Field__c(DB_Field_Name__c='apptopia_6m_total_revenue_growth',
                         Default_display_field__c=false, UI_Label__c='App Revenue 6M %', 
                         Active__c=true, Static_Selection__c=false, 
                         DB_Column__c='ct.companies_cross_data_full.apptopia_6m_total_revenue_growth',
                         Optional_display_Field__c=true),
                     new Intelligence_Field__c(DB_Field_Name__c='ivp_city',
                         Default_display_field__c=false, UI_Label__c='City', 
                         Active__c=true, Static_Selection__c=true, 
                         DB_Column__c='ct.companies_cross_data_full.ivp_city',
                         Optional_display_Field__c=true)
                 };
                 insert iiFields;
             }
             return iiFields;
        }set;
    }
    // Just_For_You_Setting__c
    // Global_Contact_Cadence__c
    // Clearbit_Callout_Users__c 
    // Intelligence_Field__c
    
    
    
    
    
}