/**
* @author Amjad-Concretio
* @date 2018-04-30
* @className IVP_RecentlyUnassignedService
*
* @description service class to help batch to process accounts
*/
public class IVP_RecentlyUnassignedService {
    public static void prepareMap(List<sObject> scope, Map<String,Set<String>> accountToUnassignedUser, Map<String,String> userNameTOId){
        for(Recently_Unassigned__c rec : (Recently_Unassigned__c[])scope){
            if(accountToUnassignedUser.containsKey(rec.Company__c) && userNameTOId.get(rec.Old_Owner__c) != null){
                Set<String> temp = accountToUnassignedUser.get(rec.Company__c);
                
                //Adding a check for text 255 we can store only 15 IDs on Unassigned Users
                if(temp.size()>15){
                    continue;
                }
                temp.add(userNameTOId.get(rec.Old_Owner__c));
                accountToUnassignedUser.put(rec.Company__c,temp);
            }else if(userNameTOId.get(rec.Old_Owner__c) != null){
                accountToUnassignedUser.put(rec.Company__c, new set<String>{userNameTOId.get(rec.Old_Owner__c)});
            }
        }
    }
}