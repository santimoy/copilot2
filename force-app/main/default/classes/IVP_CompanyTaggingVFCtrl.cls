/**
 * @author Tapeshwar Kumar
 * @date 2018-04-25
 * @className CompanyTaggingCtrl
 * @group IVPSmartDash
 *
 * @description contains methods to handle IVPTagging functionality
*/
public class IVP_CompanyTaggingVFCtrl {
    public String accountIds{get; set;}
    public IVP_CompanyTaggingVFCtrl(ApexPages.StandardSetController controller){
    	accountIds='';
        if(((List<Account>)controller.getSelected()).size() == 0){
            accountIds='Not Selected';
        }else{
            for (Account acct : (List<Account>)controller.getSelected()){
                accountIds += acct.Id + ',';
            }    
        }
    }
}