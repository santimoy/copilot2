@isTest
private class EmailDiscovererControllerTest
{
	final static String EMAIL_ADDRESS = 'test.contact@test.com';
	final static String VALID_EMAIL = 'dummy.user@test.com';

	final static String FIRST_NAME = 'John';
	final static String LAST_NAME = 'Doe';

	@testSetup static void setup()
	{
		Account testAccount = new Account( Name = 'Test Company' );
		testAccount.Website = 'www.test.com';
		insert testAccount;

		Contact testContact = TestFactory.createContacts( 1, testAccount.Id, 'TestContact', false )[0];
		testContact.Key_Contact__c = true;
		testContact.Email = EMAIL_ADDRESS;
		testContact.FirstName = FIRST_NAME;
		testContact.LastName = LAST_NAME;
		insert testContact;

		Email_Finder_Settings__c setting = new Email_Finder_Settings__c();
		setting.Name = 'email-checker.com';
		setting.API_Endpoint__c = 'https://api.emailverifyapp.com/api/a/v1?key={!APIKEY}&email={!EMAIL}';
		setting.API_Key__c = 'EE9C41A1';
		setting.Is_Active__c = true;
		setting.Overall_Timeout__c = '30000';
		setting.Request_Timeout__c = '10000';
		insert setting;
	}

	@isTest static void testEmailDiscovererController()
	{
		Test.setMock( HttpCalloutMock.class, new EmailFinderMockHttpResponseGenerator() );

		Account testAccount = [ SELECT Id, Website_Domain__c FROM Account LIMIT 1 ];

		Test.startTest();

			EmailDiscovererController.EmailDiscovererWrapper wrapper = EmailDiscovererController.initializeEmailDiscoverer( testAccount.Id );
			System.assert( wrapper != NULL, 'Email Discoverer Wrapper should be generated with Account and Key Contact information' );
			System.assertEquals( FIRST_NAME, wrapper.firstName, 'firstName property should be set to Key Contact\'s First Name' );
			System.assertEquals( LAST_NAME, wrapper.lastName, 'lastName property should be set to Key Contact\'s Last Name' );
			System.assertEquals( 'test.com', wrapper.domain, 'domain property should be set to Account\'s Website Domain' );

			List<EmailFinder.EmailDefinition> allDiscovererEmails = EmailDiscovererController.findDiscovererEmails( wrapper.firstName, wrapper.lastName, wrapper.domain );
			System.assertEquals( 1, allDiscovererEmails.size() );

			EmailDiscovererController.createKeyContact( testAccount.Id, 'Mary', 'Jane', VALID_EMAIL );
			List<Contact> keyContacts = [ SELECT Id, FirstName, LastName, Email FROM Contact WHERE Key_Contact__c = true ];
			System.assertEquals( 1, keyContacts.size(), 'There should be only one Key Contact per Account. The new contact should be set as Key Contact' );
			System.assertEquals( 'Mary', keyContacts[0].FirstName );
			System.assertEquals( 'Jane', keyContacts[0].LastName );
			System.assertEquals( VALID_EMAIL, keyContacts[0].Email );

			final String NEW_EMAIL = 'dummyuser@test.com';
			EmailDiscovererController.updateKeyContact( testAccount.Id, NEW_EMAIL );
			keyContacts = [ SELECT Id, FirstName, LastName, Email FROM Contact WHERE Key_Contact__c = true ];
			System.assertEquals( 1, keyContacts.size(), 'There should be only one Key Contact per Account' );
			System.assertEquals( 'Mary', keyContacts[0].FirstName );
			System.assertEquals( 'Jane', keyContacts[0].LastName );
			System.assertEquals( NEW_EMAIL, keyContacts[0].Email );

		Test.stopTest();
	}

	// implementing a mock http response generator for concur
	public class EmailFinderMockHttpResponseGenerator implements HttpCalloutMock
	{
		public HTTPResponse respond( HTTPRequest req )
		{
			EmailFinder.EmailJson mjsonOk = new EmailFinder.EmailJson();
			mjsonOk.status = 'Ok';
			mjsonOk.additionalStatus = 'Success';
			mjsonOk.Message = '';
			mjsonOk.emailAddressChecked = VALID_EMAIL;
			mjsonOk.emailAddressSuggestion = VALID_EMAIL;
			mjsonOk.emailAddressProvided = VALID_EMAIL;

			String mjsonString = JSON.serialize( mjsonOk );
			// Create a fake response
			HttpResponse res = new HttpResponse();
			res.setHeader( 'Content-Type', 'application/json' );
			res.setBody( mjsonString );
			res.setStatusCode( 200 );
			return res;
		}
	}
}