/*
*	CommonConfigUtil class : It contains all static methods related with configuration for Common Config custom settings.
*
*	Author: Wilson Ng
*	Date:   Sept 12, 2012
*
*	Last Modified:
*		WNG 4-4-2018 Deprecated items: hard-to-crack, do-not-miss, and primary-interested
		DL 8-21-2018: Added PARDOT_CONVERTED_OWNER_FULL_NAME variable to populate Pardot_Converted_Owner_Full_Name__c on config object.
*/
public with sharing class CommonConfigUtil {

	// Static Variables for default values
	public static String  ACCOUNTEXPIRATION_UNASSIGNED_LASTNAME	= 'Unassigned';
	public static String  ACCOUNTEXPIRATION_RECORD_TYPES_FILTER	= 'Prospect';
	public static integer ACCOUNT_OWNER_LIMIT					= 500;
	public static String  ACCOUNT_OWNER_ERROR_MESSAGE_EXCEEDED	= 'You have exceeded the maximum number of Account records to own.';
	public static String  RATING_OWNER_PROFILE_FILTER			= 'Analyst,Analyst Full';
	public static String  RATING_RECORD_TYPES_FILTER			= 'Prospect';
	public static String  RATING_DEAL_FIELDS					= 'Company_Quality__c,Deal_Imminence__c,Deal_Size__c,Deal_Type__c,Deal_Notes__c,Responsive__c';
	public static Integer ACTIVITY_VIEWER_MOBILE_NUM_PAST_ACT	= 10;
	public static Integer ACTIVITY_VIEWER_MOBILE_NUM_CHARS_DESC = 500;
	public static String  PARDOT_CONVERTED_OWNER_FULL_NAME      = 'Unassigned';

	// 4-4-2018 the following settings are to be deprecated:
	public static String  ACCOUNTEXPIRATION_EXCLUDED_STAGES		= 'Term Sheet Proposed,Term Sheet Signed,Portfolio Company';
	public static String  ACCOUNT_WO_FINANCIAL_ERROR_MESSAGE	= 'You have exceeded the maximum number of companies without financial data.';
	public static integer ACCOUNT_WO_FINANCIAL_NEW_USER_COUNT	= 300;
	public static integer ACCOUNT_WO_FINANCIAL_OLD_USER_COUNT	= 100;
	public static integer FREQUENCY_OF_HARD_TO_CRACK			= 365;
	public static String  HARDTOCRACK_ALREADY_MARKED_MESSAGE	= 'You have exceeded the number of times you may mark this company as Hard To Crack.';
	public static integer HARDTOCRACK_AMOUNT_OF_TIME 			= 730;			// in days
	public static String  HARDTOCRACK_ERRMSG_EXCEEDED	 		= 'You have exceeded the maximum number of Hard to Crack companies.';
	public static String  HARDTOCRACK_ERRMSG_NOTALLOWED			= 'You are not allowed to delete a record with the Hard to Crack indicator equals true.';
	public static String  HARDTOCRACK_ERRMSG_NOTOWNER	 		= 'You are not allowed to update the Hard to Crack indicator. Only the record owner or system administrator can update the Hard to Crack indicator.';
	public static integer HARDTOCRACK_MAX_NUMBER_COMPANIES		= 8;			// per owner
	public static integer HARDTOCRACK_TIMES_PER_COMPANY			= 1;
	public static String  FINANCIAL_INFOMATION_FIELDS			= 'EBITDA__c,Revenue__c';
	public static integer NEW_USER_FOR							= 60;


	// Static Methods
	public static Common_Config__c getConfig()
	{
		// Get the Org record
		Common_Config__c cconf = Common_Config__c.getOrgDefaults();
		if(cconf.Id == NULL){
			// if org default record not exists, then get default values and insert
			cconf = getDefaultCommonConfiguration();
		}
		if (Test.isRunningTest()) {
			// use default settings if running tests
			populateDefaultCommonConfiguration(cconf);
			update cconf;
		}
		// Get current user record
		cconf = Common_Config__c.getInstance();
		System.Debug('Common Config: '+cconf);
		return cconf;
	}

	// Create and insert new custom setting with default values for Common Config
	static Common_Config__c getDefaultCommonConfiguration(){
		Common_Config__c cconf = new Common_Config__c( SetupOwnerId=UserInfo.getOrganizationId() );
		populateDefaultCommonConfiguration(cconf);
		insert cconf;
		return cconf;
	}

	static void populateDefaultCommonConfiguration(Common_Config__c cconf){
		cconf.AccountExpiration_Unassigned_Lastname__c		= ACCOUNTEXPIRATION_UNASSIGNED_LASTNAME;
		cconf.AccountExpiration_RecordTypes__c 				= ACCOUNTEXPIRATION_RECORD_TYPES_FILTER;
		cconf.Account_Owner_Limit__c 						= ACCOUNT_OWNER_LIMIT;
		cconf.Account_Owner_Error_Message_Exceeded__c 		= ACCOUNT_OWNER_ERROR_MESSAGE_EXCEEDED;
		cconf.Company_Rating_Profiles__c 					= RATING_OWNER_PROFILE_FILTER;
		cconf.Company_Rating_Record_Types__c 				= RATING_RECORD_TYPES_FILTER;
		cconf.Company_Rating_Fieldnames__c 					= RATING_DEAL_FIELDS;
		cconf.Activity_Viewer_Mobile_Num_Past_Activity__c 	= ACTIVITY_VIEWER_MOBILE_NUM_PAST_ACT;
		cconf.Activity_Viewer_Mobile_Num_Chars_Desc__c 		= ACTIVITY_VIEWER_MOBILE_NUM_CHARS_DESC;
		cconf.Pardot_Converted_Owner_Full_Name__c           = PARDOT_CONVERTED_OWNER_FULL_NAME;

		// 4-4-2018 the following are to be deprecated:
		cconf.AccountExpiration_Excluded_Stages__c			= ACCOUNTEXPIRATION_EXCLUDED_STAGES;
		cconf.Account_w_o_Financial_Info_Error_Message__c 	= ACCOUNT_WO_FINANCIAL_ERROR_MESSAGE;
		cconf.Account_w_o_Financial_Info_Limit_New_Usr__c 	= ACCOUNT_WO_FINANCIAL_NEW_USER_COUNT;
		cconf.Account_w_o_Financial_Info_Limit_Old_Usr__c 	= ACCOUNT_WO_FINANCIAL_OLD_USER_COUNT;
		cconf.Financial_Information_Fields__c 				= FINANCIAL_INFOMATION_FIELDS;
		cconf.Frequency_of_Hard_to_Crack__c 				= FREQUENCY_OF_HARD_TO_CRACK;
		cconf.New_User_For__c 								= NEW_USER_FOR;
		cconf.Hard_To_Crack_Times_Per_Company__c 			= HARDTOCRACK_TIMES_PER_COMPANY;
		cconf.Hard_to_Crack_Already_Marked_Message__c		= HARDTOCRACK_ALREADY_MARKED_MESSAGE;
		cconf.HardToCrack_MaxNumberOfCompanies__c 			= HARDTOCRACK_MAX_NUMBER_COMPANIES;
		cconf.HardToCrack_AmountOfTime__c 					= HARDTOCRACK_AMOUNT_OF_TIME;
		cconf.HardToCrack_ErrorMessage_Exceeded__c 			= HARDTOCRACK_ERRMSG_EXCEEDED;
		cconf.HardToCrack_ErrorMessage_NotAllowed__c 		= HARDTOCRACK_ERRMSG_NOTALLOWED;
		cconf.HardToCrack_ErrorMessage_NotOwner__c			= HARDTOCRACK_ERRMSG_NOTOWNER;
	}

	// Test method
	@isTest
	public static void CommonConfigUtil_Test()
	{
		test.startTest();

		// test new record
		Common_Config__c objDefaultConfigUtil = CommonConfigUtil.getConfig();
		System.debug('Inside The objDefaultConfigUtil'+objDefaultConfigUtil);

		test.stopTest();
	}
}