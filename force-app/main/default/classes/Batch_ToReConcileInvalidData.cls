/*
Class Name   :  Batch_ToReConcileInvalidData
Description  :  To correct incorrect Opportunity Data
Author       :  Sukesh
Created On   :  November 08 2019
*/

global with sharing class Batch_ToReConcileInvalidData implements Database.Batchable<sObject> {

    //String strQuery = 'SELECT Id, Mark_for_Deletion__c, StageName FROM Opportunity';
    String strQuery = 'SELECT Id, Name, Mark_for_Deletion__c, StageName FROM Opportunity WHERE RecordType.Name = \'Ignite Opportunity\' AND LeadSource <> \'Roundtable Attendee\' AND IsClosed = FALSE ORDER BY CloseDate DESC LIMIT 200';

    global Batch_ToReConcileInvalidData() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> lstOpportunity) {

        markOppForDeletion(lstOpportunity);
        updateOppStageToQualified(lstOpportunity);
        deleteCampInfluencesandContactRoles(lstOpportunity);
    }

     global void finish(Database.BatchableContext BC) {

    }

    /**
    @Description Method To mark opportunity for deletion
    @author Sukesh
    @date November 08 2019
    @param lstOpportunity List of Opportunity
    */
    private void markOppForDeletion(List<Opportunity> lstOpportunity) {

        system.debug('The following Opportunities were selected: ');
        for(Opportunity oppty : lstOpportunity){
            system.debug('  Id: ' + oppty.Id + '; Name: ' + oppty.Name);    
        }


        List<Opportunity> lstOppToUpdate = new List<Opportunity>();

        Set<String> setStatusNeeded = new Set<String>{'call set','meeting set','connected via email',' declined call','defer','rescheduled'};
        Set<Id> setCampaignIdToDeleteOpp = new Set<Id>();

        Map<Id, Set<CampaignInfluence>> mapCampaignIdToSetCampInfluence = new Map<Id, Set<CampaignInfluence>>();
        Map<Id, Set<CampaignInfluence>> mapCampaignIdToSetCampaignInfluence = new Map<Id, Set<CampaignInfluence>>();

        for(CampaignInfluence objCampaignInfluence: [SELECT Id,
                                                             CampaignId,
                                                             OpportunityId,
                                                             Opportunity.Target_Company__c,
                                                             Campaign.Type
                                                        FROM CampaignInfluence
                                                       WHERE OpportunityId IN : lstOpportunity]) {
            if(objCampaignInfluence.Campaign.Type == 'Portfolio Company Demand Gen') {

                if(!mapCampaignIdToSetCampInfluence.containsKey(objCampaignInfluence.CampaignId))
                    mapCampaignIdToSetCampInfluence.put(objCampaignInfluence.CampaignId, new Set<CampaignInfluence>{objCampaignInfluence});
                else {
                    mapCampaignIdToSetCampInfluence.get(objCampaignInfluence.CampaignId).add(objCampaignInfluence);
                }
            }
        }
        mapCampaignIdToSetCampaignInfluence.putAll(mapCampaignIdToSetCampInfluence);
        
        for(CampaignMember objCampaignMember : [SELECT Id,
                                                       Status,
                                                       Contact.AccountId,
                                                       CampaignId
                                                  FROM CampaignMember
                                                 WHERE CampaignId IN :mapCampaignIdToSetCampInfluence.keySet()]) {
            
            if(setStatusNeeded.contains((objCampaignMember.Status).toLowerCase())) {
                
                for(CampaignInfluence objCampaignInfluence : mapCampaignIdToSetCampInfluence.get(objCampaignMember.CampaignId)) {

                    if(objCampaignMember.Contact.AccountId == objCampaignInfluence.Opportunity.Target_Company__c) {

                        //setCampaignIdToDeleteOpp.remove(objCampaignMember.CampaignId);
                        if(mapCampaignIdToSetCampaignInfluence.containsKey(objCampaignMember.CampaignId))
                            mapCampaignIdToSetCampaignInfluence.remove(objCampaignMember.CampaignId);
                    }
                }
            }

            // else {
            //     setCampaignIdToDeleteOpp.add(objCampaignMember.CampaignId);
            // }
        }
        
        for(Id CampaignIdVal : mapCampaignIdToSetCampaignInfluence.keySet()) {

           // if(setCampaignIdToDeleteOpp.contains(CampaignIdVal)) {

                for(CampaignInfluence  objCampaignInfluence : mapCampaignIdToSetCampaignInfluence.get(CampaignIdVal)) {

                    lstOppToUpdate.add(new Opportunity(Id = objCampaignInfluence.OpportunityId,
                                                       Mark_for_Deletion__c = TRUE));
                }
            //}
        }

        system.debug('The following Opportunities will be marked for deletion: ');
        for(Opportunity oppty : lstOppToUpdate){
            system.debug('Id: ' + oppty.Id + '; Name: ' + oppty.Name);    
        }
        //update lstOppToUpdate;
    }

    /**
    @Description Method to update opportunity stage as Ignite Qualified
    @author Sukesh
    @date November 08 2019
    @param lstOpportunity List of Opportunity
    */
    private void updateOppStageToQualified(List<Opportunity> lstOpportunity) {

        List<Opportunity> lstOppToUpdate = new List<Opportunity>();

        Set<String> setStatusNeeded = new Set<String>{'attended','call set','meeting set','connected via email'};
        Set<Id> setCampaignIdToUpdateOpp = new Set<Id>();

        Map<Id, Set<CampaignInfluence>> mapCampaignIdToSetCampInfluence = new Map<Id, Set<CampaignInfluence>>();

        for(CampaignInfluence  objCampaignInfluence: [SELECT Id,
                                                             CampaignId,
                                                             OpportunityId,
                                                             Campaign.Type,
                                                             Contact.AccountId,
                                                             Opportunity.Target_Company__c
                                                             FROM CampaignInfluence
                                                             WHERE OpportunityId IN : lstOpportunity]) {

            if(!mapCampaignIdToSetCampInfluence.containsKey(objCampaignInfluence.CampaignId))
                mapCampaignIdToSetCampInfluence.put(objCampaignInfluence.CampaignId, new Set<CampaignInfluence>{objCampaignInfluence});
            else {
                mapCampaignIdToSetCampInfluence.get(objCampaignInfluence.CampaignId).add(objCampaignInfluence);
            }
        }
        
        for(CampaignMember objCampaignMember : [SELECT Id,
                                                       Status,
                                                       CampaignId,
                                                       Contact.AccountId
                                                       FROM CampaignMember
                                                       WHERE CampaignId IN :mapCampaignIdToSetCampInfluence.keySet()]) {
                                                        
            if(setStatusNeeded.contains((objCampaignMember.Status).toLowerCase())) {

                for(CampaignInfluence objCampaignInfluence : mapCampaignIdToSetCampInfluence.get(objCampaignMember.CampaignId)) {
                      
                    if(objCampaignMember.Contact.AccountId == objCampaignInfluence.Opportunity.Target_Company__c) {
                        //setCampaignIdToUpdateOpp.add(objCampaignMember.CampaignId);
                        lstOppToUpdate.add(new Opportunity(Id = objCampaignInfluence.OpportunityId,
                                                           StageName = 'Ignite Qualified'));
                    }
                }
            }
        }

        // for(Id CampaignIdVal : mapCampaignIdToSetCampInfluence.keySet()) {

        //     if(setCampaignIdToUpdateOpp.contains(CampaignIdVal)) {

        //         for(CampaignInfluence  objCampaignInfluence : mapCampaignIdToSetCampInfluence.get(CampaignIdVal)) {

        //             lstOppToUpdate.add(new Opportunity(Id=objCampaignInfluence.OpportunityId,
        //                                                StageName = 'Ignite Qualified'));
        //         }
        //     }
        // }

        system.debug('The following Opportunities will be update with Stage = Ignite Qualified: ');
        for(Opportunity oppty : lstOppToUpdate){
            system.debug('Id: ' + oppty.Id + '; Name: ' + oppty.Name);    
        }

        //update lstOppToUpdate;
    }

    /**
    @Description Method to delete Campaign Influences and Contact Roles
    @author Sukesh
    @date November 08 2019
    @param lstOpportunity List of Opportunity
    */
    private void deleteCampInfluencesandContactRoles(List<Opportunity> lstOpportunity) {

        List<CampaignInfluence> lstCampaignInfluenceToDelete = new List<CampaignInfluence>();
        List<OpportunityContactRole> lstOppContactRoleToDelete = new List<OpportunityContactRole>();

        Set<String> setStatusNeeded = new Set<String>{'call set','meeting set','connected via email',' declined call','defer','rescheduled'};
        Set<Id> setCampaignIdToDeleteInfluence = new Set<Id>();
        Set<Id> setContactIdRoleNotToDelete = new Set<Id>();
        Set<Id> setContactIdRoleToDelete = new Set<Id>();

        Map<Id, Set<CampaignInfluence>> mapCampaignIdToSetCampInfluence = new Map<Id, Set<CampaignInfluence>>();
        // Map<Id, Set<Id>> mapCampaignIdToSetContactId = new Map<Id, Set<Id>>();

        for(CampaignInfluence  objCampaignInfluence: [SELECT Id,
                                                             CampaignId,
                                                             OpportunityId,
                                                             Opportunity.Target_Company__c,
                                                             Campaign.Type,
                                                             ContactId,
                                                             Campaign.Name,
                                                             Contact.Name,
                                                             Opportunity.Name
                                                             FROM CampaignInfluence
                                                             WHERE OpportunityId IN : lstOpportunity]) {

            if(objCampaignInfluence.Campaign.Type == 'Portfolio Company Demand Gen') {

                if(!mapCampaignIdToSetCampInfluence.containsKey(objCampaignInfluence.CampaignId))
                    mapCampaignIdToSetCampInfluence.put(objCampaignInfluence.CampaignId, new Set<CampaignInfluence>{objCampaignInfluence});
                else {
                    mapCampaignIdToSetCampInfluence.get(objCampaignInfluence.CampaignId).add(objCampaignInfluence);
                }

                // if(!mapCampaignIdToSetContactId.containsKey(objCampaignInfluence.CampaignId))
                //     mapCampaignIdToSetContactId.put(objCampaignInfluence.CampaignId, new Set<Id>{objCampaignInfluence.ContactId});
                // else {
                //     mapCampaignIdToSetContactId.get(objCampaignInfluence.CampaignId).add(objCampaignInfluence.ContactId);
                // }
            }
        }

        for(CampaignMember objCampaignMember : [SELECT Id,
                                                       Status,
                                                       CampaignId,
                                                       ContactId,
                                                       Contact.AccountId,
                                                       Campaign.Name,
                                                       Contact.Name
                                                       FROM CampaignMember
                                                       WHERE CampaignId IN :mapCampaignIdToSetCampInfluence.keySet()]) {

            if(!setStatusNeeded.contains((objCampaignMember.Status).toLowerCase())){
               
                for(CampaignInfluence objCampaignInfluence : mapCampaignIdToSetCampInfluence.get(objCampaignMember.CampaignId)) {
                    
                    if(objCampaignMember.Contact.AccountId == objCampaignInfluence.Opportunity.Target_Company__c) {
                        
                        setCampaignIdToDeleteInfluence.add(objCampaignMember.CampaignId);
                        setContactIdRoleToDelete.add(objCampaignMember.ContactId);
                    }
                }
            }

            else {
                setContactIdRoleNotToDelete.add(objCampaignMember.ContactId);
            }
        }

        for(Id CampaignIdVal : mapCampaignIdToSetCampInfluence.keySet()) {

            if(setCampaignIdToDeleteInfluence.contains(CampaignIdVal)) {

                for(CampaignInfluence objCampaignInfluence : mapCampaignIdToSetCampInfluence.get(CampaignIdVal)) {

                    lstCampaignInfluenceToDelete.add(new CampaignInfluence(Id = objCampaignInfluence.Id));
                }
            }
        }

        system.debug('The following Campaign Influences will be deleted: ');
        for(CampaignInfluence ci : lstCampaignInfluenceToDelete){
            //system.debug('  Oppty Id: ' + ci.OpportunityId + '; Oppty Name: + ' + ci.Opportunity.Name + '; Campaign Id: ' + ci.CampaignId + ' Campaign Name: + ' + ci.Campaign.Name + '; Contact Id: ' + ci.ContactId + ' Contact Name: + ' + ci.Contact.Name);    
            system.debug('  Campaign Influence Id: ' + ci.Id);    
        }
        //delete lstCampaignInfluenceToDelete;

        for(Id contactIdToDelete : setContactIdRoleToDelete) {

            if(setContactIdRoleNotToDelete.contains(contactIdToDelete))
                setContactIdRoleToDelete.remove(contactIdToDelete);
        }

        for(OpportunityContactRole objOpportunityContactRole : [SELECT Id,
                                                                       ContactId,
                                                                       Opportunity.Name,
                                                                       Contact.Name
                                                                       FROM OpportunityContactRole
                                                                       WHERE OpportunityId IN: lstOpportunity
                                                                       AND ContactId IN: setContactIdRoleToDelete]) {

            //if(setContactIdRoleToDelete.contains(objOpportunityContactRole.ContactId)) {

            lstOppContactRoleToDelete.add(new OpportunityContactRole(Id = objOpportunityContactRole.Id));
            //}

        }

        system.debug('The following Opportunity Contact Roles will be deleted: ');
        for(OpportunityContactRole ocr : lstOppContactRoleToDelete){
            //system.debug('  Oppty Id: ' + ci.OpportunityId + '; Oppty Name: + ' + ci.Opportunity.Name + '; Contact Id: ' + ci.ContactId + ' Contact Name: + ' + ci.Contact.Name);    
            system.debug('OpptyContactRole Id: ' + ocr.Id);    
        }
        //delete lstOppContactRoleToDelete;
    }
}