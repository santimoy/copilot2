/*
    @description    CompanyLeadController Tests
    @date           12th July 2016
    @author         Simon Coles
    @rev            Mike Gill updated to use GLOBAL TestFactory

    @todo

*/

@isTest
public class CompanyLeadController_Test {

    @isTest static void testInitialRecordSetReturned() {

        List<Account> accts = new List<Account>();
        List<CompanyLead__c> cleads = new List<CompanyLead__c>();

        // Create 5 Test Accounts
        accts = TestFactory.createAccounts(5);
        //System.debug(LoggingLevel.ERROR, 'List of Accts ' + accts);
        // Create 5 Company Lead Records 3 of which are linked to Account Records
        cleads = TestFactory.createCompanyLeads(5, accts);
        //System.debug(LoggingLevel.ERROR, 'List of Cleads ' + cleads);

        // Initiate the Standard Controller
        Test.Starttest();
        // Setup the Link to the VF Page
        PageReference pageRef = Page.CompanyLead; //replace with your VF page name
        Test.setCurrentPage(pageRef);
        // Initiate the Controller
        CompanyLeadController controller = new CompanyLeadController();
        controller = new CompanyLeadController();

        // Run the initial Get Records
        controller.getCurrentCompanyList();
        controller.nextPage();
        controller.prePage();
        controller.firstPage();
        controller.lastPage();
        controller.buildQuery();
        // Test Initial Records Counted match the number created 5
        System.assertEquals(5, controller.totalFilteredRecords);
        Test.Stoptest();
    }

    @isTest static void testAllFilters() {

        List<Account> accts = new List<Account>();
        List<CompanyLead__c> cleads = new List<CompanyLead__c>();

        // Create 5 Test Accounts
        accts = TestFactory.createAccounts(5);
        // Create 5 Company Lead Records 3 of which are linked to Account Records
        cleads = TestFactory.createCompanyLeads(5, accts);

        // Initiate the Standard Controller
        Test.Starttest();
        // Setup the Link to the VF Page
        PageReference pageRef = Page.CompanyLead;
        Test.setCurrentPage(pageRef);
        // Initiate the Controller
        CompanyLeadController controller = new CompanyLeadController();
        controller = new CompanyLeadController();

        // Load the Controller with Various Filters and test results.
        // These filters should bring back all records
        controller.companyNameFilter = 'TestCLead';
        controller.empCountFilter_minVal = 1;
        controller.empCountFilter_maxVal = 100;
        controller.empCount6MthGrowthFilter_minVal = 1;
        controller.empCount6MthGrowthFilter_maxVal = 100;
        controller.stageFilter = 'pre series a';
        controller.locationFilter = 'united kingdom';
        controller.verticalFilter = 'Advertising';
        controller.buildQuery();
        /*
        date myDate = date.today();
        String dayString = myDate.format();
        String[] dts = dayString.split('/');
        String newDate = dts[1] + '/' + dts[0] + '/' + dts[2];
        controller.fundatingDateFromFilter = newDate;
        controller.fundatingDateToFilter = newDate;
        controller.fundAmountFilter_minVal = 1000;
        controller.fundAmountFilter_maxVal = 1000;
        controller.meetCallDateFromFilter = newDate;
        controller.meetCallDateToFilter = newDate;
        */

        // Get Records
        controller.getCurrentCompanyList();
        //System.debug(LoggingLevel.ERROR, 'Records Returned ' + controller.CompanyList);

        // Test Initial Records Counted match the number created 5
        //System.assertEquals(5, controller.totalFilteredRecords);
        Test.Stoptest();
    }
}