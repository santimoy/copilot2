public with sharing class M2M_NewController {
	public static final String PARAM_SEARCHCONFIGNAME = 'scn';
	public static final String PARAM_LINKCONFIGNAME = 'lcn';
	public static final String PARAM_FROMID = 'fid';
	public static final String PARAM_FROMNAME = 'fn';
	public static final String PARAM_RETURL = 'retURL';

	// Properties 	
	public String  fromName{get; private set;}
	public M2M_NewController controller{get;private set;}
	
	public String searchTerm{get;set;}
	public List<sObject> searchResults{get;private set;}
	public boolean searchPerformed{get;private set;}
	public list<Schema.FieldSetMember> searchFieldSet{get; private set;}
	
	public String  m2mLinkUrlBase{get;private set;}
	public String  searchObjectPrefix{get;private set;}
	
	// Properties - label merges
	public String searchPageTitle{get{ return String.format(System.Label.M2M_SearchPageTitle, new string[]{objectLabel}); }}
	public String searchInitialMsg{get{ return String.format(System.Label.M2M_SearchInitialMsg, new string[]{objectLabel}); }}
	
	// Controller private variables
	private String searchConfigName;
	private String linkConfigName;
	private String retUrl; 
	private String fromId;
	private M2M_Config__c currSearchConfig;
	private Integer maxSearchResults;
	private Schema.sObjectType sObjToken;
	private string objectLabel;
	
	public M2M_NewController(){
		map<String, String> params = ApexPages.currentPage().getParameters(); 
		searchConfigName = params.get(PARAM_SEARCHCONFIGNAME);
		linkConfigName = params.get(PARAM_LINKCONFIGNAME);
		retUrl = params.get(PARAM_RETURL);
		fromId = params.get(PARAM_FROMID);
		fromName = params.get(PARAM_FROMNAME);
		
		if (searchConfigName!=null && searchConfigName!='') currSearchConfig = M2M_Config__c.getInstance(searchConfigName);
		maxSearchResults = currSearchConfig.SearchResultLimit__c.intValue();

		searchTerm = '';		
		searchResults = new List<sObject>();
		searchPerformed = false;
		
		m2mLinkUrlBase = generateM2MLinkUrl()+'&'+M2M_LinkController.PARAM_TOID+'=';
		
		sObjToken = Schema.getGlobalDescribe().get(currSearchConfig.SearchObjectName__c);
		Schema.DescribesObjectResult sObjDescribe = sObjToken.getDescribe(); 
		searchObjectPrefix = sObjDescribe.getKeyPrefix();
		objectLabel = sObjDescribe.getLabel();
		
		controller = this;
	}
	
	public PageReference goSearch(){
		searchPerformed = false;
		
		if (searchTerm!=null && searchTerm.trim()!=''){
			if (searchTerm.trim().length()<2){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_SearchTooShort));
			}
			else{
				
				Integer maxSize = maxSearchResults + 1;
				String escapedSearchTerm = escapeSearchString(searchTerm);
				
				// get fields from search Field Set
				Schema.FieldSet fset = sObjToken.getDescribe().fieldSets.getMap().get(currSearchConfig.SearchFieldSet__c);
				searchFieldSet = fset.getFields();
				
				string fieldString = '';
				for (FieldSetMember currMem : searchFieldSet){
					fieldString += currMem.getFieldPath() + ',';	
				}
				fieldString = fieldString.subString(0,fieldString.length()-1);
				
				String soslQuery = 'FIND {'+escapedSearchTerm+'*} RETURNING '+currSearchConfig.SearchObjectName__c+' ('+fieldString+') LIMIT '+maxSize;  
				System.debug('soslQuery:'+soslQuery);
				List<List<SObject>> searchList=search.query(soslQuery);
				
				searchResults = searchList[0];
				if (searchResults.size()==maxSize){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 
						String.format(System.Label.M2M_TooManyResults, new String[]{maxSearchResults.format()}) ));
					searchResults.remove(maxSize-1);
				}
				
				searchPerformed = true;
			}
		}
		else{
			searchResults = new List<sObject>();
		}
		return null;
	}
	
	public PageReference newRecord(){
		String newRecordUrl = '/'+searchObjectPrefix+'/e';
		PageReference pr = new PageReference(newRecordUrl);
		map<string, string> params = pr.getParameters();

		String targetSaveUrl = generateM2MLinkUrl();
			
		params.put('saveURL', targetSaveUrl);
		params.put('cancelURL', retUrl);
		params.put('retURL', retUrl);
		// need to figure out how to deal with the Save and New button
		//  must be of the format /<prefix>/e?
		//  need to override the new url and do some fancy override=1 and override=0
//		params.put('save_new_url', targetSaveUrl);
		
		return pr;
	}
	
	private String generateM2MLinkUrl(){
		return '/apex/m2m_link?'+
			M2M_LinkController.PARAM_LINKCONFIGNAME+'='+linkConfigName+'&'+
			M2M_LinkController.PARAM_RETURL+'='+retUrl+'&'+
			M2M_LinkController.PARAM_FROMID+'='+fromId;
	}
	private string escapeSearchString(string searchString){
		return searchString.replaceAll('\\?','\\\\?').replaceAll('&','\\\\&').replaceAll('\\|','\\\\|').replaceAll('!','\\\\!')
			.replaceAll('\\{','\\\\{').replaceAll('\\}','\\\\}').replaceAll('\\[','\\\\[').replaceAll('\\]','\\\\]')
			.replaceAll('\\(','\\\\(').replaceAll('\\)','\\\\)').replaceAll('\\^','\\\\^').replaceAll('~','\\\\~')
			.replaceAll(':','\\\\:').replaceAll('\\\\','\\\\').replaceAll('"','\\\\"').replaceAll('\'','\\\\\'')
			.replaceAll('\\+','\\\\+').replaceAll('-',' ');//.replaceAll('-','\\\\-');
	}
	
	// Test methods ----------------------------------------------------
	public static testMethod void testThisClass(){
		
		M2M_Config__c testConfig = new M2M_Config__c(Name='testSearchConfig',  
			SearchFieldSet__c='CompanySearchFields', SearchObjectName__c='Account', SearchResultLimit__c=1);
		insert testConfig;
		
		Account testCompany = new Account(name='testCompany');
		insert testCompany;
		
		PageReference testPageReference = Page.M2M_New; 
		map<string, string> params = testPageReference.getParameters();
		params.put(M2M_NewController.PARAM_SEARCHCONFIGNAME, 'testSearchConfig');
		params.put(M2M_NewController.PARAM_LINKCONFIGNAME, 'testLinkConfig');
		params.put(M2M_NewController.PARAM_FROMID, testCompany.Id);
		params.put(M2M_NewController.PARAM_FROMNAME, testCompany.Name);
		params.put(M2M_NewController.PARAM_RETURL, '/'+testCompany.Id);
		
		Test.setCurrentPage(testPageReference);
		M2M_NewController testController = new M2M_NewController();
		
		// check defaults
		System.assertEquals('' ,testController.searchTerm);
		System.assertEquals(0 ,testController.searchResults.size());
		System.assertEquals(false ,testController.searchPerformed);
		System.assertEquals(1 ,testController.maxSearchResults);
		System.assertEquals('testCompany' ,testController.fromName);
		
		String m2mLink = '/apex/m2m_link?'+
			M2M_LinkController.PARAM_LINKCONFIGNAME+'=testLinkConfig&'+
			M2M_LinkController.PARAM_RETURL+'=/'+testCompany.Id+'&'+
			M2M_LinkController.PARAM_FROMID+'='+testCompany.Id+'&'+
			M2M_LinkController.PARAM_TOID+'=';
		System.assertEquals(m2mLink ,testController.m2mLinkUrlBase);
		
		// setup a search
		Account testCompetitor1 = new Account(name='testCompetitor1');		
		Account testCompetitor2 = new Account(name='testCompetitor2');
		insert new Account[]{testCompetitor1, testCompetitor2};
		
		testController.searchTerm = 'testComp';
		Test.setFixedSearchResults(new Id[]{testCompetitor1.Id, testCompetitor2.Id});
		testController.goSearch();
		
		System.assertEquals(1 ,testController.searchResults.size());
		System.assertEquals(true ,testController.searchPerformed);
		
		
		// setup a bad search
		testController.searchTerm = '    ';
		testController.goSearch();
		
		System.assertEquals(0 ,testController.searchResults.size());
		System.assertEquals(false ,testController.searchPerformed);


		// setup a too short search
		testController.searchTerm = 'a';
		testController.goSearch();
		
		System.assertEquals(0 ,testController.searchResults.size());
		System.assertEquals(false ,testController.searchPerformed);

		// check the new link
		PageReference newRecordPR = testController.newRecord();
		map<string,string> newRecordParams = newRecordPR.getParameters();
		
		String newLink = '/apex/m2m_link?'+
			M2M_LinkController.PARAM_LINKCONFIGNAME+'=testLinkConfig&'+
			M2M_LinkController.PARAM_RETURL+'=/'+testCompany.Id+'&'+
			M2M_LinkController.PARAM_FROMID+'='+testCompany.Id;
			
		System.assert(newRecordPR.getUrl().startsWith('/001/e'));
		System.assertEquals(newLink ,newRecordParams.get('saveURL'));
		System.assertEquals('/'+testCompany.Id ,newRecordParams.get('cancelURL'));
		System.assertEquals('/'+testCompany.Id ,newRecordParams.get('retURL'));
		/**/	
	}
}