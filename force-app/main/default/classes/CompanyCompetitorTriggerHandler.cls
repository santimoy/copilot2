public with sharing class CompanyCompetitorTriggerHandler {
  /**/
  public void onAfterInsert(list<CompanyCompetitor__c> trigRecsNew){
    
    list<CompanyCompetitor__c> invRecs = new list<CompanyCompetitor__c>();
    for(CompanyCompetitor__c currRec : trigRecsNew){
      if (currRec.InverseCompanyCompetitor__c!=null) continue; 
      
      invRecs.add(new CompanyCompetitor__c(Company__c=currRec.Competitor__c,
        Competitor__c=currRec.Company__c, InverseCompanyCompetitor__c=currRec.Id));
    }
    if (invRecs.size()>0 ) insert invRecs;
    
    list<CompanyCompetitor__c> updtRecs = new list<CompanyCompetitor__c>();
    for(CompanyCompetitor__c currInvRec : invRecs){
      updtRecs.add(new CompanyCompetitor__c(Id=currInvRec.InverseCompanyCompetitor__c, InverseCompanyCompetitor__c=currInvRec.Id));
    }
    if (updtRecs.size()>0 ) update updtRecs;
  }
  
  public void onAfterDelete(list<CompanyCompetitor__c> trigRecsOld){
    
    list<CompanyCompetitor__c> invRecs = new list<CompanyCompetitor__c>();
    for(CompanyCompetitor__c currRec : trigRecsOld){
      if (currRec.InverseCompanyCompetitor__c==null) continue; 
      
      invRecs.add(new CompanyCompetitor__c(Id=currRec.InverseCompanyCompetitor__c));
    }
    if (invRecs.size()>0 ) delete invRecs;
  }
  
  // Test Methods
  static testMethod void testThisClass(){
    // insert test
    Account testCompany = new Account(Name='testCompany');
    Account testCompetitor = new Account(Name='testCompetitor');
    insert new Account[]{testCompany, testCompetitor};
    
    CompanyCompetitor__c testRec = new CompanyCompetitor__c(Company__c=testCompany.Id, Competitor__c=testCompetitor.Id);
    insert testRec;
    testRec = [select id, InverseCompanyCompetitor__c, Competitor__c, Company__c from CompanyCompetitor__c where Id = :testRec.Id];
    System.assertNotEquals(null, testRec.InverseCompanyCompetitor__c);
    
    CompanyCompetitor__c testInv = [select id, Company__c, Competitor__c, InverseCompanyCompetitor__c
      from CompanyCompetitor__c where id = :testRec.InverseCompanyCompetitor__c];
    System.assertEquals(testRec.Id, testInv.InverseCompanyCompetitor__c);
    System.assertEquals(testRec.Company__c, testInv.Competitor__c);
    System.assertEquals(testRec.Competitor__c, testInv.Company__c);
    
    
    // delete test
    delete testRec;
    list<CompanyCompetitor__c> testDelList = [select id from CompanyCompetitor__c where id = :testRec.InverseCompanyCompetitor__c];
    System.assertEquals(0, testDelList.size());
  }
  /**/
}