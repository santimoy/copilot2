/**
* @author 10k advisor
* @date 2018-07-05
* @className IVPTopicAssignmentTriggerHandler
* @group IVPSmartDash
*
* @description contains methods to handle TopicAssignment Functionality
*/
public class IVPTopicAssignmentTriggerHandler {
    public static void handlerAfterProcess(List<TopicAssignment> lstOfTopicAssignment){
        Map<Id,Id> actAndTopicIds = new Map<Id,Id>();
        Map<Id,String> topicNamesandEntityId = new Map<Id,String>();
        Map<Id,Account> accountTagUpdate = new Map<Id,Account>();
        boolean check=false;
        for(TopicAssignment topicAssign: lstOfTopicAssignment){
            if(((String)topicAssign.EntityId).startsWith('001')){
                actAndTopicIds.put(topicAssign.Id,topicAssign.EntityId);
            }
        }
        
        for(TopicAssignment topicAssignOld: [SELECT EntityId,Topic.Name 
                                             FROM TopicAssignment 
                                             WHERE Id IN : actAndTopicIds.keyset()]){
                                                 String topicName = topicAssignOld.Topic.Name+';';
                                                 if(topicNamesandEntityId.containskey(topicAssignOld.EntityId)){
                                                     topicName+=topicNamesandEntityId.get(topicAssignOld.EntityId);
                                                 }
                                                 topicNamesandEntityId.put(topicAssignOld.EntityId,topicName); 
                                             }
        
        for(Account act: [SELECT Id, Tags__c FROM Account WHERE Id IN: actAndTopicIds.Values()]){
            if(act.Tags__c == null){
                act.Tags__c='';
            }
            
            if(topicNamesandEntityId.containsKey(act.Id)){
                String oldTags = act.Tags__c;
                String topicNames = topicNamesandEntityId.get(act.Id);
                for(String tag: topicNames.split(';')){
                    if(!(oldTags.startsWith(tag+';') || oldTags.contains(';'+tag+';'))){
                        Integer newTopicLength = topicNames.length();
                        Integer oldTopicLength = oldTags.length();
                        if((oldTopicLength+newTopicLength)>255){
                            oldTags=oldTags.subString(0, oldTags.lastIndexOf(';',254-newTopicLength)+1);
                            oldTags= topicNames+oldTags;
                        }else{
                            oldTags= topicNames+oldTags; 
                        }
                        
                    }
                }
                act.Tags__c=oldTags;
            }
            accountTagUpdate.put(act.Id,act);
        }
        
        if(!accountTagUpdate.isEmpty()){
            IVPUtils.updateRecords(accountTagUpdate.values());
        }
    }
    
    public static void handlerBeforeDeleteProcess(List<TopicAssignment> lstOfTopicAssignment){
        Map<Id,Id> actAndTopicIds = new Map<Id,Id>();
        Map<Id,String> topicNamesandEntityId = new Map<Id,String>();
        Map<Id,Account> accountTagUpdate = new Map<Id,Account>();
        boolean check=false;
        
        for(TopicAssignment topicAssign: lstOfTopicAssignment){
            if(((String)topicAssign.EntityId).startsWith('001')){
                actAndTopicIds.put(topicAssign.Id,topicAssign.EntityId);
            }
        }
        
        for(TopicAssignment topicAssignOld: [SELECT EntityId,Topic.Name FROM TopicAssignment 
                                             WHERE Id IN : actAndTopicIds.keyset()]){
                                                 String topicName = topicAssignOld.Topic.Name+';';
                                                 if(topicNamesandEntityId.containskey(topicAssignOld.EntityId)){
                                                     topicName+=topicNamesandEntityId.get(topicAssignOld.EntityId);
                                                 }
                                                 topicNamesandEntityId.put(topicAssignOld.EntityId,topicName); 
                                             }
        
        for(Account act: [SELECT Id, Tags__c FROM Account WHERE Id IN: actAndTopicIds.Values()]){
            if(act.Tags__c == null){
                act.Tags__c='';
            }
            
            if(topicNamesandEntityId.containsKey(act.Id)){
                String oldTags = act.Tags__c;
                String topicNames = topicNamesandEntityId.get(act.Id);
                for(String tag: topicNames.split(';')){
                    if(oldTags.startsWith(tag+';') || oldTags.contains(';'+tag+';')){
                        oldTags= oldTags.replaceAll(tag+';','');
                    }
                }
                act.Tags__c=oldTags;
            }
            accountTagUpdate.put(act.Id,act);
        }
        
        if(!accountTagUpdate.isEmpty()){
            IVPUtils.updateRecords(accountTagUpdate.values());
        }
    }
}