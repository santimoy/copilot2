/**
*@name      	: OppAuto_Service_Test
*@developer 	: 10K developer
*@date      	: 2018-11-22
*@description   : The class used as test class of OpportunityTriggerHandler
**/
@isTest
public class OpportunityTriggerHandler_Test {
    @testSetup static void testSetup() {
        //Creating custom setting
        List<IVP_General_Config__c> gcLst = new List<IVP_General_Config__c>();
        gcLst.add(new IVP_General_Config__c(Name='OppAuto_UserName',Value__c='alessandro@insightpartners.com'));
        gcLst.add(new IVP_General_Config__c(Name='Campaign Member Status',Value__c='Attended,Follow up 1,Follow up 2,Follow up 3,Call Set,Meeting Set,Connected via Email,No Response,Defer,Declined,Reschedule'));
        insert gcLst;
        
        //Creating account
    }  
    public static testMethod void  testOpportunityDeletion(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = OppAuto_ProcessCampaignMemberBtch_Test.prepareData('Attended','Attended', 'IB_RT_Case1', false, 'Dinner / Roundtable', 'Open', 'Check with portfolio'
                                       , 0,1, contacts);
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        List<Opportunity> oppsToTest = [SELECT Id,Description,Name,NextStep,StageName FROM Opportunity];
        System.assertEquals(1, [SELECT Id,Influenced_Opportunities__c FROM Campaign WHERE Id = :campObj.Id][0].Influenced_Opportunities__c);     
        System.assertEquals(1,[SELECT Id FROM CampaignInfluence WHERE OpportunityID = :oppsToTest[0].Id].size());
        delete oppsToTest;
        System.assertEquals(0,[SELECT Id FROM CampaignInfluence WHERE OpportunityID = :oppsToTest[0].Id].size());
        System.assertEquals(0, [SELECT Id,Influenced_Opportunities__c FROM Campaign WHERE Id = :campObj.Id][0].Influenced_Opportunities__c);     
    }

}