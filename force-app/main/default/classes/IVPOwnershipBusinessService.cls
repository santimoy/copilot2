/***********************************************************************************
 * @author      10k-Expert
 * @name		IVPOwnershipBusinessService
 * @date		2018-11-11
 * @description	This class contains all the generic methods related to BusinessHour, working day calculation. 
 ***********************************************************************************/
public class IVPOwnershipBusinessService {
    
    public class BusinessDaysWrapper{
        public DateTime businessDate;
        public Integer interval;
        public String dateKey;
        public Boolean isHourgap;
        private DateTime startDate;
        private Integer initInterval;
        
        
        public BusinessDaysWrapper(Boolean isHourgap, DateTime startDate){
            this.startDate = startDate;
            businessDate = startDate;
            // default interval as 1 for next working day
            initInterval = interval = 24;
            dateKey = startDate.format('YYYY-MM-dd');
            isHourgap = false;
        }
        
        public BusinessDaysWrapper(DateTime startDate){
            this(false, startDate);
        }
        
        public BusinessDaysWrapper(Boolean isHourgap, DateTime startDate, Integer interval){
            this.isHourgap = isHourgap;
            this.startDate = startDate;
            businessDate = startDate;
            initInterval = this.interval = interval;
            dateKey = startDate.format('YYYY-MM-dd');
        }
        
        public BusinessDaysWrapper(DateTime startDate, Integer interval){
            this(false, startDate, interval);
        }
        
        public DateTime getStartDate(){
            return startDate;
        }
        
        public void setStartDate(DateTime startDate){
            this.startDate = startDate;
        }
        
        public Integer getInterval(){
            return initInterval;
        }
        
    }
    
    /***********************************************************************
     * @description preparing next work days with respect to single passed BusinessDaysWrapper data
     * @return void
     * @author 10K-Expert
     * @param BusinessDaysWrapper source data
     ***********************************************************************/
    public static void prepareNextWorkingDay(BusinessDaysWrapper businessDate){
        prepareNextWorkingDay(new List<BusinessDaysWrapper>{businessDate});
    }
    
    /***********************************************************************
     * @description preparing next work days with respect to passed list BusinessDaysWrapper data
     * @return void
     * @author 10K-Expert
     * @param List of BusinessDaysWrapper source data
     ***********************************************************************/
    public static void prepareNextWorkingDay(List<BusinessDaysWrapper> businessDateLst){
        List<Holiday> holidays = getFutureHolidays(System.today());
        processBusinessDays(businessDateLst, holidays);
    }
    
    /***********************************************************************
     * @description preparing next work days with respect to passed list BusinessDaysWrapper data, and passsed pastdate to get holidays from that date
     * @return void
     * @author 10K-Expert
     * @param List of BusinessDaysWrapper source data, Date pastDate for holidays
     ***********************************************************************/
    public static void prepareNextWorkingDayWithPastHoliday(List<BusinessDaysWrapper> businessDateLst, Date pastDate){
        Date holidayDateTime = System.today();
        holidayDateTime =  holidayDateTime < pastDate ? holidayDateTime : pastDate;
        List<Holiday> holidays = getFutureHolidays(holidayDateTime);
        processBusinessDays(businessDateLst, holidays);
    }
    
    /***********************************************************************
     * @description preparing working days by taking future holiday from todays
     * @return void
     * @author 10K-Expert
     * @param List of BusinessDaysWrapper source data
     ***********************************************************************/
    public static void prepareWorkingDays(List<BusinessDaysWrapper> businessDateLst){
        List<Holiday> holidays = getHolidays();
        processBusinessDays(businessDateLst, holidays);
    }
    
    /***********************************************************************
     * @description preparing working days with respect to pass holidays list
     * @return void
     * @author 10K-Expert
     * @param List of BusinessDaysWrapper source data, List of holidays
     ***********************************************************************/
    private static void processBusinessDays(List<BusinessDaysWrapper> businessDateLst, List<Holiday> holidays){
        Set<String> holidaysSet = new Set<String>();
        for(Holiday off : holidays){
            if(off.IsAllDay){
                holidaysSet.add(off.ActivityDate.format());
            }
        }
        Map<String, BusinessDaysWrapper> businessHourMap = new Map<String, BusinessDaysWrapper>();
        for(BusinessDaysWrapper dateObj: businessDateLst){
            String key = dateObj.dateKey+':'+dateObj.initInterval;
            if(!businessHourMap.containsKey(key)){
                
                dateObj = extractNextWorkingDay(dateObj, holidaysSet);
                businessHourMap.put(key, dateObj);
                
            }else{
                BusinessDaysWrapper processedDateObj = businessHourMap.get(key);
                dateObj.businessDate = processedDateObj.businessDate;
                dateObj.interval = processedDateObj.interval;
            }
        }
        System.debug('businessHourMap :: '+businessHourMap);
    }
    
    /***********************************************************************
     * @description Getting extact next working day for a businessday data with respect to holidays and excluding SAT, SUN as well.
     * @return void
     * @author 10K-Expert
     * @param BusinessDaysWrapper source data, List of holidays
     ***********************************************************************/
    private static BusinessDaysWrapper extractNextWorkingDay(BusinessDaysWrapper businessDate, Set<String> holidays){
        Datetime result = businessDate.businessDate;
        Boolean isReverse = businessDate.interval < 0;
        businessDate.interval = Math.abs(businessDate.interval);
        Integer intervalDiff  = isReverse ? -1 : 1;
        for(Integer i = businessDate.isHourgap ? -1 : 0; i < businessDate.interval;){
            Integer diff = intervalDiff;
            if(businessDate.isHourgap){
                 diff *= (businessDate.interval > 24 ? 24 : businessDate.interval);
                 // to control iteration
                 businessDate.interval-=24;
            }else{
                // to control iteration
                 i++;
            }
            
            DateTime tempDate = businessDate.isHourgap ? result.addHours(diff) : result.addDays(diff) ;
            String tempDateDayFormat = tempDate.format('EEE');
            String tempDateFormat = tempDate.format('M/d/YYYY');
            // assuming Sat, Sun as off
            Boolean isIterate = true;
            do {
                isIterate = false;
                if(tempDateDayFormat == 'Sat' || tempDateDayFormat == 'Sun' || holidays.contains(tempDateFormat)){
                    tempDate = tempDate.addDays(intervalDiff);
                    tempDateDayFormat = tempDate.format('EEE'); 
                    tempDateFormat = tempDate.format('M/d/YYYY');
                    isIterate = true;
                }
            } while(isIterate);
            result = tempDate;
        }
        
        businessDate.interval = 0;
        Date resultDate = result.Date();
        Date startDate = businessDate.getStartDate().Date();
        businessDate.businessDate = result;
        if(businessDate.interval != 0){
            businessDate.setStartDate(result.addDays(intervalDiff));
            businessDate = extractNextWorkingDay(businessDate, holidays);
        }
        return businessDate;
    }
    
    /***********************************************************************
     * @description returning list of future holidays upto 50k from  specified date as need to calculte business days
     * @return List of holiday 
     * @author 10K-Expert
     * @param nothing
     ***********************************************************************/
    private static List<Holiday> getFutureHolidays(Date fromDate){
        return [SELECT Id, ActivityDate, StartTimeInMinutes, EndTimeInMinutes, IsAllDay FROM Holiday
                WHERE ActivityDate >= :fromDate
                ORDER BY ActivityDate
                LIMIT 50000];
    }
    
    /***********************************************************************
     * @description returning list of holidays upto 50k in the order of latest holidays from the last 1 months as need to calculte business days
     * @return List of holiday 
     * @author 10K-Expert
     * @param nothing
     ***********************************************************************/
    private static List<Holiday> getHolidays(){
        Date oneMonthsAgo = System.today().addMonths(-1);
        return [SELECT Id, ActivityDate, StartTimeInMinutes, EndTimeInMinutes, IsAllDay FROM Holiday
                WHERE ActivityDate >=: oneMonthsAgo
                ORDER BY ActivityDate DESC
                LIMIT 50000];
    }
	/*Usage-Example
	    // need a DateTime as Source
    	DateTime sDate = System.now();
        
        // need to create instance of buniessDay
        ***specify Date and interval, default is 1
        IVPOwnershipBusinessService.BusinessDaysWrapper sD = new IVPOwnershipBusinessService.BusinessDaysWrapper(sDate, 2);
        
        //call method the following method to prepare next working with specified BuniessDay
        IVPOwnershipBusinessService.prepareNextWorkingDay(sD);
        //to use the nextworking day's datetime use the property of <businessDayInstance>.businessDate
        
        We can also process a list to prepare nextworking day
        DateTime slDate = System.now();
        List < IVPOwnershipBusinessService.BusinessDaysWrapper > businessDateLst = new List < IVPOwnershipBusinessService.BusinessDaysWrapper > ();
        businessDateLst.add(new IVPOwnershipBusinessService.BusinessDaysWrapper(slDate, 3));
        businessDateLst.add(new IVPOwnershipBusinessService.BusinessDaysWrapper(slDate, 2));
        businessDateLst.add(new IVPOwnershipBusinessService.BusinessDaysWrapper(slDate, 10));
        for (IVPOwnershipBusinessService.BusinessDaysWrapper sds: businessDateLst) {
            // show source values
            // System.debug(sds);
        }
        IVPOwnershipBusinessService.prepareNextWorkingDay(businessDateLst);
        for (IVPOwnershipBusinessService.BusinessDaysWrapper sds: businessDateLst) {
            // show processed values
            // System.debug(sds);
            
            //to use the nextworking day's datetime use the property of <businessDayInstance>.businessDate
        }
	*/
}