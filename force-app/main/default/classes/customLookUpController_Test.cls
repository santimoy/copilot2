@isTest
public class customLookUpController_Test {
    
     public static testmethod void Method1()
    {
        Sourcing_Preference__c sp = new Sourcing_Preference__c();
        sp.Name__c = 'Demo';
        sp.External_Id__c = 'hdjb';
        sp.Sent_to_DWH__c = true;
        sp.Is_Template__c = true;
        sp.JSON_QueryBuilder__c='{"condition":"AND","rules":[{"id":"LOWER(ct.companies_cross_data_full.ivp_country)","field":"LOWER(ct.companies_cross_data_full.ivp_country)","type":"string","input":"select","operator":"equal","value":"united states"}],"valid":true}';
        insert sp;
        test.startTest();
        List<Sourcing_Preference__c>  spList = customLookUpController.fetchLookUpValues('Demo','Sourcing_Preference__c');
        system.assertEquals(1, spList.size());
        test.stopTest();
    }
}