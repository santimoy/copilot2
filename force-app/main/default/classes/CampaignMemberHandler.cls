/**
*@name      : CampaignMemberHandler
*@developer : 10K developer
*@date      : 2017-09-21
*@description   : The class used as handler of campaignMembertrgr
						* Whenever a new campaignmember is inserted/updated/deleted need to do same with child campaign's campaignmember 
						*CampaignMembers will be matched using CampaignMember.Contact/CampaignMember.Lead values whichever is populated!(because we dont know whihc child campaignMember records is deleted)
**/
public class CampaignMemberHandler  extends TriggerHandler  {
    public CampaignMemberHandler(){}
    /* This method will be called for after insert Event and create the campaignmember records for child campaigns */
    /*public override void afterInsert(){
        if(!CampaignCompanyAutomation.AllowChildCampaignAndMemberUpdate)
            CampaignCompanyAutomation.insertChildCampaignMembersFromParentCampaignMembers(
                CampaignCompanyAutomation.applyChildMemberCheckAndGetParentCampaignMembers( Trigger.new, 'create' )
            );
    }*/
   	
    /* This method will be called for after update event and update the campaignmember records for child campaigns */
    /*public override void afterUpdate(){
      	if(!CampaignCompanyAutomation.AllowChildCampaignAndMemberUpdate) 
            CampaignCompanyAutomation.updateChildCampaignMembersFromParentCampaignMembers(
            	CampaignCompanyAutomation.applyChildMemberCheckAndGetParentCampaignMembers( Trigger.new, 'modify' )
        );
    }*/
    
    /* This method will be called for before delete event and delete the campaignmember records for child campaigns */
    /*public override void beforeDelete(){

        if(!CampaignCompanyAutomation.AllowChildCampaignAndMemberUpdate)
            CampaignCompanyAutomation.deleteChildCampaignMembersWhenParentCampaignMemberDeleted( 
                CampaignCompanyAutomation.applyChildMemberCheckAndGetParentCampaignMembers( Trigger.old, 'delete' )
            );
    }*/
    private void populateCompanyId(List<CampaignMember> campMembers, Set<Id> contactIds){
        Map<Id, Contact> IdWithContactObj = new Map<Id, Contact>([SELECT Id,Account.Name FROM Contact WHERE Id IN :contactIds]);
        for(CampaignMember campMember : campMembers){
            if(campMember.ContactId != NULL && IdWithContactObj.containsKey(campMember.ContactId)){
            	campMember.Company_Name__c =  IdWithContactObj.get(campMember.ContactId).AccountId;   
            }
        }
        
    }
    public void processbeforeInsert(List<CampaignMember> campMembers){
        Set<Id> campignIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        List<CampaignMember> campMembersToPopulate = new List<CampaignMember>();
        for(CampaignMember campMember : campMembers){
            campignIds.add(campMember.CampaignId);
            if(campMember.Company_Name__c == NULL){
                campMembersToPopulate.add(campMember);
                if(campMember.ContactId != NULL)
                    contactIds.add(campMember.ContactId);
            }
            
        }
        populateCompanyId(campMembersToPopulate, contactIds);
        updateCampaignMemberProcessingOnDate(campignIds, (List<CampaignMember>)Trigger.new);    
        
    }
    public override void beforeInsert(){
    	processbeforeInsert((List<CampaignMember>)Trigger.new); 
    }
    public void processBeforeUpadate(List<CampaignMember> campMembers, Map<Id, CampaignMember> campMemberOldMap){
        Set<ID> campignIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        List<CampaignMember> campMembersToPopulate = new List<CampaignMember>();
        List<CampaignMember> campMembersToUpdate = new List<CampaignMember>();
        for(CampaignMember campMember : campMembers){
            if( (campMemberOldMap.get(campMember.Id).Status != campMember.Status) || 
                (campMemberOldMap.get(campMember.Id).Opportunity__c == NULL && campMember.Ready_For_Processing_On__c  != NULL) ){
                campMembersToUpdate.add(campMember);
                campignIds.add(campMember.CampaignId);
            }
            if(campMember.Company_Name__c == NULL){
                campMembersToPopulate.add(campMember);
                if(campMember.ContactId != NULL)
                    contactIds.add(campMember.ContactId);
            }
        }
        populateCompanyId(campMembersToPopulate, contactIds);
        updateCampaignMemberProcessingOnDate(campignIds,campMembersToUpdate);    
        
    }
    public override void beforeUpdate(){
        processBeforeUpadate((List<CampaignMember>)Trigger.new, (Map<Id,CampaignMember>)Trigger.oldMap);
    }
    void updateCampaignMemberProcessingOnDate(Set<Id> campaignIds, List<CampaignMember> campMembers){
        updateCampaignMemberProcessingOnDate_static(campaignIds,campMembers);
    }
    
    public static void updateCampaignMemberProcessingOnDate_static(Set<Id> campaignIds, List<CampaignMember> campMembers){
        Set<String> campMembersStatus = OppAuto_Service.getCampaignMembersStatus();
        Map<Id, Campaign> campaignsToUpdateMap = new Map<Id, Campaign>([SELECT Id,Type FROM Campaign WHERE Id IN : campaignIds]);
        
        for(CampaignMember campMemberObj : campMembers){
            
            Campaign campObj = campaignsToUpdateMap.get(campMemberObj.CampaignId);
            if(campObj != NULL && campObj.Type != NULL && campMemberObj.Status != NULL && campMembersStatus != NULL){
                if(((campObj.Type.equals('Dinner / Roundtable') || campObj.Type.equals('Innovation Briefing')) && campMemberObj.Status.equals('Attended')) 
                   || (campObj.Type.equals('Portfolio Company Demand Gen') && campMembersStatus.contains(campMemberObj.Status.toLowerCase()))){
                       campMemberObj.Ready_For_Processing_On__c = System.now();
                   }    
            }
        } 
    }
    
}