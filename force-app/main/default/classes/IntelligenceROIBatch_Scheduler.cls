global class IntelligenceROIBatch_Scheduler implements Schedulable {
    public static String CRON_EXP = '0 0 12 * * ? *';

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new IntelligenceROIBatch());
    }
}