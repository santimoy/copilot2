/**
 * @author  : 10k-Advisor
 * @name    : SalesloftCadenceMemberSyncSchd 
 * @date    : 2018-08-09
 * @description   : The class used as batch to process cadenceMembers those are having no Contact/Cadence
**/
public class SalesloftCadenceMemberSyncSchd implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable{
    
    String sourceObject;
    String objEndPoint;
    
    public SalesloftCadenceMemberSyncSchd(){}
    
    public SalesloftCadenceMemberSyncSchd(String sourceObject){
        this.sourceObject = sourceObject;
        objEndPoint = Salesloft.getEndPoint(sourceObject);
    }
    
    public void execute(SchedulableContext SC) {
       System.enqueueJob(new SalesLoftSyncQ('people'));
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String filter=' WHERE Id=NULL';// no need 
        String fields='Id,Salesloft_Id__c';
        if(sourceObject=='people'){
            filter=' WHERE Contact__c = NULL';
            fields+=',Contact__c, Salesloft_People_Id__c';
        }else if(sourceObject=='cadence'){
            filter=' WHERE Cadence__c = NULL';
            fields+=',Cadence__c, Salesloft_Cadence_Id__c';
        }
        return Database.getQueryLocator('SELECT ' +fields
                                        +' FROM Salesloft_Cadence_Member__c '
                                        +filter);
    }
    
    public void execute(Database.BatchableContext BC, List<Salesloft_Cadence_Member__c> cadenceMembers){
        if(Test.isRunningTest()){
            Test.setMock(HttpCalloutMock.class, new SalesloftMock());
        }
        processMembers(cadenceMembers, sourceObject, objEndPoint);
    }
    
    public static void processMembers(List<Salesloft_Cadence_Member__c> cadenceMembers, String sourceObject, String objEndPoint){
        
        String idParam='';
        for(Salesloft_Cadence_Member__c cadenceMember : cadenceMembers){
            // recIds.add(cadenceMember.Salesloft_People_Id__c);
            if(sourceObject=='people' && String.isNotBlank(cadenceMember.Salesloft_People_Id__c)){
                idParam+='&ids[]='+cadenceMember.Salesloft_People_Id__c;
            }else if(sourceObject=='cadence' && String.isNotBlank(cadenceMember.Salesloft_Cadence_Id__c)){
                idParam+='&ids[]='+cadenceMember.Salesloft_Cadence_Id__c;
            }
        }
        
        HttpResponse response = SalesLoft.getAPIResponse(objEndPoint, 'GET',
                        new Map<String, String>{'include_paging_counts' =>'true','per_page'=>'100',''=>idParam});
        
        if(response!=NULL){
            if(sourceObject=='people'){
                matchCadenceMemebrWithContact(cadenceMembers, response);
            }else if(sourceObject=='cadence'){
                matchCadenceMemebrWithCadence(cadenceMembers, response);
            }
        }
    }
    public void finish(Database.BatchableContext BC){ 
        if(sourceObject=='people'){
            // the following job we will execute once all the queues are processed via line
            Database.executeBatch(new SalesloftCadenceMemberSyncSchd('cadence'),100);
        }
    }
    
    private static void matchCadenceMemebrWithContact(List<Salesloft_Cadence_Member__c> cadenceMembers, HttpResponse response){
        Salesloft.PeopleResponse peopleData = (Salesloft.PeopleResponse)JSON.deserialize(
                                                    response.getBody(), 
                                                    Salesloft.PeopleResponse.Class);
            
        if(peopleData!=NULL){
            if(peopleData.data!=NULL){
                Map<String, String> conMap = SalesloftPeopleSyncService.processData(peopleData.data);
                System.debug('->'+conMap);
                List<Salesloft_Cadence_Member__c> cmembersToUpdate = new List<Salesloft_Cadence_Member__c> ();
                List<Salesloft_Cadence_Member__c> cmembersToDelete = new List<Salesloft_Cadence_Member__c> ();
                
                for(Salesloft_Cadence_Member__c cmember : cadenceMembers){
                    if(conMap.containsKey(cmember.Salesloft_People_Id__c)){
                        cmember.Contact__c = conMap.get(cmember.Salesloft_People_Id__c);
                        cmembersToUpdate.add(cmember);
                    }else{
                        cmembersToDelete.add(cmember);
                    }
                }
                
                if(!cmembersToUpdate.isEmpty()){
                    update cmembersToUpdate;
                }
                if(!cmembersToDelete.isEmpty()){
                    delete cmembersToDelete;
                }
            }
        }
    }
    
    private static void matchCadenceMemebrWithCadence(List<Salesloft_Cadence_Member__c> cadenceMembers, HttpResponse response){
        Salesloft.CadenceResponse cadenceData = (Salesloft.CadenceResponse)JSON.deserialize(
                                                    response.getBody(), 
                                                    Salesloft.CadenceResponse.Class);
            
        if(cadenceData!=NULL){
            if(cadenceData.data!=NULL){
                Map<String, String> cadMap = SalesloftCadenceSyncService.processData(cadenceData.data);
                
                List<Salesloft_Cadence_Member__c> cmembersToUpdate = new List<Salesloft_Cadence_Member__c> ();
                List<Salesloft_Cadence_Member__c> cmembersToDelete = new List<Salesloft_Cadence_Member__c> ();
                
                for(Salesloft_Cadence_Member__c cmember : cadenceMembers){
                    if(cadMap.containsKey(cmember.Salesloft_Cadence_Id__c)){
                        cmember.Cadence__c = cadMap.get(cmember.Salesloft_Cadence_Id__c);
                        cmembersToUpdate.add(cmember);
                    }else{
                        cmembersToDelete.add(cmember);
                    }
                }
                
                if(!cmembersToUpdate.isEmpty()){
                    update cmembersToUpdate;
                }
                if(!cmembersToDelete.isEmpty()){
                    delete cmembersToDelete;
                }
            }
        }
    }

}