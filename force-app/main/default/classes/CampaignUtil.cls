public class CampaignUtil {
    public CampaignUtil(){}
    /*Fields we don't want in update: Shcema is giving names in small letters*/
    /*Campaign.Name should not just be copied same to child, it would be PortFolioCompany.Name + ' ' +ParentCampaign.Name*/
    //public static Set<String> excludedFields = new Set<String>{'ownerid', 'portfolio_company__c', 'name'};
    /*
     * this method will take object name and return all accessable fields
 	*/
  /*public static String getObjectFields(String objectName, Boolean isUpdatable){
         String fieldsVal = '';
         DescribeSObjectResult objDef = Schema.getGlobalDescribe().get(objectName).getDescribe();
         Map<String, SObjectField> fields = objDef.fields.getMap();
         
         for(String fieldName : fields.keySet()){
             //Fields we can populate on insert
			if( isUpdatable == false && fields.get(fieldName).getDescribe().isCreateable() ) 
               	fieldsVal += fieldName +',';

			if( isUpdatable == true && fields.get(fieldName).getDescribe().isUpdateable() && !excludedFields.contains(fieldName) )
                fieldsVal += fieldName +',';
         }   
         if(fieldsVal.endsWith(','))
             fieldsVal= fieldsVal.substring(0,fieldsVal.length()-1);
      
      	
      
         return fieldsVal;
    }*/
    /*
     * This method will take filter,object name,set of id and return all records
 	*/
    /*public static List<Sobject> getAllRecords(String filter, String ObjName, Set<ID> setOfId,boolean isUpdatable){
		
        String Query = 'SELECT '+  getObjectFields(ObjName, isUpdatable) 
            			+ ' FROM '+ ObjName;
        query += (String.isNotEmpty(filter)) ? filter : '';
        return Database.query(query);
    }*/
    /*
     * This method will take campaignId and create child campaignMember records
 	*/
    //public static void insertChildCampMember(Map<Id,Set<Id>> parentCampaignIdWithChildCampaignIds){
    //    List<CampaignMember> campMemberListToInsert = new List<CampaignMember>();
        /* We are prepareing this map because in trigger we can't access parent object fields 
			so we will be use this map to create CampaignMemberStatus
		*/
      /*  Map<Id,Id> childCampaignWithParent = new Map<Id,Id>();
        for(CampaignMember campMember : (List<CampaignMember>)getAllRecords(' WHERE campaignID in :setOfId','CampaignMember',parentCampaignIdWithChildCampaignIds.keySet(),false)){
            //Create Child members for applicable Campaigns
            for(Id childCampaignId : parentCampaignIdWithChildCampaignIds.get(campMember.CampaignId)){
                childCampaignWithParent.put(childCampaignId, campMember.CampaignId);
                CampaignMember childMember = campMember.clone();
                childMember.CampaignId = childCampaignId;
                campMemberListToInsert.add(childMember);
            }
        }
        if(!campMemberListToInsert.isEmpty()){
            Set<Id> campaignIds = new Set<Id>();
            campaignIds.addAll(childCampaignWithParent.keySet());
            campaignIds.addAll(childCampaignWithParent.values());
            Map<Id, Map<String, CampaignMemberStatus>> campaignIdWithMembersStatus = CampaignCompanyAutomation.getCampaignIdWithMemberStatusMap(campMemberListToInsert,campaignIds);
            
            List<CampaignMemberStatus> campMembersStatus = new List<CampaignMemberStatus>();
            for(CampaignMember campMember : campMemberListToInsert){
                //Checking that CampaignMemberStatus should not be already exists
                if(campMember.Status != null && campaignIdWithMembersStatus.get(campMember.CampaignId) != null && 
                   !campaignIdWithMembersStatus.get(campMember.CampaignId).containsKey(campMember.Status)){
                       CampaignMemberStatus statusRec = campaignIdWithMembersStatus.get(childCampaignWithParent.get(campMember.CampaignId)).get(campMember.Status);
                       
                       campMembersStatus.add(new CampaignMemberStatus(CampaignId = campMember.CampaignId, 
                                                                      Label = statusRec.Label,
                                                                      IsDefault = statusRec.IsDefault,
                                                                      HasResponded = statusRec.HasResponded));
                   }       
                
            } 
            if(!campMembersStatus.isEmpty())
                insert campMembersStatus;
            insert campMemberListToInsert;
        }
    }*/
    
    /*
     * This method will take campaignId and delete child campaign records
 	*/
    /*public static void deleteChildCampaign(Set<String> campNames){
        List<Campaign> campListToDelete = [SELECT Name from Campaign where Name in :campNames];
        if(!campListToDelete.isEmpty())
            delete campListToDelete;
    }*/
   
}