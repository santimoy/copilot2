//
// 4-4-2018: TO BE DEPRECATED
//
@isTest
private class FinancialsTriggerHandlerTest {
	private static Account setupData(){
		Account acc = new Account(Name='testacct1',
                    Website = 'http://www.www.dev',
                    Description = 'test company'
                   );
		insert acc;

		
		List<Financials__c> newfins= new List<Financials__c>();
		Financials__c fin= new Financials__c(Revenue__c=100.00,Year_Text__c='2014', Account__c=acc.Id);
		newfins.add(fin);
		fin= new Financials__c(Revenue__c=null,Year_Text__c='2015', Account__c=acc.Id);
		newfins.add(fin);
		insert newfins;
		return acc;
	}
	@isTest static void testFinancials() {
		// Implement test code
		Account newacc= setupData();
		Account acc=[select Latest_Year_Revenue_Entered__c,Latest_Revenue__c, id from Account where id=:newacc.Id ];
		System.assertEquals('2014',acc.Latest_Year_Revenue_Entered__c);
		System.assertEquals(100.00,acc.Latest_Revenue__c);

	}
	
	
	
}