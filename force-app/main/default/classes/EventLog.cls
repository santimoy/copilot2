/**
 * @File Name          : EventLog.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 6/11/2019, 3:13:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2019   Anup Kage     Initial Version
**/
public with sharing class EventLog {
    // private static String lOG_LEVEL{get{
    //     if(lOG_LEVEL == null){
    //         Integration_API_Settings__c userSetting = Integration_API_Settings__c.getOrgDefaults();
    //         if(userSetting.Event_Log_Level__c != null){
    //             lOG_LEVEL = userSetting.Event_Log_Level__c.toLowerCase();
    //         }else{
    //             lOG_LEVEL = 'error';
    //         }
    //     }
    //     return lOG_LEVEL;
    // }set;}

    

    public static String lOG_LEVEL = 'error';
    public static List<Event_Log__c> errorLogs = new List<Event_Log__c>();

    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, Exception exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, Exception exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }

    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, AsyncException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, AsyncException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());

        errorLogs.add(objEventLog);
    }
    
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, BigObjectException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, BigObjectException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }

    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, CalloutException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }

    public static void generateLog(HttpRequest request, String className, String methodName, CalloutException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, DmlException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, DmlException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, EmailException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, EmailException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, ExternalObjectException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, ExternalObjectException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, LimitException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, LimitException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, JSONException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, JSONException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, ListException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, ListException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }

    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, MathException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, MathException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, NoAccessException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, NoAccessException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, NoDataFoundException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, NoDataFoundException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, NoSuchElementException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());       
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, NoSuchElementException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, NullPointerException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, NullPointerException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }

    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, QueryException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, QueryException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, RequiredFeatureMissingException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, RequiredFeatureMissingException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, SearchException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, SearchException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, SecurityException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, SecurityException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, SerializationException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
     public static void generateLog(HttpRequest request, String className, String methodName, SerializationException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, SObjectException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, SObjectException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, StringException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, StringException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, TypeException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, TypeException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, VisualforceException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, VisualforceException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, XmlException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        errorLogs.add(objEventLog);
    }
    public static void generateLog(HttpRequest request, String className, String methodName, XmlException exp) {
        Event_Log__c objEventLog = createEventLogRecord(request, className, methodName);
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());
        errorLogs.add(objEventLog);
    }
    private static Event_Log__c createEventLogRecord(HttpRequest request, HttpResponse response, String className, String methodName){
        Event_Log__c objEvent =  new Event_Log__c();
        if (request != null){
            objEvent.Request_URI__c = request.getEndpoint();
            //objEvent.Request_Payload__c = 
            if(String.isNotBlank(request.getBody()) && request.getBody().length() > 131072) {
                objEvent.Request_Payload__c = request.getBody().left(131072);
            }else{
                 objEvent.Request_Payload__c = request.getBody();
            }
        }
        if (response != null){
            //objEvent.Response_Payload__c = response.getBody();
            if(String.isNotBlank(response.getBody()) && response.getBody().length() > 131072) {
                objEvent.Response_Payload__c = response.getBody().left(131072);
            }else{
                 objEvent.Response_Payload__c = response.getBody();
            }
            objEvent.Error_Code__c = string.valueof(response.getStatusCode());
        }
        objEvent.Class_Name__c = className;
        objEvent.Method_Name__c = methodName;
        objEvent.User__c = UserInfo.getuserId();
        objEvent.Type__c = lOG_LEVEL;
        return objEvent;
    }
    private static Event_Log__c createEventLogRecord(HttpRequest request, String className, String methodName){
        Event_Log__c objEvent =  new Event_Log__c();
        if (request != null){
            objEvent.Request_Payload__c = request.getBody();
            objEvent.Request_URI__c = request.getEndpoint();
        }
        objEvent.Error_Code__c = '';
        objEvent.Class_Name__c = className;
        objEvent.Method_Name__c = methodName;
        objEvent.User__c = UserInfo.getuserId();
        objEvent.Type__c = lOG_LEVEL;
        return objEvent;
    }

     private static Event_Log__c createEventLogRecord(String className, String methodName, String errorMessage){
        Event_Log__c objEvent =  new Event_Log__c();        
        objEvent.Description__c = errorMessage;
        objEvent.Class_Name__c = className;
        objEvent.Method_Name__c = methodName;
        objEvent.User__c = UserInfo.getuserId();
        objEvent.Type__c = lOG_LEVEL;
        return objEvent;
    }
    private static void updateEventLogRec(Event_Log__c objEventLog, String lineNumber, String getMessage){
        objEventLog.Line_Number__c = lineNumber;
        objEventLog.Description__c = getMessage;
        if(getMessage.length() > 32760){
            objEventLog.Error_Message__c = getMessage.left(32760);
        }else{
             objEventLog.Error_Message__c = getMessage;
        }
    }

    /**
    Insert the Event_Log__c
     */
    public static void commitLogs(){
        try {
            insert errorLogs;
            errorLogs.clear();
        } catch (DmlException exp) {
           System.debug('Error On inserting ErrorLogs: '+exp.getCause() + ' '+exp.getMessage()); 
        }
    }
     public static void generateLog(HttpRequest request, HttpResponse response, String className, String methodName, String type) {
        Event_Log__c objEventLog = createEventLogRecord(request, response, className, methodName);
        objEventLog.Type__c = type;
        errorLogs.add(objEventLog);
    }
    public static void generateLog(String className, String methodName, String type, Exception exp) {
        // Event_Log__c objEventLog = createEventLogRecord( className, methodName);
        Event_Log__c objEventLog = new Event_Log__c(); 
        updateEventLogRec(objEventLog, String.valueOf( exp.getLineNumber()),exp.getMessage());        
        objEventLog.Type__c = type;
        errorLogs.add(objEventLog);
    }
    /**
    * @description 
    * @author Anup Kage | 6/11/2019 
    * @param className 
    * @param methodName 
    * @param type 
    * @param description 
    * @return void 
    **/
    public static void generateLog(String className, String methodName, String type, String description) {
        Event_Log__c objEventLog = new Event_Log__c();         
        objEventLog.Class_Name__c = className;
        objEventLog.Method_Name__c = methodName;
        objEventLog.User__c = UserInfo.getuserId();
        objEventLog.Type__c = type;
        objEventLog.Description__c = description;
        errorLogs.add(objEventLog);
    }
    /**
    * @description 
    * @author Anup Kage | 6/11/2019 
    * @param className 
    * @param methodName 
    * @param type 
    * @param description 
    * @param errorMessage 
    * @return void 
    **/
    public static void generateLog(String className, String methodName, String type, String description, String errorMessage) {
        Event_Log__c objEventLog = new Event_Log__c();         
        objEventLog.Class_Name__c = className;
        objEventLog.Method_Name__c = methodName;
        objEventLog.User__c = UserInfo.getuserId();
        objEventLog.Type__c = type;
        objEventLog.Description__c = description;
        if(errorMessage.length() > 32760){
            objEventLog.Error_Message__c = errorMessage.left(32760);
        }else{
             objEventLog.Error_Message__c = errorMessage;
        }
        errorLogs.add(objEventLog);
    }
    
    public static void generateLog(String className, String methodName, Database.SaveResult sr) {
        if(!sr.isSuccess()) {
            if(sr.getErrors() != null) {
                String errorMessage =  'Id: '+ sr.getId() + '; ';
                for(Database.Error err: sr.getErrors()) {
                    errorMessage += 'Message: '
                                    + err.getMessage()
                                    + '; Fields: '
                                    + err.getFields()
                                    + '; Status Code: '
                                    + err.getStatusCode()
                                    + '\n\r';
                }
                Event_Log__c objEvent = createEventLogRecord(className, methodName, errorMessage);
                objEvent.type__c = 'error';
                errorLogs.add(objEvent);
            }
        }
    }

    public static void generateLog(String className, String methodName, List<Database.SaveResult> saveResult) {
        for(Database.SaveResult sr: saveResult) {
            if(!sr.isSuccess()) {
                if(sr.getErrors() != null) {
                    String errorMessage =  'Id: '+ sr.getId() + '; ';
                    for(Database.Error err: sr.getErrors()) {
                        errorMessage += 'Message: '
                                        + err.getMessage()
                                        + '; Fields: '
                                        + err.getFields()
                                        + '; Status Code: '
                                        + err.getStatusCode()
                                        + '\n\r';
                    }
                   Event_Log__c objEvent = createEventLogRecord(className, methodName, errorMessage);
                   objEvent.type__c = 'error';
                   errorLogs.add(objEvent);
                }
            }
        }
    }
    public static void generateLog(String className, String methodName,Database.UpsertResult ur) {
        if(!ur.isSuccess()) {
            if(ur.getErrors() != null) {
                String errorMessage =  'Id: '+ ur.getId() + '; ';
                for(Database.Error err: ur.getErrors()) {
                    errorMessage += 'Message: '
                                    + err.getMessage()
                                    + '; Fields: '
                                    + err.getFields()
                                    + '; Status Code: '
                                    + err.getStatusCode()
                                    + '\n\r';
                }
                Event_Log__c objEvent = createEventLogRecord(className, methodName, errorMessage);
                objEvent.type__c = 'error';
                errorLogs.add(objEvent);
            }
        }
    }

    public static void generateLog(String className, String methodName, List<Database.UpsertResult> urList) {
        for(Database.UpsertResult ur: urList) {
            if(!ur.isSuccess()) {
                if(ur.getErrors() != null) {
                    String errorMessage =  'Id: '+ ur.getId() + '; ';
                    for(Database.Error err: ur.getErrors()) {
                        errorMessage += 'Message: '
                                        + err.getMessage()
                                        + '; Fields: '
                                        + err.getFields()
                                        + '; Status Code: '
                                        + err.getStatusCode()
                                        + '\n\r';
                    }
                    Event_Log__c objEvent = createEventLogRecord(className, methodName, errorMessage);
                    objEvent.type__c = 'error';
                    errorLogs.add(objEvent);
                }
            }
        }
    } 
    public static void limitLogger(String className, String methodName, String description){
        Event_Log__c objEventLog = new Event_Log__c(); 
        description += '\n'
                    + 'Isqueueable :'+ System.isQueueable() + '\n'
	                + 'isFuture :'+ System.isFuture()+'\n'
	                + 'isScheduled :' + System.isScheduled()+ '\n'
	                + 'isbatchable:' + System.isBatch() + '\n'
                    + 'Total number Callouts : '+ Limits.getLimitCallouts() + '\n'
                    + 'Number of Callouts processed : ' + Limits.getCallouts() + '\n'
                    + 'Total number of DML statements :' + Limits.getLimitDMLStatements() + '\n'
                    + 'Number of DML statements Called :'+ Limits.getDmlStatements() + '\n'
                    + 'Total number of SOQL queries :' + Limits.getLimitQueries() + '\n'
                    + 'Number of SOQL queries Called:' + Limits.getQueries() +  '\n'
                    + 'Total amount of HEAP memory :' + Limits.getLimitHeapSize() + '\n'
                    + 'Amount of HEAP memory used :' +Limits.getHeapSize() + '\n'
                    + 'Maximum CPU time (in milliseconds): ' + Limits.getLimitCpuTime() + '\n'
                    + ' CPU time used in current transaction (in milliseconds) '+ Limits.getCpuTime() ;

        objEventLog.Class_Name__c = className;
        objEventLog.Method_Name__c = methodName;
        objEventLog.User__c = UserInfo.getuserId();
        objEventLog.Type__c = 'DEBUG';
        objEventLog.Description__c = description;
        errorLogs.add(objEventLog);
    }
}