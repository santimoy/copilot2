/*
Created By  : Sukesh Kumar 
Created On  : 04/04/19
Description : Controller for Financials Component
*/
public without sharing class financialsComponentController {
	
	/**  
	@Description Method to be called on Load of Component
	@author Sukesh Kumar
	@date 04/05/2019
	@param  strRecId Account Id from which component is invoked
	@return   JSON formatted data of Account and Financials
	*/
	@AuraEnabled
	public static String fetchInitData(String strRecId) {

		FinancialInitData objFinancialInitData = new FinancialInitData();

		objFinancialInitData.accountData = [SELECT Id,
												   Name,
												   Total_Funding_Raised__c,
												   DWH_Total_Funding_Raised__c
											  FROM Account
											 WHERE Id=:strRecId];

		objFinancialInitData.lstRevenueTypeOptions = getPicklistValues('Opportunity','Revenue_Type__c');

		Date objDate = Date.today();
        Integer intCurrentYear = (objDate.year());
        String strPriorPriorYear =String.valueOf(intCurrentYear-2);
        String strPriorYear =String.valueOf(intCurrentYear-1);
        String strCurrentYear =String.valueOf(intCurrentYear);
        String strNextYear =String.valueOf(intCurrentYear+1);

        List<Financials__c> lstFinancials = [SELECT Id,
        											Account__c,
        											Revenue__c,
        											EBITDA__c,
        											Year_Text__c,
        											Net_Retention__c,
        											Gross_Retention__c,
        											Revenue_Type__c
        									   FROM Financials__c 
        									  WHERE Account__c =: strRecId 
        									    AND (Year_Text__c =:strPriorPriorYear 
        									         OR Year_Text__c =:strPriorYear 
        									         OR Year_Text__c =:strCurrentYear 
        									         OR Year_Text__c =:strNextYear)];
        for(Financials__c objFinancials:lstFinancials) {
            objFinancialInitData.revenueType = objFinancials.Revenue_Type__c;
        	if(objFinancials.Year_Text__c == strPriorPriorYear) {
	            objFinancialInitData.priorPriorRevenueValue = objFinancials.Revenue__c;
	            objFinancialInitData.priorPriorEbitaValue = objFinancials.EBITDA__c;
	            objFinancialInitData.priorPriorNetValue = objFinancials.Net_Retention__c;
	            objFinancialInitData.priorPriorGrossValue = objFinancials.Gross_Retention__c;
	        }
	        if(objFinancials.Year_Text__c == strPriorYear) {

	        	objFinancialInitData.priorRevenueValue = objFinancials.Revenue__c;
	        	objFinancialInitData.priorEbYearValue = objFinancials.EBITDA__c;
	        	objFinancialInitData.priorNetValue = objFinancials.Net_Retention__c;
	            objFinancialInitData.priorGrossValue = objFinancials.Gross_Retention__c;
	        }
	        else if(objFinancials.Year_Text__c == strCurrentYear) {

	        	objFinancialInitData.currentRevenueValue = objFinancials.Revenue__c;
	        	objFinancialInitData.currentEbYearValue = objFinancials.EBITDA__c;
	        	objFinancialInitData.currentNetValue = objFinancials.Net_Retention__c;
	            objFinancialInitData.currentGrossValue = objFinancials.Gross_Retention__c;
	        }
	        else if(objFinancials.Year_Text__c == strNextYear) {

	        	objFinancialInitData.nextRevenueValue = objFinancials.Revenue__c;
	        	objFinancialInitData.nextEbYearValue = objFinancials.EBITDA__c;
	        	objFinancialInitData.nextNetValue = objFinancials.Net_Retention__c;
	            objFinancialInitData.nextGrossValue = objFinancials.Gross_Retention__c;
	        }
	    }

	    return JSON.serialize(objFinancialInitData);
	}

	/**  
	@Description Method to be called click of save, to Save the financial details
	@author Sukesh Kumar
	@date 04/05/2019
	@param  strRecId Account Id from which component is invoked
	@return   JSON formatted data of Account and Financials
	*/
	@AuraEnabled 
	public static void saveData(String recordId,Decimal priorRevenueValue,
						    	Decimal currentRevenueValue,Decimal nextRevenueValue,
						    	Decimal priorEbitaValue,Decimal currentEbitaValue,
						    	Decimal nextEbitaValue,Decimal totalFundingRaisedValue,
						    	Decimal priorPriorRevenueValue, Decimal priorPriorEbitaValue, 
						    	Decimal currentNetValue, Decimal nextNetValue,
						    	Decimal priorNetValue,Decimal currentGrossValue,
						    	Decimal nextGrossValue, Decimal priorGrossValue,
						    	Decimal priorPriorGrossValue, Decimal priorPriorNetValue, 
						    	String strRevenueType) {

		RecordType rtOppInvestmentOpportunity = [SELECT Id 
												   FROM RecordType 
												  WHERE Name ='Investment Opportunity' 
												  LIMIT 1];

        Account objAccount = [SELECT Id,
        							 OwnerId  
        						FROM Account 
        					   WHERE Id =:recordId];

        Account objAccountToUpdate = new Account(Id = recordId,
        										 Total_Funding_Raised__c = totalFundingRaisedValue);

        Date objDate = Date.today();

    	Integer intCurrentYear = (objDate.year());
        String strPriorPriorYear =String.valueOf(intCurrentYear-2);
  	    String strPriorYear =String.valueOf(intCurrentYear-1);
  	    String strCurrentYear =String.valueOf(intCurrentYear);
  	    String strNextYear =String.valueOf(intCurrentYear+1);
        Boolean isPriorPriorYear = false;
  	    Boolean isPriorYear = false;
  	    Boolean isCurrentYear = false;
  	    Boolean isNextYear = false;
       	List<Financials__c> lstFinancials = [SELECT Id,
                                                    Account__c,
                                                    Revenue__c,
                                                    EBITDA__c,
                                                    Year_Text__c,
                                                    Net_Retention__c,
                                                    Gross_Retention__c
                                               FROM Financials__c 
                                              WHERE Account__c =:recordId 
                                              AND (Year_Text__c =:strPriorPriorYear 
                                                   OR Year_Text__c =:strPriorYear 
                                                   OR Year_Text__c =:strCurrentYear 
                                                   OR Year_Text__c =:strNextYear)];
        for(Financials__c objFinancials : lstFinancials) {

            objFinancials.Revenue_Type__c = strRevenueType;

            if(objFinancials.Year_Text__c == strPriorPriorYear) {
                objFinancials.Revenue__c = priorPriorRevenueValue;
                objFinancials.EBITDA__c = priorPriorEbitaValue;
                objFinancials.Net_Retention__c = priorPriorNetValue;
                objFinancials.Gross_Retention__c = priorPriorGrossValue;
                isPriorPriorYear = true;
            }
            else if(objFinancials.Year_Text__c ==strPriorYear) {
                objFinancials.Revenue__c = priorRevenueValue;
                objFinancials.EBITDA__c = priorEbitaValue;
                objFinancials.Net_Retention__c = priorNetValue;
                objFinancials.Gross_Retention__c = priorGrossValue;
                isPriorYear = true;
            }
            else if(objFinancials.Year_Text__c ==strCurrentYear) {
                
                objFinancials.Revenue__c = currentRevenueValue;
                objFinancials.EBITDA__c = currentEbitaValue;
                objFinancials.Net_Retention__c = currentNetValue;
                objFinancials.Gross_Retention__c = currentGrossValue;
                isCurrentYear = true;
            }
            else if(objFinancials.Year_Text__c ==strNextYear) {
                
                objFinancials.Revenue__c = nextRevenueValue;
                objFinancials.EBITDA__c = nextEbitaValue;
                objFinancials.Net_Retention__c = nextNetValue;
                objFinancials.Gross_Retention__c = nextGrossValue;
                isNextYear = true;
            }
        }
        
        if(!isPriorPriorYear && (priorPriorRevenueValue != null || priorPriorEbitaValue != null || priorPriorNetValue != null || priorPriorGrossValue != null)){
          lstFinancials.add(new Financials__c(Account__c =recordId,Year_Text__c = strPriorPriorYear,Revenue__c=priorPriorRevenueValue,EBITDA__c=priorPriorEbitaValue,
                        Revenue_Type__c = strRevenueType, Net_Retention__c = priorPriorNetValue, Gross_Retention__c = priorPriorGrossValue));
        }
        if(!isPriorYear && (priorRevenueValue != null || priorEbitaValue != null || priorNetValue != null || priorGrossValue != null)) {
            lstFinancials.add(new Financials__c(Account__c =recordId,Year_Text__c = strPriorYear,Revenue__c=priorRevenueValue,EBITDA__c=priorEbitaValue,
                            Revenue_Type__c = strRevenueType, Net_Retention__c = priorNetValue, Gross_Retention__c = priorGrossValue));
        }
        if(!isCurrentYear && (currentRevenueValue != null || currentEbitaValue != null || currentNetValue != null || currentGrossValue != null)) {
            lstFinancials.add(new Financials__c(Account__c =recordId,Year_Text__c = strCurrentYear,Revenue__c=currentRevenueValue,EBITDA__c=currentEbitaValue,
                                Revenue_Type__c = strRevenueType, Net_Retention__c = currentNetValue, Gross_Retention__c = currentGrossValue));

        }
        if(!isNextYear && (nextRevenueValue != null || nextEbitaValue != null || nextNetValue != null || nextGrossValue != null)) {
            lstFinancials.add(new Financials__c(Account__c =recordId,Year_Text__c = strNextYear,Revenue__c=nextRevenueValue,EBITDA__c=nextEbitaValue,
                            Revenue_Type__c = strRevenueType, Net_Retention__c = nextNetValue, Gross_Retention__c = nextGrossValue));

        }

        upsert lstFinancials;
		update objAccountToUpdate;
	}

	/**  
	@Description Method to be fetch the picklist values of a field
	@author Sukesh Kumar - Reused from SendToPipeController
	@date 04/08/2019
	@param  ObjectApi_name API Name of object
	@param  Field_name Picklist field name 
	@return   List string, with Picklist values
	*/
	public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
	  	List<String> lstPickvals=new List<String>();
	  	lstPickvals.add('');
	  	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
	  	Sobject Object_name = targetType.newSObject();
	  	Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
	    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
	    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
	    List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
	    for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
	      lstPickvals.add(a.getValue());//add the value  to our final list
   		}
  		return lstPickvals;
	}
	/**
	Inner Class to provide info on Load of component
	*/
	public class FinancialInitData {

		public Account accountData;
    	public List<String> lstRevenueTypeOptions;
		public User currentUser;
		public Decimal priorPriorRevenueValue;
    	public Decimal priorRevenueValue;
		public Decimal currentRevenueValue;
		public Decimal nextRevenueValue;
		public Decimal priorPriorEbitaValue;
    	public Decimal priorEbYearValue;
		public Decimal currentEbYearValue;
		public Decimal nextEbYearValue;
		
		public Decimal priorPriorNetValue;
    	public Decimal priorNetValue;
		public Decimal currentNetValue;
		public Decimal nextNetValue;
		public Decimal priorPriorGrossValue;
    	public Decimal priorGrossValue;
		public Decimal currentGrossValue;
		public Decimal nextGrossValue;
		public String revenueType;
	}

}