/**
* @author 10k-Advisor
* @date 2018-08-29
* @className SalesloftCadenceMemberSyncSchdTest
* @description : To Test the SalesloftCadenceMemberSyncSchd Class..
*/

@isTest
private class SalesloftCadenceMemberSyncSchdTest {
    
    private static testMethod void testSyncSchdForCadenceAndPeople() {
        
        IVPTestFuel tFuel=new IVPTestFuel();
        List<Contact> contacts = tFuel.contacts;
        
        List<Salesloft_Cadence__c> cad = new List<Salesloft_Cadence__c>();
        cad.add(new Salesloft_Cadence__c(Salesloft_Id__c = SalesloftMock.people1Id,Name = 'Test1'));
        cad.add(new Salesloft_Cadence__c(Salesloft_Id__c = SalesloftMock.people1Id, name = 'Test2'));
        insert cad;
        
        List<Salesloft_Cadence_Member__c> cadmember = new List<Salesloft_Cadence_Member__c>();
        cadmember.add(new Salesloft_Cadence_Member__c(Salesloft_Id__c = SalesloftMock.cadmemeberId1 , 
                                                      Salesloft_People_Id__c = SalesloftMock.people1Id ,
                                                      Salesloft_Cadence_Id__c = SalesloftMock.cadenceId1));
        cadmember.add(new Salesloft_Cadence_Member__c(Salesloft_Id__c = SalesloftMock.cadmemeberId2 , 
                                                      Salesloft_People_Id__c = SalesloftMock.people1Id ,
                                                      Salesloft_Cadence_Id__c = SalesloftMock.cadenceId1));
        insert cadmember;
        
        insert new IVP_General_Config__c(Name = 'salesloft-api-key', Value__c = 'Test');
        Test.startTest();
        Database.executeBatch(new SalesloftCadenceMemberSyncSchd('people'));
        Test.stopTest();
        
        List<Salesloft_Cadence_Member__c> caddata = [SELECT Cadence__c from Salesloft_Cadence_Member__c WHERE 
                                                     Contact__c !=NULL AND Cadence__c !=NULL AND 
                                                     Salesloft_Cadence_Id__c !=NULL AND Salesloft_People_Id__c !=NULL];
        System.assertEquals(caddata.size(),2);
        
    }
}