//
// 3-30-2018 TO BE DEPRECATED
//
global with sharing class Alexa_Param implements Comparable{
	public string paramName{get;set;}
	public string paramVal{get;set;}
	
	public Alexa_Param(string name, string paramVal){
		this.paramName = name;
		this.paramVal = paramVal;
	}
	
	global Integer compareTo(Object compareTo) {
		Alexa_Param compareToParam = (Alexa_Param) compareTo;
		return paramName.compareTo(compareToParam.paramName);			
	}
	
	public string toParamString(){
		return ( paramVal==null ) ? paramName + '=' : paramName + '=' + EncodingUtil.urlEncode(paramVal, 'UTF-8');
	}
}