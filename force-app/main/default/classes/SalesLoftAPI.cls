/**
 * @author  : 10k-Advisor
 * @name    : SalesLoftAPI 
 * @date    : 2018-08-09
 * @description   : The class used to do callout(s) of Salesloft and return response based on values
**/
public class SalesLoftAPI {
    private static final String ENCODING = 'UTF-8';
    public abstract class Request{ }
    
    public abstract class Response{      
        public abstract System.Type getType();        
        // default implementation for most of the responses
        // could be overriden if property names in salesforce conflict with SalesLoftAPI response
        public virtual object populateFromJson(String jsonResponse) {  
            return Json.deserialize( jsonResponse, getType());
        }
    }
    
    /**
     *@description  : Prepare HttpRequest using passed parameters and return the HttpResponse
     *@return-type  : HttpRequest
    **/
    private static HttpRequest prepareRequest(String endpoint, String method, Map<String, String> headers){
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setMethod(method);
        req.setTimeout(120000);
        
        //Setting Headers
        for(String headerName : headers.keySet()){
            req.setHeader(headerName, headers.get(headerName));
        }
        system.debug(req.getBody());
        return req;
    }
    
    /**
     *@description  : Call prepareRequest and call actual request and return the HttpResponse
     *@return-type  : HttpResponse
    **/
    public static HttpResponse getCalloutResponse(String endPoint, String method, 
                                                    Map<String, String> headers, Map<String, String> params,
                                                    SalesLoftAPI.Request request){
        
        if(params!=NULL){
            String conjuction = '?';
            for(String key : params.keySet()){
                if(key=='' && params.get(key)=='')
                    continue;
                
                endPoint+=conjuction+key+'='+params.get(key);
                conjuction='&';
            }
        }
        
        Http h = new Http();
        HttpRequest req = prepareRequest(endPoint, method, headers);
        if(request != NULL){
            req.setBody(json.serialize(request));
        }
        HttpResponse res = h.send(req);
        system.debug(res.getBody());
        return res;
    }
    
}