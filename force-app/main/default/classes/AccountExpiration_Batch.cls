/*
*	AccountExpiration_Batch class : Contains logic for Account Expiration
*
*	Author: Vincent Ip
*	Date:   Sept 12, 2012
*
*	Last Modified:
*		WNG 4-4-2018 Simplied query filter to expire date, owner and account RT only.
*
*/
global class  AccountExpiration_Batch implements Database.Batchable<sObject>, Schedulable{

	public list<string> testAccountIds {get;set;}
	
	public AccountExpiration_Batch(){
		testAccountIds = new list<string>();
	}
	// ----------------------------------------------------------------------------------------------------------------------------------
	// Batchable Interface methods 
	global Database.QueryLocator start(Database.BatchableContext BC){
		Common_Config__c config = CommonConfigUtil.getConfig();

		list<User> unassignedList = [select id from User where LastName = :config.AccountExpiration_Unassigned_Lastname__c];
		if (unassignedList.size()<=0) throw new AccountExpiration_Batch_Exception('Cannot find Unassigned User.');
		
		User unassignedUser = unassignedList.get(0);
		string unassignedUserId = unassignedUser.Id;
		
		list<string> recordTypes = config.AccountExpiration_RecordTypes__c.split('\\s*,\\s*');
		
		// setup the query
		Date todayDate = System.today();
		String queryString = 'Select Id, Name, Expire_Date__c, OwnerId from Account '+
			'where Expire_Date__c <= :todayDate and OwnerId != :unassignedUserId and RecordType.DeveloperName in :recordTypes';

		if (Test.isRunningTest() && testAccountIds.size()>0) queryString += ' and id in :testAccountIds';

		System.Debug('\n---queryString:'+queryString);
		return Database.getQueryLocator(queryString);	
	}

	global void execute(Database.BatchableContext BC, List<Account> batchList){
		Common_Config__c config = CommonConfigUtil.getConfig();
		list<User> unassignedList = [select id from User where LastName = :config.AccountExpiration_Unassigned_Lastname__c];
		if (unassignedList.size()<=0) throw new AccountExpiration_Batch_Exception('Cannot find Unassigned User.');
		
		User unassignedUser = unassignedList.get(0);
		
		for (Account currAcct : batchList){
			System.debug('\n---currAcct:'+currAcct);
			currAcct.OwnerId = unassignedUser.Id;
		}
		
		update batchList;
	}
	
	global void finish(Database.BatchableContext BC){
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// methods needed for the Schedulable interface
	global void execute(SchedulableContext sc){
		Database.executebatch(this, 200);
	}
	
	public class AccountExpiration_Batch_Exception extends Exception{}
	
	
	// ----------------------------------------------------------------------------------------------------------------------------------
	// test coverage methods
	@isTest (SeeAllData=true)
	static void testThisClass(){
		Common_Config__c config = CommonConfigUtil.getConfig();
		list<string> recordTypesString = config.AccountExpiration_RecordTypes__c.replaceAll(' ', '').split(',');
		list<RecordType> recordTypes = [select Id from RecordType where sObjectType='Account' and DeveloperName in :recordTypesString];
		
		list<Account> acctList = new list<Account>();
		acctList.add(new Account(name='parent1', RecordTypeId=recordTypes[0].Id));
		acctList.add(new Account(name='test1', RecordTypeId=recordTypes[0].Id));
		acctList.add(new Account(name='test2', RecordTypeId=recordTypes[0].Id));
		acctList.add(new Account(name='test3', RecordTypeId=recordTypes[0].Id));
		insert acctList;

		// update data
		acctList[1].Expire_Date__c=system.today();
		acctList[2].Expire_Date__c=system.today().addDays(-1);
		acctList[3].Expire_Date__c=system.today().addDays(1);
		update acctList;

		AccountExpiration_Batch testClass = new AccountExpiration_Batch();
		testClass.testAccountIds.add( acctList[0].Id );
		testClass.testAccountIds.add( acctList[1].Id );
		testClass.testAccountIds.add( acctList[2].Id );
		testClass.testAccountIds.add( acctList[3].Id );
		
		Test.startTest();

		SchedulableContext ctx;
		testClass.execute(ctx);

		Test.stopTest();
		
		// get the unassigned user
		list<User> unassignedList = [select id from User where LastName = :config.AccountExpiration_Unassigned_Lastname__c];
		if (unassignedList==null || unassignedList.size()==0)
			throw new AccountExpiration_Batch_Exception('Cannot find Unassigned User.');
		User unassignedUser = unassignedList.get(0);
		
		// refetch the accounts
		Account testAcct1 = [select id, Name, Expire_Date__c, ownerId from Account where id = :acctList[1].Id];
		System.debug('---testAcct1:'+testAcct1);
		System.assertEquals(unassignedUser.Id, testAcct1.OwnerId);
		
		Account testAcct2 = [select id, Name, Expire_Date__c, ownerId from Account where id = :acctList[2].Id];
		System.debug('---testAcct2:'+testAcct2);
		System.assertEquals(unassignedUser.Id, testAcct2.OwnerId);

		Account testAcct3 = [select id, Name, Expire_Date__c, ownerId from Account where id = :acctList[3].Id];
		System.debug('---testAcct3:'+testAcct3);
		System.assertNotEquals(unassignedUser.Id, testAcct3.OwnerId);
	}
}