/*
*	FinancialsYearlyRollover_BatchTest class : Test class coverage for yearly rolling over on Account Revenue fields
*
*	Author: Wilson Ng
*	Date:   Apr 4, 2018
*
*/
@isTest
private class FinancialsYearlyRollover_BatchTest {
    
    @isTest static void testRollover()
	{
		integer testyear = system.today().year();
		Account acct = new Account(Name='testaccount');
		insert acct;
		list<Financials__c> finList = new list<Financials__c>();
		finList.add(new Financials__c(Account__c=acct.Id, Year_Text__c=String.valueOf(testyear+1), Revenue__c=1, EBITDA__c=1));
		finList.add(new Financials__c(Account__c=acct.Id, Year_Text__c=String.valueOf(testyear), Revenue__c=2, EBITDA__c=2));
		finList.add(new Financials__c(Account__c=acct.Id, Year_Text__c=String.valueOf(testyear-1), Revenue__c=3, EBITDA__c=3));
		finList.add(new Financials__c(Account__c=acct.Id, Year_Text__c=String.valueOf(testyear-2), Revenue__c=4, EBITDA__c=4));
		insert finList;
		
		Test.startTest();
		
		FinancialsYearlyRollover_Batch testClass = new FinancialsYearlyRollover_Batch();
		SchedulableContext ctx;
		testClass.execute(ctx);
		
		Test.stopTest();
	}
}