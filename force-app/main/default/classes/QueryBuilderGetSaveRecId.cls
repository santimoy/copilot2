public class QueryBuilderGetSaveRecId {
	@auraEnabled public Id newRecId;
    @auraEnabled public Boolean isPass;
    @auraEnabled public String validationStatus;
    @auraEnabled public String recentValidationError;
    @auraEnabled public DateTime lastValidated;
    @auraEnabled public String newExternalId;
}