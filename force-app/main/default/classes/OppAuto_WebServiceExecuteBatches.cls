/**
*@name      	: OppTuto_WebServiceExecuteBatches
*@developer 	: 10K developer
*@date      	: 2018-10-05
*@description   : This class is use to execute the batch 
*@Calling       : Detail Page Button (Button Name : Process Campaign Member)
**/
global with sharing class OppAuto_WebServiceExecuteBatches{
    @auraEnabled
     WebService static void ExecuteProfitProcessData(String campId){
        OppAuto_Service.updateCampaignProcessingStatus(campId, 'In Progress');
        OppAuto_ProcessCampaignMemberBtch oppAutoBtch = new OppAuto_ProcessCampaignMemberBtch(campId);
        oppAutoBtch.isCallingFromDetailPage = true;
        Database.executeBatch(oppAutoBtch,1); 
    }
}