/*
*	Description : It contains test coverage for Batch_ToReConcileInvalidData logics.
*   Author      : Nimisha
*	Created On  : November 08, 2019
*/
@isTest
private class Batch_ToReConcileInvalidData_Test {
	
    @testSetup
    public static void testSetup(){
        
        List<RecordType> lstAccRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Prospect'];
        Id RecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ignite Opportunity').getRecordTypeId();
        
        List<Account> lstAccount = new List<Account>();
        for(integer i = 1; i <= 5; i++) {
            lstAccount.add(new Account(Name = 'Test Account '+i, RecordTypeId = lstAccRecordType[0].Id));
        }
        insert lstAccount;
        
        List<Opportunity> lstOpportunity = New List<Opportunity>();
        for(integer i = 1; i <= 5; i++) {
            lstOpportunity.add(new Opportunity(Name = 'Test Opportunity '+i, 
                                               StageName = 'Prospecting', 
                                               CloseDate = System.today(), 
                                               AccountId = lstAccount[i-1].Id,
                                               Target_Company__c = lstAccount[0].Id,
                                               RecordTypeId = RecordTypeIdOpportunity,
                                               Amount = 1.00));
        }
        insert lstOpportunity;
        
        //Creating Campaign
        Campaign objCampaign = new Campaign(Name = 'Test Campaign', 
                                            IsActive = TRUE, 
                                            Type = 'Portfolio Company Demand Gen', 
                                            StartDate = System.today());
        insert objCampaign;
        
        List<Contact> lstContact = new List<Contact>();
        for(integer i = 1; i <= 5; i++){
            lstContact.add(new Contact(FirstName = 'Test',LastName =  'Contact '+i, AccountId = lstAccount[i-1].Id));
        }
        insert lstContact;
        
        List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
        for(integer i = 1; i <= 5; i++){
        	lstCampaignMember.add(new CampaignMember(Attendee_Type__c = 'Executive Guest',
        	                                         Status = (i < 2 ? 'Meeting Set' : 'Call Set'),
        	                                         campaignId = objCampaign.Id,
        	                                         contactId = lstContact[i-1].Id));
        }
        insert lstCampaignMember;
        
        //Creating CampaignInfluenceModel
        CampaignInfluenceModel objModel = [SELECT Id FROM CampaignInfluenceModel  LIMIT 1];
        List<CampaignInfluence> lstCampaignInfluence = new List<CampaignInfluence>();
        for(Integer i = 1; i <= 5; i++){
            lstCampaignInfluence.add(new CampaignInfluence(CampaignId = objCampaign.Id,
                                                           OpportunityId = lstOpportunity[i-1].Id,
                                                           ModelId = objModel.Id,
                                                           Opp_Auto_Is_Calling_Manual__c = true));
        }
        insert lstCampaignInfluence;
    }
    
    @isTest 
	static void testMarkOppForDeletion() {
	    test.startTest();
	    Batch_ToReConcileInvalidData objBatch_ToReConcileInvalidData = new Batch_ToReConcileInvalidData();
	    Database.executeBatch(objBatch_ToReConcileInvalidData, 200);
	    Test.stopTest();
	    //System.assertEquals(5, [SELECT Id FROM Opportunity WHERE Mark_for_Deletion__c = true].size());
	}
}