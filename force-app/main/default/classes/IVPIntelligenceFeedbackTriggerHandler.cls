public class IVPIntelligenceFeedbackTriggerHandler {
    
    public static void onAfterUpdate(List<Intelligence_Feedback__c> updatedRecords, Map<Id, Intelligence_Feedback__c> oldRecordsMap){
        
        Map<Id, Id> mapOfAccountAndNewOwnerIds = new Map<Id, Id>();
        
        Id userId = UserInfo.getUserId();
        
        System.debug('on AfterUpdate: oldRecords in IVPIntelligenceFeedbackTriggerHandler:' + JSON.serializePretty(oldRecordsMap.values()));
        System.debug('on AfterUpdate: updatedRecords in IVPIntelligenceFeedbackTriggerHandler:' + JSON.serializePretty(updatedRecords));
        
        for(Intelligence_Feedback__c record:updatedRecords){
            
            if(record.Accurate_IQ_Score__c != oldRecordsMap.get(record.Id).Accurate_IQ_Score__c 
               && record.Accurate_IQ_Score__c == 'Accept'){ 
                
                mapOfAccountAndNewOwnerIds.put(record.Company_Evaluated__c, userId);
            }
        }
        
        if(!mapOfAccountAndNewOwnerIds.isEmpty()){
            IVPCompanyService.changeAccountOwner(mapOfAccountAndNewOwnerIds);
        }
    }
}