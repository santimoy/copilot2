/**
 * @author Nazia Khanam
 * @date 2018-05-19
 * @className IVPCompanyListViewCtlrTest
 * @description : To test the IVPComapanyListViewCtlr..
 */

@isTest
private class IVPCompanyListViewCtlrTest {

	private static testMethod void test1() {
        Test.startTest();
		IVPCompanyListViewCtlr IVPCompanyList = new IVPCompanyListViewCtlr();
        Test.setMock(HttpCalloutMock.class, new IVPWatchDogMock());
        IVPCompanyListViewCtlr.InitData Initdata = IVPCompanyListViewCtlr.getInitData();
       Test.stopTest();
        system.debug('init data:'+Initdata);
        system.assertNotEquals(NULL,Initdata);
    }
    private static testMethod void test2(){
    	List<ListView> listviews =[SELECT Id,DeveloperName FROM ListView WHERE SObjectType='Account' LIMIT 1];
    	Test.startTest();
        IVPCompanyListViewCtlr.maintainDefautListView(listviews[0].DeveloperName);
        Test.stopTest();
        IVP_SD_ListView__c IVP_SD_ListView = IVPUtils.currentUserSetting();
        system.assertEquals(listviews[0].DeveloperName, IVP_SD_ListView.List_View_Name__c);
    }
	private static testMethod void test3(){
        IVPCompanyListViewCtlr.maintainChartListView('{"fieldApi" : "Owner_Status_Calc__c", "selectedValue" : "Current Dash" , "source" : "hart", "viewName" : "chart_Current Dash"}');
    }
}