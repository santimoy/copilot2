public with sharing class checkRecursive {
    
    // 1-18-2018 WN updated for multiple trigger support
	static set<String> triggerNames = new set<String>();
    public static boolean runOnce(string trgname, 
    								boolean isBefore, boolean isAfter,
    								boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete) {
    	string t = trgname 
    				+ (isBefore ? '_before' : isAfter ? '_after' : '')
    				+ (isInsert ? '_insert' : isUpdate ? ' _update' : isDelete ? '_delete' : isUndelete ? '_undelete' : '');
    	boolean run = !triggerNames.contains(t);
        triggerNames.add(t);
        return run;
    }
    
    // original runOnce applies to the ContactTrigger
    public static boolean runOnce() {
    	return runOnce('ContactTrigger', false, false, false, false, false, false);
    }
    
    // clears all recursive flags
    public static void resetAll() {
    	triggerNames.clear();
    }
    
    // resets recursive flags using triggername.startsWith
    public static void reset(string trgname) {
    	set<string> removeSet = new set<string>();
    	for(string s : triggerNames)
	    	if(s.startsWithIgnoreCase(trgname))
    			removeSet.add(s);
    	triggerNames.removeAll(removeSet);
    }
    
    static testmethod void testCheckRecursive() {
    	
    	Test.startTest();
    	
    	checkRecursive.runOnce('TestTrigger', true, false, true, false, false, false);
    	checkRecursive.reset('TestTrigger');
    	checkRecursive.runOnce();
    	checkRecursive.resetAll();
    	
    	Test.stopTest();
    }
}