/*
Author : Snehil Jaiswal
Description : This class will be used for writing all the functionalities related to Sourcing_Preference__c object. 
Date Created : 15th March 2019
Change 1 : 
*/
public class SourcingPreferenceHelper {
    // Method to update Serialized__c field
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered
    // @return void. 
    public static void updateSerializedField(Map<Id,Sourcing_Preference__c> oldTriggerMap, List<Sourcing_Preference__c> newTriggerList, Boolean isInsert) {
        //List<Id> sourcPrefIdList = new List<Id>();
        List<Sourcing_Preference__c> sourcPrefUpsertList = new List<Sourcing_Preference__c>();
        List<Sourcing_Preference__c> updateList = new List<Sourcing_Preference__c>();
        String getCurrentUserDbCol = getDbCol();
        
        for (Sourcing_Preference__c a : newTriggerList) {
            Sourcing_Preference__c sourcingPrefRec = a.clone(true, true, true, true); 
            sourcingPrefRec.Id = a.Id;
            sourcingPrefRec.Cloned_From__c = a.Cloned_From__c;
            sourcingPrefRec.External_Id__c = a.External_Id__c;
            sourcingPrefRec.Include_Exclude__c = a.Include_Exclude__c;
            sourcingPrefRec.JSON_QueryBuilder__c = a.JSON_QueryBuilder__c;
            sourcingPrefRec.Last_Validated__c = a.Last_Validated__c;
            sourcingPrefRec.Name__c = a.Name__c;
            sourcingPrefRec.Recent_Validation_Error__c = a.Recent_Validation_Error__c;
            sourcingPrefRec.Status__c = a.Status__c;
            sourcingPrefRec.SQL_Where_Clause__c = a.SQL_Where_Clause__c;
            sourcingPrefRec.Validation_Status__c = a.Validation_Status__c;
            sourcingPrefRec.SQL_Query__c = a.SQL_Query__c;
            sourcingPrefRec.Serialized__c = '';
            
            String DWH_Exception =  a.DWH_Exception__c;
            String DWH_Req_Payload =  a.DWH_Req_Payload__c;
            String DWH_Resp_Body =  a.DWH_Resp_Body__c;
            String DWH_Status =  a.DWH_Status__c;
            Integer DWH_Resp_Code =  Integer.valueOf(a.DWH_Resp_Code__c);
            
            sourcingPrefRec.DWH_Exception__c='';
            sourcingPrefRec.DWH_Req_Payload__c='';
            sourcingPrefRec.DWH_Resp_Body__c='';
            sourcingPrefRec.DWH_Resp_Code__c=null;
            sourcingPrefRec.DWH_Status__c='';
            string recUrl = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/'+sourcingPrefRec.Id+' ';
            sourcingPrefWrapper sourPrfWrap = new sourcingPrefWrapper();
            sourPrfWrap.record = sourcingPrefRec;
            sourPrfWrap.url = recUrl;
            string serializedData = JSON.serialize(sourPrfWrap, false);
            integer maxSize = 131071;
            if(serializedData != null && serializedData.length() > maxSize ){
                sourcingPrefRec.Serialized__c = serializedData.substring(0, maxSize);
            }else{
                sourcingPrefRec.Serialized__c = serializedData;
            }
            sourcingPrefRec.DWH_Exception__c = DWH_Exception;
            sourcingPrefRec.DWH_Req_Payload__c = DWH_Req_Payload;
            sourcingPrefRec.DWH_Resp_Body__c = DWH_Resp_Body;
            sourcingPrefRec.DWH_Resp_Code__c = DWH_Resp_Code;
            sourcingPrefRec.DWH_Status__c = DWH_Status;
            
            String newSubStringOfDbCol = '';
            String newSqlQuery = sourcingPrefRec.SQL_Query__c;
            
            if(newSqlQuery != null && newSqlQuery != ''){
                newSubStringOfDbCol = newSqlQuery.substringBetween('SELECT ', ' From');
            }
            if(sourcingPrefRec.Sent_to_DWH__c == false){
                if(isInsert){
                    sourcPrefUpsertList.add(sourcingPrefRec);
                    system.debug('In insert');
                }else if(newSubStringOfDbCol != '' && newSqlQuery != null && getCurrentUserDbCol != newSubStringOfDbCol){
                    system.debug('in db coln diff');
                    sourcingPrefRec.SQL_Query__c = newSqlQuery.replace(newSubStringOfDbCol, getCurrentUserDbCol);
                    sourcPrefUpsertList.add(sourcingPrefRec);
                }else if(oldTriggerMap != null && oldTriggerMap.get(a.id).SQL_Query__c != a.SQL_Query__c || (oldTriggerMap.get(a.id).Is_Template__c != a.Is_Template__c 
                                                                                                             || oldTriggerMap.get(a.id).Name__c != a.Name__c || oldTriggerMap.get(a.id).Include_Exclude__c != a.Include_Exclude__c || oldTriggerMap.get(a.id).Status__c != a.Status__c)){
                                                                                                                 sourcPrefUpsertList.add(sourcingPrefRec);
                                                                                                                 system.debug('In other criteria');
                                                                                                             }
            }
            sourcingPrefRec.Sent_to_DWH__c = false;
            if(isInsert){
                sourcingPrefRec.Sent_to_DWH__c = true;
            }
            updateList.add(sourcingPrefRec);
        }
        if(updateList.size() > 0) {
            update updateList;
            if(sourcPrefUpsertList != null && sourcPrefUpsertList.size()>0){
                System.enqueueJob(new SourcingPreferenceUpsertQue(sourcPrefUpsertList));
            }
        }
    }
    
    
    
    // Method to get current user DB_Column field value.
    // argument.
    // @param .
    // @return string
    public static String getDbCol() {
        String commaSepratedList = '';
        try{
            Intelligence_User_Preferences__c intUserPrefRec = [Select Id, name, Display_Columns__c, User_Id__c, Sourcing_Signal_Category_Preferences__c, My_Dash_Signal_Preferences__c from Intelligence_User_Preferences__c Where User_Id__c =: UserInfo.getUserId() LIMIT 1];
            if(intUserPrefRec != null && intUserPrefRec.Display_Columns__c != null && intUserPrefRec.Display_Columns__c.trim() != ''){
                List<String> columnList = intUserPrefRec.Display_Columns__c.split(';');
                for(Integer i = 0; i < columnList.size(); i++){
                    commaSepratedList += columnList[i].substringBetween('(',')') + ', ' ;
                }
                commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length() - 2);
            }else{
                commaSepratedList = 'ivp_name';
            }
        } catch (Exception ex) {
            
        }
        return commaSepratedList;
    }
    
    // Wrapper Class to create value for Serialized__c field
    // argument.
    // @param .
    // @return . 
    public class sourcingPrefWrapper{
        public Sourcing_Preference__c record { get; set;}
        public string url { get; set;}
    }
    
    // Method to update Serialized__c field
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered
    // @return void. 
    public static void setExternalId(List<Sourcing_Preference__c> newTriggerList) {
        for (Sourcing_Preference__c a : newTriggerList) {
            if(a.External_Id__c == null && a.Sent_to_DWH__c == false){
                a.External_Id__c = getUniqueId();
            }
            a.Status__c = 'Active';
        }
    }
    
    
    
    // Method to get unique value for External_Id__c field
    // argument.
    // @param 
    // @return string. 
    public static string getUniqueId(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,5)+ '-' + h.SubString(5,10) + '-' + h.SubString(10,15);
        return guid;
    }
    
    // Method to update Is_Template field
    // argument.
    // @param newTriggerList is the list of listing for which the event is triggered
    // @return void. 
    public static void updateIsTemplate(List<Sourcing_Preference__c> newTriggerList) {
        for (Sourcing_Preference__c a : newTriggerList) {
            if(a.Status__c != null && a.Status__c == 'Archived'){
                a.Is_Template__c = false;
            }
        }
    }
    
}