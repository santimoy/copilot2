/**
 *  @date: May 9, 2018
 *  @author: Neha Gurnani
 *  @desciption: This class is used to cover the code of IVPSDLVMaintainVisibilityService's
**/
@isTest
private class IVPSDLVMaintainVisibilityServiceTest {
	private static testMethod void testPrepareUserAndShareListView() {
		List<GroupMember> listgroupMember = Database.query('SELECT Id, GroupId, Group.DeveloperName, UserOrGroupId FROM GroupMember');
		List<ListView> listviews = Database.query('Select Id,Name, DeveloperName, CreatedById, CreatedBy.Name FROM ListView WHERE SObjectType=\'Account\' AND (not Name like \'%public%\') ORDER BY LastModifiedDate DESC LIMIT 20' );	
		IVPTestFuel tFuel = new IVPTestFuel();
       
		List<Group> groups = tFuel.groups;
         Test.startTest();
		List<GroupMember> groupMembers = tFuel.groupMembers;
    		Map<id,String> records = IVPSDLVMaintainVisibilityService.prepareUserWithGroupNameMap(listviews);
    		IVPSDLVMaintainVisibilityService.shareListViews(records, listviews, 'Account');
		Test.stopTest();
		System.assert(listgroupMember.size()>records.size());
	}
    
    /**
 *  @date: May 22, 2018
 *  @author: Nazia Khanam
 *  @desciption: This Method is used to cover the code of IVPSDLVMaintainVisibilityService.checkAndCreateIVPGroupForUser() method.
**/
    
    private static testMethod void testcheckAndCreateIVPGroupForUser() {
        GroupMember grMember=[SELECT UserOrGroupId FROM GroupMember LIMIT 1];
        Group gr=[SELECT name from Group where Name!=NULL Limit 1];
		String resultString=IVPSDLVMaintainVisibilityService.checkAndCreateIVPGroupForUser(grMember.UserOrGroupId,gr.Name,true);
        IVPSDLVMaintainVisibilityService.getIVPUserGroupName(grMember.UserOrGroupId,gr.Name);
        String strOFSoqlQuery=IVPSDLVMaintainVisibilityService.soqlQeury;
        System.assertEquals(true,resultString.startsWith('tst_ivp'));
        system.assert(strOFSoqlQuery!=NULL);
    }
}