@isTest
public class IVPOwnershipTestFuel {
    public void populateIVPGeneralConfig(){
        System.runAs(new User(Id=UserInfo.getUserId())){
            Ownership_Assignment_Config__c oac = new Ownership_Assignment_Config__c();
            oac.SetupOwnerId=UserInfo.getUserId();
            oac.Bypass_Ownership_Validations__c = 'N';
            oac.Super_User__c=true;
            oac.Total_Count__c=550;
            oac.Priority_Count__c = 150;
            oac.Wait_To_Pick_Up_After_Drop__c = 3;
            oac.Wait_To_Reprioritize_After_Demote__c = 3;
            oac.No_Action_Auto_Transfer_After_N_Days__c = 3;
            oac.Ownership_Request_Balance__c = 5;
            
            insert oac;

            List<IVP_General_Config__c> ivpGeneralLst = new List<IVP_General_Config__c>();
            
            ivpGeneralLst.add(new IVP_General_Config__c(Name='total-count', Value__c='550'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='priority-count', Value__c='150'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='ownership-request-balance', Value__c='5'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='check-open-opportunity-last-n-days', Value__c='-1'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='wait-to-pick-up-after-drop', Value__c='3'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='email-notify-hour-gap-exp-date', Value__c='24'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='final-email-notify-hour-gap-exp-date', Value__c='1'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='ownership-request-balance-days', Value__c='30'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='partial-owner-interaction-n-days', Value__c='180'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='no-action-auto-transfer-after-n-days', Value__c='3'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='demote-auto-steal-available-n-days', Value__c='3'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='wait-to-re-prioritize-after-demote', Value__c='3'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='demote-auto-steal-for-n-days-all', Value__c='3'));
            ivpGeneralLst.add(new IVP_General_Config__c(Name='demote-auto-steal-for-n-days-partial', Value__c='1'));
            
            
            
            insert ivpGeneralLst;
        }
    }
    
    public void populateCommonConfig(){
        System.runAs(new User(Id=UserInfo.getUserId())){
            Ownership_Assignment_Config__c oac = new Ownership_Assignment_Config__c();
            oac.SetupOwnerId=UserInfo.getUserId();
            oac.Bypass_Ownership_Validations__c = 'N';
            oac.Super_User__c=true;
            oac.Total_Count__c=550;
            oac.Priority_Count__c = 150;
            oac.Wait_To_Pick_Up_After_Drop__c = 3;
            oac.Wait_To_Reprioritize_After_Demote__c = 3;
            oac.No_Action_Auto_Transfer_After_N_Days__c = 3;
            oac.Ownership_Request_Balance__c = 5;
            
            insert oac;
            List<Common_Config__c> commonConfigLst = new List<Common_Config__c>();
            
            Common_Config__c cconf = new Common_Config__c( SetupOwnerId=UserInfo.getOrganizationId());
            cconf.Task_Required_Fields__c='Name';
            cconf.Edit_Restriction_Message__c='You cannot <EDITTYPE> this activity record since the following Company fields are not filled out: <FIELDLIST>.';
            cconf.Task_Edit_Profiles__c='System Administrator, Bank Mgmt, Analyst Full,System Admin - External';
            commonConfigLst.add(cconf);
            
            insert commonConfigLst;
        }
    }
    
    public String prospectRTId{
        get{
            if(prospectRTId == NULL){
                prospectRTId = '';
                List<RecordType> rts = [SELECT Id, Name, DeveloperName FROM RecordType 
                                        WHERE SobjectType='ACCOUNT' AND DeveloperName='Prospect' LIMIT 1];
                if(!rts.isEmpty()){
                    prospectRTId = rts[0].Id;
                }
            }
            return prospectRTId;
        }set;
    }
    
    public String opptyIORTId{
        get{
            if(opptyIORTId == NULL){
                opptyIORTId = '';
                Id IORTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Investment Opportunity').getRecordTypeId();
                if(opptyIORTId != NULL){
                    opptyIORTId = IORTId;
                }
            }
            return opptyIORTId;
        }set;
    }
    
    public List<Account> accounts{
        get {
            if(accounts == NULL) {
                accounts = new List<Account>();
                String prospectId = prospectRTId;
                
                accounts.add(new Account(Name='Test Account 1', RecordTypeId=prospectId, Ownership_Status__c='Watch'));
                accounts.add(new Account(Name='Test Account 2', RecordTypeId=prospectId, Ownership_Status__c='Priority'));
                
                insert accounts;
            }
            return accounts;
        }
        set;
    }
    
    public List<Company_Request__c> companyRequests{
        get {
            if(companyRequests == NULL) {
                companyRequests = new List<Company_Request__c>();
                
                List<Account> accountLst = [Select Id,Name,RecordTypeId,Ownership_Status__c,OwnerId From Account Where Id In : accounts];
                
                companyRequests.add(new Company_Request__c(Status__c='Pending', Company__c=accountLst[0].Id, Request_By__c=accountLst[0].OwnerId, Request_To__c=UserInfo.getUserId()));
                companyRequests.add(new Company_Request__c(Status__c='Pending', Company__c=accountLst[1].Id, Request_By__c=UserInfo.getUserId(), Request_To__c=accountLst[1].OwnerId));
                
                insert companyRequests;
            }
            return companyRequests;
        }
        set;
    }
    
    public List<User> users{
        get {
            if (users == NULL) {
                List<Profile> profiles = [SELECT Id FROM PROFILE 
                                          WHERE Name IN ('Analyst Full') ORDER By Name ASC];
                String namePrefix = 'own';
                users = new List<User>{
                    new User(alias = namePrefix+'1', email=namePrefix+'Unit.Test@unittest.com',
                             emailencodingkey='UTF-8', firstName='First2', lastname=namePrefix+'Last2', languagelocalekey='en_US',
                             localesIdkey='en_US', profileId = profiles[0].Id,
                             timezonesIdkey='Europe/Berlin', username=namePrefix+'unit.test1@unittest.com', Ownership_Request_Balance__c=5,
                             Priority_Company_Count__c=120, Watch_Company_Count__c=200),
                        
                        new User(alias = namePrefix+'2', email=namePrefix+'Unit.Test2@unittest.com',
                                 emailencodingkey='UTF-8', firstName='First2', lastname='Last2', languagelocalekey='en_US',
                                 localesIdkey='en_US', profileId = profiles[0].Id,
                                 timezonesIdkey='Europe/Berlin', username=namePrefix+'unit.test2@unittest.com', Ownership_Request_Balance__c=5,
                                 Priority_Company_Count__c=150, Watch_Company_Count__c=350),
                        
                        new User(alias = namePrefix+'3', email=namePrefix+'Unit.Test3@unittest.com',
                                 emailencodingkey='UTF-8', firstName='First3', lastname='Last3', languagelocalekey='en_US',
                                 localesIdkey='en_US', profileId = profiles[0].Id,
                                 timezonesIdkey='Europe/Berlin', username=namePrefix+'unit.test3@unittest.com', Ownership_Request_Balance__c=5,
                                 Priority_Company_Count__c=100, Watch_Company_Count__c=300)   
                    };
                insert users;
            }
            return users;
        }
        set;
    }
    
    public List<Holiday> holidays{
        get {
            if(holidays == NULL) {
                holidays = new List<Holiday>();
                
                holidays.add(new Holiday(Name='Test Holiday 1', IsAllDay=true, ActivityDate=System.today()));
                
                insert holidays;
            }
            return holidays;
        }
        set;
    }
    
    public List<Opportunity> opportunities{
        get {
            if(opportunities == NULL) {
                opportunities = new List<Opportunity>();
                String accProspectRTId = prospectRTId;
                List<Account> accountLst = accounts;
                
                opportunities.add(new Opportunity(Name='Test Opportunity1', StageName='Prospecting', CloseDate=System.today(), AccountId=accountLst[0].Id, RecordTypeId=opptyIORTId));
                opportunities.add(new Opportunity(Name='Test Opportunity1', StageName='Prospecting', CloseDate=System.today(), AccountId=accountLst[0].Id, RecordTypeId=opptyIORTId));
                
                insert opportunities;
            }
            return opportunities;
        }
        set;
    }
    
    public List<Contact> contacts{
        get {
            if(contacts == NULL) {
                contacts = new List<Contact>();
                
                contacts.add(new Contact(Title='Mr.',FirstName='Test',LastName='Contact 1',AccountId=accounts[0].Id));
                contacts.add(new Contact(Title='Mr.',FirstName='Test',LastName='Contact 2',AccountId=accounts[0].Id));
                
                insert contacts;
            }
            return contacts;
        }
        set;
    }
    
    public List<Task> tasks{
        get {
            if(tasks == NULL) {
                tasks = new List<Task>();
                List<Account> accountLst = accounts;
                List<Contact> contactLst = contacts;
                
                tasks.add(new Task(Subject='Email:Test Task1[Outreach][OUT]',Description='To: Test Account, From: Test Contact',Status='New',Priority='Normal',CallType='Outbound',WhatId=accountLst[0].Id,Type__c='Email',WhoId=contactLst[0].Id,Source__c='Outbound'));
                tasks.add(new Task(Subject='Follow up:Test Task2',Status='New',Priority='Normal',CallType='Outbound',WhatId=accountLst[0].Id,Type__c='Email',WhoId=contactLst[0].Id,Source__c='Outbound'));
                tasks.add(new Task(Subject='Following up:Test Task3',Status='New',Priority='Normal',CallType='Outbound',WhatId=accountLst[0].Id,Type__c='Email',WhoId=contactLst[1].Id,Source__c='Outbound'));
                tasks.add(new Task(Subject='Canceled:Test Task4[Outreach]',Description='to: Test Account, from: Test Contact',Status='New',Priority='Normal',CallType='Outbound',WhatId=accountLst[0].Id,Type__c='Email',Source__c='Outbound'));
                tasks.add(new Task(Subject='Reply:Test Task1',Status='New',Priority='Normal',CallType='Inbound',WhatId=accountLst[0].Id,Type__c='Email',WhoId=contactLst[0].Id,Source__c='Inbound'));
                tasks.add(new Task(Subject='Sent Email:Test Task2[Outreach][IN]',Description='to: Test Account, from: Test Contact',Status='New',Priority='Normal',CallType='Inbound',WhatId=accountLst[0].Id,Type__c='Email',WhoId=contactLst[0].Id,Source__c='Inbound'));
                tasks.add(new Task(Subject='Sent Email:Test Task3[Outreach]',Description='To: Test Account, From: Test Contact',Status='New',Priority='Normal',CallType='Inbound',WhatId=accountLst[0].Id,Type__c='Email',WhoId=contactLst[1].Id,Source__c='Inbound'));
                tasks.add(new Task(Subject='Meeting Booked:Test Task4',Status='New',Priority='Normal',CallType='Inbound',WhatId=accountLst[0].Id,Type__c='Email',Source__c='Inbound'));
                
                insert tasks;
            }
            return tasks;
        }
        set;
    }
}