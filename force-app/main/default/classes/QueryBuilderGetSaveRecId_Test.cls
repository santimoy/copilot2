@isTest
public class QueryBuilderGetSaveRecId_Test {
     @TestSetup
    static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@ivptest.com');
        insert u1;
        
    }
    
    public static testmethod void Method() 
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
            sourcingPreference.Name__c = 'Test';
            sourcingPreference.Include_Exclude__c = 'Include';
            sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
            insert sourcingPreference;
            
            test.startTest();
            
            QueryBuilderGetSaveRecId QbSave = new QueryBuilderGetSaveRecId();
            QbSave.isPass = true;
            QbSave.validationStatus = 'Valid';
            QbSave.recentValidationError = '';
            QbSave.lastValidated = System.now();
            QbSave.newRecId = sourcingPreference.Id;
            QbSave.newExternalId = sourcingPreference.External_Id__c;
            test.stopTest();
            
        }
    }
}