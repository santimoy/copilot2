/**
 * @author 10k advisor
 * @date 2018-04-16
 * @className IVPMetadataServiceProvider
 * @group IVPSmartDash / tagging
 *
 * @description contains methods to handle Listview related functionality
 */
 public class IVPMetadataServiceProvider {
	
	public class ListViewData{
		public String filterScope;
		public Boolean includeFilter;
		public String source;
		public String fieldApi;
		public String value;
		public ListViewData(){
			filterScope = 'Mine';
			includeFilter = false;
		}
	}
	
	public static map<String, String> operationMap = new map<String, String>{
		'equals'=>'equals',
		'notequals'=>'notEqual',
		'notequal'=>'notEqual',
		'lessthan'=>'lessThan',
		'greaterthan'=>'greaterThan',
		'lessthanorequalto'=>'lessOrEqual',
		'lessorequal'=>'lessOrEqual',
		'greaterorequal'=>'greaterOrEqual',
		'contains'=>'contains',
		'like'=>'contains',
		'nocontains'=>'notContain',
		'notcontain'=>'notContain',
		'includes'=>'includes',
		'excludes'=>'excludes',
		'startswith'=>'startsWith',
		'startsWith'=>'startsWith'
	};
	public Static String HeadFilterScope = 'MINE';
	
	public static ListViewData listViewObj;
	
	public class IVPMetadataServiceProviderException extends Exception {}
	
	public static IVPMetadataService.MetadataPort service{
		get{
			if(service == NULL){
				service = new IVPMetadataService.MetadataPort();
		        service.SessionHeader = new IVPMetadataService.SessionHeader_element();
		        service.SessionHeader.sessionId = IVPUtils.USER_SESSION_ID;
			}
			return service;
		}set;
    }
	
    public static IVPMetadataService.ListView readListViewDetailByName(String viewName){
		if(System.Test.isRunningTest()){
			System.Test.setMock(WebServiceMock.class, new IVPWebServiceMockImpl());
		}
        // Read List View definition
        IVPMetadataService.ReadListViewResult readListViewResult = (IVPMetadataService.ReadListViewResult)
        	service.readMetadata('ListView', new String[] { viewName });
        
        if(readListViewResult != NULL && !readListViewResult.getRecords().isEmpty()){
        	IVPMetadataService.ListView listView =(IVPMetadataService.ListView) readListViewResult.getRecords()[0];
        	
        	if(listView != NULL && listView.columns != NULL){
        		return listView;
        	}
        }
        return NULL;
	}
	
	public static IVPMetadataService.ListView extractListView(IVP_SD_ListView__c ipvSDListView){
		
		if(ipvSDListView != NULL && String.isNotEmpty(ipvSDListView.List_View_Name__c)){
	    	List<ListView> currentListViews = [SELECT Id, DeveloperName 
	    										FROM ListView 
	    										WHERE SobjectType = 'Account' AND DeveloperName =: ipvSDListView.List_View_Name__c];
			
			IVPMetadataService.ListView currentListView;
			
			if(!currentListViews.isEmpty()){
				return readListViewDetailByName('ACCOUNT.' + currentListViews[0].DeveloperName);
			}
		}
		return NULL;	
	}
	/*
	private static IVPMetadataService.ListView prepareListView(IVPListViewMetaData listView){
	    IVPMetadataService.ListView currentListView = new IVPMetadataService.ListView();
	    currentListView.columns = new List<String>();
	    Set<String> columnsSet = new Set<String>();
	    
	    for(IVPListViewMetaData.ColumnData column : listView.columns){
	        String fieldName = column.fieldNameOrPath;
	        if(!fieldName.endsWithIgnoreCase('__c')){
	            String objFieldName = fieldName.toLowerCase();
	            fieldName = '';
	            if(objFieldName == 'recordtype.name'){
	                objFieldName = 'recordtypeid';
	            }else if(objFieldName == 'owner.alias'){
	                objFieldName = 'ownerid';
	            }
	            if(IVPListViewMetaData.fieldWithName.containsKey(objFieldName)){
	                fieldName = IVPListViewMetaData.fieldWithName.get(objFieldName);
	            }
	        }
	        if(!columnsSet.contains(fieldName) && fieldName != ''){
	            currentListView.columns.add(fieldName);
	            columnsSet.add(fieldName);
	        }
	        String scope = IVPUtils.extractSOQLScope(listView.query);
	        currentListView.filterScope = HeadFilterScope;//scope=='' ? 'Mine': 'Mine';
	    }
	    
	    return currentListView;
	}
	*/
	public static Map<String, Object> maintainListView(Map<String, Object> valueMap){
		Map<String, Object> resultMap = new Map<String, Object>{'success'=>true};
		listViewObj = new ListViewData();
		try{
			String source = String.valueOf(valueMap.get('source'));
			listViewObj.source = source.toLowerCase();
			listViewObj.fieldApi = (String) valueMap.get('fieldApi');
			listViewObj.value = (String) valueMap.get('selectedValue');
			Schema.DisplayType fieldType = IVPListViewMetaData.extractFieldType(listViewObj.fieldApi);
			if(fieldType == Schema.DisplayType.Boolean){
			    listViewObj.value = listViewObj.value=='true' ? '1' : '0';
			}
			String currentLVId = '';
			if(valueMap.containsKey('selectedLVId')){
				currentLVId = (String) valueMap.get('selectedLVId');
			}
			Object chkFilter = valueMap.get('chkFilter');
			if(chkFilter != NULL && chkFilter instanceof Boolean){
				listViewObj.includeFilter = Boolean.valueOf(chkFilter);
			}
			
			Object filterScope = valueMap.get('filterScope');
			if(filterScope != NULL && filterScope instanceof String){
				listViewObj.filterScope = (String)filterScope;
			}
			Map<String, Object> filtersMap = maintainListView(listViewObj, currentLVId);//, listViewObj.fieldApi, listViewObj.value, listViewObj.source);
			
			for(String key: filtersMap.keySet()){
			    String fkey = key;
			    if(key.endsWithIgnoreCase('__c')){
			        fkey=key.toLowerCase();
			    }
			    Map<String, Object> fieldDetails;
			    if(IVPListViewMetaData.fieldWithLabel.containsKey(fkey)){
			        fieldDetails = (Map<String, Object>)IVPListViewMetaData.fieldWithLabel.get(fkey);
			        fieldDetails.put('value', filtersMap.get(key));
			    }else{
			        fieldDetails = new Map<String, Object>{
			            'value'=>filtersMap.get(key),
			            'fieldLabel'=>key,
			            'fieldType'=>'string'
			        };
			    }
		        resultMap.put(key,fieldDetails);
			}
		}catch(Exception excp){
			resultMap.put('success', false);
			resultMap.put('errors', excp.getMessage());
		}
		return resultMap;
	}
	
	public static Map<String, Object> maintainListView(ListViewData listViewObj, String currentLVId){//, String fieldName, String newValue, String filterType){
		try{
		    String userId = UserInfo.getUserId();
			IVPSDLVMaintainVisibilityService.getIVPUserGroupName(userId, UserInfo.getName());
			String groupName = IVPSDLVMaintainVisibilityService.getIVPUserGroupName(userId, UserInfo.getName());
            IVPMetadataService.ListView currentListView = IVPUtils.getListOwnIVPView(IVPUtils.IVP_SD_NAME_INITIAL+listViewObj.source+'_'+userId);
            
            if(listViewObj.source == 'tag'){
				currentListView = prepareTagListView(currentListView);
			}else if(listViewObj.source == 'chart'){
				currentListView = prepareChartListView(currentListView);
			}
			
			if(currentListView != NULL){
				IVPMetadataService.ListView lastAccessLV;
				if(currentLVId != ''){
		       		lastAccessLV = new IVPListViewMetaData().parse(IVPUtils.fetchListViewDetailById(currentLVId));
				}else{
			    	lastAccessLV = getLastAccessListView();
				}
    			if(lastAccessLV != NULL && lastAccessLV.columns != NULL ){
    			    if(!lastAccessLV.columns.isEmpty()){
    			        currentListView.columns = lastAccessLV.columns;
    			    }
    			    // while preparing tag listview copying filters of listview to currentListView if needed.
    		        if(listViewObj.includeFilter && listViewObj.source=='tag'){
    					if(currentListView.filters != NULL && !currentListView.filters.isEmpty()){
    						currentListView.filters = lastAccessLV.filters;
    					}
    				}
    			}
				if(!listViewObj.includeFilter){
					currentListView.filters = new IVPMetadataService.ListViewFilter[]{};
					currentListView.booleanFilter = '';
				}
				
				return modifiyListView(currentListView, listViewObj);
			}
		}catch(Exception excp){
			System.debug(excp.getLineNumber());
			System.debug(excp.getStackTraceString());
			System.debug(excp.getMessage());
			throw new IVPMetadataServiceProviderException(excp.getMessage());
		}
		return new Map<String, Object>();
	}
	
	public static IVPMetadataService.ListView prepareTagListView(IVPMetadataService.ListView currentListView){
		
		if(currentListView == NULL ){
			IVP_SD_ListView__c ipvSDListView = IVPUtils.currentUserSetting();
			currentListView = extractListView(ipvSDListView);
            
            if(currentListView == NULL){
		        ipvSDListView = IVPUtils.currentOrgSetting();
		        if(String.isNotEmpty(ipvSDListView.List_View_Name__c)){
			        currentListView = IVPUtils.getListOwnIVPView(ipvSDListView.List_View_Name__c);
				}
			}
			
		}
		return currentListView;
	}
	
	private static IVPMetadataService.ListView getLastAccessListView(){
		IVPMetadataService.ListView currentListView;
		IVP_SD_ListView__c ipvSDListView = IVPUtils.currentUserSetting();
        if(ipvSDListView != NULL && String.isNotEmpty(ipvSDListView.List_View_Name__c)){
	        currentListView = IVPUtils.getListOwnIVPView(ipvSDListView.List_View_Name__c);
		}
		return currentListView;
	}
	
	public static IVPMetadataService.ListView prepareChartListView(IVPMetadataService.ListView currentListView){
		
		if(currentListView == NULL ){
			IVP_SD_ListView__c ipvSDListView = IVPUtils.currentOrgSetting();
			currentListView = extractListView(ipvSDListView);
			if(currentListView == NULL){
                ipvSDListView = IVPUtils.currentUserSetting();
				currentListView = extractListView(ipvSDListView);
			}
		}
		return currentListView;
	}
	
	public static Map<String, Object> modifiyListView(IVPMetadataService.ListView currentListView, ListViewData listViewObj){
		String fieldName = listViewObj.fieldApi;
		String newValue = listViewObj.value;
		Map<String, Object> filtersMap = new Map<String, Object>();
		if(currentListView != NULL){
			currentListView.fullName = 'ACCOUNT.ivp_sd_';
			currentListView.Label = 'Companies ';
			currentListView.fullName += listViewObj.source;
			
			if(listViewObj.source == 'tag'){
				currentListView.Label += 'w/ Topic: ' + (String.isEmpty(newValue) ? 'ALL' : newValue);
			}else if(listViewObj.source == 'chart'){
				String fieldLabel = IVPUtils.getAccountFieldLabel(fieldName);
				currentListView.Label += 'filtered by Dash Breakdown';
			}
			currentListView.fullName += '_' + UserInfo.getUserId();
            if(currentListView.Label.length() >40){
            	currentListView.Label = currentListView.Label.substring(0,37)+'...';
            }
    		String filterLogic = '';
    		Integer filterIndex = 1;
        	if(currentListView.filters == NULL){
	        	currentListView.filters = new IVPMetadataService.ListViewFilter[]{};
	        }
	        
	        Boolean noRecordTypeFilter = true;
	        String accProspectRT = IVPUtils.accProspectRTId !='' ? 'Prospect' : '';
	        System.debug(fieldName);
        	if(!fieldName.endsWithIgnoreCase('__c')
        		&& IVPListViewMetaData.fieldWithName.containsKey(fieldName.toLowerCase())){
        		
        		fieldName = IVPListViewMetaData.fieldWithName.get(fieldName.toLowerCase());
        	}
        	System.debug(fieldName);
        	String filterToRemove = '';
        	Map<String, Integer> filterWithIndex = new Map<String, Integer>();
        	Integer existingIndex = currentListView.filters.size();
        	Map<String, IVPMetadataService.ListViewFilter> filterMap = new Map<String, IVPMetadataService.ListViewFilter>();
        	for(Integer index = 0, length=currentListView.filters.size(); index < length; index++){
        		IVPMetadataService.ListViewFilter filter = currentListView.filters[index];
        		filter.operation=filter.operation.toLowerCase();
        		
        		if(filter.operation == 'like'){
        			filter.value=filter.value.remove('\'');
					if(filter.value.endsWith('%')){
        				filter.operation = 'startswith';
	        			if(filter.value.startsWith('%')){
        					filter.operation = 'contains';
        				}
        			}
        		}
        		filter.operation = operationMap.get(filter.operation.toLowerCase());
            	
            	if(filter.operation == NULL){
            		System.debug('notfound '+filter.field);
            		continue;
            	}
            	
        		if(filter.operation == 'contains' || filter.operation == 'notContain' || filter.operation == 'startsWith'){
            		filter.value = filter.value.remove('%');
            	} else if(filter.operation == 'greaterOrEqual' || filter.operation == 'lessOrEqual'){
            		filter.value = filter.value.remove(',');
            	}
            	
        		String key = filter.field+':'+filter.operation;
        		if(!filterMap.containsKey(key)){
        			filterMap.put(key, filter);
        		}else{
        			IVPMetadataService.ListViewFilter oldFilter = filterMap.get(key);
        			oldFilter.value+=','+filter.value;
        			filterMap.put(key, oldFilter);
        		}
        		
        	}
        	
        	currentListView.filters = filterMap.values();
            for(Integer index = currentListView.filters.size()-1; index > -1; index--){
            	IVPMetadataService.ListViewFilter filter = currentListView.filters[index];
            	filter.operation = operationMap.get(filter.operation.toLowerCase());
            	if(filter.field.endsWith('RECORDTYPE') ){
            		if(!noRecordTypeFilter){
            			currentListView.filters.remove(index);
            			continue;
            		}
            		if( accProspectRT != ''){
	        			filter.value = 'ACCOUNT.'+accProspectRT;
	        			filter.operation = 'equals';
	        			filterLogic += filterIndex++ + ' AND ';
	        			noRecordTypeFilter = false;
	        			filtersMap.put('ACCOUNT.RECORDTYPE', accProspectRT);
	        			continue;
            		}
            	}
            	if(!filterWithIndex.containsKey(filter.field)){
            		filterWithIndex.put(filter.field, index);
            	}
            	
            	if(filter.field.equalsIgnoreCase(fieldName)){
            		fieldName = currentListView.filters[index].field;
            		currentListView.filters.remove(index);
            	}else{
            		filterLogic += filterIndex++ + ' AND ';
            		filtersMap.put(filter.field, filter.value);
            	}
            	filtersMap.put(filter.field, filter.value);
            }
            
            String newFilterLogic ='';
            /*
            for(Integer index = 1,length = currentListView.filters.size(); index < length; ){
            	System.debug(length+'='+existingIndex + '=' + index);
            	if(index < length-1){
            		newFilterLogic += index++ + ' AND ';
            	}else{
            		newFilterLogic += index++ + ' AND ' + index++;
            	}
            }
            
            System.debug('newFilterLogic'+newFilterLogic);*/
            
            // filterLogic = newFilterLogic;
            if(noRecordTypeFilter && accProspectRT != ''){
            	currentListView.filters.add(prepareFilter('ACCOUNT.RECORDTYPE', 'equals', accProspectRT));
            	filterLogic += filterIndex++ + ' AND ';
            	filtersMap.put('ACCOUNT.RECORDTYPE', accProspectRT);
            }
            
            filterLogic = filterLogic.removeEnd(' AND ');
        	currentListView.booleanFilter = '';
            
            if(String.isNotEmpty(newValue) || listViewObj.source == 'chart') {
        		filterLogic+=(filterLogic == '' ? '' :  ' AND ' );
            	
            	if(listViewObj.source == 'tag'){
            		filterLogic+= '(';
            		String startsWith='';
            		String contains='';
            		for(String tagValue: newValue.split(',')){
		            	startsWith += tagValue+';,';
		            	contains += ';'+tagValue+';,';
            		}
	            	
	            	currentListView.filters.add(prepareFilter(fieldName, 'startsWith', startsWith.removeEnd(',')));
	            	currentListView.filters.add(prepareFilter(fieldName, 'contains', + contains.removeEnd(',')));
	            	
	            	filterLogic += filterIndex++ + ' OR ' + filterIndex++ +' OR ';
            		filterLogic = filterLogic.removeEnd(' OR ');
            		filterLogic+=')';
            		
            	}else if(listViewObj.source == 'chart'){
            		currentListView.filters.add(prepareFilter(fieldName, 'equals', newValue));
	            	
	            	filterLogic += filterIndex;
	            	filtersMap.put(fieldName, newValue);
            	}
            	currentListView.booleanFilter = filterLogic;
            }
            currentListView.filterScope = listViewObj.filterScope;
            String groupName = IVPSDLVMaintainVisibilityService.getIVPUserGroupName(UserInfo.getUserId(),UserInfo.getName());
            if(groupName != ''){
            	IVPMetadataService.SharedTo sharedTo = new IVPMetadataService.SharedTo();
		        sharedTo.group_x = new String[]{groupName};
		        currentListView.sharedTo = sharedTo;
            }
            if(filterIndex >= 11){
            	throw new IVPMetadataServiceProviderException('Too many filters are applied!');
            }
            if(currentListView.columns != NULL ) {
                // System.debug(JSON.serialize(currentListView));
                List<IVPMetadataService.UpsertResult> results =
	                service.upsertMetadata(
	                    new IVPMetadataService.Metadata[] { currentListView });
	            handleUpsertResults(results[0]);
	        }
	        return filtersMap;
		}
		return new Map<String, Object>();
	}
	
	private static IVPMetadataService.ListViewFilter prepareFilter(String fieldName, String operation, String newValue){
		IVPMetadataService.ListViewFilter newFilter = new IVPMetadataService.ListViewFilter();
    	newFilter.operation = operation;
    	newFilter.field = fieldName;
    	newFilter.value = (fieldName == 'ACCOUNT.RECORDTYPE' ? 'ACCOUNT.' : '' ) + newValue;
		return newFilter;
	}
	
	public static void handleUpsertResults(IVPMetadataService.UpsertResult upsertResult) {
        // Nothing to see?
        if(upsertResult == NULL || upsertResult.success)
            return;
        // Construct error message and throw an exception
        if(upsertResult.errors != NULL) {
            List<String> messages = new List<String>();
            messages.add(
                (upsertResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + upsertResult.fullName + '.');
            for(IVPMetadataService.Error error : upsertResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields != NULL && error.fields.size()>0 ?
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(!messages.isEmpty())
                throw new IVPMetadataServiceProviderException(String.join(messages, ' '));
        }
        if(!upsertResult.success)
            throw new IVPMetadataServiceProviderException('Request failed with no specified error.');
    }
    
    public static void shareListViewiWithGroup(String viewName, String groupName){
        if(System.Test.isRunningTest()){
			System.Test.setMock(WebServiceMock.class, new IVPWebServiceMockImpl());
		}
        // Read List View definition
        IVPMetadataService.ListView listView =
            (IVPMetadataService.ListView) service.readMetadata('ListView',
                new String[] { viewName }).getRecords()[0];
        
        if(listView.columns != NULL){
	        IVPMetadataService.SharedTo sharedTo = new IVPMetadataService.SharedTo();
	        sharedTo.group_x = new String[]{groupName};
	        listView.sharedTo = sharedTo;
	        
	        List<IVPMetadataService.UpsertResult> results =
	            service.upsertMetadata(
	                new IVPMetadataService.Metadata[] { listView });
	                
			handleUpsertResults(results[0]);
        }
    }
}