public with sharing class IVPBusinessHoursWrapper {

    private static IVPBusinessHoursWrapper bhwInstance;

    private BusinessHours bhRecord;
    private String bhName;
    private String localTimezone;
    public Id bhId {public get; private set;}

    private IVPBusinessHoursWrapper() {

        this.bhName = getBusinessHoursName();
        this.bhRecord = findBusinessHours();
        this.bhId = bhRecord.Id;
        this.localTimezone = UserInfo.getTimeZone().getID();
    }

    /*******************************************************************************
    * @description  The Business Hours record will have the same name as Country on
    *				User record. Users with incorrect setup will have default BH.
    ********************************************************************************/
    private String getBusinessHoursName(){
        return [SELECT Country FROM User WHERE Id = :UserInfo.getUserId()].get(0).Country;

    }

    /*******************************************************************************
    * @description  The Business Hours record will have the same name as Country on
    *				User record. Users with incorrect setup will have default BH.
    ********************************************************************************/
    private BusinessHours findBusinessHours(){
        List<BusinessHours> bhs = [SELECT Id, Name, IsDefault FROM BusinessHours WHERE IsActive = true AND (Name = :this.bhName OR IsDefault = true)];
        if (bhs.size() == 1){
            return bhs.get(0);                  // default one
        }else{
            for(BusinessHours bh : bhs){
                if(!bh.IsDefault) return bh;    // country specific
            }
        }
        return null;
    }


    /*******************************************************************************
    * @description  Calculates x working days after a set date, taking into account
    *				Non-working days and public holidays for this Business Hours.
    ********************************************************************************/
    public DateTime calculateWorkingDay(Datetime startDate, Integer delay){
        Datetime result = startDate;
        Boolean isReverse = delay < 0;
        if(bhRecord != NULL){
            delay = Math.abs(delay);
            for(Integer i = 0; i < delay; i++){
                if(isReverse){
                    Datetime nextStartDate = BusinessHours.nextStartDate(bhRecord.Id, result);
                    Integer step = 1;
                    result = BusinessHours.nextStartDate(bhRecord.Id, result.addDays(-step));
                    while(result.date() == nextStartDate.date()){
                        step++;
                        result = BusinessHours.nextStartDate(bhRecord.Id, result.addDays(-step));
                    }
                } else {
                    result = BusinessHours.nextStartDate(bhRecord.Id, result.addDays(1));
                }
            }
        }else{
            result = result.addDays(delay);
        }
        
        return DateTime.newInstance(result.year(), result.month(), result.day(), startDate.hour(), startDate.minute(), startDate.second());
    }
    
    /*
    private DateTime convertToGMT(DateTime localDate){
        String strCurrentTime = localDate.format('YYYY-MM-dd HH:mm:ss', localTimezone);
        Datetime dateCurrentTime = Datetime.valueofgmt(strCurrentTime);
        return dateCurrentTime;
    }

    private DateTime convertToLocal(DateTime dateGMT){
        String resultString = dateGMT.format('YYYY-MM-dd HH:mm:ss', localTimezone);
        Datetime resultDate = Datetime.valueof(resultString);
        return resultDate;
    }*/

    /*******************************************************************************
    * @description  Since this is only a wrapper class around Business Hours, makes
    *				sense to have it as a singleton.
    ********************************************************************************/
    public static IVPBusinessHoursWrapper getInstance(){
        if(bhwInstance == null){
            bhwInstance = new IVPBusinessHoursWrapper();
        }
        return bhwInstance;
    }
}