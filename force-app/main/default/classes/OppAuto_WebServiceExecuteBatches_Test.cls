/**
*@name      	: OppAuto_WebServiceExecuteBatches_Test
*@developer 	: 10K developer
*@date      	: 2018-10-23
*@description   : The class used as test class of OppAuto_WebServiceExecuteBatches
**/
@isTest
public class OppAuto_WebServiceExecuteBatches_Test {
    @isTest
    public static void testExecuteProfitProcessData(){
        Campaign camp = new Campaign(Name = 'Test Campaign', IsActive = TRUE, Type='Portfolio Company Demand Gen', StartDate=System.today());
        insert camp;
        
        Test.startTest();
        OppAuto_WebServiceExecuteBatches.ExecuteProfitProcessData(camp.Id);
        Test.stopTest();
        
        System.assertEquals('Completed',[SELECT Processing_Status__c FROM Campaign LIMIT 1].Processing_Status__c);
    }
}