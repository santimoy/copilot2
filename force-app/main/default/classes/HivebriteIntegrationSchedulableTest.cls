/**
 * @File Name          : HivebriteIntegrationSchedulableTest.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 8:59:48 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/11/2019   Anup Kage     Initial Version
**/
@isTest(SeeAllData = false)
private with sharing class HivebriteIntegrationSchedulableTest {
    @TestSetup
    static void makeData(){
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = true;
        accToken.Event_Log_Level__c = 'DEBUG';  
        UPSERT accToken;
    }
    @IsTest
    static void getHivebriteUsersIntoSFContacts(){
        
        
		list<Contact> cList = new list<Contact>();
		cList.add(new Contact(FirstName='Troy1', LastName='Milton Wing', Email='thomas.lesnick@insightpartners.com'));
		insert cList;
        Test.startTest();
       
        HivebriteIntegrationSchedulable obj = new HivebriteIntegrationSchedulable('CONTACT_HB_TO_SF');
        obj.execute(null);

        HivebriteIntegrationSchedulable obj2 = new HivebriteIntegrationSchedulable('CONTACT_SF_TO_HB');
        obj2.execute(null);
        
        HivebriteIntegrationSchedulable obj4 = new HivebriteIntegrationSchedulable('CONTACT_SF_TO_HB', cList);
        obj4.execute(null);
        
       
        Test.stopTest();
    }
    @IsTest
    static void getHivebriteCompanySFContacts(){
        Account acct = new Account(Name='testacct');
		acct.Website__c='http://saasbydesign.com';
		acct.OwnerId=UserInfo.getUserId();
		insert acct;

        Test.startTest();
        
        List<Sobject> recordList = new List<Sobject>();
        recordList.add(acct);
       
        HivebriteIntegrationSchedulable obj1 = new HivebriteIntegrationSchedulable('PORTFOLIO_SF_TO_HB', recordList);
        obj1.execute(null);

        HivebriteIntegrationSchedulable obj12 = new HivebriteIntegrationSchedulable('PARTNER_SF_TO_HB', recordList);
        obj12.execute(null);

        Test.stopTest();
    }
    @IsTest
    static void callExperienceRecords(){
        
       Test.startTest();
        List<Contact> recordList = new List<Contact>();  
        HivebriteIntegrationTestFactory.setupMockResponse();
        Account Compnay = HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact objContact = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Compnay.Id);
        recordList.add(objContact);

        HivebriteIntegrationSchedulable obj1 = new HivebriteIntegrationSchedulable('EXPERIENCE_HB_TO_SF');
        obj1.execute(null);
        
        HivebriteIntegrationSchedulable obj2 = new HivebriteIntegrationSchedulable('EXPERIENCE_SF_TO_HB', recordList);
        obj2.execute(null);
        Test.stopTest();
        
    }
    @IsTest
    static void callUserRecords(){
        
        
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = false;
        UPSERT accToken;
        Account Compnay = HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact objContact = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Compnay.Id);
        List<Contact>recordList = new List<Contact>();
        recordList.add(objContact);
        Test.startTest();
        
        HivebriteIntegrationSchedulable obj1 = new HivebriteIntegrationSchedulable('EXPERIENCE_HB_TO_SF');
        obj1.execute(null);
        
        HivebriteIntegrationSchedulable obj2 = new HivebriteIntegrationSchedulable('EXPERIENCE_SF_TO_HB', recordList);
        obj2.execute(null);

        Test.stopTest();
        
    }
    // @IsTest
    // static void callCompanyRecords(){
        
       
        
    // }
    @IsTest
    static void callContactsRecords(){
        
        Test.startTest();
        List<Contact> recordList = new List<Contact>();  
        HivebriteIntegrationTestFactory.setupMockResponse();
        Account Compnay = HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact objContact = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Compnay.Id);
        recordList.add(objContact);

        HivebriteIntegrationSchedulable obj1 = new HivebriteIntegrationSchedulable('CONTACT_HB_TO_SF');
        obj1.execute(null);
        
        HivebriteIntegrationSchedulable obj2 = new HivebriteIntegrationSchedulable('CONTACT_SF_TO_HB', recordList);
        HivebriteIntegrationSchedulable obj3 = new HivebriteIntegrationSchedulable('DELETECONTACT_SF_TO_HB', recordList);
        obj2.execute(null);
        obj3.execute(null);
        Test.stopTest();
        
    }    
}