/*
Class Name   :  BatchToRetriveTextFromCallOutResponse
Description  :  To Fetch first 255 characters from Callout Response field of Key Contact Scope and store in Trimmed Callout response field
Author       :  Sukesh
Created On   :  October 14 2019
*/

global with sharing class BatchToRetriveTextFromCallOutResponse implements Database.Batchable<sObject> {

    String strQuery = 'SELECT Id, Callout_Response__c,Trimmed_Callout_response__c FROM Key_Contact_Scope__c'; //WHERE Id=\'a6Mm00000004r7Q\'

    global BatchToRetriveTextFromCallOutResponse() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Key_Contact_Scope__c> lstKeyContactScope) {

        List<Key_Contact_Scope__c> lstKeyContactScopeToUpdate = new List<Key_Contact_Scope__c>();

        for(Key_Contact_Scope__c objKeyContactScope : lstKeyContactScope) {

            if(!String.isEmpty(objKeyContactScope.Callout_Response__c)) {

                lstKeyContactScopeToUpdate.add(new Key_Contact_Scope__c(Id=objKeyContactScope.Id,
                                                                        Trimmed_Callout_response__c = (objKeyContactScope.Callout_Response__c.left(255))));
            }

        }

        update lstKeyContactScopeToUpdate;
    }

    global void finish(Database.BatchableContext BC) {

    }
}