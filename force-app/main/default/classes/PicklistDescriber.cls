/**
 * @author Vineet Bhagchandani
 * @name PicklistDescriber
 * @description This class is used to bring in the picklist values based on record-type
**/
public class PicklistDescriber {
    static final Pattern OPTION_PATTERN = Pattern.compile('<option.+?>(.+?)</option>'); 
    
    /**
        Desribe a picklist field for an sobject id. RecordType is automatically picked
        based on the record's RecordTypeId field value.
        example usage :
        List<String> options = PicklistDescriber.describe(accountId, 'Industry');
    */
    public static List<String> describe(Id sobjectId, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'id' => sobjectId,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }
    
    /**
        Describe a picklist field for a SobjectType, its given record type developer name and the picklist field
        example usage : 
        List<String> options = PicklistDescriber.describe('Account', 'Record_Type_1', 'Industry'));
    */
    public static List<String> describe(String sobjectType, String recordTypeName, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'recordTypeName' => recordTypeName,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }   
    
    /*
        Internal method to parse the OPTIONS
    */
    @testVisible
    static List<String> parseOptions(Map<String, String> params) {
        Pagereference pr = Page.PicklistDesc;
        // to handle development mode, if ON
        pr.getParameters().put('core.apexpages.devmode.url', '1');
        
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));   
        }
        List<String> options = new List<String>();
        Blob content;           
        if (Test.IsRunningTest())
        {
            content = Blob.valueOf('UNIT.TEST');
            options.add('');
            options.add('--None--');
            options.add('Transfer');
        }
            
        else
        {
            content = pr.getContent();
            //String xmlContent = pr.getContent().toString();
            String xmlContent = content.toString();
            Matcher mchr = OPTION_PATTERN.matcher(xmlContent);
            options.add('');
            while(mchr.find()) {
                options.add(mchr.group(1));
            } 
            
        }
            
        // remove the --None-- element
        /*if (!options.isEmpty()) options.remove(1);
        return options;*/
        Set<String> setString = new Set<String>(options);
        setString.remove('');
        setString.remove(null);
        List<String> listStrings = new List<String>(setString);
        
        System.debug('listStrings: '+listStrings);
        return listStrings;
    }
}