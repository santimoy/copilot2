public class JustForYouCompaniesController {
    @AuraEnabled
    public static JustForYouResponse getColumnList(){
        JustForYouResponse returnObj = new JustForYouResponse();
        List<Sourcing_Preference__c> sourcingPreferenceList = getSourcing();       
        Just_For_You_Setting__c justForYouSetting = getjustForYouSetting();
        Set<String> labelSet = new Set<String>(); Set<String> apiSet = new Set<String>(); Set<String> hideSet = new Set<String>();
        Set<String> excludeLabelSet = new Set<String>();Set<String> excludeApiSet = new Set<String>();
        PreferencePreviewController.fieldClass fieldList = new PreferencePreviewController.fieldClass();
        
        returnObj.wrapColName = getWrapColName();
        if(returnObj.wrapColName.size()>0){
            returnObj.isColnWrap = true;
        }else{
            returnObj.isColnWrap = false;
        }
        returnObj.colMinWidth = getMinWidthMap();
        Intelligence_Settings__c userSetting = Intelligence_Settings__c.getInstance(UserInfo.getUserId());
        returnObj.colnWrapMaxLength = userSetting.Wrap_Large_Columns_Max_Length__c;
        fieldList = PreferencePreviewController.getDisplayColumn(null,false);
        if(fieldList.fApiList!=null){
            apiSet.addAll(fieldList.fApiList); 
        }
        if(fieldList.fLabelList!=null){
            labelSet.addAll(fieldList.fLabelList);
        }
        
        system.debug('labelSet=='+labelSet);
        
        List<Intelligence_Field__c> requriredFieldList = [SELECT Id, Name, DB_Table__c, DB_Field_Name__c,Default_display_field__c ,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c,
                                                             Max_Length_Column__c, Sort_Descending_First__c, Insert_Only__c, Target_Field__c, Target_Field_Length__c, Target_Field_Type__c, Target_Object__c,
                                                             Is_All_Preferences_Static_Field__c,Is_Preference_Filter_Static_Field__c,QBFlt_Filter_Grouping__c
                                                          FROM Intelligence_Field__c  ORDER BY Order__c];
        List<IIField> iifields = new List<IIField>();
        Map<String, String> feedbackItems = new Map<String, String>();
        for(Intelligence_Field__c fields : requriredFieldList){
            IIField iifield = new IIField();
            iifield.dbField = fields.DB_Column__c;
            iifield.title = fields.UI_Label__c;
            iifield.visible = fields.Active__c;
            
            Map<String,object> lookup = new Map<String,object>();
            lookup.put('insert_only__c', fields.Insert_Only__c);
            lookup.put('max_length_column__c', fields.Max_Length_Column__c);
            lookup.put('sort_descending_first__c', fields.Sort_Descending_First__c);
            lookup.put('target_field__c', fields.Target_Field__c);
            lookup.put('target_field_length__c', fields.Target_Field_Length__c);
            lookup.put('target_field_type__c', fields.Target_Field_Type__c);
            lookup.put('target_object__c', fields.Target_Object__c);
            iifield.lookup = lookup;
            iifields.add(iifield);
            
            if(fields.Is_All_Preferences_Static_Field__c == true){
                apiSet.add(fields.DB_Field_Name__c);
                labelSet.add(fields.UI_Label__c);
                if(!fields.Is_Preference_Filter_Static_Field__c){
                    excludeApiSet.add(fields.DB_Field_Name__c);
                    excludeLabelSet.add(fields.UI_Label__c);
                }
            }
            if(fields.Active__c && 'Feedback'==fields.QBFlt_Filter_Grouping__c){
                feedbackItems.put(fields.DB_Field_Name__c,fields.UI_Label__c);
            }
        }
        returnObj.feedbackItems = feedbackItems;
        returnObj.datatable_columns = iifields;
        returnObj.excludeApiList = excludeApiSet;
        returnObj.excludeLabelList = excludeLabelSet;
        if(!apiSet.contains('ivp_name')){
            apiSet.add('ivp_name');labelSet.add('Name');
        }
        if(apiSet.contains('ivp_name') && !apiSet.contains('website')){
            apiSet.add('Website');hideSet.add('Website');labelSet.add('Website');
        }
        if(apiSet.contains('ivp_name')){
            apiSet.add('sf_id');hideSet.add('sf_id');labelSet.add('sf_id');
            apiSet.add('ct_id');hideSet.add('ct_id');labelSet.add('ct_id');
        }
        if(!apiSet.contains('salesforce_owner_name')){
            //apiSet.add('ownership_status');hideSet.add('Ownership Status');labelSet.add('Ownership Status');
            apiSet.add('salesforce_owner_name');hideSet.add('Owner');labelSet.add('Owner');
        }
        if(!apiSet.contains('ownership_status')){
            apiSet.add('ownership_status');hideSet.add('Ownership Status');labelSet.add('Ownership Status');
        }
        if(apiSet.contains('ivp_employee_count') && !apiSet.contains('ivp_linkedin')){
            apiSet.add('ivp_linkedin');
            hideSet.add('LinkedIn URL');
            labelSet.add('LinkedIn URL');
        }
        fieldList.fApiList = apiSet;
        fieldList.fLabelList = labelSet;
        
        
        returnObj.apiHostName = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c;
        returnObj.loginUserId = UserInfo.getUserId();
        returnObj.loginUserName = UserInfo.getName();
        returnObj.loginOrgId = UserInfo.getOrganizationId();
        system.debug('fieldList==123='+fieldList);
        returnObj.fieldList = fieldList;
        returnObj.hidefields = hideSet;
        returnObj.isActivePreference = false;
        if(justForYouSetting != null){
            returnObj.daysRequested = String.valueOf(justForYouSetting.JFY_Companies_Last_N_Days__c);
        }
        IVP_Call_Params__c ivpCustomSetting = getIvpSetting();
        if(ivpCustomSetting.Just_For_You_OrderBy__c != null){
            returnObj.orderBy = String.valueOf(ivpCustomSetting.Just_For_You_OrderBy__c);
        }
        if(sourcingPreferenceList.size() > 0){
            returnObj.isActivePreference = true;
        }
        JustForYouResponse jfyDataRepsone = getSourcingPreferenceWithCount(false,sourcingPreferenceList,justForYouSetting);
        returnObj.jfyData = jfyDataRepsone.jfyData;
        returnObj.icons = [SELECT Id, DeveloperName, MasterLabel, Label, 
                           Value__c, Indicator__c, Field_Name__c, Icon__c,JFY_Icon__c, 
                           Apply_Opacity__c, Toggle_Icon__c, Opacity_Field_Name__c, Hover_Text__c,
                           Toggle_Value__c, Order__c
                           FROM Intelligence_Feedback_Icons__mdt];
        returnObj.dwhSetting = DWH_Settings__c.getInstance(UserInfo.getUserId());
        returnObj.org = [SELECT Id FROM Organization LIMIT 1];
        return returnObj;
    }
    @AuraEnabled
    public static String callCompanyInfo(String externalId,String cname,String website){
        Logger.push('callCompanyInfo','JustForYouCompaniesController');
        List<Account> accList = new List<Account>();
        accList = [Select Id,Name From Account WHere Name=:cname and Website=:website];
        if(accList.size()>0){
            return 'Exist';
        }else{
            String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c+'/company_searches/fetch?datatables=1';
            String bodyString = 'json_filters={"record_ids":"'+externalId+'"}';
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            // need to keep max timeout is 30 sec
            req.setTimeout(30000);
            req.setBody(bodyString);
            Http http = new Http();
            HTTPResponse res;
            String result = '';
            Map<String, Object> resultMap = makeCallout(req);
            result = String.valueOf(resultMap.get('result'));
            result = result.replace('/','');
            return result;
        }
    }
    @AuraEnabled
    public static accountWrapper createCompany(List<companyClass> companyDataList,String compName,String website){
        accountWrapper accWrap = new accountWrapper();
        string tempInput = '%' + website + '%';
        List<Account> accList = new List<Account>();
        accList =  [SELECT Id,Name,Website,Ownership_Status__c,Owner.NAME FROM Account  where Name=:compName and  Website like :tempInput  and   Created_by_Insight_Intelligence__c  = true];
        
        if(accList.size()>0){
            accWrap.message = 'Exist';
            accWrap.acc = accList[0];
        }else{
            Map<String,String> accFieldsMap = new Map<String,String>();
            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap.Values())
            {
                schema.describefieldresult dfield = sfield.getDescribe();
                accFieldsMap.put(dfield.getLabel(),String.valueOf(dfield.getType()));
            }
            Account acc = new Account();
            for(companyClass cc : companyDataList){
                if(accFieldsMap.containskey(cc.fieldName)){
                    acc.put(cc.fieldName,cc.data);
                }
            }
            acc.Created_by_Insight_Intelligence__c= true;
            insert acc;
            List<Account> newAccList = new List<Account>();
            newAccList = [SELECT Id,Name,Website,Ownership_Status__c,Owner.NAME FROM Account Where Id=:acc.Id];
            if(newAccList.size()>0){
                accWrap.message = 'Created';
                accWrap.acc = newAccList[0];
            }
        }
        System.debug('accWrap=='+accWrap);
        return accWrap;
    }
    
    @AuraEnabled
    public static userResponse processAssgnUser(String companyId){
        userResponse ur = new userResponse();
        List<User> unassignedUsers = [Select Id, Name From User Where Name='Unassigned' Limit 1];
        if(!unassignedUsers.isEmpty() ){
            //companyId='0010q00000OLqExAAL';
            List<Account> accList = [Select Id, Name, Ownership_Status__c, Owner.Name From Account Where Id=:companyId Limit 1];
            if(!accList.isEmpty()){
                ur.acc = accList[0];
                if(accList[0].OwnerId == unassignedUsers[0].Id){
                    try{
                        accList[0].OwnerId = UserInfo.getUserId();
                        update accList;
                        accList = [Select Id, Name, Ownership_Status__c, Owner.Name From Account Where Id=:companyId Limit 1];
                        ur.acc = accList[0];
                        ur.types = 'Success';
                        ur.message = 'Company Assigned to you.';
                    }catch(exception e){
                        ur.types = 'Info';
                        ur.message = 'Quick Assigned company was unsuccessful.' + e.getMessage();
                    }
                    return ur;
                }else{
                    ur.types = 'Info';
                    ur.message = 'Company Already Assigned to '+accList[0].Owner.Name;
                    return ur;
                }
            }else{
                ur.types = 'Info';
                ur.message = 'The Company you have selected does not exist in Salseforce. Please check with your Administrator';
                return ur;
            }
        }else{
            ur.types = 'Error';
            ur.message = 'failed to Update/Assigned  Company';
            return ur;
        }
    }
    public static List<Sourcing_Preference__c> getSourcing(){
        String query='SELECT Id, Validation_Status__c, Status__c, Name__c, Owner_Username__c,External_Id__c FROM Sourcing_Preference__c '
            +'WHERE Status__c=\'active\' AND Validation_Status__c=\'valid\' AND Include_Exclude__c=\'Include\' '; 
        query+= !Test.isRunningTest() ? ' AND OwnerId =\''+UserInfo.getUserId()+'\'' : '';
        query+= ' LIMIT 10000';
        return Database.query(query);
    }
    
    public static Map<String,Integer> getMinWidthMap(){
        Map<String,Integer> fieldColMinWidth = new Map<String,Integer>();
        List<Intelligence_Field__c> intListMinWidth = [SELECT Id, Name, DB_Field_Name__c,UI_Label__c, DB_Column__c, Minimum_Column_Width__c
                                                       FROM Intelligence_Field__c];
        system.debug('intListMinWidth=='+intListMinWidth);
        for(Intelligence_Field__c intField : intListMinWidth){
            if(intField.UI_Label__c != null){
                if(intField.Minimum_Column_Width__c != null){
                    fieldColMinWidth.put(intField.UI_Label__c.trim(), integer.valueOf(intField.Minimum_Column_Width__c));
                }
                /*
                else{
                    fieldColMinWidth.put(intField.UI_Label__c.trim(), 250);
                }
                */
            }
            
        }
        system.debug('fieldColMinWidth=='+fieldColMinWidth);
        return fieldColMinWidth;
    }

    
    public static List<String> getWrapColName(){
        List<String> colNameList = new List<String>();
        List<String> finalColnList = new List<String>();
        Intelligence_Settings__c userSetting = Intelligence_Settings__c.getInstance(UserInfo.getUserId());
        List<Intelligence_User_Preferences__c> intUserPref = [SELECT Id, Wrap_Large_Columns__c, User_Id__c from Intelligence_User_Preferences__c where User_Id__c =: UserInfo.getUserId() limit 1];
        if(intUserPref != null && intUserPref.size() > 0){
            if(intUserPref[0].Wrap_Large_Columns__c){
                if(userSetting != null && String.isNotblank(userSetting.Wrap_Large_Columns_Name__c)){
                    colNameList = userSetting.Wrap_Large_Columns_Name__c.split(',');
                    for(String colName : colNameList){
                        String trimStr = colName.trim();
                        finalColnList.add(trimStr);
                    }
                    colNameList = finalColnList;
                }
            }
        }
        if(colNameList.size()>0){
            List<Intelligence_Field__c > intFieldList = [SELECT Id, Name, DB_Column__c, DB_Field_Name__c, UI_Label__c from Intelligence_Field__c where DB_Column__c IN :colNameList OR DB_Field_Name__c IN :colNameList];
            colNameList = new List<String>();
            for(Intelligence_Field__c intField : intFieldList){
                colNameList.add(intField.UI_Label__c);
            }
        }
        return colNameList;
    }
    
    @AuraEnabled
    public static JustForYouResponse getSourcingPreferenceWithCount(Boolean fetchData, List<Sourcing_Preference__c> sourcingPreferenceList, Just_For_You_Setting__c justForYouSetting){
        Logger.push('getSourcingPreferenceWithCount','JustForYouCompaniesController');
        JustForYouResponse returnObj = new JustForYouResponse();
        returnObj.apiHostName = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c;
        List<sourcingCompanyCount> responseList = new List<sourcingCompanyCount>();
        sourcingCompanyCount spError = new sourcingCompanyCount();
        preferenceBody pb = new preferenceBody();
        if(fetchData){
            sourcingPreferenceList = getSourcing();
            justForYouSetting = getjustForYouSetting();
        }
        System.debug('sourcingPreferenceList'+sourcingPreferenceList);
        System.debug('sourcingPreferenceList'+justForYouSetting);
        if(justForYouSetting != null){
            pb.days_requested = String.valueOf(justForYouSetting.JFY_Companies_Last_N_Days__c);
            pb.limits = '10';
        }
        
        String body = JSON.serialize(pb);
        System.debug('body-->'+body);
        body = body.replace('limits','limit');
        HttpRequest req = new HttpRequest();
        String endPoint = returnObj.apiHostName+'/preference_user/'+UserInfo.getUserId()+'/ui_setup';
        
        req.setEndpoint(endPoint);
        // need to keep max timeout is 30 sec
        req.setTimeout(30000);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        req.setBody(body);
        Http http = new Http();
        HTTPResponse res;
        String result = '';
        if(!test.isRunningTest()){
            Map<String, Object> resultMap = makeCallout(req);
            Boolean isError = (Boolean)resultMap.get('isError');
            result = String.valueOf(resultMap.get('result'));
            System.debug(result);
            System.debug(isError);
            if(isError){
                spError.errorInfo = result;
            }
        }else{
            result = '{"total_pages":null,"total_entries":null,"sql_statement":null,"response_time":null,"jsonData":null,"host":null,"fieldsLabel":null,"fieldsApi":null,"error":null,"dialect":null,"data_fields":null,"data":[["0050W000006DpJBQA0","2222222222222",874525,30]],"additional_data":null}';
        }
        
        if(String.isBlank(spError.errorInfo)){
            Map<String,String> apiDataMap = new Map<String,String>();
            PreferencePreviewController.HerokuData dataWrapper = PreferencePreviewController.parse(result);
            List<List<String>> apiDataList = dataWrapper.data;
            System.debug('------'+dataWrapper.error);
            if(String.isNotBlank(dataWrapper.error)){
                spError.errorInfo = dataWrapper.error;
                responseList.add(spError);
                returnObj.jfyData = responseList;
                return returnObj;
            }
            if(apiDataList.size()>0){
                for(List<String> lst1 :  apiDataList){
                    apiDataMap.put(lst1[1],lst1[2]);
                } 
            }
            for(Sourcing_Preference__c sp : sourcingPreferenceList){
                sourcingCompanyCount sc = new sourcingCompanyCount();
                sc.preferenceId = sp.External_Id__c;
                sc.preferenceName = sp.Name__c;
                if(apiDataMap.containskey(sp.External_Id__c)){
                    sc.companyCount = '('+apiDataMap.get(sp.External_Id__c)+')';
                }
                responseList.add(sc);
            } 
            system.debug(responseList);
        }else{
            responseList.add(spError);
        }
        returnObj.jfyData = responseList;
        return returnObj;
    }
    /*@future
    public static void logError(string data){
        Logger.push('getSourcingPreferenceWithCount','JustForYouCompaniesController');
        Logger.debugException(data);
        Logger.pop();
    }*/
    @AuraEnabled
    public static String logAjax(string dbody,String methodName,String endPoint,String status){
        Logger.push('logAjax','JustForYouCompaniesController');
        Map<String, Object> data = new Map<String, object>{
            'body'=>dbody,
            'method'=>methodName,
            'endpoint'=>endPoint,
                'status'=>status
        };
        return logDetails(data);
        /*Datetime start = system.now();
        Map<String, Object> data = new Map<String, object>{
            'body'=>dbody,
                'method'=>methodName,
                'endpoint'=>endPoint,
                 'status'=>status
                };
                    Logger.debugException(JSON.serialize(data));
        Logger.pop();
        Datetime ends = system.now();
        List<Log__c> logs = [SELECT Id,Name FROM Log__c 
                             WHERE CreatedById=:UserInfo.getUserId()
                             AND CreatedDate>=:start AND CreatedDate<=:ends];
        
        String result = Label.DWH_API_Time_Out ;
        if(!logs.isEmpty()){
            System.debug(logs[0].Name);
            result ='refId: '+String.escapeSingleQuotes(logs[0].Name)+' ' +result;
        }
        return result;*/
    }
    public static Just_For_You_Setting__c getjustForYouSetting(){
        Just_For_You_Setting__c justForYouSetting = new Just_For_You_Setting__c();
        List<Just_For_You_Setting__c> jfys = new List<Just_For_You_Setting__c>();
        
        jfys = [SELECT JFY_Companies_Last_N_Days__c FROM Just_For_You_Setting__c LIMIT 1];
        if(!jfys.isEmpty()){
            justForYouSetting = jfys[0];
        }
        return justForYouSetting;
    }
    public static IVP_Call_Params__c getIvpSetting(){
        IVP_Call_Params__c ivpCustomSetting = new IVP_Call_Params__c();
        List<IVP_Call_Params__c> callParams = new List<IVP_Call_Params__c>();
        
        callParams = [SELECT Name, Just_For_You_OrderBy__c FROM IVP_Call_Params__c WHERE Name = 'justForyou' LIMIT 1];
        if(!callParams.isEmpty()){
            ivpCustomSetting = callParams[0];
        }
      return ivpCustomSetting;
    }
    static Map<String, Object> makeCallout(HttpRequest req){
        String result='';
        Map<String, Object> resultMap = new Map<String, Object>();
        resultMap.put('isError', false);
        System.debug('req-body'+ req.getBody());
        System.debug('req-body'+ req.getEndPoint());
        try{
            Http http = new Http();
            HTTPResponse res = http.send(req);
            result = res.getBody();
            if(res.getStatusCode() !=200){
                resultMap.put('isError', true);
                result = String.escapeSingleQuotes(res.getStatus());
            }
            system.debug('result '+ result);
            resultMap.put('result', result);
        }catch(Exception e){
            Map<String, Object> data = new Map<String, object>{
                'body'=>req.getBody(),
                'method'=>req.getMethod(),
                'endpoint'=>req.getEndpoint()
            };
            result = logDetails(data);
            resultMap.put('isError', true);
            resultMap.put('result', result);
        }
        return resultMap;
    }
 
    static String logDetails(Map<String, object> data){
 
        Datetime start = system.now();
        Logger.debugException(JSON.serialize(data));
        Logger.pop();
        Datetime ends = system.now();
        List<Log__c> logs = [SELECT Id,Name FROM Log__c 
                             WHERE CreatedById=:UserInfo.getUserId()
                             AND CreatedDate>=:start AND CreatedDate<=:ends];
        
        String result = Label.DWH_API_Time_Out ;
        if(!logs.isEmpty()){
            System.debug(logs[0].Name);
            result ='refId: '+String.escapeSingleQuotes(logs[0].Name)+', ' +result;
        }
        return result;
    }
    @AuraEnabled
    public static List<Sourcing_Preference__c> getSourcePreference(){
        return [SELECT Id, Validation_Status__c, Status__c, Name__c, Owner_Username__c,External_Id__c 
                FROM Sourcing_Preference__c 
                Where Status__c='active' and Validation_Status__c='valid' and OwnerId =:Userinfo.getUserId()]; 
    }
    public class accountWrapper{
        @AuraEnabled public account acc {get;set;}
        @AuraEnabled public string message {get;set;}
    }
    public class IIField{
        @AuraEnabled public String dbField; //: "ct.companies_cross_data_full.ivp_name"
        @AuraEnabled public String title; //: "Name"
        @AuraEnabled public Boolean visible; //: true
        @AuraEnabled public Map<String, object> lookup;
        public IIField(){
            lookup =new Map<String, object>();
        }
    }
    public class preferenceBody{
        public String days_requested;
        public String limits;
    }
    public class JustForYouResponse{
        @AuraEnabled public String loginUserId {get;set;}
        @AuraEnabled public String loginOrgId {get;set;}
        @AuraEnabled public String loginUserName {get;set;}
        @AuraEnabled public String apiHostName {get;set;}
        @AuraEnabled public String orderBy {get;set;}
        @AuraEnabled public PreferencePreviewController.fieldClass fieldList {get;set;}
        @AuraEnabled public Set<String> hidefields {get;set;} 
        @AuraEnabled public String daysRequested {get;set;}
        @AuraEnabled public Boolean isActivePreference {get;set;}
        @AuraEnabled public Set<String> excludeLabelList {get;set;}
        @AuraEnabled public Set<String> excludeApiList {get;set;}
        @AuraEnabled public List<sourcingCompanyCount> jfyData {get;set;}
        @AuraEnabled public List<String> wrapColName {get;set;}
        @AuraEnabled public Map<String,Integer> colMinWidth {get;set;}
        @AuraEnabled public Boolean isColnWrap {get;set;}
        @AuraEnabled public Decimal colnWrapMaxLength {get;set;}
        @AuraEnabled public Map<String, String> feedbackItems {get;set;}
        @AuraEnabled public object datatable_columns {get;set;}
        @AuraEnabled public List<Object>icons{get;set;}
        @AuraEnabled public Object dwhSetting{get;set;}
        @AuraEnabled public Organization org{get;set;}
    }
    public class sourcingCompanyCount{
        @AuraEnabled public String preferenceId {get;set;}
        @AuraEnabled public String preferenceName {get;set;}
        @AuraEnabled public String companyCount {get;set;}
        @AuraEnabled public String errorInfo {get;set;}
    }
    
    public class userResponse{
        @AuraEnabled public string types {get;set;}
        @AuraEnabled public string message {get;set;}
        @AuraEnabled public Account acc {get;set;}
        
    }
    public class companyClass{
        @AuraEnabled public string title {get;set;}
        @AuraEnabled public string data {get;set;}
        @AuraEnabled public string fieldName {get;set;}
    }
}