@isTest
public class InvoiceLineItemAdditionController_Test {

    static testMethod void Test_ILIAdditionController(){

        /*
        //TimesheetDetail List Insertion
        List<Krow__Timesheet_Detail__c> TimeSheetDetailList = new List<Krow__Timesheet_Detail__c>();
        Krow__Timesheet_Detail__c TimeSheetDetail = new Krow__Timesheet_Detail__c ();
        TimeSheetDetail.Krow__Billable__c = true;
        TimeSheetDetail.Krow__Timesheet__c = TimesheetList[0].Id;
        TimeSheetDetail.Krow__Project__c = ProjectList[0].Id;
        TimeSheetDetail.Krow__Bill_Rate_New__c = 150;
        TimeSheetDetail.Krow__Cost_Rate_New__c = 150;
        TimeSheetDetailList.add(TimeSheetDetail);
        insert TimeSheetDetailList;
        */

        List<Account> accList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Dev_Test_Acc';
        accList.add(acc);
        insert accList;

        List<Krow__Project__c> ProjectList = new List<Krow__Project__c>();
        Krow__Project__c KpProject = new Krow__Project__c();
        KpProject.Name = 'TestTemplateKpProject';
        KpProject.Krow__Billable__c = true;
        KpProject.Krow__Project_Template__c = false;
        KpProject.Krow__Public_Project_Template__c = false;
        KpProject.Krow__Completion_Method__c = 'Task Completion';
        KpProject.Krow__Project_Status__c = 'Not Started';
        KpProject.Krow__Billing_Type__c = null;
        ProjectList.add(KpProject);
        insert ProjectList;

        ProjectList[0].Krow__Billing_Type__c = null;
        update ProjectList;

        List<Krow__Timesheet__c> TimesheetList = new List<Krow__Timesheet__c>();
        Krow__Timesheet__c TimeSheet = new Krow__Timesheet__c();
        TimeSheet.Krow__Approval_Status__c = '';
        TimeSheet.Krow__Project__c = ProjectList[0].Id;
        TimeSheet.Krow__Week_Start_Date__c = Date.today();
        TimeSheet.Krow__Week_End_Date__c = Date.today().addDays(7);
        TimesheetList.add(TimeSheet);
        insert TimesheetList;

        List<Krow__Timesheet_Split__c> TimesheetSplitList = new List<Krow__Timesheet_Split__c>();
        Krow__Timesheet_Split__c TimeSheetSplit = new Krow__Timesheet_Split__c ();
        TimeSheetSplit.Krow__Approval_Status__c = '';
        TimeSheetSplit.Krow__Billable__c = true;
        TimeSheetSplit.Krow__Date__c = Date.today().addDays(2);
        TimeSheetSplit.Krow__Timesheet__c = TimesheetList[0].Id;
        //TimeSheetSplit.Krow__Timesheet_Detail__c = null;//TimeSheetDetailList[0].Id;
        TimeSheetSplit.Krow__Invoiced__c = false;
        TimeSheetSplit.Krow__Hours__c = 5;
        TimeSheetSplit.Krow__Exclude_From_Calculation__c = true;
        TimeSheetSplit.Krow__Krow_Project__c = ProjectList[0].Id;
        TimesheetSplitList.add(TimeSheetSplit);
        insert TimesheetSplitList;

        TimesheetSplitList[0].Krow__Exclude_From_Calculation__c = false;
        update TimesheetSplitList;

        List<Project_Company__c> ProjectCompanyList = new List<Project_Company__c>();
        Project_Company__c ProCompany = new Project_Company__c ();
        ProCompany.Account__c = accList[0].Id;
        ProCompany.Krow_Project__c = ProjectList[0].Id;
        ProjectCompanyList.add(ProCompany);
        insert ProjectCompanyList;

        Krow__Invoice__c Invoice = new Krow__Invoice__c();
        Invoice.Krow__Invoice_Date__c = Date.today();
        Invoice.Krow__Invoice_Account__c = accList[0].Id;
        Invoice.Krow__Invoice_Approved_Time_Only__c = true;
        Invoice.Krow__Invoice_Approved_Expenses_Only__c = true;
        Invoice.Krow__Summarize__c = 'Totals Only';
        Invoice.Krow__Start_Date__c = Date.today().addDays(-20);
        Invoice.Krow__End_Date__c = Date.today().addDays(10);
        insert Invoice;

        //call method to add invoicelineitems
        AddInvoiceLineItem_Custom.createInvoiveLineItemsForInvoices(Invoice, '');

    }

    static testMethod void Test_ILIUpdate(){ //With Project Resource
        List<Account> accList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Dev_Test_Acc';
        accList.add(acc);
        insert accList;

        List<Krow__Project__c> ProjectList = new List<Krow__Project__c>();
        Krow__Project__c KpProject = new Krow__Project__c();
        KpProject.Name = 'TestTemplateKpProject';
        KpProject.Krow__Billable__c = true;
        KpProject.Krow__Project_Template__c = false;
        KpProject.Krow__Public_Project_Template__c = false;
        KpProject.Krow__Completion_Method__c = 'Task Completion';
        KpProject.Krow__Project_Status__c = 'Not Started';
        KpProject.Krow__Billing_Type__c = null;
        ProjectList.add(KpProject);
        insert ProjectList;

        ProjectList[0].Krow__Billing_Type__c = null;
        update ProjectList;

        List<Krow__Timesheet__c> TimesheetList = new List<Krow__Timesheet__c>();
        Krow__Timesheet__c TimeSheet = new Krow__Timesheet__c();
        TimeSheet.Krow__Approval_Status__c = '';
        TimeSheet.Krow__Project__c = ProjectList[0].Id;
        TimeSheet.Krow__Week_Start_Date__c = Date.today();
        TimeSheet.Krow__Week_End_Date__c = Date.today().addDays(7);
        TimesheetList.add(TimeSheet);
        insert TimesheetList;

        List<Krow__Project_Resources__c> ProjectResourceList = new List<Krow__Project_Resources__c>();
        Krow__Project_Resources__c ProRes = new Krow__Project_Resources__c ();
        ProRes.Name = UserInfo.getName();
        ProRes.Krow__User__c = UserInfo.getUserId();
        ProRes.Krow__Cost_Rate__c = 120;
        ProRes.Krow__Active__c = true;
        ProjectResourceList.add(ProRes);
        insert ProjectResourceList;

        List<Krow__Timesheet_Split__c> TimesheetSplitList = new List<Krow__Timesheet_Split__c>();
        Krow__Timesheet_Split__c TimeSheetSplit = new Krow__Timesheet_Split__c ();
        TimeSheetSplit.Krow__Approval_Status__c = '';
        TimeSheetSplit.Krow__Billable__c = true;
        TimeSheetSplit.Krow__Date__c = Date.today().addDays(2);
        TimeSheetSplit.Krow__Timesheet__c = TimesheetList[0].Id;
        //TimeSheetSplit.Krow__Timesheet_Detail__c = null;//TimeSheetDetailList[0].Id;
        TimeSheetSplit.Krow__Invoiced__c = false;
        TimeSheetSplit.Krow__Hours__c = 5;
        TimeSheetSplit.Krow__Exclude_From_Calculation__c = true;
        TimeSheetSplit.Krow__Krow_Project__c = ProjectList[0].Id;
        TimeSheetSplit.Krow__Project_Resource__c = ProjectResourceList[0].Id;
        TimesheetSplitList.add(TimeSheetSplit);
        insert TimesheetSplitList;

        TimesheetSplitList[0].Krow__Exclude_From_Calculation__c = false;
        update TimesheetSplitList;

        List<Project_Company__c> ProjectCompanyList = new List<Project_Company__c>();
        Project_Company__c ProCompany = new Project_Company__c ();
        ProCompany.Account__c = accList[0].Id;
        ProCompany.Krow_Project__c = ProjectList[0].Id;
        ProjectCompanyList.add(ProCompany);
        insert ProjectCompanyList;

        Krow__Invoice__c Invoice = new Krow__Invoice__c();
        Invoice.Krow__Invoice_Date__c = Date.today();
        Invoice.Krow__Invoice_Account__c = accList[0].Id;
        Invoice.Krow__Invoice_Approved_Time_Only__c = true;
        Invoice.Krow__Invoice_Approved_Expenses_Only__c = true;
        Invoice.Krow__Summarize__c = 'Totals Only';
        Invoice.Krow__Start_Date__c = Date.today().addDays(-20);
        Invoice.Krow__End_Date__c = Date.today().addDays(10);
        insert Invoice;

        //call method to add invoicelineitems
        AddInvoiceLineItem_Custom.createInvoiveLineItemsForInvoices(Invoice, '');

        List<Krow__Invoice_Line_Item__c> ILIList = new List<Krow__Invoice_Line_Item__c>();
        ILIList = [Select Id, Krow__Hours__c, Krow__Invoice__c From Krow__Invoice_Line_Item__c Where Krow__Invoice__c =: Invoice.Id];
        if(ILIList.size() > 0){
            ILIList[0].Krow__Hours__c = ILIList[0].Krow__Hours__c + 1;
            update ILIList;
        }
    }

    static testMethod void Test_ILIUpdate_2(){ //Without Project Resource
        List<Account> accList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Dev_Test_Acc';
        accList.add(acc);
        insert accList;

        List<Krow__Project__c> ProjectList = new List<Krow__Project__c>();
        Krow__Project__c KpProject = new Krow__Project__c();
        KpProject.Name = 'TestTemplateKpProject';
        KpProject.Krow__Billable__c = true;
        KpProject.Krow__Project_Template__c = false;
        KpProject.Krow__Public_Project_Template__c = false;
        KpProject.Krow__Completion_Method__c = 'Task Completion';
        KpProject.Krow__Project_Status__c = 'Not Started';
        KpProject.Krow__Billing_Type__c = null;
        ProjectList.add(KpProject);
        insert ProjectList;

        ProjectList[0].Krow__Billing_Type__c = null;
        update ProjectList;

        List<Krow__Timesheet__c> TimesheetList = new List<Krow__Timesheet__c>();
        Krow__Timesheet__c TimeSheet = new Krow__Timesheet__c();
        TimeSheet.Krow__Approval_Status__c = '';
        TimeSheet.Krow__Project__c = ProjectList[0].Id;
        TimeSheet.Krow__Week_Start_Date__c = Date.today();
        TimeSheet.Krow__Week_End_Date__c = Date.today().addDays(7);
        TimesheetList.add(TimeSheet);
        insert TimesheetList;

        List<Krow__Timesheet_Split__c> TimesheetSplitList = new List<Krow__Timesheet_Split__c>();
        Krow__Timesheet_Split__c TimeSheetSplit = new Krow__Timesheet_Split__c ();
        TimeSheetSplit.Krow__Approval_Status__c = '';
        TimeSheetSplit.Krow__Billable__c = true;
        TimeSheetSplit.Krow__Date__c = Date.today().addDays(2);
        TimeSheetSplit.Krow__Timesheet__c = TimesheetList[0].Id;
        //TimeSheetSplit.Krow__Timesheet_Detail__c = null;//TimeSheetDetailList[0].Id;
        TimeSheetSplit.Krow__Invoiced__c = false;
        TimeSheetSplit.Krow__Hours__c = 5;
        TimeSheetSplit.Krow__Exclude_From_Calculation__c = true;
        TimeSheetSplit.Krow__Krow_Project__c = ProjectList[0].Id;
        TimesheetSplitList.add(TimeSheetSplit);
        insert TimesheetSplitList;

        TimesheetSplitList[0].Krow__Exclude_From_Calculation__c = false;
        update TimesheetSplitList;

        List<Project_Company__c> ProjectCompanyList = new List<Project_Company__c>();
        Project_Company__c ProCompany = new Project_Company__c ();
        ProCompany.Account__c = accList[0].Id;
        ProCompany.Krow_Project__c = ProjectList[0].Id;
        ProjectCompanyList.add(ProCompany);
        insert ProjectCompanyList;

        Krow__Invoice__c Invoice = new Krow__Invoice__c();
        Invoice.Krow__Invoice_Date__c = Date.today();
        Invoice.Krow__Invoice_Account__c = accList[0].Id;
        Invoice.Krow__Invoice_Approved_Time_Only__c = true;
        Invoice.Krow__Invoice_Approved_Expenses_Only__c = true;
        Invoice.Krow__Summarize__c = 'Totals Only';
        Invoice.Krow__Start_Date__c = Date.today().addDays(-20);
        Invoice.Krow__End_Date__c = Date.today().addDays(10);
        insert Invoice;

        //call method to add invoicelineitems
        AddInvoiceLineItem_Custom.createInvoiveLineItemsForInvoices(Invoice, '');

        List<Krow__Invoice_Line_Item__c> ILIList = new List<Krow__Invoice_Line_Item__c>();
        ILIList = [Select Id, Krow__Hours__c, Krow__Invoice__c From Krow__Invoice_Line_Item__c Where Krow__Invoice__c =: Invoice.Id];
        if(ILIList.size() > 0){
            ILIList[0].Krow__Hours__c = ILIList[0].Krow__Hours__c + 1;
            update ILIList;
        }
    }

    static testMethod void Test_ILIDelete(){ //Without Project Resource
        List<Account> accList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Dev_Test_Acc';
        accList.add(acc);
        insert accList;

        List<Krow__Project__c> ProjectList = new List<Krow__Project__c>();
        Krow__Project__c KpProject = new Krow__Project__c();
        KpProject.Name = 'TestTemplateKpProject';
        KpProject.Krow__Billable__c = true;
        KpProject.Krow__Project_Template__c = false;
        KpProject.Krow__Public_Project_Template__c = false;
        KpProject.Krow__Completion_Method__c = 'Task Completion';
        KpProject.Krow__Project_Status__c = 'Not Started';
        KpProject.Krow__Billing_Type__c = null;
        ProjectList.add(KpProject);
        insert ProjectList;

        ProjectList[0].Krow__Billing_Type__c = null;
        update ProjectList;

        List<Krow__Timesheet__c> TimesheetList = new List<Krow__Timesheet__c>();
        Krow__Timesheet__c TimeSheet = new Krow__Timesheet__c();
        TimeSheet.Krow__Approval_Status__c = '';
        TimeSheet.Krow__Project__c = ProjectList[0].Id;
        TimeSheet.Krow__Week_Start_Date__c = Date.today();
        TimeSheet.Krow__Week_End_Date__c = Date.today().addDays(7);
        TimesheetList.add(TimeSheet);
        insert TimesheetList;

        List<Krow__Timesheet_Split__c> TimesheetSplitList = new List<Krow__Timesheet_Split__c>();
        Krow__Timesheet_Split__c TimeSheetSplit = new Krow__Timesheet_Split__c ();
        TimeSheetSplit.Krow__Approval_Status__c = '';
        TimeSheetSplit.Krow__Billable__c = true;
        TimeSheetSplit.Krow__Date__c = Date.today().addDays(2);
        TimeSheetSplit.Krow__Timesheet__c = TimesheetList[0].Id;
        //TimeSheetSplit.Krow__Timesheet_Detail__c = null;//TimeSheetDetailList[0].Id;
        TimeSheetSplit.Krow__Invoiced__c = false;
        TimeSheetSplit.Krow__Hours__c = 5;
        TimeSheetSplit.Krow__Exclude_From_Calculation__c = true;
        TimeSheetSplit.Krow__Krow_Project__c = ProjectList[0].Id;
        TimesheetSplitList.add(TimeSheetSplit);
        insert TimesheetSplitList;

        TimesheetSplitList[0].Krow__Exclude_From_Calculation__c = false;
        update TimesheetSplitList;

        List<Project_Company__c> ProjectCompanyList = new List<Project_Company__c>();
        Project_Company__c ProCompany = new Project_Company__c ();
        ProCompany.Account__c = accList[0].Id;
        ProCompany.Krow_Project__c = ProjectList[0].Id;
        ProjectCompanyList.add(ProCompany);
        insert ProjectCompanyList;

        Krow__Invoice__c Invoice = new Krow__Invoice__c();
        Invoice.Krow__Invoice_Date__c = Date.today();
        Invoice.Krow__Invoice_Account__c = accList[0].Id;
        Invoice.Krow__Invoice_Approved_Time_Only__c = true;
        Invoice.Krow__Invoice_Approved_Expenses_Only__c = true;
        Invoice.Krow__Summarize__c = 'Totals Only';
        Invoice.Krow__Start_Date__c = Date.today().addDays(-20);
        Invoice.Krow__End_Date__c = Date.today().addDays(10);
        insert Invoice;

        //call method to add invoicelineitems
        AddInvoiceLineItem_Custom.createInvoiveLineItemsForInvoices(Invoice, '');

        List<Krow__Invoice_Line_Item__c> ILIList = new List<Krow__Invoice_Line_Item__c>();
        ILIList = [Select Id, Krow__Hours__c, Krow__Invoice__c From Krow__Invoice_Line_Item__c Where Krow__Invoice__c =: Invoice.Id];
        if(ILIList.size() > 0){
            ILIList[0].Krow__Hours__c = ILIList[0].Krow__Hours__c + 1;
            delete ILIList;
        }
    }
}