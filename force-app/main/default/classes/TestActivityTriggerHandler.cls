/***********************************************************************************
* @author      10k-Expert
* @name		TestActivityTriggerHandler
* @date		2018-11-19
* @description	To test ActivityTriggerHandler
***********************************************************************************/
@isTest
public class TestActivityTriggerHandler {
@isTest
public static void testInsert(){
    IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
    // testData.populateIVPGeneralConfig();
    testData.populateCommonConfig();
    Test.startTest();
    
    List<Task> tasks = testData.tasks;
    Test.stopTest();
    
    System.assertEquals(8, [SELECT Count() FROM Task]);
}

@isTest
public static void testUpdate(){
    IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
    testData.populateIVPGeneralConfig();
    //testData.populateCommonConfig();
    List<Task> tasks = testData.tasks;
    
    Test.startTest();
    
    for(Task task : tasks){
        task.CallType = 'Inbound';
    }
    update tasks;
    
    Test.stopTest();
    
    System.assertEquals(8, [SELECT Count() FROM Task]);
}

@isTest
public static void testDelete(){
    
    IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
    //testData.populateIVPGeneralConfig();
    testData.populateCommonConfig();
    
    List<Task> tasks = testData.tasks;
    tasks[0].Type = 'Call';
    tasks[0].OwnerId = UserInfo.getUserId();
    update tasks;
    System.debug('>>>> Update Tasks FROM Test Class >>>>'+tasks);
    Common_Config__c cconf = Common_Config__c.getInstance();
    cconf.Task_Required_Fields__c = 'Description';
    cconf.Edit_Restriction_Message__c = 'Test message';
    cconf.Task_Edit_Profiles__c = 'System Administrator';
    cconf.Task_Delete_Profiles__c = 'Ignite';

    Test.startTest();
    
    try{
        insert cconf;
        delete tasks[0];
    }
    catch(Exception e){
        System.assert(e.getMessage() != NULL);
    }
    Test.stopTest();
}

/**  
@Description Method to test prevention of Task comment editing, if any draft task comment is associated with this task.
@author      Nimisha
@date        25/07/2019
*/
@isTest
public static void testPreventInlineEditingTaskComment() {
    
    /** Insert an Account */
    List<Account> lstAccount = TestFactory.createSObjectList(new Account(), 2, true);
    /** Insert an Task */
    List<Task> lstTask = new List<Task>();
    List<Task> lstTaskToBeUpdated = new List<Task>();
    for(Integer i = 0; i < 2; i++) {
        
        lstTask.addAll(TestFactory.createTasks( 1, lstAccount[i].Id, 'Task', Date.today().addDays(1), 'In Progress', true));
    }
    System.assertEquals(2, [SELECT Count() FROM Task]);      
    
    List<Task_Comment__c> lstTaskComments = new list<Task_Comment__c>{
        new Task_Comment__c(Comment__c = 'Test Subject Task Description',
                            Company__c = lstAccount[0].Id,
                            User__c = UserInfo.getUserId(),
                            Task_Id__c = lstTask[0].Id,
                            IsEditedFromComponent__c = false)
            };
                insert lstTaskComments;
    System.assertEquals(1, [SELECT Count() FROM Task_Comment__c]);  
    
    Test.startTest();
    try {
        
        for(Task oTask : lstTask){
            oTask.Description = 'Test Comment update';
            oTask.Type = 'Call';
            lstTaskToBeUpdated.add(oTask);
        }
        update lstTaskToBeUpdated;
    }catch(Exception ex) {
        
        System.assertEquals(true, ex.getMessage().contains(System.Label.Prevent_Editing_Task_Comment));
    }
    Test.stopTest();
}

/**  
@Description Method to test prevention of Task comment editing, if any draft task comment is associated with this task.
@author      Nimisha
@date        25/07/2019
*/
@isTest(seeAllData=true)
public static void testClosedActivities() {
    
    Account acct = new Account(Name = 'Apex Test');
    insert acct;
    Contact con = new Contact(LastName = 'Apex Test new');
    insert con;

    Task tsk1 = new Task(WhatId = acct.Id, Subject = 'Email: apex test', ActivityDate = date.today(), Status = 'Completed',Type__c = 'Call');
    Task tsk2 = new Task(WhatId = acct.Id, Subject = 'Email: apex test', ActivityDate = date.today(), Source__c= Null,Status = 'Completed',Type__c = 'Call');
    Task tskCon = new Task(WhoId = con.Id, Subject = 'Email: apex test', ActivityDate = date.today(), Source__c= 'Inbound', Status = 'Completed',Type__c = 'Call');
    Task[] tskList = new List<Task>{ tsk1,tskCon,tsk2};
    insert tskList;
    System.assertEquals(1, [SELECT Count() FROM Task WHERE id =: tskList[0].Id ]);
}

@isTest 
public static void ActivityTriggerTest() {
    
    Account accTest=new Account(Name='testAcc');
    insert  accTest;
    
    Test.startTest();
    
    List<task> taskList=new List<task>{
        new Task(Subject='Call',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(1),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='demo values',Type='Call',type__c='Call',Source__c='Inbound'),
            new Task(Subject='Email',Status='Not Started',Priority='Normal',ActivityDate=date.today(),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='test values',Type='Email',type__c='Email',Source__c='Inbound'),
            new Task(Subject='Other',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(2),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='test values2'),
            new Task(Subject='Call',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(3),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='demo values2')
            //   new Task(Subject='Call',Status='Not Started',Priority='High',ActivityDate=null,WhatId=null,OwnerId=null,Description='')                
            };
                insert taskList;
    
    accTest=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTest.Id];
    system.assertEquals(date.today(),accTest.Next_Action_Date_Talent__c);
    system.assertEquals('Email',accTest.Next_Action_Talent__c);
    system.assertEquals('0056C000001ba9SQAQ',accTest.Next_Action_Owner_Talent__c);
    
    taskList[1].ActivityDate=date.today().addDays(4);
    update taskList;
    
    accTest=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTest.Id];
    system.assertEquals('Call',accTest.Next_Action_Talent__c);
    system.assertEquals(date.today().addDays(1),accTest.Next_Action_Date_Talent__c);
    system.assertEquals('0056C000001ba9SQAQ',accTest.Next_Action_Owner_Talent__c);
    
    Test.stopTest(); 
    
    /*    tasklist=[Select ID From Task Where ActivityDate =:date.today()];
        delete tasklist;
        accTest=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTest.Id];
        system.assertEquals(date.parse('01/19/2020'),accTest.Next_Action_Date_Talent__c);
        system.assertEquals('Call',accTest.Next_Action_Talent__c); 

        undelete taskList[1];
        accTest=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTest.Id];
        system.assertEquals(date.parse('01/18/2020'),accTest.Next_Action_Date_Talent__c);
        system.assertEquals('Email',accTest.Next_Action_Talent__c); */
    

    } 
/*  @testSetup 
    static void ActivityTriggerTest() {

        Account accTest=new Account(Name='testAcc');
        insert  accTest;
        Task newtask=new Task(); 
        newtask.Subject='Call';
        newtask.Status='Not Started';
        newtask.Priority='Normal';
        newtask.ActivityDate=date.today().addDays(3);
        newtask.WhatId=accTest.Id;
        newtask.OwnerId='0056C000001ba9SQAQ';
        newtask.Description='demo values2';

        insert newtask; 
        List<task> taskList=new List<task>{
        new Task(Subject='Call',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(-1),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='demovalues'),
        new Task(Subject='Email',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(5),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='testvalues')
        };
        insert taskList;

    }
        @isTest
        static void testTalentTaskInsert(){

        Account accTestId=[SELECT Id,Name FROM Account WHERE Name='testAcc'];
        List<task> taskList=new List<task>{
        new Task(Subject='Call',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(1),WhatId=accTestId.Id,OwnerId='0056C000001ba9SQAQ',Description='demo values'),
            new Task(Subject='Email',Status='Not Started',Priority='Normal',ActivityDate=date.today(),WhatId=accTestId.Id,OwnerId='0056C000001ba9SQAQ',Description='test values')
        };
        insert taskList;
            //   new Task(Subject='Call',Status='Not Started',Priority='High',ActivityDate=null,WhatId=null,OwnerId=null,Description='')                
            
                
                Account accRecords=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTestId.Id];
                system.assertEquals(date.today().addDays(-1),accRecords.Next_Action_Date_Talent__c);
                system.assertEquals('Call',accRecords.Next_Action_Talent__c);
                system.assertEquals('0056C000001ba9SQAQ',accRecords.Next_Action_Owner_Talent__c);
            
    }
        @isTest
        static void testTalentTaskUpdate(){
            Account accTestId=[SELECT Id,Name FROM Account WHERE Name='testAcc'];
        List<task> listOfTasks=[Select Id,ActivityDate,Subject,OwnerId FROM Task Where WhatId=:accTestId.Id];
        for(Integer i=0;i<listOfTasks.size();i++){
                listOfTasks[0].ActivityDate=date.today().addDays(2);
                break;
        }
        
        update listOfTasks;

        Account accTestRecords=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTestId.Id];
        system.assertEquals('call',accTestRecords.Next_Action_Talent__c);
        system.assertEquals(date.today(),accTestRecords.Next_Action_Date_Talent__c);
        system.assertEquals('0056C000001ba9SQAQ',accTestRecords.Next_Action_Owner_Talent__c);
    } */

}