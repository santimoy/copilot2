@isTest
private class Test_EmailParser {
	
	@isTest static void test_method_one() {
		Account acc = new Account(Name = 'testAccount');
		insert acc;
		EmailParser_Killswitch__c killSwitch = new EmailParser_Killswitch__c();
		killSwitch.EmailSegmentDelimiters__c = 'From^wrote';
		killSwitch.Shut_off_Inbound_email_parsing__c = true;
		killSwitch.Shut_off_Outbound_email_parsing__c = true;
		killSwitch.RecordTypeParse__c = 'Task';
		insert killSwitch;
		Task t = new Task();
		t.WhatId = acc.Id;
		t.Subject = 'Reply:Email Inbound';
		t.Type__c = 'Email';
		t.Source__c = 'Inbound';
		t.Description = '\r Testing \n email \nbody\n> \r test\n> From: testperson\r> ';
		insert t;
		Database.executeBatch(new Batch_EmailParser('2018-07-31',acc.Id,UserInfo.getUserId(),'Inbound',true), 1);
		EmailParser.getEmailParser(t.Subject,t.Description);   
	}
	@isTest static void test_method_two() {
		Account acc = new Account(Name = 'testAccount');
		insert acc;
		EmailParser_Killswitch__c killSwitch = new EmailParser_Killswitch__c();
		killSwitch.EmailSegmentDelimiters__c = 'From^wrote';
		killSwitch.Shut_off_Inbound_email_parsing__c = true;
		killSwitch.Shut_off_Outbound_email_parsing__c = true;
		killSwitch.RecordTypeParse__c = 'Task';
		insert killSwitch;
		Task t = new Task();
		t.WhatId = acc.Id;
		t.Subject = 'Email:Email Outbound';
		t.Type__c = 'Email';
		t.Source__c = 'Outbound';
		t.Description = '\r Testing \n email \nbody\n> \r test\n> From: testperson\r> ';
		insert t;
		Database.executeBatch(new Batch_EmailParser('2018-07-31',acc.Id,UserInfo.getUserId(),'Outbound',true), 1);
		EmailParser.getEmailParser(t.Subject,t.Description);   
	}
	@isTest static void test_method_three() {
		Account acc = new Account(Name = 'testAccount');
		insert acc;
		EmailParser_Killswitch__c killSwitch = new EmailParser_Killswitch__c();
		killSwitch.EmailSegmentDelimiters__c = 'From^wrote';
		killSwitch.Shut_off_Inbound_email_parsing__c = true;
		killSwitch.Shut_off_Outbound_email_parsing__c = true;
		killSwitch.RecordTypeParse__c = 'Task';
		insert killSwitch;
		Task t = new Task();
		t.WhatId = acc.Id;
		t.Subject = 'Reply:Email Inbound';
		t.Type__c = 'Email';
		t.Source__c = 'Inbound';
		t.Description = '\r Testing \n email \nbody\n> \r test\n> From: testperson\r> ';
		insert t;
		Database.executeBatch(new Batch_EmailParser('2018-07-31',acc.Id,UserInfo.getUserId(),'Inbound',false), 1);
		EmailParser.getEmailParser(t.Subject,t.Description);   
	}
	@isTest static void test_method_four() {
		Account acc = new Account(Name = 'testAccount');
		insert acc;
		EmailParser_Killswitch__c killSwitch = new EmailParser_Killswitch__c();
		killSwitch.EmailSegmentDelimiters__c = 'From^wrote';
		killSwitch.Shut_off_Inbound_email_parsing__c = true;
		killSwitch.Shut_off_Outbound_email_parsing__c = true;
		killSwitch.RecordTypeParse__c = 'Task';
		insert killSwitch;
		Task t = new Task();
		t.WhatId = acc.Id;
		t.Subject = 'Email:Email Outbound';
		t.Type__c = 'Email';
		t.Source__c = 'Outbound';
		t.Description = '\r Testing \n email \nbody\n> \r test\n> From: testperson\r> ';
		insert t;
		Database.executeBatch(new Batch_EmailParser('2018-07-31',acc.Id,UserInfo.getUserId(),'Outbound',false), 1);
		EmailParser.getEmailParser(t.Subject,t.Description);   
		Batch_ActivityEmailParserScheduler.scheduleMe(1);
	}
	
	
}