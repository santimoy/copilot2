@isTest
public class IVP_UserTrigger_Handler_Test {
    private static testMethod void testCreateGroupOnInsert(){
        IVPTestFuel tFuel = new IVPTestFuel();
        
        //This is returning Users List(Dummy Records) from IVPTestFuel class
        Test.startTest();
        List<User> users = tfuel.users;
        Test.stopTest();
        System.assert(!users.isEmpty() , 'Checking whether users list size is greater than 0 0r not');
        
        List<String> groupName = new List<String>{IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL+users[0].Id};
        List<Group> groupList = [SELECT Id, DeveloperName FROM Group 
        							WHERE DeveloperName in :groupName];
        
        System.assertEquals(1, groupList.size(), 'New group created');
    }
    private static testMethod void testCreateGroupOnUpdate(){
        IVPTestFuel tFuel = new IVPTestFuel();
        //This is returning Users List(Dummy Records) from IVPTestFuel class
        List<User> users = tfuel.users;
        System.assert(users.size()>0, 'Checking whether users list size is equals to 0 0r not');
        
        List<Profile> profiles = [SELECT Id,Name FROM PROFILE 
                                  WHERE Name IN ('System Administrator','Analyst Full') ORDER By Name ASC];
       
        System.assertEquals(profiles.size(), 2, 'Checking whether profiles list have 2 records or not'); 
        System.assertEquals(profiles[0].Name, 'Analyst Full', 'Chceking whether First Pofile is Analyst Full or not');
        System.assertEquals(profiles[1].Name, 'System Administrator', 'Chceking whether Second Pofile is System Administrator or not');
        
        System.assertEquals(profiles[0].Id, users[0].profileId,'Checking whether profile Id of second user is Analyst Full or not');
        
        //Updating First User's Profile from Analyst Full to System Administrator
        users[0].profileId = profiles[1].Id;
        
        //Updating Users Records
        update users;
        
        //Now again Updating Users Profile from System Administrator to Analyst Full for check duplicated group will created or not
        users[0].profileId = profiles[0].Id;
        Update users;
       
        List<String> groupName = new List<String>{IVPSDLVMaintainVisibilityService.IVP_SDLV_NAME_INITIAL+users[0].Id};
        List<Group> groupList = [SELECT Id, DeveloperName FROM Group 
        							WHERE DeveloperName in :groupName];
		System.assertEquals(1, groupList.size(), 'New group created');
    }
}