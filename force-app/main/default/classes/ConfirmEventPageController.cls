public with sharing class ConfirmEventPageController {
	public String ObjectId {get;set;}
	public String UserEmail {get;set;}
	public String ResponseCode {get;set;}
    public String ResponseMessage {get;set;}
	public ConfirmEventPageController () {
		ObjectId = ApexPages.currentPage().getParameters().get('ObjectId');
		UserEmail = ApexPages.currentPage().getParameters().get('UserEmail');
		ResponseCode = ApexPages.currentPage().getParameters().get('ResponseCode');
	}
	public void InitPage() {
		List<SL_LIGHT_RSVP__SL_Event_Invitees__c> inviteesToInsert = new List<SL_LIGHT_RSVP__SL_Event_Invitees__c>();
		List<User> allUsers = [Select Id from User where Email =:UserEmail limit 1];
		if(!allUsers.isEmpty()) {
			List<SL_LIGHT_RSVP__SL_Event_For_RSVP__c> event = [Select Id from SL_LIGHT_RSVP__SL_Event_For_RSVP__c where Id=:ObjectId AND SL_LIGHT_RSVP__Available_Seats__c > 0 limit 1];
			if(!event.isEmpty()) {
				List<SL_LIGHT_RSVP__SL_Event_Invitees__c> existingInvites = [Select Id from SL_LIGHT_RSVP__SL_Event_Invitees__c where SL_LIGHT_RSVP__User__c =:allUsers[0].Id AND SL_LIGHT_RSVP__Event_For_RSVP__c =:event[0].Id limit 1];
				if(existingInvites.isEmpty()) {
					SL_LIGHT_RSVP__SL_Event_Invitees__c invitee = new SL_LIGHT_RSVP__SL_Event_Invitees__c();
					invitee.SL_LIGHT_RSVP__User__c =allUsers[0].Id;
					invitee.SL_LIGHT_RSVP__Event_For_RSVP__c =event[0].Id;
					inviteesToInsert.add(invitee);
				}
                else {
                    ResponseMessage = 'You are already registered for the event.';
                }
			}
            else {
                ResponseMessage = 'The event is at capacity.';
            }
		}
		if(!inviteesToInsert.isEmpty()) {
			insert inviteesToInsert;
            ResponseMessage = 'You are now registered for the event.';
		}
	}
}