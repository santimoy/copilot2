/**
* @author Amjad-Concretio
* @date 2018-04-30
* @className IVP_RecentlyUnassigned_Batch
*
* @description Batch to process all the recently unassigned records
*/
public class IVP_RecentlyUnassigned_Batch implements Database.Batchable<sObject>, Database.Stateful{
    
    /**
    * MAP for storing account Id to Unassigned user set.
    * At class level because we want to process all records in once.
    * finish method will pass this to queable job.
    **/
    public Map<String,Set<String>> accountToUnassignedUser = new Map<String,Set<String>>();
    //Map for UserName to UserId, Because we having Name in Old_Owner__c
    public Map<String,String> userNameTOId {
        get{
            if(userNameTOId == null){
                userNameTOId = new Map<String,String>();
                for(User u: [Select Id,Name From User Where isActive =: true]){
                    userNameTOId.put(u.Name,(u.Id+'').substring(0, 15));
                }
            }
            return userNameTOId;
        }set;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String days = system.label.IVP_Recently_Unassigned_Days; 
        String query = 'Select Old_Owner__c,Company__c FROM Recently_Unassigned__c ';
        query +=    'where Old_Owner__c != \'Unassigned\' And Changed_Date_time__c = LAST_N_DAYS:'+days;
        query += ' ORDER BY Changed_Date_Time__c DESC';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        IVP_RecentlyUnassignedService.prepareMap(scope, accountToUnassignedUser, userNameTOId);
    }
    
    public void finish(Database.BatchableContext BC){
        System.debug('accountToUnassignedUser');
        System.debug(accountToUnassignedUser);
        System.enqueueJob(new IVP_RecentlyUnassigned_Queueable(accountToUnassignedUser));
        
    }
    
}