/**
 * @File Name          : HivebriteIntegrationCompanyHelper.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 6/11/2019, 4:22:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/10/2019   Anup Kage     Initial Version
**/
public class HivebriteIntegrationCompanyHelper {
    private String Integration_API; // label of metadata record
    private String Integration_API_Request; // label of metadata record
    private String Integration_API_Object_Label; // label of metadata record
    private String ExternalIDField; // used IN upserting records
    private String ObjectName; // used IN upserting records
    private String jobType;
    List<SObject> recordList;

    private String LOG_LEVEL =  HivebriteIntegrationUtils.lOG_LEVEL;
    private Integration_API__mdt ApiBaseURL = HivebriteIntegrationUtils.ApiBaseURL;
    
    public HivebriteIntegrationCompanyHelper(String Integration_API_Object_Label, String Integration_API,String jobType, List<SObject> recordList){
        this.Integration_API_Object_Label = Integration_API_Object_Label;
        this.Integration_API = Integration_API;
        // this.Integration_API_Request = Integration_API_Request;
        this.jobType = jobType;
        this.recordList = recordList;
        if(ApiObject != null)
            this.ObjectName = ApiObject.Object_API_Name__c;
    }
    public HivebriteIntegrationCompanyHelper(){

    }
    public HivebriteIntegrationCompanyHelper(String Integration_API_Object_Label, String Integration_API, List<SObject> recordList){
        this.Integration_API_Object_Label = Integration_API_Object_Label;
        this.Integration_API = Integration_API;
        // this.Integration_API_Request = Integration_API_Request;
        this.jobType = jobType;
        this.recordList = recordList;
        if(ApiObject != null)
            this.ObjectName = ApiObject.Object_API_Name__c;
    }


    private Integration_API_Object__mdt ApiObject {get{
        if(ApiObject == null){
            ApiObject = [SELECT Id, Label, Integration_API__c, Integration_Object_Name__c, Object_API_Name__c, Object_Label__c 
                        FROM Integration_API_Object__mdt
                        WHERE Label =: Integration_API_Object_Label limit 1];
        }
        return ApiObject;
     }set;}
    
    /**
    query method endpont and parameters
    */
    private Integration_API_Request__mdt createCompanyRequest {get{
        if(createCompanyRequest == null){
            createCompanyRequest = [SELECT Integration_API__r.Base_URL__c, Path__c, Verb__c 
                                    FROM Integration_API_Request__mdt 
                                    WHERE Action__c = 'Create' and Integration_API_Object__r.label =:Integration_API_Object_Label limit 1];
                                    // Label = 'Create Company' limit 1];
        }
        return createCompanyRequest;
    }set;}
    private Integration_API_Request__mdt updateCompanyRequest {get{
        if(updateCompanyRequest == null){
            updateCompanyRequest = [SELECT Integration_API__r.Base_URL__c, Path__c, Verb__c 
                                    FROM Integration_API_Request__mdt 
                                    WHERE Action__c = 'Update' and Integration_API_Object__r.label =:Integration_API_Object_Label limit 1];
                                    /*Label = 'Update Company' */ 
        }
        return updateCompanyRequest;
    }set;}
    /**
    to create request Body need field Mapping 
     */
     //TODO  handle multiple SOQL queries OR  create generic SOQL Query class and all SOQL 
    private List<Integration_API_Field__mdt> mapingFieldsRecords{get{
        if(mapingFieldsRecords == null){
             mapingFieldsRecords = [SELECT ID, Data_Type__c, default_value__c,Field_Mapping_Type__c, Integration_Field_Name__c, Precision__c, Size__c, Field_API_Name__c, External_Id__c
                                    FROM Integration_API_Field__mdt 
                                    WHERE Integration_API_Object__r.Label =: this.Integration_API_Object_Label order by External_Id__c DESC ];
        }
        return mapingFieldsRecords;        
     }set;}

    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    public void startCalloutProcess(){

        //!STEP 1 :prepare request Body
        Map<Id, HttpRequest> requestByRecordId = new Map<Id,HttpRequest>();
        Map<Id, SObject> accountById = new Map<Id, SObject>(recordList);
        Integration_API_Field__mdt ExternalIDFieldObj = HivebriteIntegrationQueryUtils.getExternalIdField(Integration_API_Object_Label);
        if(ExternalIDFieldObj != null ){
            ExternalIDField = ExternalIDFieldObj.Field_API_Name__c;
        }
        Map<Id, HttpResponse> responseByRecordId = new Map<Id,HttpResponse>();
         for(sobject record : recordList){
            //  System.debug('inisde callout asyc'+Limits.getLimitAsyncCalls());
            if(IsNotHittingCallOutLimit()){
                requestByRecordId.put(record.Id, prepareRequestData(record));
                // check we are hitting the limts or not;
                //!STEP 3 :hit endpoint
                HttpResponse response = HttpUtils.doCallout(requestByRecordId.get(record.Id));
                if(response.getStatusCode() == 401 && IsNotHittingCallOutLimit()){
                    EventLog.generateLog(requestByRecordId.get(record.Id), response, 'HivebriteIntegrationCompanytHelper', 'startCalloutProcess', 'error');
                    HttpUtils.handleOAuth('Hivebrite API', 'Get OAuth Token', false);
                    Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
                    // avoid loop execution when we constanly getting error from oAuth
                    // if(accToken.LastModifiedDate == System.now()){
                    if(accToken.LastModifiedDate >= System.now().addMinutes(-1) && accToken.LastModifiedDate <= System.now()){
                        enqueueNextJob(jobtype, accountById.values());
                        break;
                    }
                } else if(response.getStatusCode() == 200 || response.getStatusCode() == 201) {
                    accountById.remove(record.Id);
                    responseByRecordId.put(record.Id, response);
                } else/* if(lOG_LEVEL.equalsIgnoreCase('DEBUG') )*/{ // store record when we have debug level as 'DEBUD'
                    EventLog.generateLog(requestByRecordId.get(record.Id), response, 'HivebriteIntegrationCompanytHelper', 'startCalloutProcess', 'error');
                }                
            } else {
                enqueueNextJob(jobtype, accountById.values());
                break;
            }
        }
        //!STEP 4 :mapFields with Account 
        updateSFCompanyRecord(responseByRecordId, requestByRecordId);         
    }
    

    /**
    Checking we are not hiting CallOut Limits
     */
    private Boolean IsNotHittingCallOutLimit(){
        return Limits.getCallouts() < Limits.getLimitCallouts() - 1 ;

    }
    // preapare JSON DATA for requestbody.
    private HttpRequest prepareRequestData(SObject record){
        //!STEP 2 :prepare request         
        String requestBody = prepareRequestBody(record);

        String ExternalFieldValue = ExternalIDField != null ? String.valueof(record.get(ExternalIDField)) : '' ;
        // System.debug('okay im heere');
        HttpRequest request;
        if(String.isNotBlank(ExternalIDField) && String.isNotBlank(ExternalFieldValue)){
            request = HttpUtils.getCallOutData(ApiBaseURL, updateCompanyRequest);
            String endpoint = request.getEndpoint();
            if(endpoint.contains('{id}')){            
                endpoint = endpoint.replace('{id}', ExternalFieldValue);
                request.setEndpoint(endpoint);
            }
        }else {
            request = HttpUtils.getCallOutData(ApiBaseURL, createCompanyRequest);            
        }        
        if( String.isNotBlank(requestBody) && (request.getMethod() == 'PUT' || request.getMethod() == 'POST')){
            request.setBody(requestBody);
        }
        return request;       
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @param record 
    * @return String 
    **/
    private String prepareRequestBody(SObject record){
        Map<String, Object> valueByfield = new Map<String, Object>();
        for(Integration_API_Field__mdt objMdtFld : mapingFieldsRecords){
            if(objMdtFld.Field_Mapping_Type__c == 'Inbound'){ // field type is Inbound then skip that field.
                    continue;
            }
            // 
            if(objMdtFld.Integration_Field_Name__c == 'venture.locations[country_code]' && String.isNotBlank(String.valueOf(record.get(ExternalIDField)))){
               //Added by Anup: Quick bug fix before UAT...... If you get some time suggest dynamic values to avoid fields values.
                // valueByfield =  HivebriteIntegrationUtils.createJSONBody(objMdtFld.Integration_Field_Name__c, valueByfield, objMdtFld.Field_API_Name__c, record, objMdtFld.default_value__c);
            }else{
                Object fldValue = HivebriteIntegrationUtils.getFieldValue(objMdtFld, record);
                valueByfield =  HivebriteIntegrationUtils.createJSONBody(objMdtFld.Integration_Field_Name__c, valueByfield, fldValue /*objMdtFld.Field_API_Name__c, record, objMdtFld.default_value__c */);
            }
            if(String.isBlank(ExternalIDField) && objMdtFld.External_Id__c){
                ExternalIDField = objMdtFld.Field_API_Name__c;
            }
        }
        return JSON.serialize( valueByfield);
    }

    /**
    map hivebrite id with Company record
     */
    private void updateSFCompanyRecord(Map<Id, HttpResponse> responseByRecordId, Map<Id, HttpRequest> requestByRecordId){
         //!STEP 5: Update Records
        // List<Integration_API_Field__mdt> externalFldMapList = [SELECT ID, Data_Type__c, Integration_Field_Name__c, Precision__c, Size__c, Field_API_Name__c, External_Id__c
        //                                                         FROM Integration_API_Field__mdt 
        //                                                         WHERE Integration_API_Object__r.Label =: this.Integration_API_Object_Label AND 
        //                                                         External_Id__c = true ];
        // if(externalFldMapList.size() > 1){ 
        //     EventLog.generateLog('HivebriteIntegrationCompanytHelper', 
        //                         'updateSFCompanyRecord',
        //                         'error',
        //                         'We have'+externalFldMapList.size()+'  ExternalId fields in the metadata. Please update Integration_API_Field__mdt records for '+this.Integration_API_Object_Label );
        //     return;
        // }
        Integration_API_Field__mdt externalFldMapList = HivebriteIntegrationQueryUtils.getExternalIdField(this.Integration_API_Object_Label);
        List<Account> accountList = new List<Account>();
        for(Id recId : responseByRecordId.keySet()){
            HttpResponse response = responseByRecordId.get(recId);
            
            if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                Account objAcc = updateAccount(response.getBody(), externalFldMapList);
                if(objAcc != null){
                    objAcc.Id = recId;
                    //objAcc.LastModifiedById = '00556000003Lp89AAC';
                    accountList.add(objAcc);
                }
            }else {
                
                EventLog.generateLog(requestByRecordId.get(recId), response, 'HivebriteIntegrationCompanytHelper', 'updateSFCompanyRecord', 'ERROR');
            }
        }
        //TriggerHandler.bypass('AccountTriggerHandlerHivebrite');
        List<Database.saveResult> resultList = Database.update(accountList, false);
        EventLog.generateLog('HivebriteIntegrationCompanytHelper', 'updateSFCompanyRecord', resultList);
    }
    // update Account record with  hivebrite Id
    private Account updateAccount(String responseBody, Integration_API_Field__mdt externalFldMap){
         Account objAccRet;
        try{
            Map<String, Object> companyJSON = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
            List<String> fldList = externalFldMap.Integration_Field_Name__c.split('\\.');
            Integer count = 1;
            for(String fld : fldList){
                if(companyJSON.containsKey(fld)){
                    if(Count == fldList.size()){
                        objAccRet = new Account();
                        objAccRet.put(externalFldMap.Field_API_Name__c, String.valueOf(companyJSON.get(fld)));
                    }else{
                        companyJSON = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(companyJSON.get(fld))); 
                    }
                }else{
                    companyJSON = null;
                }
                count++;
            }
        }catch(JSONException e){
            EventLog.generateLog('HivebriteIntegrationCompanytHelper', 'updateAccount', 'error', e);
        }
        catch (Exception e){
            EventLog.generateLog('HivebriteIntegrationCompanytHelper', 'updateAccount', 'error', e);
        }
        return objAccRet;
    }
    @TestVisible
    private void enqueueNextJob(String jobType, List<Account> smallRecordsList ){
       
        HivebriteIntegrationQueueable objQueue = new HivebriteIntegrationQueueable(jobType, smallRecordsList);
        if(!Test.isRunningTest() && !smallRecordsList.isEmpty()){
            // call Queueable class to update company record on hivebrite server.
             System.debug('Inside Company helper------------->'+jobType);
            ID jobId = System.enqueueJob(objQueue);            
        }    
    }
}