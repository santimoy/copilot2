public class EmailBlasterController
{
	@AuraEnabled
	public static List<String> getAllEmails( Id recordId )
	{
		List<String> allEmails = new List<String>();
		if( String.isNotBlank( recordId ) )
		{
			Account currentAccount = [ SELECT Id, Website_Domain__c FROM Account WHERE Id = :recordId ];
			List<Contact> keyContacts = [ SELECT Id, FirstName, LastName, Email, Title FROM Contact WHERE AccountId = :recordId AND Key_Contact__c = true ORDER BY CreatedDate DESC LIMIT 1 ];
			if( !keyContacts.isEmpty() )
			{
				Contact keyContact = keyContacts.get(0);
				if( String.isNotBlank( keyContact.Email ) )
				{
					allEmails.add( keyContact.Email );
				}
				else
				{
					if( String.isNotBlank( keyContact.FirstName ) )
					{
						String sfirst = keyContact.FirstName.replace( '"', '' );
						String slast = keyContact.LastName.replace( '"', '' );
						List<String> slNames = slast.split(' ');
						if( slNames.size() > 1 )
						{
							if( slNames.size() == 2 )
								sLast = slNames.get(1);
							if( slNames.size() == 3 )
								sLast = slNames.get(2);
						}

						String websiteDomain = ( String.isNotBlank( currentAccount.Website_Domain__c ) ) ? currentAccount.Website_Domain__c : '';
						EmailFinder finder = new EmailFinder( websiteDomain, slast, sfirst );
						allEmails = finder.getAllCombinations();
					}
				}
			}
		}

		return allEmails;
	}
}