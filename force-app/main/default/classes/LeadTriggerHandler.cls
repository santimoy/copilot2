/**
* @Description LeadTriggerHandler is used to perform logic for LeadTrigger
*
* @Author: Daniel Llewellyn
* @Date:   Aug 20, 2018
* @Note: DL 08/20/2018 Initial Draft
**/

public without sharing class LeadTriggerHandler {

    Common_Config__c cconf;
    Id ConvertedLeadOwnerId;
    String prospectRTId;
    String portCoRTId;
    /**
    *@Description constructor. Puts common config in public scope
    **/
    public LeadTriggerHandler()
    {
        cconf = CommonConfigUtil.getConfig();
        List<RecordType> recList = [Select Id,Name from RecordType where SObjectType ='Account' and (Name ='Prospect' OR Name = 'Portfolio Company')];
        for(RecordType r:recList)
        {
            if(r.Name == 'Prospect')
                prospectRTId = r.Id;
            else if(r.Name == 'Portfolio Company')
                portCoRTId = r.Id;
        }
        string ownerName = cconf.Pardot_Converted_Owner_Full_Name__c.trim();

        list<User> users = [select Id, Name from User where name = :ownerName limit 1];

        for(User thisUser :users)
            ConvertedLeadOwnerId = thisUser.Id;

        if(ConvertedLeadOwnerId == null) throw new customException('Unable to find user with name ' + cconf.Pardot_Converted_Owner_Full_Name__c + '. Unable to continue. Aborting lead conversion');
    }

	/**
    *@Description Handler method meant to be called by LeadTrigger during before insert event.
    *@Param newLeads list of new leads from the Trigger.new context variable.
    **/
    public void onBeforeInsert( List<Lead> newLeads )
    {
        System.debug('\n\n\n-------Starting onBeforeInsert');
        
        // if website empty and email not empty, then populate website value with parsed email domain value
        for(Lead ld : newLeads)
        	if(String.isBlank(ld.Website) && String.isNotBlank(ld.Email))
        		ld.Website = parseEmailDomain(ld.Email);
    }
    
    /**
    *@Description Handler method meant to be called by LeadTrigger during after insert event.
    *@Param newLeads list of new leads from the Trigger.new context variable.
    **/
    public void onAfterInsert( List<Lead> newLeads )
    {
        System.debug('\n\n\n-------Starting onAfterInsert');

        list<Database.LeadConvertResult> createdAccountsFromPardotLeads = convertAndMatchPardotLeads( newLeads );

        system.debug('\n\n\n------- Finished converting!');
        system.debug(createdAccountsFromPardotLeads);

		if(createdAccountsFromPardotLeads!=null && createdAccountsFromPardotLeads.size()>0) {
			// additional processing to toggle contact key contact
			map<Id, Id> accountContactMap = new map<Id, Id>();
			for(Database.LeadConvertResult lcr : createdAccountsFromPardotLeads)
				if(lcr.isSuccess())
					accountContactMap.put(lcr.getAccountId(), lcr.getContactId());
				
			updateAccountKeyContact(accountContactMap);
		}
    }

    /**
    *@Description Method that finds existing accounts using duplicate rules to convert leads automatically that are created by Pardot. If an account with a matching website domain is found that is used
    *             as the accountId for the converted lead. If not a new account is created using normal lead convert process.
    *@Param leads List of existing leads to evaluate, convert, and attach to accounts if possible.
    *@Return list of lead conversion results.
    **/
    public list<Database.LeadConvertResult> convertAndMatchPardotLeads (list<Lead> leads)
    {
        system.debug('\n\n\n------- Entering convertAndMatchPardotLeads method');

        list<Lead> pardotLeads = new list<Lead>(); //filtered list of leads created by the pardot user
        list<Database.LeadConvert> leadConverts = new list<Database.LeadConvert>(); //list of converted leads to be returned to the caller
        set<id> duplicateAccountIds = new set<id>();
        map<Id,Id> tempAccountToLeadIdMap = new map<Id,Id>();

        //iterate over every lead. We only care about those created by pardot where the pardot url value is not empty. 
        //Then parse out their website domain from the website field.  we will use that to match against existing accounts.
        for(Lead thisLead : leads)
        {
            system.debug(thisLead);
            if(String.isEmpty(thisLead.pi__url__c))
            {
                system.debug('\n\n\n------- Lead Pardot URL is empty. Skipping lead convert logic');
                continue;
            }

            pardotLeads.add(thisLead);
        }

        //if there are no leads that were created by a pardot user then return empty list because the call to findDuplicates will error on an empty list so avoid that.
        if(pardotLeads.isEmpty()) return null;

        //Now that we have all the leads to convert we need to find accounts that would be duplicates. We do this by creating a temporary account (not inserting it)
        //that is generated from the lead information. Then with those temporary accounts we run a duplicate check to see which ones are dupes. For all those that are dupes
        //we set the leads related account id to the existing record. For those that are not dupes we allow the lead convert to create a new account as normal.
        map<Id,List<Account>> leadToTempAccountMap = createAccountFromLead(pardotLeads);

        list<Account> accountsToFindDupesFor = new list<Account>();

        //crate one big list of all the accounts by looping over each list of accounts returned by createAccountFromLead
        for(Id thisLeadId : leadToTempAccountMap.keySet())
        {
            accountsToFindDupesFor.addAll(leadToTempAccountMap.get(thisLeadId));
        }
        system.debug('\n\n\n------- Generated accounts from leads');
        system.debug(accountsToFindDupesFor);
        //Find all duplicate accounts
        Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates( accountsToFindDupesFor );

        system.debug('\n\n\n------- Found all duplicate results');
        system.debug(results);

        //loop over each duplicate (each account may hit multiple duplicate rules)
        for(Datacloud.FindDuplicatesResult thisDupe : results)
        {
            //;oop over each duplicate result for this specific account
            for(Datacloud.DuplicateResult thisResult : thisDupe.duplicateResults)
            {
                //loop over each rule within this result looking for the account rules
                for(Datacloud.MatchResult matchResult : thisResult.getMatchResults())
                {
                    for (Datacloud.MatchRecord matchRec : matchResult.getMatchRecords())
                    {
                        duplicateAccountIds.add((Id) matchRec.getRecord().get('id'));
                    }
                }
            }

        }

        //get the record types of all the accounts that were found to be duplicates. We only want accounts that are prospects to be accounts for these leads
        list<Account> matchingAccounts = [select Id, Website_Domain__c, Name from Account where Id in :duplicateAccountIds and (RecordType.Name = 'Prospect' or RecordType.Name='Portfolio Company')];

        system.debug('\n\n\n------- Found all matching accounts that were a duplicate and have a record type of prospect or portfolio company');
        system.debug(matchingAccounts);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];

        //record any leads who don't get a match from the logic below. Then query for any accounts that have the same name and set them as the lead id if a match is found.
        map<string,Lead> unmatchedLeadsKeyedByName = new map<string,Lead>();
        for(Lead thisLead : pardotLeads)
        {
            boolean foundMatchingAccount = false;
            Database.LeadConvert thisConvert =  new Database.LeadConvert();
            thisConvert.setLeadId(thisLead.Id);
            thisConvert.setConvertedStatus(convertStatus.MasterLabel);
            thisConvert.setDoNotCreateOpportunity(true);
            //thisConvert.setOwnerId(ConvertedLeadOwnerId);
            string thisLeadWebsiteDomain = parseWebsiteDomain(thisLead.website);
            string thisLeadEmailDomain = parseEmailDomain(thisLead.Email);
            system.debug('\n\n\n------- Looking at lead: ' + thisLead.Id + '. Looking for an account with a matching website domain. ');
            for(Account thisAccount : matchingAccounts)
            {
                system.debug('\n\n\n-------' +  thisAccount.Website_Domain__c + ' vs ' + thisLeadWebsiteDomain);
                system.debug('\n\n\n-------' +  thisAccount.Website_Domain__c + ' vs ' + thisLeadEmailDomain);
                //check to see if the website domain on this prospect account matches the domain on the lead. If so set this account as this leads account.
                if(thisAccount.Website_Domain__c == thisLeadWebsiteDomain || thisAccount.Website_Domain__c == thisLeadEmailDomain)
                {
                    system.debug('\n\n\n------- Match Found on website domain or email! Setting lead convert account id');
                    thisConvert.setAccountId(thisAccount.Id);
                    foundMatchingAccount = true;
                    break;
                }
            }

            //if not match was found then we can try searching for them by a matching account name.
            if(!foundMatchingAccount)
            {
                unmatchedLeadsKeyedByName.put(thisLead.Company,thisLead);
            }
            else
            {
                leadConverts.add(thisConvert);
            }
        }

        system.debug('\n\n\n------- There are ' + unmatchedLeadsKeyedByName.size() + ' leads that do not yet have account ids. Will now attempt to match them based on account name');
        system.debug(unmatchedLeadsKeyedByName);
        //now find any accounts that have a name matching any of the lead names that were not matched above so we can loop through them for each lead and try to match them up
        list<Account> accountsWithMatchingNames = [select id, name from account where name in :unmatchedLeadsKeyedByName.keySet() and (RecordType.Name = 'Prospect' or RecordType.Name='Portfolio Company')];

        system.debug('\n\n\n------- Found ' + accountsWithMatchingNames.size() + ' accounts with matching names. Will attempt to pair them to their leads now!');
        system.debug(accountsWithMatchingNames);

        //loop over every lead still without a matched account.
        for(Lead thisLead : unmatchedLeadsKeyedByName.values())
        {
            boolean foundMatchingAccount = false;
            Database.LeadConvert thisConvert =  new Database.LeadConvert();
            thisConvert.setLeadId(thisLead.Id);
            thisConvert.setConvertedStatus(convertStatus.MasterLabel);
            thisConvert.setDoNotCreateOpportunity(true);
            thisConvert.setOwnerId(ConvertedLeadOwnerId);
            Lead matchingLead = unmatchedLeadsKeyedByName.get(thisLead.Company);

            for(Account thisAccount : accountsWithMatchingNames)
            {
                system.debug('\n\n\n-------' +  thisAccount.Name + ' vs ' + thisLead.Company);

                if(thisAccount.Name == thisLead.Company)
                {
                    system.debug('\n\n\n------- Match Found on company name! Setting lead convert account id');
                    thisConvert.setAccountId(thisAccount.Id);
                    foundMatchingAccount = true;
                    break;
                }
            }

            if(!foundMatchingAccount)
            {
                system.debug('\n\n\n------- No matching account could be found for lead with name: ' + thisLead.Company + ' ID: ' + thisLead.Id + '. A new account will be created for it');
            }
            //at this point whether a match was found or not the lead needs to be setup to be queued. If no match was found the system will just create an account for it.
            leadConverts.add(thisConvert);
        }

        system.debug('\n\n\n------- Converting ' + leadConverts.size() + ' Leads!');
        return Database.convertLead(leadConverts);

    }

	/**
    *@Description Method to check for account key contact, if none, then update the contact as a key contact
    *@Param Map of converted account ids and associated new contact ids
    *@Return None.  Process will update contact's key contact flag if there's no key contact associated to the account
    **/
    private void updateAccountKeyContact(map<Id, Id> accountContactMap)
    {
        system.debug('\n\n\n------- updateKeyContact');
        if(accountContactMap!=null && accountContactMap.size()>0) {
        	system.debug('account ids: ' + accountContactMap.keySet());
        	set<Id> acctsWithKeyContact = new set<Id>();
        	for(Contact c : [select Id, AccountId from Contact where Key_Contact__c=true and AccountId = :accountContactMap.keySet()])
        		acctsWithKeyContact.add(c.AccountId);
        		
        	list<Contact> contactsToUpdate = new list<Contact>();
        	for(Id acctId : accountContactMap.keySet())
        		if(!acctsWithKeyContact.contains(acctId))
        			contactsToUpdate.add(new Contact(Id=accountContactMap.get(acctId), Key_Contact__c=true));
        			
        	system.debug('contacts to be updated:' + contactsToUpdate);
        	if(contactsToUpdate.size() > 0)
        		update contactsToUpdate;
        }
    }
    
    /**
    *@Description Method that creates an account from a lead. Used for duplicate detection. Accounts are not inserted. All fields that have an exact name match on both account and lead objects
    *             will attempt to be populated by data from the lead. Errors will be silently caught so if a value is not being filled and you don't see an error that is why. Check data types, truncation lengths, etc.
    *@Param leads list of existing leads from which to generate accounts.
    *@Return map of lead ids to account data.
    **/
    private map<Id,list<Account>> createAccountFromLead(list<Lead> leads)
    {
        system.debug('\n\n\n------- createAccountFromLead');

        map<Id,list<Account>> accounts = new map<Id,list<Account>>();

        for(Lead thisLead : leads)
        {
            list<Account> tempAccounts = new list<Account>();

            string parsedWebsite = parseWebsiteDomain(thisLead.website);
            string parsedEmail = parseEmailDomain(thisLead.email);

            Account websiteDomainAccount1 = new Account();
            websiteDomainAccount1.Website_Domain__c = parsedWebsite;
            websiteDomainAccount1.Name = thisLead.Company;
            websiteDomainAccount1.RecordTypeId = prospectRTId;
            Account websiteDomainAccount2 = new Account();
            websiteDomainAccount2.Website_Domain__c = parsedWebsite;
            websiteDomainAccount2.Name = thisLead.Company;
            websiteDomainAccount2.RecordTypeId = portCoRTId;
    
            Account emailDomainAccount1 = new Account();
            emailDomainAccount1.Website_Domain__c = parsedEmail;
            emailDomainAccount1.Name = thisLead.Company;
            emailDomainAccount1.RecordTypeId = prospectRTId;
            Account emailDomainAccount2 = new Account();
            emailDomainAccount2.Website_Domain__c = parsedEmail;
            emailDomainAccount2.Name = thisLead.Company;
            emailDomainAccount2.RecordTypeId = portCoRTId;

            //if the email and website are not the same we want to make temp accounts to match them both (only if they are not null)
            //otherwise if the website isn't null them create an account for that
            //if the parsed website is null and the parsed email isn't then add that.
            //basically we want one entry for each unique domain/email that isn't null.
            if(parsedWebsite != parsedEmail)
            {

                if(parsedWebsite != null && parsedWebsite!='')
                {
                    tempAccounts.add(websiteDomainAccount1); 
                    tempAccounts.add(websiteDomainAccount2);   
                } 
                if(parsedEmail != null && parsedEmail!='')
                {
                    tempAccounts.add(emailDomainAccount1);
                    tempAccounts.add(emailDomainAccount2);   
                } 
            }
            else if(parsedWebsite != null&&parsedWebsite != '')
            {
                tempAccounts.add(websiteDomainAccount1);
                tempAccounts.add(websiteDomainAccount2);
            }
            else if(parsedEmail != null&&parsedEmail != '')
            {
                tempAccounts.add(emailDomainAccount1);
                tempAccounts.add(emailDomainAccount2);
            }
            accounts.put(thisLead.Id,tempAccounts);
        }

        return accounts;
    }

    /**
    *@Description Extracts the domain name from a provided string by removing protocols and subdomains from the start of the url. EX http://www.google.com return google.com
    *@Param website a URL to parse and return just the TLD (top level domain) of
    *@Return a string that is the top level domain of the provided string.
    **/
    private static string parseWebsiteDomain(string website)
    {
        system.debug('\n\n\n----- Attempting to parse website string: ' + website);
        if(website == null || website == '') return '';
        System.Url websiteURL = new System.Url( ( website.startsWithIgnoreCase( 'http' ) ? '' : 'http://' ) + website );
        String hostURL = websiteURL.getHost();
        //Strip out www.
        return hostURL.toLowerCase().remove( 'www.' );
    }

    /**
    *@Description Extracts the domain name from a provided string by removing any content before and including the @ symbol. Used to get domain names from email address
    *@Param email to parse and return domain of. Returns null if none found.
    *@Return a string that is the domain of the provided string.
    **/
    public static string parseEmailDomain(string emailAddr)
    {
        emailAddr = emailAddr != null ? emailAddr : '';
        string[] domainParts = emailAddr.split('@');
        return domainParts.size() == 2 ? domainParts[1].toLowerCase() : null;
    }

    public class customException extends Exception {}

}