public without sharing class EmailParser {
	public EmailParser() {
		
	}

	public static String getEmailParser(String emailSubject,String description)
	{
		//System.debug(tList[0].Description);
		List<String> seperations = new List<String>();
		if(description!=null&&description.contains('\n'))
		{
			seperations = description.split('\n');
		}
		else if(description!=null)
		{
			seperations.add(description);
		}
		Integer currentSegment = 0;
		Map<Integer,List<String>> segmentMap = new Map<Integer,List<String>>();
		for(String s:seperations)
		{
			Boolean goodSeperation = true;
			System.debug('Seperation=='+s);
			if(!s.contains('-- \n')
				&&!s.contains('--\n')
				&&!s.contains('-----Original Message-----')
				&&!s.contains('________________________________')
				&&!s.contains('Sent from my iPhone')
				&&s!=''
				&&s!=' '
				&&s!=' \r'
				&&s!=' \R'
				&&s!=' \n'
				&&s!=' \N'
				&&s!='\r '
				&&s!='\R '
				&&s!='\n '
				&&s!='\N '
				&&s!='\n'
				&&s!='\r'
				&&s!='\n '
				&&s!='\r '
				&&s!='> \r'
				&&s!='>> \r'
				&&s!='>> \r'
				&&s!='>>> \r'
				&&s!='>>>> \r'
				&&s!='>>>> \r'
				&&s!='>  \r'
				&&s!='>>  \r'
				&&s!='>>  \r'
				&&s!='>>>  \r'
				&&s!='>>>>  \r'
				&&s!='>>>>  \r'
				&&s!='> > \r'
				&&s!='> >  \r'
				&&s!='//\r'
				&&s.length()>1
				)
			{

				if(s.length()>7&&s.substring(0,7)== '<https:')
				{
					goodSeperation = false;
				}
				if(s.length()>1&&s.substring(0,1)== '\r' &&s.length()<3)
				{
					goodSeperation = false;
				}
				s=s.remove('\r').trim();
				if(s.length()==0)
				{
					goodSeperation = false;
				}
				if(goodSeperation)
				{
					if(s.length()>2&&s.substring(0,2)=='> ')
					{
						String strippedString = s.substring(2,s.length());
						currentSegment = 1;
						if(segmentMap.get(currentSegment)==null)
						{
							List<String> emailStringList = new List<String>();
							emailStringList.add(strippedString);
							segmentMap.put(currentSegment,emailStringList);
						}
						else
						{
							List<String> emailStringList = segmentMap.get(currentSegment);
							emailStringList.add(strippedString);
							segmentMap.put(currentSegment,emailStringList);
						}
					}
					else
					{
						if(segmentMap.get(currentSegment)==null)
						{
							List<String> emailStringList = new List<String>();
							emailStringList.add(s);
							segmentMap.put(currentSegment,emailStringList);
						}
						else
						{
							List<String> emailStringList = segmentMap.get(currentSegment);
							emailStringList.add(s);
							segmentMap.put(currentSegment,emailStringList);
						}
					}
				}
			}
		}
		String finalEmailBody = '';
		EmailParser_Killswitch__c killswitchCS = EmailParser_Killswitch__c.getOrgDefaults();
		String fromDelimiters = killswitchCS.EmailSegmentDelimiters__c;
        List<String> fromDelimitersList = new List<String>();
        if(String.isNotBlank(fromDelimiters))
			fromDelimitersList = fromDelimiters.split('~');
		if(segmentMap.get(0)!=null)
		{
			String replyChainString = '';
			Integer x = 0;
			for(String s :segmentMap.get(0))
			{
				System.debug('String=='+s);
				s+='\r';
				
				if((!emailSubject.contains(' Declined:')
					&&!emailSubject.contains(' Canceled:')
					&&!emailSubject.contains(' Accepted:')
					//Spanish
					&&!emailSubject.contains(' Rechazado:')
					&&!emailSubject.contains(' Cancelado:')
					&&!emailSubject.contains(' Aceptado:')
					//French
					&&!emailSubject.contains(' Diminué:')
					&&!emailSubject.contains(' Annulé:')
					&&!emailSubject.contains(' Accepté:')
					//German
					&&!emailSubject.contains(' Abgelehnt:')
					&&!emailSubject.contains(' Abgebrochen:')
					&&!emailSubject.contains(' Akzeptiert:')
					//Portugese
					&&!emailSubject.contains(' Recusado:')
					&&!emailSubject.contains(' Cancelado:')
					&&!emailSubject.contains(' Aceitaram:')
					//Italien
					&&!emailSubject.contains(' rifiutato:')
					&&!emailSubject.contains(' Annullato:')
					&&!emailSubject.contains(' Accettato:')
					))
				{
					Boolean breakFor = false;
					for(String split:fromDelimitersList)
					{
						if(x>4&&s.contains(split+':'))
						{
							breakFor = true;
						}
					}
					if(breakFor)
					{
						break;
					}
				}
				x++;
				replyChainString+=s + '\n';


			}
			//List<String> replies = replyChainString.split('wrote:');
			finalEmailBody+=replyChainString;
			
		}
		
		return finalEmailBody;
		
	}
}