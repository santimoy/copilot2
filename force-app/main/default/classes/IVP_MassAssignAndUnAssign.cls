/**
 * @Name IVP_MassAssignAndUnAssign
 * @Developer 10k Advisor
 * @Description Controller for Company Mass Assignment to logged in user!
*/
public class IVP_MassAssignAndUnAssign {
    
    public List<Account> selectedAccounts {get; set;}
    public List<Account> lstAccountWhichAreSelected {get; set;}
    public Set<Id> setAccountIds {get;set;}
    public Integer countOfAccountsSelected {get; set;}
    public List<SelectOption> listLossDropReason {get; set;}
    public String selectedLossDropReason {get; set;}
    public String lossReason {get; set;}
    
    public boolean isBatchRun{set; get;}
    
    public IVP_MassAssignAndUnAssign(ApexPages.StandardSetController controller){
        
        lstAccountWhichAreSelected = new List<Account>();
        selectedAccounts = new List<Account>();
        listLossDropReason = new List<SelectOption>();
        lossReason = '';
        isBatchRun = false;
        
        Schema.DescribeFieldResult fieldResult = Account.Loss_Drop_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        listLossDropReason.add(new SelectOption('None','None'));
        selectedLossDropReason = 'None';
        
        for( Schema.PicklistEntry f : ple){
            listLossDropReason.add(new SelectOption(f.getLabel(), f.getValue()));
        }    
        
        if(((List<Account>)controller.getSelected()).size() == 0){
            countOfAccountsSelected=0;
        }else{
            lstAccountWhichAreSelected= (List<Account>)controller.getSelected();
            setAccountIds = new Map<Id,Account> (lstAccountWhichAreSelected).keySet();
            //setAccountIds = IVP_MassAssignAndUnAssign.getUnlockAccounts( ((Map<Id,Account>)new Map<Id,Account>(lstAccountWhichAreSelected)).keySet());
        }
        countOfAccountsSelected = lstAccountWhichAreSelected.size();
    }
    
    public void scheduleForMassAssign(){
        if(countOfAccountsSelected!=0 && !isBatchRun){
            Database.executeBatch(new IVP_MassAssignUnAssignBatch(true,setAccountIds,'',''),200); 
            isBatchRun = true;
        }
    }
    
    public void scheduleForMassUnassign(){
		if(countOfAccountsSelected!=0 && !isBatchRun){
            Database.executeBatch(new IVP_MassAssignUnAssignBatch(false,setAccountIds,selectedLossDropReason,lossReason),200); 
            isBatchRun = true;
        }
    }
    
    public static Set<Id> getUnlockAccounts(Set<Id> setAccountIds){
        List<ProcessInstance> listProcessInstance = [SELECT Id,TargetObjectId,Status FROM ProcessInstance WHERE TargetObjectId IN :setAccountIds AND Status = 'Pending'];
        if(!listProcessInstance.isEmpty()){
            for(ProcessInstance processInstanceObj : listProcessInstance){
                setAccountIds.remove(processInstanceObj.TargetObjectId);
            }
        }        
        return setAccountIds;
    }
}