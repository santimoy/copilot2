/**
 * Created by Ranjeet Kumar on 11/5/2017.
 */

public with sharing class ResourceHoursTriggerHelper {

    public static void HandleBeforeInsert(List<Resource_Hours__c> ResourceHoursList){
        Boolean IsDuplicate = ResourceHoursTriggerHelper.CheckForDuplicates(ResourceHoursList);
        if(IsDuplicate){
            ResourceHoursList[0].addError('Duplicate Resource Hour record is not allowed!');
        }
    }

    public static Boolean CheckForDuplicates(List<Resource_Hours__c> ResourceHoursList){
        Boolean IsDuplicate = false;
        if(ResourceHoursList.size() > 0){
            Set<Id> ProjectHourIds = new Set<Id>();
            Set<Id> ResourceIds = new Set<Id>();
            for(Resource_Hours__c ResHour : ResourceHoursList){
                ProjectHourIds.add(ResHour.Project_Hours__c);
                ResourceIds.add(ResHour.Project_Resource__c);
            }
            List<Resource_Hours__c> ExistingResourceHourList = new List<Resource_Hours__c>();
            ExistingResourceHourList = [Select Id From Resource_Hours__c Where Project_Hours__c In : ProjectHourIds AND Project_Resource__c In : ResourceIds];
            if(ExistingResourceHourList.size() > 0){
                IsDuplicate = true;
            }
        }
        return IsDuplicate;
    }

    public static void UpdateResourceNameOnProjectHours(List<Resource_Hours__c> ResourceHoursList){
        if(ResourceHoursList != NULL && ResourceHoursList.size() > 0){
            Set<Id> ProjectHrIds = new Set<Id>();
            for(Resource_Hours__c ResHr : ResourceHoursList){
                if(ResHr.Project_Hours__c != NULL){
                    ProjectHrIds.add(ResHr.Project_Hours__c);
                }
            }
            system.debug('>>>>> ProjectHrIds Size--> '+ProjectHrIds.size());
            if(ProjectHrIds.size() > 0){
                ResourceHoursTriggerHelper.MatchProjectResourceName(ProjectHrIds);
            }
        }
    }

    public static void MatchProjectResourceName(Set<Id> ProjectHrIds){
        if(ProjectHrIds != NULL && ProjectHrIds.size() > 0){
            List<Resource_Hours__c> ResourceHoursList = new List<Resource_Hours__c>();
            ResourceHoursList = [Select Id, Project_Hours__c, Project_Resource__c, Project_Resource__r.Name From Resource_Hours__c Where Project_Hours__c In : ProjectHrIds ORDER BY Project_Resource__r.Name ASC];
            system.debug('>>>>> ResourceHoursList size--> '+ResourceHoursList.size());
            if(ResourceHoursList.size() > 0){
                Map<Id, String> ProjectHrVsResourceNames = new Map<Id, String>();
                for(Resource_Hours__c ResHr : ResourceHoursList){
                    if(ResHr.Project_Resource__c != NULL){
                        if(!ProjectHrVsResourceNames.containsKey(ResHr.Project_Hours__c)){
                            ProjectHrVsResourceNames.put(ResHr.Project_Hours__c,'');
                        }
                        String ResourceNames = ProjectHrVsResourceNames.get(ResHr.Project_Hours__c);
                        system.debug('>>>>> Before ResourceNames --> '+ResourceNames);
                        if(!ResourceNames.containsIgnoreCase(ResHr.Project_Resource__r.Name)){
                            ResourceNames += ResHr.Project_Resource__r.Name + ', ';
                        }
                        system.debug('>>>>> After ResourceNames --> '+ResourceNames);
                        ProjectHrVsResourceNames.put(ResHr.Project_Hours__c,ResourceNames);
                    }
                }
                List<Project_Hours__c> ProjectHrList = new List<Project_Hours__c>();
                ProjectHrList = [Select Id, Project_Resources__c From Project_Hours__c Where Id In : ProjectHrIds];
                if(ProjectHrList.size() > 0){
                    for(Project_Hours__c ProjectHr : ProjectHrList){
                        ProjectHr.Project_Resources__c = '';
                        if(ProjectHrVsResourceNames.containsKey(ProjectHr.Id)){
                            ProjectHr.Project_Resources__c = ProjectHrVsResourceNames.get(ProjectHr.Id).removeEnd(', ');
                        }
                    }
                    system.debug('>>>>> ProjectHrList Size --> '+ProjectHrList.size());
                    update ProjectHrList ;
                }
            }
        }
    }
}