global class Batch_ActivityEmailParserScheduler implements Schedulable{
    
    public Integer batchSize;
    public static void scheduleMe(Integer batchSizeParam)
    {
    	String cron = '0 0 * * * ?'; //Cron for schedule the job for every hour
        System.schedule('ActivityEmailParser Every Hour', cron, new Batch_ActivityEmailParserScheduler(batchSizeParam));
       
    }
    public Batch_ActivityEmailParserScheduler(Integer batchSizeParam)
    {
    	if(batchSizeParam!=null)
        {
        	batchSize = batchSizeParam;
        }
        else
        {
        	batchSize=1;
        }
    }
    global void execute(SchedulableContext sc) {
        Batch_EmailParser  btch = new Batch_EmailParser(null,null,null,null,true); 
        Database.executebatch(btch,batchSize);
    }
}