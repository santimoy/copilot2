@isTest
public class IVP_RecentlyUnassigned_Test {
    @testSetup static void testData(){
        List<Account> lstAccount= new List<Account>();
        for(Integer i=0 ;i <1;i++)
        {
            Account acc = new Account();
            acc.Name ='Name'+i;
            lstAccount.add(acc);
        }
        
        insert lstAccount;
        
        List<Recently_Unassigned__C> lstRUS= new List<Recently_Unassigned__C>();
        for(Integer i=0 ;i <2;i++)
        {
            Recently_Unassigned__C rec = new Recently_Unassigned__C();
            rec.company__c = lstAccount.get(0).Id;
            rec.old_owner__c = UserInfo.getName();
            rec.Changed_Date_Time__c = System.now();
            lstRUS.add(rec);
        }
        insert lstRUS;
    }
    static void testBatch() 
    {
        List<Account> lstAcc = [SELECT Unassigned_Users__c FROM Account];
        System.assertEquals(Null,lstAcc[0].Unassigned_Users__c);
        
        Test.startTest();
        
        IVP_RecentlyUnassigned_Batch p = new IVP_RecentlyUnassigned_Batch();
        Database.executeBatch(p); 
        
        Test.stopTest();
        
        lstAcc = [SELECT Unassigned_Users__c FROM Account];
        System.assertEquals(UserInfo.getUserId().substring(0, 15),lstAcc[0].Unassigned_Users__c);
    }
    static testMethod void testScheduler() {
        Test.startTest();
        
        IVP_RecentlyUnassigned_Scheduler scheduler = new IVP_RecentlyUnassigned_Scheduler();
        String cron = '0 0 23 * * ?'; 
        String jobId = system.schedule('Schedule Recently unassigned batch', cron, scheduler);
        Test.stopTest();
        
        List<Account> lstAcc = [SELECT Unassigned_Users__c FROM Account];
        system.assertEquals(1, [SELECT count() FROM CronTrigger where Id = :jobId],'A job should be scheduled');
    }
}