public with sharing class ActivityHistoryPOC {
    public ActivityHistoryPOC() {

    }

    @AuraEnabled
    public static void SelectTalentTasksForAccount(Id AccountId){
        String queryString = 'SELECT Id, Name,';
        
        // SOQL security requirements: No WHERE clause, LIMIT 500, fixed ORDER BY
        // Filters and offset are performed post SOQL query
        
        // OpenActivities
        queryString += ' ( SELECT Id, AccountId, AlternateDetailId, ActivityDate, ActivitySubtype, ActivityType, Description, EndDateTime, IsTask, OwnerId, Owner.Name, PrimaryAccountId, PrimaryWho.Name, PrimaryWhoId, ReminderDateTime, StartDateTime, Status, Subject, WhatId, WhoId, Type__c ' +
            'FROM OpenActivities ORDER BY ActivityDate ASC NULLS FIRST, LastModifiedDate DESC LIMIT 500 ), ';
	
        // ActivityHistories
        queryString += ' ( SELECT Id, AccountId, AlternateDetailId, ActivityDate, ActivitySubtype, ActivityType, Description, EndDateTime, OwnerId, IsTask, Owner.Name, PrimaryAccountId, PrimaryWho.Name, PrimaryWhoId, ReminderDateTime, StartDateTime, Status, Subject, WhatId, WhoId, Type__c ' +
            'FROM ActivityHistories '; 
        queryString+='  ORDER BY ActivityDate DESC NULLS FIRST, LastModifiedDate DESC LIMIT 500) ';
        queryString += ' FROM Account WHERE Id = \'' + AccountId + '\'';
        
        System.debug( 'queryString : ' + queryString );
 
        List<Account> acctList = Database.Query(queryString);

        List<sObject> taskList = new List<sObject>();
        List<Id> idList = new List<Id>();
        for(sObject task : acctList[0].getSObjects('ActivityHistories')){
            taskList.add(task);
            idList.add(task.Id);
        }

        String profileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()][0].Name;
        List<Activity_Timeline_Team__mdt> activityTeamProfileList = [SELECT Team_Profile_Name__c FROM Activity_Timeline_Team__mdt WHERE Profile_Name__c = :profileName];
        List<String> teamProfileNameList = new List<String>();
        for (Activity_Timeline_Team__mdt activityTeamProfile : activityTeamProfileList){
            teamProfileNameList.add(activityTeamProfile.Team_Profile_Name__c);
        }
        Map<Id, Profile> teamProfileIdMap = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name IN :teamProfileNameList OR Name = :profileName]);
        Map<Id, User> teamUserMap = new Map<Id, User>([SELECT Id FROM User WHERE Profile.Id IN :teamProfileIdMap.keyset()]); 

        //Map<Id, User> mapMyTeamUserIds = new Map<Id, User>([SELECT Id FROM User WHERE Id = :UserInfo.getUserId() OR ManagerId = :UserInfo.getUserId()]);

        Map<Id, Task> mapMyTeamTasks = new Map<Id, Task>([SELECT Id, RecordType.Name FROM Task WHERE OwnerId IN :teamUserMap.keyset()]);

        for(sObject task : taskList){
            if(mapMyTeamTasks.get(task.Id) == null){
                system.debug('Task: ' + task.Id + ' is not owned by My Team');
            }
            else {
                system.debug('Task: ' + task.Id + ' is owned by My Team');
            }
        }

        Map<Id, Task> mapTalentTasks = new Map<Id, Task>([SELECT Id, RecordType.Name FROM Task WHERE RecordType.Name = 'Talent' AND Id IN :idList]);

        for(sObject task : taskList){
            if(mapTalentTasks.get(task.Id) == null){
                system.debug('Task: ' + task.Id + ' is not a Talent Task');
            }
            else {
                system.debug('Task: ' + task.Id + ' is a Talent Task');
            }
        }
    }

}