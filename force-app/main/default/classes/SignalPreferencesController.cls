public class SignalPreferencesController {
    @AuraEnabled public Intelligence_User_Preferences__c userPrefObj ;
    @AuraEnabled public Map<String, List<Signal_Category__c>> signalCategoryList ;
    
    @AuraEnabled
    public static SignalPreferencesController fetchSignals() {
        SignalPreferencesController singnalPrefControllerObj = new SignalPreferencesController();
        singnalPrefControllerObj.userPrefObj = [Select Id, name, User_Id__c, Sourcing_Signal_Category_Preferences__c, My_Dash_Signal_Preferences__c from Intelligence_User_Preferences__c Where User_Id__c =: UserInfo.getUserId() LIMIT 1];
        List<Signal_Category__c> signalCategoryList = [Select Id, Name, External_Id__c, Group__c ,Description__c from Signal_Category__c where Do_not_display__c = false ORDER BY Name];
        singnalPrefControllerObj.signalCategoryList = new Map<String, List<Signal_Category__c>>();
        if(signalCategoryList != null && signalCategoryList.size() > 0 ){
            for(Signal_Category__c sc : signalCategoryList) {
                List<Signal_Category__c> scList = new List<Signal_Category__c>();
                if(sc.Group__c != null && singnalPrefControllerObj.signalCategoryList.get(sc.Group__c) != null) {
                    scList.addAll(singnalPrefControllerObj.signalCategoryList.get(sc.Group__c));
                }
                scList.add(sc);
                singnalPrefControllerObj.signalCategoryList.put(sc.Group__c, scList);
            }
        }
        
        return singnalPrefControllerObj;
    }
    
    @AuraEnabled
    public static Boolean saveUserPreference(Intelligence_User_Preferences__c userPrefObj, String cmpTypeObj) {
        if(userPrefObj != null) {
            List<Signal_Category__c> signalCategoryList = [Select Id, Name, External_Id__c, Group__c ,Description__c from Signal_Category__c where Do_not_display__c = true ORDER BY Name];
            if(!signalCategoryList.isEmpty()) {
                String sourcingSignalPreferencesStr;
                String sourcingSignalDashPreferencesStr;
                for (Signal_Category__c singleCategory : signalCategoryList) {
                    if (singleCategory.External_Id__c != null) {
                        if(cmpTypeObj == 'Sourcing_Signal_Preferences') {
                            if(!userPrefObj.Sourcing_Signal_Category_Preferences__c.contains(singleCategory.External_Id__c)) {
                                if(sourcingSignalPreferencesStr != null) {
                                    sourcingSignalPreferencesStr =  sourcingSignalPreferencesStr + ', ' + singleCategory.External_Id__c;
                                } else {
                                    sourcingSignalPreferencesStr =  singleCategory.External_Id__c;
                                }
                            }
                        }
                        
                        if(cmpTypeObj == 'My_Smart_Dash_Preferences') {
                            if(!userPrefObj.My_Dash_Signal_Preferences__c.contains(singleCategory.External_Id__c)) {
                                if(sourcingSignalDashPreferencesStr != null) {
                                    sourcingSignalDashPreferencesStr =  sourcingSignalDashPreferencesStr + ', ' + singleCategory.External_Id__c;
                                } else {
                                    sourcingSignalDashPreferencesStr =  singleCategory.External_Id__c;
                                }
                            }
                        }
                    }
                }
                if(sourcingSignalPreferencesStr != null) {
                    if(String.isNotEmpty(userPrefObj.Sourcing_Signal_Category_Preferences__c)) {
                        userPrefObj.Sourcing_Signal_Category_Preferences__c = sourcingSignalPreferencesStr + ', ' + userPrefObj.Sourcing_Signal_Category_Preferences__c;
                    } else {
                        userPrefObj.Sourcing_Signal_Category_Preferences__c = sourcingSignalPreferencesStr;
                    }
                }
                
                if(sourcingSignalDashPreferencesStr != null) {
                    if(String.isNotEmpty(userPrefObj.My_Dash_Signal_Preferences__c)) {
                        userPrefObj.My_Dash_Signal_Preferences__c = sourcingSignalDashPreferencesStr + ', ' + userPrefObj.My_Dash_Signal_Preferences__c;
                    } else {
                        userPrefObj.My_Dash_Signal_Preferences__c = sourcingSignalDashPreferencesStr;
                    }
                }
            }
            update userPrefObj;
            return true;
        } else {
            return false;
        }
    }
}