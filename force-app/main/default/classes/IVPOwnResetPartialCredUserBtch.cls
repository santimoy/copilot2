/**
 *@name : IVPOwnResetPartialCredUserBtch
 *@description : Resetting Partial Credit User field, if no interaction is being done in last n days
**/
public class IVPOwnResetPartialCredUserBtch implements Database.Batchable<sObject>, Schedulable{
    
    public void execute(SchedulableContext sc){
        Logger.push('Schedulable.execute','IVPOwnResetPartialCredUserBtch');
        Database.executeBatch(new IVPOwnResetPartialCredUserBtch());
        Logger.pop();
    }
    public Database.QueryLocator start(Database.BatchableContext BC){
        //getting OwnerShipPreventDaysCount if not found then using n days
        Integer daysCountOfReset = IVPOwnershipService.partialOwnerLastInterationDays;
        Map<String, IVPOwnershipBusinessService.BusinessDaysWrapper> 
            businessDateMap = new Map<String, IVPOwnershipBusinessService.BusinessDaysWrapper>();
        businessDateMap.put('now', new IVPOwnershipBusinessService.BusinessDaysWrapper(System.now(), -daysCountOfReset));
        
        IVPOwnershipBusinessService.prepareWorkingDays(businessDateMap.values());
        Datetime last6MonthDateTime = businessDateMap.get('now').businessDate;
        String queryStr = 'SELECT Id FROM Account WHERE Partial_Credit_User_Changed_On__c <=: last6MonthDateTime AND Partial_Credit_User__c !=NULL';
        return Database.getQueryLocator(queryStr);
    }
    
    public void execute(Database.BatchableContext BC, List<Account> batchList){
        Logger.push('Batchable.execute','IVPOwnResetPartialCredUserBtch');
        processRecords(batchList);
        Logger.pop();
    }
    
    public static void processRecords(List<Account> accounts){
        Logger.push('processRecords','IVPOwnResetPartialCredUserBtch');
        for(Account acc : accounts){
            acc.Partial_Credit_User__c = NULL;
            acc.Partial_Credit_User_Changed_On__c = NULL;
        }
        Boolean gotError = false;
        try{
            update accounts;
        }catch(Exception excp ){
            gotError = true;
            Logger.debugException(excp);
        }
        if(gotError){
            Logger.pop();
        }
        
    }
    public void finish(Database.BatchableContext BC){
        
    }

}