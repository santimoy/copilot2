/**
 * @Name IVP_CompanyTaggingCtrlTest
 * @Developer 10k Advisor
 * @Description Test class for Tagging
*/
@isTest
private class IVP_CompanyTaggingCtrlTest {

    @testSetup static void setupData() {
        List<Account> listAccount = new List<Account>();
        listAccount.add(new Account(Name='Test-Account-1'));
        listAccount.add(new Account(Name='Test-Account-1'));
        
        insert listAccount;
        
    }
	private static testMethod void testGetTopicsList() {
        Test.startTest();
        
        List<Account> listAccount = [Select Id,Tags__c from Account];
        String accountIdsString = getCommaSeparatedAccountIds(listAccount);
        
        Topic newTopic1 = new Topic(Name='TestTopic1');
        insert newTopic1;
       
        IVP_CompanyTaggingCtrl.addTagToSelectedAccounts(accountIdsString,newTopic1);
        IVPTestFuel tFuel=new IVPTestFuel();
        List<Account> lstAccounts=tFuel.companies;
        String strToGetTopicList=''+lstAccounts[0].Id +','+lstAccounts[1].Id;
        Map<String,Object> mapListTopics = IVP_CompanyTaggingCtrl.getTopicsList(strToGetTopicList);
        
        Test.stopTest();
	    System.assertEquals(1,((List<Topic>) mapListTopics.get('lstOfTopics')).size());
	    
	}
	private static testMethod void testInsertNewTopic() {
	    Test.startTest();
	    List<Account> listAccount = [Select Id,Tags__c from Account];
        System.debug('In-Test'+listAccount);
        String accountIdsString = getCommaSeparatedAccountIds(listAccount);
        System.debug('In-Test accountIdsString'+accountIdsString);
	    String returnStatus = IVP_CompanyTaggingCtrl.insertNewTopic('TestTopic',accountIdsString);
	  Map<String,String> mapOfstring=(map<String,String>)System.JSON.deserialize(returnStatus, Map<String,String>.class);
        Test.stopTest();
        system.assertEquals('true',mapOfstring.get('isSuccess'));
	}
    
    private static testmethod void testAddTagToSelectedAccounts(){
        Test.startTest();
        List<Account> listAccount = [Select Id,Tags__c from Account];
        String accountIdsString = getCommaSeparatedAccountIds(listAccount);
        
        Topic newTopic1 = new Topic(Name='TestTopic1');
        insert newTopic1;
    
        Topic newTopic2 = new Topic(Name='TestTopic2');
        insert newTopic2;
        
        Map<String,String> addTagResultForNew = ((Map<String,String>) Json.deserialize(IVP_CompanyTaggingCtrl.addTagToSelectedAccounts(accountIdsString,newTopic1),Map<String,String>.class )) ;
        Map<String,String> addTagResultForAlready = ((Map<String,String>) Json.deserialize(IVP_CompanyTaggingCtrl.addTagToSelectedAccounts(accountIdsString,newTopic2),Map<String,String>.class )) ;
        
        Test.stopTest();
        System.assertEquals('true',addTagResultForNew.get('isSuccess'));
        System.assertEquals('true',addTagResultForAlready.get('isSuccess'));
        
    }
    private static testmethod void testUnTagAccounts(){
        Test.startTest();
        List<Account> listAccount = [Select Id,Tags__c from Account];
        String accountIdsString = getCommaSeparatedAccountIds(listAccount);
        
        Topic newTopic1 = new Topic(Name='TestTopic1');
        insert newTopic1;
        Topic newTopic2 = new Topic(Name='TestTopic2');
        insert newTopic2;
        IVP_CompanyTaggingCtrl.addTagToSelectedAccounts(accountIdsString,newTopic1);
        List<IVP_CompanyTaggingCtrl.TagResponse> ObjJson=new List<IVP_CompanyTaggingCtrl.TagResponse>();
        List<Topic> tp=[select id,name from Topic];
        IVP_CompanyTaggingCtrl.TagResponse obj1=new IVP_CompanyTaggingCtrl.TagResponse();
        obj1.topicId=newTopic1.id;
        obj1.topicName=newTopic1.name;
        IVP_CompanyTaggingCtrl.TagResponse obj2=new IVP_CompanyTaggingCtrl.TagResponse();
        obj2.topicId=newTopic2.id;
        obj2.topicName=newTopic2.name;
        ObjJson.add(obj1);
        ObjJson.add(obj2);
            String JSONString = JSON.serialize(ObjJson);
        String returnMessage =  IVP_CompanyTaggingCtrl.unTagAccounts(JSONString,accountIdsString);
        Test.stopTest();
	  Map<String,String> mapOfstring=(map<String,String>)System.JSON.deserialize(returnMessage, Map<String,String>.class);
       System.assertEquals('true',mapOfstring.get('isSuccess'));
    }
    
    
	private static String getCommaSeparatedAccountIds(List<Account> listAccount){
	    List<String> acctIds = new List<String> ();
        for(Account acctObj : listAccount){
            acctIds.add(acctObj.Id);
        }
        String accountIdsString = String.join(acctIds, ',');
        return accountIdsString;
	}


}