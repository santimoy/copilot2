/***********************************************************************************
* @author      10k-Expert
* @name		IVPBusinessHoursWrapperTest
* @date		2019-01-22
* @description	To test IVPBusinessHoursWrapper
***********************************************************************************/
@isTest
public class IVPBusinessHoursWrapperTest {
    @isTest 
    public static void calculateWorkingDayTest()
    {
        List<RecordType> rType = [SELECT Id FROM RecordType WHERE sObjectType='Account' And DeveloperName = 'Prospect' limit 1];
        Account acc = new Account(Name='Test Account', Ownership_Status__c='Watch', RecordTypeId=rType[0].Id);
        insert acc;
        
        Account acObj = [SELECT Id, OwnerId, Ownership_Status__c, RecordTypeId FROM Account Where Id =: acc.Id];
        
        acObj.OwnerId = UserInfo.getUserId();
        acObj.Ownership_Status__c = 'Priority';
        
        Test.startTest();
        
        	update acObj;
        	IVPBusinessHoursWrapper wrapObj = IVPBusinessHoursWrapper.getInstance();
        	DateTime dTimeObj = wrapObj.calculateWorkingDay(System.now(), -2);
        Test.stopTest();
        
        System.assert(wrapObj.bhId != null);
        System.assert(dTimeObj < System.now());
    }
    @isTest 
    public static void calculateWorkingDayTest2()
    {
        List<RecordType> rType = [SELECT Id FROM RecordType WHERE sObjectType='Account' And DeveloperName = 'Prospect' limit 1];
        
        Account acc = new Account(Name='Test Account', Ownership_Status__c='Watch', RecordTypeId=rType[0].Id);
        insert acc;
        
        Account acObj = [SELECT Id, OwnerId, Ownership_Status__c, RecordTypeId FROM Account Where Id =: acc.Id];
        
        acObj.OwnerId = UserInfo.getUserId();
        acObj.Ownership_Status__c = 'Priority';
        
        Test.startTest();
        
        	update acObj;
        	IVPBusinessHoursWrapper wrapObj = IVPBusinessHoursWrapper.getInstance();
        	DateTime dTimeObj = wrapObj.calculateWorkingDay(System.now(), 2);
        	
        Test.stopTest();
        System.assert(wrapObj.bhId != null);
        System.assert(dTimeObj > System.now());
    }
}