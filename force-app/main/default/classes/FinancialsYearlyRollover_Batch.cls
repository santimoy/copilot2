/*
*	FinancialsYearlyRollover_Batch class : Contains logic for yearly rolling over on Account Revenue fields
*							Reminder to Schedule this class to execute yearly on Jan 1st:
*							System.Schedule('Financials yearly rollover', '0 1 0 1 1 ?', new FinancialsYearlyRollover_Batch() );
*
*	Author: Wilson Ng
*	Date:   Apr 4, 2018
*
*/
global class  FinancialsYearlyRollover_Batch implements Database.Batchable<sObject>, Schedulable {

	private list<string> inputAccountIds = null;
	
	public FinancialsYearlyRollover_Batch() {
	}
	public FinancialsYearlyRollover_Batch(string inputAcctId) {
		inputAccountIds = new list<string> { inputAcctId };
	}
	public FinancialsYearlyRollover_Batch(list<string> inputAcctIds) {
		inputAccountIds = new list<string>(inputAcctIds);
	}
	
	// ----------------------------------------------------------------------------------------------------------------------------------
	// Batchable Interface methods 
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		// setup the query
		String priorPriorYear = String.valueOf(System.today().year()-2);
		String queryString = 'Select Id, (select Id from Financials__r where Year_Text__c >= :priorPriorYear order by Year_Text__c desc) from Account' +
				((inputAccountIds!=null && inputAccountIds.size()>0) ? ' where Id in :inputAccountIds' : '');

		System.Debug('---queryString:'+queryString);
		return Database.getQueryLocator(queryString);	
	}

	global void execute(Database.BatchableContext BC, List<Account> batchList) {
		
		// blank out account rev fields
		list<Account> accounts = new list<Account>();
		list<Financials__c> financials = new list<Financials__c>();
		for(Account a : batchList) {
			accounts.add(new Account(Id=a.Id, 
							Current_Revenue__c = null, Current_EBITDA__c = null,
							Next_Year_Revenue__c = null, Next_Year_EBITDA__c = null,
							Prior_Year_Revenue__c = null, Prior_Year_EBITDA__c = null,
							PriorPriorRevenue__c = null, PriorPriorEBITDA__c = null,
							Revenue_Est_Source__c = null, Revenue_Estimate__c = null));
			for(Financials__c f : a.Financials__r)
				financials.add(f);
		}
		// update accounts with trigger bypass
		AccountTriggerHandler.BYPASSTRIGGER=true;
		AccountTriggerHandlerWOS.BYPASSTRIGGER=true;
		update accounts;
		
		// update the financials records
		update financials;
	}
	
	global void finish(Database.BatchableContext BC) {
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// methods needed for the Schedulable interface
	global void execute(SchedulableContext sc){
		Database.executebatch(this, 200);
	}
	
	public class FinancialsYearlyRollover_Batch_Exception extends Exception{}
}