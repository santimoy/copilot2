@isTest(SeeAllData = false)
public with sharing class DatatypeConversionTest {
    // public DatatypeConversionTest() {

    // }
    @IsTest
    static void typeCastMethod(){
        Account accObj = new Account();
        User userObj = new User();
        Contact conObj = new Contact();
        Lead leadObj = new Lead();
        Test.startTest();
        Object stringObj = 'test string';
        DatatypeConversion.convertTo(stringObj, 'String', accObj, 'Name');
        DatatypeConversion.convertTo(stringObj, 'picklist', accObj, 'Name');

        Object boolObj = true;
        DatatypeConversion.convertTo(boolObj, 'Boolean', userObj, 'isActive');

        Object dateObj = Date.Today();
        DatatypeConversion.convertTo(dateObj, 'date', conObj, 'Birthdate');

        Object dateTimeObj = System.now();
        DatatypeConversion.convertTo(dateTimeObj, 'datetime', conObj, 'LastCURequestDate');

        Object decimalObj = 1.1;
         DatatypeConversion.convertTo(dateObj, 'decimal', accObj, 'NumberOfEmployees');

        Object DoubleObj = 1.1;
        DatatypeConversion.convertTo(dateObj, 'double', accObj, 'NumberOfEmployees');

        Object idObj = '0015600000KFotx';
        DatatypeConversion.convertTo(idObj, 'id', accObj, 'id');

        Object integerObj = 1;
        DatatypeConversion.convertTo(integerObj, 'integer', leadObj, 'NumberOfEmployees');

        Object LongObj = 1;
        DatatypeConversion.convertTo(LongObj, 'long', leadObj, 'NumberOfEmployees');

        Object TimeObj = System.now().time();
        DatatypeConversion.convertTo(TimeObj, 'time', leadObj, 'NumberOfEmployees');

        Object textobj = 'string';
        DatatypeConversion.convertTo(textobj, 'text', accObj, 'Name');
		Object objnull = null;
        
        DatatypeConversion.convertTo(objnull, 'text', accObj, 'Name');
        Test.stopTest();
        
    }
}