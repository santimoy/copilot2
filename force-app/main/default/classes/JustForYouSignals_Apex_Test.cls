@IsTest
private class JustForYouSignals_Apex_Test{

    @TestSetup static void testSetup(){
        Account acc = new Account();
        acc.Name  = 'Test Company';
        acc.website = 'www.google.com';
        insert acc;
        
        
        Sourcing_Preference__c spcobj = new Sourcing_Preference__c();
        spcobj.Validation_Status__c = 'valid';
        spcobj.Status__c = 'Active';
        spcobj.Name__c = 'Test Sourcing';
        spcobj.External_Id__c = 'ABC1234568';
        spcobj.Include_Exclude__c = 'Include';
        spcobj.Last_Validated__c = Date.newInstance(2019, 1, 25);
        spcobj.Recent_Validation_Error__c = 'Status Unavailable';
        insert spcobj;

        List<Sourcing_Preference__c> spList = new List<Sourcing_Preference__c>();
        Sourcing_Preference__c sp = new Sourcing_Preference__c();
        sp.Validation_Status__c = 'valid';
        sp.Status__c = 'Active';
        sp.Name__c = 'Test Sourcing';
        sp.Cloned_From__c = spcobj.Id;
        //sp.Owner_Username__c = Userinfo.getName();
        sp.External_Id__c = 'ABC12345';
        sp.Include_Exclude__c = 'Exclude';
        sp.JSON_QueryBuilder__c = '{"condition":"AND","rules":[{"id":"LOWER(ct.companies_cross_data_full.ivp_description)","field":"LOWER(ct.companies_cross_data_full.ivp_description)","type":"string","input":"text","operator":"equal","value":"4444"}],"valid":true}';
        sp.Last_Validated__c = Date.newInstance(2019, 1, 20);
        sp.Recent_Validation_Error__c = 'Status Unavailable';
        spList.add(sp);
        insert spList;

        List<Sourcing_Preference__c> updateList = new List<Sourcing_Preference__c>();
        String getCurrentUserDbCol = 'Testing Purpose';

        for (Sourcing_Preference__c a : spList) {

            Sourcing_Preference__c sourcingPrefRec = a.clone(true, true, true, true);
            sourcingPrefRec.Cloned_From__c = spcobj.Id;
            sourcingPrefRec.External_Id__c = a.External_Id__c;
            sourcingPrefRec.Include_Exclude__c = a.Include_Exclude__c;
            sourcingPrefRec.JSON_QueryBuilder__c = a.JSON_QueryBuilder__c;
            sourcingPrefRec.Last_Validated__c = a.Last_Validated__c;
            sourcingPrefRec.Name__c = a.Name__c;
            sourcingPrefRec.Recent_Validation_Error__c = a.Recent_Validation_Error__c;
            sourcingPrefRec.Status__c = a.Status__c;
            sourcingPrefRec.SQL_Where_Clause__c = a.SQL_Where_Clause__c;
            sourcingPrefRec.Validation_Status__c = a.Validation_Status__c;
            sourcingPrefRec.SQL_Query__c = a.SQL_Query__c;
            sourcingPrefRec.Serialized__c = '';
            string recUrl = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/'+sourcingPrefRec.Id+' ';
            String newSubStringOfDbCol = '';
            String newSqlQuery = sourcingPrefRec.SQL_Query__c;
            updateList.add(sourcingPrefRec);
        }

        if(updateList.size() > 0) {
            update updateList;
        }
        
        List<Just_For_You_Setting__c> jfysList = new List<Just_For_You_Setting__c>();
        Just_For_You_Setting__c jfys = new Just_For_You_Setting__c();
        jfys.Name = 'Test CS2';
        jfys.JFY_Companies_Last_N_Days__c = 2;
        jfysList.add(jfys);
        insert jfysList;

        List<IVP_Call_Params__c> ivpcpList = new List<IVP_Call_Params__c>();
        IVP_Call_Params__c ivpcp = new IVP_Call_Params__c();
        ivpcp.Name = 'justforyou';
        ivpcp.Just_For_You_OrderBy__c = 'Name';
        ivpcpList.add(ivpcp);
        insert ivpcpList;
        List<Intelligence_Field__c> iiFields = new List<Intelligence_Field__c>();
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='all_matched_preferences',
            UI_Label__c='All Matched Preferences', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=false,
            Is_All_Preferences_Static_Field__c=true));
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='most_recent_matched_preference',
            UI_Label__c='Most Recent Matched Preference', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=false,
            Is_All_Preferences_Static_Field__c=true));
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='most_recent_appearance_reason',
            UI_Label__c='Most Recent Matched Reason', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=true,
            Is_All_Preferences_Static_Field__c=true));
        iiFields.add(new Intelligence_Field__c(
            DB_Field_Name__c='most_recent_appearance_date',
            UI_Label__c='Most Recent Matched Date', 
            Active__c=false, Static_Selection__c=false, DB_Column__c='None.None',
            Is_Preference_Filter_Static_Field__c=true,
            Is_All_Preferences_Static_Field__c=true));
        insert iiFields;

    }
    
    @isTest private static void testGetInitData(){
        String jsonInput = '{"viewId":"","isLimit":true}';
        List<Just_For_You_Setting__c> JustForYouSettingList = new List<Just_For_You_Setting__c>();
        Just_For_You_Setting__c  JustForYouSettingObj = new Just_For_You_Setting__c();
        JustForYouSettingObj.Name = 'TestingCustom';
        JustForYouSettingObj.Signals_Last_N_Days__c = 2;
        JustForYouSettingObj.Records_Limit__c =5;
        JustForYouSettingObj.Fields_List__c ='id,sf_id,signal_date,tag_name,headline,snippets,source,url,df_id,website,ct_id';
        JustForYouSettingObj.JFY_Companies_Last_N_Days__c =30;
        JustForYouSettingList.add(JustForYouSettingObj);
        if(JustForYouSettingList.size() > 0){
            insert JustForYouSettingList;
        }

        List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
        Signal_Category__c sgcobj = new Signal_Category__c();
        sgcobj.Name = 'TestingSignal';
        sgcobj.External_Id__c = 'sdfghg65';
        sgcobj.Active__c = true;
        SignalCategoryList.add(sgcobj);
        if(SignalCategoryList.size() > 0){
            insert SignalCategoryList;
        }
        Sourcing_Preference__c spcobj = new Sourcing_Preference__c();
        spcobj.Validation_Status__c = 'valid';
        spcobj.Status__c = 'Active';
        spcobj.Name__c = 'Test Sourcing';
        spcobj.External_Id__c = 'TST1234569';
        spcobj.Include_Exclude__c = 'Include';
        spcobj.Last_Validated__c = Date.newInstance(2019, 1, 25);
        spcobj.Recent_Validation_Error__c = 'Status Unavailable';
        insert spcobj;

        Test.startTest();
        JustForYouSignals_Apex.getInitData(jsonInput);
        Test.stopTest();
    }

    @isTest private static void testGetCompanySignals(){
        IVPTestFuel tFuel = new IVPTestFuel();
        List<ListView> lv = [SELECT Id FROM ListView WHERE SobjectType = 'Account' LIMIT 1];
        List<Account> companies = tFuel.companies;
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<Company_Signal__c> companySignals = tFuel.companySignals;
        String json ='{"viewId" : "'+lv[0].Id+'"}';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new IVPWatchDogMock());
        JustForYouSignals_Apex.IVPWDData wdData = JustForYouSignals_Apex.getCompanySignals(json);

        Test.stopTest();
    }

    @isTest private static void TestsetWDFilters(){

        List<String> colours = new List<String>();
        colours.add('red');
        colours.add('green');
        colours.add('element');
        List<String> cats = new list<String>();
        cats.add('acquisition-acquiree');
        cats.add('debt-financing');
        cats.add('restructuring');
        Test.startTest();
        JustForYouSignals_Apex.setWDFilters(colours,cats);
        Test.stopTest();
    }

    @isTest private static void TestGetWDFilters2(){

        List<Intelligence_User_Preferences__c> uPres = new  List<Intelligence_User_Preferences__c>();

        Intelligence_User_Preferences__c IntelligenceUserPreferencesObj = new Intelligence_User_Preferences__c();
        IntelligenceUserPreferencesObj.User_Id__c = UserInfo.getUserId();
        IntelligenceUserPreferencesObj.Signal_Filter_Colour__c = 'red;green;yellow';
        IntelligenceUserPreferencesObj.WD_Filter_Category__c = 'red;green;yellow';
        //  IntelligenceUserPreferencesObj.My_Dash_Signal_Preferences__c = 'acquisition-acquirer, acquisition-acquiree';
        //  IntelligenceUserPreferencesObj.Sourcing_Signal_Category_Preferences__c ='conference-presenter,customer-win,conference-attendee ';

        uPres.add(IntelligenceUserPreferencesObj);
        insert uPres;


        List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
        Signal_Category__c sgcobj = new Signal_Category__c();
        sgcobj.Name = 'TestingSignal';
        sgcobj.External_Id__c = 'sdfghg65';
        sgcobj.Active__c = true;
        SignalCategoryList.add(sgcobj);
        insert SignalCategoryList;


        Test.startTest();
        JustForYouSignals_Apex.getWDFilters();
        Test.stopTest();

    }

    @isTest private static void TestsetIVPSDSettings(){

        String testParameter = '{"selClrsOps":["red","green","yellow"],"selCatsOps":[],"modifyMetaData":true}';
        Test.startTest();
        JustForYouSignals_Apex.setIVPSDSettings(testParameter);
        Test.stopTest();
    }

}