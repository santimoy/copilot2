@isTest
private class PreferencePreviewControllerTest {
    @isTest static void test1(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        List<Intelligence_User_Preferences__c> intelligenceUserList = new List<Intelligence_User_Preferences__c>();
        Intelligence_User_Preferences__c intelligenceobj = new Intelligence_User_Preferences__c();
        intelligenceobj.Display_Columns__c = 'Name (ivp_name);Country (ivp_country);Owner (salesforce_owner_name);';
        intelligenceobj.Name = 'Test Class';
        intelligenceobj.User_Id__c = Userinfo.getUserId();        
        intelligenceUserList.add(intelligenceobj);
        if(intelligenceUserList.size() > 0){
            insert intelligenceUserList;            
        }
        System.assertEquals(1, intelligenceUserList.size());
        
        List<Intelligence_Field__c> intelligenceFieldList = new List<Intelligence_Field__c>();        
        Intelligence_Field__c  IntelligenceFieldcobj = new Intelligence_Field__c();
        IntelligenceFieldcobj.DB_Field_Name__c ='Test Db';
        IntelligenceFieldcobj.UI_Label__c = 'testing';
        IntelligenceFieldcobj.DB_Column__c = 'testing';
        IntelligenceFieldcobj.Order__c =2.00;
        IntelligenceFieldcobj.Active__c=true;
        IntelligenceFieldcobj.Static_Selection__c = true;
        intelligenceFieldList.add(IntelligenceFieldcobj);
        
        Intelligence_Field__c  IntelligenceFieldcobj1 = new Intelligence_Field__c();
        IntelligenceFieldcobj1.DB_Field_Name__c ='Test Db';
        IntelligenceFieldcobj1.UI_Label__c = 'Company Name';
        IntelligenceFieldcobj1.DB_Column__c = 'testing';
        IntelligenceFieldcobj1.Order__c =2.00;
        IntelligenceFieldcobj1.Active__c=true;
        IntelligenceFieldcobj1.Static_Selection__c = true;
        intelligenceFieldList.add(IntelligenceFieldcobj1);
        
        if(intelligenceFieldList.size() > 0){
            insert intelligenceFieldList;
        }
        
        System.assertEquals(2, intelligenceFieldList.size());
        
        PreferencePreviewController.getInitialColumns();
        List<IVP_Call_Params__c> IVPCallParamscList = new List<IVP_Call_Params__c>();        
        IVP_Call_Params__c IVPCallParams = new IVP_Call_Params__c();
        IVPCallParams.Name = 'Preview';
        IVPCallParams.Preview_Universe_Limit__c = 50;
        IVPCallParams.Preview_Preference_Limit__c = 15 ;
        IVPCallParams.Just_For_You_OrderBy__c ='ct.companies_cross_data_full.iq_score asc NULLS LAST';
        IVPCallParams.Preview_Preference_OrderBy__c ='ct.companies_cross_data_full.iq_score desc NULLS LAST';
        IVPCallParamscList.add(IVPCallParams);
        if(IVPCallParamscList.size() > 0){
            insert IVPCallParamscList;
        }
        System.assertEquals(1, IVPCallParamscList.size()); 
        
        Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
        sourcingPreference.Name__c = 'Test';
        sourcingPreference.Include_Exclude__c = 'Include';
        sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
        sourcingPreference.External_Id__c = 'frkd-e23-53';
        sourcingPreference.JSON_QueryBuilder__c = '{"condition":"AND","rules":[{"id":"LOWER(ct.company_cross_data_view.ivp_country)","field":"LOWER(ct.company_cross_data_view.ivp_country)","type":"string","input":"select","operator":"equal","value":"india"},'+
            '{"id":"LOWER(ct.company_cross_data_view.ivp_country)","field":"testing","type":"string","input":"select","operator":"equal","value":"india"}'
            +'],"valid":true}';
        insert sourcingPreference;
        Test.startTest();
        PreferencePreviewController.fetchRecName(sourcingPreference.External_Id__c);
        PreferencePreviewController.getDisplayColumn(sourcingPreference.External_Id__c, true);
        
        Set<String> fLabelList1 = new Set<String>();
        Set<String> fApiList1 = new Set<String>();
        fLabelList1.add('User Id');
        fLabelList1.add('Last Visited WatchDog');
        
        fApiList1.add('User_Id__c');
        fApiList1.add('Last_Visited_WatchDog__c');
        
        PreferencePreviewController.fieldClass fc =new PreferencePreviewController.fieldClass();
        fc.fLabelList = fLabelList1;
        fc.fApiList = fApiList1; 
        PreferencePreviewController.fetchHerokuData('Preview','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        PreferencePreviewController.fetchHerokuData('Preview Universe','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        //PreferencePreviewController.fetchHerokuData('Preview ','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC',JSON.serialize(fc));
        Test.stopTest();
    }
    
    @isTest static void test2(){
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        List<Intelligence_User_Preferences__c> intelligenceUserList = new List<Intelligence_User_Preferences__c>();
        Intelligence_User_Preferences__c intelligenceobj = new Intelligence_User_Preferences__c();
        //intelligenceobj.Display_Columns__c = 'Name (ivp_name);Country (ivp_country);Owner (salesforce_owner_name);';
        intelligenceobj.Name = 'Test Class';
        intelligenceobj.User_Id__c = Userinfo.getUserId();
        intelligenceobj.Display_Columns__c = null;        
        intelligenceUserList.add(intelligenceobj);
        if(intelligenceUserList.size() > 0){
            insert intelligenceUserList;
        }
        
        System.assertEquals(1, intelligenceUserList.size());
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        List<Intelligence_Field__c> intelligenceFieldList = new List<Intelligence_Field__c>();        
        Intelligence_Field__c  IntelligenceFieldcobj = new Intelligence_Field__c();
        //IntelligenceFieldcobj.Name = 'test';
        IntelligenceFieldcobj.DB_Field_Name__c ='Test Db';
        IntelligenceFieldcobj.UI_Label__c = 'testing';
        IntelligenceFieldcobj.Order__c =2.00;
        IntelligenceFieldcobj.Active__c=true;
        IntelligenceFieldcobj.Static_Selection__c = true;
        intelligenceFieldList.add(IntelligenceFieldcobj);
        if(intelligenceFieldList.size() > 0){
            insert intelligenceFieldList;
        }
        System.assertEquals(1, intelligenceFieldList.size());
        
        PreferencePreviewController.getInitialColumns();
        
        List<IVP_Call_Params__c> IVPCallParamscList2 = new List<IVP_Call_Params__c>();
        IVP_Call_Params__c IVPCallParams = new IVP_Call_Params__c();
        IVPCallParams.Name = 'Preview';
        IVPCallParams.Preview_Universe_Limit__c = 50;
        IVPCallParams.Preview_Preference_Limit__c = 15 ;
        IVPCallParams.Just_For_You_OrderBy__c ='ct.companies_cross_data_full.iq_score asc NULLS LAST';
        IVPCallParams.Preview_Preference_OrderBy__c ='ct.companies_cross_data_full.iq_score desc NULLS LAST';
        IVPCallParamscList2.add(IVPCallParams);
        if(IVPCallParamscList2.size() > 0){
            insert IVPCallParamscList2;
        }
        System.assertEquals(1,IVPCallParamscList2.size());
        
        Set<String> fLabelList1 = new Set<String>();
        Set<String> fApiList1 = new Set<String>();
        fLabelList1.add('User Id');
        fLabelList1.add('Last Visited WatchDog');        
        fApiList1.add('User_Id__c');
        fApiList1.add('Last_Visited_WatchDog__c');
        
        PreferencePreviewController.fieldClass fc =new PreferencePreviewController.fieldClass();
        fc.fLabelList = fLabelList1;
        fc.fApiList = fApiList1; 
        
        PreferencePreviewController.responseClass rc =new PreferencePreviewController.responseClass();
        rc.fLabelList = fLabelList1;
        rc.fApiList = fApiList1; 
        
        PreferencePreviewController.HerokuData hd = new  PreferencePreviewController.HerokuData();
        hd.total_pages = 10;
        hd.additional_data =true;
        hd.host = 'test';
        hd.total_entries = 4;
        hd.dialect ='testing';
        hd.error = 'test';
        hd.sql_statement ='test';
        // hd.data_fields ='test';
        
        PreferencePreviewController.Response_time rt = new  PreferencePreviewController.Response_time();
        //   rt.query_execution = 2;
        //  rt.count_query_execution = 2;
        //   rt.total_execution_time = 2;
        
        Test.startTest();
       // PreferencePreviewController.getDisplayColumn();
        //  Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        // PreferencePreviewController.fetchHerokuData('Preview Universe','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        // PreferencePreviewController.fetchHerokuData('Preview','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        //  PreferencePreviewController.fetchHerokuData('Preview Universe','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        Test.stopTest();
        
        // PreferencePreviewController.fetchHerokuData('Preview','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        //PreferencePreviewController.fetchHerokuData('Preview ','1','f70c9-2920d-f3ba7','Display_Columns__c','DESC');
        
    }
    @isTest static void test3(){
        Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
        sourcingPreference.Name__c = 'Test';
        sourcingPreference.Include_Exclude__c = 'Include';
        sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
        sourcingPreference.External_Id__c = 'frkd-e23-53';
        sourcingPreference.JSON_QueryBuilder__c = '{"condition":"AND","rules":[{"id":"LOWER(ct.company_cross_data_view.ivp_country)","field":"LOWER(ct.company_cross_data_view.ivp_country)","type":"string","input":"select","operator":"equal","value":"india"}],"valid":true}';
        insert sourcingPreference;
        system.debug(sourcingPreference);
        Test.startTest();
        PreferencePreviewController.fetchRecName(sourcingPreference.External_Id__c);
        PreferencePreviewController.getDisplayColumn(sourcingPreference.External_Id__c, true);
        Test.stopTest();
    }
}