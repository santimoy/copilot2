/*
Class Name   :  Batch_UpsertAccountsToKeyContactScope
Description  :  To Upsert Prospect and Mark for deletion false Account records to Key Contact Scope Object
Author       :  Sukesh
Created On   :  August 07 2019
*/
global with sharing class Batch_UpsertAccountsToKeyContactScope implements Database.Batchable<sObject> {

    String strQuery = 'SELECT Id FROM Account WHERE RecordType.Name=\'Prospect\' AND Marked_for_Deletion__c = FALSE';

    global Batch_UpsertAccountsToKeyContactScope() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Account> lstAccount) {

        List<Key_Contact_Scope__c> lstKeyContactScopeToInsert = new List<Key_Contact_Scope__c>();

        Map<String, String> mapAccountIdToContactId = new Map<String, String>();
        Map<String, Account> mapAccountIdToAccount = new Map<String, Account>();

        //To Make sure that Accounts without any Contacts are also pushed to Key Contact Scope
        for(Account objAccount : lstAccount) {

            mapAccountIdToContactId.put(objAccount.Id, '');
        }

        //Iterate over Child Contacts of Account Scope where Key Contact = true
        for(Contact objContact  : [SELECT Id,
                                          AccountId
                                     FROM Contact
                                    WHERE AccountId IN : lstAccount
                                      AND Key_Contact__c = TRUE]) {

            mapAccountIdToContactId.put(objContact.AccountId, objContact.Id);
        }

        Set<String> setAccountIdWithKeyContact = mapAccountIdToContactId.KeySet();

        for(Account objAccount : (List<Account>)(Database.query('SELECT Id,Owner.FirstName,Owner.LastName,Owner.Name, '+System.Label.Clearbit_Domain+' FROM Account WHERE Id IN : setAccountIdWithKeyContact'))) {

            mapAccountIdToAccount.put(objAccount.Id, objAccount);
        }

        //Fetch the Userd Id's with Clear Bit Licence
        Set<Id> setUserwithClearBitLicence = fetchUserIdsWithClearBitLicence();

        //Iterate over each Account and its Key Contact
        for(Id accountId : mapAccountIdToContactId.KeySet()) {

            String strDomain =  String.ValueOf((mapAccountIdToAccount.get(accountId)).get(System.Label.Clearbit_Domain));

            Key_Contact_Scope__c objKeyContactScope = new Key_Contact_Scope__c(Account__c = accountId,
                                                                               Account_Id__c = accountId,
                                                                               Owner_Name__c = mapAccountIdToAccount.get(accountId).Owner.Name,
                                                                               Contact_Integration_Payload__c = createJsonPayload(strDomain, accountId, setUserwithClearBitLicence));

            if(!String.isEmpty(mapAccountIdToContactId.get(accountId))) {

                objKeyContactScope.Current_Key_Contact__c = mapAccountIdToContactId.get(accountId);
            }


            lstKeyContactScopeToInsert.add(objKeyContactScope);
        }

        system.System.debug('=====lstKeyContactScopeToInsert========='+lstKeyContactScopeToInsert);

        upsert lstKeyContactScopeToInsert Key_Contact_Scope__c.Fields.Account_Id__c;
    }

    global void finish(Database.BatchableContext BC) {

    }

    /**
    @Description Method Create JSON Payload for ClearBit API Call
    @author Sukesh
    @date August 07 2019
    @param strDomain The website of Account
    @param  strAccountId Account Id for which PayLoad needs to be created
    */
    private String createJsonPayload(String strDomain,String strAccountId, Set<Id> setUserwithClearBitLicence) {

        //Fetch all the Clear Bit Roles
        List<Clearbit_Role_Settings__c> listClearBitRoles = Clearbit_Role_Settings__c.getAll().values();
        //Fetch all the DWH Roles
        List<DWH_Role_Settings__c> listDWHRoles = DWH_Role_Settings__c.getAll().values();

        //Check if current user has Clear Bit licence
        String strhasClearbitLicense = (setUserwithClearBitLicence.contains(UserInfo.getUserId())) ? 'True' : 'False';

        String strBody = '{ "domain":"'+strDomain+'","account_id":"'+strAccountId+'","hasClearbitLicense":"'+strhasClearbitLicense+'","user_id":"'+UserInfo.getUserId()+'","fallbackid":"'+System.Label.Fallback_Clearbit_Owner_Id+'","clearbit_role_rankings":[';

        //Iterate over all the Clear Bit Roles
        for(Clearbit_Role_Settings__c objClearbit_Role_Settings : listClearBitRoles) {

            String strStatus = (objClearbit_Role_Settings.Active__c) ? 'Active' : 'InActive';

            strBody += '{"role":"'+objClearbit_Role_Settings.Role__c+'","ranking":"'+objClearbit_Role_Settings.Ranking__c+'","status":"'+strStatus+'"},';
        }

        strBody = strBody.removeEnd(',');
        strBody += '],"dwh_role_rankings":[';

        //Iterate over all the DWH Roles
        for(DWH_Role_Settings__c objDWH_Role_Settings : listDWHRoles) {

            String strStatus = (objDWH_Role_Settings.Active__c) ? 'Active' : 'InActive';

            strBody += '{"role":"'+objDWH_Role_Settings.Role__c+'","ranking":"'+objDWH_Role_Settings.Ranking__c+'","status":"'+strStatus+'"},';
        }

        strBody = strBody.removeEnd(',');

        strBody += ']}';

        return strBody;
    }

    /**
    @Description Method that return the list of users with clearbit licence
    @author Sukesh
    @date August 07 2019
    @See AccountTriggerHandler.fetchUserIdsWithClearBitLicence
    @return   Set of User Id's with clearbit licence
    */
    private static Set<Id> fetchUserIdsWithClearBitLicence() {

        Set<Id> setUserwithClearBitLicence = new Set<Id>();

        List<Clearbit_Package_Namespace__mdt> lstClearBitPackageNamespace = [SELECT Id,
                                                                                    Namespace__c
                                                                               FROM Clearbit_Package_Namespace__mdt];

        String strClearBitNameSpace = (!lstClearBitPackageNamespace.isEmpty()) ?  lstClearBitPackageNamespace[0].Namespace__c : '';

        if(!String.isEmpty(strClearBitNameSpace)) {

            for(User objUser : [SELECT Id,
                                       Name
                                  FROM User
                                  WHERE Id IN (SELECT UserId
                                                 FROM UserPackageLicense
                                                WHERE (PackageLicense.NamespacePrefix =:strClearBitNameSpace))]) {

                setUserwithClearBitLicence.add(objUser.Id);

            }
        }

        return setUserwithClearBitLicence;
    }
}