/**
 * Created by Ranjeet Kumar on 11/5/2017.
 */

public with sharing class ProjectHoursTriggerHelper {

    public static void HandleBeforeInsert(List<Project_Hours__c> ProjectHoursList){
        if(ProjectHoursList.size() > 0){
            Boolean IsDuplicate = ProjectHoursTriggerHelper.CheckForDuplicates(ProjectHoursList);
            if(IsDuplicate){
                ProjectHoursList[0].addError('Duplicate Project Hour record is not allowed!');
            }
        }
    }

    public static Boolean CheckForDuplicates(List<Project_Hours__c> ProjectHoursList){
        Boolean IsDuplicate = false;
        if(ProjectHoursList.size() > 0){
            Set<Id> InvoiceIds = new Set<Id>();
            Set<Id> ProjectIds = new Set<Id>();
            for(Project_Hours__c ProHour : ProjectHoursList){
                InvoiceIds.add(ProHour.Invoice__c);
                ProjectIds.add(ProHour.Krow_Project__c);
            }
            List<Project_Hours__c> ExistingProHoursList = new List<Project_Hours__c>();
            ExistingProHoursList = [Select Id From Project_Hours__c Where Invoice__c In : InvoiceIds AND Krow_Project__c In : ProjectIds];
            if(ExistingProHoursList.size() > 0){
                IsDuplicate = true;
            }
        }
        return IsDuplicate;
    }
}