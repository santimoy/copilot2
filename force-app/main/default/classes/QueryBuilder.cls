public class QueryBuilder{
    @AuraEnabled public string query ;
    @AuraEnabled public string replacement ;
    @AuraEnabled public List<Intelligence_Field__c> fieldListObj;
    @AuraEnabled public List<Map<string, List<String>>> mapPicklistObj;
    @AuraEnabled public String picklistBody;
    @AuraEnabled public boolean fetchPicklistErr;
    
    @auraEnabled
    public static QueryBuilder fetchFilters(){
        QueryBuilder QueryBuidObj = new QueryBuilder();
        List<Intelligence_Field__c> intFieldList = [select Id,Active__c, Name, UI_Label__c, DB_Column__c,  Filterable_from_UI__c,DB_Table__c,QBFlt_Manual_LOVs__c,
                                                    Default_display_field__c, QBFlt_Input__c, DWH_Data_Type__c, QBFlt_Value_Separator__c, QBFlt_Default_Value__c,
                                                    QBFlt_Size__c, QBFlt_Rows__c, QBFlt_Multiple__c, QBFlt_Placeholder__c, QBFlt_Vertical__c, QBFlt_Num_Min__c,
                                                    QBFlt_Num_Max__c, QBFlt_Operators__c, QBFlt_Default_Operator__c, QBFlt_Plugin_Name__c, QBFlt_Is_Filter__c, QBFlt_Lowercase__c,
                                                    QBFlt_LOVs__c, QBFlt_SF_Values__c,QBFlt_Typeahead__c, Target_Field__c, QBFlt_Filter_Grouping__c from Intelligence_Field__c
                                                    where QBFlt_Is_Filter__c  = true AND QBFlt_no_filter__c = false AND Active__c = true 
                                                    ORDER BY QBFlt_Filter_Grouping__c,Order__c NUllS Last, UI_Label__c];
        List<String> picklistNames = new List<String>();
        for(Intelligence_Field__c intField : intFieldList){
            if(intField.QBFlt_LOVs__c != null){
                picklistNames.add(intField.QBFlt_LOVs__c);
            }
        }
        if(picklistNames != null && picklistNames.size()>0){
            QueryBuidObj.fieldListObj = intFieldList;
            LoggerWrapper logWrap = getPicklListValues(picklistNames);
            if(logWrap.isApiError){
                QueryBuidObj.picklistBody = logWrap.apiError;
                QueryBuidObj.fetchPicklistErr = true;
            }else{
                QueryBuidObj.picklistBody = logWrap.responseBody;
                QueryBuidObj.fetchPicklistErr = false;
            }
        }else{
            QueryBuidObj.fieldListObj = intFieldList;
            QueryBuidObj.picklistBody = '';
        }
        return QueryBuidObj;
    }
    
    public static LoggerWrapper getPicklListValues(List<String> picklistNames){
        // method name, className
        String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c+'/preference_user/ui_setup';
        LoggerWrapper logWrap = HttpCallout.makeCallout(endPoint, 'GET', '', Integer.valueOf(System.Label.IVP_Preference_User_Resp_Status), 'getPicklListValues','QueryBuilder');
        return logWrap;
    }
    
    @auraEnabled
    public static Sourcing_Preference__c fetchRecord(Id recId){
        return [Select Id, Name, OwnerId, External_Id__c, Include_Exclude__c,Advanced_Mode__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, Name__c,
                Owner_Username__c,Recent_Validation_Error__c,  SQL_Query__c, SQL_Where_Clause__c, Serialized__c, Status__c, Validation_Status__c,
                Cloned_From__c From Sourcing_Preference__c Where Id=: recId Limit 1];
    }
    
    @auraEnabled
    public static List<Intelligence_Field__c> getKeywordQuery(){

        List<Intelligence_Field__c> intFieldList = [Select Id, Name, Active__c, UI_Label__c,Keyword_Criteria__c , QBFlt_Lowercase__c, Target_Field_Type__c, DB_Column__c,
                                                    DB_Table__c, Filterable_from_UI__c, Default_display_field__c from Intelligence_Field__c
                                                    Where Keyword_Criteria__c = true AND Active__c = true];
        return intFieldList;
    }
    
    @auraEnabled
    public static QueryBuilderGetSaveRecId saveSourcingPreference(Sourcing_Preference__c saveRec){
        try{
            Intelligence_User_Preferences__c intUserPrefRec = [Select Id, name, Display_Columns__c, User_Id__c, Sourcing_Signal_Category_Preferences__c,
                                                               My_Dash_Signal_Preferences__c from Intelligence_User_Preferences__c Where User_Id__c =: UserInfo.getUserId() LIMIT 1];
            String commaSepratedList='';
            if(intUserPrefRec.Display_Columns__c != null && intUserPrefRec.Display_Columns__c.trim() != ''){
                List<String> columnList = intUserPrefRec.Display_Columns__c.split(';');
                for(Integer i = 0; i < columnList.size(); i++){
                    commaSepratedList += columnList[i].substringBetween('(',')') + ', ' ;
                }
                commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length() - 2);
            }else{
                commaSepratedList = 'ivp_name';
            }
            boolean isNew = true ;
            if(isNew){
                //saveRec.SQL_Where_Clause__c = saveRec.SQL_Where_Clause__c.replaceAll(System.Label.IVP_DWH_Query_View_Name_To_Replace, System.Label.IVP_DWH_Query_Table_Name);
                saveRec.SQL_Query__c = 'SELECT '+ commaSepratedList + ' From ct.'+System.Label.IVP_DWH_Query_View_Name_To_Replace+'  Where '+  saveRec.SQL_Where_Clause__c;
            }else{
                saveRec.SQL_Where_Clause__c = saveRec.SQL_Where_Clause__c.replaceAll(System.Label.IVP_DWH_Query_View_Name_To_Replace, System.Label.IVP_DWH_Query_Table_Name);
                //saveRec.SQL_Where_Clause__c = saveRec.SQL_Where_Clause__c.replaceAll('ct.'+System.Label.IVP_DWH_Query_View_Name_To_Replace, 'c');
                saveRec.SQL_Query__c = 'SELECT '+ commaSepratedList + ' From ct.'+System.Label.IVP_DWH_Query_View_Name_To_Replace+' as c Where '+ saveRec.SQL_Where_Clause__c.replaceAll('ct.'+System.Label.IVP_DWH_Query_View_Name_To_Replace, 'c');
            }            
        }catch(Exception e){
            system.debug('No Intelligence_User_Preferences__c record found for logged in user.');
        }
        saveRec.OwnerId = UserInfo.getUserId();
        saveRec.status__c = 'Active';
        if(saveRec.External_Id__c == null){
            saveRec.External_Id__c = getUniqueId();
        }
        saveRec = makeCallout(saveRec);
        system.debug(saveRec.Validation_Status__c);
        QueryBuilderGetSaveRecId myNewRec= new QueryBuilderGetSaveRecId();
        system.debug('saveRec=='+saveRec);
        myNewRec.validationStatus = saveRec.Validation_Status__c;
        String validationErr =  saveRec.Recent_Validation_Error__c;
        
        if(validationErr != null && validationErr != '' && validationErr.contains('Position')){
            myNewRec.recentValidationError = validationErr.substring(0, validationErr.indexOf('Position'));
        }else{
            myNewRec.recentValidationError = validationErr;
        }
        
        if(saveRec.DWH_Exception__c != null){
            myNewRec.recentValidationError = saveRec.DWH_Exception__c;
        }
        myNewRec.lastValidated = saveRec.Last_Validated__c;
        //saveRec.Validation_Status__c = 'In Progress';
        system.debug(saveRec.Validation_Status__c);
        upsert saveRec;
        myNewRec.newRecId = saveRec.Id;
        myNewRec.newExternalId = saveRec.External_Id__c;
        myNewRec.isPass = true;
        return myNewRec;
    }
    
    public static Sourcing_Preference__c makeCallout(Sourcing_Preference__c sourcPref) {
        String recUrl = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/'+sourcPref.Id+' ';
        sourcingPrefWrapper sourPrfWrap = new sourcingPrefWrapper();
        sourPrfWrap.record = sourcPref;
        sourPrfWrap.url = recUrl;
        sourcPref.Serialized__c = JSON.serialize(sourPrfWrap, false);
        
        sourcingPrefWrapper sourPrfWrap1 = new sourcingPrefWrapper();
        sourPrfWrap1.record = sourcPref;
        sourPrfWrap1.url = recUrl;
        //sourPrfWrap1.record.Validation_Status__c='None';
        //sourPrfWrap1.record.Validation_Status__c = null ;
        sourPrfWrap1.record.Validation_Status__c='Valid';
        String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c+'/preferences';
        System.debug('{"method":"POST","body":'+JSON.serialize(sourPrfWrap1, false) + ',"url":"'+endPoint+'"}');
        LoggerWrapper logWrap = HttpCallout.makeCallout(endPoint, 'POST', JSON.serialize(sourPrfWrap1, false), Integer.valueOf(System.Label.IVP_Preferences_Upsert_Resp_Status), 'makeCallout','QueryBuilder');
        sourcPref.DWH_Req_Payload__c = JSON.serialize(sourPrfWrap1, false);
        system.debug(logWrap.isApiError);
        if(logWrap.isApiError){
            sourcPref.DWH_Status__c = 'Failure';
            sourcPref.DWH_Exception__c = logWrap.apiError;
            sourcPref.Recent_Validation_Error__c = logWrap.apiError;
            sourcPref.Validation_Status__c = 'Invalid';
            if(logWrap.readTimeout){
                sourcPref.Validation_Status__c = 'In Progress';
                logWrap.apiError = Label.DWH_Preference_API_Time_Out;
                sourcPref.Recent_Validation_Error__c = logWrap.apiError;
                sourcPref.DWH_Exception__c = logWrap.apiError;
            }
            sourcPref.Last_Validated__c = System.now();
            system.debug(sourcPref.Validation_Status__c);
            return sourcPref;
        }else{
            sourcPref.DWH_Resp_Code__c = logWrap.responseCode;
            integer maxSize = 131071;
            if(logWrap.responseBody != null && logWrap.responseBody.length() > maxSize ){
                sourcPref.DWH_Resp_Body__c = logWrap.responseBody.substring(0, maxSize);
            }else{
                sourcPref.DWH_Resp_Body__c = logWrap.responseBody;
            }
            
            GetValidationDetails validationDetails = (GetValidationDetails)JSON.deserialize(logWrap.responseBody, GetValidationDetails.class);
            sourcPref.Validation_Status__c = validationDetails.validation_status;
            if(sourcPref.Validation_Status__c != null && sourcPref.Validation_Status__c == 'Valid'){
                sourcPref.DWH_Status__c = 'Success';
                sourcPref.Recent_Validation_Error__c = '';
            }else{
                sourcPref.Validation_Status__c = 'Invalid';
                sourcPref.DWH_Status__c = 'Success'; 
                string validationError = validationDetails.Recent_Validation_Error;
                integer maxSize1 = 255;
                if(validationError != null && validationError.length() > maxSize1 ){
                    sourcPref.Recent_Validation_Error__c = validationError.substring(0, maxSize1);
                }else{
                    sourcPref.Recent_Validation_Error__c = validationError;
                }
            }
            String stringDateTime = validationDetails.Last_Validated;
            if(stringDateTime != null){
                sourcPref.Last_Validated__c = Datetime.valueOf(stringDateTime);
            }
            sourcPref.DWH_Exception__c = null;
            return sourcPref;
        }
    }
    
    @AuraEnabled
    public static List<List<String>> searchInvestorRecords_Apex(String searchString) {
        String sanitizedSearchString = String.escapeSingleQuotes(searchString);
        String whereClause = '';
        getInvestorBody getInvestor = new getInvestorBody();
        getInvestor.search_query = sanitizedSearchString;
        List<List<String>> InvestorDataList = new List<List<String>>();
        
        String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c+'/investors/search';
        LoggerWrapper logWrap = HttpCallout.makeCallout(endPoint, 'POST', JSON.serialize(getInvestor, false), Integer.valueOf(System.Label.IVP_Investor_Search_Resp_Status), 'searchInvestorRecords_Apex','QueryBuilder');
        if(logWrap.isApiError){
            return InvestorDataList;
        }else{
            InvestorData iData = parse(logWrap.responseBody);
            InvestorDataList = iData.data;
            return InvestorDataList;
        }
    }
    
    public class getInvestorBody{
        public string search_query { get; set;}
    }
    
    public static InvestorData  parse(String json) {
        return (InvestorData) System.JSON.deserialize(json, InvestorData.class);
    }
    public class InvestorData {
        @AuraEnabled public List<List<String>> data {get;set;}
    }
    
    // Wrapper Class to create value for Serialized__c field
    // argument.
    // @param .
    // @return . 
    public class sourcingPrefWrapper{
        public Sourcing_Preference__c record { get; set;}
        public string url { get; set;}
    }
    
    public static string getUniqueId(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,5)+ '-' + h.SubString(5,10) + '-' + h.SubString(10,15);
        return guid;
    }
    
}