/**
 * @author MukeshKS-Concretio
 * @date 2018-04-18
 * @className IVPFeelingLuckyController
 * @group FeelingLucky
 *
 * @description contains methods to handle IFL component
 */
public with sharing class IVPFeelingLuckyController {
	public class IVPFeelingLuckyControllerException extends Exception{}
    
    /*******************************************************************************************************
     * @description GetFeelingLuckyList - this method is used for to get list of IFL records of current logged in user
     * @param void
     * @return List<Intelligence_Feedback__c> feelingLuckyList
     * @author MukeshKS-concretio
	*/
    @AuraEnabled
    public static IVPFeelingLuckyWrapper getFeelingLuckyList(){
        return IVPFeelingLuckyService.getFeelingLuckyList(true);
    }
    
    /*******************************************************************************************************
     * @description SaveIFLFeedback - this method is used for to save IFL feedback
     * @param Intelligence_Feedback__c feedbackObj
     * @return Intelligence_Feedback__c feedbackObj
     * @author MukeshKS-concretio
	*/
    @AuraEnabled
    public static IVPResponse saveIFLFeedback(Intelligence_Feedback__c feedbackObj){
        return IVPFeelingLuckyService.saveIFLFeedback(feedbackObj);
    }
    
    /**
     * GetRefreshedFeelingLuckyList this method is used to run batch to assign IFL records
     * to users
     * @param void
     * @return void
     * @author Mukesh-ConcretIo
     */
    @AuraEnabled
    public static void getRefreshedFeelingLuckyList(){
        System.debug('getRefreshedFeelingLuckyList is here');
        IVPFeelingLuckyService.runIFLBatch();
    }

    /**
     * GetRefreshedFeelingLuckyList this method is used to run batch to assign IFL records
     * to users
     * @param void
     * @return void
     * @author Mukesh-ConcretIo
     */
    /*@AuraEnabled
    public static IVPResponse doTriggerAssignIFLRecordsProcess(){
        return IVPFeelingLuckyService.runIFLBatchNew();
    }*/

    /**
     * IsUserSystemAdmin this method is used for to check, current user is system administrator or not
     * @param void
     * @return Boolean returns true if current user is system administrator
     * @author Mukesh-ConcretIo
     */
    @AuraEnabled
    public static Boolean isUserSystemAdmin(){
        return IVPUserService.isCurrentUserSystemAdmin();
    }
}