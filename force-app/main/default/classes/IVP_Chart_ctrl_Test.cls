@isTest
public class IVP_Chart_ctrl_Test {
    @testSetup 
	static void setupData(){
		IVPTestFuel tFuel = new IVPTestFuel();
		tFuel.populateLVSettings();
	}
    @isTest static void testGetInitData(){
    	Map<String, Object> initdata = IVP_Chart_ctrl.getInitData('Account');
    }
    @isTest static void testGetDefaultValueOfChart(){
        IVP_Chart_ctrl chartObj = new IVP_Chart_ctrl();
        VisualEditor.DataRow dataRow = chartObj.getDefaultValue();
        System.assertEquals(false,dataRow.isSelected());
    }
    @isTest static void testGetValuesOfChart(){
        IVP_Chart_ctrl chartObj = new IVP_Chart_ctrl();
        VisualEditor.DynamicPickListRows dynamicPickListRows = chartObj.getValues();
        System.assertEquals(3,dynamicPickListRows.size());
    }
    @isTest static void testGetData(){
        IVPTestFuel tFuel = new IVPTestFuel();
        
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        List<Account> accounts= tFuel.companies;
        for(Account acc : accounts){
            acc.ownerId = UserInfo.getUserId();
            acc.RecordTypeId = recordTypeId;
        }
        update accounts;
        List<IVP_Chart_ctrl.ObjectWrapper> objectWrappers = IVP_Chart_ctrl.getGroupableFields('Account');
        //Because the Groupable fields for an object are varies by org to org.
        //System.assertEquals(18,objectWrappers.size());
        
        
        IVP_Chart_ctrl.Data data;
        String viewName = '';
        
        List<ListView> listviews = [SELECT Id, DeveloperName FROM ListView 
        									WHERE DeveloperName in ('My_Recently_Unassigned_Public','My_Smartdash_Public') 
        									ORDER BY DeveloperName desc
        									LIMIT 1];
		if(!listviews.isEmpty()){
		    viewName = listviews[0].DeveloperName;
		}
		Test.startTest();
		    Test.setMock(HttpCalloutMock.class, new IVPWatchDogMock());
            data = IVP_Chart_ctrl.getData('Account','RecordTypeId', '', 9, viewName, false,True);
        Test.stopTest();
    }
}