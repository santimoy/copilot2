public class SourcingPreferenceUpsertQue implements Queueable, Database.AllowsCallouts {
    public static Boolean TEST_BYPASS = Test.isRunningTest();
    public List<Sourcing_Preference__c> sourcingPrefList;
    public SourcingPreferenceUpsertQue(List<Sourcing_Preference__c> sourcingPrefList){
        this.sourcingPrefList = sourcingPrefList;
    } 
    public void execute(QueueableContext context) {
        if(sourcingPrefList != null && sourcingPrefList.size()> 0){
            Sourcing_Preference__c sourcPref = sourcingPrefList[0];
            
            String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c+'/preferences';
            HttpCallout.TEST_BYPASS = TEST_BYPASS;
            LoggerWrapper logWrap = HttpCallout.makeCallout(endPoint, 'POST', sourcPref.Serialized__c, Integer.valueOf(System.Label.IVP_Preferences_Upsert_Resp_Status), 'makeCallout','SourcingPreferenceUpsertQue');
            sourcPref.DWH_Req_Payload__c = sourcPref.Serialized__c;
            system.debug('logWrap=='+logWrap);
            if(logWrap.isApiError){
                sourcPref.DWH_Status__c = 'Failure';
                sourcPref.DWH_Exception__c = logWrap.apiError;
                sourcPref.Recent_Validation_Error__c = logWrap.apiError;
                sourcPref.Validation_Status__c = logWrap.apiError == Label.DWH_API_Time_Out ? 'In Progress' : 'Invalid';
                sourcPref.Last_Validated__c = System.now();
                update sourcPref;
            }else{
                sourcPref.DWH_Resp_Code__c = logWrap.responseCode;
                integer maxSize = 131071;
                if(logWrap.responseBody != null && logWrap.responseBody.length() > maxSize ){
                    sourcPref.DWH_Resp_Body__c = logWrap.responseBody.substring(0, maxSize);
                }else{
                    sourcPref.DWH_Resp_Body__c = logWrap.responseBody;
                }
                
                GetValidationDetails validationDetails = (GetValidationDetails)JSON.deserialize(logWrap.responseBody, GetValidationDetails.class);
                sourcPref.Validation_Status__c = validationDetails.validation_status;
                system.debug('sourcPref.Validation_Status__c='+sourcPref.Validation_Status__c);
                if(sourcPref.Validation_Status__c != null && sourcPref.Validation_Status__c == 'Valid'){
                    sourcPref.DWH_Status__c = 'Success';
                    sourcPref.Recent_Validation_Error__c = '';
                }else{
                    sourcPref.Validation_Status__c = 'Invalid';
                    sourcPref.DWH_Status__c = 'Success';
                    string validationError = validationDetails.Recent_Validation_Error;
                    integer maxSize1 = 255;
                    if(validationError != null && validationError.length() > maxSize1 ){
                        sourcPref.Recent_Validation_Error__c = validationError.substring(0, maxSize1);
                    }else{
                        sourcPref.Recent_Validation_Error__c = validationError;
                    }
                }
                String stringDateTime = validationDetails.Last_Validated;
                if(stringDateTime != null){
                    sourcPref.Last_Validated__c = Datetime.valueOf(stringDateTime);
                }
                sourcPref.DWH_Exception__c = null;
                update sourcPref;
            }
            sourcingPrefList.remove(0);
            if(!sourcingPrefList.isEmpty() ){
                if(!Test.isRunningTest()){
                    System.enqueueJob(new SourcingPreferenceUpsertQue(sourcingPrefList));
                }
            }
        }
    }
    
}