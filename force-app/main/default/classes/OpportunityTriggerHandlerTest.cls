/***********************************************************************************
 * @author      10k-Expert
 * @name		OpportunityTriggerHandlerTest
 * @date		2018-11-19
 * @description	To test OpportunityTriggerHandler
 ***********************************************************************************/
@isTest
public class OpportunityTriggerHandlerTest {
	@testSetup
    public static void testSetup(){
        List<RecordType> recordTypeLst = [SELECT Id FROM RecordType WHERE DeveloperName = 'Prospect'];
        
        List<Account> accLst = new List<Account>();
        
        accLst.add(new Account(Name='Test Account1', RecordTypeId=recordTypeLst[0].Id));
        accLst.add(new Account(Name='Test Account2', RecordTypeId=recordTypeLst[0].Id));
        
        insert accLst;
        
        List<Opportunity> oppLst = New List<Opportunity>();
        
        oppLst.add(new Opportunity(Name='Test Opportunity1', StageName='Prospecting', CloseDate=System.today(), AccountId=accLst[0].Id));
        
        insert oppLst;
        
        List<Company_Request__c> compantRequestLst = New List<Company_Request__c>();
        
        compantRequestLst.add(new Company_Request__c(Company__c=accLst[0].Id, Status__c='Pending',Request_By__c=accLst[0].OwnerId, Request_To__c=UserInfo.getUserId()));
        insert compantRequestLst;
    }
    /*
	@isTest
    public static void testBeforeInsert(){
        List<Account> accLst = [SELECT Id FROM Account WHERE Name = 'Test Account1' LIMIT 1];
        
        List<Opportunity> oppLst = New List<Opportunity>();
        
        oppLst.add(new Opportunity(Name='Test Opportunity1', StageName='Prospecting', CloseDate=System.today(), AccountId=accLst[0].Id));
        
        Test.startTest();
        
        List<Database.SaveResult> results = Database.insert(oppLst, false);
        
        Test.stopTest();
        
        System.assertEquals(results[0].isSuccess(), false);
    }
    */
	@isTest
    public static void testBeforeUpdate(){
        //List<Account> accLst = [SELECT Id FROM Account WHERE Name = 'Test Account2'];
        
        List<Opportunity> oppLst = [SELECT Id, AccountId,StageName FROM Opportunity LIMIT 1];
        //oppLst[0].AccountId = accLst[0].Id;
        oppLst[0].StageName = 'Closed Won';
        Test.startTest();
        	update oppLst;
        
        	//List<Database.SaveResult> results = Database.update(oppLst, false);

        Test.stopTest();
        
        List<Company_Request__c> companyRequestLst = [SELECT Id, Status__c FROM Company_Request__c];
        for(Company_Request__c crObj : companyRequestLst){
            System.assert(crObj.Status__c == 'Rejected');
        }
    }
}