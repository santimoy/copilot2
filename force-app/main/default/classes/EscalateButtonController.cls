public without sharing class EscalateButtonController {

    private static final Id ACC_PROSPECT_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Prospect').getRecordTypeId();

    @AuraEnabled
    public static list<string> getPickListValues(){
        list<string> escalatedReasons = new list<string>();
        escalatedReasons.add(''); 
        Schema.SObjectType s = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get('Escalation_Reason__c').getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            
                System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
                escalatedReasons.add(pickListVal.getValue());
            
            } 
            return escalatedReasons;  
    }

    @AuraEnabled
   public static WrapperForEscalate fetchInitData(String recordId){
        
        System.debug('recordId'+recordId);
        
        Set<id> ownerIdSet = new Set<id>();
        
        Account objAcc  = [SELECT Id, Name, OwnerId, Escalated__c
                           FROM Account 
                           WHERE Id =: recordId AND 
                           RecordTypeId =: ACC_PROSPECT_RT_ID];
        // Escalate_Output_RichText_Value__mdt obj = [SELECT RichTextValue__c 
        //                                     FROM Escalate_Output_RichText_Value__mdt];

        List<Escalate_Reason_Text_Value__mdt> escalateReasonList = [SELECT Lable__c, Description__c, Order__c
                                                                FROM Escalate_Reason_Text_Value__mdt
                                                                ORDER by Order__c ASC];

        WrapperForEscalate wrapperObj = new WrapperForEscalate();
        wrapperObj.objAcc = objAcc;
        wrapperObj.escalateReasonMdtList = escalateReasonList;
        return wrapperObj;
    }
     
    @AuraEnabled
    public static Account saveRecord(String recordID, Boolean isEscalated,String EscalatedNotes,String EscalatedReason,Date EscalatedDate, String SelectedEscalationDate){
        
        System.debug('isEscalated: '+isEscalated);
        System.debug('EscalatedNotes: '+EscalatedNotes);
        System.debug('EscalatedReason: '+EscalatedReason);
        System.debug('SelectedEscalationDate: '+SelectedEscalationDate);

        Date todayDate = Date.today();

        if(isEscalated){
                EscalatedDate = todayDate;
        }
        else{
            if(SelectedEscalationDate.contains('3')){
                EscalatedDate = todayDate.addMonths(3);
            }else if(SelectedEscalationDate.contains('6')){
                EscalatedDate = todayDate.addMonths(6);
            }else{
                EscalatedDate = null;
            }
        }
        Account objAcc  = [SELECT Id,Name,Escalated__c, Escalation_Reason__c, Escalation_Notes__c, Escalated_Date__c
                            FROM Account 
                            WHERE Id =: recordId AND 
                            RecordTypeId =: ACC_PROSPECT_RT_ID];
        objAcc.Escalated__c = isEscalated;
        objAcc.Escalation_Notes__c = EscalatedNotes;
        objAcc.Escalation_Reason__c = EscalatedReason;
        objAcc.Escalated_Date__c =EscalatedDate;
        UPDATE objAcc;

        System.debug('objAcc: '+objAcc);
        return objAcc;

    }
    
    public class WrapperForEscalate{
      	 @AuraEnabled
        public Account objAcc;
		@AuraEnabled
        public List<Escalate_Reason_Text_Value__mdt> escalateReasonMdtList;
        
    }

}