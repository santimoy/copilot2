/*
*       Account Last Activity Type Batch class
*
*       Methods in this class can be run from developer console:
*           1) Runonce:           AccountLastActivityTypeBatch.runThisJob();
*
*       Author  :   Wilson Ng 
*		Date	:	August 21, 2012
* 
*/
global class AccountLastActivityTypeBatch implements Database.Batchable<sObject>, Schedulable {

	// variables for managing multiple batch queries
	integer cycleNum = 0;
	List<string> cycleNameList = new List<string>();
	List<string> cycleQueryList = new List<string>();
	integer batchsize = 200;
	public string currentCycleName {
		get {
			return (cycleNum != -1 ? cycleNameList[cycleNum] : 'AccountLastActivityTypeBatch');
		}
	}
	
	// constructors
	global AccountLastActivityTypeBatch() {
		cycleNameList = new list<string> {
			'AccountLastActivityTypeBatch'
		};
		cycleQueryList = new list<string> {
			'select Id, AccountId from Event where (IsAllDayEvent=true AND ActivityDate in (YESTERDAY,TODAY)) OR (IsAllDayEvent=false AND ActivityDateTime in (YESTERDAY,TODAY)) order by AccountId'
		};
	}
	
	// batch start method
	global Database.QueryLocator start(Database.BatchableContext BC) {
		if(cycleNum >= cycleQueryList.size())
			throw new AccountLastActivityTypeException('Implementation error: cycle number is greater than the list of batch queries.');
		string query = cycleQueryList[cycleNum];
		return Database.getQueryLocator(query);
	}
	
	// batch execute method
	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		if(currentCycleName == cycleNameList[0])
			this.processYesterdayEvents((List<Event>)scope);
	}
	
	// batch finish method
	global void finish(Database.BatchableContext BC) {
		cycleNum++;
		if(cycleNum < cycleQueryList.size())
			scheduleNextCycle(this);
	}
	
	// scheduler execute method
	global void execute(SchedulableContext sc) {
		database.executebatch(this, this.batchsize);
	}
	public void scheduleNextCycle(Schedulable s) {
		DateTime in30secs = DateTime.now().addSeconds(30);
		
		String scheduleName = currentCycleName;
		// total jobname can be 64 characters long, so scheduleName cannot be more than 49 characters
		if (scheduleName.length() > 49)
			scheduleName = scheduleName.substring(0,49);
		String jobName = scheduleName+'-'+in30secs.format('yyyyddMMHHmmss');
		
		if(!Test.isRunningTest())
			String schedId = System.schedule(jobName, in30secs.format('s m H d M ? yyyy'), s);
	}
	
	// apex developer console commands:
	// 1) for running one time, immediately (actually in 30 seconds)
	global static void runThisJob() {
		AccountLastActivityTypeBatch m = new AccountLastActivityTypeBatch();
		m.scheduleNextCycle(m);
	}
	
	// process yesterday events
	private void processYesterdayEvents(list<Event> pastEventsList) {
		set<Id> acctIds = new set<Id>();
		for(Event e : pastEventsList)
			if(e.AccountId != null)
				acctIds.add(e.AccountId);
		ActivityTriggerHandler.updateAccount_LastActivityType(acctIds);
	}
	
	//
	// Exception class: AccountLastActivityTypeException
	//
	global class AccountLastActivityTypeException extends Exception {
	}
	
	//
	// test methods
	@IsTest
	static void testThisBatchable_AccountLastActivityType() {
		
		// create dummy data
		createDummyData(100);
		
		// test batch
		Test.StartTest();
		AccountLastActivityTypeBatch bat1 = new AccountLastActivityTypeBatch();
		ID batchprocessid1 = Database.executeBatch(bat1);
		Test.StopTest();
	}
	
	@IsTest
	static void testRunThisJob_AccountLastActivityType() {
		
		// create dummy data
		createDummyData(100);
		
		// test runthisjob
		Test.startTest();
		AccountLastActivityTypeBatch.runThisJob();
		Test.stopTest();
	}
	
	// creates dummy account and event data for test methods
	static void createDummyData(integer num) {
		
		list<Account> acctList = new list<Account>();
		for(integer i=0; i<num; i++)
			acctList.add(new Account(Name='testacct'+i));
		insert acctList;
		
		list<Event> eventList = new list<Event>();
		for(integer i=0; i<num; i++)
			eventList.add(new Event(Subject='testevent'+i, WhatId=acctList[i].Id, Type__c='Call', ActivityDateTime=system.now()-1, DurationInMinutes=30));
		insert eventList;
		
	}
	
}