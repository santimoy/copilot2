//
// 3-30-2018 TO BE DEPRECATED
//
public with sharing class Alexa_Connector {
	private static final string AWS_ACCESS_KEYID ='AKIAJGEAJLB7ABL22P7A';
	private static final string AWS_SECRET_KEY = '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';
	private static final string AWS_HTTP_VERB = 'GET';
	private static final string AWS_DOMAIN = 'awis.amazonaws.com';
	private static final integer AWS_HTTP_TIMEOUT = 60000;  
	
	private static final string ACTION = 'UrlInfo';
	private static final string RESPONSE_GROUP = 'Rank,RankByCountry';
	private static final string SIGNATURE_METHOD = 'HmacSHA256';
	private static final string SIGNATURE_VERSION = '2';  

	public static string testResponse = '';

	public string createQueryString(list<string> websites){
		
		string baseUrl = 'http://'+AWS_DOMAIN+'/';
		list<Alexa_Param> params = new list<Alexa_Param>();
		
		params.add(new Alexa_Param('AWSAccessKeyId', AWS_ACCESS_KEYID));
		params.add(new Alexa_Param('Action',ACTION));

		// single URL parameters
		//params.add(new Alexa_Param('ResponseGroup',RESPONSE_GROUP));
		//params.add(new Alexa_Param('Url', websites.get(0)) );

		// multi batch url parameters
		params.add(new Alexa_Param('UrlInfo.Shared.ResponseGroup',RESPONSE_GROUP));
		Integer i=1;
		for (string currWebsite : websites){
			params.add(new Alexa_Param(String.format('UrlInfo.{0}.Url', new string[]{String.valueOf(i)}), currWebsite));
			i++;
		}
		
		params.add(new Alexa_Param('SignatureMethod', SIGNATURE_METHOD));
		params.add(new Alexa_Param('SignatureVersion', SIGNATURE_VERSION));
		
		DateTime currTime = DateTime.Now();
		string timestampString = currTime.formatGmt('yyyy-MM-dd')+'T'+currTime.formatGmt('HH:mm:ss.SSS')+'Z';
		params.add(new Alexa_Param('Timestamp', timestampString));

		System.Debug('\n---params:'+params);

		string signature = createSignature(baseUrl, params);
		params.add(new Alexa_Param('Signature', signature)); 
		
		return createUrl(baseUrl, params);
	}
	
	public string createSignature(string baseUrl, list<Alexa_Param> params){
		
		string stringToSign = createSigningString( createUrl(baseUrl, params) );
		System.Debug('\n---stringToSign:'+stringToSign);
		
		blob signature = Crypto.generateMac(SIGNATURE_METHOD, Blob.valueOf(stringToSign), Blob.valueOf(AWS_SECRET_KEY));
		string signatureBase64 = EncodingUtil.base64Encode(signature);
		return signatureBase64;
			
	}
	
	public string createUrl(string baseUrl, list<Alexa_Param> params){
		string finalUrl = baseUrl + '?';
		params.sort();
		
		for (Alexa_Param currParam : params){
			finalUrl += currParam.toParamString() + '&';
		}
		
		finalUrl = finalUrl.substring(0, finalUrl.length()-1);
		System.Debug('\n---finalUrl:'+finalUrl);
		return finalUrl;
	}
	
	public string createSigningString(string targetUrl){
		
		integer queryIndex = targetUrl.indexOf('?');
		string paramString = targetUrl.subString(queryIndex+1, targetUrl.length());
		string stringToSign = String.format('{0}\n{1}\n/\n{2}', 
			new string[]{AWS_HTTP_VERB, AWS_DOMAIN, paramString});
		return stringToSign;
		
	}
	
	
	public string getUrlInfo(list<string> websites){
		
		Http http = new Http();
		HttpRequest req = new HttpRequest();
		req.setEndpoint(createQueryString(websites));
		req.setMethod(AWS_HTTP_VERB);
		req.setTimeout(AWS_HTTP_TIMEOUT);
		
		String body = '';
		if (Test.isRunningTest()){
			body = testResponse;
		}
		else{
			HttpResponse resp = http.send(req);
			if (resp.getStatusCode()!=200){
				throw new Alexa_Exception( String.format('An error occured while calling the Alexa web service. Status Code: {0} {1}\nRequest Url:{2}', 
					new string[]{ String.valueOf(resp.getStatusCode()), resp.getStatus(), req.getEndpoint() }) ); 
			}
			body = resp.getBody();
		}
		System.debug('\n---body:'+body);
		return body;
	}
	
	public DOM.XMLNode getUrlInfoDom(list<string> websites){
		string xmlString = getUrlInfo(websites);
		 
		DOM.Document doc = new DOM.Document();
		try{
			doc.load(xmlString);
			DOM.XMLNode root = doc.getRootElement();
			return root;
		}
		catch(XmlException e){
			throw new Alexa_Exception( String.format('An error occured while calling the Alexa web service. Error Message: {0}\n{1}', 
				new string[]{ String.valueOf(e.getMessage()), e.getStackTraceString() }) );
		}
	}
	
	// inner class
	public class Alexa_Exception extends Exception{}
	
}