/**
 * @author Dharmendra Karamchandani
 * @date 2018-04-19
 * @className IVPCompanyListViewCtlr
 * @group IVPCompanyListView
 *
 * @description contains methods to handle IVPCompanyListView component
 */
 public with sharing class IVPCompanyListViewCtlr {
    
    /*******************************************************************************************************
     * @description used as a wrapper to hold data to initialize IVPCompanyListView component
     * 				having list of listviews of account and custom setting record
     * @className InitData
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    public class InitData{
        @AuraEnabled public IVPListViewRESTResponse.LVResponse[] listViews;
        @AuraEnabled public IVP_SD_ListView__c ipvSDListView;
        @AuraEnabled public IVP_SD_ListView__c ipvOrgSDListView;
        
        public InitData(IVPListViewRESTResponse.LVResponse[] listViews, IVP_SD_ListView__c ipvSDListView, 
        				IVP_SD_ListView__c ipvOrgSDListView){
            this.listViews = listViews;
            this.ipvSDListView = ipvSDListView;
            this.ipvOrgSDListView = ipvOrgSDListView;
        }
    }
    
    
    /*******************************************************************************************************
     * @description used to return Custom data Wrapper 
     * @return InitData
     * @author Dharmendra Karamchandani
     * @note Don't change value of this property.
     */
    @AuraEnabled
    public static InitData getInitData(){
        return new InitData(IVPUtils.fetchListViewsForCurrentUser(),
                            IVPUtils.currentUserSetting(),
                            IVPUtils.currentOrgSetting());
    }
    
    /*******************************************************************************************************
     * @description used to maintain custom setting records of current user
     * @return void
     * @author Dharmendra Karamchandani
     * @param viewName this is name of List-view
     * @note Don't change value of this property.
     */
    @AuraEnabled
    public static void maintainDefautListView(String viewName){
    	if(String.isNotEmpty(viewName)){
	    	IVP_SD_ListView__c ipvSDListView = IVPUtils.currentUserSetting();
	    	ipvSDListView.List_View_Name__c = viewName;
	    	if(ipvSDListView.SetupOwnerId == null){
	    		ipvSDListView.SetupOwnerId = UserInfo.getUserId();
	    	}
    		upsert ipvSDListView;
    	}
    }
    
    @AuraEnabled
    public static Map<String, Object> maintainChartListView(String inputJson){
    	Map<String, Object> valueMap = (Map<String, Object>)JSON.deserializeUntyped(inputJson);
    	Map<String, Object> resultMap = IVPMetadataServiceProvider.maintainListView(valueMap);
   		return resultMap;
    }
}