//
// 3-30-2018 TO BE DEPRECATED
//
global class Alexa_ScheduledBatchProcess implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{
    public static final string COUNTRY_COMPARISON_ACCOUNTFIELD = 'Alexa_Country_Users__c';
    public static final string COUNTRY_COMPARISON_RESULTFIELD = 'Users';
    
    private static final string NS_URLINFORESP = 'http://alexa.amazonaws.com/doc/2005-10-05/';
    private static final string NS_URLINFORESP_RESP = 'http://awis.amazonaws.com/doc/2005-07-11';
    
    private String errorMessages;
    
    public String testWhereClause {get;set;}
    public Alexa_ScheduledBatchProcess(){
        testWhereClause = '';
    }
    
    // ----------------------------------------------------------------------------------------------------------------------------------
    // Batchable Interface methods 
    global Database.QueryLocator start(Database.BatchableContext BC){
        String queryString = 'Select Id, Name, Website__c, Alexa_Global_Rank__c, Alexa_Country__c, Alexa_Country_Rank__c, '+
            'Alexa_Country_Users__c, Alexa_Country_Page_Views__c From Account where Website__c != null'; 
        if (testWhereClause!='') queryString += ' and '+testWhereClause;
        System.Debug('queryString:'+queryString);
        return Database.getQueryLocator(queryString);   
    }

    global void execute(Database.BatchableContext BC, List<Account> batchList){
        errorMessages = '';
        Alexa_Connector alexa = new Alexa_Connector(); 
        
        list<string> websites = new list<string>();
        
        for (Account currAcct : batchList){
            websites.add(currAcct.Website__c);
        }
        DOM.XmlNode respDom = alexa.getUrlInfoDom(websites);
        system.debug('\n---VERBOSE---respDom:'+respDom);

        map<Id, Account> updateAccount = new map<Id, Account>();
        
        // we will assume that our response is ordred in the same order we requested the data
        Integer i=-1;
        if (respDom==null || respDom.getChildElements() ==null){
            collectErrorMessage('no response from alexa service for current batch of websites :'+websites);
        }
        else{ 
            for(DOM.XmlNode currResponse : respDom.getChildElements()){
                i++;
                
                if (currResponse==null || currResponse.getChildElement('UrlInfoResult',NS_URLINFORESP_RESP) == null || 
                    currResponse.getChildElement('UrlInfoResult',NS_URLINFORESP_RESP).getChildElement('Alexa',NS_URLINFORESP_RESP) == null ||
                    currResponse.getChildElement('UrlInfoResult',NS_URLINFORESP_RESP).getChildElement('Alexa',NS_URLINFORESP_RESP).getChildElement('TrafficData',NS_URLINFORESP_RESP) == null ){
                        
                    collectErrorMessage('incomplete response from alexa service for single website :'+websites.get(i));
                    continue;
                }
                
                // traverse down into the DOM tree to the Traffic Data Node
                DOM.XmlNode trafficDataNode = currResponse.getChildElement('UrlInfoResult',NS_URLINFORESP_RESP)
                    .getChildElement('Alexa',NS_URLINFORESP_RESP).getChildElement('TrafficData',NS_URLINFORESP_RESP);
                //system.debug('\n---VERBOSE---trafficDataNode:'+trafficDataNode);                  
                    
                String websiteCanonical = trafficDataNode.getChildElement('DataUrl', NS_URLINFORESP_RESP).getText();
                if (websiteCanonical.endsWith('/')) websiteCanonical.substring(0, websiteCanonical.length()-1);
                system.debug('\n---websiteCanonical:'+websiteCanonical);
                
                Account matchAccount = batchList.get(i);
                if(matchAccount!=null){
                    System.Debug('\n---Match Found:'+matchAccount.Name);
                    parseNodeText(matchAccount, 'Alexa_Website_Canonical__c', trafficDataNode.getChildElement('DataUrl',NS_URLINFORESP_RESP), 'Text');
                    parseNodeText(matchAccount, 'Alexa_Global_Rank__c', trafficDataNode.getChildElement('Rank',NS_URLINFORESP_RESP), 'Decimal');
    
                    matchAccount.put(COUNTRY_COMPARISON_ACCOUNTFIELD, -1);
                    
                    // iterate over the Country Nodes
                    for (DOM.XmlNode currCountryNode : trafficDataNode.getChildElement('RankByCountry', NS_URLINFORESP_RESP).getChildElements() ){
                        //system.debug('\n---VERBOSE---currCountryNode:'+currCountryNode);                  
                        
                        string nodeCompareValueString = currCountryNode.getChildElement('Contribution',NS_URLINFORESP_RESP)
                            .getChildElement(COUNTRY_COMPARISON_RESULTFIELD,NS_URLINFORESP_RESP).getText();
                        Decimal nodeCompareValue = getAlexaPercentageValue(nodeCompareValueString);
                        
                        if ( (Decimal) matchAccount.get(COUNTRY_COMPARISON_ACCOUNTFIELD) < nodeCompareValue ) {
                            
                            parseNodeAttrib(matchAccount, 'Alexa_Country__c', currCountryNode, 'Code', '');
                            parseNodeText(matchAccount, 'Alexa_Country_Rank__c', currCountryNode.getChildElement('Rank',NS_URLINFORESP_RESP), 'Decimal');
                            
                            DOM.XmlNode contribNode = currCountryNode.getChildElement('Contribution',NS_URLINFORESP_RESP);
                            if (contribNode != null){
                                parseNodeText(matchAccount, 'Alexa_Country_Page_Views__c', contribNode.getChildElement('PageViews',NS_URLINFORESP_RESP), 'Percent');
                                parseNodeText(matchAccount, 'Alexa_Country_Users__c', contribNode.getChildElement('Users',NS_URLINFORESP_RESP), 'Percent');
                            }                       
                        }
                    }
                    if (matchAccount.get(COUNTRY_COMPARISON_ACCOUNTFIELD) == -1) matchAccount.put(COUNTRY_COMPARISON_ACCOUNTFIELD, null);
                    updateAccount.put(matchAccount.Id, matchAccount);
                }
            }
        }
        if (updateAccount.size()>0) update updateAccount.values();
        
        if (errorMessages!='') throw new Alexa_ScheduleBatchException(errorMessages);
        /**/
    }

    private void collectErrorMessage(string errorString){
        System.debug(errorString);
        errorMessages += '\n'+errorString;
    }

    global void finish(Database.BatchableContext BC){
    }

    // ----------------------------------------------------------------------------------------------------------------------------------
    // methods needed for the Schedulable interface
    global void execute(SchedulableContext sc){
        Database.executebatch(this, 5); // this is the max batch size for the alexa service
    }

    // ----------------------------------------------------------------------------------------------------------------------------------
    // utility methods for parsing alexa returns
    public Decimal getAlexaPercentageValue(string percentageValue){
        percentageValue = percentageValue.replace('%','');
        Decimal nodeValue = null;
        try{ 
            if (percentageValue!=null && percentageValue.trim()!='') nodeValue = Decimal.valueOf(percentageValue);
        }
        catch(Exception e){
            // don't really need to do anything here - will assume that there was some decimal conversion error 
            collectErrorMessage('Exception occurred during decimal coversion:'+e.getMessage()+' \n percentageValue:'+percentageValue);
        }
        return nodeValue;
    }
    public void parseNodeAttrib(Account matchAccount, string accountField, DOM.XmlNode sourceNode, string attribName, string nameSpace){
        try{
            matchAccount.put(accountField, sourceNode.getAttribute(attribName, nameSpace));
        }
        catch(Exception ex){
            // matchAccount.put(accountField, null);  // not so sure we should clear out the old data...
            // ignore errors, but put them to the debug log if anyone is listening
            collectErrorMessage('Error parsing for attribute '+attribName+' : '+ex.getMessage()+'\n'+ex.getStackTraceString());
        }
    }
    public void parseNodeText(Account matchAccount, string accountField, DOM.XmlNode sourceNode, string targetType){
        try{
            string nodeText = sourceNode.getText();
            matchAccount.put(accountField, null);
            if (nodeText!=null && nodeText!=''){
                if (targetType == 'Decimal'){
                    matchAccount.put(accountField, Decimal.valueOf(nodeText));
                }
                else if(targetType == 'Percent'){ 
                    matchAccount.put(accountField, getAlexaPercentageValue(nodeText));
                }
                else{
                    matchAccount.put(accountField, nodeText);
                }
            }
        }
        catch(Exception ex){
            // matchAccount.put(accountField, null);  // not so sure we should clear out the old data...
            // ignore errors, but put them to the debug log if anyone is listening
            collectErrorMessage('Error parsing for text for '+sourceNode.getName()+' : '+ex.getMessage()+'\n'+ex.getStackTraceString());
        }
    }
    
    public class Alexa_ScheduleBatchException extends Exception{}
    
    // ----------------------------------------------------------------------------------------------------------------------------------
    // test classes
    static testMethod void testThisClass(){
        Account testAcct1 = new Account(Name='Microsoft', Website__c='http://www.microsoft.com');
        Account testAcct2 = new Account(Name='Yahoo', Website__c='http://www.yahoo.com');
        
        insert new Account[]{testAcct1, testAcct2};     
        
        Alexa_Connector.testResponse = '<?xml version="1.0"?><aws:UrlInfoResponse xmlns:aws="http://alexa.amazonaws.com/doc/2005-10-05/"><aws:Response xmlns:aws="http://awis.amazonaws.com/doc/2005-07-11"><aws:OperationRequest><aws:RequestId>491863c6-f941-ddbb-73bf-171fd11adbbe</aws:RequestId></aws:OperationRequest><aws:UrlInfoResult><aws:Alexa><aws:TrafficData><aws:DataUrl type="canonical">microsoft.com/</aws:DataUrl><aws:Rank>32</aws:Rank><aws:RankByCountry><aws:Country Code="US"><aws:Rank>37</aws:Rank><aws:Contribution><aws:PageViews>19.6%</aws:PageViews><aws:Users>17.3%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="CN"><aws:Rank>72</aws:Rank><aws:Contribution><aws:PageViews>6.4%</aws:PageViews><aws:Users>8.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="IN"><aws:Rank>20</aws:Rank><aws:Contribution><aws:PageViews>8.4%</aws:PageViews><aws:Users>7.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="JP"><aws:Rank>43</aws:Rank><aws:Contribution><aws:PageViews>5.7%</aws:PageViews><aws:Users>5.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="DE"><aws:Rank>36</aws:Rank><aws:Contribution><aws:PageViews>4.3%</aws:PageViews><aws:Users>3.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="FR"><aws:Rank>32</aws:Rank><aws:Contribution><aws:PageViews>2.9%</aws:PageViews><aws:Users>3.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="GB"><aws:Rank>29</aws:Rank><aws:Contribution><aws:PageViews>4.0%</aws:PageViews><aws:Users>3.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="BR"><aws:Rank>33</aws:Rank><aws:Contribution><aws:PageViews>3.1%</aws:PageViews><aws:Users>3.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="RU"><aws:Rank>40</aws:Rank><aws:Contribution><aws:PageViews>3.2%</aws:PageViews><aws:Users>2.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="MX"><aws:Rank>22</aws:Rank><aws:Contribution><aws:PageViews>2.0%</aws:PageViews><aws:Users>2.4%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="IT"><aws:Rank>45</aws:Rank><aws:Contribution><aws:PageViews>1.9%</aws:PageViews><aws:Users>2.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="CA"><aws:Rank>35</aws:Rank><aws:Contribution><aws:PageViews>2.3%</aws:PageViews><aws:Users>2.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="ES"><aws:Rank>40</aws:Rank><aws:Contribution><aws:PageViews>1.8%</aws:PageViews><aws:Users>1.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="IR"><aws:Rank>35</aws:Rank><aws:Contribution><aws:PageViews>1.4%</aws:PageViews><aws:Users>1.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="KR"><aws:Rank>40</aws:Rank><aws:Contribution><aws:PageViews>1.2%</aws:PageViews><aws:Users>1.6%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="AU"><aws:Rank>31</aws:Rank><aws:Contribution><aws:PageViews>1.5%</aws:PageViews><aws:Users>1.3%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="TR"><aws:Rank>46</aws:Rank><aws:Contribution><aws:PageViews>1.1%</aws:PageViews><aws:Users>1.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="PK"><aws:Rank>28</aws:Rank><aws:Contribution><aws:PageViews>1.0%</aws:PageViews><aws:Users>1.1%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="NL"><aws:Rank>28</aws:Rank><aws:Contribution><aws:PageViews>1.2%</aws:PageViews><aws:Users>1.1%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="ID"><aws:Rank>50</aws:Rank><aws:Contribution><aws:PageViews>1.0%</aws:PageViews><aws:Users>1.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="TH"><aws:Rank>25</aws:Rank><aws:Contribution><aws:PageViews>1.4%</aws:PageViews><aws:Users>1.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="PL"><aws:Rank>50</aws:Rank><aws:Contribution><aws:PageViews>1.1%</aws:PageViews><aws:Users>1.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="AR"><aws:Rank>27</aws:Rank><aws:Contribution><aws:PageViews>0.8%</aws:PageViews><aws:Users>1.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="TW"><aws:Rank>37</aws:Rank><aws:Contribution><aws:PageViews>0.9%</aws:PageViews><aws:Users>0.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="ZA"><aws:Rank>23</aws:Rank><aws:Contribution><aws:PageViews>0.9%</aws:PageViews><aws:Users>0.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="DZ"><aws:Rank>24</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="SA"><aws:Rank>49</aws:Rank><aws:Contribution><aws:PageViews>0.7%</aws:PageViews><aws:Users>0.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="EG"><aws:Rank>43</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="CO"><aws:Rank>21</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="BE"><aws:Rank>20</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="VE"><aws:Rank>29</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="PE"><aws:Rank>20</aws:Rank><aws:Contribution><aws:PageViews>0.5%</aws:PageViews><aws:Users>0.6%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="NG"><aws:Rank>33</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.6%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="MY"><aws:Rank>43</aws:Rank><aws:Contribution><aws:PageViews>0.5%</aws:PageViews><aws:Users>0.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="AT"><aws:Rank>36</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="O"><aws:Rank/><aws:Contribution><aws:PageViews>15.0%</aws:PageViews><aws:Users>15.1%</aws:Users></aws:Contribution></aws:Country></aws:RankByCountry></aws:TrafficData></aws:Alexa></aws:UrlInfoResult><aws:ResponseStatus xmlns:aws="http://alexa.amazonaws.com/doc/2005-10-05/"><aws:StatusCode>Success</aws:StatusCode></aws:ResponseStatus></aws:Response><aws:Response xmlns:aws="http://awis.amazonaws.com/doc/2005-07-11"><aws:OperationRequest><aws:RequestId>491863c6-f941-ddbb-73bf-171fd11adbbe</aws:RequestId></aws:OperationRequest><aws:UrlInfoResult><aws:Alexa>  <aws:TrafficData><aws:DataUrl type="canonical">yahoo.com/</aws:DataUrl><aws:Rank>4</aws:Rank><aws:RankByCountry><aws:Country Code="US"><aws:Rank>4</aws:Rank><aws:Contribution><aws:PageViews>35.3%</aws:PageViews><aws:Users>32.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="IN"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>8.8%</aws:PageViews><aws:Users>8.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="TW"><aws:Rank>1</aws:Rank><aws:Contribution><aws:PageViews>11.8%</aws:PageViews><aws:Users>3.8%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="IR"><aws:Rank>2</aws:Rank><aws:Contribution><aws:PageViews>3.0%</aws:PageViews><aws:Users>3.3%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="GB"><aws:Rank>7</aws:Rank><aws:Contribution><aws:PageViews>2.3%</aws:PageViews><aws:Users>3.3%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="ID"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>2.7%</aws:PageViews><aws:Users>2.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="CN"><aws:Rank>29</aws:Rank><aws:Contribution><aws:PageViews>2.1%</aws:PageViews><aws:Users>2.4%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="BR"><aws:Rank>9</aws:Rank><aws:Contribution><aws:PageViews>1.6%</aws:PageViews><aws:Users>2.4%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="FR"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>1.7%</aws:PageViews><aws:Users>2.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="CA"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>1.9%</aws:PageViews><aws:Users>2.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="IT"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>1.3%</aws:PageViews><aws:Users>2.1%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="MX"><aws:Rank>6</aws:Rank><aws:Contribution><aws:PageViews>1.7%</aws:PageViews><aws:Users>2.1%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="DE"><aws:Rank>10</aws:Rank><aws:Contribution><aws:PageViews>1.3%</aws:PageViews><aws:Users>1.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="NG"><aws:Rank>4</aws:Rank><aws:Contribution><aws:PageViews>1.2%</aws:PageViews><aws:Users>1.4%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="ES"><aws:Rank>8</aws:Rank><aws:Contribution><aws:PageViews>0.9%</aws:PageViews><aws:Users>1.4%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="JP"><aws:Rank>22</aws:Rank><aws:Contribution><aws:PageViews>1.0%</aws:PageViews><aws:Users>1.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="KR"><aws:Rank>9</aws:Rank><aws:Contribution><aws:PageViews>1.0%</aws:PageViews><aws:Users>1.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="PH"><aws:Rank>4</aws:Rank><aws:Contribution><aws:PageViews>1.5%</aws:PageViews><aws:Users>1.2%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="AU"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>0.9%</aws:PageViews><aws:Users>1.1%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="PK"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>0.9%</aws:PageViews><aws:Users>1.1%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="EG"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>0.7%</aws:PageViews><aws:Users>1.0%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="HK"><aws:Rank>1</aws:Rank><aws:Contribution><aws:PageViews>1.7%</aws:PageViews><aws:Users>0.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="RO"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>0.7%</aws:PageViews><aws:Users>0.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="MY"><aws:Rank>6</aws:Rank><aws:Contribution><aws:PageViews>0.7%</aws:PageViews><aws:Users>0.9%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="VN"><aws:Rank>7</aws:Rank><aws:Contribution><aws:PageViews>0.5%</aws:PageViews><aws:Users>0.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="AR"><aws:Rank>7</aws:Rank><aws:Contribution><aws:PageViews>0.5%</aws:PageViews><aws:Users>0.7%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="TH"><aws:Rank>7</aws:Rank><aws:Contribution><aws:PageViews>0.5%</aws:PageViews><aws:Users>0.6%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="SG"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>0.6%</aws:PageViews><aws:Users>0.6%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="SA"><aws:Rank>7</aws:Rank><aws:Contribution><aws:PageViews>0.4%</aws:PageViews><aws:Users>0.6%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="BD"><aws:Rank>4</aws:Rank><aws:Contribution><aws:PageViews>0.4%</aws:PageViews><aws:Users>0.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="RU"><aws:Rank>31</aws:Rank><aws:Contribution><aws:PageViews>0.4%</aws:PageViews><aws:Users>0.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="ZA"><aws:Rank>5</aws:Rank><aws:Contribution><aws:PageViews>0.3%</aws:PageViews><aws:Users>0.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="CO"><aws:Rank>6</aws:Rank><aws:Contribution><aws:PageViews>0.3%</aws:PageViews><aws:Users>0.5%</aws:Users></aws:Contribution></aws:Country><aws:Country Code="O"><aws:Rank/><aws:Contribution><aws:PageViews>9.3%</aws:PageViews><aws:Users>12.4%</aws:Users></aws:Contribution></aws:Country></aws:RankByCountry></aws:TrafficData></aws:Alexa></aws:UrlInfoResult><aws:ResponseStatus xmlns:aws="http://alexa.amazonaws.com/doc/2005-10-05/"><aws:StatusCode>Success</aws:StatusCode></aws:ResponseStatus></aws:Response></aws:UrlInfoResponse>';
        
        Alexa_ScheduledBatchProcess testClass = new Alexa_ScheduledBatchProcess();
        Test.startTest();
        SchedulableContext sc = null;
        testClass.execute(sc);
        Test.stopTest();
        
        testAcct1 = [select Id, Name, Website__c, Alexa_Global_Rank__c, Alexa_Country__c, Alexa_Country_Rank__c, 
            Alexa_Country_Users__c, Alexa_Country_Page_Views__c From Account where id = :testAcct1.Id];
        System.assertEquals(32, testAcct1.Alexa_Global_Rank__c);
        System.assertEquals(37, testAcct1.Alexa_Country_Rank__c);
        System.assertEquals('US', testAcct1.Alexa_Country__c);

        testAcct2 = [select Id, Name, Website__c, Alexa_Global_Rank__c, Alexa_Country__c, Alexa_Country_Rank__c, 
            Alexa_Country_Users__c, Alexa_Country_Page_Views__c From Account where id = :testAcct2.Id];
        System.assertEquals(4, testAcct2.Alexa_Global_Rank__c);
        System.assertEquals(4, testAcct2.Alexa_Country_Rank__c);
        System.assertEquals('US', testAcct2.Alexa_Country__c);
    }
}