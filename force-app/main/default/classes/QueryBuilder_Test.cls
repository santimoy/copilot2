@isTest
public class QueryBuilder_Test {
    
    @TestSetup
    static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@ivptest.com');
        insert u1;
        
        Intelligence_User_Preferences__c  intUserRec = new Intelligence_User_Preferences__c ();
        intUserRec.CreatedById = u1.Id;
        intUserRec.Display_Columns__c = 'Name (ivp_name);City (ivp_city);Country (ivp_country);Intent 3M Growth % (intent_to_buy_score_3m_growth)';
        intUserRec.Sourcing_Signal_Category_Preferences__c = 'acquisition-acquirer, competitive-challenge, Customer, employee-growth, form-10k';
        intUserRec.My_Dash_Signal_Preferences__c = 'cquisition-acquirer, acquisition-acquiree, award, cb-new-company, ceo-change, competitive-challenge';
        intUserRec.User_Id__c = string.valueOf(u1.id) ;
        insert intUserRec;
        
        Intelligence_Field__c intField = new Intelligence_Field__c();
        intField.Active__c = true;
        intField.UI_Label__c = 'Description';
        intField.Keyword_Criteria__c = true;
        intField.Target_Field_Type__c = 'TEXTAREA';
        intField.DB_Column__c = 'ct.companies_cross_data_full.ivp_stage';
        intField.Filterable_from_UI__c = false;
        intField.Default_display_field__c = true;
        intField.QBFlt_Is_Filter__c = true;
        intField.QBFlt_no_filter__c = false;
        intField.QBFlt_LOVs__c = 'ct.companies_cross_data_full.ivp_stage';
        insert intField;
        
        Intelligence_Field__c intField1 = new Intelligence_Field__c();
        intField1.Active__c = true;
        intField1.UI_Label__c = 'Verticals';
        intField1.Keyword_Criteria__c = true;
        intField1.Target_Field_Type__c = 'STRING';
        intField1.DB_Column__c = 'ct.companies_cross_data_full.ivp_industry_list';
        intField1.Filterable_from_UI__c = false;
        intField1.Default_display_field__c = false;
        intField1.QBFlt_Is_Filter__c = true;
        intField1.QBFlt_no_filter__c = false;
        insert intField1;
    }
    
    public static testmethod void Method1()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
            sourcingPreference.Name__c = 'Test';
            sourcingPreference.Include_Exclude__c = 'Include';
            sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
            insert sourcingPreference;
            
            test.startTest();
            
            QueryBuilder qb = new QueryBuilder();
            QueryBuilder.fetchRecord(sourcingPreference.Id);
            QueryBuilder.getKeywordQuery();
            QueryBuilder.searchInvestorRecords_Apex('Test');
            QueryBuilder.searchInvestorRecords_Apex('Test');
            QueryBuilder.fetchFilters();
            QueryBuilder.saveSourcingPreference(sourcingPreference);
            
            test.stopTest();
            
        }
    }
    
     public static testmethod void Method2()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            test.startTest();
            QueryBuilder qb = new QueryBuilder();
            QueryBuilder.searchInvestorRecords_Apex('Test');
            QueryBuilder.fetchFilters();
            test.stopTest();
        }
    }
    
     public static testmethod void Method3()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpExceptionGenerator());
            
            Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
            sourcingPreference.Name__c = 'Test';
            sourcingPreference.Include_Exclude__c = 'Include';
            sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
            insert sourcingPreference;
            
            test.startTest();
            
            QueryBuilder qb = new QueryBuilder();
            QueryBuilder.fetchRecord(sourcingPreference.Id);
            QueryBuilder.getKeywordQuery();
            QueryBuilder.searchInvestorRecords_Apex('Test');
            QueryBuilder.fetchFilters();
            QueryBuilder.saveSourcingPreference(sourcingPreference);
            
            test.stopTest();
        }
    }
    
    
}