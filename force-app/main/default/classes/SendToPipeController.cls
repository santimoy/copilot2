public without sharing class SendToPipeController {


   @AuraEnabled
   public static  List<String> fetchPick_PipeStatus()
    {
      List<String> lstData = new List<String>();
      Schema.DescribeFieldResult fieldResult = Opportunity.Pipe_Status__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      for(Schema.PicklistEntry s:ple){

      lstData.add(s.getLabel());

      } 
      return lstData;
    }

	 @AuraEnabled
    public static String fetchInitData(String recId)
    {
    	SendToPipeInitData sendToPipe = new SendToPipeInitData();
    	List<Opportunity> lstOpenOpp = [SELECT Id FROM Opportunity WHERE AccountId =: recId AND IsClosed = FALSE AND OwnerId =: UserInfo.getUserId() LIMIT 1];
    	sendToPipe.isHavingOpenOpp = !lstOpenOpp.isEmpty();
    	sendToPipe.accountData=[Select Id,Name,NumberOfEmployees,Short_Description__c,Deal_Type__c,
    	BillingStreet,BillingCity,BillingStateCode,BillingPostalCode,BillingCountry,
    	Next_Action__c,Next_Action_Date__c,
    	ShippingStreet,ShippingCity,ShippingStateCode,ShippingPostalCode,ShippingCountry,
    	Company_Quality__c,Deal_Size__c,Deal_Imminence__c,Capital_History__c,
    	Current_EBITDA__c,Current_Revenue__c,Prior_Year_Revenue__c,
        Next_Year_EBITDA__c,Next_Year_Revenue__c,Prior_Year_EBITDA__c,PriorPriorEBITDA__c,
        PriorPriorRevenue__c,Total_Funding_Raised__c,DWH_Total_Funding_Raised__c from Account where Id =:recId limit 1];
        sendToPipe.dealTypeOptions = getPicklistValues('Opportunity','Deal_Type__c');
		sendToPipe.companyQualityOptions = getPicklistValues('Opportunity','Company_Quality__c');
		sendToPipe.dealSizeOptions = getPicklistValues('Opportunity','Deal_Size__c');
		sendToPipe.dealImminenceOptions = getPicklistValues('Opportunity','Deal_Imminence__c');
		sendToPipe.capitalHistoryOptions = getPicklistValues('Opportunity','Capital_History__c');
		sendToPipe.stageOptions = getPicklistValues('Opportunity','Old_Stage__c');
    sendToPipe.pipeStatusOptions = getPicklistValues('Opportunity', 'Pipe_Status__c');
        sendToPipe.lstRevenueTypeOptions = getPicklistValues('Opportunity','Revenue_Type__c');
        sendToPipe.countryOptions = fetchCountries();
		sendToPipe.currentUser = [Select Id,Primary_Partner__c,Primary_Partner__r.Name from User where Id =: UserInfo.getUserId() limit 1];
		sendToPipe.objFinancialData = new FinancialData();
        Date d = Date.today();
        Integer currentYearInt = (d.year());
        String priorPriorYear =String.valueOf(currentYearInt-2);
        String priorYear =String.valueOf(currentYearInt-1);
        String currentYear =String.valueOf(currentYearInt);
        String nextYear =String.valueOf(currentYearInt+1);
        List<Financials__c> financials = [Select Id,Account__c,Revenue__c,EBITDA__c,Year_Text__c, Net_Retention__c,
        											Gross_Retention__c, Revenue_Type__c from Financials__c where Account__c =: recId AND (Year_Text__c =:priorPriorYear OR Year_Text__c =:priorYear OR Year_Text__c =:currentYear OR Year_Text__c =:nextYear)];
        for(Financials__c f:financials)
        {
            sendToPipe.objFinancialData.revenueType = f.Revenue_Type__c;
        	if(f.Year_Text__c == priorPriorYear) {
            sendToPipe.objFinancialData.priorPriorRevenueValue = f.Revenue__c;
            sendToPipe.objFinancialData.priorPriorEbitaValue = f.EBITDA__c;
            sendToPipe.objFinancialData.priorPriorNetValue = f.Net_Retention__c;
            sendToPipe.objFinancialData.priorPriorGrossValue = f.Gross_Retention__c;
          }
          if(f.Year_Text__c ==priorYear)
        	{ 
        		sendToPipe.objFinancialData.priorRevenueValue = f.Revenue__c;
        		sendToPipe.objFinancialData.priorEbitaValue = f.EBITDA__c;
        		sendToPipe.objFinancialData.priorNetValue = f.Net_Retention__c;
	            sendToPipe.objFinancialData.priorGrossValue = f.Gross_Retention__c;
        	}
        	else if(f.Year_Text__c ==currentYear)
        	{
        		sendToPipe.objFinancialData.currentRevenueValue = f.Revenue__c;
        		sendToPipe.objFinancialData.currentEbitaValue = f.EBITDA__c;
        		sendToPipe.objFinancialData.currentNetValue = f.Net_Retention__c;
	            sendToPipe.objFinancialData.currentGrossValue = f.Gross_Retention__c;
        	}
        	else if(f.Year_Text__c ==nextYear)
        	{
        		sendToPipe.objFinancialData.nextRevenueValue = f.Revenue__c;
        		sendToPipe.objFinancialData.nextEbitaValue = f.EBITDA__c;
        		sendToPipe.objFinancialData.nextNetValue = f.Net_Retention__c;
	            sendToPipe.objFinancialData.nextGrossValue = f.Gross_Retention__c;
        	}
        }
	    
        DependentPicklists handler = new DependentPicklists();
        Map<String, List<String>> dependentResults = handler.controllingToDependentValues(SendToPipeController.getPicklistValuesTemp('User','CountryCode'),SendToPipeController.getPicklistValuesTemp('User','StateCode'));
        Map<String,String> countryValues = getPicklistValuesMap('User','CountryCode');
        Map<String,String> stateValues = getPicklistValuesMap('User','StateCode');
        Map<String, List<String>> convertedResults = new Map<String, List<String>>();
        for(String countryVal: dependentResults.keySet())
        {
          List<String> tempList = new List<String>();
          tempList.add('-None-');
          tempList.addAll(dependentResults.get(countryVal));
          convertedResults.put(countryValues.get(countryVal),tempList);
        }
        sendToPipe.countryToStates =convertedResults;
        return JSON.serialize(sendToPipe);
    }

    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
	  	List<String> lstPickvals=new List<String>();
	  	lstPickvals.add('');
	  	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
	  	Sobject Object_name = targetType.newSObject();
	  	Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
	    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
	    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
	    List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
	    for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
	      lstPickvals.add(a.getValue());//add the value  to our final list
   		}
  		return lstPickvals;
	}
  public static Map<String,String> getPicklistValuesMap(String ObjectApi_name,String Field_name){ 
      Map<String,String> mapPickvals=new Map<String,String>();
      Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
      Sobject Object_name = targetType.newSObject();
      Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
        mapPickvals.put(a.getValue(),a.getLabel());//add the value  to our final list
      }
      return mapPickvals;
  }

  public static List<Schema.PicklistEntry> getPicklistValuesTemp(String ObjectApi_name,String Field_name){ 
      List<String> lstPickvals=new List<String>();
      lstPickvals.add('');
      Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
      Sobject Object_name = targetType.newSObject();
      Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      return field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
  }
	@AuraEnabled
    public static void saveData(String recordId,String oppName,
    	String oppDescription,String accShippingStreet,String accShippingCity,
    	String accShippingState,String accShippingCountry,String accShippingPostalCode,String partnerObject,
    	String opportunityTeamMembers,String nextAction,String nextActionDate,
    	String dealTypeValue,String companyQualityValue,String dealSizeValue,String dealImminenceValue,
    	String capitalHistoryValue,String stageValue, String financialDataJSON, 
    	Decimal totalFundingRaisedValue,
    	String inboundNameOfBank,String interest,Decimal numberOfEmployees,String otherKPI, String strRevenueType,
      String pipeStatus, String investors)
    {
        FinancialData objFinancialData = (FinancialData) JSON.deserialize(financialDataJSON, FinancialData.class);
    	RecordType oppRecId = [Select Id from RecordType where Name ='Investment Opportunity' limit 1];
       Account accQuery = [Select Id,OwnerId from Account where Id =:recordId limit 1];
       Account accountToUpdate = new Account(Id = recordId);
       accountToUpdate.ShippingStreet = accShippingStreet;
       accountToUpdate.ShippingCity = accShippingCity;
      accountToUpdate.ShippingCountry = accShippingCountry;
       accountToUpdate.ShippingPostalCode = accShippingPostalCode;
       accountToUpdate.Total_Funding_Raised__c=totalFundingRaisedValue;
       Date d = Date.today();
       	
        Integer currentYearInt = (d.year());
        String priorPriorYear =String.valueOf(currentYearInt-2);
  	    String priorYear =String.valueOf(currentYearInt-1);
  	    String currentYear =String.valueOf(currentYearInt);
  	    String nextYear =String.valueOf(currentYearInt+1);
        Boolean hasPriorPriorYear = false;
  	    Boolean hasPriorYear = false;
  	    Boolean hasCurrentYear = false;
  	    Boolean hasNextYear = false;
       	List<Financials__c> financials = [Select Id,Account__c,Revenue__c,EBITDA__c,Year_Text__c from Financials__c where Account__c =: recordId AND (Year_Text__c =:priorPriorYear OR Year_Text__c =:priorYear OR Year_Text__c =:currentYear OR Year_Text__c =:nextYear)];
        for(Financials__c f:financials)
        {
            f.Revenue_Type__c = strRevenueType;
            if(f.Year_Text__c == priorPriorYear) {
                f.Revenue__c = objFinancialData.priorPriorRevenueValue;
                f.EBITDA__c = objFinancialData.priorPriorEbitaValue;
                f.Net_Retention__c = objFinancialData.priorPriorNetValue;
                f.Gross_Retention__c = objFinancialData.priorPriorGrossValue;
                hasPriorPriorYear = true;
            }
        	else if(f.Year_Text__c == priorYear)
        	{
                f.Revenue__c = objFinancialData.priorRevenueValue;
                f.EBITDA__c = objFinancialData.priorEbitaValue;
                f.Net_Retention__c = objFinancialData.priorNetValue;
                f.Gross_Retention__c = objFinancialData.priorGrossValue;
        		hasPriorYear = true;
        	}
        	else if(f.Year_Text__c ==currentYear)
        	{
                f.Revenue__c = objFinancialData.currentRevenueValue;
                f.EBITDA__c = objFinancialData.currentEbitaValue;
                f.Net_Retention__c = objFinancialData.currentNetValue;
                f.Gross_Retention__c = objFinancialData.CurrentGrossValue;
        		hasCurrentYear = true;
        	}
        	else if(f.Year_Text__c ==nextYear)
        	{
                f.Revenue__c = objFinancialData.nextRevenueValue;
                f.EBITDA__c = objFinancialData.nextEbitaValue;
                f.Net_Retention__c = objFinancialData.nextNetValue;
                f.Gross_Retention__c = objFinancialData.nextGrossValue;
        		hasNextYear = true;
        	}
        }
        if(!hasPriorPriorYear && (objFinancialData.priorPriorRevenueValue != null || objFinancialData.priorPriorEbitaValue != null || objFinancialData.priorPriorNetValue != null || objFinancialData.priorPriorGrossValue != null)){
          financials.add(new Financials__c(Account__c =recordId,Year_Text__c = priorPriorYear,Revenue__c=objFinancialData.priorPriorRevenueValue,EBITDA__c=objFinancialData.priorPriorEbitaValue,
                        Revenue_Type__c = strRevenueType, Net_Retention__c = objFinancialData.priorPriorNetValue, Gross_Retention__c = objFinancialData.priorPriorGrossValue));
        }
        if(!hasPriorYear && (objFinancialData.priorRevenueValue != null || objFinancialData.priorEbitaValue != null || objFinancialData.priorNetValue != null || objFinancialData.priorGrossValue != null))
        {
        	financials.add(new Financials__c(Account__c =recordId,Year_Text__c = priorYear,Revenue__c=objFinancialData.priorRevenueValue,EBITDA__c=objFinancialData.priorEbitaValue,
        	                Revenue_Type__c = strRevenueType, Net_Retention__c = objFinancialData.priorNetValue, Gross_Retention__c = objFinancialData.priorGrossValue));
        }
        if(!hasCurrentYear && (objFinancialData.currentRevenueValue != null || objFinancialData.currentRevenueValue != null || objFinancialData.currentNetValue != null || objFinancialData.currentGrossValue != null))
        {
        	financials.add(new Financials__c(Account__c =recordId,Year_Text__c = currentYear,Revenue__c=objFinancialData.currentRevenueValue,EBITDA__c=objFinancialData.currentEbitaValue,
        	                Revenue_Type__c = strRevenueType, Net_Retention__c = objFinancialData.currentNetValue, Gross_Retention__c = objFinancialData.currentGrossValue));

        }
        if(!hasNextYear && (objFinancialData.nextRevenueValue != null || objFinancialData.nextEbitaValue != null || objFinancialData.nextNetValue != null || objFinancialData.nextGrossValue != null))
        {
        	financials.add(new Financials__c(Account__c =recordId,Year_Text__c = nextYear,Revenue__c=objFinancialData.nextRevenueValue,EBITDA__c=objFinancialData.nextEbitaValue,
        	                Revenue_Type__c = strRevenueType, Net_Retention__c = objFinancialData.nextNetValue, Gross_Retention__c = objFinancialData.nextGrossValue));

        }
        upsert financials;
        //accountToUpdate.Pr=priorRevenueValue;
       	//accountToUpdate.Current_Revenue__c=currentRevenueValue;
       	//accountToUpdate.Next_Year_Revenue__c=nextRevenueValue;
       	//accountToUpdate.Prior_Year_EBITDA__c=priorEbitaValue;
       	//accountToUpdate.Current_EBITDA__c=currentEbitaValue;
       	//accountToUpdate.Next_Year_EBITDA__c=nextEbitaValue;
       update accountToUpdate;
       if(accShippingState!='-None-')
       {
        accountToUpdate.ShippingStateCode = accShippingState;
       }
       else
       {
       accountToUpdate.ShippingStateCode = null;
       }
      
      update accountToUpdate;
       Opportunity newOpp = new Opportunity();
       newOpp.Name = oppName;
       newOpp.Total_Funding__c = totalFundingRaisedValue;
       newOpp.Short_description__c = oppDescription;
       newOpp.Next_Action__c = nextAction;
       newOpp.Next_Action_Date__c = (nextActionDate!=null?Date.valueOf(nextActionDate):null);
       newOpp.Deal_Type__c = dealTypeValue;
       newOpp.Company_Quality__c = companyQualityValue;
       newOpp.Deal_Size__c = dealSizeValue;
       newOpp.Deal_Imminence__c = dealImminenceValue;
       newOpp.Capital_History__c = capitalHistoryValue;
       newOpp.StageName = stageValue;

       newOpp.PriorPriorRevenue__c = objFinancialData.priorPriorRevenueValue;
       newOpp.PriorPriorEBITDA__c =objFinancialData.priorPriorEbitaValue;
       newOpp.Prior_Year_Revenue__c = objFinancialData.priorRevenueValue;
       newOpp.Prior_Year_EBITDA__c = objFinancialData.priorEbitaValue;
       newOpp.Current_Revenue__c = objFinancialData.currentRevenueValue;
       newOpp.Current_EBITDA__c = objFinancialData.currentEbitaValue;
       newOpp.Next_Year_Revenue__c = objFinancialData.nextRevenueValue;
       newOpp.Next_Year_EBITDA__c = objFinancialData.nextEbitaValue;
       
       newOpp.PriorPriorNet_Retention__c = objFinancialData.priorPriorNetValue;
       newOpp.PriorPriorGross_Retention__c =objFinancialData.priorPriorGrossValue;
       newOpp.Prior_Year_Net_Retention__c = objFinancialData.priorNetValue;
       newOpp.Prior_Year_Gross_Retention__c = objFinancialData.priorGrossValue;
       newOpp.Current_Net_Retention__c = objFinancialData.currentNetValue;
       newOpp.Current_Gross_Retention__c = objFinancialData.currentGrossValue;
       newOpp.Next_Year_Net_Retention__c = objFinancialData.nextNetValue;
       newOpp.Next_Year_Gross_Retention__c = objFinancialData.nextGrossValue;
       
       newOpp.If_Inbound_name_of_bank__c=inboundNameOfBank;
       newOpp.Interest__c = interest;
       newOpp.Employees__c =numberOfEmployees;
       newOpp.Other_KPI__c = otherKPI;
       newOpp.Pipe_Status__c = pipeStatus;
       newOpp.Investors__c = investors;
       system.debug('investors: ' + investors );
       //need inbound field
       if(partnerObject!=null&&partnerObject!='')
       {
       	 newOpp.Partner__c =  partnerObject;
       }
       
       newOpp.StageName = stageValue;
       newOpp.RecordTypeId = oppRecId.Id;
       Date closedDate = Date.today();
       newOpp.CloseDate = closedDate.addMonths(6);
       newOpp.AccountId = recordId;
       newOpp.Revenue_Type__c = strRevenueType;
       System.debug('description before insert=='+newOpp.Short_description__c);
       insert newOpp;
       System.debug('description after insert=='+newOpp.Short_description__c);
       Opportunity o = [Select Id,Short_description__c from Opportunity where Id =:newOpp.Id limit 1];
       System.debug('description after query insert=='+o.Short_description__c);
      if(opportunityTeamMembers!=null)
       {
       List<LookupResult> oppTeamMemberResults = (List<LookupResult>)JSON.deserialize(opportunityTeamMembers, List<LookupResult>.class);
       List<OpportunityTeamMember> oppTeamMembers = new List<OpportunityTeamMember>();
        for(LookupResult oppTeam:oppTeamMemberResults)
        {
            oppTeamMembers.add(new OpportunityTeamMember(UserId=oppTeam.rId,OpportunityId =newOpp.Id));
        } 
       insert oppTeamMembers;
     }

    }
    
    public static List<String> fetchCountries()
    {
      Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      List<String> countries = new List<String>();
      countries.add('');
      System.debug('Picklist::'+ple);
      for( Schema.PicklistEntry f : ple){
        countries.add(f.getLabel());
      }
      return countries;
    }
    @AuraEnabled
    public static List<String> fetchStates(String country)
    {
      Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      List<String> countries = new List<String>();
      countries.add('');
      System.debug('Picklist::'+ple);
      for( Schema.PicklistEntry f : ple){
        countries.add(f.getLabel());
      }
      return countries;
    }
    @AuraEnabled
    public static TypeAheadRes[] typeAheadFuncLtng(String rName, String sObjName,String filter, String recordData)
    {
        return TypeaheadFunction.srch(rName,sObjName,filter,'', recordData);
    }
    public class LookupResult
    {
    	public String rId;
    	public String rName;
    }
    public class SendToPipeInitData
	{
		public Account accountData;
		public List<String> dealTypeOptions;
		public List<String> companyQualityOptions;
		public List<String> dealSizeOptions;
		public List<String> dealImminenceOptions;
		public List<String> capitalHistoryOptions;
		public List<String> stageOptions;
    public List<String> pipeStatusOptions;
        public List<String> countryOptions;
        public List<String> lstRevenueTypeOptions;
		public User currentUser;
		public FinancialData objFinancialData;
        public Map<String, List<String>> countryToStates;
        public Boolean isHavingOpenOpp;
	}
	
	public class FinancialData 
	{
	    public Decimal priorPriorRevenueValue;
        public Decimal priorRevenueValue;
		public Decimal currentRevenueValue;
		public Decimal nextRevenueValue;
		public Decimal priorPriorEbitaValue;
        public Decimal priorEbitaValue;
		public Decimal currentEbitaValue;
		public Decimal nextEbitaValue;
		public Decimal priorPriorNetValue;
    	public Decimal priorNetValue;
		public Decimal currentNetValue;
		public Decimal nextNetValue;
		public Decimal priorPriorGrossValue;
    	public Decimal priorGrossValue;
		public Decimal currentGrossValue;
		public Decimal nextGrossValue;
		public String revenueType;
	}
}