global class IntelligenceROIBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
         System.debug('4>>>>>>>>'+[SELECT app_name__c, Company__c, company_id__c, company_url__c, event_type__c, org_id__c, Trigger__c, User__c, request_data__c, response_status__c, app_data__c FROM Intelligence_ROI__c WHERE response_status__c ='Queued']);
        String query = 'SELECT app_name__c, Company__c, company_id__c, company_url__c, event_type__c, org_id__c, Trigger__c, User__c, request_data__c, response_status__c, app_data__c FROM Intelligence_ROI__c WHERE response_status__c =\'Queued\'';
        System.debug('query'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Intelligence_ROI__c> lstIntelligenceROI) {
        System.debug('lstIntelligenceROI'+lstIntelligenceROI);
        IntelligenceROIHelper.SFApplicationAPI(lstIntelligenceROI);
    }
    
    public void finish(Database.BatchableContext BC) {

        Datetime dtTimeBefore12Months = System.now().addMonths(-12);
        Datetime dtTimeBefore6Months = System.now().addMonths(-6);
        List<Intelligence_ROI__c> lstIntelligenceROIToBeDeleted = new List<Intelligence_ROI__c>([SELECT Id FROM Intelligence_ROI__c 
                                                                                                           WHERE ((response_status__c = 'Failed'
                                                                                                                  AND createdDate <=: dtTimeBefore12Months)
                                                                                                                  OR (response_status__c = 'Success'
                                                                                                                  AND createdDate <=: dtTimeBefore6Months))]);   
        if(!lstIntelligenceROIToBeDeleted.isEmpty())
            delete lstIntelligenceROIToBeDeleted;
    }
}