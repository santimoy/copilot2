@isTest
private class ActivityTriggerTestClass {
   @testSetup 
    static void ActivityTriggerTest() {

        Account accTest=new Account(Name='testAcc');
        insert  accTest;
        Task newtask=new Task(); 
        newtask.Subject='Call';
        newtask.Status='Not Started';
        newtask.Priority='Normal';
        newtask.ActivityDate=date.today().addDays(3);
        newtask.WhatId=accTest.Id;
        newtask.OwnerId='0056C000001ba9SQAQ';
        newtask.Description='demo values2';

        insert newtask; 
        List<task> taskList=new List<task>{
        new Task(Subject='Call',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(-1),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='demovalues'),
        new Task(Subject='Email',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(5),WhatId=accTest.Id,OwnerId='0056C000001ba9SQAQ',Description='testvalues')
        };
        insert taskList;

    }
        @isTest
        static void testTalentTaskInsert(){

        Account accTestId=[SELECT Id,Name FROM Account WHERE Name='testAcc'];
        List<task> taskList=new List<task>{
        new Task(Subject='Call',Status='Not Started',Priority='Normal',ActivityDate=date.today().addDays(1),WhatId=accTestId.Id,OwnerId='0056C000001ba9SQAQ',Description='demo values'),
            new Task(Subject='Email',Status='Not Started',Priority='Normal',ActivityDate=date.today(),WhatId=accTestId.Id,OwnerId='0056C000001ba9SQAQ',Description='test values')
        };
        insert taskList;
            //   new Task(Subject='Call',Status='Not Started',Priority='High',ActivityDate=null,WhatId=null,OwnerId=null,Description='')                
            
                
                Account accRecords=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTestId.Id];
                system.assertEquals(date.today().addDays(-1),accRecords.Next_Action_Date_Talent__c);
                system.assertEquals('Call',accRecords.Next_Action_Talent__c);
                system.assertEquals('0056C000001ba9SQAQ',accRecords.Next_Action_Owner_Talent__c);
            
    }
        @isTest
        static void testTalentTaskUpdate(){
            Account accTestId=[SELECT Id,Name FROM Account WHERE Name='testAcc'];
        List<task> listOfTasks=[Select Id,ActivityDate,Subject,OwnerId FROM Task Where WhatId=:accTestId.Id];
        for(Integer i=0;i<listOfTasks.size();i++){
                listOfTasks[0].ActivityDate=date.today().addDays(2);
                break;
        }
        
        update listOfTasks;

        Account accTestRecords=[SELECT Id,Next_Action_Talent__c,Next_Action_Date_Talent__c,Next_Action_Owner_Talent__c FROM Account WHERE Id =:accTestId.Id];
        system.assertEquals('call',accTestRecords.Next_Action_Talent__c);
        system.assertEquals(date.today(),accTestRecords.Next_Action_Date_Talent__c);
        system.assertEquals('0056C000001ba9SQAQ',accTestRecords.Next_Action_Owner_Talent__c);
    } 
    
}