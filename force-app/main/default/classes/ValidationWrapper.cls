/***********************************************************************************
 * @author 10k-Expert
 * @name ValidationWrapper
 * @date 2018-10-30
 * @description data class that is using for ownership validation and data
 ***********************************************************************************/
public class ValidationWrapper{
    
    @AuraEnabled public String errorMsg;
    @AuraEnabled public String successMsg;
    @AuraEnabled public Integer untouchableCount;
    @AuraEnabled public Integer watchlistCount;
    @AuraEnabled public Integer requestBalance;
    @AuraEnabled public String companyOwnerWithCurrentStatus;
    @AuraEnabled public Boolean isCompanyOwnerAndUserSame;
    @AuraEnabled public Boolean hasOpenRequest;
    @AuraEnabled public List<Company_Request__c> requests;
    @AuraEnabled public Boolean currentUserIsRequester;
    @AuraEnabled public String requestBalanceUpdateDate;
    @AuraEnabled public Integer ownershipRequestBalanceDays;
    @AuraEnabled public Integer ownershipRequestBalance;
    
    public ValidationWrapper(){
        errorMsg = '';
        successMsg = '';
        untouchableCount = 0;
        watchlistCount = 0;
        requestBalance = 0;
        requests = new List<Company_Request__c>();
        companyOwnerWithCurrentStatus = '';
        isCompanyOwnerAndUserSame = false;
        hasOpenRequest = false;
        currentUserIsRequester = false;
        requestBalanceUpdateDate = NULL;
        ownershipRequestBalance = 0;
        ownershipRequestBalanceDays = 0;
    }
    public ValidationWrapper(Boolean prepareVdalue){
        if(prepareVdalue){
            errorMsg = '';
            successMsg = '';
            requests = new List<Company_Request__c>();
            hasOpenRequest = false;
            isCompanyOwnerAndUserSame = false;
            currentUserIsRequester = false;
            requestBalanceUpdateDate = NULL;
            companyOwnerWithCurrentStatus = NULL;
            ownershipRequestBalance = IVPOwnershipService.ownershipRequestBalance;
            ownershipRequestBalanceDays = IVPOwnershipService.ownershipRequestBalanceDays;
            requestBalance = ownershipRequestBalance;
            untouchableCount = IVPOwnershipService.priorityBalance;
            watchlistCount = IVPOwnershipService.totalCompanyBalance - untouchableCount;
        }else{
            this();
        }
    }
}