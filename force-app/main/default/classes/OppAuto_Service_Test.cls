/**
*@name      	: OppAuto_Service_Test
*@developer 	: 10K developer
*@date      	: 2018-10-22
*@description   : The class used as test class of OppAuto_Service
**/
@isTest
public class OppAuto_Service_Test {
    @testSetup static void testSetup() {
        IVP_General_Config__c gcMember = new IVP_General_Config__c(Name='Campaign Member Status',Value__c='Follow up 1,Follow up 2');
        insert gcMember;

        List<Campaign> campLst = new List<Campaign>();
        campLst.add(new Campaign(Name = 'Test Campaign', IsActive = TRUE, Type='Portfolio Company Demand Gen', StartDate=System.today()));
        campLst.add(new Campaign(Name = 'Test Campaign1', IsActive = TRUE, Type='Dinner / Roundtable', StartDate=System.today()));
        insert campLst;

        Account acc = new Account (Name = 'Test Account');
        insert acc;
        Account acc1 = new Account (Name = 'Test Account 123');
        insert acc1;

        List<Contact> conLst = new List<Contact>();
        for(Integer i = 0; i < 10; i++){
            conLst.add(new Contact(FirstName = 'Hello'+i,LastName = 'Test'+i,AccountId = acc.Id));
        }
        insert conLst;

        List<Opportunity> oppList = new List<Opportunity>();
        for(Integer i = 0; i < 10; i++){
            oppList.add(new Opportunity(Name = 'Hello Opp'+i,Description='Testing ' + i + ' Opportunity',StageName = 'Lead',CloseDate = Date.valueOf('2019-01-01'),AccountId = acc.Id));
        }
        insert oppList;

        CampaignMemberStatus cms = new CampaignMemberStatus(CampaignId=campLst[1].Id, HasResponded=true, Label='Attended');
        insert cms;

        List<CampaignMember> campMemLst = new List<CampaignMember>();
        for(Integer i = 0; i < 10; i++){
            if(i < 5){
                campMemLst.add(new CampaignMember(Company_Name__c = (i < 2 ? acc.Id : acc1.Id), Status = 'Follow up 1', CampaignId = campLst[0].Id, ContactId = conLst[i].Id, Attendee_Type__c = 'Executive Guest'));
            }else{
                campMemLst.add(new CampaignMember(Company_Name__c = acc1.Id, Status = 'Attended', CampaignId = campLst[1].Id, ContactId = conLst[i].Id, Attendee_Type__c = i < 7 ? 'Executive Guest' : 'Portfolio Executive'));
            }
        }
        insert campMemLst;
    }
    @isTest public static void testPrepareOpportunityNamesSet(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember];
        Set<String> opportunityNames = new Set<String>();

        Test.startTest();
        OppAuto_Service.prepareOpportunityNamesSet(campMembers, campMembers[0], opportunityNames);
        Test.stopTest();

        System.assertEquals(1,opportunityNames.size());
    }
    @isTest public static void testCreateOpportunity(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,Company_Name__c,Status,Meeting_Date__c,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember WHERE Campaign.Type = 'Portfolio Company Demand Gen'];
        Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
        Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
        Set<String> updatedOppNames = new Set<String>();
        for(Opportunity oppObj : [SELECT Id,Name,Description,First_Meeting_Date__c,OppAuto_Opportunity_New_Name__c FROM Opportunity])
        {
            oppNameWithOpportunityMap.put(oppObj.Name, oppObj);
            oppNameWithOppObjForContactRole.put(oppObj.Name, oppObj.Id);
            updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
        }
        User user = [SELECT Id, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
        Set<String> processedOppNames = new Set<String>();
        Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();

        Test.startTest();
        OppAuto_Service.createOrUpdateOpportunity(campMembers, campMembers[0], oppNameWithOpportunityMap, opportunitiesToProcessMap, user.id, processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, false);
        Test.stopTest();

        System.assertEquals(1,processedOppNames.size());
        System.assertEquals(1,opportunitiesToProcessMap.values().size());
        //System.assertEquals(1, oppNameWithMemberList.size());
    }
    @isTest public static void testUpdateOpportunity(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,Company_Name__c,Status,Meeting_Date__c,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember WHERE Campaign.Type = 'Portfolio Company Demand Gen'];
        Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
        Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
        Set<String> updatedOppNames = new Set<String>();
        for(Opportunity oppObj : [SELECT Id,Name,Description,First_Meeting_Date__c,OppAuto_Opportunity_New_Name__c FROM Opportunity])
        {
            oppNameWithOpportunityMap.put(oppObj.Name, oppObj);
            oppNameWithOppObjForContactRole.put(oppObj.Name, oppObj.Id);
            updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
        }
        User user = [SELECT Id, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
        Set<String> processedOppNames = new Set<String>();
        Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();

        Test.startTest();
        OppAuto_Service.createOrUpdateOpportunity(campMembers, campMembers[0], oppNameWithOpportunityMap, opportunitiesToProcessMap, user.id, processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, false);
        Test.stopTest();

        System.assertEquals(1,processedOppNames.size());
        System.assertEquals(1,opportunitiesToProcessMap.values().size());
    }
    @isTest public static void testCreateContactRoleWithDinnerAndRoundtable(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,Company_Name__c,Status,Meeting_Date__c,Campaign.Name,ContactId,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember WHERE Campaign.Type = 'Dinner / Roundtable'];
        Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
        Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
        Set<String> updatedOppNames = new Set<String>();
        for(Opportunity oppObj : [SELECT Id,Name,Description,First_Meeting_Date__c,OppAuto_Opportunity_New_Name__c FROM Opportunity])
        {
            oppNameWithOpportunityMap.put(oppObj.Name, oppObj);
            oppNameWithOppObjForContactRole.put(oppObj.Name, oppObj.Id);
            updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
        }
        User user = [SELECT Id, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
        Set<String> processedOppNames = new Set<String>();
        Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();

        Test.startTest();
        OppAuto_Service.createOrUpdateOpportunity(campMembers, campMembers[0], oppNameWithOpportunityMap, opportunitiesToProcessMap, user.id, processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, false);
        List<Opportunity> oppList = new List<Opportunity>();
        for(OppAuto_Service.OpportuntiyWithMemberWrapper oppWithMemberObj : opportunitiesToProcessMap.values()){
                oppList.add(oppWithMemberObj.oppObj);
        }
        insert oppList;
        OppAuto_Service.createContactRole(opportunitiesToProcessMap, oppNameWithOpportunityMap, oppNameWithMemberList, oppNameWithOppObjForContactRole);
        Test.stopTest();

        System.assertEquals(1,[SELECT Id FROM OpportunityContactRole LIMIT 1].size());
    }
    @isTest public static void testCreateContactRoleWithPortfoilioCompany(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,Company_Name__c,Status,Meeting_Date__c,Campaign.Name,ContactId,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember WHERE Campaign.Type = 'Portfolio Company Demand Gen'];
        Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
        Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
        Set<String> updatedOppNames = new Set<String>();
        for(Opportunity oppObj : [SELECT Id,Name,Description,First_Meeting_Date__c,OppAuto_Opportunity_New_Name__c FROM Opportunity])
        {
            oppNameWithOpportunityMap.put(oppObj.Name, oppObj);
            oppNameWithOppObjForContactRole.put(oppObj.Name, oppObj.Id);
            updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
        }
        User user = [SELECT Id, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
        Set<String> processedOppNames = new Set<String>();
        Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();

        Test.startTest();
        OppAuto_Service.createOrUpdateOpportunity(campMembers, campMembers[0], oppNameWithOpportunityMap, opportunitiesToProcessMap, user.id, processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, false);
        List<Opportunity> oppList = new List<Opportunity>();
        for(OppAuto_Service.OpportuntiyWithMemberWrapper oppWithMemberObj : opportunitiesToProcessMap.values()){
            	oppWithMemberObj.oppObj.StageName = 'Lead';
                oppList.add(oppWithMemberObj.oppObj);
        }
        insert oppList;
        OppAuto_Service.createContactRole(opportunitiesToProcessMap, oppNameWithOpportunityMap, oppNameWithMemberList, oppNameWithOppObjForContactRole);
        Test.stopTest();

        System.assertEquals(1,[SELECT Id FROM OpportunityContactRole LIMIT 1].size());
    }
    @isTest public static void testCreateCompanyInfluence(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,Company_Name__c,Status,Meeting_Date__c,Campaign.Name,ContactId,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember WHERE Campaign.Type = 'Dinner / Roundtable'];
        Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
        Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
        Set<String> updatedOppNames = new Set<String>();
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity oppObj : [SELECT Id,Name,Description,First_Meeting_Date__c,OppAuto_Opportunity_New_Name__c FROM Opportunity])
        {
            oppIds.add(oppObj.Id);
            oppNameWithOpportunityMap.put(oppObj.Name, oppObj);
            oppNameWithOppObjForContactRole.put(oppObj.Name, oppObj.Id);
            updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
        }
        User user = [SELECT Id, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
        Set<String> processedOppNames = new Set<String>();
        Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();

        Test.startTest();
        OppAuto_Service.createOrUpdateOpportunity(campMembers, campMembers[0], oppNameWithOpportunityMap, opportunitiesToProcessMap, user.id, processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, false);
        List<Opportunity> oppList = new List<Opportunity>();
        for(OppAuto_Service.OpportuntiyWithMemberWrapper oppWithMemberObj : opportunitiesToProcessMap.values()){
                oppList.add(oppWithMemberObj.oppObj);
        }
        insert oppList;
        OppAuto_Service.createCampaignInfluence(opportunitiesToProcessMap, oppIds, oppNameWithMemberList ,oppNameWithOppObjForContactRole);
        Test.stopTest();

        System.assertEquals(1,[SELECT Id FROM CampaignInfluence LIMIT 1].size());
    }
    @isTest public static void testCreateCampaignCompany(){
        List<CampaignMember> campMembers = [SELECT Id,Attendee_Type__c,Company_Name__r.Name,Company_Name__c,Status,Meeting_Date__c,Campaign.Name,ContactId,
                                            Campaign.StartDate,Campaign.Type,Campaign.Portfolio_Company__r.Name FROM CampaignMember WHERE Campaign.Type = 'Dinner / Roundtable'];
        Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
        Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
        Set<String> updatedOppNames = new Set<String>();
        for(Opportunity oppObj : [SELECT Id,Name,Description,First_Meeting_Date__c,OppAuto_Opportunity_New_Name__c FROM Opportunity])
        {
            oppNameWithOpportunityMap.put(oppObj.Name, oppObj);
            oppNameWithOppObjForContactRole.put(oppObj.Name, oppObj.Id);
            updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
        }
        User user = [SELECT Id, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' LIMIT 1];
        Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
        Set<String> processedOppNames = new Set<String>();
        Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign camp : [SELECT Id FROM Campaign]){
            campaignIds.add(camp.Id);
        }

        Test.startTest();
        OppAuto_Service.createOrUpdateOpportunity(campMembers, campMembers[0], oppNameWithOpportunityMap, opportunitiesToProcessMap, user.id, processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, false);
        //OppAuto_Service.createCampaignCompany(opportunitiesToProcessMap, campaignIds);
        Test.stopTest();

        //System.assertEquals(1,[SELECT Id FROM Campaign_Company__C LIMIT 1].size());
    }
    @isTest public static void testUpdateCampaignProcessingStatus(){
        Campaign camp = [SELECT Id FROM Campaign LIMIT 1];
        String processingStatus = 'Completed';

        Test.startTest();
        OppAuto_Service.updateCampaignProcessingStatus(camp.Id, processingStatus);
        Test.stopTest();

        System.assertEquals('Completed',[SELECT Processing_Status__c FROM Campaign LIMIT 1].Processing_Status__c);
    }
    @isTest public static void testOpportuntiyWithMemberWrapper(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        CampaignMember campMem1 = [SELECT Id FROM CampaignMember WHERE Campaign.Type = 'Dinner / Roundtable' LIMIT 1];
        CampaignMember campMem2 = [SELECT Id FROM CampaignMember WHERE Campaign.Type = 'Portfolio Company Demand Gen' LIMIT 1];

        Test.startTest();
        OppAuto_Service.OpportuntiyWithMemberWrapper opportuntiyWithMemberWrapper= new OppAuto_Service.OpportuntiyWithMemberWrapper(opp,campMem1,campMem2,false);
        Test.stopTest();

        System.assertEquals(false,opportuntiyWithMemberWrapper.isCreate);
    }
}