//
// 3-30-2018 TO BE DEPRECATED
//
public abstract class Crunchbase_Data {
	public static final string DATE_SEPARATOR = '-';
	public static final string CB_BASEURL_COMPANY = 'http://www.crunchbase.com/company/';
	public static final string CB_BASEURL_FINORG = 'http://www.crunchbase.com/financial-organization/';
	public static final string CB_BASEURL_PERSON = 'http://www.crunchbase.com/person/';
	
	public static final string CB_INVESTORLINK_COMPANY = '<a href="{0}{1}">{2}</a><br/>\n';
	public static final string CB_INVESTORLINK_FINORG = '<a href="{0}{1}">{2}</a><br/>\n';
	public static final string CB_INVESTORLINK_PERSON = '<a href="{0}{1}">{2} {3}</a><br/>\n';

	public static string FUNDING_ALT_UNIQUEID = '{0}|{1}|{2}|{3}|{4}'; // company-id, round code, year, month, day
	
	
	// ----------------------------------------------------------------------------------------------------------------------------------
	// utility methods
	public static string capitalizeFirst(string inString){
		if (inString == null || inString =='') return inString;
		if (inString.length()==1) return inString.toUpperCase();
			
		return inString.subString(0,1).toUpperCase()+inString.subString(1);
	}

	
	// ----------------------------------------------------------------------------------------------------------------------------------
	// inner classes
	public class cbCompany{
		public string name {get;set;}
		public string permalink {get;set;}
		public string crunchbase_url {get;set;}
		
		// funding data
		public string total_money_raised {get;set;}
		public list<cbFundingRound> funding_rounds {get;set;}
		
		public cdAcquisition acquisition {get;set;}
	}

	public class cbPerson{
		public string first_name {get;set;}
		public string last_name {get;set;}
		public string permalink {get;set;}
	}

	public class cbFundingRound{
		public string round_code {get;set;}
		public double raised_amount {get;set;}
		public string raised_currency_code {get;set;}
		public integer funded_year {get;set;}
		public integer funded_month {get;set;}
		public integer funded_day {get;set;}
		public string source_url {get;set;}
		
		public list<cbFundingRound_Investor> investments {get;set;}
		
		public Crunchbase_Funding_Data__c toSfFundingData(Account currAcct){
			Crunchbase_Funding_Data__c fundingData = new Crunchbase_Funding_Data__c(Account__c=currAcct.Id, 
				Batch_UniqueId__c=getUniqueId(currAcct),
				Batch_Round__c=capitalizeFirst(round_code),
				Batch_Date__c=buildDate(),
				Batch_Amount__c=raised_amount,
				Batch_Amount_Currency_Code__c=raised_currency_code,
				Batch_Investors__c=buildInvestors()
			);
			return fundingData;
		}
		
		//..............................................................................................................
		// utility methods for cbFundingRound
		private string getUniqueId(Account currAcct){
			return string.format(FUNDING_ALT_UNIQUEID, new string[]{
				currAcct.Id,
				(round_code==null) ? '' : round_code,
				(funded_year==null) ? '' : string.valueOf(funded_year),
				(funded_month==null) ? '' : string.valueOf(funded_month),
				(funded_day==null) ? '' : string.valueOf(funded_day)
			});
		}
		private string buildDate(){
			string outString = '';
			
			if (funded_year == null || funded_year == 0) return outString;
			outString += String.valueOf(funded_year);
			
			if (funded_month == null || funded_month == 0) return outString;
			outString += DATE_SEPARATOR + ((funded_month < 10)?'0':'') + String.valueOf(funded_month);
	
			if (funded_day == null || funded_day == 0) return outString;
			outString += DATE_SEPARATOR + ((funded_day < 10)?'0':'') + String.valueOf(funded_day);
			
			return outString;
		}
		private string buildInvestors(){
			string outString = '';
			
			if (investments==null || investments.size()==0) return outString;
			for(Crunchbase_Data.cbFundingRound_Investor currInvestor : investments){
				if (currInvestor.company != null){
					outString += string.format(CB_INVESTORLINK_COMPANY, new string[]{
						CB_BASEURL_COMPANY,
						currInvestor.company.permalink,
						currInvestor.company.name
					});
				}
				else if (currInvestor.person != null){
					outString += string.format(CB_INVESTORLINK_PERSON, new string[]{
						CB_BASEURL_PERSON,
						currInvestor.person.permalink,
						currInvestor.person.first_name,
						currInvestor.person.last_name
					});
				}
				else if (currInvestor.financial_org != null){
					outString += string.format(CB_INVESTORLINK_FINORG, new string[]{
						CB_BASEURL_FINORG,
						currInvestor.financial_org.permalink,
						currInvestor.financial_org.name
					});
				}
			}
			return outString;
		}
	}
	
	public class cbFundingRound_Investor{
		public cbPerson person {get;set;}		
		public cbCompany company {get;set;}		
		public cbCompany financial_org {get;set;}		
	}


	public class cdAcquisition{
		public double price_amount {get;set;}
		public string price_currency_code {get;set;}
		public Integer acquired_year {get;set;}
		public Integer acquired_month {get;set;}
		public Integer acquired_day {get;set;}
		
		public cbCompany acquiring_company {get;set;}

		public string buildDate(){
			string outString = '';
			
			if (acquired_year == null || acquired_year == 0) return outString;
			outString += String.valueOf(acquired_year);
			
			if (acquired_month == null || acquired_month == 0) return outString;
			outString += DATE_SEPARATOR + ((acquired_month < 10)?'0':'') + String.valueOf(acquired_month);
	
			if (acquired_day == null || acquired_day == 0) return outString;
			outString += DATE_SEPARATOR + ((acquired_day < 10)?'0':'') + String.valueOf(acquired_day);
			
			return outString;
		}
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// test methods
	static testMethod void testThisClass(){
		
		// test the static methods
		string testString = Crunchbase_Data.capitalizeFirst('a');
		System.assertEquals('A', testString);
		
		// test those inner classes
		
		Account testAccount = new Account(Name='testAccount');
		insert testAccount;
		
		// create a testFunding
		Crunchbase_Data.cbFundingRound testFunding1 = new Crunchbase_Data.cbFundingRound();
		testFunding1.round_code = 'a';
		testFunding1.raised_amount = 1;
		testFunding1.raised_currency_code = 'USD';
		testFunding1.funded_year = 2010;
		testFunding1.funded_month = 1;
		testFunding1.funded_day = 1;
		testFunding1.source_url = 'http://www.google.com/';
		testFunding1.investments = new list<cbFundingRound_Investor>();
		
		cbFundingRound_Investor testFundingInvestor1 = new cbFundingRound_Investor();
		 
		Crunchbase_Data.cbPerson testPerson1 = new Crunchbase_Data.cbPerson();
		testPerson1.first_name = 'test';
		testPerson1.last_name = 'person';
		testPerson1.permalink = 'test-person';
		
		testFundingInvestor1.person = testPerson1;
		testFunding1.investments.add(testFundingInvestor1);
		
		Crunchbase_Funding_Data__c test1 = testFunding1.toSfFundingData(testAccount);
		System.assertEquals(testAccount.Id, test1.Account__c);
		System.assertEquals(1, test1.Batch_Amount__c);
		System.assertEquals('2010-01-01', test1.Batch_Date__c);
		System.assertEquals('A', test1.Batch_Round__c);
		System.assertEquals('<a href="http://www.crunchbase.com/person/test-person">test person</a><br/>\n', test1.Batch_Investors__c);
		
		// create another testFunding
		Crunchbase_Data.cbFundingRound testFunding2 = new Crunchbase_Data.cbFundingRound();
		testFunding2.round_code = 'debt_round';
		testFunding2.raised_amount = 2;
		testFunding2.raised_currency_code = 'USD';
		testFunding2.funded_year = 2012;
		testFunding2.funded_month = 2;
		testFunding2.funded_day = 2;
		testFunding2.source_url = '';
		testFunding2.investments = new list<cbFundingRound_Investor>();
		
		cbFundingRound_Investor testFundingInvestor21 = new cbFundingRound_Investor();
		 
		Crunchbase_Data.cbCompany testCompany21 = new Crunchbase_Data.cbCompany();
		testCompany21.name = 'testCompany';
		testCompany21.permalink = 'testCompany';
		
		testFundingInvestor21.company = testCompany21;
		testFunding2.investments.add(testFundingInvestor21);

		cbFundingRound_Investor testFundingInvestor22 = new cbFundingRound_Investor();
		 
		Crunchbase_Data.cbCompany testFinOrg22 = new Crunchbase_Data.cbCompany();
		testFinOrg22.name = 'testFinOrg';
		testFinOrg22.permalink = 'testFinOrg';
		
		testFundingInvestor22.financial_org = testFinOrg22;
		testFunding2.investments.add(testFundingInvestor22);

		Crunchbase_Funding_Data__c test2 = testFunding2.toSfFundingData(testAccount);
		System.assertEquals(testAccount.Id, test2.Account__c);
		System.assertEquals(testAccount.Id+'|debt_round|2012|2|2', test2.Batch_UniqueId__c);
		System.assertEquals(2, test2.Batch_Amount__c);
		System.assertEquals('2012-02-02', test2.Batch_Date__c);
		System.assertEquals('Debt_round', test2.Batch_Round__c);
		System.assertEquals('<a href="http://www.crunchbase.com/company/testCompany">testCompany</a><br/>\n'
			+'<a href="http://www.crunchbase.com/financial-organization/testFinOrg">testFinOrg</a><br/>\n', test2.Batch_Investors__c);
			
		// write some acquisition code
		Crunchbase_Data.cdAcquisition cbAcq = new Crunchbase_Data.cdAcquisition();
		cbAcq.price_amount = 1;
		cbAcq.price_currency_code = 'USD';
		cbAcq.acquired_year = 2012;
		cbAcq.acquired_month = 12;
		cbAcq.acquired_day = 31;
		cbAcq.acquiring_company = testCompany21;
		
		System.assertEquals('2012-12-31', cbAcq.buildDate());
		
	}
}