/**
 * @author  : 10k-Expert
 * @name    : IVPOwnershipInboundEmailService
 * @description : 
 * 
**/
global class IVPOwnershipInboundEmailService implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        // checking extra text in the reply
        Integer firstNextLine = email.plainTextBody.indexOf('\n');
        if(firstNextLine > -1){
            email.plainTextBody = email.plainTextBody.substring(0, firstNextLine);
        }
        String htmlBody = String.isNotEmpty(email.htmlBody) ? email.htmlBody : '';
        
        EmailInputs responseData = new EmailInputs(email.plainTextBody);
        
        String companyRequestKeyPrefix = Company_Request__c.getSobjectType().getDescribe().getKeyPrefix();
        Integer requestIdIndex = htmlBody.indexOf('/'+companyRequestKeyPrefix);
        String errorContent = '';
        if(requestIdIndex > 0 && (responseData.isAccepeted || responseData.isRejected)){
            String requestId = htmlBody.substring(requestIdIndex+1, requestIdIndex+16);
            
            List<Company_Request__c> companyRequests = [SELECT Id, Company__c, Status__c, Request_By__c FROM Company_Request__c WHERE Id = :requestId AND Status__c = 'Pending' LIMIT 1];
            if(!companyRequests.isEmpty()){
                String newStatus = '';
                ValidationWrapper response = IVPOwnershipService.checkUnassignedValidation(new List<Id>{companyRequests[0].Request_By__c}).get(companyRequests[0].Request_By__c);
                if(responseData.reason.length() > 32768){
                    responseData.reason = responseData.reason.substring(0, 32768-4)+'...';
                }
                // getting values from the ownership map
                /************
                * Calling the method getOwnershipAssignmentsMap which returns a Map which contains the users with Ownership_Assignment_Config 
                Custom setting and getting the values from it and applying the same
                ************/ 
                Map<Id, Ownership_Assignment_Config__c> ownershipAssignmentsMap = IVPOwnershipService.getOwnershipAssignmentsMap(new Set<Id>{companyRequests[0].Request_By__c});
                Boolean byPassValidation = ownershipAssignmentsMap.containsKey(companyRequests[0].Request_By__c) 
                    && (ownershipAssignmentsMap.get(companyRequests[0].Request_By__c).Bypass_Ownership_Validations__c!=Null?(ownershipAssignmentsMap.get(companyRequests[0].Request_By__c).Bypass_Ownership_Validations__c.equalsIgnoreCase('Y')?true:false):false);
        System.debug('byPassValidation> '+ byPassValidation); 
                
                
                if(responseData.isAccepeted){
                    Map<Id,List<Opportunity>> accountWithOpenOppMap = TaskCreationController.checkOpenOpportunity(new List<Id>{companyRequests[0].Company__c});
                    
                    response.errorMsg = '';
                    if(!byPassValidation)
                    {
                        if(response.watchlistCount <= 0){
                            response.errorMsg = System.Label.IVP_Ownership_Requester_Total_Balance_Error + '\n';
                        }
                        if(response.untouchableCount <= 0){
                            response.errorMsg += System.Label.IVP_Ownership_Requester_Priority_Balance_Error + '\n';
                        }
                    }
                    
                    if(!accountWithOpenOppMap.isEmpty() && !accountWithOpenOppMap.get(companyRequests[0].Company__c).isEmpty()){
                        Integer openOpportunityLastCheckDays = IVPOwnershipService.openOpportunityLastInterationMonth;
                        String replaceStr = openOpportunityLastCheckDays > 0 ? openOpportunityLastCheckDays + '' : 'n';
                        response.errorMsg += System.Label.IVP_Ownership_Requester_Company_Open_Opportunity_Error.replace('{!n}', replaceStr + '') + '\n';
                    }
                    if(response.errorMsg == ''){
                        // there is no error so accepting requesting
                        newStatus = 'Accepted';
                    }else{
                        String errorMsg = Label.IVP_Company_Request_Email_Failing_Validation_Error;
                        Integer msgInd = errorMsg.indexOf('{!msg}');
                        if(msgInd>-1){
                            errorMsg = errorMsg.replace('{!msg}', response.errorMsg);
                        }else{
                            errorMsg+='\n'+response.errorMsg;
                        }
                        errorContent += errorMsg + '\n';
                        //throw new IvpOwnershipInboundEmailServiceException(errorMsg);
                    }
                }else if(responseData.isRejected){
                    // rejecting requesting as the per the response
                    newStatus = 'Rejected';
                }
                if(newStatus != ''){
                    try{
                        companyRequests[0].Comment_By_Owner__c = responseData.reason;
                        companyRequests[0].Status__c = newStatus;
                        update companyRequests;
                    }catch(Exception ex){
                        String errorMsg = Label.IVP_Company_Request_Email_Failing_Validation_Error;
                        Integer msgInd = errorMsg.indexOf('{!msg}');
                        if(msgInd>-1){
                            errorMsg = errorMsg.replace('{!msg}', ex.getMessage() + '\n Here are the following details:\n'+ ex.getStackTraceString() +'\n');
                        }else{
                            errorMsg+='\n'+response.errorMsg;
                        }
                        errorContent += errorMsg + '\n';
                        //throw ex;
                    }
                }
            }else{
                errorContent += Label.IVP_Company_Request_Email_No_Open_request_Error + '\n';
                //throw new IvpOwnershipInboundEmailServiceException(Label.IVP_Company_Request_Email_No_Open_request_Error);
            }
        }else{
            errorContent += Label.IVP_Company_Request_Email_not_start_with_Accept_Reject + '\n';
            //throw new IvpOwnershipInboundEmailServiceException(Label.IVP_Company_Request_Email_not_start_with_Accept_Reject);
        }
        if(errorContent != ''){
            Id orgWideEmailId = IVPOwnEmailNotificationBtch.getSysAdminOrgWideEmailId();
            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage email1 = new Messaging.SingleEmailMessage();
            email1.setSubject(email.subject);
            email1.setPlainTextBody(TaskCreationController.extractErrorMessage(errorContent));
            email1.setToAddresses(new List<String>{email.fromAddress});
            email1.setReferences(email.references[0]);
            if(orgWideEmailId != NULL){
                email1.setOrgWideEmailAddressId(orgWideEmailId);
            }
            emails.add(email1);
            Messaging.sendEmail(emails);
        }
        return result;
    }
    
    public class EmailInputs{
        Boolean isAccepeted;
        Boolean isRejected;
        String reason;
        
        public EmailInputs(String plainTextBody){
            String acceptToRemove = 'accept*[a-z]*';
            String rejectToRemove = 'reject*[a-z]*';
            String removeStr = 'IVPTest->->->->->->-';
            
            isAccepeted = false;
            isRejected = false;
            reason = '';
            
            if(String.isNotEmpty(plainTextBody) ){
                Integer removeStrLen = removeStr.length();
                String bodyText = plainTextBody.toLowerCase();
                Integer bodyLen = bodyText.length();
                reason = bodyText.replaceFirst(acceptToRemove, removeStr);
                if(reason.startsWith(removeStr)){
                    isAccepeted = true;
                    reason = extractReason(reason, plainTextBody, bodyLen, removeStrLen);
                }else{
                    reason = bodyText.replaceFirst(rejectToRemove, removeStr);
                    if(reason.startsWith(removeStr)){
                        isRejected = true;
                        reason = extractReason(reason, plainTextBody, bodyLen, removeStrLen);
                    }
                }
            }
        }
        
        public String extractReason(String text, String content, Integer contentLen, Integer preFixLen){
            Integer textLen = text.length();
            Integer startIndex = contentLen-textLen+preFixLen+1;
            if(startIndex > 0 && startIndex < contentLen){
                return content.substring(startIndex);
            }
            return '';
        }
    }
    public class IvpOwnershipInboundEmailServiceException extends Exception{
        
    }
}