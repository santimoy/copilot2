@istest(SeeAllData = false)
public with sharing class HttpUtilsTest {
    @IsTest
    static void methodName(){
        
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        HivebriteIntegrationSchedulable obj = new HivebriteIntegrationSchedulable('CONTACT_HB_TO_SF');
        obj.execute(null);
        Test.stopTest(); 
    }
    
    @IsTest
    static void testStatusFail() {
        Test.startTest();
        StaticResourceCalloutMock mock1 = new StaticResourceCalloutMock();
        mock1.setStaticResource('HivebriteUserTestClass');
        mock1.setStatusCode(400);	// For Failing the Status Code 
        //   mock1.setHeader('Authorization', 'Bearer ');
        Test.setMock(HttpCalloutMock.class, mock1);
        
        HivebriteIntegrationSchedulable obj = new HivebriteIntegrationSchedulable('CONTACT_HB_TO_SF');
        
        obj.execute(null);
        
        Test.stoptest();
    }
    @IsTest
    static void getAccessTokenTest(){
        
        Test.startTest();
        HttpResponse response = new HttpResponse();        
        response.setBody('{"access_token": "123","token_type": "Bearer", "expires_in": "123", "refresh_token": "1234","scope": "admin","created_at": 1574058511 }');
        HttpUtils.getAccessToken( response);

        Test.stopTest();
        
    }
    @IsTest
    static void createRequest(){
        
        Integration_API__mdt ApiBaseURL = [SELECT ID, Base_URL__c FROM Integration_API__mdt LIMIT 1 ];
        Integration_API_Request__mdt reqData = [SELECT ID, Path__c, Verb__c FROM Integration_API_Request__mdt LIMIT 1];
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);

        HttpRequest req = HttpUtils.getCallOutData(ApiBaseURL, reqData);
        HttpUtils.doCallout(req);

        Test.stopTest();
        
    }
    @isTest
    static void createRequestWithAccess(){
        HttpResponse response = new HttpResponse();        
        response.setBody('{"access_token": "123","token_type": "Bearer", "expires_in": "123", "refresh_token": "1234","scope": "admin","created_at": 1574058511 }');
        HttpUtils.getAccessToken( response);
        Integration_API__mdt ApiBaseURL = [SELECT ID, Base_URL__c FROM Integration_API__mdt LIMIT 1 ];
        Integration_API_Request__mdt reqData = [SELECT ID, Path__c, Verb__c FROM Integration_API_Request__mdt LIMIT 1];
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);

        HttpRequest req = HttpUtils.getCallOutData(ApiBaseURL, reqData);
        HttpUtils.doCallout(req);

        Test.stopTest();
        
    }
    @IsTest
    static void OAuthPartSuccessTest(){
        HttpResponse response = new HttpResponse();        
        response.setBody('{"access_token": "123","token_type": "Bearer", "expires_in": "123", "refresh_token": "1234","scope": "admin","created_at": 1574058511 }');
        HttpUtils.getAccessToken( response);
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteAccesTokenTestData');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        HttpUtils.handleOAuth('Hivebrite API', 'Get OAuth Token', false);

        Test.stopTest();
        
    }
     @IsTest
    static void OAuthPartSuccessWithSettingTest(){
        
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteAccesTokenTestData');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        HttpUtils.handleOAuth('Hivebrite API', 'Get OAuth Token', false);

        Test.stopTest();
        
    }
    @IsTest
    static void OAuthPartFailTest(){
        
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteAccesTokenTestData');
        mock.setStatusCode(400);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        HttpUtils.handleOAuth('Hivebrite API', 'Get OAuth Token', true);
        HttpUtils.handleOAuth('Hivebrite API', 'Get OAuth Token', false);

        Test.stopTest();
        
    }
}