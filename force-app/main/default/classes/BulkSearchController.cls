public class BulkSearchController{


	public BulkSearchController(){
		baseURL=System.URL.getSalesforceBaseURL().toExternalForm();
		foundAccounts= new List<AccountResult>();
		selectedIds= new List<Id>();
		autoOpen='false';
		searchValues='';
		assignErrors=new List<assignError>();
		rawIdList='';

	}


	Public Static String UNASSIGNEDID='005d0000001dj1YAAQ';

	public String searchValues {get;set;}

	public Integer numberOfResults {get;set;}

	public List<List<SObject>> searchResult {get;set;}

	public List<Contact> foundContacts {get;set;}
	public List<AccountResult> foundAccounts {get;set;}



	public List<Id> selectedIds {get;set;}

	public String rawIdList {get;set;}

	public String baseURL {
		get;
		set;
	}

	public String autoOpen {
		get;set;

	}

	public class assignError{

		public Id  recordId {get;set;}
		public String err {get;set;}
		public String accountName {get;set;}
		public String newOwner {get;set;}
		public Boolean isSuccess {get;set;}
	}

	public List<assignError> assignErrors {get;set;}

	public class AccountResult{
		public AccountResult(Account a,String s){
			acc=a;
			searchTerm=s;
		}
		public String searchTerm {get;set;}
		public Account acc {get;set;}
	}
	//public void searchAuto() {
	//	autoOpen='true';
	//	search();
	//}


	//public void searchNonAuto() {
	//	autoOpen='false';
	//	search();
	//}
	 public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.BulkSearchFields.getFields();

    }

    //force refresh for hidden ids
    public PageReference emptyMethod() {

    	system.debug('AAA Set RawId:'+rawIdList);
    	return null;
    }

 public String jsonErrors {  
      get {  
          
           String d= JSON.serializepretty(assignErrors);  
          
           return d;   
      }  
      private set;  
    }  

    public PageReference assignSelected(){
    	Map<Id,Account> accs= new Map<Id,Account> ();
    	system.debug('AAA'+rawIdList);
    	assignErrors=new List<assignError>();
    	if(rawIdList.length()==0) return null;
    	selectedIds=rawIdList.split(';');
    	if(selectedIds!=null) {

    		List<Account> accts=[select Id,OwnerId,Name from Account where id in :selectedIds];

    		List<Account> pending= new List<Account>();
	    	for(Account a:accts){
	    		//a.OwnerId==UNASSIGNEDID  &&
	    		if( true || a.Id!=null) {
	    			a.OwnerId=UserInfo.getUserId();
	    			if(!accs.containsKey(a.Id)) accs.put(a.Id,a);
	    			//pending.add(a);
	    		}
	    	}
	    // Important that retain the original order of keys
	    	for(String s:selectedIds) {
	    		if(accs.containsKey(s)) pending.add(accs.get(s));
	    	}


		    Database.SaveResult[] srList = Database.update(pending, false);
		    Integer cnt=0;
			// Iterate through each returned result
			for (Database.SaveResult sr : srList) {
				assignError  e= new assignError();
				e.recordId=sr.getId();
				e.isSuccess=sr.isSuccess();
			  	e.accountName=pending.get(cnt).Name;
			    if (sr.isSuccess()) {
			    	e.newOwner=UserInfo.getName();
			   		
			    }
			    else {
			             
			        for(Database.Error err : sr.getErrors()) {
			           e.err=err.getStatusCode() + ': ' + err.getMessage();
			           break;
			        }
			    }
				assignErrors.add(e);
				cnt++;
			}
	   	
    		//if(searchValues.length()>0) search();	
    	}
    	return null;
    	
    }


    //public void unassignSelected(){
    //	system.debug('AAA'+rawIdList);
    //	if(rawIdList.length()==0) return;
    //	selectedIds=rawIdList.split(';');
    //	if(selectedIds!=null) {

    //		List<Account> accts=[select Id,OwnerId from Account where id in :selectedIds];

    //		List<Account> pending= new List<Account>();
	   // 	for(Account a:accts){
	   // 		if(a.OwnerId==UserInfo.getUserId()  && a.Id!=null) {
	   // 			a.OwnerId=UNASSIGNEDID;
	   // 			pending.add(a);
	   // 		}
	   // 	}
	   //	 	update pending;
    //		//if(searchValues.length()>0) search();	


    //	}

    	
    //}


    //public void assign(){
    //	if(foundAccounts!=null) {
    //		List<Account> pending= new List<Account>();
	   // 	for(AccountResult w:foundAccounts){
	   // 		Account a=w.acc;
	   // 		if(a.OwnerId==UNASSIGNEDID  && a.Id!=null) {
	   // 			a.OwnerId=UserInfo.getUserId();
	   // 			pending.add(a);
	   // 		}
	   // 	}
	   //	 	update pending;
    //		//if(searchValues.length()>0) search();	
    //	}

    	
    //}


public String escapeChars(String input){
 

 String retVal=input.replace('\\','\\\\');
 retVal=retVal.replace(':','\\:');
 retVal=retVal.replace('?','\\?');
 retVal=retVal.replace('*','\\*');

 retVal=retVal.replace('\'','\\\'');
 retVal=retVal.replace('}','\\}');
 retVal=retVal.replace('{','\\{');
 retVal=retVal.replace('(','\\(');
 retVal=retVal.replace(')','\\)');
 retVal=retVal.replace(']','\\]');
 retVal=retVal.replace('[','\\[');
 retVal=retVal.replace('&','\\&');
 retVal=retVal.replace('"','\\"');
 retVal=retVal.replace('!','\\!');
 //retVal=retVal.replace('*','\\*');
 retVal=retVal.replace('|','\\|');
 retVal=retVal.replace('^','\\^');
 retVal=retVal.replace('-','\\-');
 retVal=retVal.replace('+','\\+');
 return retVal;
}
public String currentSearch{ get;set;}    
public Boolean firstTimeSearch {get;set;}

// Need to use Javascript remoting to get around query limits, the user needs to be able to search on 100 values
public  void newSearch(){
		String[] sList =currentSearch.split('[\n\r]');
		Boolean firstTime=firstTimeSearch;
		//String[] sList =searchVal.split('[\n\r]');
		List<List<SObject>> searchResult;
if(firstTime){
		foundAccounts=new List<AccountResult>();
		foundContacts=new List<Contact>();
}
	//List<List<SObject>>
		Set<String> requiredFields = new Set<String>();
		requiredFields.add('Id');
		requiredFields.add('Name');
		requiredFields.add('Website');
		requiredFields.add('OwnerId');
		List<String> extraFields= new List<String>();
		Map<String,Schema.DisplayType> fieldTypes= new Map<String,Schema.DisplayType>();
		String additionalFields='';
		for(Schema.FieldSetMember f : SObjectType.Account.FieldSets.BulkSearchFields.getFields()) {
			String fieldName=f.getFieldPath();
			Schema.DisplayType t= f.getType();
			

            if(!requiredFields.contains(fieldName)){ 
            	additionalFields += ', '+fieldName;
            	extraFields.add(fieldName);
            	fieldTypes.put(fieldName,t);
        	}
        }
		String searchExpr='';
		for(String s:sList){
			if(s.length()<=1) continue;
			if(searchExpr.length()>0)
			searchExpr+=' OR ';
			//searchExpr+='"'+s+'"';
			searchExpr+=s;
			String searchquery='FIND {'+escapeChars(s)+'} IN ALL FIELDS RETURNING Account (Id, Name,WebSite,OwnerId'+additionalFields+'), Contact (Id,FirstName,LastName,Name,Email)';

			searchResult=search.query(searchquery);

			
			List<Account> acctResult=(List<Account>)searchResult.get(0);

			for(Account a:acctResult){
				sObject sObj=(sObject)a;
				for(String f:extraFields){
					if(fieldTypes.get(f)==Schema.DisplayType.TEXTAREA || fieldTypes.get(f)==Schema.DisplayType.STRING){
						if(f.contains('.')) continue;
						System.debug('AAA:'+f);
						if(sObj.get(f)==null) continue;
						String conv=(String)sObj.get(f);
						if(conv.length()>100)
							sObj.put(f,conv.left(100)+'...');
					} 
				}
			}
			List<Contact> contactResult=(List<Contact>)searchResult.get(1);
			
				
			if(acctResult.size()>0) foundAccounts.add(new AccountResult(acctResult.get(0),s)); else {
					account a = new Account();
					a.Name=s+' (0 matches)';
					foundAccounts.add(new AccountResult(a,s));
			}
			if(contactResult.size()>0) foundContacts.add(contactResult.get(0));
		
		}

	
		
	}


	//public void search(){

	//	String[] sList =searchValues.split('[\n\r]');
	
	////List<List<SObject>>
	//	Set<String> requiredFields = new Set<String>();
	//	requiredFields.add('Id');
	//	requiredFields.add('Name');
	//	requiredFields.add('Website__c');
	//	requiredFields.add('OwnerId');
	//	foundAccounts=new List<Account>();
	//	foundContacts=new List<Contact>();
	//	String additionalFields='';
	//	for(Schema.FieldSetMember f : this.getFields()) {
	//		String fieldName=f.getFieldPath();
 //           if(!requiredFields.contains(fieldName)) additionalFields += ', '+fieldName;
 //       }
	//	String searchExpr='';
	//	for(String s:sList){
	//		if(searchExpr.length()>0)
	//		searchExpr+=' OR ';
	//		searchExpr+='"'+s+'"';

	//		String searchquery='FIND {'+s+'} IN ALL FIELDS RETURNING Account (Id, Name,WebSite__c,OwnerId'+additionalFields+'), Contact (Id,FirstName,LastName,Name,Email)';

	//		searchResult=search.query(searchquery);

	//		List<Account> acctResult=(List<Account>)searchResult.get(0);
	//		List<Contact> contactResult=(List<Contact>)searchResult.get(1);
				
	//		if(acctResult.size()>0) foundAccounts.add(acctResult.get(0)); else {
	//				account a = new Account();
	//				a.Name=s+' (0 matches)';
	//				foundAccounts.add(a);
	//		}
	//		if(contactResult.size()>0) foundContacts.add(contactResult.get(0));
	//	}

		

		
	//}

}