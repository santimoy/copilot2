@isTest
private class TimesheetControllerTest {
	
	@isTest static void TestLoadTimesheets() {


	
		
	
		Account acc= new Account();
		Date currentDate=Date.today();
		currentDate=currentDate.addDays(-30);
		Common_Config__c cconf = new Common_Config__c(Timesheet_Go_Live__c=(currentDate.addDays(-20)));
		insert cconf;


		acc.Name='Test';
		insert acc;
		Project__c project= new Project__c();
		project.Company__c=acc.Id;
		project.Billable__c=true;
		project.Start_Date__c=currentDate;
		
		insert project;
		Working_Group_Member__c wm = new Working_Group_Member__c();
		wm.Project__c=project.id;

		wm.Working_Group_Member__c=UserInfo.getUserId();
		wm.Start_Date__c=currentDate;
		insert wm;
		List<Weekly_Update__c> wu= new List<Weekly_Update__c>();
		ApexPages.StandardSetController sc = new ApexPages.StandardSetController(wu);

		TimesheetController controller = new TimesheetController(sc);
		test.StartTest();

		controller.loadTimeSheets();

		test.StopTest();

		wu=[select id from Weekly_Update__c];
		system.assertNotEquals(0,wu.size());

	}
	
	
}