@isTest
public class SourcingPreferenceArchive_Test {

    @TestSetup
    static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@ivptest.com');
        insert u1;
        
        Intelligence_User_Preferences__c  intUserRec = new Intelligence_User_Preferences__c ();
        intUserRec.CreatedById = u1.Id;
        intUserRec.Display_Columns__c = 'Name (ivp_name);City (ivp_city);Country (ivp_country);Intent 3M Growth % (intent_to_buy_score_3m_growth)';
        intUserRec.Sourcing_Signal_Category_Preferences__c = 'acquisition-acquirer, competitive-challenge, Customer, employee-growth, form-10k';
        intUserRec.My_Dash_Signal_Preferences__c = 'cquisition-acquirer, acquisition-acquiree, award, cb-new-company, ceo-change, competitive-challenge';
        intUserRec.User_Id__c = string.valueOf(u1.id) ;
        insert intUserRec;
    }
    
    public static testmethod void Method1()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
          
            
            Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
            sourcingPreference.Name__c = 'Test';
            sourcingPreference.Status__c = 'Active';
            sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
            insert sourcingPreference;
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            test.startTest();
            Sourcing_Preference__c sourcingPreference1 = [Select Id, Name, OwnerId, External_Id__c, Include_Exclude__c,Advanced_Mode__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, Name__c,
                                                      Owner_Username__c,Recent_Validation_Error__c,  SQL_Query__c, SQL_Where_Clause__c, Serialized__c, Status__c, Validation_Status__c,
                                                      Cloned_From__c, Sent_to_DWH__c From Sourcing_Preference__c LIMIT 1];
            Sourcing_Preference__c SourcPref = SourcingPreferenceArchive.fetchSourcingPreference(sourcingPreference.Id);
            system.assertEquals(sourcingPreference1,SourcPref);
            Boolean status = SourcingPreferenceArchive.saveSourcingPreference(sourcingPreference);
            system.assertEquals(true,status);
            test.stopTest();
        }
    }
    
    public static testmethod void Method2()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            test.startTest();
            Sourcing_Preference__c SourcPref = SourcingPreferenceArchive.fetchSourcingPreference(null);
            system.assertEquals(null,SourcPref);
            test.stopTest();
        }
    }
}