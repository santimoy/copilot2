public class IVPFeelingLuckyWrapper {
    @auraEnabled
    public List<Intelligence_Feedback__c> iflRecords {get; set;}
    @auraEnabled
    public String fieldsAndLabels  {get; set;}
    @auraEnabled
    public Map<String, String> feedbackPicklist {get; set;}
    
}