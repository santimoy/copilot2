@isTest
public with sharing class EscalateButtonControllerTest {
    public EscalateButtonControllerTest() {

    }
    @testSetup static void setup() {
        Account testAccts = new Account(Name = 'TestAcct',OwnerId=UserInfo.getUserId(),Escalated__c=true);
        insert testAccts;  
        Account testAcctsNew = new Account(Name = 'TestAcct New',Escalated__c=false);
        insert testAcctsNew;   
       }
   @isTest
    public static void testfetchInitData(){
        Test.startTest();
        EscalateButtonController.WrapperForEscalate objWrapper = new EscalateButtonController.WrapperForEscalate();
        Account objAccount = [SELECT Id,Escalated__c,OwnerId FROM Account WHERE Name='TestAcct' LIMIT 1];
        objWrapper = EscalateButtonController.fetchInitData(objAccount.Id);
        System.debug('objWrapper'+EscalateButtonController.fetchInitData(objAccount.Id));
        
        Account objAccountNew = [SELECT Id,Escalated__c,OwnerId FROM Account WHERE Name='TestAcct New' LIMIT 1];
        EscalateButtonController.fetchInitData(objAccountNew.Id);
        System.assertNotEquals(objWrapper, EscalateButtonController.fetchInitData(objAccount.Id));
        Test.stopTest();        
    }
    @isTest
    public static void testsaveRecord(){
        Test.startTest();
        Account objAccount = [SELECT Id,Escalated__c, Escalation_Reason__c, Escalation_Notes__c, Escalated_Date__c FROM Account WHERE Name='TestAcct' LIMIT 1];
		
        EscalateButtonController.saveRecord(string.valueOf(objAccount.id),true,'11','Top Opportunity',Date.newInstance(2020, 18, 1),string.valueOf(Date.newInstance(2020, 3, 5)));
        EscalateButtonController.saveRecord(string.valueOf(objAccount.id),false,'11','Top Opportunity',Date.newInstance(2020, 18, 1),string.valueOf(Date.newInstance(2020, 3, 5)));
        EscalateButtonController.saveRecord(string.valueOf(objAccount.id),false,'11','Top Opportunity',Date.newInstance(2020, 18, 1),string.valueOf(Date.newInstance(2020, 6, 5)));
        System.assertEquals(2, [SELECT count() FROM Account]);
        Test.stopTest(); 
    }
     @isTest
    public static void testgetPickListValues(){
         Test.startTest();
         EscalateButtonController.getPickListValues();
        Test.stopTest();
    }
 
}