public with sharing class CompanyBankTriggerHandler {
  /**/
  public void onAfterInsert(list<CompanyBank__c> trigRecsNew){
    
    list<CompanyBank__c> invRecs = new list<CompanyBank__c>();
    for(CompanyBank__c currRec : trigRecsNew){
      if (currRec.InverseCompanyBank__c!=null) continue; 
      
      invRecs.add(new CompanyBank__c(Company__c=currRec.Bank__c,
        Bank__c=currRec.Company__c, InverseCompanyBank__c=currRec.Id));
    }
    if (invRecs.size()>0 ) insert invRecs;
    
    list<CompanyBank__c> updtRecs = new list<CompanyBank__c>();
    for(CompanyBank__c currInvRec : invRecs){
      updtRecs.add(new CompanyBank__c(Id=currInvRec.InverseCompanyBank__c, InverseCompanyBank__c=currInvRec.Id));
    }
    if (updtRecs.size()>0 ) update updtRecs;
  }
  
  public void onAfterDelete(list<CompanyBank__c> trigRecsOld){
    
    list<CompanyBank__c> invRecs = new list<CompanyBank__c>();
    for(CompanyBank__c currRec : trigRecsOld){
      if (currRec.InverseCompanyBank__c==null) continue; 
      
      invRecs.add(new CompanyBank__c(Id=currRec.InverseCompanyBank__c));
    }
    if (invRecs.size()>0 ) delete invRecs;
  }
  
  // Test Methods
  static testMethod void testThisClass(){
    // insert test
    Account testCompany = new Account(Name='testCompany');
    Account testBank = new Account(Name='testBank');
    insert new Account[]{testCompany, testBank};
    
    CompanyBank__c testRec = new CompanyBank__c(Company__c=testCompany.Id, Bank__c=testBank.Id);
    insert testRec;
    testRec = [select id, InverseCompanyBank__c, Bank__c, Company__c from CompanyBank__c where Id = :testRec.Id];
    System.assertNotEquals(null, testRec.InverseCompanyBank__c);
    
    CompanyBank__c testInv = [select id, Company__c, Bank__c, InverseCompanyBank__c
      from CompanyBank__c where id = :testRec.InverseCompanyBank__c];
    System.assertEquals(testRec.Id, testInv.InverseCompanyBank__c);
    System.assertEquals(testRec.Company__c, testInv.Bank__c);
    System.assertEquals(testRec.Bank__c, testInv.Company__c);
    
    
    // delete test
    delete testRec;
    list<CompanyBank__c> testDelList = [select id from CompanyBank__c where id = :testRec.InverseCompanyBank__c];
    System.assertEquals(0, testDelList.size());
  }
  /**/
}