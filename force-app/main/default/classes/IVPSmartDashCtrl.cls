public class IVPSmartDashCtrl {
    
    @AuraEnabled
    public static Boolean getTodayVisitedWatchDog(Boolean isDML){
        Boolean isVisitedToday = false;
        List<Intelligence_User_Preferences__c> ups = [SELECT Id, Last_Visited_WatchDog__c 
                                                    FROM Intelligence_User_Preferences__c
                                                    WHERE User_Id__c = :UserInfo.getUserId()];
        Intelligence_User_Preferences__c up;
        if(!ups.isEmpty()){
            up = ups[0];
        }else{
            up = new Intelligence_User_Preferences__c(User_Id__c = UserInfo.getUserId(), Name='Smart Dash');
        }
        if(up.Last_Visited_WatchDog__c != NULL){
            isVisitedToday = up.Last_Visited_WatchDog__c.format('YYYMMdd') == System.now().format('YYYMMdd');
        }
        up.Last_Visited_WatchDog__c = System.now();
        if(isDML)
            upsert up;
        return isVisitedToday;
    }
    
}