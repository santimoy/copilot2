global class Batch_EmailParser implements Database.Batchable<sObject> {
	private String dateRange;
	private String accountId;
	private String taskOwnerId;
	private String inboundOrOutbound;
    private Boolean runOnAll;

    public Batch_EmailParser(String dateParam,String accountParam,String taskOwnerParam,String inboundOrOutboundParam,Boolean runOnAllParam) {
        dateRange = dateParam+'T00:00:00Z';
        accountId = accountParam;
        taskOwnerId = taskOwnerParam;
        inboundOrOutbound = inboundOrOutboundParam;
        runOnAll = runOnAllParam;
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = '';
		String filterAddition = '';
        if(!runOnAll)
        {
            if(dateRange!=null)
                filterAddition += ' AND CreatedDate >'+dateRange+' ';
            if(accountId!=null)
                filterAddition += ' AND AccountId =\''+accountId+'\' ';
            if(taskOwnerId!=null)
                filterAddition += ' AND OwnerId = \''+taskOwnerId+'\' ';
            if(inboundOrOutbound!=null)
                filterAddition += ' AND Source__c = \''+inboundOrOutbound+'\' ';
        }
        else
        {
             filterAddition += ' AND Email_Parsed__c = false AND (Source__c = \'Inbound\' OR Source__c = \'Outbound\')';
        }
		
        EmailParser_Killswitch__c killswitchCS = EmailParser_Killswitch__c.getOrgDefaults();
        if(killswitchCS.RecordTypeParse__c!=null)
        {
            List<String> recordTypes = killswitchCS.RecordTypeParse__c.split(',');
            if(recordTypes.size()==1)
            {
                for(String s:recordTypes)
                {
                    filterAddition += ' AND RecordType.Name = \''+s+'\'';
                }
            }
            else
            {
                filterAddition += ' AND (RecordType.Name = \''+recordTypes[0]+'\'';
                for(Integer x = 1;x<recordTypes.size();x++)
                {
                    filterAddition += 'OR RecordType.Name = \''+recordTypes[x]+'\'';
                }
                filterAddition += ')';
            }
           
        }
        if(killswitchCS.RecordTypeParse__c!=null)
		  query = 'Select Id,OwnerId,WhoId,WhatId,AccountId,Type__c,Type,Source__c,ActivityDate,CreatedDate, Subject, Description from Task where Type__c = \'Email\''+filterAddition;
        System.debug(query);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Task> lstTask) {
   		 List<Task> outBoundEmails = new List<Task>();
        Set<Id> outBoundEmailContactIds = new Set<Id>();
        Map<Id,Contact> outBoundEmailContacts = new Map<Id,Contact>();
        System.debug(lstTask);
   		for(Task t: lstTask)
   		{
   			if(t.Source__c=='Inbound')
   			{
                String taskSubject = t.Subject;
                Boolean scrapeEmail = false;
                if(taskSubject.length()>=6&&taskSubject.substring(0,6)=='Reply:')
                {
                    scrapeEmail = true;
                }
                else if(taskSubject.length()>=11&&taskSubject.substring(0,11)=='Sent Email:')
                {
                    scrapeEmail = true;
                }
                else if(taskSubject.length()>=15&&taskSubject.substring(0,15)=='Meeting Booked:')
                {
                    scrapeEmail = true;
                }
                if(scrapeEmail)
                {
                    t.Description = EmailParser.getEmailParser(t.Subject,t.Description);
                }
   			}
   			else if (t.Source__c == 'Outbound')
   			{
		       
                Boolean scrapeEmail = false;
                if(t.Type__c=='Email')
                {
                    String taskSubject = t.Subject;
                    if(taskSubject.length()>=6&&taskSubject.substring(0,6)=='Email:')
                    {
                        scrapeEmail = true;
                    }
                    else if(taskSubject.length()>=10&&taskSubject.substring(0,10)=='Follow up:')
                    {
                        scrapeEmail = true;
                    }
                    else if(taskSubject.length()>=13&&taskSubject.substring(0,13)=='Following up:')
                    {
                        scrapeEmail = true;
                    }
                    else if(taskSubject.length()>=9&&taskSubject.substring(0,9)=='Canceled:')
                    {
                        scrapeEmail = true;
                    }
                    
                    if(scrapeEmail)
                    {
                        outBoundEmails.add(t);
                        outBoundEmailContactIds.add(t.WhoId);
                    }
                }
   			}
            if(runOnAll)
            {
                t.Email_Parsed__c = true;
            }
   		}
        if(!outBoundEmailContactIds.isEmpty())
        {
            outBoundEmailContacts = new Map<Id,Contact>([Select Id,Name,Title from Contact where Id in:outBoundEmailContactIds]);
        }
        //System.debug('outBoundEmailContacts=='+outBoundEmailContacts);

        for(Task t:outBoundEmails)
        {
            Contact c = outBoundEmailContacts.get(t.WhoId);
            if(c!=null)
            {
                //t.Subject = 'Outbound Email';
                t.Description = 'Email Outbound to '+c.Name +(c.Title!=null?', '+c.Title:'')+(t.ActivityDate!=null?' on '+t.ActivityDate.month()+'/'+t.ActivityDate.day()+'/'+t.ActivityDate.year():'');
                     //System.debug('tast t=='+t);

            }
            else
            {
                t.Description = 'Outbound Email'+(t.ActivityDate!=null?' on '+t.ActivityDate.month()+'/'+t.ActivityDate.day()+'/'+t.ActivityDate.year():'');

            }
        }
        if(!lstTask.isEmpty())
        {
        	update lstTask;
        }
        if(!outBoundEmails.isEmpty())
        {
        	update outBoundEmails;
        }
	}

	global void finish(Database.BatchableContext BC) {
		
	}
}