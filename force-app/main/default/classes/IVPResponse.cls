public class IVPResponse {
    @auraEnabled
    public String code {get; set;}
    
    @auraEnabled
    public String message {get; set;}
    
    @auraEnabled
    public Object data {get; set;}
}