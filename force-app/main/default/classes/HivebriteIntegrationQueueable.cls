/**
* Using this call will call multiple handle class based on the Jobtype.We Used the Queueable class to avoid the Callou tLimitations.
* Aslo chain the dependent callout.
* 
* @author  Anup Kage 
* @version 1.0
* @since  2019/August/12
*/	

public with sharing class HivebriteIntegrationQueueable implements Queueable, Database.AllowsCallouts {

    private List<Account> acctList;
    private List<Contact> contList;
    private List<SObject> recordList;
    private List<Object> objectList;
    private String LOG_LEVEL = HivebriteIntegrationUtils.lOG_LEVEL; 
    private String jobType;
    private Datetime currentTime;

    public enum JobType {
         PORTFOLIO_BATCH
        ,PORTFOLIO_SF_TO_HB
        ,PORTFOLIO_HB_TO_SF
        ,PARTNER_SF_TO_HB
        ,CONTACT_BATCH
        ,CONTACT_SF_TO_HB
        ,CONTACT_HB_TO_SF
    }

    public HivebriteIntegrationQueueable() {
    }
    /**
   * parmeterized constructor to receive JobType Name 
   * @param jobtype This is to specify which job we want to run.
   */
    public HivebriteIntegrationQueueable(String jobType) {
        this.jobType = jobType;
    }
    /**
   * parmeterized constructor to receive JobType Name and timestamp. dateTime is used to update the custom setting when all callouts are done. 
   * @param jobtype This is to specify which job we want to run.
   */
    public HivebriteIntegrationQueueable(String jobType, Datetime currentTime) {
        this.jobType = jobType;
        this.currentTime = currentTime;
    }
    // public HivebriteIntegrationQueueable(String jobType,List<SObject> recordList, Datetime currentTime) {
    //     this.jobType = jobType;
    //     this.currentTime = currentTime;
    // }

    /**
   * parmeterized constructor to receive JobType Name and List of records
   * Used When we want to call scheduable class from Trigger.
   * @param jobtype This is to specify which job we want to run.
   */
    public HivebriteIntegrationQueueable(String jobType, List<SObject> recordList){
        this.jobType = jobType;
        this.recordList = recordList;
    }
    //TODO : Do we required this constructor
    public HivebriteIntegrationQueueable(String jobType, List<Object> objectList){
        // system.debug('Entering HivebriteIntegrationQueueable.execute(); jobType = ' + jobType + '; contListRequest = ' );
        this.jobType = jobType;
        this.objectList = objectList;
    }
       
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @param context 
    * @return void 
    **/
    public void execute(QueueableContext context) {

        Integration_API_Settings__c userSetting = Integration_API_Settings__c.getOrgDefaults();
        if(!userSetting.Integration_Is_Enabled__c){
            if(userSetting.Event_Log_Level__c	!= null && ( userSetting.Event_Log_Level__c.equalsIgnoreCase('INFO') || userSetting.Event_Log_Level__c.equalsIgnoreCase('debug'))){
                String ErrorMessage =' Queueable '+ jobType +'<BR/>';
                if(recordList != null && !recordList.isEmpty()){
                    Map<Id, SObject> objectById = new Map<Id, SObject>(recordList);
                    ErrorMessage += ' The following Records were not synced to Hivebrite: '+ String.join(new List<Id>(objectById.keySet()), ' ,');
                }
                EventLog.generateLog('HivebriteIntegrationQueueable', jobType , 'INFO', 'Integration is disabled',ErrorMessage);
                EventLog.commitLogs();
            }
            return;
        }
        System.debug(LoggingLevel.DEBUG, 'queueable class--------->'+jobType);
        if(LOG_LEVEL.containsIgnoreCase('Debug')){
            String description = ' Start of HivebriteIntegrationQueueable : '+jobType+' \n Id :' + context.getJobId();
            EventLog.limitLogger('HivebriteIntegrationQueueable', 'execute', description);
        }
        //Call Helper Classes to build HttpRequests
        if(jobType == 'PORTFOLIO_SF_TO_HB'){
            hivebriteCompanyHandler();
        }
        else if(jobType == 'PORTFOLIO_HB_TO_SF'){
           
        } 
        else if (jobType == 'PARTNER_SF_TO_HB'){
            HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', jobtype, recordList);
            objHelper.startCalloutProcess();
        } 
        // fetch User records from Hivebrite and create Contact record in Salesforce.
        else if (jobType == 'CONTACT_HB_TO_SF'){           
            HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', 'Get Contacts');
            objHelper.startCalloutProcess();
        }
         else if (jobType == 'CONTACT_SF_TO_HB'){           
            HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', recordList);
            objHelper.insertContactRecords();
        }
        // fetch Experience records from Hivebrite and update Conatct record in Salesforce.
        else if (jobType == 'EXPERIENCE_HB_TO_SF'){            
            HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Experience', 'Hivebrite API', 'Get User experiences');
            objHelper.currentTime = this.currentTime;
            objHelper.startCalloutProcess();
        }else if (jobType == 'EXPERIENCE_SF_TO_HB'){            
            HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Experience', 'Hivebrite API', recordList);
            objHelper.updateExperenceRecords();
        }else if(jobType == 'DELETECONTACT_SF_TO_HB'){
            System.debug(LoggingLevel.DEBUG, 'queueable class--------->DELETECONTACT_SF_TO_HB');
            HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Delete', 'Hivebrite API', recordList);
            objHelper.deleteRecordsFromHivebrite();
        }
        
        if(LOG_LEVEL.containsIgnoreCase('Debug')){
            String description = ' END of HivebriteIntegrationQueueable : \n Id :' + context.getJobId();
            EventLog.limitLogger('HivebriteIntegrationQueueable', 'execute', description);
        }
        EventLog.commitLogs();
     }
   /**
   * we are chaining queueable job to perform callout which is dependent on current job.
   * @param jobtype This is to specify which job we want to run.
   */
   @testVisible
    private void enqueueNextJob(String jobType){
        HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable(jobType);
        if(!Test.isRunningTest()){
           Id jobId = System.enqueueJob(nextJob);
        }
    }
    /**
   * we are chaining queueable job to perform callout which is dependent on current job.
   * @param jobtype This is to specify which job we want to run.
   * @param recordsList List of records on which we want to perform callouts.
   */
    @testVisible
     private void enqueueNextJob(String jobType, List<SObject> recordsList){
        HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable(jobType, recordsList);
        if(!Test.isRunningTest()){
            Id jobId = System.enqueueJob(nextJob);
        }
    }   
    /**
    * Call company handler to create Company records in hivebrite
    * CallOut if we have records greater then zero.
    */
    private void hivebriteCompanyHandler(){
        if(recordList.size() > 0){
            // do Call out and upsert records in hivebrite
            HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', jobtype, recordList);
            objHelper.startCalloutProcess();
        }
    }
}