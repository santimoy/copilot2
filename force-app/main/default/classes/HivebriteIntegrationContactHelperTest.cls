/**
 * @File Name          : HivebriteIntegrationContactHelperTest.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 6:06:27 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/11/2019   Anup Kage     Initial Version
**/
@isTest(SeeAllData = false)
public with sharing class HivebriteIntegrationContactHelperTest {

    
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
     @IsTest
    static void getHivebriteUserBadRequest(){
        Integration_API_Settings__c accToken = new Integration_API_Settings__c();
        accToken.Last_Run_Timestamp__c = datetime.newInstance(2019, 10, 11, 12, 28, 43);
        insert accToken;
        
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse(); 
        HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User',
                                                                                             'Hivebrite API', 
                                                                                             'Get Contacts'); // helper class will  form request
        contHelper.startCalloutProcess();      
       
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    @IsTest
    static void getHivebriteExperienceIntoSFContacts(){ 
        
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();      
        HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User Experience',
                                                                                             'Hivebrite API', 
                                                                                             'Get User experiences'); // helper class will  form request
        contHelper.startCalloutProcess();
        
        Test.stopTest();
        
    }
    
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    @IsTest
    static void methodFail(){ 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponse401();
        HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User Experience',
                                                                                             'Hivebrite API', 
                                                                                             'Get User experiences');
        contHelper.startCalloutProcess();
        
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    @IsTest
    static void testCoverMethods(){ 
        List<Contact> recordList = new List<Contact>();        
        
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContact(Company.Id);
        recordList.add(hivebriteUser);
        HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User Experience',
                                                                                             'Hivebrite API', 
                                                                                             recordList);
        
        contHelper.startCalloutProcess();        
        Test.stopTest();
    }
    
   
 
    
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    @IsTest
    static void getUsersFromHivebrite(){
        
        Test.startTest();
            HivebriteIntegrationTestFactory.setupMockResponse();
            Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();          
            
            HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User',
                                                                                             'Hivebrite API', 
                                                                                             'Get Contacts'); // helper class will  form request
            contHelper.startCalloutProcess();            
        Test.stopTest();        
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    @IsTest
    static void HivebriteJSONExceptionResponse(){
        Test.startTest();
            HivebriteIntegrationTestFactory.setupMockBadResponseJSONException();
            Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
            Contact hivebriteUser = HivebriteIntegrationTestFactory.createContact(Company.Id);
            Contact objUpdate = new Contact(LastName= 'Test121 001', FirstName='UPDATE 001'+Datetime.now(), id = hivebriteUser.Id);
            update objUpdate;
            
        Test.stopTest();
    }
    @IsTest
    static void insertExperienceRecords(){
        List<Contact> recordList = new List<Contact>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        
        // recordList.add(hivebriteUser);
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Experience', 'Hivebrite API', contactList);
        objHelper.updateExperenceRecords();
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void updateExperienceRecords(){
        //<Contact> recordList = new List<Contact>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        
        // recordList.add(hivebriteUser);
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact';
        List<Contact> contactList = Database.query(query);
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Experience', 'Hivebrite API', contactList);
        objHelper.updateExperenceRecords();
        Test.stopTest();
        
    }
    //=======================================================================================================
    
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertUserRecordsInHivebrite(){   
        List<Account> accountList = new List<Account>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContact(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponseWithDiffUsers();
       
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact';
        List<Contact> contactList = Database.query(query);
                        
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', contactList);
        objHelper.insertContactRecords();

        Test.stopTest();
        
    }
    @IsTest
    static void insertUserRecordsInHivebriteBadRequest(){
        List<Account> accountList = new List<Account>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContact(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponse401();
     
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', contactList);
        objHelper.insertContactRecords();

        Test.stopTest();
        
    }
     @IsTest
    static void updateUserRecordsInHivebrite(){
        List<Account> accountList = new List<Account>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponseWithDiffUsers();
      
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', contactList);
        objHelper.insertContactRecords();
       // System.debug('--------------------->'+[SELECT Hivebrite_Id__c FROM Contact ]);
        objHelper.enqueueNextJob('CONTACT_HB_TO_SF');
        objHelper.enqueueNextJob('CONTACT_HB_TO_SF',System.now());
        objHelper.enqueueNextJob('CONTACT_HB_TO_SF',contactList);
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    @IsTest
    static void getHivebriteUserIntoSFContacts(){
        Integration_API_Settings__c accToken = new Integration_API_Settings__c();
        accToken.Last_Run_Timestamp__c = datetime.newInstance(2019, 10, 11, 12, 28, 43);
        insert accToken;
        
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse(); 
        HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User',
                                                                                             'Hivebrite API', 
                                                                                             'Get Contacts'); // helper class will  form request
        contHelper.startCalloutProcess();      
         

        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 11/12/2019 
    * @return void 
    **/
    @IsTest
    static void deleteRecordsFromHivebriteSuccess(){
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = true;
        accToken.Event_Log_Level__c = 'DEBUG';  
        UPSERT accToken;
        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        hivebriteUser.No_Longer_with_Company__c= true;
        UPDATE hivebriteUser;
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        // 'https://insight.sandbox.us.hivebrite.com/api/admin/v1/users/'
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass'); // we are parsing any response
        mock.setStatusCode(204);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Delete', 'Hivebrite API', contactList);
        objHelper.deleteRecordsFromHivebrite();
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 11/12/2019 
    * @return void 
    **/
     @IsTest
    static void deleteRecordsFromHivebriteNotFound(){
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = true;
        accToken.Event_Log_Level__c = 'DEBUG';  
        UPSERT accToken;

        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        hivebriteUser.No_Longer_with_Company__c= true;
        UPDATE hivebriteUser;
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        // 'https://insight.sandbox.us.hivebrite.com/api/admin/v1/users/'
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass'); // we are parsing any response
        mock.setStatusCode(404);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Delete', 'Hivebrite API', contactList);
        objHelper.deleteRecordsFromHivebrite();
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 11/12/2019 
    * @return void 
    **/
     @IsTest
    static void deleteRecordsFromHivebriteOAuth(){
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = true;
        accToken.Event_Log_Level__c = 'DEBUG';  
        UPSERT accToken;

        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        hivebriteUser.No_Longer_with_Company__c= true;
        UPDATE hivebriteUser;
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        // 'https://insight.sandbox.us.hivebrite.com/api/admin/v1/users/'
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass'); // we are parsing any response
        mock.setStatusCode(401);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Delete', 'Hivebrite API', contactList);
        objHelper.deleteRecordsFromHivebrite();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 11/12/2019 
    * @return void 
    **/
     @IsTest
    static void deleteRecordsFromHivebriteBadRequest(){
        
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = true;
        accToken.Event_Log_Level__c = 'DEBUG';  
        UPSERT accToken;
        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        hivebriteUser.No_Longer_with_Company__c= true;
        UPDATE hivebriteUser;
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        // 'https://insight.sandbox.us.hivebrite.com/api/admin/v1/users/'
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass'); // we are parsing any response
        mock.setStatusCode(500);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest(); 
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User Delete', 'Hivebrite API', contactList);
        objHelper.deleteRecordsFromHivebrite();
        Test.stopTest();
    }
}