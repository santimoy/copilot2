/*

    @description    Test Class for CompanyBank Trigger & Handler
    @author         Mike Gill
    @date           12th July 2016

*/

@isTest
private class CompanyBankTrigger_Test {
    
    private static Account testCompany;
    private static Account testBank;
    
    static {
        
        testCompany = new Account(Name='testCompany');
        insert testCompany;
        testBank = new Account(Name='testBank');
        insert testBank;
        
        
    }
     // Test Methods
    static testMethod void testThisClass(){
    // insert test
    
    CompanyBank__c testRec = new CompanyBank__c(Company__c=testCompany.Id, Bank__c=testBank.Id);
    insert testRec;
    testRec = [select id, InverseCompanyBank__c, Bank__c, Company__c from CompanyBank__c where Id = :testRec.Id];
    System.assertNotEquals(null, testRec.InverseCompanyBank__c);
    
    CompanyBank__c testInv = [select id, Company__c, Bank__c, InverseCompanyBank__c
      from CompanyBank__c where id = :testRec.InverseCompanyBank__c];
    System.assertEquals(testRec.Id, testInv.InverseCompanyBank__c);
    System.assertEquals(testRec.Company__c, testInv.Bank__c);
    System.assertEquals(testRec.Bank__c, testInv.Company__c);
    
    
    // delete test
    delete testRec;
    list<CompanyBank__c> testDelList = [select id from CompanyBank__c where id = :testRec.InverseCompanyBank__c];
    System.assertEquals(0, testDelList.size());
  }

}