/**
* @author Amjad-Concretio
* @date 2018-04-30
* @className IVP_RecentlyUnassigned_Queueable
*
* @description Queueable class to update accounts with recently unassigned owners
*/
public class IVP_RecentlyUnassigned_Queueable implements Queueable {
    Map<String,Set<String>> accountToUsers = new Map<String,Set<String>>();
    
    public IVP_RecentlyUnassigned_Queueable(Map<String,Set<String>> accountToUsers){
        this.accountToUsers = accountToUsers;
        
    }
    public void execute(QueueableContext context) {
        System.debug(Limits.getHeapSize() );
        System.debug(accountToUsers.size() );
        List<Account> acctsToUpdate = new List<Account>();
        List<String> keys = new List<String>();
        //keys.addAll(accountToUsers.keySet());
        Integer k = 0;
        for(String key : accountToUsers.keySet()){
            
            if(k>1000){
                break;
            }
            keys.add(key);
            k++;
            
        }
        System.debug('Before loop accountToUsers.size =>'+accountToUsers.size()+' keys.size'+keys.size()    );
        for(Integer i=0;i<1000;i++){
            if(i>=keys.size())
                break;
            List<String> tmp = new List<String>();
            tmp.addAll(accountToUsers.get(keys[i]));
            acctsToUpdate.add(new Account(Id=keys[i],  Unassigned_Users__c = String.join(tmp,';')));
            accountToUsers.remove(keys[i]);
        }
        System.debug(acctsToUpdate);
        if(acctsToUpdate.size()>0){
            Database.SaveResult[] SR = Database.update(acctsToUpdate,false);
            System.debug(SR);
        }
        System.debug('After loop accountToUsers.size =>'+accountToUsers.size());
        System.debug(Limits.getHeapSize() );
        if(accountToUsers.size()>0){
            System.enqueueJob(new IVP_RecentlyUnassigned_Queueable(accountToUsers));
        }
        
    }
    
}