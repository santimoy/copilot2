@isTest
private class EmailFinderTest
{
	@testSetup static void setup()
	{
		Email_Finder_Settings__c emailFinderSetting = new Email_Finder_Settings__c( Name = 'email-checker.com',
																					Overall_Timeout__c = '10000',
																					Request_Timeout__c = '30000',
																					API_Endpoint__c = 'https://api.emailverifyapi.com/api/a/v1?key={!APIKEY}&email={!EMAIL}',
																					API_Key__c = 'EE9C41A1',
																					Is_Active__c = true );
		insert emailFinderSetting;

		Email_Format_Template__c template = new Email_Format_Template__c( Name = 'FInit.Last@webdomain.com', Enabled__c = true );
		Email_Format_Template__c template2 = new Email_Format_Template__c( Name = 'FInit.LInit@webdomain.com', Enabled__c = true );
		Email_Format_Template__c template3 = new Email_Format_Template__c( Name = 'First.LInit@webdomain.com', Enabled__c = true );
		insert new List<Email_Format_Template__c>{ template, template2, template3 };
	}

	@isTest static void testEmailFinder()
	{
		Test.setMock(HttpCalloutMock.class, new EmailFinderMockHttpResponseGenerator());
		
		EmailFinder testEmailFinder = new EmailFinder( 'test.com', 'John', 'Doe' );
		testEmailFinder.findEmailInit();
		testEmailFinder.tryDummyEmail();

		Test.startTest();

			testEmailFinder.firstName = ''; testEmailfinder.lastName = 'John Doe';
			List<String> allCombinations = testEmailFinder.getAllCombinations();
			System.assertEquals( 3, allCombinations.size() );

			testEmailFinder.firstName = ''; testEmailfinder.lastName = 'John Doe';
			testEmailFinder.currentBatch = testEmailFinder.getEmails( 'first' );
			System.assertEquals( 1, testEmailFinder.currentBatch.size() );
			
			testEmailFinder.firstName = ''; testEmailfinder.lastName = 'John Doe';
			testEmailFinder.findEmail();
			System.assertEquals( 1, testEmailFinder.finalList.size() );
			
			testEmailFinder.currentBatch = testEmailFinder.getEmails( 'second' );
			System.assertEquals( 3, testEmailFinder.currentBatch.size() );

			testEmailFinder.findEmail();
			System.assertEquals( 4, testEmailFinder.finalList.size() );

		Test.stopTest();
	}
	
	// implementing a mock http response generator for emailfinder
	public class EmailFinderMockHttpResponseGenerator implements HttpCalloutMock {
		public HTTPResponse respond(HTTPRequest req) {
			EmailFinder.EmailJson mjson = new EmailFinder.EmailJson();
			mjson.status = 'Ok';
			mjson.additionalStatus = '';
			mjson.Message = '';
			mjson.emailAddressChecked='dummy.user@test.com';
			mjson.emailAddressSuggestion='dummy.user@test.com';
			mjson.emailAddressProvided='dummy.user@test.com';
			String mjsonString = JSON.serialize(mjson);				
			System.debug(mjsonString);
			// Create a fake response
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setBody(mjsonString);
			res.setStatusCode(200);
			return res;
		}
	}

}