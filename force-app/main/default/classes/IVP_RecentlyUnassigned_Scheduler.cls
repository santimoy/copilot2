/**
* @author Amjad-Concretio
* @date 2018-04-30
* @className IVP_RecentlyUnassigned_Scheduler
*
* @description Scgeduler to schedule batch job
*/
public class IVP_RecentlyUnassigned_Scheduler implements Schedulable{
    public void execute(SchedulableContext sc) {
        IVP_RecentlyUnassigned_Batch b = new IVP_RecentlyUnassigned_Batch(); 
        Database.executeBatch(b);
    }
}