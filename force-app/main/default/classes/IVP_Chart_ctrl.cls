global class IVP_Chart_ctrl extends VisualEditor.DynamicPickList {
    /**
        * @author Amjad Khan
        * @date 26/04/2018
        *
        * @description This method returns the default value for the chart type that is 'Doughnut'. If user doesn't specify the chart type
        * then this will be the default value for the chart type for the component.
    */
    global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('Doughnut', 'doughnut');
        return defaultValue;
    }
    
    global override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows  chartTypes = new VisualEditor.DynamicPickListRows();
        chartTypes.addRow(new VisualEditor.DataRow('Doughnut', 'doughnut'));
        chartTypes.addRow(new VisualEditor.DataRow('Pie', 'pie'));
        chartTypes.addRow(new VisualEditor.DataRow('Bar', 'bar'));
        return chartTypes;
    }
    
    private static String fetchLVQuery(String viewId){
    	String jsonResponse = IVPUtils.fetchListViewDetailById(viewId);
        Map<String, Object> responseValue = new Map<String, Object>();
        try{
        	object response = JSON.deserializeUntyped(jsonResponse);
        	if(response instanceof Map<String, Object>){
            	responseValue = (Map<String, Object>)JSON.deserializeUntyped(jsonResponse);
        	}
        }catch(Exception excp){
        	
        }
        if(responseValue.containsKey('query')){
            return (String)responseValue.get('query');
        }
        return '';
    }
    @TestVisible
    static String prepareWhereClause(String viewId){
        String soqlQuery = fetchLVQuery(viewId);
        System.debug(';--------> '+soqlQuery);
        if(soqlQuery != ''){
            Integer whIndex = soqlQuery.indexOfIgnoreCase('where');
            Integer oIndex = soqlQuery.indexOfIgnoreCase('order by');
            if(whIndex > 0){
                String whereCls = '';
                if(oIndex > 0 && whIndex > 0){
                    whereCls = soqlQuery.substring(whIndex, oIndex);
                }else{
                    whereCls = soqlQuery.substring(whIndex);
                }
                Integer indexOfRT = whereCls.indexOfignoreCase('recordType');
                if(indexOfRT > 0){
                    Integer startAndRTInd = whereCls.lastIndexOfIgnoreCase('AND',indexOfRT);
                    if(startAndRTInd < 0){
                        startAndRTInd = whereCls.lastIndexOfIgnoreCase('WHERE', indexOfRT);
                        if(startAndRTInd>=0){
                            startAndRTInd+=5;
                        }
                    }
                    Integer afterAndRTInd = whereCls.indexOfIgnoreCase('AND',indexOfRT+1);
                    if(afterAndRTInd > 0){
                        whereCls = whereCls.replace(whereCls.substring(startAndRTInd, afterAndRTInd+3),'');
                    }else{
                        whereCls = whereCls.replace(whereCls.substring(startAndRTInd),'');
                    }
                }
                whereCls= (whereCls.toLowerCase().trim() == 'Where' ? '' : whereCls);
                return whereCls;
                
            }

        }
        return '';
    }
    
    @AuraEnabled
    public static Data getData(String objectName, String groupByFieldName, String whereClause, Integer createdMonth, String viewName, Boolean includeFilter, Boolean updateValue) {
        
        String filters = ' WHERE ';
        if(includeFilter != null && includeFilter){
            viewName='ivp_sd_chart_'+UserInfo.getUserId();
        }
        if(String.isNotBlank(viewName)){
            List<ListView> views = [SELECT Id FROM ListView WHERE SObjectType = 'ACCOUNT' AND DeveloperName=:viewName LIMIT 1];
            if(!views.isEmpty()){
                String lvFilter = prepareWhereClause(views[0].Id);
                System.debug('lvFilter '+lvFilter);
                if(String.isNotBlank(lvFilter)){
                   filters = lvFilter + ' AND';
                }
                else if(String.isNotEmpty(lvFilter)){
                }
            }
        }
        System.debug('->filters '+filters);
        filters+=' RecordType.Name = \'Prospect\' AND OwnerId = \''+UserInfo.getUserId()+'\'';
        
        List<ObjectWrapper> objectRecordCountList = new List<ObjectWrapper>();
        String  query='';
        SobjectField sField= Account.SObjectType.getDescribe().fields.getMap().get(groupByFieldName);
        if(sField!=null){
			system.debug('value:'+updateValue);
            if(updateValue != null && updateValue){
                
                maintainDefautDashField(groupByFieldName);
            }
            DescribeFieldResult field= sField.getDescribe();
            List<SObjectType> sTypes = field.getReferenceTo();
            if(!field.isGroupable()){
    		    throw new AuraHandledException( field.getLabel() +' can not be grouped!');
    		}
            if(!sTypes.isEmpty()){
                String namedField ='';
                Map<String, SobjectField> fieldMap = sTypes[0].getDescribe().fields.getMap();
                for(String fieldKey : fieldMap.keySet()){
                    if(fieldMap.get(fieldKey).getDescribe().isNameField()){
                        namedField = fieldKey;
                        break;
                    }
                }
                if(namedField != ''){
                    String relationShipName = groupByFieldName.endsWith('Id') ? groupByFieldName.removeEnd('Id') 
                                        : groupByFieldName.endsWithIgnoreCase('__c') ? groupByFieldName.removeEndIgnoreCase('__c')+'__r' : '';
                    
                    if(relationShipName != ''){
                        relationShipName+='.'+namedField;
                    }
                    groupByFieldName = relationShipName;
                }    
            }
        }
        if(!(String.isEmpty(objectName) || String.isEmpty(groupByFieldName))){
            query = 'SELECT count(Id), '+ groupByFieldName +' groupBy';
            query += ' FROM ' + objectName;            
            query += ' '+filters;
            query += ' GROUP BY '+groupByFieldName;	
            query += ' LIMIT 50000';
        }
        System.debug('query========='+query);
        Integer cnt = 0;
        try{
            if(!String.isEmpty(query)){
                for(AggregateResult obj : Database.query(query)){
                    String groupBy = Label.IVP_SD_Chart_No_Label_Text;
                    if(obj.get('groupBy') != NULL){
                        groupBy = String.valueOf(obj.get('groupBy'));
                    }
                    // if(groupBy!='') {
                        objectRecordCountList.add(new ObjectWrapper(groupBy,String.valueOf(obj.get('expr0'))));
                        cnt += Integer.valueOf(obj.get('expr0'));
                    // }
                }
                
            }
        }catch(System.UnexpectedException ex){
            System.debug(ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
        return new Data(objectRecordCountList,cnt);
    }
    
    @AuraEnabled
    public static List<ObjectWrapper> getGroupableFields(String sObjectName){       
        List<ObjectWrapper> fieldsNameValueList = new List<ObjectWrapper>();
        /*Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);														//getting schema of specified object
        if(sObjectType != NULL){
            Schema.DescribeSObjectResult sObjectFieldDescribe = sObjectType.getDescribe();
            Map<String, Schema.SObjectField> fieldSetMap = new Map<String, Schema.SObjectField>(sObjectFieldDescribe.fields.getMap());		// getting field map for the specified object
            for(String fieldName : fieldSetMap.keySet()){
                if(fieldSetMap.get(fieldName) != NULL){
                    Schema.DescribeFieldResult field = fieldSetMap.get(fieldName).getDescribe();
                    if(field.isGroupable() && !field.isIdLookup() && !String.valueOf(field.getType()).equalsIgnoreCase('Reference')){
                        fieldsNameValueList.add(new ObjectWrapper(field.Name, field.Label));
                    }
                }
            }
        }*/
        Map<String,String> mapOfFields = IVPUtils.getFieldSetFields('Company_Surface_Chart','Account');
        System.debug(mapOfFields);
        for(String fieldLabel : mapOfFields.keySet()){
            String tmp = mapOfFields.get(fieldLabel).remove('Calc');
			fieldsNameValueList.add(new ObjectWrapper(fieldLabel,tmp));
        }
        
        return fieldsNameValueList;
    }

    @AuraEnabled
    public static Map<String, Object> getInitData(String sObjectName){
        IVP_SD_ListView__c sdLV = IVPUtils.currentUserSetting();
        return new Map<String, Object>{
                'ivpSetting'=>sdLV,
                'fields'=>getGroupableFields(sObjectName)
        };
    }
    @AuraEnabled
    public static void maintainDefautDashField(String fieldName){
    	if(String.isNotEmpty(fieldName)){
	    	IVP_SD_ListView__c ipvSDListView = IVPUtils.currentUserSetting();
	    	ipvSDListView.Dash_BreakDown_Default_field__c = fieldName;
	    	if(ipvSDListView.SetupOwnerId == null){
	    		ipvSDListView.SetupOwnerId = UserInfo.getUserId();
	    	}
    		upsert ipvSDListView;
    	}
    }
    
    public class Data{
        @AuraEnabled
        public List<ObjectWrapper> recs;
        @AuraEnabled
        public Integer total;

        public Data(List<ObjectWrapper> recs,Integer total){
            this.recs = recs;
            this.total = total;
        }

    }
    
    public class ObjectWrapper {
        @AuraEnabled
        public String key;
        @AuraEnabled
        public String value;
        
        public ObjectWrapper(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}