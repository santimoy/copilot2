global class EmailFinderController {

	private final Account currAccount;
    public static final string KICKBOXIO = 'kickbox.io';
    public static final string EMAILHUNTER = 'emailhunter.co';
    public static final string EMAILCHECKER = 'email-checker.com';


    public list<Email_Finder_Settings__c> EmailServices{
        get;
        set;
    }


    public class EmailDefinition {

        public String EmailAddress{
            get;
            set;
        }
        public Boolean Selected{
            get;
            set;
        }

        public Boolean IsHunterValid{
            get;
            set;
        }
        public String IsKickBoxValid{
            get;
            set;
        }
        public String IsCheckerValid{
            get;
            set;
        }
    }

    public List<EmailDefinition> EmailSelection {
        get;
        set;
    }
    public String KeyContactEmailAddress {
        get;
        set;
    }

    public Boolean HasKeyContact {
        get;
        set;
    }

    public String APIKey {
        get;
        set;
    }
    public String APIName {
        get;
        set;
    }

    public String EndPoint{
        get;
        set;
    }

    public Contact KeyContact {
        get;
        set;
    }

    public String SearchResult{
        get;
        set;
    }

    public String Domain {
        get;
        set;
    }
    public EmailFinderController(ApexPages.StandardController stdController) {
        List<String> fields= new List<String>();
        fields.add('Website__c');
        stdController.addFields(fields);
        this.currAccount = (Account)stdController.getRecord();
        KeyContactEmailAddress='';
        HasKeyContact=false;
        APIKey='';
        EndPoint='';
        APIName='';
        Domain='';
        getWebServiceUrl();

        List<Contact> contacts=[select id,name,Email,FirstName,LastName from Contact where Key_Contact__c=true and Account.Id=:currAccount.Id];
        if(contacts.size()>0){
            KeyContact=contacts.get(0);
            HasKeyContact=true;
            List<String> domainParts=currAccount.Website__c.split('\\.');

            Domain=domainParts.get(domainParts.size()-2)+'.'+domainParts.get(domainParts.size()-1);
            System.URL fullURL=new Url(currAccount.Website__c);

            Domain =fullURL.getHost();
            Domain=Domain.replace('www.','');
            getEmails();
        }

        //
    }

    public void getEmails(){
        EmailSelection = new List<EmailDefinition>();
        if(!String.isBlank(Domain)){

            String sfirst=KeyContact.FirstName;
            String slast=KeyContact.LastName;
            if(KeyContact.FirstName==null) {
                List<String> splitNames=slast.split(' ');
                if(splitNames.size()>1){
                    sfirst=splitNames.get(0);
                    slast=splitNames.get(1);
                }
                else {
                    return;
                }
            }




            String fi = sfirst.substring(0,1);
            String mi = '';
            

            String li = slast.substring(0,1);
            //[[sfirst,fi],[slast,li]];
            List<List<String>> names = new List<List<String>>();
            names.add(new List<String>{sfirst,fi});
            names.add(new List<String>{slast,li});
            String[] separators = new String[]{'.', '', '_'};

            
            Integer counter = 0;
            EmailDefinition defn;
            for(Integer i=0; i<separators.size(); i++)
            {
                String s = separators[i];
                for(Integer j=0; j<2; j++)
                {
                    for(Integer k=0; k<2; k++)
                    {
                        defn= new EmailDefinition();

                        defn.EmailAddress = names.get(0).get(j)+s+names.get(1).get(k)+'@'+Domain;
                        defn.Selected=false;
                        defn.IsHunterValid=false;
                        
                        EmailSelection.add(defn);


                        defn= new EmailDefinition();

                        defn.EmailAddress = names.get(1).get(k)+s+names.get(0).get(j)+'@'+Domain;
                        defn.Selected=false;
                         defn.IsHunterValid=false;
                       
                        EmailSelection.add(defn);

                    }
                }

            }
            for(Integer j=0; j<2; j++)
            {

                defn= new EmailDefinition();

                defn.EmailAddress = names.get(0).get(j)+'@'+Domain;
                defn.Selected=false;
                 defn.IsHunterValid=false;
                       
                EmailSelection.add(defn);

            }
            for(Integer k=0; k<2; k++)
            {
                defn= new EmailDefinition();

                defn.EmailAddress = names.get(1).get(k)+'@'+Domain;
                defn.Selected=false;
                 defn.IsHunterValid=false;
                       
                EmailSelection.add(defn);
                

            }
        }
    }
    public void findEmail() {
       System.debug('findEmail:'+EndPoint);
       System.debug('findEmail:'+KeyContact.Name);
       System.debug('findEmail:'+KeyContact.FirstName);
       System.debug('findEmail:'+KeyContact.LastName);
       System.debug('findEmail:'+currAccount.Website__c);

       String firstName=KeyContact.FirstName;
       String lastName=KeyContact.LastName;
       if(KeyContact.FirstName==null) {
        List<String> names=lastName.split(' ');
        if(names.size()>1){
            firstName=names.get(0);
            lastName=names.get(1);
        }
        else {
            SearchResult='Contact name is invalid. Please check first and last name';
            return;
        }
    }
        if(KeyContact!=null && currAccount.Website__c!=null){
System.debug('Selected Count:'+EmailSelection.size());
            for(EmailDefinition defn:EmailSelection){
                System.debug('Selected Email:'+defn.EmailAddress);
                 if(defn.Selected) { 
                    for(Email_Finder_Settings__c setting:EmailServices) {
                         String ep=setting.API_Endpoint__c.replace('{!APIKEY}',setting.API_Key__c).replace('{!EMAIL}',defn.EmailAddress);

                         HttpRequest req = new HttpRequest();
                         Http http = new Http();
                         req.setMethod('GET');

                         req.setEndpoint(ep);
                         system.debug('FIND ENDPOINT:'+ep);
                         HTTPResponse resp = http.send(req);
                         String json = resp.getBody().replace('\n', '');
                         if(setting.Name==KICKBOXIO)
                            defn.IsKickBoxValid=json;
                        if(setting.Name==EMAILCHECKER)
                            defn.IsCheckerValid=json;
                            
                    }
                }
            }
         }
         else {

             SearchResult='This company has no website';

         }


       // set the method

       

   




}


public  void getWebServiceUrl(){


            // in the query below, the order by should make the Production url first and the Sandbox url last
            EmailServices = [select id, Name, API_Endpoint__c, API_Key__c, Is_Active__c from Email_Finder_Settings__c 
            where Is_Active__c=true];
            /*
            if (settings.size()>0){
                APIKey=settings.get(0).API_Key__c;
                EndPoint=settings.get(0).API_Endpoint__c;
                APIName=settings.get(0).Name;

            }
            */


        }


    }