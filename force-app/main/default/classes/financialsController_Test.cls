/*

    @description    Test Class for financialsController Controller
    @author         Mike Gill
    @date           13th July 2016

    @todo 			Add Asserts

*/
@isTest
private class financialsController_Test {


    private static Account testCompany;
    static PageReference pref;
    static financialsController ext;

    static {

        testCompany = new Account(Name = 'testCompany');
        insert testCompany;

        Financials__c newFin = new Financials__c(Account__c = testCompany.Id);
        newFin.Year_Text__c = '2012';
        newFin.Revenue__c = 1111;
        newFin.EBITDA__c = 1111;
        newFin.Gross_Margin__c = 1111;
        newFin.Head_Count__c = 1111;
        newFin.Customers__c = 11111;
        insert newFin;


    }

    @isTest static void test_SaveChanges() {
        pref = Page.TalFinancialGrid;
        pref.getParameters().put('id', testCompany.Id);
        test.setCurrentPage(pref);

        Test.startTest();
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(testCompany);
        ext = new financialsController ( con );


        ext.saveChanges();
        Test.stopTest();




    }

    @isTest static void test_addFinancials() {
        pref = Page.TalFinancialGrid;
        pref.getParameters().put('id', testCompany.Id);
        test.setCurrentPage(pref);

        Test.StartTest();
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(testCompany);
        ext = new financialsController ( con );



        ext.addFinacials();
        Test.stopTest();

    }

    @isTest static void test_newFinancials() {
        pref = Page.TalFinancialGrid;
        pref.getParameters().put('id', testCompany.Id);
        test.setCurrentPage(pref);

        Test.StartTest();
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(testCompany);
        ext = new financialsController ( con );



        ext.newFinancials();
        Test.stopTest();

    }

    @isTest static void test_cancel() {
        pref = Page.TalFinancialGrid;
        pref.getParameters().put('id', testCompany.Id);
        test.setCurrentPage(pref);

        Test.StartTest();
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(testCompany);
        ext = new financialsController ( con );



        ext.cancel();
        Test.stopTest();

    }





}