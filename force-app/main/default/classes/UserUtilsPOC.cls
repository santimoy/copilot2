public with sharing class UserUtilsPOC {

    //Get all users
    static Map<Id, User> myMap = new Map<Id, User>([Select Id, Name, ManagerId From User WHERE IsActive = true]);

    public UserUtilsPOC() {

    }

    //Adds the current user to the team
    //Recursively calls this method to get the team for each direct report
    //The top level call should return the complete list of team members
    public static List<User> getUserTeam(Id userId){
        List<User> userTeam = new List<User>();
        userTeam.add(myMap.get(userId));

        for(User u : myMap.Values()){
            if(u.ManagerId == userId){
                system.debug('Getting team for Name: ' + u.Name);
                userTeam.addAll(getUserTeam(u.Id));
            }
        }
		System.debug('userTeam+'+userTeam.size());
        return userTeam;
    }

}