/**
*@name      	: Opp_CampaignInfluenceTriggerHandler_Test
*@developer 	: 10K developer
*@date      	: 2018-10-22
*@description   : The class used as test class of OppAuto_CampaignInfluenceTriggerHandler
**/
@isTest
public class OppAuto_CampaignInfluenceTrgrHandlerTest {
    @isTest public static void testStopAutometicInfluenceRecordCreate(){
        //Try and Catch block is there because when Opp_Auto_Is_Calling_Manual__c field is false it throws exception
        try{
            //Getting record type
            Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ignite Opportunity').getRecordTypeId();
            
            //Creating Opportunity
            List<Opportunity> oppList = new List<Opportunity>();
            for(Integer i = 0; i < 10; i++){
                oppList.add(new Opportunity(StageName = 'Prospecting',CloseDate = System.today() ,Name = 'Test Opportunity '+i,RecordTypeId=recordTypeId, Amount = 0));
            }
            insert oppList;
            
            //Creating Campaign
            Campaign camp = new Campaign(Name = 'Test Campaign', IsActive = TRUE, Type='Portfolio Company Demand Gen', StartDate=System.today());
            insert camp;
            
            //Creating CampaignInfluenceModel
            CampaignInfluenceModel model = [SELECT Id FROM CampaignInfluenceModel  LIMIT 1];
            
            //Creating CampaignInfluence
            List<CampaignInfluence> campInfluenceRecords = new List<CampaignInfluence>();
            for(Integer i = 0; i < 10; i++){
                campInfluenceRecords.add(new CampaignInfluence(CampaignId=camp.Id,ModelId=model.Id,OpportunityId=oppList[i].Id,Opp_Auto_Is_Calling_Manual__c=(i< 5 ? true : false)));
            }
            insert campInfluenceRecords;
            
            Test.startTest();
            OppAuto_CampaignInfluenceTriggerHandler.stopAutometicInfluenceRecordCreate(campInfluenceRecords);
            Test.stopTest();
        }
        catch(Exception exp){
            System.debug('exp==' + exp);
            System.assert(exp.getMessage().contains('Can not process'));
        }
    }
}