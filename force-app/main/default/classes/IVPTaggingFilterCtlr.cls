/**
 * @author Dharmendra Karamchandani
 * @date 2018-04-16
 * @className IVPTaggingFilterCtlr
 * @group IVPWatchDog
 *
 * @description contains methods to handle IVPTagging component
 */
 public class IVPTaggingFilterCtlr {
	
	public class IVPTaggingFilterCtlrException extends Exception{ }
	
	@AuraEnabled
    public static object getInitData(){
    	IVPSDLVMaintainVisibilityService.checkAndCreateIVPGroupForUser(UserInfo.getUserId(), UserInfo.getName(), true);
    	return IVP_CompanyTaggingCtrl.getFilterTopics('');
    }
     
    @AuraEnabled
    public static Map<String, Object>  maitainTags(String inputJson){
    	Map<String, Object> valueMap = (Map<String, Object>)JSON.deserializeUntyped(inputJson);
		Map<String, Object> resultMap = IVPMetadataServiceProvider.maintainListView(valueMap);
		return resultMap;
    }
}