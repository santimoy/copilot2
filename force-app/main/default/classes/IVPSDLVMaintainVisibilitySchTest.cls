/**
 *  @date: May 9, 2018
 *  @author: Neha Gurnani
 *  @desciptio: This class is used to cover the code of IVPSDLVMaintainVisibilitySch's
**/
@isTest
private class IVPSDLVMaintainVisibilitySchTest {
	private static testMethod void testScheduleJob() {
					 String CRON_EXP = '0 0 0 3 9 ? 2018';
        			 String jobId=system.schedule('testingSchedule',CRON_EXP,new IVPSDLVMaintainVisibilitySch());
	}
@isTest
    private static void testBatchJob(){
        Test.startTest();
        			IVPSDLVMaintainVisibilitySch ivp=new IVPSDLVMaintainVisibilitySch();
        			 Database.executeBatch(ivp);
        Test.stopTest();
    }

}