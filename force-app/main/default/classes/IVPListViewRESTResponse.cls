public class IVPListViewRESTResponse {
	
	public class LVResponse{
		// @AuraEnabled public String describeUrl;
		@AuraEnabled public String developerName;
		@AuraEnabled public String id;
		@AuraEnabled public String label;
		// @AuraEnabled public String resultsUrl;
		// @AuraEnabled public String soqlCompatible;
		// @AuraEnabled public String url;
	}
	@AuraEnabled 
	public List<LVResponse> listviews;
	
	public IVPListViewRESTResponse(){
		listviews = new List<LVResponse>();
	}
}