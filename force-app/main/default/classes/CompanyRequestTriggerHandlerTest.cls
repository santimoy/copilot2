/***********************************************************************************
 * @author      10k-Expert
 * @name		CompanyRequestTriggerHandlerTest
 * @date		2018-11-19
 * @description	To test CompanyRequestTriggerHandler
 ***********************************************************************************/
@isTest
public class CompanyRequestTriggerHandlerTest {
    @isTest
    public static void testAfterInsert(){
        IVPOwnershipTestFuel testData = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        
        Test.startTest();
        
        List<Company_Request__c> companyRequests = testData.companyRequests;
        
        Test.stopTest();
        // one Company_Request__Share record will be created for each Company_Request record and one will be cloned for that, 
        // so for two company_request records total 4 Company_Request__Share will be created.
        //System.assertEquals([SELECT count() FROM Company_Request__Share], 2);
        System.assertEquals([SELECT count() FROM Company_Request__Share], 4);
    }
    
	@isTest
    public static void testBeforeUpdate(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        List<Account> accounts = testData.accounts;
        accounts[0].Top_10__c = true;
        update accounts;
        List<Company_Request__c> companyRequestLst = testData.companyRequests;
        
        companyRequestLst[0].Status__c = 'Accepted';
        companyRequestLst[1].Status__c = 'Rejected';
        
        Test.startTest();
        
        update companyRequestLst;

        Test.stopTest();
        
        System.assertEquals([SELECT Partial_Credit_User__c FROM Account LIMIT 1].Partial_Credit_User__c, UserInfo.getUserId());
    }
    // for testing status 'Transfer - Undisputed' and 'Transfer - No Response'
    @isTest
    public static void testAfterStatusUpdate(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        List<Company_Request__c> companyRequestLst = testData.companyRequests;
        
        companyRequestLst[0].Status__c = 'Transfer - Undisputed';
        companyRequestLst[1].Status__c = 'Transfer - No Response';
        
        Test.startTest();
        
        update companyRequestLst;

        Test.stopTest();
        List<Account> accList = [SELECT Id,Ownership_Status__c,Partial_Credit_User__c FROM Account];
        for(Account acc : accList){
            System.assertEquals(acc.Partial_Credit_User__c,UserInfo.getUserId());
            System.assertEquals(acc.Ownership_Status__c,'Priority');
            
        }
        //System.assertEquals([SELECT Partial_Credit_User__c FROM Account LIMIT 1].Partial_Credit_User__c, UserInfo.getUserId());
    }
}