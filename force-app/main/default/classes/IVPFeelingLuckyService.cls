/**
 * @author MukeshKS-Concretio
 * @date 2018-04-26
 * @className IVPFeelingLuckyService
 * @group FeelingLucky
 *
 * @description contains methods related to I'm feeling lucky
 */
public class IVPFeelingLuckyService {
	public class IVPFeelingLuckyServiceException extends Exception{}
    
    
    /**
	 * @description returns max count to assign company
	 * @return Integer
	 * @author Mukesh-Concretio
	 */
    public static Integer maxCompanyToAssign  = IVPUtils.maxCompanyToAssign;
    
    /**
	 * @description returns max reject count of IFL record
	 * @return Integer
	 * @author Mukesh-Concretio
	 */
    public static Integer maxIFLRejectAttempts  = IVPUtils.maxIFLRejectAttempts;
    
    /**
     * @description returns Id of current loggedin User Id
     * @return Id
     * @author MukeshKS-Concretio
     */
    private static Id currentLoggedInUserId = UserInfo.getUserId();
    
    /**
     * @description returns Id of Unassigned User (Username is Unassigned.)
     * @return Id
     * @author MukeshKS-Concretio
     */
    private static Id unassignedUserId = IVPUtils.unassignedUserId;
    
    /**
     * @description returns Id of I'm Feeling Lucky record type id from Intelligence Feedback object
     * @return Id
     * @author MukeshKS-Concretio
     */
    private static Id iFLRecordTypeId = IVPUtils.iFLRecordTypeId;
    
    /**
     * @description returns set of valid feedback type
     * @return Set<String>
     * @author MukeshKS-Concretio
     */
    private static Set<String> validFeedbackType = new Set<String>{'Accept','Reject'};
   
    
    /**
     * @description GetFeelingLuckyList - this method is used for to get list of IFL records of current logged in user
     * @param void
     * @return List<Intelligence_Feedback__c> feelingLuckyList
     * @author MukeshKS-concretio
	*/
    public static IVPFeelingLuckyWrapper getFeelingLuckyList(Boolean doCalloutForList){

		Map<String, Map<String,Schema.DisplayType>> fieldSetFieldsWithType = IVPUtils.getFieldSetFieldsWithType('IFL_Component_Fields','Intelligence_Feedback__c');
        
        String query = 'SELECT Id, Company_Evaluated__r.Id, Company_Evaluated__r.Name, '
            		 + 'Company_Evaluated__r.Website, Company_Evaluated__r.Website_LinkedIn__c, Company_Evaluated__r.DWH_LinkedIn__c '
            		 + ( (!fieldSetFieldsWithType.isEmpty()) ? ' ,' + String.join(new List<String>(fieldSetFieldsWithType.keySet()), ', ') + ' ' : '')
            		 + 'FROM Intelligence_Feedback__c '
            		 + 'WHERE Is_Expired__c = False AND User__c = :currentLoggedInUserId AND Accurate_IQ_Score__c = null '
            		 + 'AND RecordTypeId = :iFLRecordTypeId '
            		 + 'ORDER BY Company_Evaluated__r.IFL_Load_Date__c DESC '
                     + 'LIMIT :maxCompanyToAssign ';
        
        List<Intelligence_Feedback__c> feelingLuckyList = new List<Intelligence_Feedback__c>();
        
        for(Intelligence_Feedback__c intlFeedbackObj: ( List<Intelligence_Feedback__c> ) Database.query(query)){

            feelingLuckyList.add(intlFeedbackObj);
        }
        
        IVPFeelingLuckyWrapper feelingLuckyRecords = new IVPFeelingLuckyWrapper();
        
        feelingLuckyRecords.iflRecords = feelingLuckyList;
        
        feelingLuckyRecords.fieldsAndLabels = JSON.serialize(fieldSetFieldsWithType);
		
        if (doCalloutForList) {   

            //rejectFeedbackPicklist 
	        feelingLuckyRecords.feedbackPicklist = IVPUtils.retrievePicklistByRecordTypeId(iFLRecordTypeId,'Why_is_score_wrong__c','Intelligence_Feedback__c');
        }
        
        return feelingLuckyRecords;
    }
    
    /**
     * @description SaveIFLFeedback - this method is used for to save IFL feedback
     * @param Intelligence_Feedback__c feedbackObj
     * @return List<Intelligence_Feedback__c> list of IFL object
     * @author MukeshKS-concretio
	*/
    public static IVPResponse saveIFLFeedback(Intelligence_Feedback__c feedbackObj){
        IVPResponse response = new IVPResponse();
        
        String feedbackType = feedbackObj.Accurate_IQ_Score__c;
		feedbackObj.User_Response_Date_Time__c = System.now();		
        
        if(feedbackObj == null){
            throw new IVPFeelingLuckyServiceException('An error occurred attempting to save IFL feedback:'
                                                   +'\n feedbackObj was null.');
        }
        
        if(feedbackType == null){
            throw new IVPFeelingLuckyServiceException('An error occurred attempting to save IFL feedback:'
                                                   +'\n feedbackType was null.');
        }
        
        if(!validFeedbackType.contains(feedbackType)){
            throw new IVPFeelingLuckyServiceException('An error occurred attempting to save IFL feedback:'
                                                   +'\n feedbackType was invalid.');
        }
            
        if(!isValidIFLRecord(feedbackObj.Id)){
            feedbackObj.Accurate_IQ_Score__c = 'Ignore';
            feedbackObj.Why_is_score_wrong__c = '';
            feedbackObj.Comments__c = '';
            // assign new IFL record
            assignIFLRecords(currentLoggedInUserId, 1);
        }
        
		List<Intelligence_Feedback__c> feedbacks = new List<Intelligence_Feedback__c>{feedbackObj};
        
        if(!feedbacks.isEmpty()){
            
            List<Database.SaveResult> feedbackSaveResult = Database.update(feedbacks);
            
            if(!feedbackSaveResult[0].isSuccess()){
                throw new IVPFeelingLuckyServiceException('An error occurred attempting to save IFL feedback: ' 
                                                       + feedbackSaveResult[0].getErrors()[0].getMessage());
            } else {
                
                response.code = feedbacks[0].Accurate_IQ_Score__c;
                 
                if(response.code == 'Accept' || response.code == 'Reject'){
                    response.data = (Object) feedbacks[0];
                    response.message = (response.code == 'Reject') ? 'Thanks for your valuable feedback!' : 'Thanks for accepting this company!';
                } else if(response.code == 'Ignore'){
                    response.message = 'Sorry this company has been assigned to someone else, we\'ve added a new Company to your list.';
                    response.data = (Object) getFeelingLuckyList(false);
                }
            }
        }
        System.debug('response' +response);
        return response;
    }
    
    /**
     * @description isValidIFLRecord - this method is used for to check IFL record is valid or not
     * @param Id iFLRecordId
     * @return Boolean
     * @author MukeshKS-concretio
	*/
    public static Boolean isValidIFLRecord(Id iFLRecordId) {
        
        String query = 'SELECT COUNT() FROM Intelligence_Feedback__c '
            		 + 'WHERE Id = :iFLRecordId AND User__c = :currentLoggedInUserId AND Accurate_IQ_Score__c = null '
                     + 'AND Is_Expired__c = False '
            		 + 'AND RecordTypeId = :iFLRecordTypeId ' 
                     + 'AND Company_Evaluated__r.IFL_Load_Date__c != null '
            		 + 'AND Company_Evaluated__r.OwnerId = :unassignedUserId ' ;
        
        //List<Intelligence_Feedback__c> intelligenceFeedbackList = ( List<Intelligence_Feedback__c> ) Database.query(query);
        return (Database.countQuery(query) > 0 ) ? true : false;
    }
    
    /**
     * @description assignIFLRecords - this method is used for to assign new IFL records
     * @param Set<Id> userIds
     * @param Integer limt how many records need to be assign
     * @return List<Intelligence_Feedback__c> iFLRecords - list of newly assigned IFL records
     * @author MukeshKS-concretio
	*/
    public static List<Intelligence_Feedback__c> assignIFLRecords(Id userId, Integer limt){
        List<Intelligence_Feedback__c> iFLRecords = new List<Intelligence_Feedback__c>();
        
        List<Account> validCompanies = getValidCompanies(limt);
        System.debug('validCompanies'+validCompanies);
        if(!validCompanies.isEmpty()) {
            
            for(Account companyObj:validCompanies){
                iFLRecords.add(new Intelligence_Feedback__c(
                    Company_Evaluated__c = companyObj.Id,
                    User__c = userId,
                    Sequence__c = companyObj.IFL_Sequence__c + 1,
                    Assignment_Date_Time__c = System.now(),
                    IQ_Score_at_Creation__c =   companyObj.IQ_Score__c,
                    RecordTypeId = iFLRecordTypeId
                ));
            }
        }
        
        if(!iFLRecords.isEmpty()){
            
            List<Database.SaveResult> iFLRecordsSaveResult = Database.insert(iFLRecords);
            
            if(!iFLRecordsSaveResult[0].isSuccess()){
                throw new IVPFeelingLuckyServiceException('An error occurred attempting to assign new IFL record: ' 
                                                       + iFLRecordsSaveResult[0].getErrors()[0].getMessage());
            }   
        }
        return iFLRecords;
    }
    
    /**
     * @description GetValidCompanies - this method is used for to get list of valid companies to assign with IFL records
     * @param Integer limt pass limit to get number of companies
     * @return List<Account>
     * @author MukeshKS-concretio
	*/
    public static List<Account> getValidCompanies(Integer limt){
        
        List<Account> validCompanies = new List<Account>();
        
        String query = 'SELECT Id, IQ_Score__c, IFL_Sequence__c FROM Account WHERE OwnerId = :unassignedUserId '
            		 + 'AND IFL_Load_Date__c != null AND IFL_Reject_Count__c < :maxIFLRejectAttempts AND IFL_Is_Assignment_Ready__c = True '
            		 + 'AND Id NOT IN (SELECT Company_Evaluated__c FROM Intelligence_Feedback__c '
            		 + 'WHERE Is_Expired__c = False AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId ) '
            		 + 'AND Id NOT IN (SELECT Company_Evaluated__c FROM Intelligence_Feedback__c '
            		 + 'WHERE User__c = :currentLoggedInUserId AND RecordTypeId = :iFLRecordTypeId )'
        			 + 'ORDER BY IFL_Load_Date__c DESC '
            		 + 'LIMIT :limt';
        
        for(Account companyObj:( List<Account> ) Database.query(query)){
            validCompanies.add(companyObj);
        }
        
        return validCompanies;
    }
    
    /**
     * GetUserCompanySet this method prepares Map of userId and set of companyIds which companies
     * assigned to users
     * @param  List<Account> companies contains list of companies
     * @param  List<Id> userIds   contains list of userIds
     * @return Map<Id,Set<Id>> Map of userId and set of companyIds which companies
     * assigned to users
     * @author Mukesh-Concretio
     */
    public static Map<Id, Set<Id>> getUserCompanySet(List<Account> companies, List<Id> userIds){
        Map<Id, Set<Id>> userCompanyMap = new Map<Id,Set<Id>>();
        
        Map<Id, Account> companiesMap = new Map<Id, Account>(companies);
        Set<Id> companyIds = companiesMap.keySet();
        
        String query = 'SELECT Id, User__c,Company_Evaluated__c FROM Intelligence_Feedback__c '
                     + 'WHERE User__c IN :userIds AND Company_Evaluated__c IN :companyIds AND RecordTypeId = :iFLRecordTypeId ';
        
        for(Intelligence_Feedback__c obj: (List<Intelligence_Feedback__c>) Database.query(query)) {
            if( userCompanyMap.containsKey(obj.User__c) ){
                userCompanyMap.get( obj.User__c ).add( obj.Company_Evaluated__c );
            }else{
                userCompanyMap.put( obj.User__c, new Set<Id> { obj.Company_Evaluated__c } );
            }
        }
        
        return userCompanyMap;
    }

    /**
     * GetUsersActiveIFLCountMap this method prepares Map of userId and count of assigned ifl records
     * @param List<Id> userIds
     * @return Map<Id,Integer> returns Map of userId and count of assigned ifl records
     * @author Mukesh-Concretio
     */
    public static Map<Id, Integer> getUsersActiveIFLCountMap(List<Id> userIds){
        Map<Id, Integer> usersActiveIFLCountMap = new Map<Id, Integer>();

        for(Id userId:userIds){

           usersActiveIFLCountMap.put(userId, 0);
        }
        
        String query = 'SELECT Id, User__c,Company_Evaluated__c FROM Intelligence_Feedback__c '
                     + 'WHERE User__c IN :userIds AND Is_Expired__c = False AND Accurate_IQ_Score__c = null AND RecordTypeId = :iFLRecordTypeId ';
        
        for(Intelligence_Feedback__c obj: (List<Intelligence_Feedback__c>) Database.query(query)) {

            Id userId = obj.User__c;
            
            Integer recordCount = usersActiveIFLCountMap.get(userId);
                
            usersActiveIFLCountMap.put(userId, recordCount + 1 );
        }
        
        return removeFullyAssignedUsers(usersActiveIFLCountMap);
    }

    /**
     * RemoveFullyAssignedUsers this method removed users who have maxCompanyToAssign companies
     * @param Map<Id,Integer> usersActiveIFLCountMap contains Map of userId and count of assigned IFL
     * records
     * @return Map<Id,Integer> usersActiveIFLCountMap contains Map of userId and count of assigned IFL
     * records
     */
    public static Map<Id, Integer> removeFullyAssignedUsers(Map<Id, Integer> usersActiveIFLCountMap){
        
        for(Id userId : usersActiveIFLCountMap.keyset() ){

           if(  usersActiveIFLCountMap.get(userId) >= IVPUtils.maxCompanyToAssign){
                usersActiveIFLCountMap.remove(userId);
           }

        }
        return usersActiveIFLCountMap;
    }

    /**
     * RunIFLBatch - this method runs batch to assign IFL records to users
     * @return void
     */
    public static void runIFLBatch(){
        if(IVPUserService.isCurrentUserSystemAdmin()){
            Database.executeBatch(new IVPFeelingLuckyAssignmentBatch());
        } else {
            throw new IVPFeelingLuckyServiceException('You don\'t have permission to process IFL batch!');
        }
    }
}