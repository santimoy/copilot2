/**
 * @File Name          : HivebriteIntegrationQueueableTest.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 9:09:10 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/11/2019   Anup Kage     Initial Version
**/
@isTest(SeeAllData = false)
public with sharing class HivebriteIntegrationQueueableTest {
    @TestSetup
    static void makeData(){
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = true;
        accToken.Event_Log_Level__c = 'DEBUG';
        UPSERT accToken;
    }
    @IsTest
    static void getHivebriteUsersIntoSFContacts(){
        
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteUserTestClass');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);

        HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable('CONTACT_HB_TO_SF');
        ID jobID = System.enqueueJob(nextJob);
        
        Test.stopTest();
        
    }
    @IsTest
    static void getHivebriteExperienceIntoSFContacts(){ 
        
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HivebriteExperienceTestClass');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);

        HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable('EXPERIENCE_HB_TO_SF');
        ID jobID = System.enqueueJob(nextJob);
        Test.stopTest();
        
    }
    @IsTest
    static void coverMethods(){
        
        Test.startTest();
        List<Sobject> recordList = new List<Sobject>();
       insert recordList;
           HivebriteIntegrationQueueable obj = new HivebriteIntegrationQueueable();
            HivebriteIntegrationQueueable objList = new HivebriteIntegrationQueueable('CONTACT_SF_TO_HB', new List<Object>());
           HivebriteIntegrationQueueable accountList = new HivebriteIntegrationQueueable('CONTACT_SF_TO_HB', new List<Account>());
            
        Test.stopTest();
        
    }
    @IsTest
    static void diablesIntegration(){
        
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponseWithDiffUsers();
        Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
        accToken.Integration_Is_Enabled__c = false;
        accToken.Event_Log_Level__c = 'INFO';
        UPSERT accToken;
        HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable('EXPERIENCE_HB_TO_SF');
        ID jobID = System.enqueueJob(nextJob);

        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);

        HivebriteIntegrationQueueable nextJob1 = new HivebriteIntegrationQueueable('PORTFOLIO_SF_TO_HB', recordList);
        ID jobID1 = System.enqueueJob(nextJob1);
        Test.stopTest();
        
    }
    @IsTest
    static void allMethodsCover(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        Test.startTest();
             
            HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable('PORTFOLIO_SF_TO_HB', recordList);
            ID jobID = System.enqueueJob(nextJob);           
            
        Test.stopTest();
        
    }
    @IsTest
    static void PORTFOLIOHBTOSF(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        Test.startTest();
        HivebriteIntegrationQueueable nextJob1 = new HivebriteIntegrationQueueable('PORTFOLIO_HB_TO_SF');
        ID jobID1 = System.enqueueJob(nextJob1);
        Test.stopTest();
        
    }
    @IsTest
    static void PARTNERSFTOHB(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        
        Test.startTest();
        HivebriteIntegrationQueueable nextJob2 = new HivebriteIntegrationQueueable('PARTNER_SF_TO_HB', recordList);
        ID jobID2 = System.enqueueJob(nextJob2);
        Test.stopTest();
        
    }
    @IsTest
    static void CONTACTHBTOSF(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        
        Test.startTest();
        HivebriteIntegrationQueueable nextJob3 = new HivebriteIntegrationQueueable('CONTACT_HB_TO_SF');
        ID jobID3 = System.enqueueJob(nextJob3);
        Test.stopTest();
        
    }
    @IsTest
    static void CONTACTSFTOHB(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        
        Test.startTest();
        HivebriteIntegrationQueueable nextJob4 = new HivebriteIntegrationQueueable('CONTACT_SF_TO_HB', contactList);
            ID jobID4 = System.enqueueJob(nextJob4);

        Test.stopTest();
        
    }
    @IsTest
    static void EXPERIENCEHBTOSF(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        
        Test.startTest();
        HivebriteIntegrationQueueable nextJob5 = new HivebriteIntegrationQueueable('EXPERIENCE_HB_TO_SF', System.now());
        ID jobID5 = System.enqueueJob(nextJob5);
        Test.stopTest();
        
    }
    @IsTest
    static void methodName(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteExpId(Company.Id); 

        String query1 = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> recordList = Database.query(query1);
        // Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        
        HivebriteIntegrationTestFactory.setupMockResponse();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        Test.startTest();
        HivebriteIntegrationQueueable nextJob6 = new HivebriteIntegrationQueueable('EXPERIENCE_SF_TO_HB', contactList);
            ID jobID6 = System.enqueueJob(nextJob6);

        HivebriteIntegrationQueueable nextJob7 = new HivebriteIntegrationQueueable('DELETECONTACT_SF_TO_HB', contactList);
        nextJob7.enqueueNextJob('DELETECONTACT_SF_TO_HB');
        nextJob7.enqueueNextJob('DELETECONTACT_SF_TO_HB', contactList);
        ID jobID7 = System.enqueueJob(nextJob7);
        Test.stopTest();
        
    }
   
}