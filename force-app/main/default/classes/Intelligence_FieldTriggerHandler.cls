/*
*  Intelligence_FieldTriggerHandler is used to perform logic for Intelligence_FieldTrigger
*
*  Author: Virendra
*  Date:   April 09, 2018
*  
*/

public with sharing class Intelligence_FieldTriggerHandler {
    
    
    public void Intelligence_FieldTriggerHandler(){
        
        
    }

    // Handle all Events to be process on Before Insert 
    public static void handleBeforeInsert(list<Intelligence_Field__c> intelligenceFields){
        validateTarget_DBFieldNames(intelligenceFields);   
    }
    
    // Handle all Events to be process on Before Update 
    public static void handleBeforeUpdate(list<Intelligence_Field__c> intelligenceFields, Map<id,Intelligence_Field__c> oldintelligenceFields){
       list<Intelligence_Field__c>   intFieldsUpdated = new list<Intelligence_Field__c>();
       
       set<String> columnsToBeMakeHidden = new set<String>(); 
       
       for(Intelligence_Field__c Intelligence_Field : intelligenceFields){
         
         if(oldintelligenceFields.get(Intelligence_Field.Id).Target_Field__c != Intelligence_Field.Target_Field__c){
           intFieldsUpdated.add(Intelligence_Field);
         }
         
         //Check if COlumns is updated to Hide Values from Search list result
         if(Intelligence_Field.Hide_Values__c == true && 
             oldintelligenceFields.get(Intelligence_Field.Id).Hide_Values__c != Intelligence_Field.Hide_Values__c){
             String columnDbColumnName =    Intelligence_Field.DB_Column__c;
             
             columnDbColumnName = columnDbColumnName.substring(columnDbColumnName.lastIndexOf('.')+1);
            
             String columnUILabel = Intelligence_Field.UI_Label__c;
             if(Intelligence_Field.UI_Label__c != null){
                columnsToBeMakeHidden.add(columnUILabel+' ('+columnDbColumnName+')');
             }else{
                columnsToBeMakeHidden.add(columnDbColumnName);
             }
         }
         
       }
       
       if(!intFieldsUpdated.isEmpty()){
         validateTarget_DBFieldNames(intFieldsUpdated);   
       }
       
       if(!columnsToBeMakeHidden.isEmpty()){
           rePopulateIntelligenceUserPreferences(columnsToBeMakeHidden);
       }
    }
    
      // Re Populate Intelligence User Preferences
    private static void rePopulateIntelligenceUserPreferences(Set<String> columnsToBeMakeHidden){
        
        list<Intelligence_User_Preferences__c> intPreferencesUpdated = new list<Intelligence_User_Preferences__c>();
        
        for(Intelligence_User_Preferences__c  intPreference: [ Select id,Display_Columns__c,User_Id__c from Intelligence_User_Preferences__c]){
            
            String currentDisplayColumnStr = intPreference.Display_Columns__c;
            for(String columnHeader : columnsToBeMakeHidden){ 
                 if(currentDisplayColumnStr.contains((columnHeader+';'))){
                     currentDisplayColumnStr = currentDisplayColumnStr.replace((columnHeader+';'),'');
                 }else if(currentDisplayColumnStr.contains(columnHeader)){
                     currentDisplayColumnStr = currentDisplayColumnStr.replace(columnHeader,'');
                 }
            }
            intPreference.Display_Columns__c = currentDisplayColumnStr;
            intPreferencesUpdated.add(intPreference);
        }
        
        if(!intPreferencesUpdated.isEmpty()){
            update intPreferencesUpdated;
        }
        
    }

    private static void validateTarget_DBFieldNames(list<Intelligence_Field__c> intelligenceFields){
       
       Map<String, Schema.SObjectType> schemaMap =  Schema.getGlobalDescribe();
       Map<String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
      
       for(Intelligence_Field__c Intelligence_Field : intelligenceFields){
           String  targetColumnName =  Intelligence_Field.Target_Field__c;
           if(targetColumnName != null){
           
              
              
              targetColumnName = Intelligence_Field.Target_Field__c; //targetColumnName.toLowerCase();
              

              
              if(!fieldMap.containsKey(targetColumnName)){
                 Intelligence_Field.addError( System.label.DW_Mapping_Exception_Message+' For Target Field: ' +targetColumnName );
              }else{
                  Schema.DescribeFieldResult fo = fieldMap.get(targetColumnName).getDescribe();
                  Intelligence_Field.Target_field_type__c = String.valueOf(fo.getType());
                  Intelligence_Field.Target_field_length__c = fo.getLength();              
                  
              }
           }
       }

    }
    
    
}