/*
Class Name   : KeyContactScopeTriggerHandlerTest
Description  : Test Code for Handler for trigger on Key Contact scope object
Author       : Sukesh
Created Date : 26th August 2019
*/
@isTest
private class KeyContactScopeTriggerHandlerTest {

    @isTest
    static void testKeyContactBestContactPopulation(){

        List<Account> lstAccountToInsert = new List<Account>();
        List<Contact> lstContactToInsert = new List<Contact>();
        List<Key_Contact_Scope__c> lstKeyContactScopeToInsert = new List<Key_Contact_Scope__c>();

        for(Integer intCount=0; intCount<25; intCount++) {

            lstAccountToInsert.add(new Account(RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId(),
                                                       Name = 'Test Account '+intCount));
        }

        insert lstAccountToInsert;

        for(Integer intAccountCount=0; intAccountCount<20; intAccountCount++) {

                lstContactToInsert.add(new Contact(LastName = 'Test Contact '+intAccountCount,
                                                   AccountId = lstAccountToInsert[intAccountCount].Id,
                                                   Email = 'TestContact'+intAccountCount+'@mail.com'));
        }

        insert lstContactToInsert;
        for(Integer intCount=0; intCount<20; intCount++) {

            lstKeyContactScopeToInsert.add(new Key_Contact_Scope__c(Account__c = lstAccountToInsert[intCount].Id,
                                                                    Account_Id__c = lstAccountToInsert[intCount].Id));
        }

        insert lstKeyContactScopeToInsert;


        Integer intCount = 0;
        for(Key_Contact_Scope__c objKeyContactScope : lstKeyContactScopeToInsert) {

            objKeyContactScope.Best_Contact_Email__c = lstContactToInsert[intCount].Email;
            intCount++;
        }

        update lstKeyContactScopeToInsert;

        intCount = 0;
        for(Key_Contact_Scope__c objKeyContactScope : lstKeyContactScopeToInsert) {

            objKeyContactScope.Best_Contact_Email__c = '';
            intCount++;
        }

        update lstKeyContactScopeToInsert;
    }
}