@isTest
private class test_temp_AccountWebsiteUpdate {
	
	@isTest static void test_method_one() {
		Account acc = new Account(Name = 'testAccount',Website='www.google.com');
		insert acc;
		acc.Website_Domain__c = '';
		update acc;
		Database.executeBatch(new temp_AccountWebsiteUpdate(), 1);
	}
}