/***********************************************************************************
* @author      10k-Expert
* @name    IVPOwnershipInboundEmailServiceTest
* @date    2019-01-21
* @description  To test IVPOwnershipInboundEmailService
***********************************************************************************/
@isTest
public class IVPOwnershipInboundEmailServiceTest {
    @isTest
    public static void acceptTestCase(){
        
        IVPOwnershipTestFuel testData = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        
        List<Company_Request__c> companyRequests = testData.companyRequests;
        Company_Request__c crObj = companyRequests[0];
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Testing Mail';
        email.fromName = 'test test';
        email.fromAddress = 'rtyagi@10kview.com';
        email.htmlBody = 'THis is test mail body/'+crObj.Id;
        List<String> references = new List<String>();
        references.add('rtyagi@10kview.com');
        
        email.references = references;
        email.plainTextBody = 'Accept-----Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'rtyagi@10kview.com';
        // setup controller object
        IVPOwnershipInboundEmailService serviceObj = new IVPOwnershipInboundEmailService();
        Messaging.InboundEmailResult result = serviceObj.handleInboundEmail(email, envelope);
        Company_Request__c crObj2 = [Select Id,Name,Status__c,Comment_By_Owner__c From Company_Request__c Where Id =: crObj.id];
        System.assertEquals( result.success  ,true); 
        System.assert(result.success == true);
        System.assert(crObj2.Status__c == 'Accepted');
        
        
    }
    @isTest
    public static void failTestCase(){
        
        IVPOwnershipTestFuel testData = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        
        List<Company_Request__c> companyRequests = testData.companyRequests;
        Company_Request__c crObj = companyRequests[0];
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Testing Mail';
        email.fromName = 'test test';
        email.fromAddress = 'rtyagi@10kview.com';
        email.htmlBody = 'THis is test mail body/'+crObj.Id;
        List<String> references = new List<String>();
        references.add('rtyagi@10kview.com');
        
        email.references = references;
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'rtyagi@10kview.com';
        
        // setup controller object
        IVPOwnershipInboundEmailService serviceObj = new IVPOwnershipInboundEmailService();
        Messaging.InboundEmailResult result = serviceObj.handleInboundEmail(email, envelope);
        Company_Request__c crObj2 = [Select Id,Name,Status__c,Comment_By_Owner__c From Company_Request__c Where Id =: crObj.id];
        System.assert(result.success == true);
        System.assert(crObj2.Status__c == 'pending');
        
    }
    @isTest
    public static void rejectTestCase(){
        
        IVPOwnershipTestFuel testData = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        
        List<Company_Request__c> companyRequests = testData.companyRequests;
        Company_Request__c crObj = companyRequests[0];
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        
        email.subject = 'Testing Mail';
        email.fromName = 'test test';
        email.fromAddress = 'rtyagi@10kview.com';
        email.htmlBody = 'THis is test mail body/'+crObj.Id;
        List<String> references = new List<String>();
        references.add('rtyagi@10kview.com');
        
        email.references = references;
        email.plainTextBody = 'reject,Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'rtyagi@10kview.com';
        


        // setup controller object
        IVPOwnershipInboundEmailService serviceObj = new IVPOwnershipInboundEmailService();
        Messaging.InboundEmailResult result = serviceObj.handleInboundEmail(email, envelope);
        Company_Request__c crObj2 = [Select Id,Name,Status__c,Comment_By_Owner__c From Company_Request__c Where Id =: crObj.id];
        System.assert(result.success == true);
        System.assert(crObj2.Status__c == 'Rejected');
           
    }
    @isTest
    public static void limitTestCase(){
        
        IVPOwnershipTestFuel testData = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        
        List<Company_Request__c> companyRequests = testData.companyRequests;
        Company_Request__c crObj = companyRequests[0];
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Testing Mail';
        email.fromName = 'test test';
        email.fromAddress = 'rtyagi@10kview.com';
        email.htmlBody = 'THis is test mail body/'+crObj.Id;
        List<String> references = new List<String>();
        references.add('rtyagi@10kview.com');
        
        email.references = references;
        email.plainTextBody = 'accept,Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'rtyagi@10kview.com';
        List<Opportunity> oppLst = testData.opportunities;
        List<IVP_General_Config__c> ivpGeneralLst = new List<IVP_General_Config__c>();
        ivpGeneralLst.add(IVP_General_Config__c.getValues('total-count'));
        ivpGeneralLst.add(IVP_General_Config__c.getValues('priority-count'));
        ivpGeneralLst[0].Value__c='0';
        ivpGeneralLst[1].Value__c='0';
        update ivpGeneralLst;
        // setup controller object
        IVPOwnershipInboundEmailService serviceObj = new IVPOwnershipInboundEmailService();
        Messaging.InboundEmailResult result = serviceObj.handleInboundEmail(email, envelope);
        Company_Request__c crObj2 = [Select Id,Name,Status__c,Comment_By_Owner__c From Company_Request__c Where Id =: crObj.id];
        System.assert(result.success == true);
        System.assert(crObj2.Status__c == 'Pending');
           
    }
}