/*
Class Name   :  Batch_CallCBforAccountWithOutContact
Description  :  To Mark call clear bit true for Key Contact Scope's Account where Number of Contacts is Zero
Author       :  Sukesh
Created On   :  August 20 2019
*/
global with sharing class Batch_CallCBforAccountWithOutContact implements Database.Batchable<sObject>,Database.AllowsCallouts {

    String strQuery = 'SELECT Id, Account__c, Number_of_Contacts_Initial__c,Account__r.'+System.Label.Clearbit_Domain+' FROM Key_Contact_Scope__c  WHERE Number_of_Contacts_Initial__c =null  OR Number_of_Contacts_Initial__c = 0'+(Test.isRunningTest()?' LIMIT 10':'');/* Id=\'a6N0m0000009tnBEAQ\' */

    global Batch_CallCBforAccountWithOutContact() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Key_Contact_Scope__c> lstKeyContactScope) {

        List<Account> lstAccountToUpdate = new List<Account>();
        List<Key_Contact_Scope__c> lstKeyContactScopeToUpdate = new List<Key_Contact_Scope__c>();

        Map<String, Account> mapAccountIdToAccount = new Map<String, Account>();

        Set<String> setAccountId = new Set<String>();

        for(Key_Contact_Scope__c objKeyContactScope : lstKeyContactScope) {

            setAccountId.add(objKeyContactScope.Account__c);
        }


        for(Account objAccount : (List<Account>)(Database.query('SELECT Id, '+System.Label.Clearbit_Domain+' FROM Account WHERE Id IN : setAccountId'))) {

            mapAccountIdToAccount.put(objAccount.Id, objAccount);
        }

        for(Key_Contact_Scope__c objKeyContactScope : lstKeyContactScope) {

            sendAccountInfo(String.ValueOf((mapAccountIdToAccount.get(objKeyContactScope.Account__c)).get(System.Label.Clearbit_Domain)), objKeyContactScope.Account__c);

            lstKeyContactScopeToUpdate.add(new Key_Contact_Scope__c(Id=objKeyContactScope.Id,
                                                                    Processed_Date_Time__c = system.now()));
        }

        update lstKeyContactScopeToUpdate;
    }

    global void finish(Database.BatchableContext BC) {

    }

    /**
    @Description Method Create JSON Payload for ClearBit API Call
    @author Sukesh
    @date August 20 2019
    @param strDomain The website of Account
    @param  strAccountId Account Id for which PayLoad needs to be created
    */
    public static void sendAccountInfo(String strDomain, String strAccountId) {

        String strEndPoint = PreferenceCenterService.getPreferenceCenterOrgDefault().Preference_Center_Endpoint__c;
        System.debug('strEndPoint> ' +strEndPoint);

        //Fetch all the Clear Bit Roles
        List<Clearbit_Role_Settings__c> listClearBitRoles = Clearbit_Role_Settings__c.getAll().values();
        //Fetch all the DWH Roles
        List<DWH_Role_Settings__c> listDWHRoles = DWH_Role_Settings__c.getAll().values();

        //Fetch the Userd Id's with Clear Bit Licence
        Set<Id> setUserwithClearBitLicence = fetchUserIdsWithClearBitLicence();

        //Check if current user has Clear Bit licence
        String strhasClearbitLicense = (setUserwithClearBitLicence.contains(UserInfo.getUserId())) ? 'True' : 'False';

        String strBody = '{ "domain":"'+strDomain+'","account_id":"'+strAccountId+'","hasClearbitLicense":"'+strhasClearbitLicense+'","user_id":"'+UserInfo.getUserId()+'","fallbackid":"'+System.Label.Fallback_Clearbit_Owner_Id+'","clearbit_role_rankings":[';

        //Iterate over all the Clear Bit Roles
        for(Clearbit_Role_Settings__c objClearbit_Role_Settings : listClearBitRoles) {

            String strStatus = (objClearbit_Role_Settings.Active__c) ? 'Active' : 'InActive';

            strBody += '{"role":"'+objClearbit_Role_Settings.Role__c+'","ranking":"'+objClearbit_Role_Settings.Ranking__c+'","status":"'+strStatus+'"},';
        }

        strBody = strBody.removeEnd(',');
        strBody += '],"dwh_role_rankings":[';

        //Iterate over all the DWH Roles
        for(DWH_Role_Settings__c objDWH_Role_Settings : listDWHRoles) {

            String strStatus = (objDWH_Role_Settings.Active__c) ? 'Active' : 'InActive';

            strBody += '{"role":"'+objDWH_Role_Settings.Role__c+'","ranking":"'+objDWH_Role_Settings.Ranking__c+'","status":"'+strStatus+'"},';
        }

        strBody = strBody.removeEnd(',');

        strBody += ']}';

        System.debug('===strBody===='+strBody);

        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();

        request.setEndpoint(strEndPoint+'/contacts');
        request.setHeader('Content-Type','application/json');
        request.setMethod('POST');
        request.setBody(strBody);
        if(!Test.isRunningTest()){
            response = http.send(request);
            System.debug('===response===='+response.getBody());
        }
    }

    /**
    @Description Method that return the list of users with clearbit licence
    @author Sukesh
    @date August 20 2019
    @See AccountTriggerHandler.fetchUserIdsWithClearBitLicence
    @return   Set of User Id's with clearbit licence
    */
    private static Set<Id> fetchUserIdsWithClearBitLicence() {

        Set<Id> setUserwithClearBitLicence = new Set<Id>();

        List<Clearbit_Package_Namespace__mdt> lstClearBitPackageNamespace = [SELECT Id,
                                                                                    Namespace__c
                                                                               FROM Clearbit_Package_Namespace__mdt];

        String strClearBitNameSpace = (!lstClearBitPackageNamespace.isEmpty()) ?  lstClearBitPackageNamespace[0].Namespace__c : '';

        if(!String.isEmpty(strClearBitNameSpace)) {

            for(User objUser : [SELECT Id,
                                       Name
                                  FROM User
                                  WHERE Id IN (SELECT UserId
                                                 FROM UserPackageLicense
                                                WHERE (PackageLicense.NamespacePrefix =:strClearBitNameSpace))]) {

                setUserwithClearBitLicence.add(objUser.Id);

            }
        }

        return setUserwithClearBitLicence;
    }
}