/*
*   ClearGeoPointeTripFields class : InvocableMethod called by process builder flow to clear the geopointe trip message and flag fields
*                                       on Account and Account Owner (User) objects.
*
*   Author: Wilson Ng
*   Date:   May 04, 2018
*
*/
public without sharing class ClearGeoPointeTripFields {
    
    @InvocableMethod(label='Clear GeoPointe Trip' description='Clears the GeoPointe Trip Message and Boolean fields.')
    public static list<Id> ClearGeoPointeTripFields(list<Id> acctIds) {
        system.debug('acctIds: ' + acctIds);
        if(acctIds!=null && acctIds.size()>0) {
            // call future methods
            ClearGeoPointe_Account(acctIds);
            ClearGeoPointe_AccountOwner(acctIds);
        }
        return acctIds;
    }
    
    @future
    static void ClearGeoPointe_Account(list<Id> acctIds) {
        
        // sleep for 6 seconds
        wait(6000);
        
        // bypass account triggers
        AccountTriggerHandler.BYPASSTRIGGER = true;
        AccountTriggerHandlerWOS.BYPASSTRIGGER = true;
        
        list<Account> updList = new list<Account>();
        for(Id myid : acctIds)
            updList.add(new Account(Id=myId, Geopointe_Trip_Message__c=null, GeoPoint_Short_Message__c=null));
        update updList;
    }
    
    @future
    static void ClearGeoPointe_AccountOwner(list<Id> acctIds) {
        
        // sleep for 6 seconds
        wait(6000);
        
        map<Id, User> updMap = new map<Id, User>();
        for(Account acct : [select Id, OwnerId from Account where id = :acctIds])
            if(!updMap.containsKey(acct.OwnerId))
                updMap.put(acct.OwnerId, new User(Id=acct.OwnerId, Visit_Company_Email__c=false, Visit_Message_Sent__C=false));
        update updMap.values();
    }
    

    /**********************************************************
     *  Helper Method: wait
     *  @param Integer millisec : time for wait (millisecond)
     *********************************************************/
    public static void wait(Integer millisec) {
        
        if(millisec == null || millisec < 0) {
            millisec = 0;
        }
            
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < millisec) {
            //sleep for parameter x millisecs
            finishTime = DateTime.now().getTime();
        }
    }
}