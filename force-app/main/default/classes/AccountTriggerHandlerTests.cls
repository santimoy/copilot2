/**

**/
@isTest
private class AccountTriggerHandlerTests
{
	private static List<Account> accounts;

	private static User SetupUser(  )
	{

		Profile p = [ SELECT Id FROM Profile WHERE Name = 'Analyst Full' ];
		User u = new User( Alias = 'standt',
						   Email='standarduser@testorg.dev',
						   EmailEncodingKey = 'UTF-8',
						   LastName = 'Testing',
						   LanguageLocaleKey='en_US',
						   LocaleSidKey = 'en_US',
						   ProfileId = p.Id,
						   TimeZoneSidKey = 'America/Los_Angeles',
						   UserName = 'analystuser@testorg.dev' );


		return u;
	}

	private static void SetupData( Id userId, Integer accountsToCreate, Integer accountOwnerLimit )
	{
		Common_Config__c config = CommonConfigUtil.getConfig();
		System.debug(config);

		// can't use user-level custom settings, so let's use the profile-level
		config = Common_Config__c.getInstance(userinfo.getProfileId());
		config.Account_Owner_Limit__c = accountOwnerLimit;
		if(config.Id == null) insert config;
		else update config;
		
		// create dummy account data
		accounts = new List<Account>();
		for( Integer i = 0; i < accountsToCreate; i++ )
		{
            accounts.add( new Account( Name = 'testacct'+i,
                                      Website = 'http://www.www.dev'+i,
                                      Description = 'test company',
                                      BillingStreet = '1 Street',
                                      BillingCity = 'City',
                                      BillingState = 'New York',
                                      BillingPostalCode = '64033',
                                      BillingCountry = 'United States',
                                      Partial_Credit_User__c = userId,
                                      Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer',
                                      Ownership_Status__c = 'Priority',
                                      Top_10__c = TRUE,
                                      Top_10_Bucket__c = 'Do Not Miss',
                                      OwnerId = userId ) );
        }
		insert accounts;
		checkRecursive.resetAll();
	}

	/* original - TO BE DELETED
	private static User SetupUser( Integer accountOwnerLimit )
	{
		Common_Config__c config = CommonConfigUtil.getConfig();
		System.debug(config);

		Profile p = [ SELECT Id FROM Profile WHERE Name = 'Analyst Full' ];
		User u = new User( Alias = 'standt',
						   Email='standarduser@testorg.dev',
						   EmailEncodingKey = 'UTF-8',
						   LastName = 'Testing',
						   LanguageLocaleKey='en_US',
						   LocaleSidKey = 'en_US',
						   ProfileId = p.Id,
						   TimeZoneSidKey = 'America/Los_Angeles',
						   UserName = 'analystuser@testorg.dev' );

		// can't use user-level custom settings, so let's use the profile-level
		config = Common_Config__c.getInstance(p.Id);
		config.Account_Owner_Limit__c = accountOwnerLimit;
		if(config.Id == null) insert config;
		else update config;

		return u;
	}

	private static void SetupData( Id userId, Integer accountsToCreate )
	{
		Common_Config__c config = CommonConfigUtil.getConfig();
		System.debug(config);

		// create dummy account data
		accounts = new List<Account>();
		for( Integer i = 0; i < accountsToCreate; i++ )
		{
			accounts.add( new Account( Name = 'testacct'+i,
									   Website = 'http://www.www.dev'+i,
									   Description = 'test company',
									   BillingStreet = '1 Street',
									   BillingCity = 'City',
									   BillingState = 'New York',
									   BillingPostalCode = '64033',
									   BillingCountry = 'United States',
									   OwnerId = userId ) );
		}
		insert accounts;
		checkRecursive.resetAll();
	}

	*/

	private static void SetupPortfolioData()
	{
		Common_Config__c config = CommonConfigUtil.getConfig();
		System.debug(config);
	}

	/* 4-4-2018 TO BE DEPRECATED
	@isTest static void HardToCrack_Test()
	{
		User runAsUser = SetupUser(10);
		System.runAs( runAsUser )
		{
			SetupData( runAsUser.Id, 10 );

			accounts[0].Hard_To_Crack__c = true;
			Database.SaveResult[] srList = Database.update( accounts, false );
			checkRecursive.resetAll();

			System.assertEquals(true, srList[0].isSuccess());

			accounts[1].Hard_To_Crack__c = true;
			srList = Database.update( accounts, false );
			checkRecursive.resetAll();

			System.assertEquals(true, srList[0].isSuccess());
			System.assertEquals(false, srList[1].isSuccess());
		}
	}

	@isTest static void Do_Not_Miss_Test()
	{
		User runAsUser = SetupUser(10);
		System.runAs( runAsUser )
		{
			SetupData( runAsUser.Id, 10 );

			accounts[0].Priority__c = 'Do Not Miss';
			accounts[0].Do_Not_Miss_Rationale__c='Important';
			accounts[0].Website__c='www.insightpartners.com';
			Database.SaveResult[] srList = Database.update( accounts, false );
			checkRecursive.resetAll();

			System.assertEquals(true, srList[0].isSuccess());

			accounts[1].Priority__c = 'Do Not Miss';
			Database.SaveResult[] srList2  = Database.update( accounts, false );
			checkRecursive.resetAll();

			System.assertEquals(true, srList[0].isSuccess());
			System.assertEquals(false, srList2[0].isSuccess());
		}
	}

	@isTest static  void Primary_Portfolio_Test()
	{
		User runAsUser = SetupUser(1);
		System.runAs( runAsUser )
		{
			SetupPortfolioData();

			Account account = new Account( Name='testacct1',
										   Website = 'http://www.www.dev1',
										   Description = 'test company',
										   Hard_To_Crack__c = false,
										   BillingStreet = '1 Street',
										   BillingCity = 'City',
										   BillingState = 'New York',
										   BillingPostalCode = '64033',
										   BillingCountry = 'United States',
										   Website__c = 'www.insightpartners.com',
										   OwnerId = runAsUser.Id );

			Database.SaveResult sr = Database.insert( Account, false );
			checkRecursive.resetAll();

			System.assertEquals( true, sr.isSuccess() );

			Account account2 = new Account( Name='testacct2',
											Website = 'http://www.www.dev2',
											Description = 'test company',
											Hard_To_Crack__c = false,
											BillingStreet = '1 Street',
											BillingCity = 'City',
											BillingState = 'New York',
											BillingPostalCode = '64033',
											BillingCountry = 'United States',
											OwnerId = runAsUser.Id );

			Database.SaveResult sr2 = Database.insert( Account2, false );
			checkRecursive.resetAll();

			System.assertEquals( false, sr2.isSuccess() );
		}
	}
	*/

	@isTest static void OwnerLimit_Test()
	{
		User runAsUser = SetupUser();
		System.runAs( runAsUser )
		{
			SetupData( runAsUser.Id, 0, 1 );

			Account account = new Account( Name='testacct1',
										   Website = 'http://www.www.dev1',
										   Description = 'test company',
										   BillingStreet = '1 Street',
										   BillingCity = 'City',
										   BillingState = 'New York',
										   BillingPostalCode = '64033',
										   BillingCountry = 'United States',
                                          Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer',
										   OwnerId = runAsUser.Id );

			Database.SaveResult sr = Database.insert( Account, false );
			checkRecursive.resetAll();

			System.assertEquals( true, sr.isSuccess() );

			Account account2 = new Account( Name='testacct2',
											Website = 'http://www.www.dev2',
											Description = 'test company',
											BillingStreet = '1 Street',
											BillingCity = 'City',
											BillingState = 'New York',
											BillingPostalCode = '64033',
											BillingCountry = 'United States',
                                           Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer',
											OwnerId = runAsUser.Id );

			Database.SaveResult sr2 = Database.insert( Account2, false );
			checkRecursive.resetAll();

			System.assertEquals( true, sr2.isSuccess() );
		}
	}

	@isTest static void Delete_Test()
	{
		User runAsUser = SetupUser();
		System.runAs( runAsUser )
		{
			SetupData( runAsUser.Id, 10, 10 );

			Database.DeleteResult dr = Database.delete( accounts[0], false );
			System.assertEquals(true, dr.isSuccess());

			/* 4-4-2018 TO BE DEPRECATED
			accounts[1].Hard_To_Crack__c = true;
			update accounts[1];
			checkRecursive.resetAll();

			dr = Database.delete( accounts[1], false );
			System.assertEquals( false, dr.isSuccess() );
			*/
		}
	}

    /*
	@isTest static void testInsertDummyContacts()
	{
		List<Account> testAccounts = new List<Account>();
		for( Integer i = 0; i < 10; i++)
		{
			Account a = new Account();
			a.Name = 'TestAccount' + i;
			a.Date_of_Last_Call_Meeting__c = Date.today();

			testAccounts.add( a );
		}

		Test.startTest();

			insert testAccounts;

		Test.stopTest();

		List<Contact> contactResults = [ SELECT Id, LastName FROM Contact ];
		System.assertEquals( 10, contactResults.size(), 'Dummy Contacts should be created for all Accounts' );
		for( Contact c : contactResults )
		{
			System.assert( c.LastName.contains( 'TestAccount' ), 'Contact\'s Last Name should contain Account\'s Name' );
		}
	}
    */
	@isTest static void testUpdateWebsiteDomain_ONINSERT()
	{
		final String FULL_WEBSITE = 'https://www.test.com';
		final String WEBSITE_DOMAIN_NAME = 'test.com';

		List<Account> testAccounts = new List<Account>();
		for( Integer i = 0; i < 10; i++)
		{
			Account a = new Account();
			a.Name = 'TestAccount' + i;
			a.Website = FULL_WEBSITE + i;
			testAccounts.add( a );
		}

		Test.startTest();

			insert testAccounts;

		Test.stopTest();

		List<Account> accountResults = [ SELECT Id, Name, Website, Website_Domain__c FROM Account ];
		System.debug( 'accountResults : ' + accountResults );
		System.assertEquals( 10, accountResults.size(), 'All accounts should be returned' );
		Integer i = 0;
		for( Account a : accountResults )
		{
			System.debug( 'a.Website_Domain__c : ' + a.Website_Domain__c );
			System.assert( a.Website_Domain__c.equals( WEBSITE_DOMAIN_NAME + i++ ), 'Account\'s Website Domain field should be set to domain name of the Website field' );
		}
	}

	@isTest static void testUpdateWebsiteDomain_ONUPDATE()
	{
		final String FULL_WEBSITE = 'https://www.test.com';
		final String WEBSITE_DOMAIN_NAME = 'test.com';

		final String CHANGED_FULL_WEBSITE = 'https://www.changed.com';
		final String CHANGED_WEBSITE_DOMAIN_NAME = 'changed.com';

		List<Account> testAccounts = new List<Account>();
		for( Integer i = 0; i < 10; i++)
		{
			Account a = new Account();
			a.Name = 'TestAccount' + i;
			a.Website = FULL_WEBSITE + i;
			testAccounts.add( a );
		}
		insert testAccounts;

		Integer i = 0;
		for( Account a : testAccounts )
		{
			a.Name = 'Test Company';
			a.Website = CHANGED_FULL_WEBSITE + i++;
		}

		Test.startTest();

			update testAccounts;

		Test.stopTest();

		List<Account> accountResults = [ SELECT Id, Website, Website_Domain__c FROM Account ];
		System.assertEquals( 10, accountResults.size(), 'All accounts should be returned' );
		i = 0;
		for( Account a : accountResults )
		{
			System.assert( a.Website_Domain__c.equals( CHANGED_WEBSITE_DOMAIN_NAME + i++ ), 'Account\'s Website Domain field should be set to domain name of the Website field' );
		}
	}

/*	  Test.startTest();

		// test inserts
		Database.SaveResult[] srList = database.insert(alist, false);
		checkRecursive.resetAll();
		for(integer i=0; i<=cnt; i++)
			system.assertEquals((i<cnt?true:false), srList[i].isSuccess(), 'Error: Hard to Crack trigger did not limit max allow-able...iterator equals '+i);

		// test updates
		alist[0].Hard_To_Crack__c = false;
		update alist[0];
		checkRecursive.resetAll();
		alist[0].Hard_To_Crack__c = true;
		update alist[0];
		checkRecursive.resetAll();
		system.assertEquals(true, srList[0].isSuccess(), 'Error: Hard to Crack trigger did not allow hard to crack while still under the max limit.');

		// test deletes
		Database.DeleteResult dr = database.delete(alist[0], false);
		system.assertEquals(false, dr.isSuccess(), 'Error: Hard to Crack trigger did not restrict deletion of record when hard to crack indicator equals true.');

		Test.stopTest();
*/

    @isTest
    private static void callClearBitTest()
    {
        /*insert new Clearbit_Callout_Users__c(SetupOwnerId=UserInfo.getOrganizationId());
        List<Clearbit_Role_Settings__c> lstRoleSettings = new List<Clearbit_Role_Settings__c>();
        lstRoleSettings.add(new Clearbit_Role_Settings__c(Name = 'CEO', Active__c = TRUE, Ranking__c = 1, Role__c = 'CEO'));
        lstRoleSettings.add(new Clearbit_Role_Settings__c(Name = 'Founder', Active__c = TRUE, Ranking__c = 4, Role__c = 'Founder'));
        insert lstRoleSettings;*/

        List<DWH_Role_Settings__c> listDWHRoles = new List<DWH_Role_Settings__c>();
        listDWHRoles.add(new DWH_Role_Settings__c(Name = 'CEO', Active__c = TRUE, Ranking__c = 1, Role__c = 'CEO'));
        listDWHRoles.add(new DWH_Role_Settings__c(Name = 'Founder', Active__c = TRUE, Ranking__c = 4, Role__c = 'Founder'));
        insert listDWHRoles;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ClearBitCalloutMock());
        Account objAcc = new Account( Name='testacct1',
											Website = 'http://www.www.dev1',
											Description = 'test company',
											BillingStreet = '1 Street',
											BillingCity = 'City',
											BillingState = 'New York',
											BillingPostalCode = '64033',
											BillingCountry = 'United States',
											Discount_Email__c = 'testq@t.com',
                                     Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer'
											);

        insert objAcc;
        Test.stopTest();

    }
     private static testmethod void updateHivebriteLongDescription(){
        List<Account> accountList = new List<Account>();
        Id objectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Portfolio Company').getRecordTypeId();
        Account Acc =  new Account( Name = 'testacct'+Datetime.now(),
                                   Website = 'http://www.www.dev',
                                   recordTypeId = objectRecordTypeId,
                                   Description = 'test company',
                                   Website_Content__c = 'Test',
                                   Website_Regions__c = 'Asia Pacific',
                                   Website_Sectors__c = 'B2C',
                                   Website_Verticals__c = 'Gaming',
                                   Portfolio_Company_Customers__c = 'test',
                                   BillingStreet = '1 Street',
                                   BillingCity = 'City',
                                   BillingState = 'New York',
                                   BillingPostalCode = '64033',
                                   BillingCountry = 'United States',
                                   Top_10__c=true,
                                   Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer',
                                   Top_10_Bucket__c = 'Do Not Miss');
        accountList.add(Acc);
        Id objectRecordTypeId1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        Account AccVendor =  new Account( Name = 'test Vendor'+Datetime.now(),
                                         Website = 'http://www.www.dev',
                                         recordTypeId = objectRecordTypeId1,
                                         Hivebrite_Partner_Long_Description__c = 'test',
                                         Portfolio_Company_Customers__c = 'test',
                                         Description = 'test company',
                                         BillingStreet = '1 Street',
                                         BillingCity = 'City',
                                         BillingState = 'New York',
                                         BillingPostalCode = '64033',
                                         BillingCountry = 'United States',
                                         Top_10__c=true,
                                         Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer',
                                         Top_10_Bucket__c = 'Do Not Miss');
        accountList.add(AccVendor);
        Test.startTest();
        INSERT accountList;
        Test.stopTest();
        
    }
    /**
    * @description 
    * @author Anup Kage | 8/11/2019 
    * @return testmethod 
    **/
    private static testmethod void changeRecordOwner(){
       // User runAsUser = SetupUser();
        Profile p = [ SELECT Id FROM Profile WHERE Name = 'Analyst Full' ];
		User u = new User( Alias = 'staq',
						   Email='standarduser'+Datetime.now().getTime()+'@testorg.dev',
						   EmailEncodingKey = 'UTF-8',
						   LastName = 'Unassigned',
						   LanguageLocaleKey='en_US',
						   LocaleSidKey = 'en_US',
						   ProfileId = p.Id,
						   TimeZoneSidKey = 'America/Los_Angeles',
						   UserName = 'anal'+Datetime.now().getTime()+'@testorg.dev' );


		Insert u;
        test.startTest();
		System.runAs( u )
		{
			SetupData( u.Id, 1, 1 );
            Account Obj = [SELECT Id, Ownership_Status__c FROM Account Limit 1];
            Obj.Ownership_Status__c = 'Watch';
            // Obj.OwnerId = runAsUser.Id;
            UPDATE Obj;
        }
        test.stopTest();
    }
	/**
	* @description 
	* @author Anup Kage | 8/11/2019 
	* @return testmethod 
	**/
	private static testmethod void changeRecordOwnershipStatus(){
        User runAsUser = SetupUser();
		// IVP_General_Config__c config = new  IVP_General_Config__c(); //getValues('n-top10-company');
		// config.Value__c = '1';
		// config.name = 'n-top10-company';
		// Upsert config;

        test.startTest();
		System.runAs( runAsUser )
		{
            //TaskCreationController.UNASSIGN_MANUAL = true;
			SetupData( runAsUser.Id, 1, 1 );
            Account Obj = [SELECT Id, Ownership_Status__c FROM Account Limit 1];
            Obj.Ownership_Status__c = 'Watch';
            UPDATE Obj;
        }
        test.stopTest();
    }
	/**
	* @description 
	* @author Anup Kage | 8/11/2019 
	* @return testmethod 
	**/
	private static testmethod void changeRecordOwnershipStatus1(){
        User runAsUser = SetupUser();
		// SetupData( runAsUser.Id, 1, 1 );
        test.startTest();
		System.runAs( runAsUser )
		{  	Common_Config__c config = CommonConfigUtil.getConfig();
			config = Common_Config__c.getInstance(userinfo.getProfileId());
			config.Account_Owner_Limit__c = 1;
			if(config.Id == null) insert config;
			else update config;
			
			Account obj = new Account( Name = 'testacct'+Datetime.now(),
                                      Website = 'http://www.www.dev',
                                      Description = 'test company',
                                      BillingStreet = '1 Street',
                                      BillingCity = 'City',
                                      BillingState = 'New York',
                                      BillingPostalCode = '64033',
                                      BillingCountry = 'United States',
                                      Partial_Credit_User__c = runAsUser.Id,
                                      Loss_Drop_Notes__c='test',
                                      Loss_Drop_Reason__c = 'Transfer',
                                      Ownership_Status__c = 'Watch',
                                      Top_10__c = TRUE,
                                      Top_10_Bucket__c = 'Do Not Miss',
                                      OwnerId = runAsUser.Id );
			INSERT obj;	
			checkRecursive.resetAll();					  
            Obj.Ownership_Status__c = 'Priority';
            UPDATE Obj;
        }
        test.stopTest();
    }
    
    
     /**
	* @description 
	* @author Vijay | 01/09/2020 
	* @return testmethod 
	**/
    private static testmethod void testcreateIntelligenceROI(){
        
        List<Account> accList = new List<Account>{new Account(Name = 'testROI')}; 
            Insert accList;
        
        Test.startTest();
        for(Account acc : accList){
            acc.ROI_App_Name__c = 'testROI';
            acc.ROI_Action__c = 'testROIAction';
        }
        Update accList;
        Test.stopTest();
        
        System.assertEquals('testROI', [SELECT Id,app_name__c,Company__c FROM Intelligence_ROI__c Where Company__c =: accList[0].Id].app_name__c);   
    }
    
    /**
	* @description 
	* @author Vijay | 12/27/2019 
	* @return testmethod 
	**/
	private static testmethod void testAccountRegion(){
        
        Id IgcompId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Ignite Company').getRecordTypeId();

        List<Account> accList = new List<Account>{new Account(Name='Test Today',Website_Status__c='changed',Ownership_Status__c='Watch',BillingCountry = 'Canada',RecordTypeId = IgcompId),
            new Account(Name='Test Today',Website_Status__c='changed',Ownership_Status__c='Watch',BillingCountry = 'United Kingdom',RecordTypeId = IgcompId),
            new Account(Name='Test Today',Website_Status__c='changed',Ownership_Status__c='Watch',BillingCountry = 'Albania',RecordTypeId = IgcompId),
            new Account(Name='Test Today',Website_Status__c='changed',Ownership_Status__c='Watch',BillingCountry = 'Bangladesh',RecordTypeId = IgcompId),
            new Account(Name='Test Today',Website_Status__c='changed',Ownership_Status__c='Watch',BillingCountry = 'Cuba',RecordTypeId = IgcompId)}; 
        	Insert accList;
        
        System.assertEquals('Americas', [SELECT Id,Ignite_Region__c FROM Account Where BillingCountry = 'Cuba'].Ignite_Region__c);
        System.assertEquals('APAC', [SELECT Id,Ignite_Region__c FROM Account Where BillingCountry = 'Bangladesh'].Ignite_Region__c);
        System.assertEquals('EMEA', [SELECT Id,Ignite_Region__c FROM Account Where BillingCountry = 'Albania'].Ignite_Region__c);
        System.assertEquals('UK', [SELECT Id,Ignite_Region__c FROM Account Where BillingCountry = 'United Kingdom'].Ignite_Region__c);
        System.assertEquals('NA', [SELECT Id,Ignite_Region__c FROM Account Where BillingCountry = 'Canada'].Ignite_Region__c);
        
    }
/**
	* @description 
	* @author Vijay | 01/06/2020 
	* @return testmethod 
	**/
	private static testmethod void testAccountOwnershipPriority(){
		
		List<Account> accList = new List<Account>{new Account(Name='Test top10',Top_10__c= true),
			new Account(Name='Test priorty',Ownership_Status__c='Priority'),
			new Account(Name='Test watch',Top_10__c= false,Ownership_Status__c='Watch'),
			new Account(Name='Test',Ownership_Status__c='') }; 
			Insert accList;
		
		System.assertEquals('Top 10', [SELECT Id,Ownership_Priority__c FROM Account Where Name =  'Test top10'].Ownership_Priority__c );
		System.assertEquals('Priority', [SELECT Id,Ownership_Priority__c FROM Account Where Name =  'Test priorty'].Ownership_Priority__c);
		System.assertEquals('Watch', [SELECT Id,Ownership_Priority__c FROM Account Where Name =  'Test watch'].Ownership_Priority__c);
		System.assertEquals(null, [SELECT Id,Ownership_Priority__c FROM Account Where Name =  'Test'].Ownership_Priority__c);
		
	}
}