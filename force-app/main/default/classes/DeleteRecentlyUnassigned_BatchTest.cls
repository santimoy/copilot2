/*
*	DeleteRecentlyUnassigned_BatchTest class : Test class coverage for deleting recently unassigned records
*
*	Author: Wilson Ng
*	Date:   Apr 26, 2018
*
*/
@isTest
private class DeleteRecentlyUnassigned_BatchTest {
    
    @isTest static void testDeletes()
	{
		list<Recently_Unassigned__c> dataList = new list<Recently_Unassigned__c>();
		for(Integer i=0; i<100; i++)
			dataList.add(new Recently_Unassigned__c());
		insert dataList;
		
		Test.startTest();
		
		DeleteRecentlyUnassigned_Batch testClass = new DeleteRecentlyUnassigned_Batch();
		SchedulableContext ctx;
		testClass.execute(ctx);
		
		Test.stopTest();
	}
}