/**
 * @author Vineet Bhagchandani
 * @name PickListDescControllerTest
 * @description Test Class for PickListDesc
**/
@isTest
public class PickListDescControllerTest 
{
    
    public static testMethod void DescribeMethod1()
    {
        
        List<RecordType> rts = [SELECT Id, Name, DeveloperName FROM RecordType 
                                WHERE SobjectType='ACCOUNT' AND DeveloperName='Prospect' LIMIT 1];
        
        final String FULL_WEBSITE = 'https://www.test.com';
        Account acc = new Account(RecordTypeId = rts[0].Id);
        acc.Name = 'TestAccount';
        acc.Ownership_Status__c='Watch';
        acc.Website = FULL_WEBSITE;
        acc.OwnerId=UserInfo.getUserId();
        insert acc;
        
        test.startTest();
        PicklistDescriber.describe(acc.id,'Loss_Drop_Reason__c');
        PicklistDescriber.describe('Account','Prospect','Loss_Drop_Reason__c');
        test.stopTest();
    }
    public static testMethod void DescribeMethod2()
    {
        Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        final String FULL_WEBSITE = 'https://www.test.com';
        Account acc = new Account(RecordTypeId = RecordTypeId);
        acc.Name = 'TestAccount';
        acc.Ownership_Status__c='Watch';
        acc.Website = FULL_WEBSITE;
        acc.OwnerId=UserInfo.getUserId();
        insert acc;
        
        test.startTest();
        pageReference ref=page.picklistDesc;
        test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        //    ApexPages.currentPage().getParameters().put('recordTypeId',acc.RecordTypeId);
        ApexPages.currentPage().getParameters().put('recordTypeName','Prospect');
        ApexPages.currentPage().getParameters().put('sobjectType','Account');
        ApexPages.currentPage().getParameters().put('picklistFieldName','Loss_Drop_Reason__c');
        PickListDescController obj=new PickListDescController();
        obj.sobj=acc ;
        obj.pickListFieldName='Loss_Drop_Reason__c';
        PickListDescController.isBlank('test ');
        test.stopTest();
    }
    public static testMethod void DescribeMethod3()
    {
        Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        
        final String FULL_WEBSITE = 'https://www.test.com';
        Account acc = new Account(RecordTypeId = RecordTypeId);
        acc.Name = 'TestAccount';
        acc.Ownership_Status__c='Watch';
        acc.Website = FULL_WEBSITE;
        acc.OwnerId=UserInfo.getUserId();
        insert acc;
        
        test.startTest();
        pageReference ref=page.picklistDesc;
        test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('id',acc.Id);
        ApexPages.currentPage().getParameters().put('recordTypeId',RecordTypeId);
        ApexPages.currentPage().getParameters().put('recordTypeName','Prospect');
        ApexPages.currentPage().getParameters().put('picklistFieldName','Loss_Drop_Reason__c');
        PickListDescController obj=new PickListDescController();
        obj.sobj=acc ;
        obj.pickListFieldName='Loss_Drop_Reason__c';
        PickListDescController.isBlank('test ');
        test.stopTest();
    }
    
}