/**
 * @author  : 10k-Advisor
 * @name    : SalesLoftSyncQ 
 * @date    : 2018-08-09
 * @description   : The class used to do callout(s) for specified object and sync with salesforce
 *               1. Default Queueable will go to sync cadencemember only
 *               2. Queueable also go for any object (people, cadence, cadencemember)  by using Single argument constructor
 *               3. If needed so we can also execute the Queueable for sepecific object with sepecific page number
 *                   Note: usually this is called via Queueable itself to process upto end of the record of that salesloft object
**/
public class SalesLoftSyncQ implements Queueable, Database.AllowsCallouts{
    public static String startSyncDateTime;
    String startDateTime;
    String objName; //we will use to identify object
    String objNameEndPoint; //we will use for Calls
    Integer dataPageNumber; //page number
    Boolean checkSyncingRunning;
    
    public SalesLoftSyncQ(){
        // default we will sync only CadenceMember
        objName = 'cadencemember';
        objNameEndPoint = Salesloft.CADENCE_MEMBER_END_POINT;
        // checkSyncingRunning = true;
        dataPageNumber = 1;
    }
    
    public SalesLoftSyncQ(String sfObjName){
        objName = sfObjName;
        // checkSyncingRunning = false;
        objNameEndPoint = Salesloft.getEndPoint(sfObjName);
        dataPageNumber = 1;
    }
    
    public SalesLoftSyncQ(String sfObjName, String objNameEndPoint, Integer pageNumber){
        objName = sfObjName;
        dataPageNumber = pageNumber;
        // checkSyncingRunning = false;
        this.objNameEndPoint = objNameEndPoint;
    }
    
    public SalesLoftSyncQ(String sfObjName, String objNameEndPoint, Integer pageNumber, String startSyncDateTime){
        objName = sfObjName;
        dataPageNumber = pageNumber;
        // checkSyncingRunning = false;
        this.objNameEndPoint = objNameEndPoint;
        this.startDateTime = startSyncDateTime;
    }
    
    public void execute(QueueableContext context){
        if(dataPageNumber==1 && SalesloftUtility.checkSyncingRunning()){
            // Syncing is running so we are allowing to execute another job
            return ;
        }
        if(SalesLoftSyncQ.startSyncDateTime == NULL && startDateTime != NULL){
            //maintaing syncing Start datetime which we got from previous syncing process
            SalesLoftSyncQ.startSyncDateTime = startDateTime;
        }
        
        // Boolean prepareStartSync = false;
        // Boolean checkLastSync = objName.toLowerCase() == 'cadencemember';
        if(SalesLoftSyncQ.startSyncDateTime == null){
            //preparing syncing Start datetime on the first attempt
            SalesLoftSyncQ.startSyncDateTime = System.now().formatGmt('YYYY-MM-dd--HH:mm:ss.000000+00:00');
            // formating the datetime that we can use in the Call directly
            SalesLoftSyncQ.startSyncDateTime = SalesLoftSyncQ.startSyncDateTime.replace('--', 'T');
            // prepareStartSync = checkLastSync;
        }
        
        String service = Salesloft.objectWithServiceName.get(objName);
        if(service != NULL){
            Map<String, String> params = new Map<String, String>{'include_paging_counts' =>'true','per_page'=>'100', 'page'=>String.valueOf(dataPageNumber)};
            String lastSyncDate = SalesloftUtility.extractLastSyncDate();
            
            if(lastSyncDate!=''){
                params.put('updated_at[gt]', lastSyncDate);
            }
            if(Test.isRunningTest()){
                Test.setMock(HttpCalloutMock.class, new SalesloftMock());
            }
            
            HttpResponse response = SalesLoft.getAPIResponse(objNameEndPoint, 'GET', params);
            if(response!=NULL){
                SalesloftUtility.prepareStartSyncDate();
                SalesLoftProcessDomain sfDomain = (SalesLoftProcessDomain) Type.forName(service).newInstance();
                sfDomain.processHTTPResponse(response);
            }
            
        }
    }
    
}