global class EmailFinder
{
	public static final string EMAILCHECKER = 'email-checker.com';
	public List<Email_Finder_Settings__c> emailServices { get; set; }

	global class EmailJson
	{
		@AuraEnabled
		public String status { get; set; }

		@AuraEnabled
		public String additionalStatus { get; set; }
	
		@AuraEnabled
		public String emailAddressChecked { get; set; }

		@AuraEnabled
		public String emailAddressSuggestion { get; set; }

		@AuraEnabled
		public String emailAddressProvided { get; set; }

		@AuraEnabled
		public String Message { get; set; }
	}

	global class EmailDefinition
	{
		@AuraEnabled
		public String emailAddress { get; set; }

		@AuraEnabled
		public String emailTemplate { get; set; }

		@AuraEnabled
		public Boolean Selected { get; set; }

		@AuraEnabled
		public EmailJson CheckerResults { get; set; }
	}

	@AuraEnabled
	public List<EmailDefinition> emailSelection { get; set; }
	public String apiKey { get; set; }
	public String apiName { get; set; }
	public String endPoint { get; set; }
	public Contact keyContact { get; set; }
	public String searchResult { get; set; }
	public String domain { get; set; }
	public String firstName { get; set; }
	public String lastName { get; set; }

	@AuraEnabled
	public List<EmailDefinition> finalList { get; set; }
	public List<EmailDefinition> currentBatch { get; set; }

	public EmailFinder( String domain, String last, String first )
	{
		this.domain = 'NODOMAIN.COM';

		if( String.isNotBlank( domain ) ) this.domain = domain;
		this.lastName = last.deleteWhitespace();
		this.firstName = first.deleteWhitespace();
		this.apiKey = '';
		this.endPoint = '';
		this.apiName = '';
		
		getWebServiceUrl();
	}

	public void getWebServiceUrl()
	{
		// in the query below, the order by should make the Production url first and the Sandbox url last
		emailServices = [ SELECT Id, Name, API_Endpoint__c, API_Key__c, Is_Active__c, Request_Timeout__c, Overall_Timeout__c
						  FROM Email_Finder_Settings__c 
						  WHERE Is_Active__c = true ];
	}

	public List<String> getAllCombinations()
	{
		List<String> emails = new List<String>();
		String sfirst = firstName;
		String slast = lastName;
		if( String.isBlank( firstName ) )
		{
			List<String> splitNames = slast.split(' ');
			if( splitNames.size() > 1 )
			{
				sfirst = splitNames.get(0);
				slast = splitNames.get(1);
			}
		}
		else
		{
			List<String> splitFirst = sfirst.split(' ');
			if( splitFirst.size() > 1 )
				sfirst = splitFirst.get(0);
		}

		String fi = sfirst.substring( 0, 1 );
		String mi = '';
		String li = slast.substring( 0, 1 );

		for( Email_Format_Template__c tmpl : [ SELECT Name,Enabled__c FROM Email_Format_Template__c WHERE Enabled__c = true ] )
		{
			emails.add( tmpl.Name.replace( 'FInit', fi ).replace( 'LInit', li ).replace( 'Last', slast ).replace( 'First', sfirst ).replace( 'webdomain.com', Domain ) );
		}

		return emails;
	}

	public List<EmailDefinition> getEmails( String emailGroup )
	{
		emailSelection = new List<EmailDefinition>();
		Boolean firstOnly = emailGroup == 'first';
		Boolean second = emailGroup == 'second';

		Set<String> emailsToCheck = new Set<String>();
		if( String.isNotBlank( domain ) )
		{
			String sfirst = firstName;
			String slast = lastName;
			if( String.isBlank( firstName ) )
			{
				List<String> splitNames = slast.split(' ');
				if( splitNames.size() > 1 )
				{
					sfirst = splitNames.get(0);
					slast = splitNames.get(1);
				}
				else
					return emailSelection;
			}
			else
			{
				List<String> splitFirst = sfirst.split(' ');
				if( splitFirst.size() > 1 )
					sfirst = splitFirst.get(0);
			}

			String fi = sfirst.substring( 0, 1 );
			String mi = '';
			String li = slast.substring( 0, 1 );

			//[[sfirst,fi],[slast,li]];
			List<List<String>> names = new List<List<String>>();
			names.add( new List<String>{ sfirst, fi } );
			names.add( new List<String>{ slast, li } );
			String[] separators = new String[]{ '.', '', '_' };
			
			Integer counter = 0;
			EmailDefinition defn;
			if( firstOnly )
			{
				defn = new EmailDefinition();
				defn.emailAddress = sfirst + '.' + slast + '@' + domain;
				defn.emailTemplate = 'First.Last@webdomain.com';
				defn.Selected = true;
				emailSelection.add( defn );
			}
			else
			{
				for( Email_Format_Template__c tmpl : [ SELECT Name, Enabled__c FROM Email_Format_Template__c ] )
				{
					if( !tmpl.Enabled__c ) continue;
					defn = new EmailDefinition();
					defn.emailTemplate = tmpl.Name;
					defn.emailAddress = tmpl.Name.replace( 'FInit', fi ).replace( 'LInit', li ).replace( 'Last', slast ).replace( 'First', sfirst ).replace( 'webdomain.com', domain );
					defn.Selected = tmpl.Enabled__c;

					if( defn.emailAddress != sfirst + '.' + slast + '@' + domain )
						emailSelection.add( defn );
				}
			}
		}

		return emailSelection;
	}

	public void findEmailInit()
	{
		currentBatch = new List<EmailDefinition>();
		finalList = new List<EmailDefinition>();
	}

	public Boolean tryDummyEmail()
	{
		for( Email_Finder_Settings__c setting : emailServices )
		{
			if( setting.Name == EMAILCHECKER )
			{
				String ep = setting.API_Endpoint__c.replace( '{!APIKEY}', setting.API_Key__c ).replace( '{!EMAIL}', 'djdskjdsdu@' + this.domain );
				Integer reqTimeout = Integer.valueOf(setting.Request_Timeout__c);
				HttpRequest req = new HttpRequest();
				Http http = new Http();
				req.setMethod( 'GET' );
				//The timeout can be any value between 1 and 120,000 milliseconds.
				req.setTimeout( reqTimeout );
				req.setEndpoint( ep );
				system.debug('FIND dummy ENDPOINT:'+ep);
				HTTPResponse resp;
				EmailJson res;
				try
				{
					resp = http.send( req );
					String json = resp.getBody().replace( '\n', '' );
					res= (EmailJson)System.JSON.deserialize( json, EmailJson.class );
					system.debug(json);

					return (res.status=='Ok');
				}
				catch( System.CalloutException ex )
				{
					system.debug('Exception: tryDummyEmail:'+ex.getMessage()); 
					return false;
				}
			}
		}

		return false;
	}
	
	public void findEmail()
	{
		if( String.isBlank( firstName ) )
		{
			List<String> names = lastName.split(' ');
			if( names.size() > 1 )
			{
				firstName = names.get(0);
				lastName = names.get(1);
			}
			else
			{
				searchResult = 'Contact name is invalid. Please check first and last name';
				return;
			}
		}

		Long startTimer = DateTime.now().getTime();
		Long counter = 0;
		List<Matching_Email_Template__c> matches = new List<Matching_Email_Template__c>();
		for( EmailDefinition defn : currentBatch )
		{
			System.debug( 'Selected Email:' + defn.emailAddress );
			if( defn.Selected )
			{
				for( Email_Finder_Settings__c setting : emailServices )
				{
					if( setting.Name == EMAILCHECKER )
					{
						Integer overall = Integer.valueOf( setting.Overall_Timeout__c );
						Long checkTimer = DateTime.now().getTime();
						Long milliseconds = checkTimer - startTimer;
						EmailJson res;
						if( milliseconds > overall )
						{
							res = new EmailJson();
							res.status = 'Timeout';
							res.Message = 'Exceeded the overall timeout';
							defn.CheckerResults = res;
							return;
						}
						String ep = setting.API_Endpoint__c.replace( '{!APIKEY}', setting.API_Key__c ).replace( '{!EMAIL}', defn.emailAddress );

						HttpRequest req = new HttpRequest();
						Http http = new Http();
						req.setMethod( 'GET' );
						req.setTimeout( Integer.valueOf( setting.Request_Timeout__c ) );
						req.setEndpoint( ep );
						system.debug( 'FIND ENDPOINT:' + ep );
						HTTPResponse resp;
						try
						{
							resp = http.send( req );
							String json = resp.getBody().replace( '\n', '' );
							System.debug( 'json : ' + json );
							res= (EmailJson)System.JSON.deserialize( json, EmailJson.class );
							system.debug(json);
						}
						catch( System.CalloutException ex )
						{
							res = new EmailJson();
							res.status = 'Timeout';
							res.Message = ex.getMessage();
						}

						defn.CheckerResults=res;
					}
						
				}
			}
			else
			{
				EmailJson j = new EmailJson();
				j.status = 'NOT CHECKED';
				defn.CheckerResults = j;
			}
		}
		finalList.addAll( currentBatch );
	}
}