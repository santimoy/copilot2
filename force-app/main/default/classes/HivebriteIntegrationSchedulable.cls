/**
* The HivebriteIntegrationSchedulable Class will be used to Schedule  a job  at specified time.
* The code implements an Integration with Hivebrite based on the Jobtype we specified in the constructor.
* In execute method we will call the queueable class and will pass the jabType as paramter. In some Case we are aslo list of records. 
* @author  Anup Kage 
* @version 1.0
* @since  2019/August/12
*/

global  class HivebriteIntegrationSchedulable implements Schedulable {
    
    private String jobType;
    private List<SObject> recordList;
    // Using this will create  event log records based user log type.
    private String LOG_LEVEL = HivebriteIntegrationUtils.lOG_LEVEL; 

    /**
   * parmeterized constructor to receive JobType Name
   * @param jobtype This is to specify which job we want to run.
   */
    public HivebriteIntegrationSchedulable(String jobtype){
        this.jobtype = jobtype;
    }
    /**
   * parmeterized constructor to receive JobType Name and List of records
   * Used When we want to call scheduable class from Trigger.
   * @param jobtype This is to specify which job we want to run.
   */
    public HivebriteIntegrationSchedulable(String jobtype, List<SObject> recordList){
        this.recordList = recordList;
        this.jobtype = jobtype;
    }
    
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @param SC 
    * @return void 
    **/
    global void execute(SchedulableContext SC) {
        // try{
            Integration_API_Settings__c userSetting = Integration_API_Settings__c.getOrgDefaults();
            if(!userSetting.Integration_Is_Enabled__c){
                if(userSetting.Event_Log_Level__c	!= null && (userSetting.Event_Log_Level__c.equalsIgnoreCase('INFO') || userSetting.Event_Log_Level__c.equalsIgnoreCase('debug'))){
                    String ErrorMessage =' Schedulable '+ jobType +'<BR/>';
                    if(recordList != null && !recordList.isEmpty()){
                        Map<Id, SObject> objectById = new Map<Id, SObject>(recordList);
                        ErrorMessage += ' The following Records were not synced to Hivebrite: '+ String.join(new List<Id>(objectById.keySet()), ' ,');
                    }
                    EventLog.generateLog('HivebriteIntegrationSchedulable', jobType , 'INFO', 'Integration is disabled',ErrorMessage);
                    EventLog.commitLogs();
                }
                return;
            }
            if(LOG_LEVEL.containsIgnoreCase('Debug')){
                String jobId = '';
                if(SC != null){
                    jobId = String.valueOf(SC.getTriggerId());
                } else {
                    jobId = 'execute_anonymous_apex';
                }
                String description = ' Start of HivebriteIntegrationSchedulable : \n Id :' +jobId ;
                EventLog.limitLogger('HivebriteIntegrationSchedulable', 'execute', description);
            }
            // Calling Queueable class to to callout and to avoid Limitations
            if(jobtype == 'CONTACT_HB_TO_SF'){
                // strating new job to get updated user data.
                enqueueJob(jobtype);

            }else if(jobtype == 'PORTFOLIO_SF_TO_HB'){
                enqueueJob(jobType, recordList);
                // added abort because of its calling from HivebriteIntegrationUtils. 
                // we want it to run only once.(its getting called when Contact trigger is updating Account.)
                
                // System.debug('>>>>>>' +SC.getTriggerId());
                if(SC != null){
                    System.abortJob(SC.getTriggerId());
                }
             // 	  
            }
            else if(jobtype == 'PARTNER_SF_TO_HB'){
                enqueueJob(jobType, recordList);
                // added abort because of its calling from HivebriteIntegrationUtils. 
                // we want it to run only once.(its getting called when Contact trigger is updating Account.)
                if(SC != null){
                    System.abortJob(SC.getTriggerId());
                }
                 
            }else if(jobType == 'CONTACT_SF_TO_HB'){
                enqueueJob(jobType, recordList);
                if(SC != null){
                    System.abortJob(SC.getTriggerId());
                }
            }else if (jobType == 'EXPERIENCE_SF_TO_HB'){            
                enqueueJob(jobType, recordList);
                if(SC != null){
                    System.abortJob(SC.getTriggerId());
                }
            }else if(jobType == 'DELETECONTACT_SF_TO_HB'){
                 enqueueJob(jobType, recordList);
                 if(SC != null){
                    System.abortJob(SC.getTriggerId());
                }
            }
        // }catch(Exception e){
        //     EventLog.generateLog('HivebriteIntegrationSchedulable', jobType, 'error' , e);
        // }
        if(LOG_LEVEL.containsIgnoreCase('Debug')){
                String jobId = '';
                if(SC != null){
                    jobId = String.valueOf(SC.getTriggerId());
                } else {
                    jobId = 'execute_anonymous_apex';
                }
                String description = 'END of HivebriteIntegrationSchedulable : \n Id :' +jobId;
                EventLog.limitLogger('HivebriteIntegrationSchedulable', 'execute', description);
            }
        EventLog.commitLogs();
    }

    /**
    * Call Queuable class with JobType.
    */
    private void enqueueJob(String jobType){
        HivebriteIntegrationQueueable nextJob = new HivebriteIntegrationQueueable(jobType);
        ID jobID;
        if(!Test.isRunningTest()){
            jobID = System.enqueueJob(nextJob);
        }
        if(lOG_LEVEL.containsIgnoreCase('debug')){
            String description = 'Strated New Job : '+jobType+'  '+System.now()+': JobId '+jobID;
            EventLog.generateLog('HivebriteIntegrationSchedulable', jobType, lOG_LEVEL, description);
        }
    }
    /**
    * Call Queuable class with JobType.
    */
    private void enqueueJob(String jobType, List<SObject> recordList){
         if(recordList != null && recordList.size() > 0 ){
            HivebriteIntegrationQueueable objQueue = new HivebriteIntegrationQueueable(jobType, recordList);
             ID jobID;
            if(!Test.isRunningTest()){
                jobID = System.enqueueJob(objQueue);
            }
            if(lOG_LEVEL.containsIgnoreCase('debug')){
                String description = 'Strated New Job : '+jobType+' '+System.now()+': JobId '+jobID;
                EventLog.generateLog('HivebriteIntegrationSchedulable', jobType, lOG_LEVEL, description);
            }
        }
    }
}