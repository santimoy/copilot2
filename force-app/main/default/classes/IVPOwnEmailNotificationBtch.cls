/**
 * @author 10k-Expert
 * @name IVPOwnEmailNotificationBtch
 * @description Sending Emails to the user(Company owner) indicated by Request To of Company_Request
 *              24 hours Ago before the request has been expired
 *              01 hours Ago before the request has been expired
**/
public class IVPOwnEmailNotificationBtch implements Database.Batchable<sObject>, Schedulable, Database.Stateful{
    
    public void execute(SchedulableContext sc){
        Logger.push('Schedulable.execute','IVPOwnEmailNotificationBtch');
        Database.executeBatch(new IVPOwnEmailNotificationBtch(), 10);
        Logger.pop();
    }
    
    /***********************************************************************
     * @author 10K-Expert
     * @description prepare filters and return Database.QueryLocator with respect to Company_Request__c records
     * @return Database.QueryLocator
     * @param nothing
     ***********************************************************************/
    public Database.QueryLocator start(Database.BatchableContext BC){
        Integer secondEmailNotifyHourGap = IVPOwnershipService.secondEmailNotifyHourGap;
        Integer finalEmailNotifyHourGap = IVPOwnershipService.finalEmailNotifyHourGap;
        
        List<Integer> hours = new List<Integer>();
        if(secondEmailNotifyHourGap!=NULL && secondEmailNotifyHourGap > 0 ){
            hours.add(secondEmailNotifyHourGap);
        }
        if(finalEmailNotifyHourGap!=NULL && finalEmailNotifyHourGap > 0 ){
            hours.add(finalEmailNotifyHourGap);
        }
        String queryStr = 'SELECT Id,Name, OwnerId, CreatedDate, request_by__r.Email, Request_Expire_On__c,Request_By__c,Request_To__c '
                            +'FROM Company_Request__c WHERE Status__c=\'Pending\' AND Request_Processing_Hours__c in : hours';
        
        queryStr += ' ORDER BY Request_Expire_On__c DESC';
        return Database.getQueryLocator(queryStr);
    }
    
    /***********************************************************************
     * @author 10K-Expert
     * @description basic chunk executer of the job,
     *      on basis of property, process requests to extract request records to send email, and sending email for selected request
     * @return void
     * @param Database.BatchableContext, List of Company_Request__c for job chunk
     ***********************************************************************/
     public void execute(Database.BatchableContext BC, List<Company_Request__c> batchList){
        Logger.push('Batchable.execute','IVPOwnEmailNotificationBtch');
        sendEmails(batchList); 
        Logger.pop();
    }
    
    /***********************************************************************
     * @author 10K-Expert
     * @description sending email by using template whose name is defined in the label IVP_Second_Company_Request_Email_Notification_Email_Name 
     * @return void
     * @param List of Company_Request__c records
     ***********************************************************************/
     public static void sendEmails(List<Company_Request__c> requests){
        Logger.push('sendEmails','IVPOwnEmailNotificationBtch');
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        String templateName = Label.IVP_Second_Company_Request_Email_Notification_Email_Name;
        List<EmailTemplate> emTempls = [SELECT Id, HtmlValue, Body, Subject, Name FROM EmailTemplate 
                                        WHERE DeveloperName =:templateName ];
        
        // we will make it dynamocally  using custom Label;
        Id templateId = !emTempls.isEmpty() ? emTempls[0].Id : NULL;
        if(templateId != NULL){
            Id orgWideEmailId = getSysAdminOrgWideEmailId();
            for(Company_Request__c request : requests){
                String userId = request.Request_To__c;
                Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, userId, request.Id);
                String emailSubject = email.getSubject();
                String emailTextBody = email.getPlainTextBody();
                
                email.setTargetObjectId(userId);
                email.setSubject(emailSubject);
                email.setPlainTextBody(emailTextBody);
                email.saveAsActivity = false;
                if(orgWideEmailId != NULL){
                    email.setOrgWideEmailAddressId(orgWideEmailId);
                    
                }
                emails.add(email);
            }
            Messaging.sendEmail(emails);
        }
        Logger.pop();
    }
    /***********************************************************************
     * @author 10K-Expert
     * @description getting OrgWideEmailAddress and returning Id of that if found with the name of Salesforce Admin
     * @return Id
     * @param nothing
     ***********************************************************************/
     public static Id getSysAdminOrgWideEmailId(){
        List<OrgWideEmailAddress> orgWideEmails =  [SELECT Id
                                                    FROM OrgWideEmailAddress 
                                                    WHERE DisplayName = 'Salesforce Admin'];
        if(orgWideEmails.isEmpty()){
            return null;
        }else{
            return orgWideEmails[0].Id;
        }
    }
    /***********************************************************************
     * @author 10K-Expert
     * @description re-initiating this job again with different property so job will send email
     * @return void
     * @param Database.BatchableContext
     ***********************************************************************/
    public void finish(Database.BatchableContext BC){ }

}