/*** Trigger Handler for Financials__c Custom Object 
*** Creation Date: 2015/07/27
*** Created by TMW
**
** 4-4-2018: TO BE DEPRECATED
**
**/
public class FinancialsTriggerHandler 
{
    
    public void afterInsert( List<Financials__c> newFinancialsIn )
    {
        updateLatestFinancialsonCompany(newFinancialsIn);
    }

    public void afterUpdate( List<Financials__c> newFinancialsIn, Map<Id, Financials__c> oldFinancials)
    {
      updateLatestFinancialsonCompany(newFinancialsIn);
    }

    public void updateLatestFinancialsonCompany(List<Financials__c> financials){

        Set<id> accountIds = new Set<id>();
        for(Financials__c fin: financials){
            if(!accountIds.contains(fin.Account__c)){
                accountIds.add(fin.Account__c);
           }
        }
        if(accountIds.size()>0){
            List<Financials__c> allFinancials=[select Year_Text__c ,Revenue__c, Account__c from Financials__c where Account__c in  :accountIds];
            Map<Id,List<Financials__c>> financialsByAccount = new  Map<Id,List<Financials__c>>();
            for(Financials__c fin : allFinancials){
                if(!financialsByAccount.containsKey(fin.Account__c)) financialsByAccount.put(fin.Account__c, new List<Financials__c>());
                financialsByAccount.get(fin.Account__c).add(fin);
            }
            List<Account> accounts= [select Latest_Year_Revenue_Entered__c, Latest_Revenue__c, id from Account where id in :accountIds];
            for(Account acc: accounts){
                Decimal revenue=0;
                Integer latestYear=1900;
                for(Financials__c fin:financialsByAccount.get(acc.id)) {
                    if(!String.isBlank(fin.Year_Text__c) && fin.Revenue__c!=null ){
                        if(Integer.valueOf(fin.Year_Text__c)>=latestYear){
                            latestYear=Integer.valueOf(fin.Year_Text__c);
                            revenue=fin.Revenue__c;
                        }
                    }
                }
                if(latestYear>1900){
                  acc.Latest_Year_Revenue_Entered__c=String.valueOf(latestYear);
                  acc.Latest_Revenue__c=revenue;   
                }
                else {
                     acc.Latest_Year_Revenue_Entered__c=null;
                    acc.Latest_Revenue__c=null;   
                }
                
            }
            update accounts;

        }

    }

}