@isTest
private class CompanyCompetitorTriggerHandler_Test {
    
    private static Account testCompany;
    private static Account testCompetitor;
    
    static {
     
     testCompany = new Account(Name='testCompany');
     insert testCompany;
     testCompetitor = new Account(Name='testCompetitor');
    insert testCompetitor; 
        
    }
    // Test Methods
  static testMethod void testThisClass(){
    // insert test
     
    
    CompanyCompetitor__c testRec = new CompanyCompetitor__c(Company__c=testCompany.Id, Competitor__c=testCompetitor.Id);
    insert testRec;
    testRec = [select id, InverseCompanyCompetitor__c, Competitor__c, Company__c from CompanyCompetitor__c where Id = :testRec.Id];
    System.assertNotEquals(null, testRec.InverseCompanyCompetitor__c);
    
    CompanyCompetitor__c testInv = [select id, Company__c, Competitor__c, InverseCompanyCompetitor__c
      from CompanyCompetitor__c where id = :testRec.InverseCompanyCompetitor__c];
    System.assertEquals(testRec.Id, testInv.InverseCompanyCompetitor__c);
    System.assertEquals(testRec.Company__c, testInv.Competitor__c);
    System.assertEquals(testRec.Competitor__c, testInv.Company__c);
    
    
    // delete test
    delete testRec;
    list<CompanyCompetitor__c> testDelList = [select id from CompanyCompetitor__c where id = :testRec.InverseCompanyCompetitor__c];
    System.assertEquals(0, testDelList.size());
  }
    
}