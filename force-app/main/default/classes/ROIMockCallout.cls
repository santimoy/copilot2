@isTest
public class ROIMockCallout implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse response = new HttpResponse();
        String body = '[{"success": true}, {"success": false, "error": {"user_id": "String is shorter than 18 characters"}}]';
        response.setBody(body);
        response.setStatusCode(200);
        return response;
    }
}