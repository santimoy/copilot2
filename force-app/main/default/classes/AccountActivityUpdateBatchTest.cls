/***********************************************************************
* @author Vineet Bhagchandani
*  @date   27 Feburary 2019
*  @name   	AccountActivityUpdateBatch
*  @description Updates the fields on Account for Last Activity Date and Type
*			   if tasks are related to that account.
***********************************************************************/
@IsTest
public class AccountActivityUpdateBatchTest {
    
    @IsTest static void testRunThisJob_AccountLastActivityType() {
        
        // create dummy data
        Account acct = new Account(Name='testacct');
        insert acct;
        
        
        // insert events and attach it to accounts
        list<Event> eventList = new list<Event> {
            new Event(Subject='testevent1', WhatId=acct.Id, Type__c='Call', ActivityDateTime=system.now()+1, DurationInMinutes=30)
                };
                    insert eventList;
        Account testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        eventList[0].ActivityDateTime = system.now()-1;
        update eventList[0];
        
        
        
        //insert tasks and attach it to accounts
        list<Task> taskList = new list<Task> {
            new Task(Subject='testtask2', WhatId=acct.Id, Type__c='Meeting', ActivityDate=system.today()+1, Status='In Progress')
                };
                    insert taskList;
        
        System.assertEquals('Meeting', taskList[0].type__c, 'Error: TaskActivityTrigger did not properly update Last Activity Type during task inserts');
        taskList[0].Status = 'Completed';
        
        update taskList[0];
        testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        System.assertEquals('Completed', taskList[0].Status, 'Error: TaskActivityTrigger did not properly update Last Activity Status during task inserts');
        
        test.startTest();
        Database.executeBatch(new AccountActivityUpdateBatch(),100);
        test.stopTest();
        
    }
}