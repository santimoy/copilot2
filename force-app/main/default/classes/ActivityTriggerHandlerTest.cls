@isTest
public class ActivityTriggerHandlerTest {
  public static Id TASK_TALENT_RT = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
 @isTest(SeeAllData=true)
    static void Test_ActivityTriggerHandler_Task() {
        // custom settings 
        Common_Config__c cconf = Common_Config__c.getInstance();
        cconf.Task_Required_Fields__c = 'Description'; 
        cconf.Edit_Restriction_Message__c = 'Test message';
        cconf.Task_Edit_Profiles__c = '';
        if(String.isBlank(cconf.Id))
            insert cconf;
        else
            update cconf;
        
        // create dummy data
        Account acct = new Account(Name='testacct');
        insert acct;
        Contact con = new Contact(lastname = 'testCon');
        INSERT con;
        
        
        // insert tasks
        list<Task> taskList = new list<Task> {
            new Task(Subject='testtask1', WhatId=acct.Id, Type__c='Call', ActivityDate=system.today(), Status='Completed',RecordTypeId = TASK_TALENT_RT),
            new Task(Subject='testtask4', WhatId=acct.Id,Source__c='Inbound', Type__c='Email', ActivityDate=system.today(), Status='Completed'),    
            new Task(Subject='testtask2', WhatId=acct.Id, Type__c='Meeting', ActivityDate=system.today()+1, Status='In Progress'),
            new Task(Subject='testtask3', WhatId=acct.Id, Type__c='Other', ActivityDate=system.today()+2, Status='Not Started'),
            new Task(Subject='testtask4', WhatId=acct.Id, WhoId=con.Id,Source__c=null,Type__c='Email', ActivityDate=system.today(), Status='Completed'),
            new Task(Subject='testtask5', WhatId=acct.Id,WhoId=con.Id, Type__c='Voicemail', ActivityDate=system.today(), Status='In Progress',CallDisposition = 'Call -  No Answer')
        };
        insert taskList;
        
        Account testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        system.assertEquals('Other', testResults.Last_Activity_Type__c, 'Error: TaskActivityTrigger did not properly update Last Activity Type during task inserts');
        test.startTest();
        // update tasks
        taskList[2].Status = 'Completed';
        update taskList[2];
        testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        system.assertEquals('Meeting', testResults.Last_Activity_Type__c, 'Error: TaskActivityTrigger did not properly update Last Activity Type during task update');
        

        Task t1=new Task( WhatId=acct.Id, Type__c='Call', Description='To: Thomas From: John', ActivityDate=system.today(), Status='Completed',Subject = '[Outreach] [Out]');
       
        insert t1;

        delete t1;
       

        t1=new Task( WhatId=acct.Id, Type__c='Email', Description='To: Thomas From: John Body:EmailText', ActivityDate=system.today(), Status='Completed',Subject = 'Email');
       
        insert t1;

        /*  
        // delete task
        delete taskList[1];
        testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        system.assertEquals('Call', testResults.Last_Activity_Type__c, 'Error: TaskActivityTrigger did not properly update Last Activity Type during task delete');
        
        // undelete task
        undelete taskList[1];
        testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        system.assertEquals('Meeting', testResults.Last_Activity_Type__c, 'Error: TaskActivityTrigger did not properly update Last Activity Type during task undelete');
        */
        
        test.stopTest();
    }
    
   @isTest(SeeAllData=true)
    static void Test_ActivityTriggerHandler_Event() {
        // custom settings
        Common_Config__c cconf = Common_Config__c.getInstance();
        cconf.Task_Required_Fields__c = 'Description';
        cconf.Edit_Restriction_Message__c = 'Test message';
        cconf.Task_Edit_Profiles__c = '';
        if(String.isBlank(cconf.Id))
            insert cconf;
        else
            update cconf;
                    
        // create dummy data
        Account acct = new Account(Name='testacct');
        insert acct;
       
        test.startTest();
        
        // insert events
        list<Event> eventList = new list<Event> {
            new Event(Subject='testevent1', WhatId=acct.Id, Type__c='Call', ActivityDateTime=system.now()-2, DurationInMinutes=30),
            new Event(Subject='testevent2', WhatId=acct.Id, Type__c='Meeting', ActivityDateTime=system.now()+1, DurationInMinutes=30),
            new Event(Subject='testevent3', WhatId=acct.Id, Type__c='Other', ActivityDatetime=system.now()+2, DurationInMinutes=30)
        };
        insert eventList;
        Account testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
        //system.assertEquals('Call', testResults.Last_Activity_Type__c, 'Error: EventActivityTrigger did not properly update Last Activity Type during event inserts');
        
        // update events
       // eventList[1].ActivityDateTime = system.now()-1;
       // update eventList[1];
       // testResults = [select id, name, Last_Activity_Type__c, (SELECT id, ActivityDate, IsTask, Type__c FROM ActivityHistories ORDER BY ActivityDate DESC, LastModifiedDate DESC) from Account where id = :acct.Id];
       // system.assertEquals('Meeting', testResults.Last_Activity_Type__c, 'Error: EventActivityTrigger did not properly update Last Activity Type during event update');
        
        // delete event
        delete eventList[1];
        testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
      //  system.assertEquals('Call', testResults.Last_Activity_Type__c, 'Error: EventActivityTrigger did not properly update Last Activity Type during event delete');
        
        // undelete event
        undelete eventList[1];
        testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
      //  system.assertEquals('Meeting', testResults.Last_Activity_Type__c, 'Error: EventActivityTrigger did not properly update Last Activity Type during event undelete');
        
        test.stopTest();
    }
     @isTest
    static void Test_ActivityEmail_Pasar() {
       User u = [SELECT ID FROM User WHERE ID =: UserInfo.getUserId()];
       
         Account acct = new Account(Name='testacct');
        insert acct;
        list<Task> taskList = new list<Task> {new task(Subject='Follow up: Call', OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Outbound'),
            new task( OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Outbound',Subject = 'email: Outbound'),
            new task( OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Outbound',Subject = 'Following up: Outbound'),
          	new task( OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Outbound',Subject = 'Canceled: Outbound'),
          	new task( OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Inbound',Subject = 'Reply:  '),
            new task( OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Inbound',Subject = 'Sent Email:  '),
          	new task( OwnerId =u.Id,WhatId=acct.Id,Type__c='Email', ActivityDate=system.today(),Status='Completed',Source__c  = 'Inbound',Subject = 'Meeting Booked:  ')
         };
         INSERT taskList;     
        List<EmailParser_Killswitch__c> objShutoffInboundEmail = new List<EmailParser_Killswitch__c>
        {new EmailParser_Killswitch__c(Shut_off_Outbound_email_parsing__c = false, SetupOwnerId = taskList[0].OwnerId)
        };
        INSERT objShutoffInboundEmail;
		 Account testResults = [select id, name, Last_Activity_Type__c from Account where id = :acct.Id];
      system.assertEquals('Email', testResults.Last_Activity_Type__c);
        
    }
}