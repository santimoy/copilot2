/***********************************************************************************
 * @author      10k-Expert
 * @name		CompanyRequestTriggerHandler
 * @date		2018-11-14
 * @description	TriggerHandler of CompanyRequestTrigger
 ***********************************************************************************/
public class CompanyRequestTriggerHandler {
    public static Boolean byPassRejectionCheck = false;
    public static Boolean IS_NOT_COMPANY_REQUEST_PROCESS = true;
    
    /*************************************************************************************
     * @author 		10K-Expert
     * @description handling before insert event of Company Request
     * @param		List of new company request
     * @return 		void
     *************************************************************************************/
    public static void onBeforeInsert(List < Company_Request__c > companyRequests){
        Logger.push('onBeforeInsert','CompanyRequestTriggerHandler');
        Set<Id> userIds = new Set<Id>();
        // getting requester
        for (Company_Request__c companyRequest: companyRequests) {
            if(companyRequest.Request_By__c != NULL){
                userIds.add(companyRequest.Request_By__c);
            }
        }
        // getting checking and populating Requestor_Primary_Partner with requester's Primary_Partner value.
        if(!userIds.isEmpty()){
            Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Primary_Partner__c 
                                                        FROM User 
                                                        WHERE Id in :userIds AND Primary_Partner__c != NULL]);
            
            for (Company_Request__c companyRequest: companyRequests) {
                if(companyRequest.Request_By__c != NULL && userMap.containsKey(companyRequest.Request_By__c)){
                    companyRequest.Requestor_Primary_Partner__c = userMap.get(companyRequest.Request_By__c).Primary_Partner__c;
                }
            }
        }
        Logger.pop();
    }
    
    /*************************************************************************************
     * @author 		10K-Expert
     * @description handling after insert event of Company Request
     * @param		List of new company request
     * @return 		void
     *************************************************************************************/
    public static void onAfterInsert(List < Company_Request__c > companyRequests) {
        Logger.push('onAfterInsert','CompanyRequestTriggerHandler');
        //To publish event for handling on UI
        //publishEvent(companyRequests);
        /**
         * Company_Request__Share is the "Share" table that was created when the Organization Wide Default
         * sharing setting is "Private" for Company_Rating__c SObject
         * 
         **/
        List < Company_Request__Share > sharesRecords = new List < Company_Request__Share > ();
        String groupName = Label.IVP_Ownership_Company_Request_Group_Name;
        List < Group > groups = [SELECT Id, Name, DeveloperName FROM Group where DeveloperName =: groupName LIMIT 1];
        Id ivpOwnGroupId;
        if (!groups.isEmpty()) {
            ivpOwnGroupId = groups[0].Id;
        }
        /** For each of the Test records being inserted, do the following: **/
        for (Company_Request__c companyRequest: companyRequests) {

            /** Create a new Company_Request__Share record to be inserted **/
            Company_Request__Share comReqRec = new Company_Request__Share();

            /** Populate the Company_Request__Share record with the ID of the record to be shared. **/
            comReqRec.ParentId = companyRequest.Id;

            /** Then, set the ID of user or group being granted access. In this case,
             * we're setting the Id of the Request_To__c that is current owner of company
             * Result in the Request_To__c lookup field on the Company_Request record.
             **/

            comReqRec.UserOrGroupId = companyRequest.Request_To__c;

            /** Specify that the Request_To should have edit access for this particular Company_Request record. **/
            comReqRec.AccessLevel = 'edit';
            comReqRec.RowCause = Schema.Company_Request__Share.RowCause.Manual;
            if (ivpOwnGroupId != NULL) {
                Company_Request__Share grpComReqRec = comReqRec.clone();
                grpComReqRec.UserOrGroupId = ivpOwnGroupId;
                grpComReqRec.AccessLevel = 'read';
                sharesRecords.add(grpComReqRec);
            }
            /** Add the new Share record to the list of new Share records. **/
            sharesRecords.add(comReqRec);
        }

        /** Insert all of the newly created Share records and capture save result **/
        Database.SaveResult[] jobShareInsertResult = Database.insert(sharesRecords, false);
        processCompanyRequests(companyRequests, new Map < Id, Company_Request__c > ());
        Logger.pop();
    }

    /*************************************************************************************
     * @description after update of Company Request
     * @return 		void
     * @author 		10K-Expert
     * @param		List of new company request and map of old records of company request
     *************************************************************************************/
    public static void onAfterUpdate(List < Company_Request__c > newCompanRequestLst, Map < Id, Company_Request__c > oldCompanyRequestMap) {
        Logger.push('onAfterUpdate','CompanyRequestTriggerHandler');
        processCompanyRequests(newCompanRequestLst, oldCompanyRequestMap);
        Logger.pop();
    }

    /*************************************************************************************
     * @description Populating some values on Account related record
     * @return 		void
     * @author 		10K-Expert
     * @param		List of new company request and map of old records of company request
     *************************************************************************************/
    private static void processCompanyRequests(List < Company_Request__c > newCompanRequestLst, Map < Id, Company_Request__c > oldCompanyRequestMap) {
        Logger.push('processCompanyRequests','CompanyRequestTriggerHandler');
        Map < Id, Company_Request__c > mapWithAccountIdStatus = new Map < Id, Company_Request__c > ();
        Set < Id > accIds = new Set < Id > ();
        Set < String > validStatus = new Set < String > {
            'Accepted',
            'Rejected',
            'Transfer - Undisputed',
            'Transfer - No Response'
        };
        Set < String > tranferStatus = new Set < String > {
            'Accepted',
            'Transfer - Undisputed',
            'Transfer - No Response'
        };
        Map<Id, List<Id>> accWithCmpId = new Map<Id, List<Id>> ();
        
        for (Company_Request__c companyRequest: newCompanRequestLst) {
            if (oldCompanyRequestMap.isEmpty() // for insert trigger
                ||
                (!oldCompanyRequestMap.isEmpty() && // for update trigger
                    companyRequest.Status__c != oldCompanyRequestMap.get(companyRequest.Id).Status__c)) {

                if (validStatus.contains(companyRequest.Status__c)) {
                    if(!accWithCmpId.containsKey(companyRequest.Company__c)){
                        accWithCmpId.put(companyRequest.Company__c, new List<Id>());
                    }
                    accWithCmpId.get(companyRequest.Company__c).add(companyRequest.Id);
                    accIds.add(companyRequest.Company__c);
                    mapWithAccountIdStatus.put(companyRequest.Company__c, companyRequest);
                }
            }
        }
        List < Account > accLst = [SELECT Id, Partial_Credit_User__c, OwnerId, Ownership_Status__c FROM Account WHERE Id IN: accIds];
        for (Account acc: accLst) {
            if (tranferStatus.contains(mapWithAccountIdStatus.get(acc.Id).Status__c)) {

                acc.Ownership_Status__c = System.Label.IVP_Ownership_Status_Two;
                acc.OwnerId = mapWithAccountIdStatus.get(acc.Id).Request_By__c;
                acc.Partial_Credit_User__c = mapWithAccountIdStatus.get(acc.Id).Request_To__c;
                acc.Allow_Ownership_After_Demote_Upto__c = NULL;
                acc.After_Demote_Transfer_Upto_For_All__c = NULL;
                acc.After_Demote_Transfer_Upto_For_Partial__c = NULL;
                // acc.Wait_To_Re_Prioritize_After_Demote__c = NULL;
                // to bypass validation in the case of Transfer - Undisputed
                acc.Trigger_Update__c = System.now();
            } else if (mapWithAccountIdStatus.get(acc.Id).Status__c == 'Rejected') {
                acc.Ownership_Status__c = System.Label.IVP_Ownership_Status_Two;
                acc.OwnerId = mapWithAccountIdStatus.get(acc.Id).Request_To__c;
                acc.Partial_Credit_User__c = mapWithAccountIdStatus.get(acc.Id).Request_By__c;
            }
        }

        if (!accLst.isEmpty()) {
            byPassRejectionCheck = true;
            Map<Id, String> accWithError = new Map<Id, String>();
            List<Database.SaveResult> results = Database.update(accLst,false);
            System.debug(results);
            Boolean gotError=false;
            for(Integer ind = 0, len=results.size(); ind < len; ind++){
                if(!results[ind].isSuccess()){
                    gotError = true;
                    accWithError.put(accLst[ind].Id, results[ind].getErrors()[0].getMessage());
                }
            }
            if(gotError){
                for(Company_Request__c companyRequest: newCompanRequestLst){
                    if(companyRequest.Company__c != NULL && accWithError.containsKey(companyRequest.Company__c)){
                        companyRequest.addError(accWithError.get(companyRequest.Company__c));
                    }
                }
            }
            System.debug(accWithError);
            
            byPassRejectionCheck = false;
        }
        Logger.pop();
    }
    
}