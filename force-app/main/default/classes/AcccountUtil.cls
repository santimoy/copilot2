/**
* Created by Sukesh on 3rd July, 2019.
*/
global with sharing class AcccountUtil {

    global static final String STR_PB_ACTION_WEBSITE_STATUS_CHANGED = 'Website - Status Changed';
    global static final String STR_PB_ACTION_COMPANY_COUNTRY_CHANGED = 'Company Country = EU';
    global static final String STR_PB_ACTION_BECOMING_ACTIVE_PROTFOLIO_CHANGED = 'Becoming Active Protfolio';
    global static final String STR_PB_ACTION_BECOMING_INACTIVE_PROTFOLIO_CHANGED = 'Becoming Inactive Protfolio';
    global static final String STR_PB_ACTION_COMPANY_INFO_UPDATED = 'Company Info Updated';
    /**
    @Description Constructor Do Nothing
    */
    global AcccountUtil() {

    }

    /**          
    @Description  Method that will be called from Process Builder for Company
    @date         3rd July, 2019
    @param        lstAccounts  List of new Accounts
    */                                
    global static void initatePBActions(List<companyPBRequest> lstcompanyPBRequest) {

        Set<Id> setAccountIdForWebsiteStatusChange = new Set<Id>();
        Set<Id> setAccountIdForCompanyCountryChange = new Set<Id>();
        Set<Id> setAccountIdFoActivePortfolioChange = new Set<Id>();
        Set<Id> setAccountIdForInActivePortfolioChange = new Set<Id>();
        Set<Id> setAccountIdForCompanyInfoUpdated = new Set<Id>();

        for(companyPBRequest objcompanyPBRequest : lstcompanyPBRequest) {

            if((objcompanyPBRequest.strActionType).trim() == AcccountUtil.STR_PB_ACTION_WEBSITE_STATUS_CHANGED)
                setAccountIdForWebsiteStatusChange.add(objcompanyPBRequest.accountId);

            if((objcompanyPBRequest.strActionType).trim() == AcccountUtil.STR_PB_ACTION_COMPANY_COUNTRY_CHANGED)
                setAccountIdForCompanyCountryChange.add(objcompanyPBRequest.accountId);

            if((objcompanyPBRequest.strActionType).trim() == AcccountUtil.STR_PB_ACTION_BECOMING_ACTIVE_PROTFOLIO_CHANGED)
                setAccountIdFoActivePortfolioChange.add(objcompanyPBRequest.accountId);

            if((objcompanyPBRequest.strActionType).trim() == AcccountUtil.STR_PB_ACTION_BECOMING_INACTIVE_PROTFOLIO_CHANGED)
                setAccountIdForInActivePortfolioChange.add(objcompanyPBRequest.accountId);

            if((objcompanyPBRequest.strActionType).trim() == AcccountUtil.STR_PB_ACTION_COMPANY_INFO_UPDATED)
                setAccountIdForCompanyInfoUpdated.add(objcompanyPBRequest.accountId);
        }

        if(System.isBatch() || System.isFuture())
            executeRelatedActions(setAccountIdForWebsiteStatusChange,setAccountIdForCompanyCountryChange,setAccountIdFoActivePortfolioChange,setAccountIdForInActivePortfolioChange,setAccountIdForCompanyInfoUpdated);
        else
            executeRelatedActionsAsync(setAccountIdForWebsiteStatusChange,setAccountIdForCompanyCountryChange,setAccountIdFoActivePortfolioChange,setAccountIdForInActivePortfolioChange,setAccountIdForCompanyInfoUpdated);
    }

    /**
    @Description  Method will start executing the process in future context.
    @author       Sukesh
    @date         30th August, 2019
    @param        setAccountIds  set of Account Id's
    */
    @future
    global static void executeRelatedActionsAsync(Set<Id> setAccountIdForWebsiteStatusChange, Set<Id> setAccountIdForCompanyCountryChange, Set<Id> setAccountIdFoActivePortfolioChange,Set<Id> setAccountIdForInActivePortfolioChange,Set<Id> setAccountIdForCompanyInfoUpdated) {
        executeRelatedActions(setAccountIdForWebsiteStatusChange,setAccountIdForCompanyCountryChange,setAccountIdFoActivePortfolioChange,setAccountIdForInActivePortfolioChange,setAccountIdForCompanyInfoUpdated);
    }

    global static void executeRelatedActions(Set<Id> setAccountIdForWebsiteStatusChange, Set<Id> setAccountIdForCompanyCountryChange, Set<Id> setAccountIdFoActivePortfolioChange,Set<Id> setAccountIdForInActivePortfolioChange,Set<Id> setAccountIdForCompanyInfoUpdated) {

         if(!setAccountIdForWebsiteStatusChange.isEmpty())
            updateContactsPortfolioStatus(setAccountIdForWebsiteStatusChange);

        if(!setAccountIdForCompanyCountryChange.isEmpty())
            setContactsToEU(setAccountIdForCompanyCountryChange);

        if(!setAccountIdFoActivePortfolioChange.isEmpty())
            updateContacts(setAccountIdFoActivePortfolioChange);

        if(!setAccountIdForInActivePortfolioChange.isEmpty())
            deactivateContactsFromHL(setAccountIdForInActivePortfolioChange);

        if(!setAccountIdForCompanyInfoUpdated.isEmpty())
            updateHLField(setAccountIdForCompanyInfoUpdated);
    }

    /**
    @Description  Method that set the Website status on all related Contacts.
    @author       Sukesh
    @date         3rd July, 2019
    @param        setAccountIds  set of Account Id's
    */
    global static void updateContactsPortfolioStatus(Set<Id> setAccountIds) {

        List<Contact> lstContactToUpdate = new List<Contact>();

        for(Contact objContact : [SELECT Id, Portfolio_Company_Status__c, Account.Website_Status__c
                                         FROM Contact
                                         WHERE AccountId IN: setAccountIds]) {

            lstContactToUpdate.add(new Contact(Id = objContact.Id, Portfolio_Company_Status__c = objContact.Account.Website_Status__c));
        }

        update lstContactToUpdate;
    }

    /**
    @Description  Method that set the EU Contact equals true on all related Contacts.
    @author       Sukesh
    @date         24th July, 2019
    @param        setAccountIds  set of Account Id's
    */
    global static void setContactsToEU(Set<Id> setAccountIds) {

        List<Contact> lstContactToUpdate = new List<Contact>();

        for(Contact objContact : [SELECT Id, EU_Contact__c
                                         FROM Contact
                                         WHERE AccountId IN: setAccountIds]) {

            lstContactToUpdate.add(new Contact(Id = objContact.Id, EU_Contact__c = true));
        }

        update lstContactToUpdate;
    }

    /**
    @Description  Method that set the HigherLogic Member equals to true on all related Contacts.
    @author       Sukesh
    @date         24th July, 2019
    @param        setAccountIds  set of Account Id's
    */
    public static void updateContacts(Set<Id> setAccountIds) {

        List<Contact> lstContactToUpdate = new List<Contact>();

        for(Contact objContact : [SELECT Id, No_Longer_with_Company__c, HigherLogic_Member__c
                                         FROM Contact
                                         WHERE AccountId IN: setAccountIds
                                         AND No_Longer_with_Company__c = false]) {

            lstContactToUpdate.add(new Contact(Id = objContact.Id, HigherLogic_Member__c = true));
        }

        update lstContactToUpdate;
    }

    /**
    @Description  Method that set the HigherLogic Member equals to false on all related Contacts.
    @author       Sukesh
    @date         24th July, 2019
    @param        setAccountIds  set of Account Id's
    */
    public static void deactivateContactsFromHL(Set<Id> setAccountIds) {

        List<Contact> lstContactToUpdate = new List<Contact>();

        for(Contact objContact : [SELECT Id, HigherLogic_Member__c
                                         FROM Contact
                                         WHERE AccountId IN: setAccountIds]) {

            lstContactToUpdate.add(new Contact(Id = objContact.Id, HigherLogic_Member__c = false));
        }

        update lstContactToUpdate;
    }

    /**
    @Description  Method that set the Higher Logic date and time to now on Account
    @author       Sukesh
    @date         22nd Aug, 2019
    @param        setAccountIds  set of Account Id's
    */
    public static void updateHLField(Set<Id> setAccountIds) {

        List<Account> lstAccountToUpdate = new List<Account>();

        for(Id accountId : setAccountIds) {

            lstAccountToUpdate.add(new Account(Id=accountId,
                                              Higher_Logic_Change_Date__c = system.now()));
        }

        update lstAccountToUpdate;
    }


    /**
    @Description  Inner Class to Hold Invocable Variables
    @author       Sukesh
    @date         24th July, 2019
    */
    global class companyPBRequest {

        global Id accountId;
        global String strActionType;

        public companyPBRequest(Id accountId, String strActionType) {

            this.accountId = accountId;
            this.strActionType = strActionType;
        }
    }
}