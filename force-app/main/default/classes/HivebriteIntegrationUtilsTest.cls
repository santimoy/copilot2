/**
 * @File Name          : HivebriteIntegrationUtilsTest.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 6:21:46 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/11/2019   Anup Kage     Initial Version
**/
@isTest 
public with sharing class HivebriteIntegrationUtilsTest {
       
    @IsTest
    static void testAllMethods(){
        Test.startTest();
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        Update Company;
        Test.stopTest();
        // List<Account> recordList =  new List<Account>();
        // Account testAccount = new Account(Name = 'Test Util');
        // recordList.add(testAccount);
        // Insert recordList;        
        // // For checking hiveBriteField startWith value   
        
        // Test.startTest();
        // StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        // mock.setStaticResource('HivebriteUserTestClass');
        // mock.setStatusCode(200);
        // mock.setHeader('Content-Type', 'application/json');
        // Test.setMock(HttpCalloutMock.class, mock);
        // HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', recordList);
        // objHelper.startCalloutProcess();
        
        // HivebriteIntegrationSchedulable obj12= new HivebriteIntegrationSchedulable('PARTNER_SF_TO_HB', recordList);
        // obj12.execute(null);
        // HivebriteIntegrationUtils.hivebriteCompanyIntegration(recordList);
        // Test.stopTest();
       
        // Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        // String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        // List<Account> accountList = Database.query(query);
        // Test.startTest();
        // HivebriteIntegrationTestFactory.setupMockResponse();
        // HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', 'PORTFOLIO_SF_TO_HB', accountList);
        // objHelper.startCalloutProcess();

        // HivebriteIntegrationCompanyHelper objHelper1 = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        // objHelper1.startCalloutProcess();
        
        // Test.stopTest();
    }
    @IsTest
    static void testParseJSONBody(){
    Test.startTest();
            HivebriteIntegrationTestFactory.setupMockResponse();
            Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();          
            
            HivebriteIntegrationContactHelper contHelper = new HivebriteIntegrationContactHelper('Contact-User',
                                                                                             'Hivebrite API', 
                                                                                             'Get Contacts'); // helper class will  form request
            contHelper.startCalloutProcess();            
        Test.stopTest();      
    }
  
    @IsTest
    static void createPortfolioCompany(){
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecord();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> accountList = Database.query(query);
        //List<Account> accountList =  getAccountRecord(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
        
    }
    @IsTest
    static void createVendorRecords(){
        //HivebriteVentureTestData
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecord();
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account ';
        List<Account> accountList = Database.query(query);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
        
    }
    @IsTest
    static void createUserRecords(){
        //HivebriteVentureTestData
       List<Account> accountList = new List<Account>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContact(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponseWithDiffUsers();
       
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
                        
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', contactList);
        objHelper.insertContactRecords();

        Test.stopTest();
    }
    @IsTest
    static void updateUserRecords(){
        //HivebriteVentureTestData
        List<Account> accountList = new List<Account>();        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        Contact hivebriteUser = HivebriteIntegrationTestFactory.createContactWithHivebriteId(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponseWithDiffUsers();
      
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Contact')+' FROM Contact ';
        List<Contact> contactList = Database.query(query);
        HivebriteIntegrationContactHelper objHelper = new HivebriteIntegrationContactHelper('Contact-User', 'Hivebrite API', contactList);
        objHelper.insertContactRecords();
        Test.stopTest();
    }
    @IsTest
    static void coverFilterLogic(){
        
        Test.startTest();
            String condition = 'TRUE AND FALSE OR TRUE AND TRUE';
            HivebriteIntegrationUtils.evaluate(condition);
        Test.stopTest();
    }
}