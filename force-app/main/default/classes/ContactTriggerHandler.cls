/**  
*Description:   Contact trigger set all other contacts "key_contact" value to false. Also, to Update the Count of contacts on Account.
*Author:        Nimisha Jaiswal
*Created Date:  2019-10-01
*/
public without sharing class ContactTriggerHandler {
    
    public void onAfterInsert( List<Contact> newContacts ) {
        callHivebriteMethod();
        updateKeyContacts(newContacts);
        if(system.isFuture() || system.isBatch()){
            updateContactsCountOnAccount((new Map<Id,Contact>(newContacts)).keySet());
        }
        else {
            updateContactsCountOnAccountAsync((new Map<Id,Contact>(newContacts)).keySet());
        }
    }

    public void onAfterUpdate( List<Contact> newContacts, Map<Id, Contact> oldContactMap ) {
        callHivebriteMethod();
        List<Contact> lstContact = new List<Contact>();
        for(Contact objContact : newContacts) {

            if(objContact.Key_Contact__c != oldContactMap.get(objContact.Id).Key_Contact__c
               && objContact.Key_Contact__c) {

                lstContact.add(objContact);
            }
        }

        if(!lstContact.isEmpty())
            updateKeyContacts(lstContact);

        if(system.isFuture() || system.isBatch()){
            updateContactsCountOnAccount((new Map<Id,Contact>(newContacts)).keySet());
        }
        else {
            updateContactsCountOnAccountAsync((new Map<Id,Contact>(newContacts)).keySet());
        }
    }

    public void onAfterDelete( List<Contact> oldContacts ) {

        if(system.isFuture() || system.isBatch()){
            updateContactsCountOnAccount((new Map<Id,Contact>(oldContacts)).keySet());
        }
        else {
            updateContactsCountOnAccountAsync((new Map<Id,Contact>(oldContacts)).keySet());
        }
    }

    public void onAfterUndelete( List<Contact> newContacts ) {

        if(system.isFuture() || system.isBatch()){
            updateContactsCountOnAccount((new Map<Id,Contact>(newContacts)).keySet());
        }
        else {
            updateContactsCountOnAccountAsync((new Map<Id,Contact>(newContacts)).keySet());
        }
    }

    public void updateKeyContacts( List<Contact> lstContact ) {

        Set<Id> contactIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();

        for(Contact objContact : lstContact) {
            contactIds.add(objContact.Id);
            if(objContact.Key_Contact__c==true 
                && objContact.AccountId!=null)
                accountIds.add(objContact.AccountId);
        }
        
        if(accountIds.size() > 0) {
            
            list<Contact> pending = new list<Contact>();    
            list<Contact> others = [SELECT Id, 
                                           Key_Contact__c 
                                           FROM Contact 
                                           WHERE Id <>: contactIds 
                                           AND AccountId IN: accountIds 
                                           AND Key_Contact__c = true 
                                           AND RecordType.Name != 'IR Contact'];
            for(Contact objContact : others) {
                if(!contactIds.contains(objContact.Id)) {
                    objContact.Key_Contact__c=false;
                    pending.add(objContact);
                    contactIds.add(objContact.Id);
                }
            }
        
            if(pending.size() > 0) {
                update pending;
            }
        }
    }
    /**
    * Calling the HivebriteIntegrationUtils class to insert/update data in hivebrite site.
    */
    public static void callHivebriteMethod(){
        HivebriteIntegrationUtils.hivebriteIntegrationTrigger();
    }
    @future
    public static void updateContactsCountOnAccountAsync( Set<Id> setContactIds ) {
        updateContactsCountOnAccount(setContactIds);
    }

    public static void updateContactsCountOnAccount( Set<Id> setContactIds ) {

        Set<ID> setAccountIds = new Set<ID>();
        List<Account> lstAccountsToBeUpdated = new List<Account>();
        Map<Id, List<Contact>> mapAccountIdsToContacts = new Map<Id, List<Contact>>();

        for (Contact objContact : [SELECT Id, AccountId FROM Contact WHERE Id IN: setContactIds AND AccountId <> null]) {
            
            setAccountIds.add(objContact.AccountId);
        }

        for(Contact objContact : [SELECT Id,
                                         AccountId
                                         FROM Contact
                                         WHERE AccountId IN :setAccountIds 
                                         AND RecordTypeId != :AccountTriggerHandler.CONTACT_RECORDTYPE_ID]) {
            
            if(!mapAccountIdsToContacts.containsKey(objContact.AccountId))
                mapAccountIdsToContacts.put(objContact.AccountId, new List<Contact> {objContact});
            else 
                mapAccountIdsToContacts.get(objContact.AccountId).add(objContact);
        }

        for(Id accId : mapAccountIdsToContacts.keySet()) {
            
            lstAccountsToBeUpdated.add(new Account(Id = accId, 
                                                   Number_of_Contacts__c = mapAccountIdsToContacts.get(accId).size(),
                                                   Trigger_Update__c = DateTime.now()));
        }

        if(!lstAccountsToBeUpdated.isEmpty())
            Update lstAccountsToBeUpdated;
    }
}