public with sharing class EditTaskCommentButtonController {

    /**  
	@Description fetch rich text task comment.
	@author V2FORCE
	@date 10/09/2019
	*/
	// public class WrapperForUpdateComment{
	// 	@AuraEnabled
	// 	public Task updatedTaskComment{get;set;}
	// 	@AuraEnabled
	// 	public boolean isTalentUser{get;set;}
	// 	WrapperForUpdateComment(Task updatedTaskComment,boolean isTalentUser){
	// 		this.updatedTaskComment = updatedTaskComment;
	// 		this.isTalentUser = isTalentUser;
	// 	}

	// }


	@AuraEnabled
    public static Task_Comment__c fetchTaskComment(String taskID) {
		
		//system.debug('taskID>>>>'+taskID);
		
        List<Task_Comment__c> lstTaskComment = new List<Task_Comment__c>();
		lstTaskComment = [SELECT Id, Comment__c, Task_Id__c, IsEditedFromComponent__c,
		                         Company__c, User__c, Task_Comment_Subject__c, Task_Type__c, Activity_Date__c
								 FROM Task_Comment__c
								 WHERE Task_Id__c =: taskID];
		
		if(!lstTaskComment.isEmpty()) {
			
			return lstTaskComment[0];
		}
		return new Task_Comment__c();
    }
    
    /**  
	@Description fetch task record details when task comment does not exist.
	@author V2FORCE
	@date 10/09/2019
	*/
	// @AuraEnabled
    // public static list<WrapperForUpdateComment> fetchTaskPlainTextComment(String taskID) {

	// 	boolean isUser = null;
	// 	String currentUserRoleName ='';
	// 	List<WrapperForUpdateComment> lstTask = new List<WrapperForUpdateComment>();
	// 	Id currentUserRoleId = UserInfo.getUserRoleId();
	// 	if(currentUserRoleId != null){
	// 		currentUserRoleName = [SELECT ID, DeveloperName FROM UserRole WHERE ID =: currentUserRoleId LIMIT 1].DeveloperName;
	// 	}
	// 	List<String> talentViewRoleAccessList = new List<String>{'Talent_Group','Onsite','Onsite_Management'};
	// 	if(currentUserRoleName !=null && talentViewRoleAccessList.contains(currentUserRoleName)){
	// 		isUser= true;
	// 	}
	// 	else{
	// 		isUser= false;
	// 	}
      
	// 	Task getTask = [SELECT Id, Description, WhatId, Subject, Type
	// 					  FROM Task
	// 					  WHERE Id =: taskID];
	// 	system.debug('getTask'+getTask);
		
	// 	if(lstTask.isEmpty()) {
	// 		lstTask.add(new WrapperForUpdateComment(getTask,isUser));
	// 		return lstTask;
	// 	}
	// 	return new list<WrapperForUpdateComment>();
	// }
	

	@AuraEnabled
    public static Task fetchTaskPlainTextComment(String taskID) {
        
        List<Task> lstTask = new List<Task>();
		lstTask = [SELECT Id, Description, WhatId, Subject, Type
						  FROM Task
						  WHERE Id =: taskID];
		
		if(!lstTask.isEmpty()) {
			
			return lstTask[0];
		}
		return new Task();
    }

    /**  
	@Description update task comment.
	@author V2FORCE
	@date 10/09/2019
	*/
	@AuraEnabled
    public static void updateTaskComment(Task_Comment__c objTaskComment, Task objTask) {
        
        try {
            if(objTaskComment != null) {
            
                objTaskComment.IsEditedFromComponent__c = true;
                update objTaskComment;
            }
            if(objTask != null) {
                objTask.Task_Comment__c = (objTaskComment != null) ? objTaskComment.Id : null; 
                objTask.Description = (objTaskComment != null) ? objTaskComment.Comment__c.replace('/r/n','---r---n').replace('</p>','---r---n').replace('<li>','---r---n').stripHtmlTags().replace('---r---n','\r\n') : objTask.Description;
                update objTask;
            }
            if(objTaskComment != null) {
                objTaskComment.IsEditedFromComponent__c = false;
                update objTaskComment;
            }
        } catch (Exception ex) {
            
            system.debug('==ex=>>>'+ex.getMessage()+'===Line=='+ex.getLineNumber());
        }
	}
	//@AuraEnabled
	//public static boolean checkLogedInUserRole(){
		//String currentUserRoleName ='';
		//Id currentUserRoleId = UserInfo.getUserRoleId();
		//if(currentUserRoleId != null){
		//	currentUserRoleName = [SELECT ID, DeveloperName FROM UserRole WHERE ID =: currentUserRoleId LIMIT 1].DeveloperName;
		//}
		//List<String> talentViewRoleAccessList = new List<String>{'Talent_Group','Onsite','Onsite_Management'};
		//if(currentUserRoleName !=null && talentViewRoleAccessList.contains(currentUserRoleName)){
		//	return true;
		//}
		//eturn false;
	//}
	
}