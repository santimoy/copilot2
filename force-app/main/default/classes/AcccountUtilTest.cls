/**
* Created by Sukesh on 3rd July
*/
@isTest
private class AcccountUtilTest {


    /**
        @Description  Method used to test out scenarios to update child contacts Portfolio Status when Account Website - Status is updated.
        @author       Sukesh
        @date         07/03/19
    */
    @isTest
    static void testUpdateContactsPortfolioStatus() {

        AcccountUtil acctUtl = new AcccountUtil();
        Integer noOfRecordsToBeCreated = 20;
        Integer halfNoOfRecords = 10; 
        /** Insert an Account */
        List<Account> lstAccount = TestFactory.createSObjectList(new Account(), noOfRecordsToBeCreated, true);

        /** Insert a child contacts for above inserted Account */
        List<Contact> lstContacts = TestFactory.createSObjectList(new Contact(), noOfRecordsToBeCreated, false);

        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstContacts[i].AccountId = lstAccount[i].Id;

        }
        insert lstContacts;

        /** Check for records creation */
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Account].size());
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact].size());

        //Test.startTest();
        for(Integer i = 0; i < halfNoOfRecords; i++) {

            lstAccount[i].Website_Status__c = 'Current Investment';
        }

        for(Integer i = halfNoOfRecords; i < noOfRecordsToBeCreated; i++) {

            lstAccount[i].Website_Status__c = 'Prior Investment';
        }
        Update lstAccount;
        //Test.stopTest();
        /** Check for Contacts Portfolio Status */
        // System.assertEquals(halfNoOfRecords, [SELECT Id FROM Contact WHERE Portfolio_Company_Status__c = 'Current Investment'].size());
        // System.assertEquals(halfNoOfRecords, [SELECT Id FROM Contact WHERE Portfolio_Company_Status__c = 'Prior Investment'].size());
    }

    /**
        @Description  Method used to test out scenarios to update child contacts EU Contact equals true when Account Company Country = EU updated.
        @author       Sukesh
        @date         07/24/19
    */
    @isTest
    static void testSetContactsToEU() {

        Integer noOfRecordsToBeCreated = 20;
        /** Insert an Account */
        List<Account> lstAccount = TestFactory.createSObjectList(new Account(), noOfRecordsToBeCreated, true);
        /** Insert a child contacts for above inserted Account */
        List<Contact> lstContacts = TestFactory.createSObjectList(new Contact(), noOfRecordsToBeCreated, false);
        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstContacts[i].AccountId = lstAccount[i].Id;
        }
        insert lstContacts;
        /** Check for records creation */
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Account].size());
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact].size());
        // System.assertEquals(0, [SELECT Id FROM Contact WHERE EU_Contact__c = true].size());

        //Test.startTest();
        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstAccount[i].BillingCountry = 'Austria';
        }
        Update lstAccount;
        //Test.stopTest();
        /** Check for Contacts EU Contact status */
        //System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact WHERE EU_Contact__c = true].size());
    }

    /**
        @Description  Method used to test out scenarios to update child contacts HigherLogic Member equals to true when Account Becoming Active Protfolio updated.
        @author       Sukesh
        @date         07/24/19
    */
    @isTest
    static void testUpdateContacts() {

        Integer noOfRecordsToBeCreated = 20;
        /** Insert an Account */
        List<Account> lstAccount = TestFactory.createSObjectList(new Account(), noOfRecordsToBeCreated, true);
        /** Insert a child contacts for above inserted Account */
        List<Contact> lstContacts = TestFactory.createSObjectList(new Contact(), noOfRecordsToBeCreated, false);
        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstContacts[i].AccountId = lstAccount[i].Id;
        }
        insert lstContacts;
        /** Check for records creation */
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Account].size());
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact].size());
        // System.assertEquals(0, [SELECT Id FROM Contact WHERE HigherLogic_Member__c = true].size());

        //Test.startTest();
        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstAccount[i].Website_Status__c = 'Current Investment';
        }
        Update lstAccount;
        //Test.stopTest();
         /** Check for Contacts HigherLogic Member status */
        //System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact WHERE HigherLogic_Member__c = true].size());
    }

    /**
        @Description  Method used to test out scenarios to update child contacts HigherLogic Member equals to false when Account Becoming Inactive Protfolio updated.
        @author       Sukesh
        @date         07/24/19
    */
    @isTest
    static void testDeactivateContactsFromHL() {

        Integer noOfRecordsToBeCreated = 10;
        /** Insert an Account */
        List<Account> lstAccount = TestFactory.createSObjectList(new Account(), noOfRecordsToBeCreated, true);
        /** Insert a child contacts for above inserted Account */
        List<Contact> lstContacts = TestFactory.createSObjectList(new Contact(), noOfRecordsToBeCreated, false);
        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstContacts[i].AccountId = lstAccount[i].Id;
            lstContacts[i].HigherLogic_Member__c = true;
        }
        insert lstContacts;
        /** Check for records creation */
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Account].size());
        // System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact].size());
        // System.assertEquals(0, [SELECT Id FROM Contact WHERE HigherLogic_Member__c = false].size());

        //Test.startTest();
        for(Integer i = 0; i < noOfRecordsToBeCreated; i++) {

            lstAccount[i].Website_Status__c = 'Prior Investment';

        }
        Update lstAccount;
        //Test.stopTest();
         /** Check for Contacts HigherLogic Member status */
        //System.assertEquals(noOfRecordsToBeCreated, [SELECT Id FROM Contact WHERE HigherLogic_Member__c = false].size());
    }

    /**
        @Description  Method used to update Higher Logic Change Date time update
        @author       Sukesh
        @date         22nd Aug 2019
    */
    @isTest
    static void testupdateHLField() {

        Integer noOfRecordsToBeCreated = 20;
        /** Insert an Account */
        List<Account> lstAccount = TestFactory.createSObjectList(new Account(), noOfRecordsToBeCreated, false);

        for(Account objAccount : lstAccount) {

            objAccount.Website_Status__c = 'Prior Investment';
        }

        insert lstAccount;

        for(Account objAccount : lstAccount) {

            //System.assert(!String.isEmpty(objAccount.Website_Status__c));
        }

        for(Account objAccount : lstAccount) {

            objAccount.ShippingCountry = 'India';
        }

        update lstAccount;

        for(Account objAccount : lstAccount) {

            //System.assert(!String.isEmpty(objAccount.Website_Status__c));
        }
    }
}