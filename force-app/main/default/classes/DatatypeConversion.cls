public with sharing class DatatypeConversion  {
    
    public static SObject convertTo(Object convert, String Type, SObject record, String fieldApi){
        try {
           
            if(convert == null){
                record.put(fieldApi, null);

            }else if(type.equalsIgnoreCase('boolean')){
                record.put(fieldApi, Boolean.valueOf(convert));
                
            }else if(type.equalsIgnoreCase('Date')){
                record.put(fieldApi, Date.valueOf(convert));
                            
            }else if(type.equalsIgnoreCase('Datetime')){
                record.put(fieldApi, Datetime.valueOf(convert));                        
                            
            }else if(type.equalsIgnoreCase('Decimal')){
                record.put(fieldApi, Decimal.valueOf(String.valueOf(convert)));                        
                            
            }else if(type.equalsIgnoreCase('Double')){
                record.put(fieldApi, Double.valueOf(convert));
                            
            }else if(type.equalsIgnoreCase('ID')){
                record.put(fieldApi, ID.valueOf(String.valueOf(convert)));
                            
            }else if(type.equalsIgnoreCase('Integer')){
                record.put(fieldApi, Integer.valueOf(convert));
                            
            }else if(type.equalsIgnoreCase('Long')){
                record.put(fieldApi, Long.valueOf(String.valueOf(convert)));
                            
            }else if(type.equalsIgnoreCase('String')){
                record.put(fieldApi, String.valueOf(convert));
                            
            }else if(type.equalsIgnoreCase('Time')){
                record.put(fieldApi, DateTime.valueOf(convert).time());
    
            }else if(type.equalsIgnoreCase('text')){
                record.put(fieldApi, String.valueOf(convert));

            }else{
                record.put(fieldApi, String.valueOf(convert));
            }
        } catch (Exception e) {
           System.debug('inside typecasting');
       }
        return record;
    }
}