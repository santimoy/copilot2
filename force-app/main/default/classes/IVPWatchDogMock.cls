@isTest
global class IVPWatchDogMock implements HttpCalloutMock{
    public static Boolean defaultRepsonse = true;
    
    global IVPWatchDogMock(){}
    global IVPWatchDogMock(Boolean defaultRepsonse){
        IVPWatchDogMock.defaultRepsonse = defaultRepsonse;
    }
    global HttpResponse respond(HttpRequest request){
        HttpResponse response = new HttpResponse();
        List<ListView> listViewRec = [SELECT Id FROM ListView LIMIT 1];
        String viewId = ListViewRec[0].Id;
        String body = '';
        String conditions='"conditions":[{"conditions":[{"field":"Type","operator":"equals","values":["\'Prospect\'"]},'
                            +'{"field":"Type","operator":"equals","values":["\'Portfolio\'"]},'
                            +'{"field":"Type","operator":"equals","values":["\'Former Portfolio\'"]},'
                            +'{"field":"Type","operator":"equals","values":["\'Inactive\'"]}],'
                            +'"conjunction":"or"},{"condition":{"field":"KCName__c","operator":"like","values":["\'%Rahul%\'"]}},'
                            +'{"condition":{"field":"KCName__c","operator":"like","values":["\'%Test%\'"]}},'
                            +'{"field":"RecordTypeId","operator":"equals","values":["\''+'012d0000000pNy2\''+'"]},'+
                            +'{"field":"Unassigned_for_me__c","operator":"equals","values":["true"]}],"conjunction":"and"';
       	
        if(request.getEndpoint().endsWith('listviews/')){
        	body = '{'+
                '"done": true,'+
                '"listviews": ['+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCloEAG/describe",'+
                        '"developerName": "All_Companies_Test_Public",'+
                        '"id": "00B1h000000WCloEAG",'+
                        '"label": "All Companies Test (Public)",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCloEAG/results",'+
                      '  "soqlCompatible": true,'+
                     '   "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCloEAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCmNEAW/describe",'+
                        '"developerName": "ivp_sd_chart_005d0000004UY5qAAG",'+
                        '"id": "00B1h000000WCmNEAW",'+
                        '"label": "Companies w/ Owner Status Calc = Not ...",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCmNEAW/results",'+
                       ' "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCmNEAW"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCnuEAG/describe",'+
                      '  "developerName": "Companies_Starting_with_B",'+
                       ' "id": "00B1h000000WCnuEAG",'+
                        '"label": "Companies Starting with B",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCnuEAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCnuEAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCo4EAG/describe",'+
                        '"developerName": "ivp_sd_tag_005d0000004UY5qAAG",'+
                        '"id": "00B1h000000WCo4EAG",'+
                        '"label": "Companies Tagged w/ M&A",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCo4EAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCo4EAG"'+
                    '},'+
                    '{'+
                       ' "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCuqEAG/describe",'+
                        '"developerName": "ivp_sd_tag_005d00000067zKfAAI",'+
                        '"id": "00B1h000000WCuqEAG",'+
                        '"label": "Companies Tagged w/ DEV",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCuqEAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WCuqEAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD0jEAG/describe",'+
                       ' "developerName": "ivp_sd_tag_005d0000006AohKAAS",'+
                       ' "id": "00B1h000000WD0jEAG",'+
                       ' "label": "Companies Tagged w/ M&A",'+
                       ' "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD0jEAG/results",'+
                       ' "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD0jEAG"'+
                    '},'+
                    '{'+
                       ' "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD13EAG/describe",'+
                        '"developerName": "My_Recently_Unassigned_Public",'+
                       ' "id": "00B1h000000WD13EAG",'+
                       ' "label": "My Recently Unassigned (Public)",'+
                       ' "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD13EAG/results",'+
                       ' "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD13EAG"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD6XEAW/describe",'+
                      '  "developerName": "ivp_sd_chart_005d0000004UT2nAAG",'+
                      '  "id": "00B1h000000WD6XEAW",'+
                      '  "label": "Companies w/ Company Type = Pipeline",'+
                      '  "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD6XEAW/results",'+
                      '  "soqlCompatible": true,'+
                      '  "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD6XEAW"'+
                   ' },'+
                   ' {'+
                       ' "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD9CEAW/describe",'+
                       ' "developerName": "ivp_sd_tag_005d0000004UT2nAAG",'+
                       ' "id": "00B1h000000WD9CEAW",'+
                       ' "label": "Companies Tagged w/ Demo",'+
                       ' "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD9CEAW/results",'+
                       ' "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WD9CEAW"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDFZEA4/describe",'+
                      '  "developerName": "ivp_sd_chart_0050W000006vMyMQAU",'+
                       ' "id": "00B1h000000WDFZEA4",'+
                       ' "label": "Companies w/ Owner Status Calc = Expi...",'+
                       ' "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDFZEA4/results",'+
                       ' "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDFZEA4"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDHVEA4/describe",'+
                      '  "developerName": "ivp_sd_chart_005d00000067zKfAAI",'+
                       ' "id": "00B1h000000WDHVEA4",'+
                        '"label": "Companies w/ Company Type = Prospect",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDHVEA4/results",'+
                        '"soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDHVEA4"'+
                   ' },'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDKUEA4/describe",'+
                     '   "developerName": "ivp_sd_tag_0050W000006Dv3YQAS",'+
                      '  "id": "00B1h000000WDKUEA4",'+
                      '  "label": "Companies Tagged w/ Fiedler_QA",'+
                      '  "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDKUEA4/results",'+
                      '  "soqlCompatible": true,'+
                      '  "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDKUEA4"'+
                    '},'+
                    '{'+
                    '    "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDOlEAO/describe",'+
                    '    "developerName": "ivp_sd_tag_0051h0000014DhLAAU",'+
                    '    "id": "00B1h000000WDOlEAO",'+
                    '    "label": "Companies Tagged w/ Anika_Test",'+
                    '    "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDOlEAO/results",'+
                    '    "soqlCompatible": true,'+
                    '    "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDOlEAO"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDUPEA4/describe",'+
                     '   "developerName": "ivp_sd_chart_005d00000067zKpAAI",'+
                     '   "id": "00B1h000000WDUPEA4",'+
                     '   "label": "Companies w/ Company Type = Prospect",'+
                     '   "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDUPEA4/results",'+
                     '   "soqlCompatible": true,'+
                     '   "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDUPEA4"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDV8EAO/describe",'+
                      '  "developerName": "ivp_sd_tag_005d00000067zKpAAI",'+
                      '  "id": "00B1h000000WDV8EAO",'+
                      '  "label": "Companies Tagged w/ Great Company",'+
                     '   "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDV8EAO/results",'+
                      '  "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDV8EAO"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgBEAW/describe",'+
                     '   "developerName": "My_Smartdash_public",'+
                     '   "id": "00B1h000000WDgBEAW",'+
                     '   "label": "My Smartdash (Public)",'+
                     '   "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgBEAW/results",'+
                     '   "soqlCompatible": true,'+
                     '   "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgBEAW"'+
                    '},'+
                   ' {'+
                    '    "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgfEAG/describe",'+
                    '    "developerName": "ivp_sd_tag_005d0000002aISsAAM",'+
                    '    "id": "00B1h000000WDgfEAG",'+
                    '    "label": "Companies Tagged w/ Anika_Test",'+
                    '    "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgfEAG/results",'+
                    '    "soqlCompatible": true,'+
                    '    "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgfEAG"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgkEAG/describe",'+
                     '   "developerName": "ivp_sd_chart_005d0000002aISsAAM",'+
                     '   "id": "00B1h000000WDgkEAG",'+
                     '   "label": "Companies w/ Owner Status Calc = Curr...",'+
                      '  "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgkEAG/results",'+
                      '  "soqlCompatible": true,'+
                      '  "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDgkEAG"'+
                   ' },'+
                   ' {'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDh9EAG/describe",'+
                       ' "developerName": "My_Companies_That_Need_Ratings_Public",'+
                       ' "id": "00B1h000000WDh9EAG",'+
                       ' "label": "My Companies That Need Ratings (Public)",'+
                       ' "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDh9EAG/results",'+
                       ' "soqlCompatible": true,'+
                      '  "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDh9EAG"'+
                    '},'+
                    '{'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhJEAW/describe",'+
                      '  "developerName": "ivp_sd_chart_005d0000006AohZAAS",'+
                      '  "id": "00B1h000000WDhJEAW",'+
                      '  "label": "Companies w/ Owner Status Calc = Curr...",'+
                      '  "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhJEAW/results",'+
                       ' "soqlCompatible": true,'+
                       ' "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhJEAW" '+
                   ' },'+
                   ' {'+
                    '    "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhOEAW/describe",'+
                    '    "developerName": "ivp_sd_chart_005d0000003PNspAAG",'+
                    '    "id": "00B1h000000WDhOEAW",'+
                    '    "label": "Companies w/ Owner Status Calc = Curr...", '+
                    '    "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhOEAW/results",'+
                    '    "soqlCompatible": true,'+
                    '    "url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhOEAW"'+
                   ' },'+
                   ' {'+
                     '   "describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhTEAW/describe",'+
                     '   "developerName": "ivp_sd_chart_005d0000003PNsoAAG",'+
                     '   "id": "00B1h000000WDhTEAW",'+
                     '   "label": "Companies w/ Owner Status Calc = Expi...",'+
                     '   "resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhTEAW/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhTEAW"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhsEAG/describe",'+
                        '"developerName": "ivp_sd_chart_005d0000003POTVAA4",'+
                        '"id": "00B1h000000WDhsEAG",'+
                        '"label": "Companies w/ Owner Status Calc = Curr...",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhsEAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhsEAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhxEAG/describe",'+
                        '"developerName": "ivp_sd_tag_005d0000003PNspAAG",'+
                        '"id": "00B1h000000WDhxEAG",'+
                        '"label": "Companies Tagged w/ Great Company",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhxEAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDhxEAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDi2EAG/describe",'+
                        '"developerName": "Test16",'+
                        '"id": "00B1h000000WDi2EAG",'+
                        '"label": "Test",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDi2EAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDi2EAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDiCEAW/describe",'+
                        '"developerName": "Test17",'+
                        '"id": "00B1h000000WDiCEAW",'+
                        '"label": "Test",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDiCEAW/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDiCEAW"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDiHEAW/describe",'+
                        '"developerName": "ivp_sd_tag_005d0000003POTVAA4",'+
                        '"id": "00B1h000000WDiHEAW",'+
                        '"label": "Companies Tagged w/ TeamAJ",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDiHEAW/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDiHEAW"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDj0EAG/describe",'+
                        '"developerName": "Tom_s_List_View",'+
                        '"id": "00B1h000000WDj0EAG",'+
                        '"label": "Toms List View",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDj0EAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDj0EAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDjtEAG/describe",'+
                        '"developerName": "French_companies_in_Bitcoin",'+
                        '"id": "00B1h000000WDjtEAG",'+
                        '"label": "French companies in Bitcoin",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDjtEAG/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000WDjtEAG"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfPcEAK/describe",'+
                        '"developerName": "ivp_sd_chart_005d0000003PvxOAAS",'+
                        '"id": "00B1h000000YfPcEAK",'+
                        '"label": "Companies w/ Owner Status Calc = Curr...",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfPcEAK/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfPcEAK"'+
                   ' },'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfPhEAK/describe",'+
                        '"developerName": "Test_public_LISTVIEW",'+
                        '"id": "00B1h000000YfPhEAK",'+
                        '"label": "Test public LISTVIEW",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfPhEAK/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfPhEAK"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000Yfr2EAC/describe",'+
                        '"developerName": "ivp_sd_tag_005d0000003PNssAAG",'+
                        '"id": "00B1h000000Yfr2EAC",'+
                        '"label": "Companies Tagged w/ DEV",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000Yfr2EAC/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000Yfr2EAC"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfrlEAC/describe",'+
                        '"developerName": "ivp_sd_chart_005d0000003PNssAAG",'+
                        '"id": "00B1h000000YfrlEAC",'+
                        '"label": "Companies w/ Company Type = Pipeline",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfrlEAC/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfrlEAC"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfrvEAC/describe",'+
                        '"developerName": "ivp_sd_chart_0051h0000014DhQAAU",'+
                        '"id": "00B1h000000YfrvEAC",'+
                        '"label": "Companies w/ Company Type = Prospect",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfrvEAC/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YfrvEAC"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YjP7EAK/describe",'+
                        '"developerName": "Abhay_Test_Private_LV",'+
                        '"id": "00B1h000000YjP7EAK",'+
                        '"label": "Abhay_Test_Private_LV",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YjP7EAK/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YjP7EAK"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YjUgEAK/describe",'+
                        '"developerName": "Abhay_Test_Public_LV",'+
                        '"id": "00B1h000000YjUgEAK",'+
                        '"label": "Abhay_Test_Public_LV",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YjUgEAK/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00B1h000000YjUgEAK"'+
                    '},'+
                    '{'+
                        '"describeUrl": "/services/data/v32.0/sobjects/Account/listviews/00Bd0000004JqNREA0/describe",'+
                        '"developerName": "RecentlyViewedAccounts",'+
                        '"id": "00Bd0000004JqNREA0",'+
                        '"label": "Recently Viewed Companies",'+
                        '"resultsUrl": "/services/data/v32.0/sobjects/Account/listviews/00Bd0000004JqNREA0/results",'+
                        '"soqlCompatible": true,'+
                        '"url": "/services/data/v32.0/sobjects/Account/listviews/00Bd0000004JqNREA0"'+
                    '}'+
                '],'+
                '"nextRecordsUrl": null,'+
                '"size": 37,'+
                '"sobjectType": "Account"'+
            '}';
        }else if(request.getEndpoint().contains('services/data/v32.0/sobjects/Account/listviews/')){
           body='{"columns": [{"ascendingLabel": "Z-A", "descendingLabel": "A-Z","fieldNameOrPath": "Name","hidden": false,"label": "Company Name",'+
                +'"selectListItem": "Name", "sortDirection": "ascending", "sortIndex": 0, "sortable": true,"type": "string"}, { "ascendingLabel": "Z-A",'+
                +' "descendingLabel": "A-Z", "fieldNameOrPath": "CreatedBy.Alias", "hidden": false,"label": "Created By Alias","selectListItem": "CreatedBy.Alias",'+
                +'"sortDirection": null, "sortIndex": null, "sortable": true,"type": "string"}, {"ascendingLabel": "Z-A","descendingLabel": "A-Z",'+
                +' "fieldNameOrPath": "DWH_Verticals__c", "hidden": false, "label": "Verticals", "selectListItem": "DWH_Verticals__c","sortDirection": null,'+
                +' "sortIndex": null, "sortable": true, "type": "string" }, { "ascendingLabel": "9-0","descendingLabel": "0-9","fieldNameOrPath": "Total_Funding_Raised__c",'+
                +'"hidden": false, "label": "Total Funding Raised","selectListItem": "Total_Funding_Raised__c","sortDirection": null,"sortIndex": null,'+
                +'"sortable": true,"type": "currency"},{"ascendingLabel": "9-0","descendingLabel": "0-9", "fieldNameOrPath": "NumberOfEmployees","hidden": false,'+
                +'"label": "Employees", "selectListItem": "NumberOfEmployees","sortDirection": null,"sortIndex": null,"sortable": true,"type": "int"},'+
                +' { "ascendingLabel": "9-0", "descendingLabel": "0-9","fieldNameOrPath": "DWH_Employee_Growth_6_Month__c","hidden": false,'+
                +'"label": "Employee Growth % - 6 Month", "selectListItem": "DWH_Employee_Growth_6_Month__c", "sortDirection": null,"sortIndex": null,'+
                +'"sortable": true,"type": "percent" }, {"ascendingLabel": "9-0","descendingLabel": "0-9","fieldNameOrPath": "RecordType.Name","hidden": false,'+
                +'"label": "Company Record Type","selectListItem": "RecordType.Name","sortDirection": null,"sortIndex": null,"sortable": true,"type": "double"},'+
                +'{"ascendingLabel": "Z-A", "descendingLabel": "A-Z","fieldNameOrPath": "Next_Action__c","hidden": false,"label": "Next Action",'+
                +'"selectListItem": "Next_Action__c","sortDirection": null,"sortIndex": null,"sortable": true,"type": "string"},{"ascendingLabel": "New to Old",'+
                +'"descendingLabel": "Old to New","fieldNameOrPath": "Next_Action_Date__c","hidden": false,"label": "Next Action Date", "selectListItem": "Next_Action_Date__c",'+
                +'"sortDirection": null,"sortIndex": null,"sortable": true,"type": "date" }, { "ascendingLabel": "Z-A", "descendingLabel": "A-Z",'+
                +'"fieldNameOrPath": "Owner.Alias", "hidden": false, "label": "Company Owner Alias", "selectListItem": "Owner.Alias", "sortDirection": null,"sortIndex": null,'+
                +'"sortable": true,"type": "string"},{"ascendingLabel": null,"descendingLabel": null,"fieldNameOrPath": "Id","hidden": true,"label": "Company ID",'+
                +'"selectListItem": "Id","sortDirection": null,"sortIndex": null,"sortable": false,"type": "id" }, {"ascendingLabel": null,"descendingLabel": null,'+
                +'"fieldNameOrPath": "LastModifiedBy.Alias", "hidden": true,"label": "Last Modified By Alias", "selectListItem": "LastModifiedBy.Alias","sortDirection": null,"sortIndex": null,'+
                +'"sortable": false, "type": "reference"}, {"ascendingLabel": null,"descendingLabel": null,"fieldNameOrPath": "CreatedDate","hidden": true,'+
                +'"label": "Created Date","selectListItem": "CreatedDate","sortDirection": null,"sortIndex": null,'+
                +'"sortable":false,"type":"datetime"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"LastModifiedDate",'+
                +'"hidden":true,"label":"Last Modified Date","selectListItem":"LastModifiedDate","sortDirection":null,"sortIndex":null,'+
                +'"sortable":false,"type":"datetime"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"SystemModstamp",'+
                +'"hidden":true,"label":"System Modstamp","selectListItem":"SystemModstamp","sortDirection":null,"sortIndex":null,'+
                +'"sortable":false,"type":"datetime"}],'+
                +'"id": "00B1h000000WCuq","orderBy": [{'+
                +'"fieldNameOrPath": "Name","nullsPosition": "first","sortDirection": "ascending"},{"fieldNameOrPath": "Id","nullsPosition": "first",'+
                +'"sortDirection": "ascending"}],"query": "SELECT Name, BillingCity, DWH_Verticals__c, Total_Funding_Raised__c, NumberOfEmployees,'+
                +'DWH_Employee_Growth_6_Month__c, DSL_Activity__c, Next_Action__c, Next_Action_Date__c, Tags__c, Id, RecordTypeId, CreatedDate, LastModifiedDate,'+
                +'SystemModstamp FROM Account USING SCOPE mine WHERE RecordTypeId = \''+'012d0000000pNy2\''+' AND (Tags__c like \'MollyisCool;%\''+' OR Tags__c like \''+'%;MollyisCool;%\''+')'+
                +' ORDER BY Name ASC NULLS FIRST, Id ASC NULLS FIRST", "scope": "mine","sobjectType": "Account","whereCondition": '+ (IVPWatchDogMock.defaultRepsonse ? '{}' : '{'+conditions+'}') +'}';
        }
        else if(request.getEndpoint()=='callout:IVP_ADMIN_USER'){
            body='<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'+
                +'xmlns="urn:enterprise.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body>'+
                +'<loginResponse><result><metadataServerUrl>https://ivp--Dev2.cs19.my.salesforce.com/services/Soap/m/39.0/00D29000000E98e</metadataServerUrl>'+
                +'<passwordExpired>false</passwordExpired><sandbox>true</sandbox><serverUrl>'+
                +'https://ivp--Dev2.cs19.my.salesforce.com/services/Soap/c/39.0/00D29000000E98e</serverUrl><sessionId>'+
                +'00D29000000E98e!ARIAQCIGi_IT8QdBWawzfBaKULSEMoZC3FsFJHLRl9muPq2COBgzYMihLjgw.kFQMNwfTQpmIBqNmyd7BE.njEk2gle6p5pt'+
                +'</sessionId><userId>00529000001v49zAAA</userId><userInfo><accessibilityMode>false</accessibilityMode><currencySymbol>$</currencySymbol>'+
                +'<orgAttachmentFileSizeLimit>20971520</orgAttachmentFileSizeLimit><orgDefaultCurrencyIsoCode>USD</orgDefaultCurrencyIsoCode>'+
                +'<orgDefaultCurrencyLocale>en_US</orgDefaultCurrencyLocale><orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments>'+
                +'<orgHasPersonAccounts>false</orgHasPersonAccounts><organizationId>00D29000000E98eEAC</organizationId>'+
                +'<organizationMultiCurrency>false</organizationMultiCurrency> <organizationName>Insight Venture Partners</organizationName>'+
                +'<profileId>00ed00000012Ki5AAE</profileId><roleId>00Ed0000000wzE2EAI</roleId><sessionSecondsValid>14400</sessionSecondsValid>'+
                +'<userDefaultCurrencyIsoCode xsi:nil="true"/><userEmail>akhan@10kview.com</userEmail><userFullName>Amjad Khan</userFullName>'+
                +' <userId>00529000001v49zAAA</userId><userLanguage>en_US</userLanguage><userLocale>en_US</userLocale>'+
                +'<userName>akhan@ivp.com.dev2</userName><userTimeZone>Asia/Colombo</userTimeZone><userType>Standard</userType>'+
                +'<userUiSkin>Theme3</userUiSkin> </userInfo></result></loginResponse></soapenv:Body></soapenv:Envelope>';
        }
        response.setBody(body);
        response.setStatusCode(200);
        return response;
    }
}