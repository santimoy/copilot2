/**
 * @author Nazia Khanam
 * @date 2018-05-19
 * @className IVPCompanyServiceTest
 * @description : Is use to test the IVPCompanyService.
 */
@isTest
private class IVPCompanyServiceTest {
    private static testMethod void test1(){
        Id unassignedUserId = IVPUtils.unassignedUserId;
        Map<Id,Id> mapOfAccount=new Map<Id,Id>();
        IVPTestFuel fuel=new IVPTestFuel();
        
        List<user> testUsers = fuel.users;
        system.runAs(testUsers[1]){
            
            List<Account> testAccounts = IVPTestFuel.createTestCompanies(2, unassignedUserId);
            Test.startTest();
            for(Account account:testAccounts){
                mapOfAccount.put(account.Id, testUsers[1].Id);
            }
            IVPCompanyService.changeAccountOwner(mapOfAccount);
            Test.stopTest();
            
            List<Account> updatedTestCompanies = [SELECT Id, OwnerId FROM Account WHERE Id IN :mapOfAccount.keySet()];
            System.assertEquals(testUsers[1].Id, updatedTestCompanies[0].OwnerId);
            System.assertEquals(testUsers[1].Id, updatedTestCompanies[1].OwnerId);
        }
    }
}