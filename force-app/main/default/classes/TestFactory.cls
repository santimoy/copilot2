/*
	@description	Test Factory - Use for all test data creation
	@date 		 	12th July 2016
	@author  	 	Mike Gill
	@rev

	@todo

*/


@isTest
public class TestFactory {

	public static SObject createSObject(SObject sObj) {
		// Check what type of object we are creating and add any defaults that are needed.
		String objectName = String.valueOf(sObj.getSObjectType());
		// Construct the default values class. Salesforce doesn't allow '__' in class names
		String defaultClassName = 'TestFactory.' + objectName.replaceAll('__c|__', '') + 'Defaults';
		// If there is a class that exists for the default values, then use them
		if (Type.forName(defaultClassName) != null) {
			sObj = createSObject(sObj, defaultClassName);
		}
		return sObj;
	}

	public static SObject createSObject(SObject sObj, Boolean doInsert) {
		SObject retObject = createSObject(sObj);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName) {
		// Create an instance of the defaults class so we can get the Map of field defaults
		Type t = Type.forName(defaultClassName);
		if (t == null) {
			Throw new TestFactoryException('Invalid defaults class.');
		}
		FieldDefaults defaults = (FieldDefaults)t.newInstance();
		addFieldDefaults(sObj, defaults.getFieldDefaults());
		return sObj;
	}

	public static SObject createSObject(SObject sObj, String defaultClassName, Boolean doInsert) {
		SObject retObject = createSObject(sObj, defaultClassName);
		if (doInsert) {
			insert retObject;
		}
		return retObject;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
		return createSObjectList(sObj, numberOfObjects, (String)null);
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, (String)null);
		if (doInsert) {
			insert retList;
		}
		return retList;
	}

	public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, String defaultClassName, Boolean doInsert) {
		SObject[] retList = createSObjectList(sObj, numberOfObjects, defaultClassName);
		if (doInsert) {
			insert retList;
		}
		return retList;
	}

	public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
		SObject[] sObjs = new SObject[] {};
		SObject newObj;

		// Get one copy of the object
		if (defaultClassName == null) {
			newObj = createSObject(sObj);
		} else {
			newObj = createSObject(sObj, defaultClassName);
		}

		// Get the name field for the object
		String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
		if (nameField == null) {
			nameField = 'Name';
		}

		// Clone the object the number of times requested. Increment the name field so each record is unique
		for (Integer i = 0; i < numberOfObjects; i++) {
			SObject clonedSObj = newObj.clone(false, true);
			clonedSObj.put(nameField, (String)clonedSObj.get(nameField) + ' ' + i);
			sObjs.add(clonedSObj);
		}
		return sObjs;
	}

	private static void addFieldDefaults(SObject sObj, Map<String, Object> defaults) {
		// Loop through the map of fields and if they are null on the object, fill them.
		for (String field : defaults.keySet()) {
			if (sObj.get(field) == null) {
				sObj.put(field, defaults.get(field));
			}
		}
	}

	// When we create a list of SObjects, we need to
	private static Map<String, String> nameFieldMap = new Map<String, String> {
		'Contact' => 'LastName',
		'Case' => 'Subject'
	};

	public class TestFactoryException extends Exception {}

	// Use the FieldDefaults interface to set up values you want to default in for all objects.
	public interface FieldDefaults {
		Map<String, Object> getFieldDefaults();
	}

	// To specify defaults for objects, use the naming convention [ObjectName]Defaults.
	// For custom objects, omit the __c from the Object Name

	public class AccountDefaults implements FieldDefaults {
		public Map<String, Object> getFieldDefaults() {
			return new Map<String, Object> {
				'Name' => 'Test Account'
			};
		}
	}

	public class ContactDefaults implements FieldDefaults {
		public Map<String, Object> getFieldDefaults() {
			return new Map<String, Object> {
				'FirstName' => 'First',
				'LastName' => 'Last'
			};
		}
	}

	public class OpportunityDefaults implements FieldDefaults {
		public Map<String, Object> getFieldDefaults() {
			return new Map<String, Object> {
				'Name' => 'Test Opportunity',
				'StageName' => 'Closed Won',
				'CloseDate' => System.today()
			};
		}
	}

	public class CaseDefaults implements FieldDefaults {
		public Map<String, Object> getFieldDefaults() {
			return new Map<String, Object> {
				'Subject' => 'Test Case'
			};
		}
	}

	// Create Test Account Records
	public static List<Account> createAccounts( Integer numAccts )
	{
		List<Account> accts = new List<Account>();
		for( Integer i = 0; i < numAccts; i++ )
		{
			Account a = new Account( Name = 'TestAccount' + i );
			a.Date_of_Last_Call_Meeting__c = Date.today();

			accts.add( a );
		}

		insert accts;
		return accts;
	}

	// Create Test Contact Records
	public static List<Contact> createContacts( Integer numOfRecords, Id accountId, String lastName, Boolean doInsert )
	{
		List<Contact> contacts = new List<Contact>();
		for( Integer i = 0; i < numOfRecords; i++ )
		{
			Contact c = new Contact();
			c.AccountId = accountId;
			if( i > 0 )
				c.LastName = lastName + i;
			else
				c.LastName = lastName;

			contacts.add( c );
		}

		if( doInsert )
			insert contacts;

		return contacts;
	}

	// Create Company Lead Records Link the first 3 to Account Records
	public static List<CompanyLead__c> createCompanyLeads( Integer numAccts, List<Account> accts )
	{
		List<CompanyLead__c> cleads = new List<CompanyLead__c>();
		for( Integer i = 0; i < numAccts; i++ )
		{
			CompanyLead__c c = new CompanyLead__c();
			// Add the Company Lead Data.  This will be used to test the filtering
			c.Name = 'TestCLead' + i;
			c.Sh_CompanyName__c = 'TestCLead' + i;
			c.Mm_Employees__c = i + 10;
			c.Mm_EmployeesMoM6m__c = i + 10;
			c.Mm_Stage__c = 'pre series a';
			c.Mm_Location__c = 'united kingdom';
			c.Mm_Industries__c = 'Advertising';
			c.Mm_LastFundingDate__c = date.today();
			c.Mm_TotalFunding__c = 1000;
			// Add a matching company account ID for the first 3 records.
			if (i < 3)
				c.Sh_Account__c = accts[i].Id;

			cleads.add(c);
		}

		insert cleads;
		return cleads;
	}

	public static List<Task> createTasks( Integer numToCreate, Id whatId, String subType, Date activityDate, String status, Boolean doInsert )
	{
		List<Task> testTasks = new List<Task>();
		for( Integer i = 0; i < numToCreate; i++ )
		{
			Task testTask = new Task(
				OwnerId = UserInfo.getUserId(),
				WhatId = whatId,
				ActivityDate = activityDate,
				Subject = 'Test Subject ' + subType,
				Description = 'Test Subject ' + subType + ' Description',
				Status = status,
				TaskSubtype = subType
			);

			testTasks.add( testTask );
		}

		if( doInsert )
			insert testTasks;

		return testTasks;
	}
}