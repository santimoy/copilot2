/**
*@name      	: CampaignCompanyHandler
*@developer 	: 10K developer
*@date      	: 2017-09-21
*@description   : The class used as handler of CampaignCompany_Trgr
					Whenever a new CampaignCompany is insert/delete need to insert/delete the campaign and campaignmember

Reason for Commenting out code: Due to requirement of Opportunity Automation for CampaignMember, there is no need of CampaignCompany automation so commenting out the obsolete code!
Code commented out on : 25th Oct 2018
**/

public class CampaignCompanyHandler extends TriggerHandler {
    public CampaignCompanyHandler(){}
    //public static Boolean isExecuteFromOppAuto = false;
    /*
     * This method will be called after insert
     * This method will Create Child Campaign & Members from the newly created Campaign_Company__c.
    */
    /*public override void afterInsert(){
        if(!isExecuteFromOppAuto)
       		CampaignCompanyAutomation.insertChildCampaignsAndMembersFromParentCampaignWhenNewCompanyInserted(Trigger.new);
    }*/

    /*
     * This method will be called after delete 
     * This method will Delete Child Campaign & Members from the deleted Campaign_Company__c.
	*/
    /*public override void afterDelete(){
        if(!isExecuteFromOppAuto)
        	CampaignCompanyAutomation.deletedChildCampaignsAndMembersFromParentCampaignWhenNewCompanyDeleted(Trigger.old);
    }*/
    
}