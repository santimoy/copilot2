/************************************************************************ 
*  @author Vineet Bhagchandani
*  @date   27 Feburary 2019
*  @name   	AccountActivityUpdateBatch
*  @description Updates the fields on Account for Last Activity Date and Type
*			   if tasks are related to that account.
***********************************************************************/
global class AccountActivityUpdateBatch implements Database.Batchable<sObject> {

	
	// batch start method
    global Database.QueryLocator start(Database.BatchableContext BC) {
		string query = 'Select Id from Account';
		return Database.getQueryLocator(query);
	}
	
	// batch execute method
	global void execute(Database.BatchableContext BC, List<sObject> scope) {

	this.updateLastActivityTypeandDate((List<sObject>)scope);
	}
	
	// batch finish method
	global void finish(Database.BatchableContext BC) {
		
	}
	
	/*****
	 *  process all the accounts with ActivityHistory and update their
	 *  Last Activity Date/Time
	 * 	Last Activity Type
	 * 
		*****/ 
	private void updateLastActivityTypeandDate(List<sObject> scope) {
        
        Logger.push('updateLastActivityTypeandDate','AccountActivityUpdateBatch');
        //converting the list to set to pass the method for update
        try
        {
            set<Id> acctIds = (new Map<Id,sObject>(scope)).keySet();
            ActivityTriggerHandler.updateallActivitiesDateOnAccount(acctIds);
            //ActivityTriggerHandler.updateAccount_LastActivityType(acctIds);
        }
        catch(Exception e)
        {
            // log error into Log object on error
            Logger.debugException(e);
            Logger.pop();
            // throw Exception on error
            //throw new AccountLastActivityTypeException(e.getMessage());
        }
		
	}
	// Exception class: AccountLastActivityTypeException
	/*global class AccountLastActivityTypeException extends Exception {
	}*/
    
	
}