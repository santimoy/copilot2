@isTest
public class IntelligenceFeedbackIconCtrl_Test {
	@testSetup
    public static void dataSetup(){
        List<Intelligence_Field__c> intelligenceList = new List<Intelligence_Field__c>();
        Intelligence_Field__c if1 = new Intelligence_Field__c();
        if1.UI_Label__c = 'Company Name';
        if1.DB_Field_Name__c = 'Test';
        intelligenceList.add(if1); 
        Intelligence_Field__c if2 = new Intelligence_Field__c();
        if2.UI_Label__c = 'Company Name';
        if2.DB_Field_Name__c = 'Test';
        if2.UI_Label__c = 'test';
        if2.Active__c = true;
        intelligenceList.add(if2);  
        insert intelligenceList;
    }
    
    @isTest
    public static void test1(){
        IntelligenceFeedbackIconCtrl.logAjax(JSON.serialize(new Map<String,Object>{'url'=>'','body'=>'','params'=>''}));
        IntelligenceFeedbackIconCtrl.FeedbackData fData = IntelligenceFeedbackIconCtrl.getFeedbackData();
        system.assertNotEquals(fData, null);
    }
}