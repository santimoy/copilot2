public with sharing class M2M_LinkController {
	public static final String PARAM_LINKCONFIGNAME = 'lcn';
	public static final String PARAM_RETURL = 'returl';
	public static final String PARAM_FROMID = 'fid';
	public static final String PARAM_TOID = 'newid';
	
	private String linkConfigName;
	private String retUrl;
	public String fromId{get; private set;}
	private String toId;
	
	private M2M_Config__c currLinkConfig;
	private Schema.sObjectType sObjToken;
	private String linkObjPrefix;
	
	// properties
	public string replaceUrl{get;private set;}
	public boolean errorOccurred{get;private set;}

	public M2M_LinkController(){
		map<String, String> params = ApexPages.currentPage().getParameters(); 
		linkConfigName = params.get(PARAM_LINKCONFIGNAME);
		retUrl = params.get(PARAM_RETURL);
		fromId = params.get(PARAM_FROMID);
		toId = params.get(PARAM_TOID);
		
		if (linkConfigName!=null && linkConfigName!='') currLinkConfig = M2M_Config__c.getInstance(linkConfigName);
		sObjToken = Schema.getGlobalDescribe().get(currLinkConfig.LinkObjectName__c);
		linkObjPrefix = sObjToken.getDescribe().getKeyPrefix();
	}
	
	public PageReference getRedirect(){
		errorOccurred = false;
		if (currLinkConfig.LinkObjectHasOtherData__c){
			// link object has more data for the user to fill in.  generate proper prefill url
			// Loan -> Contact
			// Contact -> Loan
			PageReference newPrefillPR = new PageReference('/'+linkObjPrefix+'/e');
			map<string,string> params = newPrefillPR.getParameters();
			params.put('CF'+currLinkConfig.LinkFromFieldId__c+'_lkid',fromId);
			params.put('CF'+currLinkConfig.LinkFromFieldId__c, getSObjectName(fromId, currLinkConfig.LinkFromObjectName__c));
			params.put('CF'+currLinkConfig.LinkToFieldId__c+'_lkid',toId);
			params.put('CF'+currLinkConfig.LinkToFieldId__c, getSObjectName(toId, currLinkConfig.LinkToObjectName__c));
			
			params.put('saveURL',retUrl);
			params.put('retURL',retUrl);
			
			replaceUrl = newPrefillPR.getUrl();
		}
		else{
			// link object is just 2 links, create the object and redirect back to return url
			sObject linkRec = createManyToManyObject();
			linkRec.put(currLinkConfig.LinkToField__c, toId);
			linkRec.put(currLinkConfig.LinkFromField__c, fromId);
			System.debug('linkRec:'+linkRec);
			try{
				insert linkRec;
			}
			catch(DMLException e){
				if (e.getDmlType(0) == System.StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.M2M_Error_NoAccess));
					errorOccurred = true;
				}
				else{
					throw e;
				}
			}
			replaceUrl = retUrl;
		}
		return null;
	}
	
	
	// if we could dynamically create a record, this would be totally modular.  instead we'll have to simply subclass this when needed
	public virtual sObject createManyToManyObject(){
		return sObjToken.newSObject();
	}

	private string getSObjectName(string sObjectID, string sObjectName){
		list<sObject> sObjList;
		string retName = ''; 
		
		string soqlQuery = 'select Name from '+sObjectName+' where id =:sObjectID'; 
		System.Debug('soqlQuery:'+soqlQuery);
		System.Debug('sObjectID:'+sObjectID);
		sObjList = Database.query(soqlQuery);
		retName = (string)((sObject)sObjList.get(0)).get('Name');
		return retName;
	}

	// Test methods ----------------------------------------------------
	public static testMethod void testThisClass(){
		
		M2M_Config__c testLinkConfig1 = new M2M_Config__c(Name='testLinkConfig1', 
			LinkObjectName__c='CompanyCompetitor__c', 
			LinkFromField__c='Company__c', LinkToField__c='Competitor__c',
			LinkObjectHasOtherData__c=false);

		M2M_Config__c testLinkConfig2 = new M2M_Config__c(Name='testLinkConfig2', 
			LinkObjectName__c='CompanyCompetitor__c', 
			LinkFromField__c='Company__c', LinkToField__c='Competitor__c',
			LinkObjectHasOtherData__c=true, LinkFromFieldId__c='00NK0000000iYG1', LinkFromObjectName__c='Account', LinkToFieldId__c='00NK0000000iYpQ', LinkToObjectName__c = 'Account');

		insert new M2M_Config__c[]{testLinkConfig1, testLinkConfig2};
		

		Account testCompany = new Account(name='testCompany');
		insert testCompany;
		
		Account testCompetitor = new Account(name='testCompetitor');		
		insert testCompetitor;
		
		// call the auto create portion of the code.....
		PageReference testPageReference1 = Page.M2M_Link;
		map<string, string> params = testPageReference1.getParameters();
		params.put(M2M_LinkController.PARAM_LINKCONFIGNAME, 'testLinkConfig1');
		params.put(M2M_LinkController.PARAM_FROMID, testCompany.Id);
		params.put(M2M_LinkController.PARAM_RETURL, '/'+testCompany.Id);
		params.put(M2M_LinkController.PARAM_TOID, testCompetitor.Id);
		
		Test.setCurrentPage(testPageReference1);
		M2M_LinkController testController = new M2M_LinkController();
		
		System.Debug('testController.fromId:'+testController.fromId);
		
		// call the link operation
		testController.getRedirect();
		
		List<CompanyCompetitor__c> resultList = [select id from CompanyCompetitor__c 
			where Competitor__c = :testCompetitor.id and Company__c = :testCompany.id];		

		System.assertEquals(1 ,resultList.size());
		System.assertEquals('/'+testCompany.Id ,testController.replaceUrl);

		// call the auto prefill portion of the code.....
		PageReference testPageReference2 = Page.M2M_Link;
		params = testPageReference2.getParameters();
		params.put(M2M_LinkController.PARAM_LINKCONFIGNAME, 'testLinkConfig2');
		params.put(M2M_LinkController.PARAM_FROMID, testCompany.Id);
		params.put(M2M_LinkController.PARAM_RETURL, '/'+testCompany.Id);
		params.put(M2M_LinkController.PARAM_TOID, testCompetitor.Id);
		
		Test.setCurrentPage(testPageReference2);
		testController = new M2M_LinkController();
		
		// call the link operation
		testController.getRedirect();
		string prefix = Schema.getGlobalDescribe().get(testLinkConfig2.LinkObjectName__c).getDescribe().getKeyPrefix();
		System.assert(testController.replaceUrl.startsWith('/'+prefix+'/e'));
		/**/
	}

}