@isTest
public class SourcingPreferenceTriggerHandler_test {
    
    @TestSetup
    static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@ivptest.com');
        insert u1;
        
        Intelligence_User_Preferences__c  intUserRec = new Intelligence_User_Preferences__c ();
        intUserRec.CreatedById = u1.Id;
        intUserRec.Display_Columns__c = 'Name (ivp_name);City (ivp_city);Country (ivp_country);Intent 3M Growth % (intent_to_buy_score_3m_growth)';
        intUserRec.Sourcing_Signal_Category_Preferences__c = 'acquisition-acquirer, competitive-challenge, Customer, employee-growth, form-10k';
        intUserRec.My_Dash_Signal_Preferences__c = 'cquisition-acquirer, acquisition-acquiree, award, cb-new-company, ceo-change, competitive-challenge';
        intUserRec.User_Id__c = string.valueOf(u1.id) ;
        insert intUserRec;
    }
    
    public static testmethod void Method1()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            List<Signal_Category__c> insertList = new List<Signal_Category__c>();
            for(Integer i =0; i< 10; i++ ){
                Signal_Category__c sigCatList = new Signal_Category__c();
                sigCatList.Name =  'Test '+i;
                sigCatList.External_Id__c = 'Test '+i;
                insertList.add(sigCatList);
            }
            insert insertList;
            
            test.startTest();
            
            test.stopTest();
        }
    }
    
    public static testmethod void Method2()
 {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            List<Signal_Category__c> insertList = new List<Signal_Category__c>();
            for(Integer i =0; i< 10; i++ ){
                Signal_Category__c sigCatList = new Signal_Category__c();
                sigCatList.Name =  'Test '+i;
                sigCatList.External_Id__c = 'Test '+i;
                insertList.add(sigCatList);
            }
            insert insertList;
            
            test.startTest();
            update insertList;
            test.stopTest();
        }
    }
}