//
// 3-30-2018 TO BE DEPRECATED
// 3-30-2018 - removed references to RKCB__ fields in order to remove the installed managed package
//
global class Crunchbase_ScheduledBatchProcess implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{

	// ----------------------------------------------------------------------------------------------------------------------------------
	// batchable interface methods
	global Database.QueryLocator start(Database.BatchableContext BC){
		//String queryString = 'Select Id, Name, RKCB__Crunchbase_Permalink__c From Account where RKCB__Crunchbase_Permalink__c != null'; 
		String queryString = 'Select Id, Name From Account'; 
		
		System.Debug('queryString:'+queryString);
		return Database.getQueryLocator(queryString);	
	}

	global void execute(Database.BatchableContext BC, List<Account> batchList){
		Crunchbase_Connector cbConnect = new Crunchbase_Connector(); 

		list<Crunchbase_Funding_Data__c> upsertList = new list<Crunchbase_Funding_Data__c>();
		list<Account> updateList = new list<Account>();
		
		for (Account currAcct : batchList){
			//Crunchbase_Data.cbCompany cbCompany = cbConnect.getCbData(currAcct.RKCB__Crunchbase_Permalink__c);
			Crunchbase_Data.cbCompany cbCompany = cbConnect.getCbData('http://to_be_deprecated.com/');
			
			if (cbCompany.acquisition!=null){
				currAcct.CB_Acquisition_Currency_Code__c = cbCompany.acquisition.price_currency_code;
				currAcct.CB_Acquisition_Amount__c = cbCompany.acquisition.price_amount; 
				currAcct.CB_Acquired_Date__c = cbCompany.acquisition.buildDate();
				if (cbCompany.acquisition.acquiring_company!=null){
					currAcct.CB_Acquired_By__c = cbCompany.acquisition.acquiring_company.name;
				}
				updateList.add(currAcct);
			}

						
			list<Crunchbase_Data.cbFundingRound> fundingData = cbCompany.funding_rounds; 
			if (fundingData!=null){
				for (Crunchbase_Data.cbFundingRound currFunding : fundingData){
					Crunchbase_Funding_Data__c sfFunding = currFunding.toSfFundingData(currAcct);
					upsertList.add(sfFunding);
				}
			}
		}
		
		if (updateList.size()>0) update updateList;
		if (upsertList.size()>0) upsert upsertList Batch_UniqueId__c;
		/**/
	}

	global void finish(Database.BatchableContext BC){
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// methods needed for the Schedulable interface
	global void execute(SchedulableContext sc){
		Database.executebatch(this, 10); // this is the max number of webservice calls per batch
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// test classes
	static testMethod void testThisClass(){
		Crunchbase_Connector.testResponse = '{"name": "Punchey","permalink": "punchey","crunchbase_url": "http://www.crunchbase.com/company/punchey","homepage_url": "http://www.punchey.com","blog_url": "","blog_feed_url": "","twitter_username": "puncheyinc","category_code": null,"number_of_employees": null,"founded_year": 2011,"founded_month": null,"founded_day": null,"deadpooled_year": null,"deadpooled_month": null,"deadpooled_day": null,"deadpooled_url": null,"tag_list": "","alias_list": "","email_address": "info@punchey.com","phone_number": "800Punchey","description": "","created_at": "Fri Aug 17 12:46:10 UTC 2012","updated_at": "Sat Aug 18 01:20:40 UTC 2012","overview": "Punchey is the best way for local merchants to get discovered and accept payments. \\n\\nThe company provides an end-to-end payment processing and marketing solution for small and medium-sized merchants. Get paid wherever, whenever and bring more customers in with Punchey Mobile and Punchey Countertop. ","image":{"available_sizes":[[[150,45],"assets/images/resized/0020/6573/206573v1-max-150x150.jpg"],[[193,59],"assets/images/resized/0020/6573/206573v1-max-250x250.jpg"],[[193,59],"assets/images/resized/0020/6573/206573v1-max-450x450.jpg"]],"attribution": null},"products":[],"relationships":[{"is_past": false,"title": "CEO Founder","person":{"first_name": "Nathaniel","last_name": "Stevens","permalink": "nathaniel-stevens","image":{"available_sizes":[[[90,150],"assets/images/resized/0001/0294/10294v1-max-150x150.jpg"],[[120,200],"assets/images/resized/0001/0294/10294v1-max-250x250.jpg"],[[120,200],"assets/images/resized/0001/0294/10294v1-max-450x450.jpg"]],"attribution": null}}},{"is_past": false,"title": "Director of Business Development ","person":{"first_name": "Yuliya","last_name": "Sychikova","permalink": "yuliya-sychikova-2","image": null}}],"competitions":[],"providerships":[],"total_money_raised": "$1.7M","funding_rounds":[{"round_code": "unattributed","source_url": "http://www.finsmes.com/2012/08/punchey-raises-1-7m.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+finsmes%2FcNHu+%28FinSMEs%29","source_description": "Punchey Raises $1.7M","raised_amount": 1700000.0,"raised_currency_code": "USD","funded_year": 2012,"funded_month": 8,"funded_day": 17,"investments":[{"company": null,"financial_org":{"name": "Stevens Ventures","permalink": "stevens-ventures","image":{"available_sizes":[[[150,74],"assets/images/resized/0020/6576/206576v1-max-150x150.jpg"],[[250,123],"assets/images/resized/0020/6576/206576v1-max-250x250.jpg"],[[305,151],"assets/images/resized/0020/6576/206576v1-max-450x450.jpg"]],"attribution": null}},"person": null}]}],"investments":[],"acquisition":{"price_amount": 170000000.0,"price_currency_code": "USD","term_code": null,"source_url": "http://www.techcrunch.com/2009/09/14/the-value-of-techcrunch50-mint-acquired-by-intuit-for-170m-two-years-after-winning-tc40/","source_description": "The Value of TechCrunch50: Mint Acquired by Intuit for $170m Two Years After Winning TC40.","acquired_year": 2009,"acquired_month": 9,"acquired_day": null,"acquiring_company":{"name": "Intuit","permalink": "intuit","image":{"available_sizes":[[[150,65],"assets/images/resized/0001/6183/16183v3-max-150x150.png"],[[250,109],"assets/images/resized/0001/6183/16183v3-max-250x250.png"],[[450,196],"assets/images/resized/0001/6183/16183v3-max-450x450.png"]],"attribution": null}}},"acquisitions":[],"offices":[{"description": "","address1": "31 State Street","address2": "","zip_code": "02109","city": "Boston","state_code": "MA","country_code": "USA","latitude": null,"longitude": null}],"milestones":[],"ipo": null,"video_embeds":[],"screenshots":[{"available_sizes":[[[150,109],"assets/images/resized/0020/6574/206574v1-max-150x150.jpg"],[[250,183],"assets/images/resized/0020/6574/206574v1-max-250x250.jpg"],[[450,329],"assets/images/resized/0020/6574/206574v1-max-450x450.jpg"]],"attribution": null}],"external_links":[]}';

		//Account testAcct = new Account(Name='Punchey', RKCB__Crunchbase_Permalink__c='http://api.crunchbase.com/v/1/company/punchey.js');
		Account testAcct = new Account(Name='Punchey');
		insert testAcct;		
		
		Crunchbase_ScheduledBatchProcess testClass = new Crunchbase_ScheduledBatchProcess();
		Test.startTest();
		SchedulableContext sc = null;
		testClass.execute(sc);
		Test.stopTest();
		
		list<Crunchbase_Funding_Data__c> fundingList = [select Id From Crunchbase_Funding_Data__c where Account__c = :testAcct.Id];
		System.assertEquals(1, fundingList.size());
	}
}