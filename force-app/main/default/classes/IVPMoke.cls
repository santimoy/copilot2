@isTest
public class IVPMoke  implements HttpCalloutMock {
	
	public HTTPResponse respond(HTTPRequest req) {
         HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setHeader('SOAPAction', 'dummy');
        System.debug(UserInfo.getOrganizationId());
        String s = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">';
        s+='<Body>';
        s+='<login xmlns="urn:enterprise.soap.sforce.com">';
        //s+='<username>akhan@ivp.com.dev2</username>';
        //s+='<password>c0ncret10xOYXINs91LjjjEza4FaOf537E</password>';
        s+='<username>{!$Credential.UserName}</username>';
        s+='<password>{!$Credential.Password}</password>';
        s+='</login>';
        s+='</Body>';
        s+='</Envelope>';                
        res.setBody(s);
       
        // Send the request, and return a response
        return res;
    }
}