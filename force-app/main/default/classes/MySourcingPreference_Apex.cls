public with sharing class MySourcingPreference_Apex {
    @AuraEnabled
    public static List<Sourcing_Preference__c> MySourcingPreferencesRecords_Apex() {
        List<Intelligence_User_Preferences__c> uPres = [
                SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c, User_Id__c
                FROM Intelligence_User_Preferences__c
                WHERE User_Id__c = :UserInfo.getUserId()
        ];
        if(uPres.isEmpty()) {
            List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
            SignalCategoryList = [SELECT External_Id__c FROM Signal_Category__c
            WHERE External_Id__c != null];
            String sourcingSignalStr;
            if(! SignalCategoryList.isEmpty()) {
                for (Signal_Category__c singleCategory : SignalCategoryList) {
                    if (singleCategory.External_Id__c != null) {
                        if (sourcingSignalStr != null) {
                            sourcingSignalStr = sourcingSignalStr + ',' + singleCategory.External_Id__c;
                        } else {
                            sourcingSignalStr = singleCategory.External_Id__c;
                        }
                    }
                }
            }
            Intelligence_User_Preferences__c IntelligenceUser = new Intelligence_User_Preferences__c();
            IntelligenceUser.Name = UserInfo.getName();
            IntelligenceUser.User_Id__c = UserInfo.getUserId();
            if(sourcingSignalStr != null){
                IntelligenceUser.Sourcing_Signal_Category_Preferences__c = sourcingSignalStr;
                IntelligenceUser.My_Dash_Signal_Preferences__c = sourcingSignalStr;
            }
            insert IntelligenceUser;
            List<Intelligence_User_Preferences__c> uPresNewUser = [
                    SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c
                    FROM Intelligence_User_Preferences__c
                    WHERE User_Id__c = :UserInfo.getUserId()
            ];
        } else {
            if (String.isEmpty(uPres[0].My_Dash_Signal_Preferences__c) || String.isEmpty(uPres[0].Sourcing_Signal_Category_Preferences__c)) {
                List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
                SignalCategoryList = [SELECT External_Id__c FROM Signal_Category__c
                WHERE External_Id__c != null];
                String sourcingSignalStr;
                if(! SignalCategoryList.isEmpty()) {
                    for (Signal_Category__c singleCategory : SignalCategoryList) {
                        if (singleCategory.External_Id__c != null) {
                            if (sourcingSignalStr != null) {
                                sourcingSignalStr = sourcingSignalStr + ',' + singleCategory.External_Id__c;
                            } else {
                                sourcingSignalStr = singleCategory.External_Id__c;
                            }
                        }
                    }
                }
                if(sourcingSignalStr != null){
                    Intelligence_User_Preferences__c IntelligenceUser = new Intelligence_User_Preferences__c();
                    IntelligenceUser.Id = uPres[0].Id;
                    IntelligenceUser.User_Id__c = uPres[0].User_Id__c;
                    if (String.isEmpty(uPres[0].My_Dash_Signal_Preferences__c)) {
                        IntelligenceUser.My_Dash_Signal_Preferences__c = sourcingSignalStr;
                    } else {
                        IntelligenceUser.My_Dash_Signal_Preferences__c = uPres[0].My_Dash_Signal_Preferences__c;
                    }
                    if (String.isEmpty(uPres[0].Sourcing_Signal_Category_Preferences__c)) {
                        IntelligenceUser.Sourcing_Signal_Category_Preferences__c = sourcingSignalStr;
                    } else {
                        IntelligenceUser.Sourcing_Signal_Category_Preferences__c = uPres[0].Sourcing_Signal_Category_Preferences__c;
                    }
                    upsert IntelligenceUser;
                }
            }
        }

        String customLabelGroupValue = System.Label.IVP_Sourcing_Preference_Order_By;
        String orderSetString = 'ORDER BY LastModifiedDate DESC';
        if(customLabelGroupValue != null) {
            orderSetString = customLabelGroupValue;
        }
        List<Sourcing_Preference__c> PreferencesRecordsList = new List<Sourcing_Preference__c>();
        String userIdString = UserInfo.getUserId();
        String  queryString = 'SELECT Id, Name, Name__c, Include_Exclude__c, CreatedDate, LastModifiedDate, Status__c, Validation_Status__c, External_Id__c, Preference_Status__c'
                +' FROM Sourcing_Preference__c WHERE OwnerId = \''+userIdString + '\' AND Status__c != \'Archived\''
                +' ORDER BY '+orderSetString+' '
                +' LIMIT 10000';
        PreferencesRecordsList =  Database.query(queryString);
        return PreferencesRecordsList;
    }

    @AuraEnabled
    public static Sourcing_Preference__c getCurrentRecordDetails_Apex(String newRecordId) {
        Sourcing_Preference__c PreferencesRecords = new Sourcing_Preference__c();
        // getting result in the query, as if record not found so we should not get Exception
        List<Sourcing_Preference__c> sps = [
                SELECT Id, Name,Name__c, Include_Exclude__c, CreatedDate, LastModifiedDate, Status__c, Validation_Status__c,External_Id__c
                FROM Sourcing_Preference__c
                WHERE Id =: newRecordId
        ];
        if(!sps.isEmpty()){
            PreferencesRecords = sps[0];
        }

        return PreferencesRecords;
    }

    @AuraEnabled
    public static String DeletePreferenceDetail_Apex(String preferenceRecordId) {
        try {
            if (preferenceRecordId != null) {
                List<Sourcing_Preference__c> PreferenceList = [SELECT Id FROM Sourcing_Preference__c WHERE Id = :preferenceRecordId];
                if (PreferenceList.size() > 0) {
                    delete PreferenceList;
                    return 'Success';
                } else {
                    return 'Fail';
                }
            }
            return null;
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR,'Error Message>>>>: ' + ex.getMessage() + 'Line Number>>>>: ' + ex.getLineNumber());
            return null;
        }
    }

    @auraEnabled
    public static boolean saveSourcingPreference(String recSave, String action){
        try {
            if(recSave != null) {
                Sourcing_Preference__c SourcingPreference = [Select Id, Name, OwnerId, External_Id__c, Include_Exclude__c,Advanced_Mode__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, Name__c,
                                                             Owner_Username__c,Recent_Validation_Error__c,  SQL_Query__c, SQL_Where_Clause__c, Serialized__c, Status__c, Validation_Status__c,
                                                             Cloned_From__c From Sourcing_Preference__c Where Id=: recSave Limit 1];
                if(action == 'Archive'){
                    action = 'Archived';
                }else if(action == 'true'){
                    action = 'Active';
                }else if(action == 'false'){
                    action = 'Inactive';
                }
                SourcingPreference.Status__c = action;
                SourcingPreference.Sent_to_DWH__c = true;
                SourcingPreference = QueryBuilder.makeCallout(SourcingPreference);
                update SourcingPreference;
                return true;
            }
            return false;
        }catch (Exception ex) {
            throw new AuraHandledException('Error Message>>>>: ' + ex.getMessage() + 'Line Number>>>>: ' + ex.getLineNumber());
        }
        
    }
}