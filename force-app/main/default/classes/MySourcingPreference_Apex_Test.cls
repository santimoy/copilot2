@isTest
private class MySourcingPreference_Apex_Test {

  @isTest private static void testMySourcingPreferencesRecords_Apex() {
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        MySourcingPreference_Apex.MySourcingPreferencesRecords_Apex();
        List<Intelligence_User_Preferences__c> uPres = [
                SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c, User_Id__c
                FROM Intelligence_User_Preferences__c
                WHERE User_Id__c = :UserInfo.getUserId()
        ];
        System.assert(uPres[0].Sourcing_Signal_Category_Preferences__c!=null);
        System.assert(uPres[0].My_Dash_Signal_Preferences__c!=null);
        uPres[0].Sourcing_Signal_Category_Preferences__c=null;
        
        update uPres;
        MySourcingPreference_Apex.MySourcingPreferencesRecords_Apex();
        uPres = [
                SELECT Id, WD_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c, User_Id__c
                FROM Intelligence_User_Preferences__c
                WHERE User_Id__c = :UserInfo.getUserId()
        ];
        System.assert(uPres[0].Sourcing_Signal_Category_Preferences__c!=null);
        System.assert(uPres[0].My_Dash_Signal_Preferences__c!=null);
  }
    
    @isTest private static void testgetCurrentRecordDetails_Apex() {
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Sourcing_Preference__c> sourcingPreferences = tFuel.sourcingPreferences;
        Sourcing_Preference__c sp = MySourcingPreference_Apex.getCurrentRecordDetails_Apex(sourcingPreferences[0].Id);
        System.assertEquals(sourcingPreferences[0].Id, sp.Id);
  }
  
  @isTest private static void testDeletePreferenceDetail_Apex() {
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Sourcing_Preference__c> sourcingPreferences = tFuel.sourcingPreferences;
        String result = MySourcingPreference_Apex.DeletePreferenceDetail_Apex(sourcingPreferences[0].Id);
        System.assertEquals('success', result.toLowerCase());
  }
  
  @isTest private static void testsaveSourcingPreference() {
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Sourcing_Preference__c> sourcingPreferences = tFuel.sourcingPreferences;
        Boolean result = MySourcingPreference_Apex.saveSourcingPreference(sourcingPreferences[0].Id, 'true');
        System.assert(result);
  }

}