/**
  
* @date Created  April 10 2018
* @description Test class for Intelligence_FieldTriggerHandler class.
*/

@isTest(SeeAllData=false)

private class Intelligence_FieldTriggerHandler_Test {
    
    private static void initData() {
        
        list<Intelligence_Field__c>   intelligenceFields = new list<Intelligence_Field__c>();
        
        Intelligence_Field__c  ieField = new Intelligence_Field__c();
        ieField.DB_Column__c           = 'ct.companies_cross_data_full.mattermark_state';
        ieField.Filterable_from_UI__c  = true;
        ieField.Hide_Values__c         = false;

        ieField.Order__c               = 1 ;
        ieField.Static_Selection__c    = true;
        ieField.Target_Field__c        = 'BillingStateCode';
        ieField.Target_Object__c       = 'Account';
        ieField.UI_Label__c            = 'State';
        intelligenceFields.add(ieField);
        
        Intelligence_Field__c ieField1 = new Intelligence_Field__c();
        ieField1.DB_Column__c           = 'ct.companies_cross_data_full.name';
        ieField1.Filterable_from_UI__c  =  false;
        ieField1.Hide_Values__c         =  true;

        ieField1.Order__c               = 0;
        ieField1.Static_Selection__c    = true;
        ieField1.Target_Field__c        = 'Name';
        ieField1.Target_Object__c       = 'Account';
        ieField1.UI_Label__c            = 'Company Name';
        intelligenceFields.add(ieField1);
        
        insert intelligenceFields;
    }
    
    public static testMethod void testIntelligenceFields() {
         initData();
         
         list<Intelligence_Field__c>   intelligenceFields = [Select id,Target_Field__c from Intelligence_Field__c limit 2];
         
         system.assertEquals(2,intelligenceFields.size());
         
         Intelligence_Field__c ieField = intelligenceFields.get(0);
         
         ieField.Target_Field__c = 'Fake Field API Name';
         
         try{
            update ieField;
            intelligenceFields = [Select id,Hide_Values__c from Intelligence_Field__c where Hide_Values__c = false];
            system.assertEquals(2,intelligenceFields.size());
            Intelligence_Field__c ieField1 = intelligenceFields.get(0);
            ieField1.Hide_Values__c = true;
            update intelligenceFields;
            
         }catch(Exception ex){
            String errorMessage = ex.getMessage();
            system.assertEquals(false,errorMessage.contains('Mapping Missmapped'));
         }
          
    }
    
}