@isTest
public class BulkSearchControllerTest{

	@isTest public static void testController(){
		Account acct = new Account(Name='testacct');
    	acct.Website='http://saasbydesign.com';
 		acct.OwnerId=UserInfo.getUserId();

    	insert acct;
    	Id[] fixedSearchResults= new Id[1];
       	fixedSearchResults[0] = acct.Id;
       	Test.setFixedSearchResults(fixedSearchResults);

     	
        BulkSearchController ctrl = new BulkSearchController();
        ctrl.searchValues='testacct';
        ctrl.currentSearch='testacct';
        ctrl.firstTimeSearch=true;
        ctrl.rawIdList=acct.Id;
        ctrl.newSearch();

        system.assertEquals(1,ctrl.foundAccounts.size(),'Did not find the required account');

         ctrl.assignSelected();
	}

}