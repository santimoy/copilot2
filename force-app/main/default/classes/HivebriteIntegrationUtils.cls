/**
 * @File Name          : HivebriteIntegrationUtils.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 6:26:02 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/10/2019   Anup Kage     Initial Version
**/
public with sharing class HivebriteIntegrationUtils {
    
    /**
    @param SObjectList : trigger records 
     */
    private static String jobType;
    
    public static String lOG_LEVEL{get{
        if(lOG_LEVEL == null){
            Integration_API_Settings__c userSetting = Integration_API_Settings__c.getOrgDefaults();
            if(userSetting.Event_Log_Level__c != null){
                lOG_LEVEL = userSetting.Event_Log_Level__c.toLowerCase();
            }else{
                lOG_LEVEL = 'error';
            }
        }
        return lOG_LEVEL;
    }set;}
    /**
    Query base url object
     */
    public static Integration_API__mdt apiBaseURL{get{
        if(ApiBaseURL == null){
            ApiBaseURL = [  SELECT ID, Base_URL__c 
                            FROM Integration_API__mdt 
                            WHERE Label = 'Hivebrite API'  limit 1 ]; //Integration_API
        }
        return ApiBaseURL;
    }set;}    
    
    // public static void hivebriteCompanyIntegration(List<SObject> recordList){
    //     // HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper(/*'Portfolio-Company', 'Hivebrite API', recordList */);
    //     // objHelper.companyRecordHandler(recordList);
    //     // hivebriteIntegrationTrigger();
    // }
    public static void hivebriteIntegrationTrigger(){
        Integration_API_Settings__c userSetting = Integration_API_Settings__c.getOrgDefaults();
        if(!userSetting.Integration_Is_Enabled__c){
            List<SObject> recordList = Trigger.new;
            if(userSetting.Event_Log_Level__c	!= null && ( userSetting.Event_Log_Level__c.equalsIgnoreCase('INFO') || userSetting.Event_Log_Level__c.equalsIgnoreCase('debug'))){
                String ErrorMessage =' Sync records to Hivebrite from Trigger.<BR/>';
                if(recordList != null && !recordList.isEmpty()){
                    Map<Id, SObject> objectById = new Map<Id, SObject>(recordList);
                    ErrorMessage += ' The following Records were not synced to Hivebrite: '+ String.join(new List<Id>(objectById.keySet()), ' ,');
                }
                EventLog.generateLog('HivebriteIntegrationQueueable', 'hivebriteIntegrationTrigger' , 'INFO', 'Integration is disabled',ErrorMessage);
                EventLog.commitLogs();
            }
            return;
        }

        SObjectType objectName = trigger.isDelete ? trigger.old.getSObjectType() : trigger.new.getSObjectType();
        if(objectName == Account.sObjectType){
            preFilterTheRecords('Account');
            insertContactsOnAccount();
           
        }else if(objectName == Contact.sObjectType){
            preFilterTheRecords('Contact'); 
        } 
        EventLog.commitLogs();   
    }
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param objectName 
    * @return void 
    **/
    private static void preFilterTheRecords( String objectName ){
    List<Integration_API_Object__mdt> apiObjectList = new List<Integration_API_Object__mdt>();
        Map<String, List<sobject>> recordListByJobtype = new Map<String, List<sobject>>();
        //!BUG: ORDER BY Order__c ASC  is commented because its not querying all records in hivebrite Org. if matching rule code does not work properly then un comment that part. 
        //! on uncommenting  if you get the issue in  FullCopy sandbox create case to Salesforce on SOQL inner query limiting the records.
        for(Integration_API_Object__mdt obj : [ SELECT Id, Integration_API__c, Object_API_Name__c,  
                                                Job_Type__c, Filter_Logic__c, 
                                                (SELECT Id, Field__c, Operator__c, Order__c, Value__c 
                                                FROM Integration_API_Rules__r 
                                                /*ORDER BY Order__c ASC */),
                                                (SELECT ID, Data_Type__c,Field_Mapping_Type__c, default_value__c, Required_Field__c, Integration_Field_Name__c, Precision__c, Size__c, Field_API_Name__c, External_Id__c
                                                FROM Integration_API_Fields__r 
                                                WHERE Field_Mapping_Type__c <> 'Inbound' /* AND Field_API_Name__c <> null */
                                                /*ORDER BY Required_Field__c ASC */)                                                
                                                FROM Integration_API_Object__mdt
                                                WHERE Object_API_Name__c =: objectName AND Job_Type__c <> null ])
        {
            

            apiObjectList.add(obj);
            recordListByJobtype.put(obj.Job_Type__c, new List<SObject>());
        }
        Map<Id,Set<String>> errorMessageById = new Map<Id, Set<String>>();
        List<SObject> recordList = Trigger.new;
        // loop through the records and de
        for(SObject objAcc : recordList){ 
            for(Integration_API_Object__mdt apiObj : apiObjectList){
                String filterLogic = apiObj.Filter_Logic__c;
                 // Check for value change
                Boolean isValuesChanged = false;
                Boolean dontAddThisRecord = false;
                Boolean isRuleValuesChanged = false;

                if(String.isNotBlank(filterLogic)){
                    for( Integration_API_Rule__mdt rule : apiObj.Integration_API_Rules__r){  
                        // check rule Values are chnaged or not  
                        if(Trigger.isUpdate){  
                            isRuleValuesChanged = isChanged(rule.Field__c, objAcc) ? true : isRuleValuesChanged;  
                        }          
                        filterLogic = filterLogic.replaceFirst(String.valueOf(Integer.valueOf(rule.Order__c)), validateCondition(objAcc, rule, apiObj.Object_API_Name__c ));
                    }
                }
               

                if(Trigger.isExecuting ){
                    for(Integration_API_Field__mdt objFld : apiObj.Integration_API_Fields__r){
                        
                        if(!objFld.External_Id__c){
                            if(Trigger.isUpdate){
                                isValuesChanged = isChanged(objFld.Field_API_Name__c, objAcc) ? true : isValuesChanged;
                                if(isChanged(objFld.Field_API_Name__c, objAcc)){
                                    System.debug('ERROR----'+apiObj.Job_Type__c+'------------>'+objFld.Field_API_Name__c);
                                }
                            }
                            // if required field  value is null then will not do callout for this record.
                            if(objFld.Required_Field__c && objFld.Field_API_Name__c != null && objAcc.get(objFld.Field_API_Name__c) == null ){
                                dontAddThisRecord = true;
                                 if(LOG_LEVEL.containsIgnoreCase('Debug') /*|| LOG_LEVEL.containsIgnoreCase('INFO')*/){
                                    String description = '* required field is empty :'+objFld.Field_API_Name__c; // Record Id ::'+objAcc.get('Id')+ ' \n 
                                    Id recId = (Id)objAcc.get('Id');
                                    if(!errorMessageById.containsKey(recId)){
                                        errorMessageById.put(recId, new Set<String>());
                                    }
                                    errorMessageById.get(recId).add(description);
                                    //EventLog.generateLog('HivebriteIntegrationSchedulable', 'execute', 'INFO', description);
                                 }
                                break;
                            }
                        }else{ // Record contains all required fields value. when  record's rules  changes its values and it meets all rules then will send to hivebrite only when extrenal fld is null.
                            if(Trigger.isUpdate && objFld.Field_API_Name__c != null && objAcc.get(objFld.Field_API_Name__c) == null && isRuleValuesChanged ){
                                // check rules  for( Integration_API_Rule__mdt rule : apiObj.Integration_API_Rules__r){  fields changed 
                                isValuesChanged = true;
                            }
                        }
                    }
                    if(Trigger.isUpdate && !isValuesChanged && !dontAddThisRecord){
                        if(LOG_LEVEL.containsIgnoreCase('Debug') /*|| LOG_LEVEL.containsIgnoreCase('INFO') */){
                            String description = '* Hivebrite Field values are not changed. So we are not sync it now.'; // Record Id ::'+objAcc.get('Id')+ ' \n 
                            Id recId =(Id)objAcc.get('Id');
                            if(!errorMessageById.containsKey(recId)){
                                errorMessageById.put(recId, new Set<String>());
                            }
                            errorMessageById.get(recId).add(description);
                            //EventLog.generateLog('HivebriteIntegrationSchedulable', 'execute', 'INFO', description);
                        }
                    }
                }
                // if size is greater then zero then only evaluate
                if(apiObj.Integration_API_Rules__r.size() > 0 && !dontAddThisRecord && String.isNotBlank(filterLogic)){
                    
                    if(evaluate(filterLogic)){ 
                        // add tolist if is update and value is changed or is on insert                    
                        if((Trigger.isExecuting && Trigger.isUpdate && isValuesChanged ) ||
                            (Trigger.isExecuting && !Trigger.isUpdate && !isValuesChanged))
                        {
                            recordListByJobtype.get(apiObj.Job_Type__c).add(objAcc);
                        }
                    }else{
                        if(LOG_LEVEL.containsIgnoreCase('Debug') /*|| LOG_LEVEL.containsIgnoreCase('INFO') */){
                            String description = '* '+apiObj.Job_Type__c+ ': record is not meeting the rules Conditions'; // 'Record Id ::'+objAcc.get('Id')+ '\n '+
                            //EventLog.generateLog('HivebriteIntegrationSchedulable', 'execute', 'INFO', description);
                            Id recId = (Id)objAcc.get('Id');
                            if(!errorMessageById.containsKey(recId)){
                                errorMessageById.put(recId, new Set<String>());
                            }
                            errorMessageById.get(recId).add(description);
                        }
                    }
                }else if(!dontAddThisRecord){ // no rule-> all record
                    if((Trigger.isExecuting && Trigger.isUpdate && isValuesChanged ) ||  
                        (Trigger.isExecuting && !Trigger.isUpdate && !isValuesChanged))
                    {
                        recordListByJobtype.get(apiObj.Job_Type__c).add(objAcc);    
                    }                    
                }                    
            }             
        }
        for(Id recId : errorMessageById.keySet()){
            String errorMessage = 'Record Id : ' + recId +' \n '+ String.join(new List<String>(errorMessageById.get(recId)), ' \n');
            EventLog.generateLog('HivebriteIntegrationSchedulable', 'execute', lOG_LEVEL, errorMessage);
        }
        // ready for queueable class.
        for(String JobTypeName : recordListByJobtype.keySet()){
            if(recordListByJobtype.get(JobTypeName).size() > 0){
                // System.debug('Rule Matched---------->'+JobTypeName +'------->'+ recordListByJobtype.get(JobTypeName).size());
                enqueueNextJob(JobTypeName, recordListByJobtype.get(JobTypeName));
            }
        }
    }
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param fieldApiName 
    * @param record 
    * @return Boolean 
    **/
    private static Boolean isChanged(String fieldApiName, SObject record){
        if(String.isEmpty(fieldApiName) || fieldApiName.contains('.')){
            return false;
        }        
        SObject oldRecord = Trigger.OldMap.get(record.Id);
        if(oldRecord.get(fieldApiName) == record.get(fieldApiName)){
            return false;
        }
        return true;
    }

    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param hiveBriteField 
    * @param mapingHivebrite 
    * @param recordFldValue 
    * @return Map<String, Object> 
    **/
    public static Map<String, Object> createJSONBody(String hiveBriteField, Map<String, Object> mapingHivebrite, Object recordFldValue){
       
        if( ( hiveBriteField.contains('[') && hiveBriteField.contains('.') && hiveBriteField.contains('condition(')  && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('.') && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('condition(') ) || 
            ( hiveBriteField.contains('[') && hiveBriteField.contains('.') && !hiveBriteField.contains('condition(') && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('.') ) || 
            ( hiveBriteField.contains('[') && !hiveBriteField.contains('.') && hiveBriteField.contains('condition(') && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('condition(') ) || 
            ( hiveBriteField.contains('[') && !hiveBriteField.contains('.') && !hiveBriteField.contains('condition(') ) )
        {
            
            list<String> lstFields  = hiveBriteField.split('\\[',2);            
            String reverseField = lstFields[1].reverse();           
            reverseField = reverseField.replaceFirst('\\]', '');
            lstFields.set(1, reverseField.reverse());
             Map<String, Object> mapValue;
             List<Object> objectList = new List<Object>();
            if(mapingHivebrite.containsKey(lstFields[0])){                
                if(String.isNotblank(String.valueOf( mapingHivebrite.get(lstFields[0])))){
                    if(mapingHivebrite.containsKey(lstFields[0])){
                        objectList = (List<Object>) mapingHivebrite.get(lstFields[0]); 
                    }
                    mapValue = new Map<String, Object>();
                }else{
                    mapValue = new Map<String, Object>();
                }
            }else{
                mapValue = new Map<String, Object>();
            }
            if(lstFields[1].equalsIgnoreCase('fldValue')){                                 
                objectList.add(recordFldValue );               
                mapingHivebrite.put(lstFields[0], objectList);
            }else{                
                Object updatedValue = createJSONBody(lstFields[1], mapValue,recordFldValue);               
                objectList.add(updatedValue);
                mapingHivebrite.put(lstFields[0], objectList);
            }
        }else if(hiveBriteField.contains('.') && ( !hiveBriteField.contains('condition(') || hiveBriteField.indexOf('.') < hiveBriteField.indexOf('condition(') )){
            list<String> lstFields = hiveBriteField.split('\\.',2);
            Map<String, Object> mapValue;
            if(mapingHivebrite.containsKey(lstFields[0])){                
                if(String.isNotblank(String.valueOf( mapingHivebrite.get(lstFields[0])))){                   
                     mapValue = (Map<String, Object>) mapingHivebrite.get(lstFields[0]);
                }else{
                    mapValue = new Map<String, Object>();
                }
            }else{
                mapValue = new Map<String, Object>();
            }
            Map<String, Object> updatedValue = createJSONBody(lstFields[1], mapValue,recordFldValue);
            
            mapingHivebrite.put(lstFields[0], updatedValue);
        }else if(hiveBriteField.startsWith('condition(')){           
           
            String flds = hiveBriteField.substringBetween('(', ')');
            flds = flds.deleteWhitespace();
            List<String> conditionValue = flds.split('\\?');
            
            String conditionfld = conditionValue[0].substringBefore('==');
            String matchingValue = conditionValue[0].substringAfter('==');
            mapingHivebrite.put(conditionfld, matchingValue);           

            mapingHivebrite = createJSONBody(conditionValue[1], mapingHivebrite,recordFldValue );

        }else{
            mapingHivebrite.put(hiveBriteField,recordFldValue);
        }

        return mapingHivebrite;
    }
   
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param apiField 
    * @param record 
    * @return Object 
    **/
    public static Object getFieldValue(Integration_API_Field__mdt apiField, SObject record){
        object retVal;
        String value; 
        String sfField = apiField.Field_API_Name__c;
        String defaultValue = apiField.default_value__c;
        if(String.isNotBlank(sfField)){
            sfField = sfField.trim();
            value = String.valueOf(record.get(sfField));
        }else{
            value = defaultValue;
        }
        if(apiField.Data_Type__c == 'Multipicklist' && value !=  null){
            String[] pickValue = value.split(';');
            retVal = pickValue;
        }else{
            retVal = value;
        }
        return retVal;
    }
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param hiveBriteField 
    * @param response 
    * @return Object 
    **/
    public static  Object parseJSONBody(String hiveBriteField, Object response){
        Object value;
        try{
            if(response == null){
                return null;
            }
            Map<String, Object> mapValue = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(response));
            if( ( hiveBriteField.contains('[') && hiveBriteField.contains('.') && hiveBriteField.contains('condition(')  && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('.') && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('condition(') ) || 
                ( hiveBriteField.contains('[') && hiveBriteField.contains('.') && !hiveBriteField.contains('condition(') && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('.') ) || 
                ( hiveBriteField.contains('[') && !hiveBriteField.contains('.') && hiveBriteField.contains('condition(') && hiveBriteField.indexOf('[') < hiveBriteField.indexOf('condition(') ) || 
                ( hiveBriteField.contains('[') && !hiveBriteField.contains('.') && !hiveBriteField.contains('condition(') ) )
            {    
                list<String> lstFields  = hiveBriteField.split('\\[',2);
                String reverseField = lstFields[1].reverse();
                reverseField = reverseField.replaceFirst('\\]', '');
                lstFields.set(1, reverseField.reverse());

                if(mapValue.containsKey(lstFields[0])){
                    // System.debug('inside of if---------------->'+lstFields[0]);
                    List<Object> objectList = (List<Object>)JSON.deserializeUntyped(JSON.serialize(mapValue.get(lstFields[0])));
                    if(lstFields[1].equalsIgnoreCase('fldValue') && !objectList.isEmpty()){
                        // System.debug('inside of match value--->'+lstFields[1]);
                        value = objectList;
                        // value = objectList[0];
                    }else{
                        for(Object objVal : objectList){                        
                            value = parseJSONBody(lstFields[1], objVal);
                            if(value != null){
                                break;
                            }
                        }
                    }            
                }else{
                    return 'FieldValueISNotThere';
                }
                
            // }else if(hiveBriteField.contains('.')){
            }else if(hiveBriteField.contains('.') && ( !hiveBriteField.contains('condition(') || hiveBriteField.indexOf('.') < hiveBriteField.indexOf('condition(') )){
                list<String> lstFields = hiveBriteField.split('\\.',2);
                if(mapValue.containsKey(lstFields[0])){
                   value = parseJSONBody(lstFields[1], mapValue.get(lstFields[0]));
                }else{
                    return 'FieldValueISNotThere';
                }
                    
            }else if(hiveBriteField.startsWith('condition(')){
                
                //custom_attributes[condition(name == _d22c5b3e_Expertise ? value)]
                // here name and value are hivebrite flds
                String flds = hiveBriteField.substringBetween('(', ')');
                flds = flds.deleteWhitespace();
                List<String> conditionValue = flds.split('\\?');            
                String conditionfld = conditionValue[0].substringBefore('==');
                String matchingValue = conditionValue[0].substringAfter('==');
                if(String.valueOf(mapValue.get(conditionfld)) == matchingValue){
                    value = parseJSONBody(conditionValue[1], mapValue);
                }  

            }else{
                if(mapValue.containsKey(hiveBriteField)){
                    value = mapValue.get(hiveBriteField);
                }else{
                    return 'FieldValueISNotThere';
                }
            }
        }catch (Exception ex){
            EventLog.generateLog('HivebriteIntegrationContactHelper', 'parseJSONBody', 'ERROR', ex);                 
        }
        return value;
    }
    /**
    * @description When Account is created. will also create records in hivebrite
    * @author Anup Kage | 7/11/2019 
    * @return void 
    **/
    private static void insertContactsOnAccount(){
        try{
            Set<Id> accountIdSet = new Set<Id>();
            if(Trigger.isExecuting && Trigger.isUpdate){
                Integration_API_Field__mdt externalIdField = HivebriteIntegrationQueryUtils.getExternalIdField('Portfolio-Company');
                Map<Id, Account> oldMap = (Map<Id, Account>)Trigger.oldMap;
                for(Account objAcc : (List<Account>)Trigger.new){
                    if(/*oldMap.get(objAcc.Id).get(externalIdField.Field_API_Name__c) == null && */
                        objAcc.get(externalIdField.Field_API_Name__c) != null)
                    {
                        accountIdSet.add(objAcc.Id);
                    }
                }
                if(!accountIdSet.isEmpty()){
                    String queryFieldString = getDynamicSoqlQuery('Contact-User', 'Contact', 'WHERE AccountId =: accountIdSet '); // get dynamic soql query 
                   
                    
                    if(String.isNotBlank(queryFieldString)){
                        List<Contact> contactList = Database.query(queryFieldString);
                        
                        if(!contactList.isEmpty()){
                            enqueueNextJob('CONTACT_SF_TO_HB', contactList);
                        }
                    }  
                }
            }
        }catch (Exception e){
             EventLog.generateLog('HivebriteIntegrationUtils', 'insertContactsOnAccount', 'error' , e);
        }
        
    }
    /**
    * @description need to query those fields which are used in the Contact Hivebrite sync
    * @author Anup Kage | 11/20/2019 
    * @param IntegartionObjectApiLabel 
    * @param objectAPI 
    * @param whereCondition 
    * @return String 
    **/
    public static String getDynamicSoqlQuery(String IntegartionObjectApiLabel, String objectAPI, String whereCondition){
        Map<String, Schema.SObjectField> fldsMap =  Schema.getGlobalDescribe().get(objectAPI).getDescribe().fields.getMap();
        String queryFieldString ='';
        String externalIdFld;
        Set<String> contactFieldSet = new Set<String>{'id'};
        for(Integration_API_Field__mdt mapFld : HivebriteIntegrationQueryUtils.getMapingFieldsRecords(IntegartionObjectApiLabel)){
            if(String.isNotBlank(mapFld.Field_API_Name__c)){
                contactFieldSet.add(mapFld.Field_API_Name__c.toLowercase());
                if(mapFld.External_Id__c){
                    externalIdFld = mapFld.Field_API_Name__c; 
                }else if(mapFld.Required_Field__c){
                    whereCondition += ' AND '+mapFld.Field_API_Name__c+' <> null ';
                }
            }
        }
        String filterLogic;
        for(Integration_API_Rule__mdt rule : HivebriteIntegrationQueryUtils.getApiRules(IntegartionObjectApiLabel)){
            if(filterLogic == null){
                filterLogic = rule.Integration_API_Object__r.Filter_Logic__c;
            }
            if(filterLogic != null && rule.Field__c != null){
                String condition;
                if(rule.Operator__c == 'equals'){ 
                    if(fldsMap.containsKey(rule.Field__c) &&  rule.Value__c != null && ( fldsMap.get(rule.Field__c).getDescribe().getType() == Schema.DisplayType.STRING || 
                        fldsMap.get(rule.Field__c).getDescribe().getType() == Schema.DisplayType.TEXTAREA )){
                        condition = ' '+rule.Field__c +' = \''+ rule.Value__c+'\'';
                    }else{
                        condition = ' '+rule.Field__c +' = '+ rule.Value__c;
                    }
                }else if(rule.Operator__c == 'not equal to'){
                    if(fldsMap.containsKey(rule.Field__c) && rule.Value__c != null && ( fldsMap.get(rule.Field__c).getDescribe().getType() == Schema.DisplayType.STRING || 
                        fldsMap.get(rule.Field__c).getDescribe().getType() == Schema.DisplayType.TEXTAREA ) ){
                        condition = ' '+rule.Field__c +' <> \''+ rule.Value__c+'\'';
                    }else{
                        condition = ' '+rule.Field__c +' <> '+ rule.Value__c;
                    }
                }
                filterLogic = filterLogic.replaceFirst(String.valueOf(Integer.valueOf(rule.Order__c)), condition);                           
            }
        }
        if(externalIdFld != null){
            whereCondition += ' AND '+externalIdFld+' = null ';
        }
        if(filterLogic != null){
            whereCondition += ' AND ('+ filterLogic + ')';
        }
        //TODO : Pending with external Id;
        if(!contactFieldSet.isEmpty()){
            List<String> fldList = new List<String>(contactFieldSet);
            queryFieldString = 'SELECT '+ String.join(fldList, ',') +' FROM '+objectAPI+' '+whereCondition;//WHERE AccountId =: accountIdSet ';
            System.debug('queryFieldString+++++++++++++++++++++++'+queryFieldString);
        }
        return queryFieldString; 
    }    
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param jobType 
    * @param smallRecordsList 
    * @return void 
    **/
    private static void enqueueNextJob(String jobType, List<SObject> smallRecordsList ){
        if(Limits.getLimitQueueableJobs() == 1 && Trigger.isExecuting){
            System.debug('Insdei ScheduledJob:::'+ jobType);
            // System.assert(false);
            Datetime dt = Datetime.now().addSeconds(4);
            String hour = String.valueOf(dt.hour());
            String min = String.valueOf(dt.minute()); 
            String ss = String.valueOf(dt.second());
            //parse to cron expression
            String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
            System.schedule('ScheduledJob ' + jobType + ' ' + String.valueOf(Math.random()), nextFireTime, new HivebriteIntegrationSchedulable(jobType, smallRecordsList));
        }else{
             System.debug('Insdei Queuable JOb:::'+ jobType);
             HivebriteIntegrationQueueable objQueue = new HivebriteIntegrationQueueable(jobType, smallRecordsList);
            if(!Test.isRunningTest()){
                // call Queueable class to update company record on hivebrite server.
                ID jobId = System.enqueueJob(objQueue);
            }
        }       
    }
   
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param record 
    * @param rule 
    * @param ObjectName 
    * @return String 
    **/
     //! TODO move this code to new class
    private static String validateCondition(SObject record, Integration_API_Rule__mdt rule, String ObjectName ){
        //! TODO handle fieldType 
        if(String.isNotBlank(rule.Field__c)){
            String fldApi = rule.Field__c;
            Object ruleValue = rule.Value__c;
            if('RecordType.Name'.equalsIgnoreCase(rule.Field__c)){
                fldApi = 'RecordTypeId';
                Map<String, Recordtype> recordTypeByName =  HivebriteIntegrationQueryUtils.getRecordTypeValue(ObjectName);
                ruleValue = recordTypeByName.get(rule.Value__c) != null ? recordTypeByName.get(rule.Value__c).Id : null;
            }
            Object recordValue;
            if( record.get(fldApi) != null){
                recordValue = String.valueOf(record.get(fldApi));
            }else{
                recordValue = record.get(fldApi);
            }
            
            if(rule.Operator__c == 'equals'){
                return recordValue == ruleValue ? 'TRUE' : 'FALSE';
                // if(string.valueOf(record.get(fldApi)) == ruleValue || (record.get(fldApi) != null && ruleValue != null && string.valueOf(record.get(fldApi)).equalsIgnoreCase(ruleValue))){
                //     return 'TRUE';
                // }else{
                //     return 'FALSE';
                // }
            }else if(rule.Operator__c == 'not equal to'){
                return recordValue != ruleValue ? 'TRUE' : 'FALSE';
                //TODO for other Operator__c
            }
        }
        return 'FALSE';
    }
    
    //TODO move to new class
    static  Map<String, String> logicTypes = new Map<String, String>();
    static  Map<String, Map<String, String>> expressionLogic = new Map<String, Map<String, String>>();
    @testvisible
    private static Boolean evaluate(String expression){
        //TODO check for and or  combination
        if(!expression.containsIgnoreCase('FALSE')) { return true; }     
         if(!expression.containsIgnoreCase('TRUE')) { return false; }
          fillLogic();       
        return Boolean.valueOf(evaluateExpression(expression));
    }    
    
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param expression 
    * @return String 
    **/
    private static String evaluateExpression(String expression) {    
        for(String logicType : logicTypes.keySet()) {
            if(expression.contains(logicType)) {
                expression = simplifyExpression(expression, logicTypes.get(logicType));
            }
        }
        
        if(expression.contains('AND') || expression.contains('OR') || expression.contains('(')) {
            expression = evaluateExpression(expression);
        }
        
        return expression;
    }
    
    
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param expression 
    * @param LogicType 
    * @return string 
    **/
    private static string simplifyExpression(String expression, String LogicType){
        Map<String, String> Logic = new Map<String, String>(expressionLogic.get(LogicType));
        
        for(String key : Logic.keySet()) {
            expression = expression.replace(key, Logic.get(key));
        }
        
        return expression;
    } 
    
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @return void 
    **/
    private static void fillLogic() {
        Map<String, String> ANDLogic = new Map<String, String>();
        Map<String, String> ORLogic = new Map<String, String>();
        Map<String, String> BRACELogic = new Map<String, String>();
        
        logicTypes.put('AND', 'AND');
        logicTypes.put('OR', 'OR');
        logicTypes.put('(', 'BRACES');
        
        // AND Logic
        ANDLogic.put('TRUE AND TRUE', 'TRUE');
        ANDLogic.put('TRUE AND FALSE', 'FALSE');
        ANDLogic.put('FALSE AND TRUE', 'FALSE');
        ANDLogic.put('FALSE AND FALSE', 'FALSE');
        expressionLogic.put('AND', ANDLogic);
        
        // OR Logic
        ORLogic.put('TRUE OR TRUE', 'TRUE');
        ORLogic.put('TRUE OR FALSE', 'TRUE');
        ORLogic.put('FALSE OR TRUE', 'TRUE');
        ORLogic.put('FALSE OR FALSE', 'FALSE');
        expressionLogic.put('OR', ORLogic);
        
        // Braces Logic
        BRACELogic.put('(TRUE)', 'TRUE');
        BRACELogic.put('(FALSE)', 'FALSE');
        expressionLogic.put('BRACES', BRACELogic);
    }
}