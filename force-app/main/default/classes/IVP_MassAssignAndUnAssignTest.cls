@isTest
public class IVP_MassAssignAndUnAssignTest {
    private static ApexPages.StandardSetController testData(){
        List<Account> listAccount = new List<Account>();
        listAccount.add(new Account(Name='Test-Account-1'));
        listAccount.add(new Account(Name='Test-Account-2'));
        
        insert listAccount;
        
        ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(listAccount);
        sc1.setSelected(listAccount);
        
        return sc1;
    }
    private static testMethod void testMassAssignAndUnAssign() {
        Test.startTest();

        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(new List<Account>());
        
        IVP_MassAssignAndUnAssign massAssignAndUnAssign1 = new IVP_MassAssignAndUnAssign( testData());
        IVP_MassAssignAndUnAssign massAssignAndUnAssign2 = new IVP_MassAssignAndUnAssign( sc2);
        Test.stopTest();
        
        System.assertEquals(2,massAssignAndUnAssign1.countOfAccountsSelected);
        System.assertEquals(0,massAssignAndUnAssign2.countOfAccountsSelected);
    }
    private static testMethod void testMassAssignBatch() {
        Test.startTest();
        
        IVP_MassAssignAndUnAssign massAssignAndUnAssign = new IVP_MassAssignAndUnAssign( testData());
        massAssignAndUnAssign.scheduleForMassAssign();
        
        Test.stopTest();
        System.assertEquals(2,massAssignAndUnAssign.countOfAccountsSelected);
    }
    private static testMethod void testMassUnAssignBatch() {
        Test.startTest();
        
        IVP_MassAssignAndUnAssign massAssignAndUnAssign = new IVP_MassAssignAndUnAssign( testData());
        massAssignAndUnAssign.selectedLossDropReason = 'No opportunity given prior capital';
        massAssignAndUnAssign.scheduleForMassUnassign();
        
        Test.stopTest();
        System.assertEquals(2,massAssignAndUnAssign.countOfAccountsSelected);
    }
}