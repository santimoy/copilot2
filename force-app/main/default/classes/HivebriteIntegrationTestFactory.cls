/**
 * @File Name          : HivebriteIntegrationTestFactory.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 9:44:57 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/11/2019   Anup Kage     Initial Version
**/
@isTest
public class HivebriteIntegrationTestFactory {
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return Account 
    **/
    public static Account  createCompanyRecord() {
        Id rtId = [select id from RecordType Where SobjectType ='Account' and name ='Portfolio Company' limit 1].Id;
        // Account acc = new Account(name ='Account '+Datetime.now(),
        //                     BillingStateCode = 'NY',
        //                     BillingCountryCode ='US',
        //                     recordTypeid = rtId,
        //                     Loss_Drop_Notes__c='asd',
        //                     Loss_Drop_Reason__c ='High valuation',
        //                     Website_Status__c = 'Current Investment',
        //                     Website_Verticals__c = 'Application Software',
        //                     Website_Sectors__c = 'B2c',
        //                     Website_Regions__c = 'North America',
        //                     Website_Blog__c = 'www.hivebrite.com',
        //                     Website_Facebook__c = 'www.facebook.com/anupkage',
        //                     Website_LinkedIn__c = 'www.linkedin.com/anupkage',
        //                     Website_Twitter__c = 'www.twitter.com/anupkage',
        //                     Website = 'www.kingIntheNorth.com');

        // INSERT acc;
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Account' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Account.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Account obj = new Account(recordTypeid = rtId,
                                    Loss_Drop_Notes__c='asd',
                                    Loss_Drop_Reason__c ='High valuation');
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        obj.BillingStateCode = 'NY';
        INSERT obj;
        return obj;
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return Account 
    **/
    public static Account  createCompanyRecordWithHivebriteId() {
        Id rtId = [select id from RecordType Where SobjectType ='Account' and name ='Portfolio Company' limit 1].Id;
        // Account acc = new Account(name ='Account '+Datetime.now(),
        //                     BillingStateCode = 'NY',
        //                     BillingCountryCode ='US',
        //                     recordTypeid = rtId,
        //                     Loss_Drop_Notes__c='asd',
        //                     Loss_Drop_Reason__c ='High valuation',
        //                     Website_Status__c = 'Current Investment',
        //                     Website_Verticals__c = 'Application Software',
        //                     Website_Sectors__c = 'B2c',
        //                     Website_Regions__c = 'North America',
        //                     Website_Blog__c = 'www.hivebrite.com',
        //                     Website_Facebook__c = 'www.facebook.com/anupkage',
        //                     Website_LinkedIn__c = 'www.linkedin.com/anupkage',
        //                     Website_Twitter__c = 'www.twitter.com/anupkage',
        //                     Website = 'www.kingIntheNorth.com',
        //                     Hivebrite_Company_Id__c = '123company321');
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Account' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Account.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Account obj = new Account(recordTypeid = rtId,
                                    Loss_Drop_Notes__c='asd',
                                    Loss_Drop_Reason__c ='High valuation');
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        obj.BillingStateCode = 'NY';
        obj.Hivebrite_Company_Id__c = '123company321';
        obj.Hivebrite_Venture_Id__c = '123venture456';
        INSERT obj;
        return obj;
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return Account 
    **/
    public static Account createVendorRecord() {
        Id rtId = [select id from RecordType Where SobjectType ='Account' and name ='Vendor' limit 1].Id;
        // Account acc = new Account(name ='Vendor '+Datetime.now(),
        //                     BillingStateCode = 'NY',
        //                     BillingCountryCode ='US',
        //                     recordTypeid = rtId,
        //                     Loss_Drop_Notes__c='asd',
        //                     Loss_Drop_Reason__c ='High valuation',
        //                     Website_Status__c = 'Current Investment',
        //                     Website_Verticals__c = 'Application Software',
        //                     Website_Sectors__c = 'B2c',
        //                     Website_Regions__c = 'North America',
        //                     Website_Blog__c = 'www.hivebrite.com',
        //                     Website_Facebook__c = 'www.facebook.com/anupkage',
        //                     Website_LinkedIn__c = 'www.linkedin.com/anupkage',
        //                     Website_Twitter__c = 'www.twitter.com/anupkage',
        //                     Website = 'www.kingIntheNorth.com',
        //                     Company_Stage__c = 'freeeeeedom',
        //                     Short_Description__c = 'heloo this is test class');
        // INSERT acc;
        // return acc;
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Account' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Account.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Account obj = new Account(recordTypeid = rtId,
                                    Loss_Drop_Notes__c='asd',
                                    Loss_Drop_Reason__c ='High valuation');
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        obj.BillingStateCode = 'NY';
        INSERT obj;
        return obj;
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return Account 
    **/
    public static Account createVendorRecordwithHivebriteId() {
        Id rtId = [select id from RecordType Where SobjectType ='Account' and name ='Vendor' limit 1].Id;
        // Account acc = new Account(name ='Vendor '+Datetime.now(),
        //                     BillingStateCode = 'NY',
        //                     BillingCountryCode ='US',
        //                     recordTypeid = rtId,
        //                     Loss_Drop_Notes__c='asd',
        //                     Loss_Drop_Reason__c ='High valuation',
        //                     Website_Status__c = 'Current Investment',
        //                     Website_Verticals__c = 'Application Software',
        //                     Website_Sectors__c = 'B2c',
        //                     Website_Regions__c = 'North America',
        //                     Website_Blog__c = 'www.hivebrite.com',
        //                     Website_Facebook__c = 'www.facebook.com/anupkage',
        //                     Website_LinkedIn__c = 'www.linkedin.com/anupkage',
        //                     Website_Twitter__c = 'www.twitter.com/anupkage',
        //                     Website = 'www.kingIntheNorth.com',
        //                     Company_Stage__c = 'freeeeeedom',
        //                     Hivebrite_Venture_Id__c = '123venture456',
        //                     Short_Description__c = 'heloo this is test class');
        // INSERT acc;
        // return acc;
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Account' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Account.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Account obj = new Account(recordTypeid = rtId,
                                    Loss_Drop_Notes__c='asd',
                                    Loss_Drop_Reason__c ='High valuation');
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        obj.BillingStateCode = 'NY';
        obj.Hivebrite_Company_Id__c = '123company321';
        obj.Hivebrite_Venture_Id__c = '123venture456';
        INSERT obj;
        return obj;
    }
    
    /**
    * @description In this method will create the Contact record which create a user record in hivebrite.Pass the AccountId as parameter.
    * @author Anup Kage | 1/11/2019 
    * @param accountId 
    * @return Contact 
    **/
    public static Contact createContact(Id accountId){
        // Contact obj =  new Contact( LastName= 'Test '+DateTime.now(),
        //                             FirstName='Class '+Datetime.now(),
        //                             AccountId = accountId,
        //                             Email = 'testclassEmail'+ Datetime.now().millisecond() +'@dev.com',
        //                             TItle='developer',
        //                             MailingCity = 'NY',
        //                             Area_Of_Interest__c = 'Sales',
        //                             Vertical__c = 'sales',
        //                             Higher_Logic_Expertise__c = 'sales',
        //                             phone = '9876541230'                                   
        //                             );
        // Insert obj;
        // return obj;
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Contact' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Contact.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Contact obj = new Contact(accountId = accountId);
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        // obj.BillingStateCode = 'NY';
        INSERT obj;
        return obj;
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @param accountId 
    * @return Contact 
    **/
    public static Contact createContactWithHivebriteId(Id accountId){
        // Contact obj =  new Contact( LastName= 'Test '+DateTime.now(),
        //                             FirstName='Class '+Datetime.now(),
        //                             AccountId = accountId,
        //                             Email = 'testclassEmail'+ Datetime.now().millisecond() +'@dev.com',
        //                             TItle='developer',
        //                             MailingCity = 'NY',
        //                             Area_Of_Interest__c = 'Sales',
        //                             Vertical__c = 'sales',
        //                             Higher_Logic_Expertise__c = 'sales',
        //                             phone = '9876541230',
        //                             Hivebrite_Id__c='7461users11671',
        //                             Hivebrite_Experience_Id__c = '2924'                            
        //                             );
        // Insert obj;
        // return obj;
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Contact' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Contact.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Contact obj = new Contact(accountId = accountId);
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        obj.Hivebrite_Id__c='7461users11671';
        // obj.Hivebrite_Experience_Id__c = '2924';
        INSERT obj;
        return obj;
    }
    public static Contact createContactWithHivebriteExpId(Id accountId){
        // Contact obj =  new Contact( LastName= 'Test '+DateTime.now(),
        //                             FirstName='Class '+Datetime.now(),
        //                             AccountId = accountId,
        //                             Email = 'testclassEmail'+ Datetime.now().millisecond() +'@dev.com',
        //                             TItle='developer',
        //                             MailingCity = 'NY',
        //                             Area_Of_Interest__c = 'Sales',
        //                             Vertical__c = 'sales',
        //                             Higher_Logic_Expertise__c = 'sales',
        //                             phone = '9876541230',
        //                             Hivebrite_Id__c='7461users11671',
        //                             Hivebrite_Experience_Id__c = '2924'                            
        //                             );
        // Insert obj;
        // return obj;
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Contact' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Contact.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Contact obj = new Contact(accountId = accountId);
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        obj.Hivebrite_Id__c='7461users11671';
        obj.Hivebrite_Experience_Id__c = '2924';
        INSERT obj;
        return obj;
    }
    public static Integration_API__mdt apiBaseURL{get{
        if(ApiBaseURL == null){
            ApiBaseURL = [  SELECT ID, Base_URL__c 
                            FROM Integration_API__mdt 
                            WHERE Label = 'Hivebrite API'  limit 1 ]; //Integration_API
        }
        return ApiBaseURL;
    }set;}    
    /**
    * @description Used to setup the mock response in hivebrite test.
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    public static void setupMockResponse(){
        String baseUrl = apiBaseURL.Base_URL__c;
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        String userEndpont = baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(userEndpont), 'HivebriteUserTestClass');

        String expEndpont = baseUrl+'/api/admin/v1/experiences/?per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(expEndpont), 'HivebriteExperienceTestClass');
        
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies/123company321', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures/123venture456', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/experiences/?per_page=10000', 'HivebriteExperienceTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences/2924', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/_bulk', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671', 'HivebriteUserTestClass');
        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);

    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    public static void setupMockResponseWithDiffUsers(){
         String baseUrl = apiBaseURL.Base_URL__c;
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        String userEndpont = baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(userEndpont), 'HivebriteFetchUserTestClass');

        String expEndpont = baseUrl+'/api/admin/v1/experiences/?per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(expEndpont), 'HivebriteExperienceTestClass');
 
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences/2924', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies/123company321', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures/123venture456', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/experiences/?per_page=10000', 'HivebriteExperienceTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/_bulk', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000', 'HivebriteFetchUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671', 'HivebriteUserTestClass');
        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);

    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    public static void setupMockBadResponse401(){
         String baseUrl = apiBaseURL.Base_URL__c;
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        String userEndpont = baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(userEndpont), 'HivebriteUserTestClass');

        String expEndpont = baseUrl+'/api/admin/v1/experiences/?per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(expEndpont), 'HivebriteExperienceTestClass');
        multimock.setStaticResource(baseUrl+'/oauth/token', 'HivebriteAccesTokenTestData');
        //

        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies/123company321', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures/123venture456', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/experiences/?per_page=10000', 'HivebriteExperienceTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/_bulk', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences/2924', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671', 'HivebriteUserTestClass');
        multimock.setStatusCode(401);
        multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);

    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @return void 
    **/
    public static void setupMockBadResponse500(){
         String baseUrl = apiBaseURL.Base_URL__c;
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        String userEndpont = baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(userEndpont), 'HivebriteUserTestClass');

        String expEndpont = baseUrl+'/api/admin/v1/experiences/?per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(expEndpont), 'HivebriteExperienceTestClass');

        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures/123venture456', 'HivebriteVentureTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/experiences/?per_page=10000', 'HivebriteExperienceTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies/123company321', 'HivebriteCompanyTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/_bulk', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000', 'HivebriteUserTestClass');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences/2924', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences', 'HivebriteExperienceSingleTestData');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671', 'HivebriteUserTestClass');
        multimock.setStatusCode(500);
        multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);
    }
    public static void setupMockBadResponseJSONException(){
         String baseUrl = apiBaseURL.Base_URL__c;
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        String userEndpont = baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(userEndpont), 'HivebriteJSONExceptionResponse');

        String expEndpont = baseUrl+'/api/admin/v1/experiences/?per_page=10000&updated_since={timestamp}';
        multimock.setStaticResource(getEndPoint(expEndpont), 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences/2924', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v2/ventures/123venture456', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/experiences/?per_page=10000', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/companies/123company321', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/_bulk', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/?full_profile=true&per_page=10000', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences/2924', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671/experiences', 'HivebriteJSONExceptionResponse');
        multimock.setStaticResource(baseUrl+'/api/admin/v1/users/7461users11671', 'HivebriteUserTestClass');
        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);
    }
    /**
    * @description 
    * @author Anup Kage | 1/11/2019 
    * @param endpoint 
    * @return string 
    **/
    private static string getEndPoint(String endpoint){
        
        if(endpoint.contains('{timestamp}')){
            Integration_API_Settings__c accToken = Integration_API_Settings__c.getOrgDefaults();
            if(accToken.Last_Run_Timestamp__c != null){
                Datetime dt = accToken.Last_Run_Timestamp__c;
                String lastRunTimeString = dt.yearGmt()+'-'+dt.monthGmt()+'-'+dt.dayGmt()+'T'+dt.hourGmt()+':'+dt.minuteGmt()+':'+dt.secondGmt();
                endpoint =  endpoint.replace('{timestamp}', lastRunTimeString);
            }else{
                endpoint = endpoint.removeEnd('&updated_since={timestamp}');
            }            
        }
        return endpoint;
    }
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @return void 
    **/
    public static void createContactRecord(){
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c = 'Contact' AND Field_API_Name__c <> null ]; 
        Map<String,Schema.SObjectField> FsMap = Contact.sObjectType.getDescribe().fields.getMap();
        Map<String, Object> fieldByDefaultValue =  getdefaultValues(FsMap, lstApiFld);
        Contact obj = new Contact();
        for(String fld : fieldByDefaultValue.keySet()){
            obj.put(fld, fieldByDefaultValue.get(fld));
        }
        INSERT obj;
    }
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param FsMap 
    * @param lstApiFld 
    * @return Map<String, Object> 
    **/
    private static Map<String, Object> getdefaultValues(Map<String,Schema.SObjectField> FsMap, List<Integration_API_Field__mdt> lstApiFld){
        Map<String, Object> fieldByDefaultValue = new Map<String, Object>();
        for(Integration_API_Field__mdt objField : lstApiFld){
            if(!FsMap.containsKey(objField.Field_API_Name__c)){
                continue;
            }
            Schema.DescribeFieldResult fld = FsMap.get(objField.Field_API_Name__c).getDescribe();
            if(objField.Field_API_Name__c.equalsIgnoreCase('Id') || objField.External_Id__c || fld.isCalculated() || fld.isAutoNumber() || fld.isDefaultedOnCreate()){
                continue;
            }
            if(fld.getType() == Schema.DisplayType.REFERENCE){

            }else if(fld.getType() == Schema.DisplayType.PICKLIST || fld.getType() == Schema.DisplayType.MULTIPICKLIST){
                // List<Schema.PicklistEntry> picklistValues = fld.getPicklistValues();
                String defaultValue = '';
                for(Schema.PicklistEntry plObj : fld.getPicklistValues()){
                    if(plObj.isActive() && plObj.isDefaultValue()){
                        defaultValue = plObj.getValue();
                    }else if(String.isBlank(defaultValue) && plObj.isActive()){
                        defaultValue = plObj.getValue();
                    }
                }
                fieldByDefaultValue.put(fld.getName(), defaultValue);
            }else if(fld.getType() == Schema.DisplayType.EMAIL){
                 fieldByDefaultValue.put(fld.getName(), 'hivebrite'+datetime.now().getTime()+'@fake.com');
            }else if(fld.getType() == Schema.DisplayType.DATE){
                 fieldByDefaultValue.put(fld.getName(), Date.Today());
            }else if(fld.getType() == Schema.DisplayType.DATETIME){
                 fieldByDefaultValue.put(fld.getName(), Date.Today());
            }else if(fld.getType() == Schema.DisplayType.BOOLEAN){
                 fieldByDefaultValue.put(fld.getName(), true);
            }else if(fld.getType() == Schema.DisplayType.STRING || fld.getType() == Schema.DisplayType.TEXTAREA ){
                String value = 'test'+datetime.now();
                if(value.length() > fld.getLength()){
                    value = value.left(fld.getLength());
                }
                fieldByDefaultValue.put(fld.getName(), value);
            }else if(fld.getType() == Schema.DisplayType.DOUBLE){
                 fieldByDefaultValue.put(fld.getName(), 1.1);
            }else if(fld.getType() == Schema.DisplayType.PHONE){
                 fieldByDefaultValue.put(fld.getName(), '1234567890');
            }else if(fld.getType() == Schema.DisplayType.URL){
                 fieldByDefaultValue.put(fld.getName(), 'www.fake.com');
            }
        }
        return fieldByDefaultValue;
    }
    /**
    * @description 
    * @author Anup Kage | 7/11/2019 
    * @param objectName 
    * @return String 
    **/
    public static String getSobjectRecords(String objectName){
        List<Integration_API_Field__mdt> lstApiFld = [ SELECT ID, Field_API_Name__c, External_Id__c
                                                        FROM Integration_API_Field__mdt 
                                                        WHERE Integration_API_Object__r.Object_API_Name__c =:objectName  AND Field_API_Name__c <> null ]; 
        Set<String> fieldSet = new Set<String>();
        fieldSet.add('id');
        for(Integration_API_Field__mdt fld : lstApiFld){
            fieldSet.add(fld.Field_API_Name__c.toLowerCase());
        }
        return String.join(new List<String>(fieldSet), ',');
    }
}