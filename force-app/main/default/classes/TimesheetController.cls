public  class TimesheetController  {
/* Class to handle loading of timesheet records ready for data entry by the current user
    2015-07-19 TMW  Initial Creation
    Handles creation of current week and previous 
    2015-08-12 TMW Handle Last_Week Snippets
    2015-09-08 Disable Last_week snippet functionality

*/



    public TimesheetController(ApexPages.StandardSetController controller) {
        controller.setPageSize(100);
        setController=controller;
        selectRecords();
    

    }

    ApexPages.StandardSetController setController;

    public void selectRecords() {  
  
     
      //Get selected records  
      List<Weekly_Update__c> selectedList = setController.getSelected();  
      if(selectedList.size()==0){
        setController.setSelected(setController.getRecords());
      } 
    }  


    List<Weekly_Update__c> pendingTimesheets;
    Set<Id> existingTimesheets;

    public List<Weekly_Update__c> getNewTimesheets(Date startDate, Date endDate, Id userId, Date currentDate) {
        List<Weekly_Update__c> pending;
        list<Working_Group_Member__c> projects=[select Project__c 
        from  Working_Group_Member__c where  Working_Group_Member__c=:userId 
        and Start_Date__c<=:endDate and (End_Date__c>=:startDate or End_Date__c =null)];

        existingTimesheets= new set<Id>();
        /* modify to get previous week */
             Date queryDate=endDate.addDays(-7);
        List<Weekly_Update__c> existing=[select Project__c,Snippets__c,Week_Ending__c from Weekly_Update__c where 
                Resource__c=:userId and (Week_Ending__c=:queryDate or Week_Ending__c=:endDate)];


        //Map<Id,String> previousWeekSnippet= new Map<Id,String>();

        for(Weekly_Update__c wu:existing){
                if(wu.Week_Ending__c==endDate)
                    existingTimesheets.add(wu.Project__c);
                else {
                    //if(wu.Snippets__c!=null) previousWeekSnippet.put(wu.Project__c,wu.Snippets__c);
                }
        }
         pending= new List<Weekly_Update__c>();

        System.debug('timesheets: count:'+projects.size());
        if(projects.size()>0){
           
            for(Working_Group_Member__c project:projects){
                if(!existingTimesheets.contains(project.Project__c)){
                    Weekly_Update__c ts= new Weekly_Update__c();
                    ts.Status__c='In Progress';
                    ts.Resource__c=userId;
                    ts.Project__c=project.Project__c;
                    ts.Week_Ending__c=endDate;
                    /*
                    if(previousWeekSnippet.containsKey(project.Project__c)){
                        ts.Last_Week__c=previousWeekSnippet.get(project.Project__c);
                    }
                    */
                    pending.add(ts);
                }
            }
            
        }
        return pending;
    }

   // Get all working group member records for the current user and load timesheets for valid weeks
   // Checks to see if timesheet already exists
    public PageReference loadTimeSheets() {

       Id userId=UserInfo.getUserId();
       Date currentDate=Date.today();
       Date startOfWeek=currentDate.toStartofWeek();  //Should be Sunday in US locale

       Date endOfWeek=startOfWeek.addDays(6);
       System.debug('startOfWeek:'+startOfWeek+',endOfWeek:'+endOfWeek);

       Date goLive=Common_Config__c.getInstance().Timesheet_Go_Live__c;
       if(goLive==null) {
        goLive=Date.today().toStartofWeek().addDays(-1);
        }
       Integer count = 1;
        pendingTimesheets=new List<Weekly_Update__c>() ;
        do {
            if(startOfWeek>goLive  ) {
                  pendingTimesheets.addAll(getNewTimesheets(startOfWeek, endOfWeek, userId, currentDate));
            }
            currentDate=currentDate.addDays(-7);
            startOfWeek=currentDate.toStartofWeek();
            endOfWeek=startOfWeek.addDays(6);
            count++;
        } while (count < 15);

       insert pendingTimesheets;

       



        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        if(retURL==null) retURL='/a0V?fcf=00BZ0000000z01K';
        PageReference pageRef = new PageReference(retURL);
      //PageReference pageRef = new PageReference('/a0V?fcf=00BZ0000000z01K');
      //PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());

        pageRef.setRedirect(true);
       return pageRef; //Returns to the case page
    }

}