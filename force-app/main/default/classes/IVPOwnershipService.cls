/***********************************************************************************
 * @author      10K-Expert
 * @name    IVPOwnershipService
 * @date    2018-10-29
 * @description  This class contains all the generic methods related to IVP Ownership. 
 ***********************************************************************************/
public class IVPOwnershipService {
    public static final String OWNERSHIP_REQUEST_BALANCE_DAYS = 'ownership-request-balance-days';
    //public static final String OWNERSHIP_REQUEST_BALANCE = 'ownership-request-balance';
    // public static final String OWNERSHIP_PREVENT_MONTH_COUNT = 'ownership-prevention-month-count';
    //public static final String REQUEST_PROCESSING_WORKING_DAYS = 'no-action-auto-transfer-after-n-days';
    //public static final String UNTOUCHABLE_COUNT = 'priority-count';
    //public static final String TOTAL_COUNT = 'total-count'; 
    public static final String PARTIAL_OWNER_LAST_N_DAYS = 'partial-owner-interaction-n-days';
    public static final String OPEN_OPPORTUNITY_LAST_N_DAYS = 'check-open-opportunity-last-n-days';
    public static final String SECOND_EMAIL_NOTIFY_HOUR = 'email-notify-hour-gap-exp-date';
    public static final String FINAL_EMAIL_NOTIFY_HOUR = 'final-email-notify-hour-gap-exp-date';
    public static final String DROP_DEMOTE_CHECK_UPTO_N_DAYS = 'drop-demote-check-upto-n-days';
    //public static final String WAIT_TO_PICK_UP_AFTER_DROP = 'wait-to-pick-up-after-drop';
    //public static final String WAIT_TO_RE_PRIORITIZE_AFTER_DEMOTE = 'wait-to-re-prioritize-after-demote';
    public static final String DEMOTE_AUTO_STEAL_AVAILABLE_N_DAYS = 'demote-auto-steal-available-n-days';
    public static final String TOP_10_COUNT = 'n-top10-company';
    public static final String DEMOTE_AUTO_STEAL_FOR_N_DAYS_ALL = 'demote-auto-steal-for-n-days-all';
    public static final String DEMOTE_AUTO_STEAL_FOR_N_DAYS_PAR = 'demote-auto-steal-for-n-days-partial';
    
    
    public static final String TOTAL_COUNT = 'Total_Count__c'; 
    public static final String UNTOUCHABLE_COUNT = 'Priority_Count__c';
    public static final String OWNERSHIP_REQUEST_BALANCE = 'Ownership_Request_Balance__c';
    public static final String REQUEST_PROCESSING_WORKING_DAYS = 'No_Action_Auto_Transfer_After_N_Days__c';
    public static final String WAIT_TO_PICK_UP_AFTER_DROP = 'Wait_To_Pick_Up_After_Drop__c';
    public static final String WAIT_TO_RE_PRIORITIZE_AFTER_DEMOTE = 'Wait_To_Reprioritize_After_Demote__c';
    
    /***********************************************************************
     * @description used to maintain validation of given user
     * @return Map of userID and ValidationWrapper
     * @author 10K-Expert
     * @param List of userIds
     ***********************************************************************/
    public static Map<Id, ValidationWrapper> checkUnassignedValidation(List<Id> userIds){
        Set<Id> userIdSet = new Set<Id>();
        List<User> users = [SELECT Id,Priority_Company_Count__c, Watch_Company_Count__c, Ownership_Request_Balance__c FROM User WHERE Id IN :userIds];
        
        for(User user : users){
            user.Priority_Company_Count__c = user.Priority_Company_Count__c != NULL ? user.Priority_Company_Count__c : 0 ;
            user.Watch_Company_Count__c = user.Watch_Company_Count__c != NULL ? user.Watch_Company_Count__c : 0 ;
            userIdSet.add(user.Id);
        }
        
        //Calculating requestBalance here with SOQL for n days via custom setting
        //Integer requestBal = IVPOwnershipService.ownershipRequestBalance;
         Integer requestBal = 0;
        Integer ownershipRequestBalanceDays = IVPOwnershipService.ownershipRequestBalanceDays;
        DateTime last30daysDateTime = DateTime.now().addDays(-ownershipRequestBalanceDays);
        
        Map<Id, Integer> userIdWithBalance = new Map<Id, Integer>();
        
        for(List<AggregateResult> results : [SELECT Request_By__c requester, Count(Id) cnt FROM Company_Request__c
                                                WHERE CreatedDate >= :last30daysDateTime
                                                AND Request_By__c in :userIds AND Status__c = 'Pending' 
                                                GROUP BY Request_By__c]){
            for(AggregateResult result : results){
                Integer cnt = Integer.valueOf(result.get('cnt') != NULL ? result.get('cnt') : 0);
                Integer newBalance = 0;
                Id requesterId = (Id)result.get('requester');
                newBalance = cnt;
                newBalance = newBalance < 0 ? 0 : newBalance;
                userIdWithBalance.put(requesterId, newBalance);
            }
        }
        Map<String, Integer> userWithRequestCount = getUserWithRequestCount(userIds);
        // Integer untouchableCount = getGeneralConfigValue(UNTOUCHABLE_COUNT);
        // Integer totalCount = getGeneralConfigValue(TOTAL_COUNT);
        Integer untouchableCount = 0;
        Integer totalCount = 0;

        // getting the map for the users having ownership assignement map
        Map<Id, Ownership_Assignment_Config__c> ownershipAssignmentsMap = IVPOwnershipService.getOwnershipAssignmentsMap(userIdSet);
        Map<Id, ValidationWrapper> validationWrapperMap = new Map<Id, ValidationWrapper>();
        for(User user : users){
            
            requestBal = ownershipAssignmentsMap.containsKey(user.Id) 
            && ownershipAssignmentsMap.get(user.Id).Ownership_Request_Balance__c!=null ? Integer.ValueOf(ownershipAssignmentsMap.get(user.Id).Ownership_Request_Balance__c) : 0;
            
            // getting the untouchableCount from ownershipAssignmentsMap based on particular user Id
            untouchableCount = ownershipAssignmentsMap.containsKey(user.Id) 
            && ownershipAssignmentsMap.get(user.Id).Priority_Count__c!=null ? Integer.ValueOf(ownershipAssignmentsMap.get(user.Id).Priority_Count__c) : 0;
            
            // getting the totalCount from ownershipAssignmentsMap based on particular user Id 
            totalCount = ownershipAssignmentsMap.containsKey(user.Id) 
            && ownershipAssignmentsMap.get(user.Id).Total_Count__c!=null ? Integer.ValueOf(ownershipAssignmentsMap.get(user.Id).Total_Count__c) : 0;     
            
            ValidationWrapper validationWrapper = new ValidationWrapper();
            Integer companyRqst = userIdWithBalance.containsKey(user.Id) ? userIdWithBalance.get(user.Id) : 0 ;
            validationWrapper.requestBalance = requestBal - (userWithRequestCount.containsKey(user.Id) ? userWithRequestCount.get(user.Id) : 0 );
            system.debug('validationWrapper.requestBalance '+validationWrapper.requestBalance);
            validationWrapper.untouchableCount = untouchableCount - Integer.valueOf(user.Priority_Company_Count__c != NULL ? user.Priority_Company_Count__c : 0) - companyRqst;
            system.debug('validationWrapper.untouchableCount '+validationWrapper.untouchableCount);
            validationWrapper.watchlistCount = totalCount - Integer.valueOf(user.Watch_Company_Count__c != NULL ? user.Watch_Company_Count__c : 0) - Integer.valueOf(user.Priority_Company_Count__c != NULL ? user.Priority_Company_Count__c : 0) - companyRqst;
            system.debug('validationWrapper.watchlistCount '+validationWrapper.watchlistCount);
            
            validationWrapperMap.put(user.Id, validationWrapper);
        }
        
        return validationWrapperMap;
    }
    /***********************************************************************
     * @description This method will return value from custom setting
     * @return Integer
     * @author 10K-Expert
     * @param Need name of General config custom setting
     ***********************************************************************/
    public static Integer getGeneralConfigValue(String key){
        IVP_General_Config__c config = IVP_General_Config__c.getValues(key);
        if(config != NULL){
            return Integer.valueOf(config.Value__c);
        }
        return NULL;
    }
    
    public static Integer getOwnershipConfigValue(String key){
        // getting map of Ownership_Assignment_Config for all owners
        Map<Id, Ownership_Assignment_Config__c> ownershipAssignmentsMap = IVPOwnershipService.getOwnershipAssignmentsMap(new Set<Id>{UserInfo.getUserId()});
        Integer config  = ownershipAssignmentsMap.containsKey(UserInfo.getUserId()) 
            && ownershipAssignmentsMap.get(UserInfo.getUserId()).get(key)!=Null?Integer.ValueOf(ownershipAssignmentsMap.get(UserInfo.getUserId()).get(key)) :null;
        //IVP_General_Config__c config = IVP_General_Config__c.getValues(key);
        if(config != NULL){
            return config;
        }
        return NULL;
    }
    /***********************************************************************
     * @description This method will return value from custom setting
     * @return String
     * @author 10K-Expert
     * @param Need name of General config custom setting
     ***********************************************************************/
    public static String getGeneralConfigStrValue(String key){
        IVP_General_Config__c config = IVP_General_Config__c.getValues(key);
        if(config != NULL){
            return config.Value__c;
        }
        return NULL;
    }
    /***********************************************************************
     * @description This method will reduce request balance of specifed user
     * @return Integer
     * @author 10K-Expert
     * @param Id of user
     ***********************************************************************/
    /*public static void reduceUserRequest(Id userId){
        List<User> users = [SELECT Id, Ownership_Request_Balance__c FROM User WHERE Id = :userId LIMIT 1];
        users[0].Ownership_Request_Balance__c = users[0].Ownership_Request_Balance__c - 1;
        update users;
    }*/
    // Use property ownershipRequestBalance instead of following method
    /*public static Integer getOwnershipRequestBalance(){
        return getGeneralConfigValue(OWNERSHIP_REQUEST_BALANCE);
    }
    */
    /***********************************************************************
     * @description Property is returning number of custom setting bind with UNTOUCHABLE_COUNT
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer getUntouchableCount(){
        return getGeneralConfigValue(UNTOUCHABLE_COUNT);
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with TOTAL_COUNT
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer getTotalCount(){
        return getGeneralConfigValue(TOTAL_COUNT);
    }
    /***********************************************************************
     * @description Property is returning number of custom setting bind with UNTOUCHABLE_COUNT
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer priorityBalance{
        /*  get{
if(priorityBalance == NULL){
priorityBalance = getGeneralConfigValue(UNTOUCHABLE_COUNT);
if(priorityBalance == NULL){
priorityBalance = 0;
}
}
return priorityBalance;
}set;
}*/
        get{
            if(priorityBalance == NULL){
                priorityBalance = getOwnershipConfigValue(UNTOUCHABLE_COUNT);
                if(priorityBalance == NULL){
                    priorityBalance = 0;
                }
            }
            return priorityBalance;
        }set;
    }
    /***********************************************************************
     * @description Property is returning number of custom setting bind with UNTOUCHABLE_COUNT
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer totalCompanyBalance{
        /*  get{
if(totalCompanyBalance == NULL){
totalCompanyBalance = getGeneralConfigValue(TOTAL_COUNT);
if(totalCompanyBalance == NULL){
totalCompanyBalance = 0;
}
}
return totalCompanyBalance;
}set;
}*/
        get{
            if(totalCompanyBalance == NULL){
                totalCompanyBalance = getOwnershipConfigValue(TOTAL_COUNT);
                if(totalCompanyBalance == NULL){
                    totalCompanyBalance = 0;
                }
            }
            return totalCompanyBalance;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with OWNERSHIP_REQUEST_BALANCE
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer ownershipRequestBalance{
        /*get{
            if(ownershipRequestBalance == NULL){
                ownershipRequestBalance = getGeneralConfigValue(OWNERSHIP_REQUEST_BALANCE);
                if(ownershipRequestBalance == NULL){
                    ownershipRequestBalance = 0;
                }
            }
            return ownershipRequestBalance;
        }set;
    }*/
        get{
            if(ownershipRequestBalance == NULL){
                ownershipRequestBalance = getOwnershipConfigValue(OWNERSHIP_REQUEST_BALANCE);
                if(ownershipRequestBalance == NULL){
                    ownershipRequestBalance = 0;
                }
            }
            return ownershipRequestBalance;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with OWNERSHIP_REQUEST_BALANCE_DAYS
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer ownershipRequestBalanceDays{
        get{
            if(ownershipRequestBalanceDays == NULL){
                ownershipRequestBalanceDays = getGeneralConfigValue(OWNERSHIP_REQUEST_BALANCE_DAYS);
                if(ownershipRequestBalanceDays == NULL){
                    ownershipRequestBalanceDays = 0;
                }
            }
            return ownershipRequestBalanceDays;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with REQUEST_PROCESSING_WORKING_DAYS
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer requestProcessingWorkingHours{
        /*get{
            if(requestProcessingWorkingHours == NULL){
                requestProcessingWorkingHours = getGeneralConfigValue(REQUEST_PROCESSING_WORKING_DAYS);
                if(requestProcessingWorkingHours == NULL){
                    requestProcessingWorkingHours = 0;
                }
            }
            return requestProcessingWorkingHours;
        }set;*/
        get{
            if(requestProcessingWorkingHours == NULL){
                requestProcessingWorkingHours = getOwnershipConfigValue(REQUEST_PROCESSING_WORKING_DAYS);
                if(requestProcessingWorkingHours == NULL){
                    requestProcessingWorkingHours = 0;
                }
            }
            return requestProcessingWorkingHours;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with PARTIAL_OWNER_LAST_N_DAYS
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer partialOwnerLastInterationDays{
        get{
            if(partialOwnerLastInterationDays == NULL){
                partialOwnerLastInterationDays = getGeneralConfigValue(PARTIAL_OWNER_LAST_N_DAYS);
                if(partialOwnerLastInterationDays == NULL){
                    partialOwnerLastInterationDays = 0;
                }
            }
            return partialOwnerLastInterationDays;
        }set;
    }
    /***********************************************************************
     * @description Property is returning number of custom setting bind with OPEN_OPPORTUNITY_LAST_N_DAYS
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer openOpportunityLastInterationMonth{
        get{
            if(openOpportunityLastInterationMonth == NULL){
                openOpportunityLastInterationMonth = getGeneralConfigValue(OPEN_OPPORTUNITY_LAST_N_DAYS);
                if(openOpportunityLastInterationMonth == NULL){
                    openOpportunityLastInterationMonth = 0;
                }
            }
            return openOpportunityLastInterationMonth;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with TOP_10_COUNT
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer top10Count{
        get{
            if(top10Count == NULL){
                top10Count = getGeneralConfigValue(TOP_10_COUNT);
                if(top10Count == NULL){
                    top10Count = 0;
                }
            }
            return top10Count;
        }set;
    }
    /***********************************************************************
     * @description Property is returning number of custom setting bind with SECOND_EMAIL_NOTIFY_HOUR
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer secondEmailNotifyHourGap{
        get{
            if(secondEmailNotifyHourGap == NULL){
                secondEmailNotifyHourGap = getGeneralConfigValue(SECOND_EMAIL_NOTIFY_HOUR);
                if(secondEmailNotifyHourGap == NULL){
                    secondEmailNotifyHourGap = 0;
                }
            }
            return secondEmailNotifyHourGap;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with FINAL_EMAIL_NOTIFY_HOUR
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer finalEmailNotifyHourGap{
        get{
            if(finalEmailNotifyHourGap == NULL){
                finalEmailNotifyHourGap = getGeneralConfigValue(FINAL_EMAIL_NOTIFY_HOUR);
                if(finalEmailNotifyHourGap == NULL){
                    finalEmailNotifyHourGap = 0;
                }
            }
            return finalEmailNotifyHourGap;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with WAIT_TO_PICK_UP_AFTER_DROP
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer waitToPickUpAfterDrop{
        /*get{
            if(waitToPickUpAfterDrop == NULL){
                waitToPickUpAfterDrop = getGeneralConfigValue(WAIT_TO_PICK_UP_AFTER_DROP);
                if(waitToPickUpAfterDrop == NULL){
                    waitToPickUpAfterDrop = 0;
                }
            }
            return waitToPickUpAfterDrop;
        }set;*/
        get{
            if(waitToPickUpAfterDrop == NULL){
                waitToPickUpAfterDrop = getOwnershipConfigValue(WAIT_TO_PICK_UP_AFTER_DROP);
                if(waitToPickUpAfterDrop == NULL){
                    waitToPickUpAfterDrop = 0;
                }
            }
            return waitToPickUpAfterDrop;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with WAIT_TO_RE_PRIORITIZE_AFTER_DEMOTE
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer waitToRePrioritizeAfterDemote{
        /*get{
            if(waitToRePrioritizeAfterDemote == NULL){
                waitToRePrioritizeAfterDemote = getGeneralConfigValue(WAIT_TO_RE_PRIORITIZE_AFTER_DEMOTE);
                if(waitToRePrioritizeAfterDemote == NULL){
                    waitToRePrioritizeAfterDemote = 0;
                }
            }
            return waitToRePrioritizeAfterDemote;
        }set;*/
        get{
            if(waitToRePrioritizeAfterDemote == NULL){
                waitToRePrioritizeAfterDemote = getOwnershipConfigValue(WAIT_TO_RE_PRIORITIZE_AFTER_DEMOTE);
                if(waitToRePrioritizeAfterDemote == NULL){
                    waitToRePrioritizeAfterDemote = 0;
                }
            }
            return waitToRePrioritizeAfterDemote;
        }set;
    }
    
    /***********************************************************************
     * @description Property is returning number of custom setting bind with DEMOTE_AUTO_STEAL_FOR_N_DAYS_PAR
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer demoteAutoStealAvailableForPartial{
        get{
            if(demoteAutoStealAvailableForPartial == NULL){
                demoteAutoStealAvailableForPartial = getGeneralConfigValue(DEMOTE_AUTO_STEAL_FOR_N_DAYS_PAR);
                if(demoteAutoStealAvailableForPartial == NULL){
                    demoteAutoStealAvailableForPartial = 0;
                }
            }
            return demoteAutoStealAvailableForPartial;
        }set;
    }
    /***********************************************************************
     * @description Property is returning number of custom setting bind with DEMOTE_AUTO_STEAL_FOR_N_DAYS_ALL
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer demoteAutoStealAvailableForAll{
        get{
            if(demoteAutoStealAvailableForAll == NULL){
                demoteAutoStealAvailableForAll = getGeneralConfigValue(DEMOTE_AUTO_STEAL_FOR_N_DAYS_ALL);
                if(demoteAutoStealAvailableForAll == NULL){
                    demoteAutoStealAvailableForAll = 0;
                }
            }
            return demoteAutoStealAvailableForAll;
        }set;
    }
    /***********************************************************************
     * @description Property is returning number of custom setting bind with DEMOTE_AUTO_STEAL_AVAILABLE_N_DAYS
     * @return Integer
     * @author 10K-Expert
     ***********************************************************************/
    public static Integer demoteAutoStealAvailableInteractionDays{
        get{
            if(demoteAutoStealAvailableInteractionDays == NULL){
                demoteAutoStealAvailableInteractionDays = getGeneralConfigValue(DEMOTE_AUTO_STEAL_AVAILABLE_N_DAYS);
                if(demoteAutoStealAvailableInteractionDays == NULL){
                    demoteAutoStealAvailableInteractionDays = 0;
                }
            }
            return demoteAutoStealAvailableInteractionDays;
        }set;
    }
    
    /***********************************************************************
     * @description prepare map of Ownership_Assignment_Config for specific ids
     * @return Map of Ownership_Assignment_Config
     * @author 10K-Expert
     * @param set of userIds
     ***********************************************************************/
    public static Map<Id, Ownership_Assignment_Config__c> getOwnershipAssignmentsMap(Set<Id> userIds){
        Map<Id, Ownership_Assignment_Config__c> ownershipAssignmentsMap = new Map<Id, Ownership_Assignment_Config__c>();
        for(Id userId : userIds){
            ownershipAssignmentsMap.put(userId, Ownership_Assignment_Config__c.getInstance(userId));
        }
        return ownershipAssignmentsMap;
    }
    
    /***********************************************************************
     * @description preparing request count with respect to passed` user ids
     * @return Map<String, Integer> 
     * @author 10K-Expert
     * @param List of user Id
     ***********************************************************************/
    public static Map<String, Integer> getUserWithRequestCount(List<Id> usersId){
        Integer ownershipRequestBalanceDays = IVPOwnershipService.ownershipRequestBalanceDays;
        DateTime last30daysDateTime = DateTime.now().addDays(-ownershipRequestBalanceDays);
        
        Map<String, Integer> userWithRequestCount = new Map<String, Integer>();
        if(ownershipRequestBalanceDays != 0){
            for(List<AggregateResult> results : [SELECT Request_By__c requester, Count(Id) cnt
                                                FROM Company_Request__c 
                                                WHERE CreatedDate >= :last30daysDateTime AND Request_By__c in :usersId
                                                GROUP BY Request_By__c]){
                
                for(AggregateResult result : results){
                    String requesterId = (String)result.get('requester');
                    Integer cnt = Integer.valueOf(result.get('cnt'));
                    userWithRequestCount.put(requesterId, cnt);
                }
            }
        }
        if(userWithRequestCount.size() < usersId.size()){
            for(Id userId : usersId){
                //sending 0 if no company request is there for a user
                userWithRequestCount.put(userId, 0);
            }
        }
        return userWithRequestCount;
    }
     /***********************************************************************
     * @description This Method checks if the user has the bypass or not
     * @return boolean
     * @author Vineet Bhagchandani
     * @param Set of Ids
     ***********************************************************************/
    public static Map<Id,boolean> checkUserHavingBypass(Set<Id> userIds){
        Map<Id,boolean> usersWithBypass = new Map<Id,boolean>();
        Map<Id, Ownership_Assignment_Config__c> ownershipAssignmentsMap = IVPOwnershipService.getOwnershipAssignmentsMap(userIds);
        
        for(Id userId : userIds)
        {
            if(ownershipAssignmentsMap.containsKey(userId) && ownershipAssignmentsMap.get(userId)!=Null)
            {
               // Boolean byPassOwnershipValidation = ownershipAssignmentsMap.containsKey(userId) 
                //    && ownershipAssignmentsMap.get(userId).Bypass_Ownership_checks__c;
                //Need to change to this once test is done
                Boolean byPassOwnershipValidation = ownershipAssignmentsMap.containsKey(userId) 
                    && (ownershipAssignmentsMap.get(userId).Bypass_Ownership_Validations__c!=Null?(ownershipAssignmentsMap.get(userId).Bypass_Ownership_Validations__c.equalsIgnoreCase('Y')?true:false):false);
                System.debug('byPassOwnershipValidation '+byPassOwnershipValidation);
                usersWithBypass.put(userId, byPassOwnershipValidation);
            }
        }
       
        
        return usersWithBypass;
    }
    
}