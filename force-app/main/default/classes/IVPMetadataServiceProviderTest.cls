@isTest
private class IVPMetadataServiceProviderTest {
	@testSetup static void dataSetup(){
		IVPTestFuel tFuel = new IVPTestFuel('MD');
		GroupMember[] groupMembers = tFuel.groupMembers;
	}
	@isTest static void testExtractOrgListView(){
		IVPTestFuel tFuel = new IVPTestFuel('MD');
		tFuel.populateLVSettings();
		IVP_SD_ListView__c sdLV = IVPUtils.currentOrgSetting();
		Test.startTest();
			IVPMetadataService.Listview lv = IVPMetadataServiceProvider.extractListView(sdLV);
		Test.stopTest();
	}
	
	@isTest static void testExtractUserListView(){
		IVPTestFuel tFuel = new IVPTestFuel('MD');
		tFuel.populateLVSettings(true, false);
		IVP_SD_ListView__c sdLV = IVPUtils.currentUserSetting();
		Test.startTest();
			IVPMetadataService.Listview lv = IVPMetadataServiceProvider.extractListView(sdLV);
		Test.stopTest();
	}
	
	@isTest static void testMaintainListViewTagWithOrgDefault(){
		IVPTestFuel tFuel = new IVPTestFuel('MDTagA');
		tFuel.populateLVSettings();
		Test.startTest();
		
		IVPWatchDogMock.defaultRepsonse = false;
		Map<String, Object> valueMap = new Map<String, Object>{'chkFilter'=>true, 'fieldApi'=>'Tags__c', 
															'filterScope'=>'Everything', 'selectedTagId'=>'', 
															'selectedValue'=>'May_24', 'source'=>'tag', 'sourceType'=>'advanced'};
		Map<String, Object> response = IVPMetadataServiceProvider.maintainListView(valueMap);
		Test.stopTest();
		System.assert(response!=NULL);
		System.assert(Boolean.valueOf(response.get('success')), 'ListView not modified');
		System.assert(!response.containsKey('error'), 'Some error founds!');
	}
	
	@isTest static void testMaintainListViewTagWithouUserLV(){
		IVPTestFuel tFuel = new IVPTestFuel('MDTagO');
		tFuel.populateLVSettings(true, false);
		Test.startTest();
		Map<String, Object> valueMap = new Map<String, Object>{'chkFilter'=>false, 'fieldApi'=>'Tags__c', 
															'filterScope'=>'Everything', 'selectedTagId'=>'', 
															'selectedValue'=>'May_24', 'source'=>'tag', 'sourceType'=>'advanced'};
		Map<String, Object> response = IVPMetadataServiceProvider.maintainListView(valueMap);
		Test.stopTest();
		System.assert(response!=NULL);
		System.assert(Boolean.valueOf(response.get('success')), 'ListView not modified');
		System.assert(!response.containsKey('error'), 'Some error founds!');
	}
	@isTest static void testMaintainListViewChart(){
		IVPTestFuel tFuel = new IVPTestFuel('MDChrtA');
		tFuel.populateLVSettings();
		Test.startTest();
		
		Map<String, Object> valueMap = new Map<String, Object>{'chkFilter'=>false, 'fieldApi'=>'Owner_Status_Calc__c', 
															'selectedValue'=>'Expiring Soon', 'source'=>'Chart', 
															'viewName'=>'chart_Expiring Soon'};
		Map<String, Object> response = IVPMetadataServiceProvider.maintainListView(valueMap);
		Test.stopTest();
		System.assert(response!=NULL);
		System.assert(Boolean.valueOf(response.get('success')), 'ListView not modified');
		System.assert(!response.containsKey('error'), 'Some error founds!');
	}
	@isTest static void testMaintainListViewChartWithouUserLV(){
		IVPTestFuel tFuel = new IVPTestFuel('MDChrtO');
		tFuel.populateLVSettings(true, false);
		Test.startTest();
		
		Map<String, Object> valueMap = new Map<String, Object>{'fieldApi'=>'Owner_Status_Calc__c', 
															'selectedValue'=>'Expiring Soon', 'source'=>'Chart', 
															'viewName'=>'chart_Expiring Soon'};
		Map<String, Object> response = IVPMetadataServiceProvider.maintainListView(valueMap);
		Test.stopTest();
		System.assert(response!=NULL);
		System.assert(Boolean.valueOf(response.get('success')), 'ListView not modified');
		System.assert(!response.containsKey('error'), 'Some error founds!');
	}
	@isTest static void testShareListViewiWithGroup(){
		IVPTestFuel tFuel = new IVPTestFuel('MDChrtG');
		tFuel.populateLVSettings();
		Test.startTest();
		
		Map<String, Object> valueMap = new Map<String, Object>{'fieldApi'=>'Owner_Status_Calc__c', 
															'selectedValue'=>'Expiring Soon', 'source'=>'Chart', 
															'viewName'=>'chart_Expiring Soon'};
		List<Group> groups = [Select Id,DeveloperName FROM Group LIMIT 1];
		IVP_SD_ListView__c sdLV = IVPUtils.currentUserSetting();
		IVPMetadataServiceProvider.ShareListViewiWithGroup(sdLV.List_View_Name__c,groups[0].DeveloperName);
		Test.stopTest();
	}
}