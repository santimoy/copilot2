@isTest
public class IVPUserService_Test {
    private static testMethod void testGoupAndUser(){
    	IVPTestFuel test=new IVPTestFuel();
        List<Group> grp=test.groups;
        List<String> groupName=new List<string>();
        groupName.add('Onsite');
        groupName.add('Onsite_Management');
        IVPUserService userservice=new IVPUserService();
        List<Group> grp1=IVPUserService.getUserGroups(groupName);
        system.debug('group nazia'+grp1.size());
        List<Group> groups = new List<Group>();
        String groupQuery = 'SELECT Id FROM Group WHERE DeveloperName IN :groupName';
        for( Group groupObj: ( List<Group> ) Database.query(groupQuery) ) {
            groups.add(groupObj);
        }
        system.assertEquals(groups.size(),grp1.size());
        set<Id> setid=IVPUserService.getGroupMembersIDs(grp1);
        Set<Id> groupMembersIDs = new Set<Id>();
        Set<Id> groupIDs = ( new Map<Id, Group>(grp1) ).keySet(); 
        String groupMemberQuery = 'SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN :groupIDs';
        for(GroupMember member: ( List<GroupMember> ) Database.query(groupMemberQuery)){
           groupMembersIDs.add(member.UserOrGroupId);
        }
        system.assertEquals(groupMembersIDs.size(),setid.size());
        
        List<User> usr=IVPUserService.getUsersByGroup(groupName);
        
        List<User> users = new List<User>();
        Set<Id> groupUserIds = IVPUserService.getGroupMembersIDs(IVPUserService.getUserGroups(groupName));
        String userQuery = 'SELECT Id FROM User WHERE IsActive = True AND Id IN :groupUserIds';
        for( User user: ( List<User> ) Database.query(userQuery) ) {
            users.add(user);
        }
      	system.assertEquals(users.size(),usr.size());
        
    }
    private static testMethod void test_isCurrentUserSystemAdmin(){
        Boolean flag=IVPUserService.isCurrentUserSystemAdmin();
        system.assertEquals(true,flag);
    }
}