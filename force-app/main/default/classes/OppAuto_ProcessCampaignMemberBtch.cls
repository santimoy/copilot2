/**
*@name      	: OppAut_ProcessCampaignMemberBtch
*@developer 	: 10K developer
*@date      	: 2018-09-27
*@description   : This Batch is use to do Opportunity Atutomation, We creates Opportunity, Contact Role,  Campaign Company
					, Campaign Influence Records From this batch.
*Frequency      : 30 minute.
**/
public class OppAuto_ProcessCampaignMemberBtch implements Database.Batchable<sObject>, Schedulable, Database.Stateful{
    public Boolean isCallingFromDetailPage = false;
    public Boolean isAbort = false;
    DateTime lastExecutionTime;
    String campaignId;
    Set<String> campaignIds;
    Map<String, Opportunity> oppNameWithOpportunityMap = new Map<String, Opportunity>();
    Map<String, String> oppNameWithOppObjForContactRole = new Map<String, String>();
    Set<String> processedOppNames = new Set<String>();
    Set<String> campMemberStatus;
    String attendeTypeToProcess;
    public Boolean isProcessExistingMembers = false;
    String campIdToProcess;
    boolean isChanged = false;
    public List<User> users = new List<User>();
	Map<String, Opportunity> opportunityToUpdateSatgeMap = new Map<String, Opportunity>();
    Map<Id, List<CampaignMember>> campIdWithPortfolioMembers = new Map<Id, List<CampaignMember>>();
	Map<Id, List<CampaignMember>> campIdWithGuestMembers = new Map<Id, List<CampaignMember>>();
	Set<String> updatedOppNames = new Set<String>();
    CampaignMember currentCampMember;
    public OppAuto_ProcessCampaignMemberBtch(){
        campaignId = '';
        campMemberStatus = new Set<String>();
    }
    public OppAuto_ProcessCampaignMemberBtch(Boolean isProcessExistingMembers){
    	this.isProcessExistingMembers = isProcessExistingMembers;
    }
    public OppAuto_ProcessCampaignMemberBtch(String campaignId){
       this.campaignId = campaignId;
    }
    public OppAuto_ProcessCampaignMemberBtch(Set<String> campaignIds){
       this.campaignIds = campaignIds;
    }
    public void execute(SchedulableContext SC) {
        Integer batchSize = 1;
        IVP_General_Config__c generalConfig = IVP_General_Config__c.getValues('Job Batch Size');
        if(generalConfig != NULL && String.isNotBlank(generalConfig.Value__c)){
        	batchSize = Integer.valueOf(generalConfig.Value__c);
        }
       	Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(), batchSize);
    }

    /*
     	If Custom Setting exists with Last Execution Time of the Job then we have to query only those records
		who is having Ready for processing time more then Last Executed On time else we are querying all CampaignMember's
		Records.
	*/
    public Database.QueryLocator start(Database.BatchableContext bc){
        campMemberStatus = OppAuto_Service.getCampaignMembersStatus();
        DateTime currentTime = System.now();

        String query = '  SELECT Id, Name, Campaign.Type,Description, Ready_For_Processing_On__c, Portfolio_Company_ID__c, Contact.AccountId,Contact.Account.Name '
                                                         + ', Campaign.Portfolio_Company__c, Attendee_Type__c, Opportunity__c'
                                                         + ', CampaignId, Company_Name__r.Name, ContactId, Company_Name__c, Status, Meeting_Date__c'
                                                         + ', Campaign.Portfolio_Company__r.Name,Portfolio_Company__c,Campaign.Name, Campaign.StartDate '
                                                         + ' FROM CampaignMember WHERE Company_Name__r.Name != \'Insight Ignite\' AND (Company_Name__c != NULL OR Contact.AccountId != NULL)  AND Campaign.RecordType.Name = \'Ignite Campaign\' AND '
             											 + ' (((Campaign.Type = \'Dinner / Roundtable\' OR Campaign.Type = \'Innovation Briefing\') AND Status = \'Attended\' )'
                                                         + ' OR (Campaign.Type = \'Portfolio Company Demand Gen\' AND Status IN : campMemberStatus AND Campaign.Portfolio_Company__c != NULL AND Campaign.Portfolio_Company__r.Name != \'Insight Ignite\' ))';


        IVP_General_Config__c generalConfig = IVP_General_Config__c.getValues('OppAuto_UserName');

        if(generalConfig != NULL && String.isNotBlank(generalConfig.Value__c)) {
            users = Database.query('SELECT Id,Name FROM User WHERE UserName LIKE \'' + generalConfig.Value__C + '%\'');
        }
        if(isProcessExistingMembers){
            query += ' AND Campaign.RecordType.Name  = \'Ignite Campaign\'';
        }else if(String.isNotBlank(campaignId)){
            query += ' AND CampaignId =:campaignId';

        }else if(campaignIds != NULL && !campaignIds.isEmpty()){
            query += ' AND CampaignId IN :campaignIds';
        }else{
            generalConfig = IVP_General_Config__c.getValues('Job Last Executed On');

            if(generalConfig != NULL && String.isNotBlank(generalConfig.Value__c)) {
                lastExecutionTime = DateTime.valueOf(generalConfig.Value__c);
                generalConfig.Value__c = currentTime.format('yyyy-MM-dd HH:mm:ss');

                update generalConfig;
                //query += ' AND Id = \'00v0R000003H7pV\'';
                query += 'AND Ready_For_Processing_On__c >= :lastExecutionTime AND Ready_For_Processing_On__c <= :currentTime';
            }else{
                /* If Custom Setting is not exists then We have to create Custom Setting */
                if(generalConfig == NULL)
                    generalConfig = new IVP_General_Config__c(Name = 'Job Last Executed On');

                generalConfig.Value__c = currentTime.format('yyyy-MM-dd HH:mm:ss');
                upsert generalConfig;
            }
        }
        system.debug('query===='+query);
        return Database.getQueryLocator(query + ' ORDER BY CampaignID,Attendee_Type__c,Opp_Auto_Stage_Priority__c ASC');
    }

    public void execute(Database.BatchableContext bc, List<CampaignMember> campMembers){
        try{
            System.debug('campMembers===='+campMembers);
            Set<Id> campaignIds = new Set<Id>();
        	Set<String> campMembersStatus = OppAuto_Service.getCampaignMembersStatus();
            Set<String> opportunityNames = new Set<String>();
            String campTypeToProcess = '';
            String attendeeType = '';
            String campId = '';
            currentCampMember = campMembers[0];
            oppNameWithOpportunityMap = new Map<String, Opportunity>();
            //This map contains Opp Name with List of Member which is not able create opportunity because opportunity is allready there but this map will create Contact Role
          	Map<String, List<CampaignMember>> oppNameWithMemberList = new Map<String, List<CampaignMember>>();
            for(CampaignMember campMember : campMembers){

                campaignIds.add(campMember.CampaignId);
                campId = campMember.CampaignId;
                campTypeToProcess = campMember.Campaign.Type;

                if(campTypeToProcess == 'Portfolio Company Demand Gen'){
                    attendeeType = 'Executive Guest';
                }else if(campTypeToProcess == 'Dinner / Roundtable' || campTypeToProcess == 'Innovation Briefing' ){
                    if(campMember.Attendee_Type__c == 'Executive Guest'){
                        attendeeType = 'Portfolio Executive';
                    }else if(campMember.Attendee_Type__c == 'Portfolio Executive'){
                        attendeeType = 'Executive Guest';
                    }
                }
            }

            if(campId != campIdToProcess || attendeeType != attendeTypeToProcess){
                attendeTypeToProcess = attendeeType;
                campIdToProcess = campId;
                isChanged = true;
            }else{
                isChanged = false;
            }
            if(isChanged){
                campIdWithPortfolioMembers = new Map<Id, List<CampaignMember>>();
                campIdWithGuestMembers = new Map<Id, List<CampaignMember>>();
                System.debug('campTypeToProcess==' + campTypeToProcess);
                System.debug('attendeTypeToProcess==' + attendeTypeToProcess);
                System.debug('campaignIds==' + campaignIds);
                List<CampaignMember> campMembersToProcess = [SELECT Id, Name, Campaign.Type,Description, Ready_For_Processing_On__c,Portfolio_Company_ID__c
                                                             , Campaign.Portfolio_Company__c, Attendee_Type__c, Opportunity__c
                                                             , CampaignId, Company_Name__r.Name, ContactId, Company_Name__c, Status, Meeting_Date__c
                                                             , Campaign.Portfolio_Company__r.Name,Contact.AccountId,Contact.Account.Name,Portfolio_Company__c,Campaign.Name, Campaign.StartDate
                                                             FROM CampaignMember
                                                             WHERE Company_Name__r.Name != 'Insight Ignite' AND (Company_Name__c != NULL OR Contact.AccountId != NULL) AND Campaign.RecordType.Name = 'Ignite Campaign' AND CampaignId IN :campaignIds
                                                             AND (Campaign.Type = :campTypeToProcess AND Attendee_Type__c =: attendeTypeToProcess)
                                                             AND (((Campaign.Type = 'Dinner / Roundtable' OR Campaign.Type = 'Innovation Briefing') AND Status = 'Attended')
                                                                  OR (Campaign.Type = 'Portfolio Company Demand Gen' AND Status IN : campMemberStatus AND Campaign.Portfolio_Company__c != NULL AND Campaign.Portfolio_Company__r.Name != 'Insight Ignite'))];
                /* Here we are preparing Portfolio and Guest Members Map*/
                System.debug('campMembersToProcess==' + campMembersToProcess);
                for(CampaignMember campMember : campMembersToProcess){
                    if(String.isNotBlank(campMember.Attendee_Type__c) && String.isNotBlank(campMember.Campaign.Type)){
                        if(campMember.Attendee_Type__c.equals('Portfolio Executive')){
                            addCampaignMemberInMap(campIdWithPortfolioMembers, campMember);

                        }else if(campMember.Attendee_Type__c.equals('Executive Guest')){
                            addCampaignMemberInMap(campIdWithGuestMembers, campMember);
                        }
                    }
                }

            }

        	/* Here we are prepairing the unique Opportunity Names Set*/
            for(CampaignMember campMember : campMembers){
                if(String.isNotBlank(campMember.Attendee_Type__c)){
                    if(CampMember.Campaign.Type.equals('Innovation Briefing') || CampMember.Campaign.Type.equals('Dinner / Roundtable')){
                        if(campMember.Attendee_Type__c.equals('Portfolio Executive')){

                            OppAuto_Service.prepareOpportunityNamesSet(campIdWithGuestMembers.containsKey(campMember.CampaignId) ? campIdWithGuestMembers.get(campMember.CampaignId) : new List<CampaignMember>()
                                                                       , campMember, opportunityNames);

                        }else if(campMember.Attendee_Type__c.equals('Executive Guest')){

                            OppAuto_Service.prepareOpportunityNamesSet(campIdWithPortfolioMembers.containsKey(campMember.CampaignId) ? campIdWithPortfolioMembers.get(campMember.CampaignId) : new List<CampaignMember>()
                                                                       , campMember, opportunityNames);
                        }
                    }else if(campMember.Campaign.Type == 'Portfolio Company Demand Gen'){
                        OppAuto_Service.prepareOpportunityNamesSet(new List<CampaignMember>{campMember}, campMember, opportunityNames);
                    }

                }
            }

            List<Opportunity> oppsToUpdate = new List<Opportunity>();
        	for(Opportunity oppObj : [SELECT Id,Name,Description,Amount, First_Meeting_Date__c,StageName,OppAuto_Opportunity_New_Name__c,CampaignId FROM Opportunity
                                      WHERE  OppAuto_Opportunity_New_Name__c IN :opportunityNames AND RecordType.Name = 'Ignite Opportunity'
                                      AND StageName Not In ('Closed Won', 'Closed Lost', 'Duplicate') ORDER BY CreatedDate ASC]){
                if(oppObj.Amount == NULL)
                	oppObj.Amount = 0;

				if(!oppNameWithOpportunityMap.containsKey(oppObj.OppAuto_Opportunity_New_Name__c)){
					oppNameWithOpportunityMap.put(oppObj.OppAuto_Opportunity_New_Name__c, oppObj);
                    oppNameWithOppObjForContactRole.put(oppObj.OppAuto_Opportunity_New_Name__c , oppObj.Id);
                    updatedOppNames.add(oppObj.OppAuto_Opportunity_New_Name__c );
				}else{
                    oppsToUpdate.add(new Opportunity(Id = oppObj.Id, StageName = 'Duplicate', Amount = 0));
				}

            }

            update oppsToUpdate;
            System.debug('oppsToUpdate===' + oppsToUpdate);
            Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap = new Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper>();
            for(CampaignMember campMember : campMembers){
                if(String.isNotBlank(campMember.Attendee_Type__c)){
                    if(campMember.Attendee_Type__c.equals('Portfolio Executive') && (campMember.Campaign.Type.equals('Innovation Briefing')
                                                                                     || CampMember.Campaign.Type.equals('Dinner / Roundtable'))){

                        createOrUpdateOpportunity(campIdWithGuestMembers, campMember, opportunitiesToProcessMap, processedOppNames
                                                  , oppNameWithMemberList, oppNameWithOppObjForContactRole
                                                  , updatedOppNames, isProcessExistingMembers);
                    }else if(campMember.Attendee_Type__c.equals('Executive Guest') && (campMember.Campaign.Type.equals('Innovation Briefing')
                                                                                       || CampMember.Campaign.Type.equals('Dinner / Roundtable'))){

                        createOrUpdateOpportunity(campIdWithPortfolioMembers, campMember, opportunitiesToProcessMap, processedOppNames
                                                  , oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames, isProcessExistingMembers);
                    }else if(campMember.Attendee_Type__c.equals('Executive Guest') && campMember.Campaign.type == 'Portfolio Company Demand Gen'){

                        system.debug('=======In=========');
                        OppAuto_Service.createOrUpdateOpportunity(new List<CampaignMember>{campMember}, campMember, oppNameWithOpportunityMap
                                                                  , opportunitiesToProcessMap, (!users.isEmpty() ? users[0].Id : '')
                                                                  , processedOppNames, oppNameWithMemberList, oppNameWithOppObjForContactRole, updatedOppNames
                                                                  , isProcessExistingMembers);
                    }
                }
            }

        	List<Opportunity> opportunitiesToInsert = new List<Opportunity>();

            Map<Id,CampaignMember> campMembersToUpdateMap = new Map<Id,CampaignMember>();
            if(!opportunitiesToProcessMap.isEmpty()){
                for(OppAuto_Service.OpportuntiyWithMemberWrapper oppwithMemberObj: opportunitiesToProcessMap.values()){
                    opportunitiesToInsert.add(oppwithMemberObj.oppObj);


                }

                if(!opportunitiesToInsert.isEmpty())
                	upsert opportunitiesToInsert;

                for(Opportunity oppObj : opportunitiesToInsert){
                    oppNameWithOppObjForContactRole.put(oppObj.OppAuto_Opportunity_New_Name__c, oppObj.ID);
                    if(opportunitiesToProcessMap.containsKey(oppObj.Name)){
                        OppAuto_Service.OpportuntiyWithMemberWrapper oppwithMemberObj = opportunitiesToProcessMap.get(oppObj.Name);
                        if(oppwithMemberObj.portfolioMember.Opportunity__c == NULL && oppwithMemberObj.oppObj.Id != null){
                            oppwithMemberObj.portfolioMember.Opportunity__c = oppwithMemberObj.oppObj.Id;
                            addMemberInMap(campMembersToUpdateMap, oppwithMemberObj.portfolioMember);
                        }
                        if(oppwithMemberObj.executiveMember.Opportunity__c == NULL && oppwithMemberObj.oppObj.Id != null){
                            oppwithMemberObj.executiveMember.Opportunity__c = oppwithMemberObj.oppObj.Id;
                            addMemberInMap(campMembersToUpdateMap, oppwithMemberObj.executiveMember);
                        }
                   }
                   if(currentCampMember.Opportunity__C == NULL && oppObj.Id!= null){
                       	currentCampMember.Opportunity__c = oppObj.Id;
                       	if(campMembersToUpdateMap.containsKey(currentCampMember.Id)
                           && campMembersToUpdateMap.get(currentCampMember.Id).Opportunity__c ==null){
                            campMembersToUpdateMap.get(currentCampMember.Id).Opportunity__c = oppObj.Id;
						}
                      	addMemberInMap(campMembersToUpdateMap, currentCampMember);
                   }
                }
            }
            populateOpportunityId(campMembersToUpdateMap, oppNameWithMemberList, oppNameWithOppObjForContactRole );
            for(String oppName : OppAuto_Service.oppNameCampIds.keySet()){
                for(String campMemberId : OppAuto_Service.oppNameCampIds.get(oppName)){
                    if(campMembersToUpdateMap.containsKey(campMemberId)){
                        if(campMembersToUpdateMap.get(campMemberId).Opportunity__C == NULL){
                           campMembersToUpdateMap.get(campMemberId).Opportunity__C = oppNameWithOppObjForContactRole.get(oppName);
                        }
                    }else{
                        campMembersToUpdateMap.put(campMemberId, new CampaignMember(Id = campMemberId, Opportunity__C = oppNameWithOppObjForContactRole.get(oppName)));
                    }
                }

            }
            populateCompanyId(campMembersToUpdateMap);
            if(!campMembersToUpdateMap.isEmpty())
                update campMembersToUpdateMap.values();

            /* This method is use to create Contact Role Records */
        	OppAuto_Service.createContactRole(opportunitiesToProcessMap, oppNameWithOpportunityMap, oppNameWithMemberList, oppNameWithOppObjForContactRole);

             /*This method is use to update the Opportunity Name on the Campaign*/
            OppAuto_Service.populateCampInfluenceIgnoreIds(new Set<Id>{currentCampMember.CampaignId}, opportunitiesToInsert);

            /* This method is use to create Unique Campaign Influence Records */
        	OppAuto_Service.createCampaignInfluence(opportunitiesToProcessMap, new Map<Id,Opportunity> (opportunitiesToInsert).keySet()
                                                   , oppNameWithMemberList, oppNameWithOppObjForContactRole);

            OppAuto_Service.updateCampaignInfluenceOppCount(currentCampMember.CampaignId);

            /* This method is use to create Unique Campaign Company Records */
        	//OppAuto_Service.createCampaignCompany(opportunitiesToProcessMap, campaignIds);

        }catch(Exception excp){
            System.debug('ex==' + excp);
            System.debug('ex==' + excp.getStackTraceString());
            if(!campMembers.isEmpty()){
                campMembers[0].OppAuto_Error__C = excp.getMessage();
                campMembers[0].OppAuto_Error_StackTrace__c = excp.getStackTraceString();
                database.update( campMembers, false );
            }
            if(isCallingFromDetailPage){
                isAbort = true;
                OppAuto_Service.updateCampaignProcessingStatus(campaignId, 'Error');
            }

        }
    }
    public void finish(Database.BatchableContext bc){
        if(isCallingFromDetailPage && !isAbort){
            OppAuto_Service.updateCampaignProcessingStatus(campaignId, 'Completed');
        }

    }
    static void populateCompanyId(Map<Id,CampaignMember> campMemberToUpdate){

        for(CampaignMember campMember : OppAuto_Service.idWithMembersToUpdateCompany.values()){
            if(campMemberToUpdate.containsKey(campMember.Id)){
                if(String.isBlank(campMember.Company_Name__c)
                   && OppAuto_Service.idWithMembersToUpdateCompany.containsKey(campMember.Id)){
                       campMember.Company_Name__c = OppAuto_Service.idWithMembersToUpdateCompany.get(campMember.Id).Company_Name__c;
                   }
            }else{
                addMemberInMap(campMemberToUpdate, campMember);
            }

        }

    }
    /* This method is use to update the member list  */
    private void populateOpportunityId(Map<ID, CampaignMember> campMemberToUpdateMap, Map<String, List<CampaignMember>> oppNameWithCampMemberList, Map<String, String> oppNameWithOppId){
        System.debug('oppNameWithCampMemberList==='+oppNameWithCampMemberList);
        for(String oppName : oppNameWithCampMemberList.keySet()){
            for(CampaignMember campMember : oppNameWithCampMemberList.get(oppName)){
                System.debug('campMember====' + campMember);
                if(oppNameWithOppId.containsKey(oppName)){
                    if(campMember.Opportunity__c == NULL)
                    	campMember.Opportunity__c = oppNameWithOppId.get(oppName);
                    addMemberInMap(campMemberToUpdateMap, campMember);
                }
            }
        }
    }
    static void addMemberInMap(Map<Id,CampaignMember> campMembersToUpdateMap, CampaignMember campMember){
        if(!campMembersToUpdateMap.containsKey(campMember.Id)){
            campMember.OppAuto_Error__c = '';
            campMember.OppAuto_Error_StackTrace__c = '';
            campMembersToUpdateMap.put(campMember.Id,campMember);
        }
    }
    /* This method is use to add the CampaignMember in Map*/
    public void addCampaignMemberInMap(Map<Id, List<CampaignMember>> campIdWithMembersMap, CampaignMember campMember){
        if(campIdWithMembersMap.containsKey(campMember.CampaignId)){
            campIdWithMembersMap.get(campMember.CampaignId).add(campMember);
        }else{
            campIdWithMembersMap.put(campMember.CampaignId, new List<CampaignMember>{campMember});
        }
    }
   	/* This method is use to execute the Service Method to create Opportunity according to Member's type */
    public void createOrUpdateOpportunity(Map<Id, List<CampaignMember>> campIdWithMembersMap, CampaignMember campMember
                                          , Map<String,OppAuto_Service.OpportuntiyWithMemberWrapper> opportunitiesToProcessMap
                                          , Set<String> processedOppNames
                                          , Map<String, List<CampaignMember>> oppNameWithMemberList
                                          , Map<String, String> oppNameWithOppObjForContactRole
                                          , Set<String> updatedOppNames
                                          , Boolean isProcessExistingMembers){

         OppAuto_Service.createOrUpdateOpportunity(campIdWithMembersMap.containsKey(campMember.CampaignId)
                                                            ? campIdWithMembersMap.get(campMember.CampaignId)
                                                            : new List<CampaignMember>()
               			                              		, campMember, oppNameWithOpportunityMap, opportunitiesToProcessMap
                                                   			, (!users.isEmpty() ? users[0].Id : NULL), processedOppNames, oppNameWithMemberList
                                                   ,oppNameWithOppObjForContactRole, updatedOppNames, isProcessExistingMembers);
    }
}