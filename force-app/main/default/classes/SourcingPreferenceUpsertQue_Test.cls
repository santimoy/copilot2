@isTest
public class SourcingPreferenceUpsertQue_Test {
    
    @TestSetup
    static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@ivptest.com');
        insert u1;
        System.runAs(u1) {
            
            List<Sourcing_Preference__c> insertList = new List<Sourcing_Preference__c>();
            Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
            sourcingPreference.Name__c = 'Test';
            sourcingPreference.External_Id__c = 'xxxxx-xxxxx-xxxxx';
            sourcingPreference.Include_Exclude__c = 'Include';
            sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
            sourcingPreference.SQL_Where_Clause__c = 'LOWER(ct.companies_cross_data_full.ivp_description) = \'test\'';
            sourcingPreference.Last_Validated__c = null;
            sourcingPreference.Recent_Validation_Error__c = null;
            sourcingPreference.Validation_Status__c = null;
            sourcingPreference.status__c = 'Inactive';
            sourcingPreference.Sent_to_DWH__c = true;
            sourcingPreference.Is_Template__c = false;
            sourcingPreference.JSON_QueryBuilder__c = '{"condition":"AND","rules":[{"id":"LOWER(ct.companies_cross_data_full.ivp_description)","field":"LOWER(ct.companies_cross_data_full.ivp_description)","type":"string","input":"text","operator":"equal","value":"test"}],"valid":true}';
            sourcingPreference.Serialized__c = '';
            insertList.add(sourcingPreference);
            insert insertList;
        }
    }
    
    public static testmethod void Method1()
    { 
        List<Sourcing_Preference__c> insertList = [select id, name__c,Advanced_Mode__c,OwnerId,Sent_to_DWH__c, Include_Exclude__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, 
                                                   Recent_Validation_Error__c, Serialized__c,SQL_Query__c, SQL_Where_Clause__c, Status__c, Validation_Status__c, 
                                                   External_Id__c from Sourcing_Preference__c];
    
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        System.enqueueJob(new SourcingPreferenceUpsertQue(insertList));
        Test.stopTest();
    }
    public static testmethod void Method2()
    { 
        List<Sourcing_Preference__c> insertList = [select id, name__c,Advanced_Mode__c,OwnerId,Sent_to_DWH__c, Include_Exclude__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, 
                                                   Recent_Validation_Error__c, Serialized__c,SQL_Query__c, SQL_Where_Clause__c, Status__c, Validation_Status__c, 
                                                   External_Id__c from Sourcing_Preference__c];
    
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SourcingPreferenceUpsertQue.TEST_BYPASS = false;
        System.enqueueJob(new SourcingPreferenceUpsertQue(insertList));
        Test.stopTest();
    }
}