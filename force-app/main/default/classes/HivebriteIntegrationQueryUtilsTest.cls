@isTest(SeeAllData = false)
public with sharing class HivebriteIntegrationQueryUtilsTest {
    public static Map<String, Map<String, Recordtype>> recordTypeByObjectName;
    public static Map<String, Integration_API_Object__mdt> apiObjectByLabel;
    public static Map<String, Integration_API_Request__mdt> apiRequestByLabel;
    public static Map<String, List<Integration_API_Field__mdt>> apiFieldByLabel;
    // String Label = PartnerProject;
    @IsTest
    static void testGetRecordTypeValue(){
        String objectName = 'Account';  
        Test.startTest();
        HivebriteIntegrationQueryUtils.getRecordTypeValue(objectName);
        Test.stopTest();
    }
    @IsTest
    static void testGetApiObject(){
        String apiObjectLabel = 'Partner-Project'; 
        Test.startTest();
        HivebriteIntegrationQueryUtils.getApiObject(apiObjectLabel);
        Test.stopTest();
    }
    @IsTest
    static void testGetApiRequest(){
        // String apiRequestLabel = 'POST'; 
        String apiRequestLabel = 'Create Contacts';
        //String apiRequestLabel = 'https://insight.sanbox.us.hivebrite.com'+'POST'; // /api/admin/v1/users/_bulk';//+'POST';
        Test.startTest();
        HivebriteIntegrationQueryUtils.getApiRequest(apiRequestLabel);
        Test.stopTest();
    }
    @IsTest
    static void testGetMapingFieldsRecords(){
        String apiObjectLabel; 
        Test.startTest();
        HivebriteIntegrationQueryUtils.getMapingFieldsRecords(apiObjectLabel);
        Test.stopTest();
    }
}