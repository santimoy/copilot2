public class EmailDiscovererController
{
	@AuraEnabled
	public static EmailDiscovererWrapper initializeEmailDiscoverer( Id recordId )
	{
		EmailDiscovererWrapper wrapper = new EmailDiscovererWrapper();
		if( String.isNotBlank( recordId ) )
		{
			System.debug( 'recordId : ' + recordId );
			List<Contact> keyContacts = [ SELECT Id, FirstName, LastName, Account.Website_Domain__c FROM Contact WHERE AccountId = :recordId AND Key_Contact__c = true ];
			System.debug( 'keyContacts : ' + keyContacts );
			if( !keyContacts.isEmpty() )
			{
				wrapper.firstName = keyContacts[0].FirstName;
				wrapper.lastName = keyContacts[0].LastName;
				wrapper.domain = keyContacts[0].Account.Website_Domain__c;
			}
			System.debug( 'wrapper : ' + wrapper );
		}

		return wrapper;
	}

	@AuraEnabled
	public static List<EmailFinder.EmailDefinition> findDiscovererEmails( String firstNameIn, String lastNameIn, String domainIn )
	{
		List<EmailFinder.EmailDefinition> emails = new List<EmailFinder.EmailDefinition>();
		emails.addAll( findAllDiscovererEmails( firstNameIn, lastNameIn, domainIn, 'first' ) );
		emails.addAll( findAllDiscovererEmails( firstNameIn, lastNameIn, domainIn, 'second' ) );

		return emails;
	}

	@AuraEnabled
	public static void createKeyContact( Id accountId, String firstNameIn, String lastNameIn, String emailAddressIn )
	{
		Contact newKeyContact = new Contact();
		newKeyContact.AccountId = accountId;
		newKeyContact.FirstName = firstNameIn;
		newKeyContact.LastName = lastNameIn;
		newKeyContact.Email = emailAddressIn;
		newKeyContact.Key_Contact__c = true;
		insert newKeyContact;
	}

	@AuraEnabled
	public static void updateKeyContact( Id accountId, String emailAddressIn )
	{
		List<Contact> allKeyContacts = [ SELECT Id, Email FROM Contact WHERE AccountId = :accountId and Key_Contact__c = true ];
		if( !allKeyContacts.isEmpty() )
		{
			Contact existingKeyContact = allKeyContacts[0];
			existingKeyContact.Email = emailAddressIn;
			update existingKeyContact;
		}
	}

	private static List<EmailFinder.EmailDefinition> findAllDiscovererEmails( String firstNameIn, String lastNameIn, String domainIn, String emailGroup )
	{
		EmailFinder finder = new EmailFinder( domainIn, lastNameIn, firstNameIn );
		finder.findEmailInit();
		finder.getEmails( emailGroup );

		for( Integer i = 0; i < finder.emailSelection.size(); i++ )
		{
			finder.currentBatch.add( finder.emailSelection.get(i) );
			if( Math.mod( i + 1, 2 ) == 0 || i + 1 == finder.emailSelection.size() )
			{
				System.debug('Batch'+ String.valueOf((i+1)));
				finder.findEmail();
				finder.currentBatch.clear();
			}
		}

		/**
		* Commented out by PC - 6/22/18
		* Not needed - creating duplicates and causes mix DML & WS callout errors.
		List<Matching_Email_Template__c> matches = new List<Matching_Email_Template__c>();
		for( EmailFinder.EmailDefinition defn : finder.finalList )
		{
			System.debug( 'defn : ' + defn );
			EmailFinder.EmailJson res = defn.CheckerResults;
			if( res.additionalStatus != 'ServerIsCatchAll' && res.status == 'Ok' )
			{
				Matching_Email_Template__c m = new Matching_Email_Template__c();
				m.Name = defn.emailTemplate;
				m.Actual_Email_Address__c = defn.emailAddress;
				matches.add( m );
			}
		}
		insert matches;
		 */

		return finder.finalList;
	}

	public class EmailDiscovererWrapper
	{
		@AuraEnabled
		public String firstName { get; set; }

		@AuraEnabled
		public String lastName { get; set; }

		@AuraEnabled
		public String domain { get; set; }

		public EmailDiscovererWrapper()
		{
			this.firstName = '';
			this.lastName = '';
			this.domain = '';
		}
	}
}