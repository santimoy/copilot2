/**
 * @author MukeshKS-Concretio
 * @date 2018-04-18
 * @className IVPUtils
 * @group FeelingLucky
 *
 * @description contains common utility methods 
 */
public class IVPUtils {
    
    public static final String  IVP_SD_NAME_INITIAL = (Test.isRunningTest() ? 'tst_' :'') + 'ivp_sd_';
    public static final String  IVP_SD_LABEL_INITIAL = (Test.isRunningTest() ? 'TST' :'') + 'IVP SD ';
    
    public final static String SF_BASE_URL{
    	get{
	    	if(SF_BASE_URL == NULL){
	    		SF_BASE_URL = URL.getSalesforceBaseUrl().toExternalForm();
	    	}
            
	    	return SF_BASE_URL;
    	}
    	set;
    }
    
    public final static String USER_SESSION_ID{
    	get{
	    	if(USER_SESSION_ID == NULL){
	    		//USER_SESSION_ID = IVPUtils.getUserSessionId();
                USER_SESSION_ID = IVPUtils.getUserSessionIdSoap();
	    	}
            
	    	return USER_SESSION_ID;
    	}
    	set;
    }
    
    /*******************************************************************************************************
     * @description specifies user name which is used for to assign owner of Company (Account)
     * @return String
     * @author MukeshKS-Concretio
     * @note Don't change value of this property.
     */
    public final static String UNASSIGNED_USER_NAME = 'Unassigned';
    
    /*******************************************************************************************************
     * @description returns user id of user which name is UNASSIGNED_USER_NAME
     * @return Id
     * @author MukeshKS-Concretio
     */
    public static Id unassignedUserId {        
        get {
            if (unassignedUserId == null) {                
                List<User> userList= [SELECT Id FROM User WHERE Name = :IVPUtils.UNASSIGNED_USER_NAME LIMIT 1];                
                if(!userList.isempty()){                    
                    unassignedUserId = userList[0].id;                
                }           
            }  
           
            return unassignedUserId;        
        }set;
    }
    
    /**
	 * @description contains list of users group name to whom assign IFL records
	 * @return List<String>
	 * @author Mukesh-Concretio
	*/
    public final static List<String> iFLUserGroups = new List<String>{(Test.isRunningTest() ? 'Test_':'')+ 'IVP_IFL_User_Group'};

    /**
     * @description contains I'm feeling lucky record type Developer name
     * @return String
     * @author Mukesh-Concretio
    */
    public final static String IMFL_RECORDTYPE_DEV_NAME = 'Im_Feeling_Lucky';
    
    /*******************************************************************************************************
     * @description returns I'm feeling lucky record type id
     * @return Id
     * @author MukeshKS-Concretio
     */
    public static Id iFLRecordTypeId {        
        get {
            if (iFLRecordTypeId == null) {                
                iFLRecordTypeId = IVPUtils.getRecordTypeId('I\'m Feeling Lucky', 'Intelligence_Feedback__c');          
            }  
            
            return iFLRecordTypeId;        
        }set;
    }
    
    /**
	 * @description returns max count to assign companies to user
	 * @return Integer
	 * @author Mukesh-Concretio
	 */
    public static Integer maxCompanyToAssign  {
        get {
            if( maxCompanyToAssign == null ){
                maxCompanyToAssign = Integer.valueOf(Label.IVP_IFL_Max_Companies_To_Assign);
            }
            
        	return maxCompanyToAssign;
    	} set;
    }
    
    /**
	 * @description returns max reject count of IFL record
	 * @return Integer
	 * @author Mukesh-Concretio
	 */
    public static Integer maxIFLRejectAttempts  {
        get {
            if( maxIFLRejectAttempts == null ){
                maxIFLRejectAttempts = Integer.valueOf(Label.IVP_IFL_Max_Reject_Attempts);
            }

        	return maxIFLRejectAttempts;
    	} set;
    }
    
    /*******************************************************************************************************
     * @description GetUserSessionId - this method returns user seesion id in VF context
     * @param void
     * @return String sessionId
     * @author Dharmendra-concretio
	*/
    public static String getUserSessionId(){
        PageReference reportPage = Page.IVPSessionId;
        
        String vfContent;
        if (!Test.isRunningTest()) {
            vfContent = reportPage.getContent().toString();
        } else {
            vfContent = '<div>START_SESSION_ID_Test1_END_SESSION_ID</div>';
        }
        // System.debug('vfContent '+vfContent);
        
        String sessionId = '';
        //vfContent = tokenWithStartEndText.toString();
        // System.debug('vfContent '+vfContent);
        // Find the position of Start_Of_Session_Id and End_Of_Session_Id
        // 'sssssss-'+data.sessionId+'--eeeee'
        Integer startP = vfContent.indexOf('START_SESSION_ID_') + 'START_SESSION_ID_'.length(),
            endP = vfContent.indexOf('_END_SESSION_ID');
        // Get the Session Id
        sessionId = vfContent.substring(startP, endP);
        // System.debug('sessionId '+sessionId.substring(1));
        // Return Session Id
        return sessionId;
    }

    /*******************************************************************************************************
     * @description GetUserSessionId - this method returns user seesion id
     * @param void
     * @return String sessionId
     * @author Amjad-concretio
    */
    public static String getUserSessionIdSoap(){
    	if(Test.isRunningTest()){
	        Test.setMock(HttpCalloutMock.class, new IVPWatchDogMock());
    	}
        Http h = new Http();

        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        //req.setEndpoint('https://test.salesforce.com/services/Soap/c/39.0/');
        req.setEndpoint('callout:IVP_ADMIN_USER');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/xml');
        req.setHeader('SOAPAction', 'dummy');
        // System.debug(UserInfo.getOrganizationId());
        String s = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">';
        s+='<Body>';
        s+='<login xmlns="urn:enterprise.soap.sforce.com">';
        //s+='<username>akhan@ivp.com.dev2</username>';
        //s+='<password>c0ncret10xOYXINs91LjjjEza4FaOf537E</password>';
        s+='<username>{!$Credential.UserName}</username>';
        s+='<password>{!$Credential.Password}</password>';
        s+='</login>';
        s+='</Body>';
        s+='</Envelope>';                
        req.setBody(s);
        // Send the request, and return a response
        HttpResponse res = h.send(req);
       
        DOM.Document doc = new DOM.Document();      
        doc.load(res.getBody());    
        DOM.XMLNode node = doc.getRootElement();
        return walkThrough(node);
    }
    
    private static String walkThrough(DOM.XMLNode node) {
        // System.debug(node.getNodeType());
        String result = '';
        
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            
            
            if (node.getText().trim() != '' && node.getName() == 'sessionId') {
                result = node.getText().trim();
            }
            if (node.getAttributeCount() > 0) { 
                for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
                    //result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
                }  
            }
            for (Dom.XMLNode child: node.getChildElements()) {
                result += walkThrough(child);
            }
            return result;
        }
        return '';  //should never reach here 
    }


    
    /*******************************************************************************************************
     * @description GetDescribeSObjectResult - this method is used for to get describe sobject result of given object name.
     * @param String objectName
     * @return Schema.DescribeSObjectResult describeSObjectResult
     * @author MukeshKS-concretio
     */
    public static Schema.DescribeSObjectResult getDescribeSObjectResult(String objectName){
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType sObjectTypeObj = globalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult describeSObjectResult = sObjectTypeObj.getDescribe();
        return describeSObjectResult;
    }
    
    /*******************************************************************************************************
     * @description GetRecordTypeId - this method is used for to get record type id of given record type name and object name.
     * @param String recordTypeName
     * @param String objectName
     * @return Id recordTypeId
     * @author MukeshKS-concretio
     */
    public static Id getRecordTypeId(String recordTypeName, String objectName){
        Schema.DescribeSObjectResult describeSObjectResultObj = getDescribeSObjectResult(objectName);
        Id recordTypeId = describeSObjectResultObj.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return recordTypeId;
    }
    
    /*******************************************************************************************************
     * @description GetFieldSetMembers - this method is used for to get field set member list of given field set name and object name.
     * @param String recordTypeName
     * @param String objectName
     * @return List<Schema.FieldSetMember> fieldSetMembers
     * @author MukeshKS-concretio 
     */
    public static List<Schema.FieldSetMember> getFieldSetMembers(String fieldSetName, String objectName) { 
        List<Schema.FieldSetMember> fieldSetMembers = new List<Schema.FieldSetMember>(); 
        Schema.DescribeSObjectResult describeSObjectResultObj = getDescribeSObjectResult(objectName);
        Map<String, Schema.FieldSet> fieldSetMap = describeSObjectResultObj.FieldSets.getMap();
        
        if(!fieldSetMap.isEmpty() ){
            Schema.FieldSet fieldSetObj = fieldSetMap.get(fieldSetName);
            fieldSetMembers = fieldSetObj.getFields();  
        }
      
        return fieldSetMembers;
    }
    
    /**
     * @description GetFieldSetFieldsWithType - this method is used for to get field and respective labels and type from field set
     * @param void
     * @return Map<String, Map<String,Schema.DisplayType>> fieldSetFieldsMap
     * @author MukeshKS-concretio
     */    
    public static Map<String, Map<String,Schema.DisplayType>> getFieldSetFieldsWithType(String fieldSetName, String objectName){
        
        Map<String, Map<String,Schema.DisplayType>> fieldSetFieldsMap = new Map<String, Map<String,Schema.DisplayType>>();
        
        List<Schema.FieldSetMember> fieldSetMembers = IVPUtils.getFieldSetMembers(fieldSetName,objectName);
        
        for(Schema.FieldSetMember fieldSetMember:fieldSetMembers){
            
            Map<String,Schema.DisplayType> typeAndLabel = new Map<String,Schema.DisplayType>();
            
            typeAndLabel.put(fieldSetMember.label, fieldSetMember.getType());
            
            fieldSetFieldsMap.put(fieldSetMember.fieldPath, typeAndLabel);
        }
       
        return fieldSetFieldsMap;
    }
    
    /**
     * @description GetFieldSetMembers - this method is used for to get field set member list of given field set name and object name.
     * @param String recordTypeName
     * @param String objectName
     * @return List<Schema.FieldSetMember> fieldSetMembers
     * @author Amjad
     * */
    public static Map<String,String> getFieldSetFields(String fieldSetName, String objectName){
        Map<String,String> fieldsMap = new Map<String,String>();
        for (Schema.FieldSetMember fsm : IVPUtils.getFieldSetMembers(fieldSetName,objectName)) {
            fieldsMap.put(fsm.fieldPath, fsm.label);
        }
        return fieldsMap;
    }
    /*******************************************************************************************************
     * @description RetrievePicklistByRecordTypeId - this method is used for to get picklist options by record type id of object
     * @param Id recordTypeId
     * @param String fieldAPIName
     * @param String objectName
     * @return Map<String, String> picklistOptsMap contains value and label of picklist options
     * @author MukeshKS-concretio 
     */
    public static Map<String, String> retrievePicklistByRecordTypeId(Id recordTypeId, String fieldAPIName, String objectName) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String host = System.Url.getSalesforceBaseURL().toExternalForm();
        String url = host + '/services/data/v42.0/ui-api/object-info/'+objectName+'/picklist-values/'+recordTypeId+'/'+fieldAPIName;        
        request.setEndpoint(url);
        request.setMethod('GET');  
        request.setHeader('Authorization', 'OAuth '+getUserSessionId());
        HttpResponse response;        
        response = http.send(request);        
        Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

        Map<String, String> picklistOptsMap = new Map<String, String>();
        if(meta.containsKey('values')){                                
            for(Object obj: (List<Object> )meta.get('values')){
                Map<String, object> temp = (Map<String, object>)obj; 
                String label = (String) temp.get('label');
                String value = (String) temp.get('value');
                picklistOptsMap.put(label.unescapeHtml4(), value.unescapeHtml4());
            }
        }
		return picklistOptsMap;  
    }
    
    /*******************************************************************************************************
     * @description currentUserSetting - this method returns Custom setting record in the context of logged-in user
     * @param void
     * @return IVP_SD_ListView__c sessionId
     * @author Dharmendra-concretio
	*/
    public static IVP_SD_ListView__c currentUserSetting(){
        
    	return IVP_SD_ListView__c.getInstance(UserInfo.getUserId());
    }
    
    /*******************************************************************************************************
     * @description currentUserSetting - this method returns Custom setting record in the context of Organization
     * @param void
     * @return IVP_SD_ListView__c sessionId
     * @author Dharmendra-concretio
	*/
    public static IVP_SD_ListView__c currentOrgSetting(){
        
    	return IVP_SD_ListView__c.getOrgDefaults();
    }
    
    /*******************************************************************************************************
     * @description used to fetch List view basis on passed List-view's Id
     * @return String
     * @author Dharmendra Karamchandani
     * @param String viewId	List-View Id
     * @note Don't change value of this property.
     */
    public static String fetchListViewDetailById(String viewId){
        
        String endPoint = IVPUtils.SF_BASE_URL + '/services/data/v32.0/sobjects/Account/listviews/' + viewId + '/describe';
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
       
        request.setMethod('GET');
        request.setHeader('Authorization','Bearer ' + IVPUtils.getUserSessionId());
        request.setHeader('Accept','application/json');
        request.setHeader('Content-type','application/json');
        HttpResponse response = http.send(request);
       
        return response.getBody();
	}
	
    /*******************************************************************************************************
     * @description used to fetch List view basis on passed List-view's Id
     * @return String
     * @author Dharmendra Karamchandani
     * @param String viewId	List-View Id
     * @note Don't change value of this property.
     */
    public static List<IVPListViewRESTResponse.LVResponse> fetchListViewsForCurrentUser(){
        IVPListViewRESTResponse listView = new IVPListViewRESTResponse ();
        
        String endPoint = IVPUtils.SF_BASE_URL + '/services/data/v32.0/sobjects/Account/listviews/';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
       
        request.setMethod('GET');
        request.setHeader('Authorization','Bearer ' + IVPUtils.getUserSessionId());
        request.setHeader('Accept','application/json');
        request.setHeader('Content-type','application/json');
        HttpResponse response = http.send(request);
        String jsonResponse = response.getBody();
        // System.debug(jsonResponse);
       	Map<String, Object> listViewObj = (Map<String, Object>)JSON.deserializeUntyped(jsonResponse);
       	if(listViewObj.containsKey('listviews') && listViewObj.get('listviews') instanceof List<Object>){
       		listView = (IVPListViewRESTResponse)JSON.deserialize(jsonResponse, IVPListViewRESTResponse.class);
       	}
        
        return listView.listviews;
	}
	
	/*******************************************************************************************************
     * @description used to update sObject List 
     * @return Map<Id,String> Id=recordId & String=Error Message 
     * @author Tapeshwar Kumar
     * @param List<sObject>
     */
	 public static Map<String,Map<Id,String>> updateRecords(List<sObject> listRecordsToUpdate){
	    Map<String,Map<Id,String>> updateSummary = new Map<String,Map<Id,String>>();
	    Map<Id,String> successRecords = new Map<Id,String>();
	    Map<Id,String> failRecords = new Map<Id,String>();
	    
        Database.SaveResult[] saveResultList = database.update(listRecordsToUpdate, false);
        for (Integer i = 0; i < saveResultList.size(); i++){
            if (saveResultList[i].isSuccess()){
                successRecords.put(listRecordsToUpdate[i].Id,'');
            }else{
                System.debug('Testing for error messages::'+saveResultList[i].getErrors()[0].getMessage());
                System.debug('Testing for error messages with extract::'+TaskCreationController.extractErrorMessage(saveResultList[i].getErrors()[0].getMessage()));
                failRecords.put(listRecordsToUpdate[i].Id, TaskCreationController.extractErrorMessage(saveResultList[i].getErrors()[0].getMessage()));
            }
        }
        updateSummary.put('successRecords',successRecords);
        updateSummary.put('failRecords',failRecords);
        
        return updateSummary;
	 }
	    
	    
	public static IVPMetadataService.ListView getListOwnIVPView(String viewName){
	    List<ListView> listViews = [SELECT Id FROM ListView where DeveloperName=:viewName LIMIT 1];
      
	    if(!listViews.isEmpty()){
	       return new IVPListViewMetaData().parse(fetchListViewDetailById(
	                listViews[0].Id));
	    }
	    return NULL;
	}
	
	public static IVPMetadataService.MetadataPort createService() {
        IVPMetadataService.MetadataPort service = new IVPMetadataService.MetadataPort();
        service.SessionHeader = new IVPMetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = IVPUtils.USER_SESSION_ID;
        return service;
    }
    
	private static Map<String, Schema.SObjectField> accountFieldMap{
		get{
			if( accountFieldMap == NULL){
				accountFieldMap = Account.sObjectType.getdescribe().fields.getmap();
			}
			return accountFieldMap;
		}set;
	}
	public static String getAccountFieldLabel(String fieldName){
		if(accountFieldMap.containsKey(fieldName)){
            
			return accountFieldMap.get(fieldName).getDescribe().getLabel();
		}
		return '';
	}
	public static String accProspectRTId{
		get{
			if(accProspectRTId == NULL){
				accProspectRTId = '';
				List<RecordType> recordTypes = [SELECT Id FROM RecordType 
												WHERE SobjectType = 'Account' AND Name = 'Prospect'
												LIMIT 1];
				if(!recordTypes.isEmpty()){
					accProspectRTId = recordTypes[0].Id;
				}
			}
           
			return accProspectRTId;
		}set;
	}
	
	public static String extractSOQLScope(String soqlQuery){
        String usingScope = 'USING SCOPE ';
        Integer usingScopeIndex = soqlQuery.indexOf(usingScope);
        
        if(usingScopeIndex >=0){
            usingScope = soqlQuery.substring(usingScopeIndex, soqlQuery.indexOf(' ',usingScopeIndex+usingScope.length()));
        }else{
        	usingScope = '';
        }
        return usingScope;
    }
}