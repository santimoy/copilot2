public class PreferencePreviewController{
    @AuraEnabled
    public static HerokuData fetchHerokuData(String pageName,String pageNo,String sourceId,String orderByColumn,String orderType){
        Logger.push('fetchHerokuData','PreferencePreviewController');
        fieldClass fieldList = getDisplayColumn(sourceId,true);
        //fieldClass fieldList = (fieldClass)JSON.deserializeStrict(listOfField, fieldClass.Class);
        
        HttpRequest req = setHttpRequest(pageName,pageNo,sourceId,fieldList,orderByColumn,orderType);
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        String result = '';
        HerokuData hd =  new HerokuData();
        try{
            System.debug('before doing '+System.now());
            System.debug('>'+req.getBody());
            res = http.send(req);
            System.debug('end '+System.now());
            result = res.getBody();
            System.debug('result ' + result);
            result = result.replaceAll('null','\"\"');
            hd = parse(result);
        }catch(Exception e){
            Datetime start = System.now();
            System.debug('end '+start);
            Map<String, Object> data = new Map<String, object>{
                'body'=>req.getBody(),
                    'method'=>req.getMethod(),
                    'endpoint'=>req.getEndpoint(),
                    'response_message'=>e.getMessage()
                    };
			Logger.debugException(JSON.serialize(data));
            Logger.pop();
            Datetime ends = system.now();
            List<Log__c> logs = [SELECT Id,Name FROM Log__c 
                                 WHERE CreatedById=:UserInfo.getUserId()
                                 AND CreatedDate>=:start AND CreatedDate<=:ends];
            
            result = Label.DWH_API_Time_Out ;
            if(!logs.isEmpty()){
                System.debug(logs[0].Name);
                result ='refId: '+String.escapeSingleQuotes(logs[0].Name)+', ' +result;
            }
            hd.errorInfo = result;
        }
        
        hd.wrapColnList = JustForYouCompaniesController.getWrapColName();
        if(hd.wrapColnList.size()>0){
            hd.isColnWrap = true;
        }else{
            hd.isColnWrap = false;
        }
        system.debug(hd.wrapColnList);
        Intelligence_Settings__c userSetting = Intelligence_Settings__c.getInstance(UserInfo.getUserId());
        hd.colnWrapMaxLength = userSetting.Wrap_Large_Columns_Max_Length__c;
        Map<String,Integer> fieldColMinWidth = JustForYouCompaniesController.getMinWidthMap();
        List<Integer> widthList = new List<Integer>();
        for(string label : fieldList.fLabelList){
            for(string colLabel : fieldColMinWidth.keySet()){
                if(label.trim() == colLabel.trim()){
                   widthList.add(fieldColMinWidth.get(colLabel)); 
                }
            }
        }
        
        hd.colnWidthList  = widthList;
        hd.fieldsLabel = fieldList.fLabelList;
        hd.fieldsApi = fieldList.fApiList;
        hd.jsonData = result;
        return hd;
    }
    
    @auraEnabled
    public static Sourcing_Preference__c fetchRecName(String recId){
        return [Select Id, Name__c, External_Id__c From Sourcing_Preference__c Where External_Id__c=: recId Limit 1];
    }
    /*
    @AuraEnabled
    public static fieldClass fetchDislayCol(){
        fieldClass fieldList = getDisplayColumn();
        system.debug('fieldList=='+fieldList);
        return fieldList;
    }*/
    
    public class responseClass{
        @AuraEnabled public Set<String> fLabelList {get;set;}
        @AuraEnabled public Set<String> fApiList {get;set;}
    }
    
    public static HttpRequest setHttpRequest(String pageName,String pageNo,String sourceId,fieldClass fieldList,String orderByColumn,String orderType){
        System.debug('setHttpRequest-->'+pageName+'='+orderByColumn+'=>'+orderType);
        HttpRequest req = new HttpRequest();
        BodyWrapper bw = new BodyWrapper();
        bw.fields = fieldList.fApiList;
        IVP_Call_Params__c justForYouObj = new IVP_Call_Params__c();
        List<IVP_Call_Params__c> callParams = new List<IVP_Call_Params__c>();
        
        callParams = [SELECT Name, Preview_Universe_Limit__c, Preview_Preference_Limit__c, Just_For_You_OrderBy__c,
                        Preview_Preference_OrderBy__c, Preview_Universe_OrderBy__c 
                        FROM IVP_Call_Params__c WHERE Name = 'Preview' LIMIT 1];
        if(!callParams.isEmpty()){
            justForYouObj = callParams[0];
        }
        String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c;
        if(pageName == 'Preview Universe'){
            endPoint += '/preferences/'+userinfo.getUserId()+'/universe';
            if(justForYouObj != null){
                if(justForYouObj.Preview_Universe_OrderBy__c != null && orderByColumn == ''){
                    bw.order_by = String.valueOf(justForYouObj.Preview_Universe_OrderBy__c);
                }else{
                    bw.order_by = getOrderByColumn(orderByColumn)+' '+orderType+' NULLS LAST';
                }
                if(justForYouObj.Preview_Universe_Limit__c != null){
                    bw.limits = String.valueOf(justForYouObj.Preview_Universe_Limit__c);
                }
            }
            //2019-08-30 DK: Added per Joshua - requested by DWH
            bw.sf_org_id = UserInfo.getOrganizationId();
            //bw.order_by = 'ct.companies_cross_data_full.iq_score asc NULLS LAST';
        }else if(pageName == 'Preview'){
            endPoint += '/preferences/'+sourceId+'/preview';
            if(justForYouObj != null){
                if(justForYouObj.Preview_Preference_OrderBy__c != null && orderByColumn == ''){
                    bw.order_by = String.valueOf(justForYouObj.Preview_Preference_OrderBy__c);
                }else{
                    bw.order_by = getOrderByColumn(orderByColumn)+' '+orderType+' NULLS LAST';
                }
                
                if(justForYouObj.Preview_Preference_Limit__c != null){
                    bw.limits = String.valueOf(justForYouObj.Preview_Preference_Limit__c);
                }
            }
            //2019-08-30 DK: Added per Joshua - requested by DWH
            bw.user_id = UserInfo.getUserId();
            bw.sf_org_id = UserInfo.getOrganizationId();
            //bw.order_by = 'ct.companies_cross_data_full.iq_score asc NULLS LAST';
        }
        String body = JSON.serialize(bw);
        body = body.replace('limits','limit');
        system.debug('bw: ' + body);
        req.setEndpoint(endPoint);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        // need to keep max timeout is 30 sec
        req.setTimeout(30000);
        req.setBody(body);
        return req;
    }
    public static String getOrderByColumn(String columnLabel){
        system.debug('columnLabel >>'+columnLabel);
        columnLabel = columnLabel.trim();
        List<Intelligence_Field__c> intelligenceFieldList = [SELECT Id, Name, DB_Field_Name__c,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c 
                                                             FROM Intelligence_Field__c 
                                                             Where UI_Label__c =:columnLabel];
        //no need to do query in debug;
        // system.debug(Database.query('SELECT Id, Name, DB_Field_Name__c,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c FROM Intelligence_Field__c Where UI_Label__c = \'Country\' order by Order__c '));
        
        system.debug('intelligenceFieldList[0].DB_Column__c  >>'+intelligenceFieldList);
        if(intelligenceFieldList.size()>0){
            
            return intelligenceFieldList[0].DB_Column__c;
        }
        return null;
    }
    public static fieldClass getDisplayColumn(String sourceId, boolean isPreview){
        Set<String> fLablelList = new Set<String>();
        Set<String> fApiList = new Set<String>();
        Set<String> remainingFLablelList = new Set<String>();
        Set<String> remainingFApiList = new Set<String>();
        Set<String> reqSet = new Set<String>{'Company Name'};
        Set<String> notAllowSet = new Set<String>{'Name'};
        Set<String> queryBuilderFApiName = new Set<String>();
        if(isPreview){
            queryBuilderFApiName = getColumnFromQueryBBuilder(sourceId);
        }
        
        fieldClass fields = new fieldClass();
        List<Intelligence_User_Preferences__c> intelligenceUserList = [SELECT Display_Columns__c, Id FROM Intelligence_User_Preferences__c Where User_Id__c=:userInfo.getUserId() limit 1];
        List<Intelligence_Field__c> requriredFieldList = [SELECT Id, Name, DB_Table__c,DB_Field_Name__c,Default_display_field__c ,UI_Label__c, Order__c, 
                                                          QBFlt_Is_Filter__c,Active__c, Static_Selection__c, DB_Column__c
                                                          FROM Intelligence_Field__c Where Active__c = true AND (UI_Label__c In:reqSet OR DB_Column__c In:queryBuilderFApiName) order by Order__c];
        if(isPreview){
            fApiList.add('ivp_name');
            fApiList.add('iq_score');
            fApiList.add('ivp_employee_count');
            fApiList.add('ivp_description');
            
            fLablelList.add('Company Name');
            fLablelList.add('IQ');
            fLablelList.add('EES');
            fLablelList.add('Description');
        }
        String feedbackTable = 'ct.'+System.Label.IVP_DWH_Query_Table_Name;
        for(Intelligence_Field__c req : requriredFieldList){
            if(req.DB_Table__c == feedbackTable && !req.QBFlt_Is_Filter__c){
                continue;
            }
            if(queryBuilderFApiName.contains(req.DB_Column__c)){
                if(req.UI_Label__c != null ) {
                    fLablelList.add(req.UI_Label__c.trim());
                }
                if(req.DB_Field_Name__c != null ){
                    fApiList.add(req.DB_Field_Name__c.trim());
                } 
            } else {
                if(req.UI_Label__c != null ) {
                    remainingFLablelList.add(req.UI_Label__c.trim());
                }
                if(req.DB_Field_Name__c != null ){
                    remainingFApiList.add(req.DB_Field_Name__c.trim());
                }
            }
        }
        if(remainingFLablelList.size() > 0) {
            fLablelList.addAll(remainingFLablelList);
        }
        if(remainingFApiList.size() > 0) {
            fApiList.addAll(remainingFApiList);
        }
        system.debug('intelligenceUserList '+intelligenceUserList );
        if(intelligenceUserList.size()>0){
            if(intelligenceUserList[0].Display_Columns__c != null){
                List<String> lstString = intelligenceUserList[0].Display_Columns__c.split(';');
                for(Integer i=0;i<lstString.size();i++){
                    fieldClass fc = new fieldClass();
                    List<String> columParts = lstString[i].split('\\(');
                    //System.debug('columParts'+lstString[i]+'>'+columParts);
                    if(columParts.size()==2){
                        String label = columParts[0];
                        String apiName = columParts[1].remove(')');
                        if(!fLablelList.contains(label) && !fApiList.contains(apiName) && label != '' && apiName != ''){
                            fLablelList.add(label);
                            fApiList.add(apiName);
                        }
                    }else if(columParts.size()==3){
                        String label = columParts[0]+'('+columParts[1];
                        String apiName = columParts[2].remove(')');
                        if(!fLablelList.contains(label) && !fApiList.contains(apiName) && label != '' && apiName != ''){
                            fLablelList.add(label);
                            fApiList.add(apiName);
                        }
                    }
                }
                //system.debug('fApiList '+fApiList);
                //system.debug('fLablelList'+fLablelList);
            }else{
                String initialColumns = '';
                List<Intelligence_Field__c> intelligenceFieldList = getInitialColumns();
                for(Intelligence_Field__c intfield : intelligenceFieldList){
                    if(intfield.DB_Field_Name__c != null && intfield.UI_Label__c != null && !notAllowSet.contains(intfield.UI_Label__c)){
                        initialColumns += intfield.UI_Label__c+' ('+intfield.DB_Field_Name__c+');';
                        fApiList.add(intfield.DB_Field_Name__c);
                        fLablelList.add(intfield.UI_Label__c );
                    }
                }
                initialColumns = initialColumns.substring(0, initialColumns.length() - 1);
                //system.debug('initialColumns'+initialColumns);
                intelligenceUserList[0].Display_Columns__c = initialColumns;
                update intelligenceUserList;
            }
            
        }
        fields.fLabelList = fLablelList;
        fields.fApiList = fApiList;
        return fields;
    }
    
    private static Set<String> getColumnFromQueryBBuilder(String sourceId) {
        Set<String> dispalyQueryBuilderColumn = new Set<String>();
        for(Sourcing_Preference__c  sp : [Select Id, JSON_QueryBuilder__c  
                                          From Sourcing_Preference__c  
                                          Where External_Id__c =: sourceId]) 
        {
            if(String.isNotEmpty(sp.JSON_QueryBuilder__c)) {
                JSONParser parser = JSON.createParser(sp.JSON_QueryBuilder__c);
                while(parser.nextToken()!= null) {
                    if(parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                        parser.nextValue();
                        if(parser.getCurrentName() == 'field') {
                            if(String.isNotEmpty(parser.getText())) {
                                if(parser.getText().contains('LOWER(')) {
                                    dispalyQueryBuilderColumn.add(parser.getText().substringBetween('LOWER(', ')').trim());
                                }
                                if(parser.getText().contains(System.Label.IVP_DWH_Query_View_Name_To_Replace)){
                                    dispalyQueryBuilderColumn.add(parser.getText().replace(System.Label.IVP_DWH_Query_View_Name_To_Replace, System.Label.IVP_Heroku_Data_Table_Name).trim());
                                    dispalyQueryBuilderColumn.add(parser.getText().replace(System.Label.IVP_DWH_Query_View_Name_To_Replace, System.Label.IVP_DWH_Query_Table_Name).trim());
                                }  else {
                                    dispalyQueryBuilderColumn.add(parser.getText().trim());
                                }
                                

                                
                            }
                        }
                    }
                }
            }
        }
        return dispalyQueryBuilderColumn;
    }
    
    
    public static List<Intelligence_Field__c> getInitialColumns(){
        List<Intelligence_Field__c> intelligenceFieldList = [SELECT Id, Name, DB_Field_Name__c,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c 
                                                             FROM Intelligence_Field__c 
                                                             Where Active__c=true and Static_Selection__c = true order by Order__c];
        
        return intelligenceFieldList;
    }
    public class BodyWrapper{
        public Set<String> fields;
        public String order_by;
        public String limits;
        public String user_id;
        public String sf_org_id;
        public String page;
    }
    public static HerokuData  parse(String json) {
        return (HerokuData) System.JSON.deserialize(json, HerokuData.class);
    }
    
    
    public class HerokuData {
        @AuraEnabled public Integer total_pages {get;set;}
        @AuraEnabled public Boolean additional_data {get;set;}
        @AuraEnabled public String host {get;set;}
        @AuraEnabled public List<List<String>> data {get;set;}
        @AuraEnabled public List<String> data_fields {get;set;}
        @AuraEnabled public Response_time response_time {get;set;}
        @AuraEnabled public String sql_statement {get;set;}
        @AuraEnabled public Integer total_entries {get;set;}
        @AuraEnabled public String dialect {get;set;}
        @AuraEnabled public Set<String> fieldsLabel {get;set;}
        @AuraEnabled public Set<String> fieldsApi {get;set;}
        @AuraEnabled public String error {get;set;}
        @AuraEnabled public String jsonData {get;set;}
        @AuraEnabled public String errorInfo {get;set;}
        @AuraEnabled public List<String> wrapColnList {get;set;}
        @AuraEnabled public List<Integer> colnWidthList {get;set;}
        @AuraEnabled public Boolean isColnWrap {get;set;}
        @AuraEnabled public Decimal colnWrapMaxLength {get;set;}
    }
    public class fieldClass{
        @AuraEnabled public Set<String> fLabelList {get;set;}
        @AuraEnabled public Set<String> fApiList {get;set;}
    }
    public class Response_time {
        @AuraEnabled Integer query_execution  {get;set;}
        @AuraEnabled Integer count_query_execution {get;set;}
        @AuraEnabled Integer total_execution_time {get;set;}
    }
}