/*
*	AccountTriggerHandlerWOS class : It contains logics to track the Company Rating for Accounts
*
*	Author: Philip
*	Date:   March 14, 2018
*
*/
public without sharing class AccountTriggerHandlerWOS
{
	public static Boolean BYPASSTRIGGER = false;

	public void onAfterInsert( List<Account> newAccounts )
	{
		System.debug( 'Starting onAfterInsert WOS' );
		if( !shouldSkipProcessing() )
			trackCompanyRatings( newAccounts, null );
	}

	public void onAfterUpdate( List<Account> newAccounts, Map<Id, Account> oldAccountMap )
	{
		System.debug( 'Starting onAfterUpdate WOS' );
		if( !shouldSkipProcessing() )
			trackCompanyRatings( newAccounts, oldAccountMap );
	}

	private Boolean shouldSkipProcessing()
	{
		return BYPASSTRIGGER;
	}

	private void trackCompanyRatings( List<Account> newAccounts, Map<Id, Account> oldAccountMap )
	{
		Common_Config__c commonConfigCS = CommonConfigUtil.getConfig();
		Set<String> accountOwnerProfiles = new Set<String>();
		if( String.isNotBlank( commonConfigCS.Company_Rating_Profiles__c ) )
			accountOwnerProfiles.addAll( commonConfigCS.Company_Rating_Profiles__c.normalizeSpace().split('\\s*,\\s*') );

		Set<String> accountRecordTypes = new Set<String>();
		if( String.isNotBlank( commonConfigCS.Company_Rating_Record_Types__c ) )
		{
			accountRecordTypes.addAll( commonConfigCS.Company_Rating_Record_Types__c.normalizeSpace().split('\\s*,\\s*') );
		}
		Set<Id> accountRTIds = new Set<Id>();
		for( RecordType rt : [ SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName IN :accountRecordTypes ] )
		{
			accountRTIds.add( rt.Id );
		}

		Set<String> accountDealFields = new Set<String>();
		String accountDealFieldQueryStr = '';
		if( String.isNotBlank( commonConfigCS.Company_Rating_Fieldnames__c ) )
		{
			accountDealFields.addAll( commonConfigCS.Company_Rating_Fieldnames__c.normalizeSpace().split('\\s*,\\s*') );
			accountDealFieldQueryStr = String.join( new list<string>(accountDealFields), ', ' );
		}
		if( String.isNotBlank( accountDealFieldQueryStr ) && !accountOwnerProfiles.isEmpty() && !accountRTIds.isEmpty() )
		{
			Set<Id> filteredAccounts = new Set<Id>();
			for( Account a : newAccounts )
			{
				for( String dealField : accountDealFields )
				{
					if( oldAccountMap == NULL
						|| ( oldAccountMap != NULL && oldAccountMap.get( a.Id ) != NULL
							&& ( ( a.get( dealField ) != oldAccountMap.get( a.Id ).get( dealField ) )
								|| ( a.OwnerId != oldAccountMap.get( a.Id ).OwnerId ) ) ) )
					{
						filteredAccounts.add( a.Id );
					}
				}
			}
			// remvoed Current_Rating__c check in the inner query as need that user's older record as well.
			String accountQueryStr = 'SELECT ' + accountDealFieldQueryStr + ', Id, Name, OwnerId, Owner.Profile.Name, Owner.Alias, RecordTypeId'
									+ ', ( SELECT Id, OwnerId, Current_Rating__c FROM Company_Ratings__r ORDER BY CreatedDate DESC ), ( SELECT Id FROM Opportunities WHERE IsClosed = false ORDER BY CreatedDate DESC )'
									+ ' FROM Account WHERE Id IN :filteredAccounts AND Owner.Profile.Name IN :accountOwnerProfiles AND RecordTypeId IN :accountRTIds';

			Set<Id> unassignedAccountIdsToUpdate = new Set<Id>();
			Set<Id> accountIdsToUpdate = new Set<Id>();
			Set<Id> parentAccountIdsOfCRsToSetToNonCurrent = new Set<Id>();
			List<Company_Rating__c> companyRatingsToUpsert = new List<Company_Rating__c>();
			List<Opportunity> opptiesToUpdate = new List<Opportunity>();
			Id unassignId = IVPUtils.unassignedUserId;
			for( Account a : Database.query( accountQueryStr ) )
			{
				if( a.Owner.Alias == 'unassign' )
				{
					unassignedAccountIdsToUpdate.add( a.Id );
				}
				else
				{
					List<Opportunity> relatedOpenOpportunities = a.Opportunities;
					if( relatedOpenOpportunities != NULL && !relatedOpenOpportunities.isEmpty() )
					{
						for( Opportunity o : relatedOpenOpportunities )
						{
							for( String dealField : accountDealFields )
							{
								o.put( dealField, a.get( dealField ) );
							}
							opptiesToUpdate.add( o );
						}
					}
					else
					{
						//Get child company ratings where Current_Rating__c = true
						List<Company_Rating__c> relatedCompanyRatings = a.Company_Ratings__r;
                        Id ownerId = a.OwnerId;
                        Id oldOwnerId = a.OwnerId;
                        if(oldAccountMap != NULL && oldAccountMap.containsKey(a.Id)){
                            oldOwnerId = oldAccountMap.get(a.Id).OwnerId;
                        }
                        Id compRatingOwnId = ownerId;
                        if(oldOwnerId != ownerId){
                            // maintain last owner's rating if last owner is unassign so maintaining current owner's rating
                            compRatingOwnId = oldOwnerId != unassignId ? oldOwnerId : ownerId;
                        }
						//Update Case
						if( relatedCompanyRatings != NULL && !relatedCompanyRatings.isEmpty() )
						{
							Company_Rating__c lastCompanyRating = null;
							for( Company_Rating__c cr : relatedCompanyRatings )
							{
							    if(cr.OwnerId == compRatingOwnId){
    								if(oldOwnerId != NULL && oldOwnerId == unassignId && cr.OwnerId == ownerId){
    								    lastCompanyRating = cr;
    								    lastCompanyRating.Current_Rating__c = TRUE;
    								}else if( cr.Current_Rating__c ){
    									lastCompanyRating = cr;
    								}
								}
							}
							if( lastCompanyRating != NULL )
							{
								if( ( oldAccountMap != NULL && a.OwnerId != oldAccountMap.get( a.Id ).OwnerId )  && (oldOwnerId == UserInfo.getUserId()))
									accountIdsToUpdate.add( a.Id );
								else
								{
									for( String dealField : accountDealFields )
									{
										lastCompanyRating.put( dealField, a.get( dealField ) );
									}
									companyRatingsToUpsert.add( lastCompanyRating );
								}
							}
							else
							{
								Company_Rating__c cr = new Company_Rating__c();
								cr.Company__c = a.Id;
								cr.OwnerId = compRatingOwnId;
								cr.Current_Rating__c = true;
								for( String dealField : accountDealFields )
								{
									cr.put( dealField, a.get( dealField ) );
								}
								companyRatingsToUpsert.add( cr );

								for( Company_Rating__c childCR : a.Company_Ratings__r )
								{
									childCR.Current_Rating__c = false;
									companyRatingsToUpsert.add( childCR );
								}
							}
						}
						else
						{
							Company_Rating__c cr = new Company_Rating__c();
							cr.Company__c = a.Id;
							cr.OwnerId = a.OwnerId;
							cr.Current_Rating__c = true;
							for( String dealField : accountDealFields )
							{
								cr.put( dealField, a.get( dealField ) );
							}
							companyRatingsToUpsert.add( cr );
						}
					}
				}
			}

			if( !unassignedAccountIdsToUpdate.isEmpty() )
			{
				if( System.isBatch() || System.isFuture() )
					updateDealFieldsOnAccountsFromNonTrigger( unassignedAccountIdsToUpdate, 'unassigned' );
				else
					updateDealFieldsOnAccountsFromTrigger( unassignedAccountIdsToUpdate, 'unassigned' );
			}

			if( !accountIdsToUpdate.isEmpty() )
			{
				if( System.isBatch() || System.isFuture() )
					updateDealFieldsOnAccountsFromNonTrigger( accountIdsToUpdate, 'regular' );
				else
					updateDealFieldsOnAccountsFromTrigger( accountIdsToUpdate, 'regular' );
			}

			if( !companyRatingsToUpsert.isEmpty() )
				upsert companyRatingsToUpsert;

			if( !opptiesToUpdate.isEmpty() )
				update opptiesToUpdate;
		}
	}

	@future
	public static void updateDealFieldsOnAccountsFromTrigger( Set<Id> accountIds, String updateAccountKey )
	{
		updateDealFieldsOnAccounts( accountIds, updateAccountKey );
	}

	private static void updateDealFieldsOnAccountsFromNonTrigger( Set<Id> accountIds, String updateAccountKey )
	{
		updateDealFieldsOnAccounts( accountIds, updateAccountKey );
	}

	public static void updateDealFieldsOnAccounts( Set<Id> accountIds, String updateAccountKey )
	{
		Common_Config__c commonConfigCS = Common_Config__c.getOrgDefaults();
		Set<String> accountDealFields = new Set<String>();
		String accountDealFieldQueryStr = '';
		if( String.isNotBlank( commonConfigCS.Company_Rating_Fieldnames__c ) )
		{
			accountDealFields.addAll( commonConfigCS.Company_Rating_Fieldnames__c.normalizeSpace().split('\\s*,\\s*') );
			accountDealFieldQueryStr = String.join( new list<string>(accountDealFields), ', ' );
		}

		if( String.isNotBlank( accountDealFieldQueryStr ) )
		{
			String accountsWithChildRecordsQueryStr = 'SELECT ' + accountDealFieldQueryStr + ', Id, Name, OwnerId, Owner.Alias, Owner.Profile.Name, RecordTypeId, ( SELECT ' + accountDealFieldQueryStr + ', Id, OwnerId, Current_Rating__c FROM Company_Ratings__r WHERE Current_Rating__c = true ORDER BY CreatedDate DESC ) FROM Account WHERE Id IN :accountIds';
			List<Account> accountsToUpdate = new List<Account>();
			for( Account a : Database.query( accountsWithChildRecordsQueryStr ) )
			{
				if( updateAccountKey == 'regular' )
				{
					List<Company_Rating__c> companyRatings = a.Company_Ratings__r;
					Company_Rating__c lastCompanyRating = null;
					for( Company_Rating__c cr : companyRatings )
					{
						if( cr.Current_Rating__c && cr.OwnerId == UserInfo.getUserId() )
							lastCompanyRating = cr;
					}

					if( lastCompanyRating != NULL )
					{
						for( String dealField : accountDealFields )
						{
							a.put( dealField, lastCompanyRating.get( dealField ) );
						}
						accountsToUpdate.add( a );
					}
				}
				else if( updateAccountKey == 'unassigned' )
				{
					for( String dealField : accountDealFields )
					{
						Map<String, Schema.SObjectField> accountObjectType = Schema.SObjectType.Account.fields.getMap();
						Schema.SObjectField objField = accountObjectType.get( dealField );
						Schema.DisplayType fieldType = objField.getDescribe().getType();
						if( String.valueOf( fieldType ) == 'BOOLEAN' )
							a.put( dealField, false );
						else
							a.put( dealField, null );
					}
					accountsToUpdate.add( a );
				}
			}

			if( !accountsToUpdate.isEmpty() )
				update accountsToUpdate;
		}
	}
}