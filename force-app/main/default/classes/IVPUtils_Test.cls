/**
 * @author Nazia Khanam
 * @date 2018-05-19
 * @className IVPUtils_Test
 * @description : Is use to test the IVPUtils.
 */

@isTest
public class IVPUtils_Test {
	private static testMethod void test1(){
    	IVPUtils utils=new IVPUtils();
        //string sessionId=IVPUtils.getUserSessionId();
        Test.startTest();
         Test.setMock(HttpCalloutMock.class, new IVPFeelingLuckyMock());
        Map<String, String> mapOfString=IVPUtils.retrievePicklistByRecordTypeId(IVPUtils.iFLRecordTypeId,'Why_is_score_wrong__c','Intelligence_Feedback__c');
       
        system.assertNotEquals(NULL,mapOfstring.size());
        
       Test.setMock(HttpCalloutMock.class, new IVPMoke());
        
       string str3=IVPUtils.USER_SESSION_ID;
       
                Test.stopTest();
        
    }
    private static testMethod void test_SF_BASE_URL(){
        string baseURL=IVPUtils.SF_BASE_URL;
        system.assertEquals(URL.getSalesforceBaseUrl().toExternalForm(), baseURL);
    }
    private static testMethod void test_unassignedUserId(){
        Id usId=IVPUtils.unassignedUserId;
        String UNASSIGNED_USER_NAME='Unassigned';
        List<User> uId=[SELECT Id FROM User WHERE Name = :UNASSIGNED_USER_NAME LIMIT 1];
        system.assertEquals(uId[0].Id,usId);
    }
    private static testMethod void test_iFLRecordTypeId(){
        Id iflId=IVPUtils.iFLRecordTypeId;
        system.assertEquals(IVPUtils.getRecordTypeId('I\'m Feeling Lucky', 'Intelligence_Feedback__c'), iflId);
    }
    private static testMethod void test_iFLUserGroups(){
        List<String> strings=IVPUtils.iFLUserGroups;
    }
     private static testMethod void test_maxCompanyToAssign(){
         Integer int1=IVPUtils.maxCompanyToAssign;
         
         System.assertEquals(Integer.valueOf(Label.IVP_IFL_Max_Companies_To_Assign),int1);
    }
     private static testMethod void test_maxIFLRejectAttempts(){
         Integer int2=IVPUtils.maxIFLRejectAttempts;
         
          System.assertEquals(Integer.valueOf(Label.IVP_IFL_Max_Reject_Attempts),int2);
    }
    private static testMethod void test_getFieldSetFieldsWithType(){
         Map<String, Map<String,Schema.DisplayType>> mapfield=IVPUtils.getFieldSetFieldsWithType('Company_Surface_Chart','Account');
        Map<String,String> mapstring=IVPUtils.getFieldSetFields('CRMC_PP__CRMC_Action_Grid_Defaults','Lead');
    }
    private static testMethod void test_currentOrgSetting(){
         IVP_SD_ListView__c ivpsdlistvies1=IVPUtils.currentOrgSetting();
    }
    private static testMethod void test_getAccountFieldLabel(){
         String accountfieldLabel= IVPUtils.getAccountFieldLabel('Name');
        
        system.assertEquals('Company Name',accountfieldLabel);
    }
     private static testMethod void test_accProspectRTId(){
         string accprospect=IVPUtils.accProspectRTId;
         id accProspectRTId;
         List<RecordType> recordTypes = [SELECT Id FROM RecordType 
												WHERE SobjectType = 'Account' AND Name = 'Prospect'
												LIMIT 1];
				if(!recordTypes.isEmpty()){
					accProspectRTId = recordTypes[0].Id;
				}
         
         system.assertEquals(accProspectRTId,accprospect);
    }
     private static testMethod void test_createService(){
         Test.startTest();
         Test.setMock(HttpCalloutMock.class, new IVPMoke());
       string str3=IVPUtils.USER_SESSION_ID;
         IVPMetadataService.MetadataPort createser=IVPUtils.createService();
           Test.stopTest();
    }
    private static testMethod void getListOwnIVPView(){
         Test.startTest();
         Test.setMock(HttpCalloutMock.class, new IVPWatchDogMock());
       IVPMetadataService.ListView ivpListViewMeta=IVPUtils.getListOwnIVPView('All_Parent_Campaigns');
        List<ListView> listViews = [SELECT Id FROM ListView where DeveloperName='All_Parent_Campaigns' LIMIT 1];
        
        system.assertNotEquals(null, ivpListViewMeta);
        
        IVP_SD_ListView__c ipvSDListView = IVPUtils.currentUserSetting();
        
        system.assertNotEquals(null,ipvSDListView);
           Test.stopTest();
    }
    private static testMethod void testupdateRecords(){
        
        List<Account> testAccounts=IVPTestFuel.createTestCompanies(5,userInfo.getUserId());
  		testAccounts[0].name='test1';
        Map<String,Map<Id,String>> mapofStringId =IVPUtils.updateRecords(testAccounts);
        
        system.assertEquals(5, mapofStringId.get('successRecords').size());
    }
    private static testMethod void testfetchListViewsForCurrentUser(){
         Test.startTest();
         Test.setMock(HttpCalloutMock.class, new IVPWatchDogMock());
        List<IVPListViewRESTResponse.LVResponse> listViewOfListView=IVPUtils.fetchListViewsForCurrentUser();
       system.assertNotEquals(NULL,listViewOfListView[0].id);
            Test.stopTest();
    }
}