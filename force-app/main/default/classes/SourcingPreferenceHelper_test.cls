@isTest
public class SourcingPreferenceHelper_test {
    
    @TestSetup
    static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@ivptest.com');
        insert u1;
        
        Intelligence_User_Preferences__c  intUserRec = new Intelligence_User_Preferences__c ();
        intUserRec.CreatedById = u1.Id;
        intUserRec.Display_Columns__c = 'Name (ivp_name);City (ivp_city);Country (ivp_country);Intent 3M Growth % (intent_to_buy_score_3m_growth)';
        intUserRec.Sourcing_Signal_Category_Preferences__c = 'acquisition-acquirer, competitive-challenge, Customer, employee-growth, form-10k';
        intUserRec.My_Dash_Signal_Preferences__c = 'cquisition-acquirer, acquisition-acquiree, award, cb-new-company, ceo-change, competitive-challenge';
        intUserRec.User_Id__c = string.valueOf(u1.id) ;
        insert intUserRec;
        
        System.runAs(u1) {
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
            sourcingPreference.Name__c = 'Test';
            sourcingPreference.External_Id__c = null;
            sourcingPreference.Include_Exclude__c = 'Include';
            sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
            sourcingPreference.SQL_Where_Clause__c = 'LOWER(ct.companies_cross_data_full.ivp_description) = \'test\'';
            sourcingPreference.Last_Validated__c = null;
            sourcingPreference.Recent_Validation_Error__c = null;
            sourcingPreference.Validation_Status__c = null;
            sourcingPreference.status__c = 'Inactive';
            sourcingPreference.Sent_to_DWH__c = false;
            sourcingPreference.Is_Template__c = false;
            sourcingPreference.JSON_QueryBuilder__c = '{"condition":"AND","rules":[{"id":"LOWER(ct.companies_cross_data_full.ivp_description)","field":"LOWER(ct.companies_cross_data_full.ivp_description)","type":"string","input":"text","operator":"equal","value":"test"}],"valid":true}';
            sourcingPreference.Serialized__c = '';
            insert sourcingPreference;
        }
    }
    
    public static testmethod void Method1()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            List<Sourcing_Preference__c> insertList = new List<Sourcing_Preference__c>();
            for(Integer i =0; i< 2; i++ ){
                Sourcing_Preference__c sourcingPreference = new Sourcing_Preference__c();
                sourcingPreference.Name__c = 'Test'+ i;
                sourcingPreference.Include_Exclude__c = 'Include';
                sourcingPreference.SQL_Query__c = 'SELECT ivp_name, ivp_city, ivp_country, intent_to_buy_score_3m_growth From';
                insertList.add(sourcingPreference);
            }
            insert insertList;
            
            test.startTest();
            
            test.stopTest();
        }
    }
    
    public static testmethod void Method2()
    {
        User u1 = [Select Id, Name, Email from User where Email ='newuser@testorg.com'];
        System.runAs(u1) {
            
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            
            Intelligence_User_Preferences__c intUserRec = [Select id, Name, Display_Columns__c from Intelligence_User_Preferences__c LIMIT 1];
            intUserRec.Display_Columns__c = '';
            update intUserRec;
            
            
            
            Sourcing_Preference__c sp = [select id, name,Advanced_Mode__c,OwnerId,  Cloned_From__c, Include_Exclude__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, 
                                         Recent_Validation_Error__c, Serialized__c,SQL_Query__c, SQL_Where_Clause__c, Status__c, Validation_Status__c, 
                                         External_Id__c from Sourcing_Preference__c LIMIT 1];
            sp.Include_Exclude__c = 'Exclude';
            sp.Sent_to_DWH__c = false;
            
            test.startTest();
            update sp;
            test.stopTest();
        }
    }
}