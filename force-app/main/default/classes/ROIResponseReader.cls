public class ROIResponseReader {

	public Boolean success;
	public Error error;

	public class Error {
		public String org_id;
	}

	
	public static List<ROIResponseReader> parse(String json) {
		return (List<ROIResponseReader>) System.JSON.deserialize(json, List<ROIResponseReader>.class);
	}
}