/**
*@name      : SalesloftMock
*@date      : 2018-08-17
*@description   : The class used as mock class related to Salesloft 
**/
@isTest
global class SalesloftMock implements HttpCalloutMock{
    public static Boolean defaultRepsonse = true;
    public static String people1Id = '118391130';
    public static String people2Id = '120491517';
    public static String cadenceId1 = '643669';
    public static String cadenceId2 = '671678';
    public static String cadmemeberId1 = '109914770';
    public static String cadmemeberId2 = '109915570';
    global SalesloftMock(){}
    global HttpResponse respond(HttpRequest request){
        Integer statusCode = 200;
        String bodydata = '{ "metadata": { "paging": { "per_page": 2, "current_page": 1, "prev_page": null }}, "data": [';
        Map<String,String> dataforpeople = new Map<String,String>();
        dataforpeople.put(people1Id,' { "id": 118391130, "first_name": "Test1", "last_name": "data1", "email_address": "test1@test1.com" } ');
        dataforpeople.put(people2Id,' { "id": 120491517, "first_name": "Test2", "last_name": "data2", "email_address": "test2@test2.com" } ');
        
        Map<String,String> dataforcadence = new Map<String,String>();
        dataforcadence.put(cadenceId1,' { "id": 643669, "name": "Test1" ,"owner" : {"id":"56473"}} ');
        dataforcadence.put(cadenceId2,' { "id": 671678, "name": "Test2", "owner" : {"id":"56473"}} ');
        
        Map<String,String> dataforcadencemember = new Map<String,String>();
        
        dataforcadencemember.put(cadmemeberId1,'{ "id": 109914770, "cadence": { "id":'+ cadenceId1 +'}, "person": { "id":'+ people1Id +' } }');
        dataforcadencemember.put(cadmemeberId2,'{ "id": 109915570, "cadence": { "id":'+ cadenceId2 +'}, "person": { "id":'+ people2Id +' } }');
            
        HttpResponse response = new HttpResponse();
        String body = '';
        if(request.getEndpoint().contains(Salesloft.PEOPLE_END_POINT)){
            body =  bodydata + dataforpeople.get(people1Id) + ',' + dataforpeople.get(people2Id) + ']}';
            system.debug('hdgh'+request.getEndpoint());
        }
        else if(request.getEndpoint().contains(Salesloft.CADENCE_END_POINT)){
            body = bodydata + dataforcadence.get(cadenceId1) + ',' + dataforcadence.get(cadenceId2) + ']}';
            system.debug(request.getEndpoint());
        }
        else if(request.getEndpoint().contains(Salesloft.CADENCE_MEMBER_END_POINT)){
            system.debug(request.getEndpoint());
            body = bodydata + dataforcadencemember.get(cadmemeberId1) + ',' + dataforcadencemember.get(cadmemeberId2) + ']}'; 
            if(request.getMethod()=='Post'){
                statusCode= 201;
                body = '{ "metadata": { "paging": { "per_page": 1, "current_page": 1, "prev_page": null }}, "data": ' + dataforcadence.get(cadenceId1)+ '}';
                response.setStatus('Created');
            }
            
        }
        else if (request.getEndpoint().contains(Salesloft.CADENCE_ID_END_POINT.replace('{id}',cadenceId1))){
            body = '{ "metadata": { "paging": { "per_page": 1, "current_page": 1, "prev_page": null }}, "data": ' + dataforcadence.get(cadenceId1)+ '}';
        }
        response.setBody(body);
        response.setStatusCode(statusCode);
        return response;	
    }
}