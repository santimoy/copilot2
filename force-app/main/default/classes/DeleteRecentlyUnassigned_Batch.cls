/*
*	DeleteRecentlyUnassigned_Batch class : Deletes all recently unassigned records where older than 150 days
*
*	Author: Wilson Ng
*	Date:   Apr 26, 2018
*
*/
global class  DeleteRecentlyUnassigned_Batch implements Database.Batchable<sObject>, Schedulable {

	public DeleteRecentlyUnassigned_Batch() {
	}
	
	// ----------------------------------------------------------------------------------------------------------------------------------
	// Batchable Interface methods 
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		// setup the query
		String queryString = 'Select Id From Recently_Unassigned__c Where CreatedDate <> ' +
				(Test.isRunningTest() ? 'YESTERDAY' : 'LAST_N_DAYS:150');

		System.Debug('---queryString:'+queryString);
		return Database.getQueryLocator(queryString);	
	}

	global void execute(Database.BatchableContext BC, List<Recently_Unassigned__c> batchList) {
		
		// delete
		delete batchList;
	}
	
	global void finish(Database.BatchableContext BC) {
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// methods needed for the Schedulable interface
	global void execute(SchedulableContext sc){
		Database.executebatch(this, 200);
	}
	
	public class DeleteRecentlyUnassigned_Batch_Exception extends Exception{}
}