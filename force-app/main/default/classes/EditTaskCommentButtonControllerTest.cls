@isTest
private with sharing class EditTaskCommentButtonControllerTest {
    
    @TestSetup
    static void makeData(){

        Account objAccount = new Account(Name ='Test Account');
		insert objAccount;
        
        Id taskRTId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Task').getRecordTypeId();
        Task objTask = new Task(Subject = 'Test Subject',
                                Status = 'Completed',
                                Description = 'Test Comment',
                                Priority = 'Normal',
                                RecordTypeId = taskRTId,
                                OwnerId = UserInfo.getUserId(),
                                WhatId = objAccount.Id,
                                ActivityDate = Date.today(),
                                Type = 'Email',
                                Type__c = 'Email',
                                Source__c = 'Inbound');
        insert objTask;
        System.assertequals(true, [SELECT Id FROM Task].size() != 0);

        Task_Comment__c oTaskComment = new Task_Comment__c(Company__c = objAccount.Id, 
                                                           User__c=UserInfo.getUserId(), 
                                                           Comment__c='Test Comment',
                                                           Task_Id__c = objTask.Id);
        insert oTaskComment;
        System.assertequals(true, [SELECT Id FROM Task_Comment__c].size() != 0);
    }

    @isTest
    private static void testEditTaskComment() {
        
        Task objTask = new Task();
        Task_Comment__c  objTaskComment = new Task_Comment__c();
        List<Task> lstTask = new List<Task>([SELECT Id, Description, WhatId, Subject, Type FROM Task]);
        objTaskComment = EditTaskCommentButtonController.fetchTaskComment(lstTask[0].Id);
        objTask = EditTaskCommentButtonController.fetchTaskPlainTextComment(lstTask[0].Id);
        System.assertequals(true, objTaskComment != null);
        System.assertequals(true, objTask != null);
        System.assertequals('Test Comment', [SELECT Id, Comment__c FROM Task_Comment__c][0].Comment__c);
        
        objTaskComment.Comment__c = 'Updated Comment';
        EditTaskCommentButtonController.updateTaskComment(objTaskComment, objTask);
        System.assertequals('Updated Comment', [SELECT Id, Comment__c FROM Task_Comment__c][0].Comment__c);
    }
}