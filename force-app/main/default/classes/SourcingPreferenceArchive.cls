public class SourcingPreferenceArchive {
    
    @auraEnabled
    public static Sourcing_Preference__c fetchSourcingPreference(Id recId){
        try{
            Sourcing_Preference__c SourcingPrefRec = [Select Id, Name, OwnerId, External_Id__c, Include_Exclude__c,Advanced_Mode__c, Is_Template__c, JSON_QueryBuilder__c, Last_Validated__c, Name__c,
                                                      Owner_Username__c,Recent_Validation_Error__c,  SQL_Query__c, SQL_Where_Clause__c, Serialized__c, Status__c, Validation_Status__c,
                                                      Cloned_From__c, Sent_to_DWH__c From Sourcing_Preference__c where Id =: recId];
            return SourcingPrefRec;
        }catch(Exception e){
            return null;
        }
    }
    @auraEnabled
    public static boolean saveSourcingPreference(Sourcing_Preference__c recSave){
        recSave = QueryBuilder.makeCallout(recSave);
        update recSave;
        return true;
    }
}