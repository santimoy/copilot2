/***********************************************************************************
 * @author      10k-Expert
 * @name    IVPOwnershipCompanyRequestUtilityTest
 * @date    2018-11-19
 * @description  To test IVPOwnershipCompanyRequestUtilityCtrl
 ***********************************************************************************/
@isTest
public class IVPOwnershipCompanyRequestUtilityTest {
    @isTest public static void testGetCompanyRequests(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        List<Company_Request__c> companyRequests = testdata.companyRequests;
        
        Test.startTest();
        
        ValidationWrapper validationWrapper = IVPOwnershipCompanyRequestUtilityCtrl.getCompanyRequests();
        
        Test.stopTest();
        
        System.assertEquals(2, validationWrapper.requests.size());
    }
    
    @isTest public static void testChangeRequestStatusAccepted(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        List<Company_Request__c> companyRequests = testdata.companyRequests;
        
        Test.startTest();
        
        ValidationWrapper validationWrapper = IVPOwnershipCompanyRequestUtilityCtrl.changeRequestStatus(companyRequests[0].Company__c, 'Accepted', '');
        
        Test.stopTest();
        
        System.assert(validationWrapper.successMsg != '');
    }
    @isTest public static void testChangeRequestStatusRejected(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        List<Company_Request__c> companyRequests = testdata.companyRequests;
        
        Test.startTest();
        
        ValidationWrapper validationWrapper = IVPOwnershipCompanyRequestUtilityCtrl.changeRequestStatus(companyRequests[0].Company__c, 'Rejected', '');
        
        Test.stopTest();
        
        System.assert(validationWrapper.successMsg != '');
    }
    @isTest public static void testGetRemainingBalance(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testData.populateIVPGeneralConfig();
        List<Company_Request__c> companyRequests = testdata.companyRequests;
        
        Test.startTest();
        
        ValidationWrapper validationWrapper = IVPOwnershipCompanyRequestUtilityCtrl.getRemainingBalance();
        
        Test.stopTest();
        System.assert(validationWrapper.requestBalance == 3);
    }
    @isTest public static void testGetCompanyRequest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        List<Company_Request__c> companyRequests = testdata.companyRequests;
        
        Test.startTest();
        boolean bypass = IVPOwnershipCompanyRequestUtilityCtrl.getByPassValue();
        ValidationWrapper validationWrapper = IVPOwnershipCompanyRequestUtilityCtrl.getCompanyRequest(companyRequests[0].Id);
        
        Test.stopTest();
        
        System.assertEquals(1, validationWrapper.requests.size());
    }
}