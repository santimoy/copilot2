/*
*	ActivityTimelineControllerTest class : It contains test coverage for ActivityTimelineController logics.
*
*	Author: Philip
*	Date:   April 17, 2018
*
*	Last Modified:
*			5/31/2018 PC Added type to color and icon mappings
*/
@isTest
private class ActivityTimelineControllerTest
{
    /* @testSetup
static void setup() {
Task tsk = new Task();
tsk.subject = 'task subject';
tsk.Type = 'Email';
/ tsk.Description = ''; //string
tsk.OwnerId = ''; //user id
tsk.WhatId = ''; //record id
/
insert tsk;
}*/
    @isTest static void testActivityTimeline()
    {
        Common_Config__c config = CommonConfigUtil.getConfig();
        
        Account testAccount = TestFactory.createAccounts( 1 )[0];
        Contact testContact = TestFactory.createContacts( 1, testAccount.Id, 'TestContact', true )[0];
        List<Task> allOpenActivities = TestFactory.createTasks( 1, testAccount.Id, 'Call', Date.today(), 'Not Started', false );
        for( Task t : allOpenActivities )
        {
            t.WhoId = testContact.Id;
            t.Type__c = 'Call';
        }
        insert allOpenActivities;
        
        List<Task> allActivityHistories = new List<Task>();
        List<Task> callActivityHistoriesLastYear = TestFactory.createTasks( 1, testAccount.Id, 'Call', Date.today().addYears(-1), 'Completed', false );
        List<Task> callActivityHistoriesThisYear1 = TestFactory.createTasks( 1, testAccount.Id, 'Call', Date.today().addDays(1), 'Completed', false );
        List<Task> callActivityHistoriesThisYear2 = TestFactory.createTasks( 1, testAccount.Id, 'Call', Date.today().addDays(-15), 'Completed', false );
        allActivityHistories.addAll( callActivityHistoriesLastYear );
        allActivityHistories.addAll( callActivityHistoriesThisYear1 );
        allActivityHistories.addAll( callActivityHistoriesThisYear2 );
        for( Task t : allActivityHistories )
        {
            t.Type__c = 'Call';
        }
        insert allActivityHistories;
        
        RecordType rtInvestOppty = [select id from RecordType where sObjectType='Opportunity' and DeveloperName='Investment_Opportunity' limit 1];
        Opportunity testOppty = new Opportunity( RecordTypeId=rtInvestOppty.Id, AccountId = testAccount.Id, Name = 'Test Oppty', StageName = 'Lost', CloseDate = Date.today(), Loss_Drop_Reason__c = 'Too much money in' );
        insert testOppty;
        
        List<EmailMessage> testEmails = new List<EmailMessage>();
        for( Integer i = 0; i < 20; i++ )
        {
            testEmails.add( new EmailMessage( FromAddress = 'test@abc.org', Incoming = true, ToAddress= 'test@xyz.org', Subject = 'Test email', TextBody = 'Test email body', MessageDate = Date.today().addDays(-1), Status = '3', RelatedToId = testOppty.Id ) );
        }
        insert testEmails;
       
        Test.startTest();
        
       ActivityTimelineController.ActivityTimelineWrapper wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, '', '','');
        List<ActivityTimelineController.ActivityWrapper> openWrappers = wrap.openActivities;
        List<ActivityTimelineController.ActivityWrapper> pastWrappers = wrap.pastActivities;
         ActivityTimelineController.ActivityWrapper wrap1 = new ActivityTimelineController.ActivityWrapper();
         wrap1.activityToAddress = 'testToAddress';
         wrap1.activityFromAddress = 'testFromAddress';
         wrap1.activityRelatedToId = 'testRelatedToId';
         wrap1.activityRelatedToName = 'testRelatedToName';
         wrap1.activityTextBody = 'testTextody';
         wrap1.alternateDetailId = testAccount.Id;
         wrap1.startDateTime = Date.today();
         wrap1.endDateTime = Date.today()+28;
       /* 
        //System.assertEquals( 12, openWrappers.size()+pastWrappers.size() );
        //System.assertEquals( 60, wrap.totalCountOfPastActivities );
        
        pastWrappers.addAll( ActivityTimelineController.loadPastActivities( testAccount.Id, 'Call', '', pastWrappers.size() ) );
        //System.assertEquals( 22, openWrappers.size()+pastWrappers.size() );
        
        pastWrappers.addAll( ActivityTimelineController.loadPastActivities( testAccount.Id, 'Call', '', pastWrappers.size() ) );
        //System.assertEquals( 32, openWrappers.size()+pastWrappers.size() );
       */ 
        //filter Call activities
        wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, 'Call', '' ,'');
        //System.assertEquals( 12, wrap.openActivities.size()+wrap.pastActivities.size() );
        
        //filter on activities that were logged in last seven days
        wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, '', 'Last_Seven','' );
        //System.assertEquals( 12, wrap.openActivities.size()+wrap.pastActivities.size() );
        
        //filter on Call activities that were logged in last seven days
        wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, 'Call', 'Last_Seven','' );
        //System.assertEquals( 2, wrap.openActivities.size()+wrap.pastActivities.size() );
        
        //filter on Call activities that are logged for next seven days.  Note: over-due activites are included in the query
        wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, 'Call', 'Next_Seven','' );
        //System.assertEquals( 12, wrap.openActivities.size()+wrap.pastActivities.size() );
        
        //filter on Call activities that are logged in last thirty days
        wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, 'Call', 'Last_Thirty','' );
        //System.assertEquals( 12, wrap.openActivities.size()+wrap.pastActivities.size() );
        
        //filter Call activities
        wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, 'Email', '','' );
        //System.assertEquals( 10, wrap.openActivities.size()+wrap.pastActivities.size() );
        
        Double val = 1261992;
        
        //System.assertEquals( '', ActivityTimelineController.getActivityColorString(Date.today(),null,val));
        Boolean isrichTextTaskComment = ActivityTimelineController.isRichTextCommentExist(allOpenActivities[0].Id);
        
        Test.stopTest();
    }
    @isTest 
    public static void testIsRichTextCommentExist()
    { 
        Test.startTest();
        Task tsk = new Task();
        tsk.subject = 'task subject';
        tsk.Type = 'Email';
        insert tsk;
        
        ActivityTimelineController.isRichTextCommentExist(tsk.Id);
        sObject sobj = new Task();
        ActivityTimelineController.getButtonMenuItems(sobj,'Type__c');
        System.assertEquals(false , ActivityTimelineController.isRichTextCommentExist(tsk.Id));
        System.assertNotEquals(null, sobj);
        Test.stopTest();
    }
    @isTest
    public static void testLoadPastActivities(){
        Test.startTest();
         Account testAccount = TestFactory.createAccounts( 1 )[0];
         ActivityTimelineController.ActivityTimelineWrapper wrap = ActivityTimelineController.getAccountWithActivities( testAccount.Id, '', '','');
        List<ActivityTimelineController.ActivityWrapper> openWrappers = wrap.openActivities;
        List<ActivityTimelineController.ActivityWrapper> pastWrappers = wrap.pastActivities;
      	
        pastWrappers.addAll( ActivityTimelineController.loadPastActivities( testAccount.Id, 'Call', '', pastWrappers.size() ) );
       // System.assertEquals( 0, openWrappers.size()+pastWrappers.size() );
        
        pastWrappers.addAll( ActivityTimelineController.loadPastActivities( testAccount.Id, 'Call', '', pastWrappers.size() ) );
        System.assertEquals( 0, openWrappers.size()+pastWrappers.size() );
        Test.stopTest();
    }
}