/***********************************************************************************
 * @author      10k-Expert
 * @name		OpportunityTriggerHandler
 * @date		2018-11-14
 * @description	TriggerHandler of OpportunityTrigger
 ***********************************************************************************/
public class OpportunityTriggerHandler {
    /******************************************************************************
     * @description after update of opportunity
     * @return 		void
     * @author 		10K-Expert
     * @param		List of new opportunities and map of old records of opportunity
     *******************************************************************************/
    public static void onAfterUpdate(List<Opportunity> newOppLst, Map<Id, Opportunity> oldOppMap){
        Set<Id> accIds = new Set<Id>();
        for(Opportunity opp : newOppLst){
            if( opp.IsClosed != oldOppMap.get(opp.Id).IsClosed
                && opp.IsClosed){
                accIds.add(opp.AccountId);
            }
        }
        
        List<Company_Request__c> companyRequestLst = [SELECT Id, Status__c FROM Company_Request__c WHERE Status__c = 'Pending' AND Company__c IN :accIds];
        
        for(Company_Request__c companyRequest : companyRequestLst){
            companyRequest.Status__c = 'Rejected';
        }
        
        if(!companyRequestLst.isEmpty()){
            // We allowing anyone to perform action via Ownership requirements
            CompanyRequestTriggerHandler.IS_NOT_COMPANY_REQUEST_PROCESS = false;
            update companyRequestLst;
            CompanyRequestTriggerHandler.IS_NOT_COMPANY_REQUEST_PROCESS = true;
        }
    }
}