/*
Class Name   : KeyContactScopeTriggerHandler
Description  : Handler for trigger on Key Contact scope object
Author       : Sukesh
Created Date : 26th August 2019
*/
public with sharing class KeyContactScopeTriggerHandler {

    public static Boolean runOnce = true;

    //Constructor
    public KeyContactScopeTriggerHandler() {

    }

    /**
    @Description Method that will be called on before update of Key Contact Scope
    @author Sukesh
    @date 26th August 2019
    @param  lstNewAccount List of newly updated Key Contact Scope
    @param mapIdToOldKeyContactScope map containing old values of newly updated Key Contact Scope
    */
    public void onBeforeUpdate(List<Key_Contact_Scope__c> lstNewKeyContactScope, Map<Id, Key_Contact_Scope__c> mapIdToOldKeyContactScope) {

        //Iterate over newly Updated Key Contact Scope
        for(Key_Contact_Scope__c objKeyContactScope : lstNewKeyContactScope) {

            if(String.isEmpty(objKeyContactScope.Best_Contact_Email__c)) {

                objKeyContactScope.Best_Contact__c = null;
            }

        }
    }

    /**
    @Description Method that will be called on after Update of Key Contact Scope
    @author Sukesh
    @date 26th August 2019
    @param  lstNewAccount List of newly updated Key Contact Scope
    @param mapIdToOldKeyContactScope map containing old values of newly updated Key Contact Scope
    */
    public void onAfterUpdate(List<Key_Contact_Scope__c> lstNewKeyContactScope, Map<Id, Key_Contact_Scope__c> mapIdToOldKeyContactScope) {

        Set<String> setBestContactEmail = new Set<String>();
        Set<String> setKeyContactScopeWithoutBestContact = new Set<String>();

        //Iterate over newly Updated Key Contact Scope
        for(Key_Contact_Scope__c objKeyContactScope : lstNewKeyContactScope) {

            if(!String.isEmpty(objKeyContactScope.Best_Contact_Email__c)) {

                    setBestContactEmail.add(objKeyContactScope.Best_Contact_Email__c);
            }
        }

        if(!setBestContactEmail.isEmpty())
            populateBestContact(setBestContactEmail);

        KeyContactScopeTriggerHandler.runOnce = true;
    }

    /**
    @Description Method that populate Best Contact on Key Contact Scope based in Best Contact Email
    @author Sukesh
    @date 26th August 2019
    @param  setBestContactEmail set of Best Contact Emails
    */
    private void populateBestContact(Set<String> setBestContactEmail) {

        List<Key_Contact_Scope__c> lstKeyContactScopeToUpdate = new List<Key_Contact_Scope__c>();

        Map<String, String> mapEmailToContactId = new Map<String, String>();

        for(Contact objContact : [SELECT Id,
                                         Email
                                    FROM Contact
                                   WHERE Email IN : setBestContactEmail]) {

            mapEmailToContactId.put((objContact.Email).toLowerCase(), objContact.Id);
        }


        for(Key_Contact_Scope__c objKeyContactScope : [SELECT Id,
                                                              Best_Contact_Email__c
                                                         FROM Key_Contact_Scope__c
                                                        WHERE Best_Contact_Email__c IN : setBestContactEmail]) {

            if(mapEmailToContactId.containsKey((objKeyContactScope.Best_Contact_Email__c).toLowerCase())) {

                lstKeyContactScopeToUpdate.add(new Key_Contact_Scope__c(Id=objKeyContactScope.Id,
                                                                    Best_Contact__c = mapEmailToContactId.get(objKeyContactScope.Best_Contact_Email__c)));
            }

            else {

                lstKeyContactScopeToUpdate.add(new Key_Contact_Scope__c(Id=objKeyContactScope.Id,
                                                                    Best_Contact__c = null));
            }
        }

        update lstKeyContactScopeToUpdate;
    }
}