/**
 * Test class coverage for trigger ContactTrigger
 *
 @author Thomas Lesnick
 @created 2015-10-01
 @version 1.0
 @since API 37.0 
 *
 Last Modified
 2018-01-16   WNG   Bulkifed code and fixed "Duplicate id in list" error
 *
 */
@isTest
public  class ContactTrigger_Test {

	public static testMethod void Test_KeyContact(){

		// insert test account
		Account acct = new Account(Name='testacct');
		acct.Website__c='http://saasbydesign.com';
		acct.OwnerId=UserInfo.getUserId();
		insert acct;

		// insert test contacts
		list<Contact> cList = new list<Contact>();
		cList.add(new Contact(Key_Contact__c=true, FirstName='Troy1', LastName='Milton Wing', Email='thomas.lesnick@insightpartners.com', AccountId=acct.id));
		cList.add(new Contact(Key_Contact__c=true, FirstName='Troy2', LastName='Milton Wing', Email='thomas.lesnick@insightpartners.com' ,AccountId=acct.id));
		cList.add(new Contact(Key_Contact__c=true, FirstName='Troy3', LastName='Milton Wing', Email='thomas.lesnick@insightpartners.com' ,AccountId=acct.id));
		cList.add(new Contact(Key_Contact__c=true, FirstName='Troy4', LastName='Milton Wing', Email='thomas.lesnick@insightpartners.com' ,AccountId=acct.id));
		cList.add(new Contact(Key_Contact__c=true, FirstName='Troy5', LastName='Milton Wing', Email='thomas.lesnick@insightpartners.com' ,AccountId=acct.id));
		insert cList;

		// reset the recursive trigger flag
		checkRecursive.resetAll();

		Test.startTest();
		cList[1].Key_Contact__c=true;	// only 2nd contact is the key contact
		update cList[1];
		Test.stopTest();
		
		List<Contact> contacts = [select id, FirstName, LastName from Contact where AccountId=:acct.Id and Key_Contact__c=true];
		system.assertEquals(1, contacts.size(), 'Total key contacts rows should return exactly 1 row.');
		system.assertEquals(cList[1].Id, contacts[0].Id, 'Incorrect key contact record returned.');
	}

}