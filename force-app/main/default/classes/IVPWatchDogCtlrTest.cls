@isTest
private class IVPWatchDogCtlrTest{
    @isTest private static void testgetCompanySignals(){
        IVPTestFuel tFuel = new IVPTestFuel();
        List<ListView> lv = [SELECT Id FROM ListView WHERE SobjectType = 'Account' LIMIT 1];
        List<Account> companies = tFuel.companies;
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<Company_Signal__c> companySignals = tFuel.companySignals;
        String json ='{"viewId" : "'+lv[0].Id+'"}';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new IVPWatchDogMock());
        IVPWatchDogCtlr.IVPWDData wdData = IVPWatchDogCtlr.getCompanySignals(json);
        
        Test.stopTest();
    }
    @isTest private static void testgetInitData(){
        IVPTestFuel.updateCurrentUserSetting();
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<Company_Signal__c> companySignals = tFuel.companySignals;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IVPWatchDogMock());
        IVPWatchDogCtlr.IVPWDData wdData = IVPWatchDogCtlr.getInitData(JSON.serialize(new Map<String,Object>{'isLimit'=>false}));
        
        Test.stopTest();
    }
    @isTest private static void testgetTasks(){
        IVPTestFuel tFuel=new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<Company_Signal__c> companySignals = tFuel.companySignals;
        List<Task> tasks=tfuel.tasks;
        String ids='["'+companies[0].id+'","'+companies[1].Id+'"]';
        Test.startTest();
        Object obj=IVPWatchDogCtlr.getTasks(ids);
        Map<String,List<Task>> taskMap = new  Map<String,List<Task>>();
        taskMap=(Map<String,List<Task>>)obj;
        
        Test.stopTest();
    }
    
    @isTest private static void testgetKeyContacts(){
        IVPTestFuel tFuel=new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Contact> contacts = tFuel.contacts;
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<String> accIds = new List<String>();
        for(Account acc : companies){
            accIds.add(acc.Id);
        }
        Test.startTest();
            Map<String, List<Id>> accKeyConMap = (Map<String, List<Id>>)IVPWatchDogCtlr.getKeyContacts(JSON.serialize(accIds));

            List<Signal_Category__c> categories = (List<Signal_Category__c>) IVPWatchDogCtlr.getCategories();
        Test.stopTest();
    }
    @isTest private static void testMaintainFilters(){
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<Company_Signal__c> companySignals = tFuel.companySignals;
        
        IVPWatchDogCtlr.InputData vData = new IVPWatchDogCtlr.InputData();
        vData.modifyMetaData = true;
        vData.selClrsOps = new List<String>{'red','green'};
        vData.selCatsOps = new List<String>();
        for(Signal_Category__c signalCategory : signalCategories){
            vData.selCatsOps.add(signalCategory.Id);
        }
        
        Test.startTest();
            IVPWatchDogCtlr.setIVPSDSettings(JSON.serialize(vData));
            Map<String, List<String>> wdFilter = IVPWatchDogCtlr.getWDFilters();
            System.assertEquals(2, wdFilter.get('colours').size());
            System.assertEquals(2, wdFilter.get('categories').size());
        Test.stopTest();
    }
    
    @isTest private static void testProcessKeyContact(){
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Contact> contacts = tFuel.contacts;
        contacts[0].AccountId = companies[0].Id;
        contacts[0].Salesloft_Id__c = SalesloftMock.people1Id;
        contacts[0].Key_Contact__c = TRUE;
        update contacts;
        
        insert new Salesloft_Cadence__c(name='Test1',Salesloft_Id__c = SalesloftMock.cadenceId1 );
        insert new IVP_General_Config__c(Name = 'salesloft-api-key', Value__c = 'Test');
        List<Signal_Category__c> signalCategories = tFuel.signalCategories;
        List<Company_Signal__c> companySignals = tFuel.companySignals;
        Test.startTest();
            IVPWatchDogCtlr.processKeyContact(JSON.serialize(new Map<String,Object>{'accId'=>companies[0].Id,'salesloftCadence'=>'Test1'}));
            System.assertEquals(1, [SELECT COUNT() FROM Salesloft_Cadence_Member__c WHERE Contact__c=:contacts[0].Id]);
        Test.stopTest();
        List<Salesloft_Cadence_Member__c> cadence = new List<Salesloft_Cadence_Member__c>([SELECT Id from Salesloft_Cadence_Member__c]);
        system.assert(cadence[0].id !=NUll);
    }
    
    @isTest private static void getCadenceMemberDetails() {
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Contact> contacts = tFuel.contacts;
        List<Salesloft_Cadence__c> salesloftCadences = tFuel.salesloftCadences;
        List<Salesloft_Cadence_Member__c> salesloftCadenceMembers = tFuel.salesloftCadenceMembers;
        List<String> contId = new List<String>();
        for(Contact cont : contacts) {
            contId.add(cont.Id);
        }
        List<String> nameStr = new List<String>();
        for(Salesloft_Cadence__c cont : salesloftCadences) {
            nameStr.add(cont.Name);
        }
        Map<String, List<String>>  inputData = new Map<String, List<String>>();
        inputData.put('selClrsOps', contId);
        inputData.put('selCatsOps', nameStr);
        Test.startTest();
        List<Salesloft_Cadence_Member__c> cadenceMember = IVPWatchDogCtlr.getCadenceMemberDetails(JSON.serialize(inputData));
        System.assertEquals(1, [SELECT COUNT() FROM Salesloft_Cadence_Member__c WHERE Contact__c=:contacts[0].Id]);
        Test.stopTest();
    }
    
    
}