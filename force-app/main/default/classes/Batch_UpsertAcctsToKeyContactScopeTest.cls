/*
Class Name   :  Batch_UpsertAccountsToKeyContactScope_Test
Description  :  Test Code for Upserting Prospect and Mark for deletion false Account records to Key Contact Scope Object
Author       :  Sukesh
Created On   :  August 07 2019
*/
@isTest
private class Batch_UpsertAcctsToKeyContactScopeTest {

    @TestSetup
    static void createTestData(){

        List<Clearbit_Role_Settings__c> lstRoleSettings = new List<Clearbit_Role_Settings__c>();
        lstRoleSettings.add(new Clearbit_Role_Settings__c(Name = 'CEO', Active__c = TRUE, Ranking__c = 1, Role__c = 'CEO'));
        lstRoleSettings.add(new Clearbit_Role_Settings__c(Name = 'Founder', Active__c = TRUE, Ranking__c = 4, Role__c = 'Founder'));
        insert lstRoleSettings;

        List<DWH_Role_Settings__c> listDWHRoles = new List<DWH_Role_Settings__c>();
        listDWHRoles.add(new DWH_Role_Settings__c(Name = 'CEO', Active__c = TRUE, Ranking__c = 1, Role__c = 'CEO'));
        listDWHRoles.add(new DWH_Role_Settings__c(Name = 'Founder', Active__c = TRUE, Ranking__c = 4, Role__c = 'Founder'));
        insert listDWHRoles;

        List<Account> lstAccountToInsert = new List<Account>();
        List<Contact> lstContactToInsert = new List<Contact>();

        for(Integer intCount=0; intCount<25; intCount++) {

            lstAccountToInsert.add(new Account(RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId(),
                                                       Name = 'Test Account '+intCount));
        }

        insert lstAccountToInsert;

        for(Integer intAccountCount=0; intAccountCount<20; intAccountCount++) {

            /*for(Integer intContactCount = 0; intContactCount<2; intContactCount++) {

                lstContactToInsert.add(new Contact(LastName = 'Test Contact '+intAccountCount+' '+intContactCount,
                                                   AccountId = lstAccountToInsert[intAccountCount].Id));
            }*/
        }

        insert lstContactToInsert;
    }

    @isTest
    static void testKeyContactScopeObjectCreation(){

        Test.startTest();
         Database.executeBatch(new Batch_UpsertAccountsToKeyContactScope());
        Test.stopTest();

        System.assertEquals(25, [SELECT Id FROM Key_Contact_Scope__c].size());
    }
}