public with sharing class IntelligenceROIHelper {
    
    public static final String STR_ROI_II_NAME = System.Label.ROI_II_Name;
    public static final String STR_ROI_II_BULK = System.Label.ROI_II_Bulk;
    public static final String STR_ROI_II_THESIS = System.Label.ROI_II_Thesis;
    public static final String STR_ROI_INSIGHT_INTELLIGENCE_PAGE_OPENED = System.Label.ROI_Insight_Intelligence_Page_Opened;
    public static final String STR_ROI_INTERFACE_LOAD_EVENT = System.Label.ROI_Interface_Load_Event;

    public static final String STR_ROI_JUST_FOR_YOU = System.Label.ROI_Just_For_You;
    public static final String STR_ROI_NEW_PREF_ADDED = System.Label.ROI_A_New_Preference_Added_W_Critera_Fields_Event	 ;
    public static final String STR_ROI_EXISTING_PREF_EDITED = System.Label.ROI_An_Existing_Preference_Edited_W_Critera_Field_Event;
    public static final String STR_ROI_JFY_UNIVERSE_VIEW = System.Label.ROI_Just_For_You_Universe_View_Event;
    public static final String STR_ROI_COMPANY_PAGE = System.Label.ROI_Company_Page;
    public static final String STR_ROI_ANALYST_TAKES_OWNERSHIP = System.Label.ROI_Analyst_takes_ownership_of_a_Company_Event;
    public static final String STR_ROI_ANALYST_RELINQUISHES_OWNERSHIP = System.Label.ROI_Analyst_Relinquishes_Ownership_of_a_Company_Event;
    public static final String STR_ROI_USER_FEEDBACK_CHANGED = System.Label.ROI_User_Feedback_Changed_Event;
    public static final String STR_ROI_SMART_DASH = System.Label.ROI_Smart_Dash;
    public static final String STR_ROI_WATCHDOG_PANE_OPENED = System.Label.ROI_Watchdog_Pane_Opened_Event;

    public IntelligenceROIHelper(ApexPages.StandardController stdController) {

    }

    /*@RemoteAction
    public static void createIntelligenceROI( List<Intelligence_ROI__c> lstIntelligenceROI ) {

        System.debug('==lstIntelligenceROI==='+lstIntelligenceROI);
        List<Intelligence_ROI__c> lstIntelligenceROIToBeUpdated = new List<Intelligence_ROI__c>();
        Map<String, ROI_Fieldset_Detail__mdt> mapEventTypeToFeildSetDetails = new Map<String, ROI_Fieldset_Detail__mdt>();
        String strRequestData = '\"request_data\": {';
        String strAppData = '\"app_data\": {';
        Map<String, Schema.FieldSet> mapAccountFieldSet = Schema.SObjectType.Account.fieldSets.getMap();
        Schema.FieldSet fieldSetAppData;
        Schema.FieldSet fieldSetReqData;

        for(ROI_Fieldset_Detail__mdt objROIFieldsetDetail : [SELECT Id, 
                                                                    MasterLabel, 
                                                                    App_Data_Field_Set__c, 
                                                                    Request_Data_Field_Set__c
                                                                    FROM ROI_Fieldset_Detail__mdt]) {

            mapEventTypeToFeildSetDetails.put(objROIFieldsetDetail.MasterLabel.toLowerCase(), objROIFieldsetDetail);
        }

        for(Intelligence_ROI__c objIntelligenceROI : lstIntelligenceROI) {

            if(mapEventTypeToFeildSetDetails.containsKey(objIntelligenceROI.event_type__c.toLowerCase())) {

                if(mapAccountFieldSet.containsKey(mapEventTypeToFeildSetDetails.get(objIntelligenceROI.event_type__c.toLowerCase()).App_Data_Field_Set__c))
                    fieldSetAppData = mapAccountFieldSet.get(mapEventTypeToFeildSetDetails.get(objIntelligenceROI.event_type__c.toLowerCase()).App_Data_Field_Set__c);
                if(mapAccountFieldSet.containsKey(mapEventTypeToFeildSetDetails.get(objIntelligenceROI.event_type__c.toLowerCase()).Request_Data_Field_Set__c))
                    fieldSetReqData = mapAccountFieldSet.get(mapEventTypeToFeildSetDetails.get(objIntelligenceROI.event_type__c.toLowerCase()).Request_Data_Field_Set__c);
            }

            if(fieldSetAppData != null) {
                
                for(Schema.FieldSetMember appDataFieldMembers : fieldSetAppData.getFields()) {

                    strAppData += '\"' + appDataFieldMembers.getFieldPath().replace('__c', '') +'\"' + ': \"' + objIntelligenceROI.Company__r.get(appDataFieldMembers.getFieldPath()) + '\", ';
                }
                strAppData = strAppData.removeEnd(', ');
            }
            strAppData += ' }';

            if(fieldSetReqData != null) {
            
                for(Schema.FieldSetMember reqDataFieldMembers : fieldSetReqData.getFields()) {

                    strRequestData += '\"' + reqDataFieldMembers.getFieldPath().replace('__c', '') +'\"' + ': \"' + objIntelligenceROI.Company__r.get(reqDataFieldMembers.getFieldPath()) + '\", ';
                }
                strRequestData = strRequestData.removeEnd(', ');
            }
            strRequestData += ' }';
            
            objIntelligenceROI.org_id__c = UserInfo.getOrganizationId();
            objIntelligenceROI.User__c = UserInfo.getUserId();
            objIntelligenceROI.request_data__c = strRequestData;
            objIntelligenceROI.app_data__c = strAppData;
            lstIntelligenceROIToBeUpdated.add(objIntelligenceROI);
        }
        insert lstIntelligenceROIToBeUpdated;
    }*/

    /*@RemoteAction
    public static void createIntelligenceROI(String appName, String evtType, Account thisAccount, String strTrigger) {

        System.debug('==thisAccount==='+thisAccount);
        Map<String, ROI_Fieldset_Detail__mdt> mapEventTypeToFeildSetDetails = new Map<String, ROI_Fieldset_Detail__mdt>();
        String strRequestData = '\"request_data\": {';
        String strAppData = '\"app_data\": {';
        Map<String, Schema.FieldSet> mapAccountFieldSet = Schema.SObjectType.Account.fieldSets.getMap();
        Schema.FieldSet fieldSetAppData;
        Schema.FieldSet fieldSetReqData;

        for(ROI_Fieldset_Detail__mdt objROIFieldsetDetail : [SELECT Id, MasterLabel, App_Data_Field_Set__c, Request_Data_Field_Set__c
                                                                    FROM ROI_Fieldset_Detail__mdt]) {

            mapEventTypeToFeildSetDetails.put(objROIFieldsetDetail.MasterLabel, objROIFieldsetDetail);
        }

        if(mapEventTypeToFeildSetDetails.containsKey(evtType.toLowerCase())) {

            if(mapAccountFieldSet.containsKey(mapEventTypeToFeildSetDetails.get(evtType.toLowerCase()).App_Data_Field_Set__c))
                fieldSetAppData = mapAccountFieldSet.get(mapEventTypeToFeildSetDetails.get(evtType.toLowerCase()).App_Data_Field_Set__c);
            if(mapAccountFieldSet.containsKey(mapEventTypeToFeildSetDetails.get(evtType.toLowerCase()).Request_Data_Field_Set__c))
                fieldSetReqData = mapAccountFieldSet.get(mapEventTypeToFeildSetDetails.get(evtType.toLowerCase()).Request_Data_Field_Set__c);
        }

        if(fieldSetAppData != null) {
            
            for(Schema.FieldSetMember appDataFieldMembers : fieldSetAppData.getFields()) {

                strAppData += '\"' + appDataFieldMembers.getFieldPath().replace('__c', '') +'\"' + ': \"' + thisAccount.get(appDataFieldMembers.getFieldPath()) + '\", ';
            }
            strAppData = strAppData.removeEnd(', ');
        }
        strAppData += ' }';

        if(fieldSetReqData != null) {
          
            for(Schema.FieldSetMember reqDataFieldMembers : fieldSetReqData.getFields()) {

                strRequestData += '\"' + reqDataFieldMembers.getFieldPath().replace('__c', '') +'\"' + ': \"' + thisAccount.get(reqDataFieldMembers.getFieldPath()) + '\", ';
            }
            strRequestData = strRequestData.removeEnd(', ');
        }
        strRequestData += ' }';
        
        Intelligence_ROI__c objIntelligenceROI = new Intelligence_ROI__c( app_name__c = appName,
                                                                          Company__c = thisAccount.Id,
                                                                          company_id__c = thisAccount.Name,
                                                                          company_url__c = thisAccount.Name,
                                                                          event_type__c = evtType,
                                                                          org_id__c = UserInfo.getOrganizationId(),
                                                                          Trigger__c = strTrigger,//'(ClassName.Method())'
                                                                          User__c = UserInfo.getUserId(),
                                                                          request_data__c = strRequestData,
                                                                          response_status__c = 'Queued',
                                                                          app_data__c = strAppData);
        insert objIntelligenceROI;
    }*/
    
    @RemoteAction
    @AuraEnabled
    public static void createIntelligenceROI( String strApplicationName,
                                            // String strCompanyDWHID,
                                            // String strCompanyURL,
                                              String strCompanyId,
                                              String strEventType,
                                              String strAppData,
                                              String strFieldValues,
                                              String strResponseStatus,
                                              String strResponseData,
                                              String strRequestData ) {
        
        List<Account> lstAccount  = new List<Account>();
        if(String.isNotBlank(strCompanyId))
            lstAccount = [SELECT Id, Cross_Table_Id__c, Website 
                                 FROM Account 
                                 WHERE Id =: strCompanyId 
                                 LIMIT 1];

        Intelligence_ROI__c objIntelligenceROI = new Intelligence_ROI__c( app_name__c = strApplicationName,
                                                                          Company__c = lstAccount.isEmpty() ? null : lstAccount[0].Id,
                                                                          company_id__c = lstAccount.isEmpty() ? '' : lstAccount[0].Cross_Table_Id__c,
                                                                          company_url__c = lstAccount.isEmpty() ? '' : lstAccount[0].Website,
                                                                          event_type__c = strEventType,
                                                                          org_id__c = UserInfo.getOrganizationId(),
                                                                          //Trigger__c = strTrigger,//'(ClassName.Method())'
                                                                          User__c = UserInfo.getUserId(),
                                                                          request_data__c = strRequestData,
                                                                          response_status__c = strResponseStatus,
                                                                          response_data__c = strResponseData,
                                                                          app_data__c = strAppData);
        insert objIntelligenceROI;    
    }

    public static void SFApplicationAPI(List<Intelligence_ROI__c> lstIntelligenceROI) {
        System.debug('lstIntelligenceROI'+lstIntelligenceROI);
        System.debug('lstIntelligenceROI size'+lstIntelligenceROI.size());
        List<Intelligence_ROI__c> lstIntelligenceROIToBeUpdated = new List<Intelligence_ROI__c>();
        DWH_Settings__c objDWHSettings = DWH_Settings__c.getInstance(UserInfo.getUserId());
        String strRequest = '[';
        
        for(Intelligence_ROI__c objIntelligenceROI : lstIntelligenceROI) {
            System.debug('lstIntelligenceROI'+objIntelligenceROI);
            // if(objIntelligenceROI.app_name__c != null
            //    && objIntelligenceROI.User__c !=null 
            //    && objIntelligenceROI.event_type__c != null
            //    && objIntelligenceROI.org_id__c != null)
            //strRequest += '{\"app_name\": \"' + objIntelligenceROI.app_name__c + '\", \"user_id\": \"' + objIntelligenceROI.User__c + '\", \"org_id\": \"' + objIntelligenceROI.org_id__c + '\", \"event_type\": \"' + (objIntelligenceROI.event_type__c != null ? objIntelligenceROI.event_type__c.toLowerCase() : '') + '\",' + (objIntelligenceROI.company_id__c != null ? ' \"company_id\": \"' + objIntelligenceROI.company_id__c + '\",' : '') +  (objIntelligenceROI.company_url__c != null ? '\"company_url\": \"' + objIntelligenceROI.company_url__c + '\", ' : '') + '\"response_status\": \"' + (objIntelligenceROI.response_status__c == 'Queued' ? 'success' : objIntelligenceROI.response_status__c.toLowerCase())  + '\", ' + (objIntelligenceROI.app_data__c == null ? '' : objIntelligenceROI.app_data__c) + '},';//+ ', ' + (objIntelligenceROI.request_data__c == null ? '' : objIntelligenceROI.request_data__c)           
            strRequest += '{\"app_name\": \"' + objIntelligenceROI.app_name__c + '\", \"user_id\": \"' + objIntelligenceROI.User__c + '\", \"org_id\": \"' + objIntelligenceROI.org_id__c + '\", \"event_type\": \"' + (objIntelligenceROI.event_type__c != null ? objIntelligenceROI.event_type__c.toLowerCase() : '') + '\",' + (objIntelligenceROI.company_id__c != null ? ' \"company_id\": \"' + objIntelligenceROI.company_id__c + '\",' : '') +  (objIntelligenceROI.company_url__c != null ? '\"company_url\": \"' + objIntelligenceROI.company_url__c + '\", ' : '') + '\"response_status\": \"' + (objIntelligenceROI.response_status__c == 'Queued' ? 'success' : objIntelligenceROI.response_status__c.toLowerCase())  + '\"},';           
        	
        }
        strRequest = strRequest.removeEnd(',');
        strRequest += ']';
        System.debug('strRequest'+strRequest);
		System.debug('strRequest'+strRequest.length());
        HttpRequest req = new HttpRequest();
        
        req.setHeader('Authorization', objDWHSettings.Auth_Key__c);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setBody(strRequest);
        req.setEndpoint(objDWHSettings.Endpoint__c+'/roi/sf');
        req.setMethod('POST');
        System.debug('req'+req);
        HTTPResponse objResp = new Http().send(req);
        System.debug('objResp'+objResp);
        System.debug('===objResp==>>'+objResp.getBody());
        if(objResp.getStatusCode() == 200) {

            List<ROIResponseReader> lstROIResponse = ROIResponseReader.parse(objResp.getBody());
            for(integer i = 0; i < lstROIResponse.size(); i++) {
                 System.debug('lstROIResponse[i].success'+lstROIResponse[i].success);
                if(lstROIResponse[i].success)
                   
                    lstIntelligenceROI[i].response_status__c = 'Success';
                else {  
                    lstIntelligenceROI[i].response_status__c = 'Failed';
                    lstIntelligenceROI[i].Exception__c = String.valueOf(lstROIResponse[i].error);
                }
                lstIntelligenceROIToBeUpdated.add(lstIntelligenceROI[i]);
            }
        } 
        else {
        
            //Update IntelligenceROI with Error response    
            for(Intelligence_ROI__c objIntelligenceROI : lstIntelligenceROI) {
                objIntelligenceROI.response_status__c = 'Failed';
                objIntelligenceROI.response_data__c = objResp.getBody();
                lstIntelligenceROIToBeUpdated.add(objIntelligenceROI);
            }
        }
        update lstIntelligenceROIToBeUpdated;
    }
}