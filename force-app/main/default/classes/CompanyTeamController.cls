public without sharing class CompanyTeamController {
	public CompanyTeamController() {
		
	}

	@AuraEnabled
	public static String fetchInitData(String recordId)
	{
		Common_Config__c config = CommonConfigUtil.getConfig();
		InitValues retValue = new InitValues();
		retValue.teamMembers = [Select Id,UserId,AccountId,Account.Name from AccountTeamMember where UserId =:recordId AND Account.Website_Status__c = 'Current Investment'];
        retValue.domainName=config.SalesforcebaseURL__c;
		return JSON.serialize(retValue);
	}
	public class InitValues
	{
		public List<AccountTeamMember> teamMembers;
		public String domainName;
	}
}