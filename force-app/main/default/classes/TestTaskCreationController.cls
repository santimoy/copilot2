@isTest
private class TestTaskCreationController {
    @testSetup static void setup() {
       Account testAccts = new Account(Name = 'TestAcct',OwnerId=UserInfo.getUserId());
       insert testAccts;   
       Task objTask = new Task(Subject = 'Test');
       INSERt objTask;   
       
       Opportunity oppt = new Opportunity(Name ='Test Opportunity',
       //AccountID = testAcct.ID,
       StageName = 'Customer Won',
       //Amount = 3000,
       CloseDate = System.today()
       );
    }
  
    
    @isTest
    public static void testCreateIntelligenceROI() {
        
        List<Account> lstAccount = [SELECT Id, Name, DWH_Name__c, KCName__c, Fax, Phone, Active__c, Account_OwnerId__c FROM Account];
        for(integer i = 0; i < lstAccount.size(); i++) {
            TaskCreationController.createIntelligenceROI(IntelligenceROIHelper.STR_ROI_II_THESIS,
                                                        lstAccount[i].Id,
                                                        IntelligenceROIHelper.STR_ROI_INSIGHT_INTELLIGENCE_PAGE_OPENED,
                                                        '',
                                                        '',
                                                        'Queued',
                                                        '',
                                                        '');
        }
        system.assertEquals(true, [SELECT Id FROM Intelligence_ROI__c].size() != 0);

    }

	/*@isTest static void test_method_one() {
		Account a = new Account(Name ='Testaccount');
		insert a;
		TaskCreationController.fetchInitData(a.Id);
		TaskCreationController.assignCurrentUser(a.Id);
		String todayString = String.valueOf(Date.today());
        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Inbound Email';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'taskComments';
        taskData.taskActivityDate = todayString;
        taskData.taskNextAction = '';
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = '';
        taskData.clearAccountNextAction = false;
        taskData.clearAccountNextActionDate = false;

		TaskCreationController.createNewTask(a.Id,JSON.serialize(taskData), new Task_Comment__c());
        taskData.taskType = 'Outbound Email';
		TaskCreationController.createNewTask(a.Id,JSON.serialize(taskData), new Task_Comment__c());
        taskData.taskType = 'Voicemail';
		TaskCreationController.createNewTask(a.Id,JSON.serialize(taskData), new Task_Comment__c());
        taskData.taskType = 'Examine';
		TaskCreationController.createNewTask(a.Id,JSON.serialize(taskData), new Task_Comment__c());
        taskData.taskType = 'Call';
		TaskCreationController.createNewTask(a.Id,JSON.serialize(taskData), new Task_Comment__c());
	}*/
    
	@isTest
    public static void testAssignCurrentUser(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        List<Account> acclst = testData.accounts;
        
        Test.startTest();
        String assignMsg = TaskCreationController.assignCurrentUser(accLst[0].Id);
        Test.stopTest();
        
        System.assertEquals('SUCCESS', assignMsg);
    }
    
	// @isTest
    // public static void testReassignCurrentUser(){
    //     IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        
    //     List<Account> acclst = testData.accounts;
    //     Id preOwnerId = acclst[0].OwnerId;
    //     Test.startTest();
    //         TaskCreationController.reassignCurrentUser(accLst[0].Id, 'Other', 'Don\'t want this company', '');
    //     Test.stopTest();
    //     System.assertNotEquals(preOwnerId, [SELECT Id, OwnerId FROM Account WHERE Id = :acclst[0].Id].OwnerId);
    //     TaskCreationController.reassignCurrentUser(accLst[0].Id, 'Other', 'Don\'t want this company', accLst[1].OwnerId);
    //     TaskCreationController.TaskCreationData t = (TaskCreationController.TaskCreationData)JSON.deserialize(TaskCreationController.fetchInitData(accLst[0].Id),
    //                                                 TaskCreationController.TaskCreationData.Class);
    //     System.debug(t);

    // }
    // @isTest
    // public static void testReassignCurrentUser1(){
    //     IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
    //     list<user> userUtils = [select id from user where NAME='Unassigned']; 
    //     IVPUtils.unassignedUserId = userUtils[0].id;
    //     List<Account> acclst = testData.accounts;
    //     Id preOwnerId = acclst[0].OwnerId;
    //     Test.startTest();
    //     TaskCreationController.reassignCurrentUser(accLst[0].Id, 'Other', 'Don\'t want this company', accLst[1].OwnerId);
    //     Test.stopTest();
    //     System.assertNotEquals(preOwnerId, [SELECT Id, OwnerId FROM Account WHERE Id = :acclst[0].Id].OwnerId);
    //      TaskCreationController.reassignCurrentUser(accLst[0].Id, 'Other', 'Don\'t want this company', '');
    //     TaskCreationController.TaskCreationData t = (TaskCreationController.TaskCreationData)JSON.deserialize(TaskCreationController.fetchInitData(accLst[0].Id),
    //                                                 TaskCreationController.TaskCreationData.Class);
    //     System.debug(t);

    // }
	
	@isTest
    public static void testCheckUnassignedValidation(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        List<Account> acclst = testData.accounts;
        
        Test.startTest();
        TaskCreationController.TaskCreationData taskData = TaskCreationController.checkUnassignedValidation(accLst[0].Id);
        Test.stopTest();
        
        System.assertEquals(taskData.validationData.companyOwnerWithCurrentStatus, 'false___Watch');
    }
    
    @isTest
    public static void testChangeCompanyStatusWithWatch(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        List<Account> acclst = testData.accounts;
        
        Test.startTest();
        ValidationWrapper validationWrapper = TaskCreationController.changeCompanyStatus(accLst[0].Id, 'Watch');
        ValidationWrapper validationWrapper_New = TaskCreationController.changeCompanyStatus(accLst[0].Id, 'Priority');
        Test.stopTest();
        try {
           TaskCreationController.changeCompanyStatus('', 'Watch');
        } catch (Exception e) {
        }  
        System.assert(String.isNotBlank(validationWrapper.successMsg));
    }
    
    // @isTest
    // public static void testChangeCompanyStatusWithPriority(){
    //     IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
    //     List<Account> acclst = testData.accounts;
        
    //     Test.startTest();
    //     ValidationWrapper validationWrapper = TaskCreationController.changeCompanyStatus(accLst[0].Id, 'Priority');
    //     Test.stopTest();
        
    //     System.assert(String.isNotBlank(validationWrapper.successMsg));
    // }
    
    @isTest
    public static void testRequestForOwnership(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        List<Account> acclst = testData.accounts;
        
        Test.startTest();
        ValidationWrapper validationWrapper1 = TaskCreationController.requestForOwnership(accLst[0].Id, 'Can you please assign this company to me');
        try{
            ValidationWrapper validationWrapper2 = TaskCreationController.requestForOwnership(accLst[0].Id, 'Can you please assign this company to me');
        }catch(AuraHandledException excp){
            // aura Exception as we already created company request for acclst[0]. 
            System.assert(true);
        }catch(Exception e){}
        Test.stopTest();
    }
    
    @isTest
    public static void testAcceptRequest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        List<Account> acclst = testData.accounts;
        
        Test.startTest(); 
        TaskCreationController.requestForOwnership(accLst[0].Id, 'Can you please assign this company to me');
        TaskCreationController.TaskCreationData t = (TaskCreationController.TaskCreationData)JSON.deserialize(TaskCreationController.fetchInitData(accLst[0].Id),
                                                    TaskCreationController.TaskCreationData.Class);
        // company request created
        System.assert(!t.validationData.requests.isEmpty());
        ValidationWrapper validationWrapper1 = TaskCreationController.acceptRequest(accLst[0].Id, 'Accepted', 'Request Accepted');
        
        TaskCreationController.requestForOwnership(accLst[0].Id, 'Can you please assign this company to me');
        List<Opportunity> oppLst = testData.opportunities;
        t = (TaskCreationController.TaskCreationData)JSON.deserialize(TaskCreationController.fetchInitData(accLst[0].Id),
                                                    TaskCreationController.TaskCreationData.Class);
        System.assert(t.isWaitToRepriortizeCheck);
        ValidationWrapper validationWrapper2 = TaskCreationController.acceptRequest(accLst[0].Id, 'Accepted', 'Request Accepted');
        
        
        Test.stopTest();
        try {
            ValidationWrapper validationWrapper = TaskCreationController.acceptRequest(accLst[0].Id, 'Pending', 'Request Accepted');
        } catch (AuraHandledException e) {
        }  
        System.assert(String.isNotBlank(validationWrapper1.successMsg));
        System.assert(String.isNotBlank(validationWrapper2.errorMsg));
        System.assertEquals([SELECT Id FROM Company_Request__c].size(), 2);
    }
    
    @isTest
    public static void testRejectRequest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        List<Account> acclst = testData.accounts;
        
        Test.startTest();
        TaskCreationController.requestForOwnership(accLst[0].Id, 'Can you please assign this company to me');
        TaskCreationController.rejectRequest(accLst[0].Id, 'Request rejected');
        Test.stopTest();
        
        System.assertEquals([SELECT Id, Status__c FROM Company_Request__c].Status__c, 'Rejected');
    }
    
    @isTest
    public static void testChangeOwnership(){
        // IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        // // List<User> users = testdata.users;
        
        // Test.startTest();
        
        // List<Account> acclst = testData.accounts;
        // ValidationWrapper validationWrapper = TaskCreationController.changeOwnership(accLst[0].Id);
        // Contact con = new Contact(LastName = 'Test'); // creating contact is for coving catch block code
        // insert con;
        
        // System.assertNotEquals(null,accLst[0].Id);
        // try {
        //     TaskCreationController.changeOwnership(con.Id);
        //     } catch (Exception e) {                         
        //     }  
        // Test.stopTest();
        Account acc = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];
        TaskCreationController.changeOwnership(acc.Id);
    }
	
	@isTest
    public static void testCompanyChangeStatus(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        List<Account> acclst = testData.accounts;
        List<Company_Request__c> companyRequests = testData.companyRequests;
        
        Test.startTest();
        ValidationWrapper validationWrapperForCompanyChangeStatus = TaskCreationController.changeCompanyStatus(acclst[0].Id, 'Watch');
            System.assert(String.isNotBlank(validationWrapperForCompanyChangeStatus.successMsg));
        Test.stopTest();
    }
  @isTest static void test_method_two() {
        Test.startTest();
        // Account objAccount = new Account(Name ='Testaccount');
        // insert objAccount;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];

        String strResponse = TaskCreationController.fetchInitDataFinancials(objAccount.Id);
         String todayString = String.valueOf(Date.today());
        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Call';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'taskComments';
        taskData.taskActivityDate = todayString;
        taskData.taskNextAction = '';   
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = 'accountPriority';
        taskData.clearAccountNextAction = true;
        taskData.clearAccountNextActionDate = true;

        TaskCreationController.saveData(objAccount.Id,JSON.serialize(taskData), 2018,
                                               2019,2020,
                                               2017,2018,
                                               2020,5643,
                                               2017,2017,
                                               1,2,3,4,5,6,7,8,
                                               'ARR', new Task_Comment__c());

        strResponse = TaskCreationController.fetchInitDataFinancials(objAccount.Id);
        taskData.taskType = 'Inbound email';
        TaskCreationController.saveData(objAccount.Id,JSON.serialize(taskData), 2018,
                                               2019,2020,
                                               2017,2018,
                                               2020,5643,
                                               2017,2017,
                                               1,2,3,4,5,6,7,8,
                                               'ARR', new Task_Comment__c());
         taskData.taskType = 'Outbound email';
      // System.assertEquals('TestAcct',[SELECT Id,Name FROM Account WHERE Id =:objAccount.Id].Name);
        /* TaskCreationController.saveData(objAccount.Id,JSON.serialize(taskData), 2018,
                                               2019,2020,
                                               2017,2018,
                                               2020,5643,
                                               2017,2017,
                                               1,2,3,4,5,6,7,8,
                                               'ARR', new Task_Comment__c());
        System.assertEquals('TestAcct',[SELECT Id,Name FROM Account WHERE Id =:objAccount.Id].Name);
        try {
           TaskCreationController.fetchInitData('');
        } catch (AuraHandledException e) {
        }  */
        Test.stopTest();
    }
    @isTest
    public static void testprepareMessage(){
        DateTime endDate = System.now();
        String msg = 'there is an error';
 
        Test.startTest();
            TaskCreationController.prepareMessage(endDate,msg);  
            System.assertNotEquals(null,endDate);
        Test.stopTest();
    }
    @isTest static void testextractErrorMessage(){
        DateTime endDate = System.now();
        String msg = 'Hello there _EXCEPTION, :there is an error';
        Test.startTest();
            TaskCreationController.extractErrorMessage(msg); 
            System.assertNotEquals(null,msg);
        Test.stopTest();
    }
    
    @isTest 
    static void testFetchInterval(){
        IVP_General_Config__c ivpGeneralConfig = new IVP_General_Config__c();
        Integer interval = TaskCreationController.fetchInterval();
        System.assertEquals(30000, interval);
    }
    
    @isTest 
    static void testCheckDraftComment(){
        
        // Account objAccount = new Account(Name ='Testaccount');
        // insert objAccount;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];

        Task_Comment__c objTaskComment = TaskCreationController.checkDraftComment(objAccount.Id);
        System.assertEquals(null, objTaskComment);
        
        Task_Comment__c oTaskComment = new Task_Comment__c(Company__c = objAccount.Id, 
                                                             User__c=UserInfo.getUserId(), 
                                                             Comment__c='Hi');
        insert oTaskComment;
        objTaskComment = TaskCreationController.checkDraftComment(objAccount.Id);
        System.assertNotEquals(null, objTaskComment);
    }
    
    @isTest 
    static void testSaveCommentAsDraft(){
        
        // Account objAccount = new Account(Name ='Testaccount');
        // insert objAccount;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];
        Task objTask = [SELECT Id FROM Task WHERE Subject = 'Test' LIMIT 1];

        String todayString = String.valueOf(Date.today());
        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Call';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'Hi There';
        taskData.taskActivityDate = todayString;
        taskData.taskNextAction = '';
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = 'accountPriority';
        taskData.clearAccountNextAction = true;
        taskData.clearAccountNextActionDate = true;

        TaskCreationController.talentData talntData = new TaskCreationController.talentData();
        talntData.activityType = 'Call';
        talntData.activitySales = true;
        talntData.taskComments = true;
        talntData.activityResearchDevelopment = true;
        talntData.activityOperations = true;
        talntData.activityMarketing = false; 
        talntData.activityCustomerSuccess = true;
        talntData.taskType = 'Call';
        talntData.talentTaskNextAction = '';
        talntData.talentTaskNextActionDate = '';
        talntData.taskIntExtVal = '';
        talntData.flwUpIntExtVal = '';
        talntData.isNextActionRequired = True;
        talntData.isSignificant = 'Yes';

        TaskCreationController.saveCommentAsDraft(objAccount.Id, new Task_Comment__c(), objTask.Id, JSON.serialize(taskData), JSON.serialize(talntData));
        System.assertEquals(1, [SELECT Id FROM Task_Comment__c].size());
        Task_Comment__c objTaskComment = [SELECT Id, Company__c, User__c, Comment__c, Task_Id__c FROM Task_Comment__c Limit 1];

        TaskCreationController.saveData(objAccount.Id,JSON.serialize(taskData), 2018,
                                               2019,2020,
                                               2017,2018,
                                               2020,5643,
                                               2017,2017,
                                               1,2,3,4,5,6,7,8,
                                               'ARR', objTaskComment);
        try{
            Account acc;
        TaskCreationController.prepareValidationData( acc);
        }
        catch(Exception ex){
            
        }
        System.assertEquals(true, [SELECT Id, Task_Id__c FROM Task_Comment__c][0].Task_Id__c != null);
    }
    
    @isTest 
    static void testDiscardDraftTaskComment(){
        
        // Account objAccount = new Account(Name ='Testaccount');
        // insert objAccount;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];


        String todayString = String.valueOf(Date.today());
        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Call';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'taskComments';
        taskData.taskActivityDate = todayString;
        taskData.taskNextAction = '';
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = 'accountPriority';
        taskData.clearAccountNextAction = true;
        taskData.clearAccountNextActionDate = true;

        TaskCreationController.saveCommentAsDraft(objAccount.Id, new Task_Comment__c(), null, JSON.serialize(taskData), null);
        System.assertEquals(1, [SELECT Id FROM Task_Comment__c].size());
        Task_Comment__c objTaskComment = [SELECT Id FROM Task_Comment__c Limit 1];

        TaskCreationController.discardDraftTaskComment(objTaskComment);
        System.assertEquals(0, [SELECT Id, Task_Id__c FROM Task_Comment__c].size());
        try {
            TaskCreationController.saveCommentAsDraft('', new Task_Comment__c(), null, JSON.serialize(taskData), null);
        } catch (Exception e) {
        }
    }
    
    @isTest
    public static void testCreateTalentTask(){
        
        // Account a = new Account(Name ='Test Company1');
		// insert a;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];

        Contact objCont = new Contact(AccountId = objAccount.Id,  LastName = 'Conner');
        INSERT objCont;
        Id loggedInUserId = UserInfo.getUserId();
        Task_Comment__c oTaskComment = new Task_Comment__c(Company__c = objAccount.Id, 
                                                             User__c=UserInfo.getUserId(), 
                                                             Comment__c='Hi');
        INSERT oTaskComment;
        Test.startTest();
        List<Id> meetingAttendeesIdList = new List<Id>();
        meetingAttendeesIdList.add(objCont.Id);
        TaskCreationController obj = new TaskCreationController();
		TaskCreationController.fetchInitData(objAccount.Id);
		TaskCreationController.assignCurrentUser(objAccount.Id);
		String todayString = String.valueOf(Date.today());
        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Inbound Email';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'taskComments';
        taskData.taskActivityDate = todayString;
        taskData.taskNextAction = '';
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = 'accountPriority';
        taskData.clearAccountNextAction = true;
        taskData.clearAccountNextActionDate = false;

        Id nextActionOwnerId = UserInfo.getUserId();
        String nextActionUsername = UserInfo.getUserName();
		//TaskCreationController.createTalentNewTask(objAccount.Id,JSON.serialize(taskData),true,false,true,false,true, oTaskComment, meetingAttendeesIdList, 'External','Internal',nextActionOwnerId, true,'Yes','Not Started',nextActionUsername,false);
       // taskData.taskType = 'Outbound Email';
		//TaskCreationController.createTalentNewTask(objAccount.Id,JSON.serialize(taskData),true,false,true,false,true, oTaskComment, meetingAttendeesIdList, 'External','Internal',nextActionOwnerId, false,'No','Not Started',nextActionUsername,false);
        //taskData.taskType = 'Other';
		//TaskCreationController.createTalentNewTask(objAccount.Id,JSON.serialize(taskData),true,false,true,false,true, oTaskComment, meetingAttendeesIdList, 'External','Internal',nextActionOwnerId, true,'Yes','Not Started',nextActionUsername,false);
        //taskData.taskType = 'Examine';
		//TaskCreationController.createTalentNewTask(objAccount.Id,JSON.serialize(taskData),true,false,true,false,true, oTaskComment, meetingAttendeesIdList, 'External','Internal',nextActionOwnerId, false,'No','Not Started',nextActionUsername,true);
        taskData.taskType = 'Call';
		TaskCreationController.createTalentNewTask(objAccount.Id,JSON.serialize(taskData),true,false,true,false,true, oTaskComment, meetingAttendeesIdList, 'External','Internal',nextActionOwnerId, true,'Yes','Not Started',nextActionUsername,true);
        System.assertNotEquals(null, taskData.taskType);
        try {
          TaskCreationController.assignCurrentUser('acc.Id');
        } catch (Exception e) {
        }  
        Test.stopTest();
    }
    
    @isTest
    public static void testclearTaskAttendees(){
        
        // Account a = new Account(Name ='Test Company1',Next_Action_Date__c = System.today());
		// insert a;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];

        Contact objCont = new Contact(AccountId = objAccount.Id, LastName = 'Conner');
        INSERT objCont;
        
        List<Id> meetingAttendeesIdList = new List<Id>();
        meetingAttendeesIdList.add(objCont.Id);
        
        Task objTask = new Task(Subject = 'Test');
        INSERt objTask;
        
        TaskRelation objTaskRelation = new TaskRelation(TaskId = objTask.Id, RelationId = objCont.Id);
        INSERT objTaskRelation;
        
        List<Id> taskRelationIdList = new List<Id>();
        taskRelationIdList.add(objTaskRelation.Id);
        TaskCreationController.clearTaskAttendees(objTask.Id, taskRelationIdList, new List<Id>());
        TaskCreationController.clearTaskAttendees(objTask.Id, taskRelationIdList, meetingAttendeesIdList);
        
        System.assertNotEquals(null,objTask.Id);
    }
      //This is to test when taskId getting null
    @isTest
    public static void testFetchMeetingAttendees(){
        Task testTask = new Task(Subject = 'Test');
        INSERT testTask;
        LIST<TaskRelation> fetchAttendees =  TaskCreationController.fetchMeetingAttendees(testTask.id);
        System.assertNotEquals(null,testTask.Id);
        LIST<TaskRelation> fetchAttendees_New =  TaskCreationController.fetchMeetingAttendees('');
    }
    
    @isTest
    public static void testfetchUser(){
        User ouser = TaskCreationController.fetchUser();
        TaskCreationController.getFieldsDetail();
        TaskCreationController.getCommentValueToPrePopulate();
        TaskCreationController.getCheckableFields();
        User usr = [SELECT Name FROM User WHERE Id =: userInfo.getUserId() ];
        //System.assertEquals('Copilot Dev', usr.Name);
        System.assertEquals(userInfo.getName(), usr.Name);
    }
   /* @isTest
    public static void testSaveDataElsePart(){
        // Account objAccount = new Account(Name ='Testaccount');
        // insert objAccount;
        Test.startTest();
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];

        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Voicemail';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'taskComments';
        taskData.taskActivityDate = String.valueOf(Date.today());
        taskData.taskNextAction = '';
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = 'accountPriority';
        taskData.clearAccountNextAction = true;
        taskData.clearAccountNextActionDate = true;
        TaskCreationController.saveData(objAccount.Id,JSON.serialize(taskData), 2018,
                                               2019,2020,
                                               2017,2018,
                                               2020,5643,
                                               2017,2017,
                                               1,2,3,4,5,6,7,8,
                                               'ARR', new Task_Comment__c());
        taskData.taskType = 'Examine';                                               
         TaskCreationController.saveData(objAccount.Id,JSON.serialize(taskData), 2018,
                                               2019,2020,
                                               2017,2018,
                                               2020,5643,
                                               2017,2017,
                                               1,2,3,4,5,6,7,8,
                                               'ARR', new Task_Comment__c());
        System.assertEquals('TestAcct',[SELECT Id,Name FROM Account WHERE Id =:objAccount.Id].Name); 
        Test.stopTest();                                  
    }*/
    @isTest
    public static void testTypeAheadFuncLtng(){
        // Account objAccount = new Account(Name ='Test Acc 02');
        // insert objAccount;
        Account objAccount = [SELECT Id FROM Account WHERE Name='TestAcct' LIMIT 1];

        TypeaheadFunction.recordData obj = new TypeaheadFunction.recordData();
        obj.isTalentUser = true;
        obj.recordId = objAccount.Id;
        String str = String.valueOf(obj);
        try{
            TaskCreationController.typeAheadFuncLtng('Name','Account','',str);
        }catch(Exception e){

        }
        
        // String str = '{"recordData": [{"isTalentUser": true,"recordId" : objAccount.Id}]}';
        // TaskCreationController.typeAheadFuncLtng('Name','Account','',str);
        // String strJSON = '{"isTalentUser":true,"recordId":objAccount.Id}';
        // TaskCreationController.typeAheadFuncLtng('Name','Account','',strJSON);
        // List<Object> lstStr = new List<Object>();
        // lstStr.add(true);
        // lstStr.add(objAccount.Id);
        // for(Object obj : lstStr){
        //     String strNew = JSON.serializePretty(obj);
        //     // String str = String.valueOf(obj);
        //     // String strNew = JSON.serialize(obj);
        //     TaskCreationController.typeAheadFuncLtng('Name','Account','',str);
        // }
    }
    @isTest
    public static void testReassignCurrentUser_New(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        list<user> userUtils = [select id from user where NAME='Unassigned']; 
        IVPUtils.unassignedUserId = userUtils[0].id;
      Test.startTest();
        Account acc = new Account(name='Test Acc',OwnerId=UserInfo.getUserId(),Next_Action_Date__c = date.newInstance(2020, 02, 20));
        insert acc;
        Account acc_New = new Account(name='Test Account',Next_Action_Date__c = date.newInstance(2020, 02, 20));
        insert acc_New;
        Id preOwnerId = acc.OwnerId;
        TaskCreationController.reassignCurrentUser(acc.Id, 'Other', 'Don\'t want this company', acc.OwnerId);
        Test.stopTest();
        System.assertNotEquals(preOwnerId, [SELECT Id, OwnerId FROM Account WHERE Id = :acc.Id].OwnerId);
         TaskCreationController.reassignCurrentUser(acc_New.Id, 'Other', 'Don\'t want this company', '');
       /* TaskCreationController.TaskCreationData t = (TaskCreationController.TaskCreationData)JSON.deserialize(TaskCreationController.fetchInitData(acc.Id),
                                                    TaskCreationController.TaskCreationData.Class);*/
        try {
           TaskCreationController.reassignCurrentUser('acc.Id', 'Other', 'Don\'t want this company', acc.OwnerId);
        } catch (Exception e) {
        }  
     }
    @isTest
    public static void testCatchMethod(){
        Test.startTest();
         Account objAccount = [SELECT Id,Name FROM Account WHERE Name='TestAcct' LIMIT 1];
        // TaskCreationController.TaskDataToExtract obj = new TaskCreationController.TaskDataToExtract();
        // obj.accountPriority = 'Test';
        // obj.clearAccountNextAction = true;
        // obj.clearAccountNextActionDate = true;

        String todayString = String.valueOf(Date.today());
        TaskCreationController.TaskDataToExtract taskData = new TaskCreationController.TaskDataToExtract();
        taskData.taskType = 'Call';
        taskData.taskSubject = 'test';
        taskData.taskComments = 'taskComments';
        taskData.taskActivityDate = todayString;
        taskData.taskNextAction = '';   
        taskData.taskNextActionDate = ''; 
        taskData.accountPriority = 'accountPriority';
        taskData.clearAccountNextAction = true;
        taskData.clearAccountNextActionDate = true;

        TaskCreationController.createNewTask(objAccount.Id,JSON.serialize(taskData), new Task_Comment__c());

        Test.stopTest();
    }
    @isTest
    public static void testCreateUser(){
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
       
        UserRole ur = new UserRole(Name = 'CEO');
        insert ur;
       
        Test.startTest();
        User u = new User(
            ProfileId = pf.Id,
            LastName = 'testCP2',
            Email = 'testCP2@gmail.com',
            Username = 'testCP02@gmail.com' ,
            CompanyName = 'RT',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = ur.Id
       );
       insert u;
       List<Account> accountLst = new List<Account>();
       System.runAs(u){
       Account a = new Account(Name = 'Test1',OwnerId=u.Id,Partial_Credit_User__c = u.Id,
                                Ownership_Status__c = 'Watch',
                                Allow_Ownership_After_Demote_Upto__c = datetime.newInstance(2020, 02, 25,13,18,0),
                                After_Demote_Transfer_Upto_For_All__c = datetime.newInstance(2020, 02, 25,13,18,0),
                                After_Demote_Transfer_Upto_For_Partial__c = datetime.newInstance(2020, 02, 25,13,18,0) 
                             );
         insert a;
        Account acc = new Account(Name = 'Test2',OwnerId=u.Id,Partial_Credit_User__c = u.Id,
                                Ownership_Status__c = 'Priority',
                                Allow_Ownership_After_Demote_Upto__c = datetime.newInstance(2020, 02, 25,13,18,0),
                                After_Demote_Transfer_Upto_For_All__c = datetime.newInstance(2020, 03, 25,13,18,0),
                                After_Demote_Transfer_Upto_For_Partial__c = datetime.newInstance(2020, 03, 25,13,18,0) 
                             );
         insert acc;
        accountLst.add(a);
        accountLst.add(acc);
        
       try{
        TaskCreationController.fetchInitData(a.Id);
        TaskCreationController.requestForOwnership(accountLst[0].Id, 'Can you please assign this company to me');
    //    TaskCreationController.requestForOwnership(a.Id, 'Can you please assign this company to me');
       }catch(Exception e){}
      }
        Test.stopTest();
    }

    // @isTest
    // public static void testAcceptRequestNEW(){
    //     IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
    //     testdata.populateIVPGeneralConfig();
    //     List<Account> acclst = testData.accounts;
    //     Account acc = new Account(Name='Test Account 1', Ownership_Status__c='Watch',Partial_Credit_User__c = UserInfo.getUserId(),After_Demote_Transfer_Upto_For_Partial__c = datetime.newInstance(2020, 03, 25,13,18,0),After_Demote_Transfer_Upto_For_All__c = datetime.newInstance(2020, 03, 25,13,18,0));
    //     insert acc;
    //     acclst.add(acc);

    //     // insert acclst;
    //     // List<Account> lstAcc = new List<Account>();
    //     // lstAcc.add(new Account(Name='Test Account 1', Ownership_Status__c='Watch',Partial_Credit_User__c = UserInfo.getUserId(),After_Demote_Transfer_Upto_For_Partial__c = datetime.newInstance(2020, 03, 25,13,18,0),After_Demote_Transfer_Upto_For_All__c = datetime.newInstance(2020, 03, 25,13,18,0)));
    //     // lstAcc.add(new Account(Name='Test Account 2', Ownership_Status__c='Priority',Partial_Credit_User__c = UserInfo.getUserId(),After_Demote_Transfer_Upto_For_Partial__c = datetime.newInstance(2020, 03, 25,13,18,0),After_Demote_Transfer_Upto_For_All__c = datetime.newInstance(2020, 03, 25,13,18,0)));
    //     // insert lstAcc;

    //     Test.startTest(); 
    //         // TaskCreationController.requestForOwnership(acclst[2].Id, 'Can you please assign this company to me');
    //     try{
    //         TaskCreationController.requestForOwnership(acclst[2].Id, 'Can you please assign this company to me');
    //     }catch(Exception e){}
    //     Test.stopTest();
    // }
}