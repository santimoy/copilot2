@isTest
global class IVPTaggingMock implements HttpCalloutMock{
   global HttpResponse respond(HttpRequest request){
       HttpResponse response = new HttpResponse();
       String b = '<?xml version="1.0" encoding="UTF-8"?>';
		b += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
		b += '<soapenv:Header>';
		b += '<ns1:SessionHeader soapenv:mustUnderstand="0" xmlns:ns1="http://soap.sforce.com/2006/04/metadata">';
		b += '<ns1:sessionId>' + IVPUtils.getUserSessionId() + '</ns1:sessionId>';
		b += '</ns1:SessionHeader>';
		b += '</soapenv:Header>';
		b += '<soapenv:Body>';
		b += '<create xmlns="http://soap.sforce.com/2006/04/metadata">';
		b += '<metadata xsi:type="ns2:ListView" xmlns:ns2="http://soap.sforce.com/2006/04/metadata">';
		//This is the API name of the list view
		b += '<fullName>Account.DharmendraViewAPI</fullName>';
		b += '<booleanFilter>1</booleanFilter>';
		//Columns you want to display
		b += '<columns>NAME</columns>';
		//b += '<columns>Tags__c</columns>';
		//Filterscope should be set to Everything for every one to be able to access this List view
		b += '<filterScope>Everything</filterScope>';
		// Enter the filter that you want to set
		b += '<filters>';
		b += '<field>Name</field>';
		b += '<operation>equals</operation>';
		b += '<value>Test Dharmendra </value>';
		b += '</filters>';
		b += '<label>Dharmendra View API</label>';
		b += '<sharedTo>null</sharedTo>';
		
		b += '</metadata>';
		b += '</create>';
		b += '</soapenv:Body>';
		b += '</soapenv:Envelope>';
		
       
       	response.setBody(b);
       
       response.setStatusCode(200);
       return response;
   }
}