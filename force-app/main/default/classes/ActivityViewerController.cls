global  class ActivityViewerController {
	public static final String PARAM_ACCOUNTID = 'id';
	public static final String PARAM_RETURL = 'retURL';
	public Static String UNASSIGNED='Unassigned';

	private String storeCurrId;
	public String currentActivityId{
      get { return storeCurrId; }
      set { 
        System.debug('Current Activity Id:'+value);
      	storeCurrId = value; }
   }

/* Investment Opportunity ID Prefix */
    public String IOPrefix{
    	get;
    	set;
    }
	private String accountId;
	private List<ActivityWrapper> accountActivities;
	 //public Getter to list accountActivities
	 public List<ActivityWrapper> getAccountActivities() { 
	  return this.accountActivities; 
	 } 


	private  Map<String, List<Integer>> colorDict = new Map<String, List<Integer>>();

	public String UserEmail{
		get {
			return UserInfo.getUserEmail();
		}
	}

/*
	public class KeyboardShortcut{
		public String Name {get;set;}
		public String Keys {get;set;}
	}
*/
	public class ActivityWrapper {
		public Task history {get;set;}
		public String Custom_Color_Border {get;set;}
		public String Custom_Color {get;set;}
		public String Long_Date {get;set;}
		public String Clean_Body {get;set;}
		public String Display_Icon {get;set;}
		public String Display_Icon2 {get;set;}
		public String Source {get;set;}
		public Date ActivityDate {get;set;}
		public String LogDate {get;set;}
		public String HideLogDate {get;set;}
		public String Type {get;set;}
		public String Subject {get;set;}
		public String LastName  {get;set;}
		public String FirstName  {get;set;}
		public String AccountId  {get;set;}
		public String OwnerId  {get;set;}		
		public String Id  {get;set;}	
		public String fileLink {get;set;}		
		public String fileName {get;set;}	
		public String iconName {get;set;}
		public String maxWidthStyle {get;set;}
	}




		private final Account currAccount;

		
	public Account getCurrAccount(){
		return currAccount;
	}

	public Contact KeyContact {get;set;}
	public Boolean HasKeyContact {get;set;}
	public String Domain {get;set;}
	public String OwnerName {get;set;}
	public String PreviousOwner {get;set;}
	public String AllEmails {get;set;}
	public String FirstEmail {get;set;}

	private Map<String,Id> attachmentIds;
	/******** CONSTRUCTOR *************/
	/******** CONSTRUCTOR *************/
	public ActivityViewerController(ApexPages.StandardController controller) {

		 List<String> fields = new List<String>();
        fields.add('Name');
        fields.add('Website__c');
        fields.add('Expire_Date__c');
        fields.add('Previous_Owner__c');
        fields.add('OwnerId');
        fields.add('Previous_Owner_Lookup__c');
        fields.add('Next_Action_Date__c');
        fields.add('Next_Action__c');
    
        if(!Test.isRunningTest())controller.addFields(fields);
       currAccount = (Account)controller.getRecord();
		map<String, String> params = ApexPages.currentPage().getParameters(); 
		accountId = params.get(PARAM_ACCOUNTID);
        accountActivities= new List<ActivityWrapper>();
        /* get attachments to link to via file name (really hacky) */
        attachmentIds = new Map<String, Id>();


 		List<Contact> contacts=[select FirstName,LastName,Email,Title from Contact where AccountId=:accountId and Key_Contact__c=true order by CreatedDate desc limit 1];
 		if(contacts.size()>0) {
			KeyContact=contacts.get(0);
			HasKeyContact=true;
		}
		else HasKeyContact=false;

		  	if(!String.isBlank(currAccount.Website__c)){
		        List<String> domainParts=currAccount.Website__c.split('\\.');
		        if(domainParts.size()>=2)
		        	Domain=domainParts.get(domainParts.size()-2)+'.'+domainParts.get(domainParts.size()-1);
			} 

	        //System.URL fullURL=new Url(currAccount.Website__c);

	        //Domain =fullURL.getHost();
	        //Domain=Domain.replace('www.','');

	}

	Keyboard_Shortcuts__c finalCut;
	List<Keyboard_Shortcuts__c> defaultCuts;
	public void InitializeViewer(){

		String oId=currAccount.OwnerId;
		PreviousOwner='';
		String previousId;

		if(currAccount.Previous_Owner_Lookup__c!=null) 
			previousId=currAccount.Previous_Owner_Lookup__c;
		else 	if(currAccount.Previous_Owner__c!=null) 
					previousId=currAccount.Previous_Owner__c;

			

		
		if(previousId!=null) {
			List<User> oldUser=[select id,Name from User where id=:previousId];
			if(oldUser.size()>0){
				PreviousOwner=oldUser.get(0).Name;

			}
		}
		List<User> accUser=[select id,Name from User where id=:oId];
		if(accUser.size()>0){
			OwnerName=accUser.get(0).Name;

		}
		List<Investment_Opportunity__c> invOpps= [select Id from Investment_Opportunity__c where Company__c=:accountId];
		List<Id> oppIds= new List<Id>();
		for(Investment_Opportunity__c o:invOpps) oppIds.add(o.Id);
        List<Attachment> accFiles=[select id,name,OwnerId,CreatedDate,Owner.FirstName,Owner.LastName,ParentId from Attachment where ParentId =:accountId or ParentId in :oppIds];

        /*
		DateTime currDt = System.now();
		DateTime recently = currDt.addSeconds(-30);

        List<Task> recentTasks =[select id,CreatedDate,Subject,Description from Task where CreatedDate>:recently];
        if(recentTasks.size()>0){
        	for(Task t: recentTasks){
        		System.debug('validateTemplate:'+t.CreatedDate);
        		DateTime upperLimit=t.CreatedDate.addSeconds(2);
                 List<EmailTemplate> tmpls=[select id, Name, LastusedDate from EmailTemplate where LastusedDate >=:t.CreatedDate and LastusedDate<=:upperLimit];
                for(EmailTemplate tmpl:tmpls){
                    if(tmpl!=null){
                         System.debug('EmailTemplate:'+tmpl.Name+'-'+tmpl.LastusedDate);
                    }
                }
        	}

        }
		*/

		Datetime currentDT=System.Now();
		DateTime checkTime=currentDT.addSeconds(-5);
		Id userId=UserInfo.getUserId();
		List<Task> tasks=[select Id,Subject,AccountId,Description,Type__c,Source__c,CreatedDate from Task where OwnerId=:userId and CreatedDate>:checkTime and AccountId=:accountId order by CreatedDate DESC ];
		if(tasks.size()>0)
			cleanTask(tasks.get(0));
		
		List<Task> listTasks=[Select Id,AccountId,Subject,Description,CreatedByID,CreatedDate,LastModifiedDate,LastModifiedById,ActivityDate,Type__c,Owner.FirstName,Owner.LastName,WhatId,Source__c,CreatedBy.LastName,LastModifiedBy.LastName
		 FROM Task where AccountId=:accountId  and IsClosed=true and IsDeleted=false ORDER BY ActivityDate DESC, LastModifiedDate DESC LIMIT 499 ALL ROWS];
		


		if(listTasks.size()>0){
			fillColorDict();

            Schema.DescribeSObjectResult r = Investment_Opportunity__c.sObjectType.getDescribe();
		IOPrefix= r.getKeyPrefix();
		Set<Id> alreadyAdded= new Set<Id>();
		System.debug('IOPrefix:'+IOPrefix);
			
			for(Task hist: listTasks) {
				ActivityWrapper wrap =createWrapper(hist);
				// System.debug('SubString:'+wrap.Display_Icon);
				for(Attachment a:accFiles){
					Date attDate = Date.newInstance(a.CreatedDate.year(), a.CreatedDate.month(),a.CreatedDate.day());
					if(wrap.history.ActivityDate<=attDate){
						if(alreadyAdded.contains(a.Id)) continue;
						else alreadyAdded.add(a.Id);
					
						ActivityWrapper w2=createAttachmentWrapper(a,attDate);
						accountActivities.add(w2);
					}
				
				}
				accountActivities.add(wrap);
			}
			
		}
		/*
		List<Account> listAccounts=[SELECT Name,(Select Id,AccountId,Subject,Description,CreatedByID,CreatedDate,LastModifiedDate,LastModifiedById,ActivityDate,Type__c,Owner.FirstName,Owner.LastName,WhatId,Source__c
		 FROM Tasks where IsClosed=true ORDER BY ActivityDate DESC, LastModifiedDate DESC LIMIT 499) from Account  WHERE Id=:accountId];
		


		if(listAccounts.size()>0){
			fillColorDict();

            Schema.DescribeSObjectResult r = Investment_Opportunity__c.sObjectType.getDescribe();
		IOPrefix= r.getKeyPrefix();
		Set<Id> alreadyAdded= new Set<Id>();
		System.debug('IOPrefix:'+IOPrefix);
			for(Account acc: listAccounts){
				for(Task hist: acc.Tasks) {
					ActivityWrapper wrap =createWrapper(hist);
					// System.debug('SubString:'+wrap.Display_Icon);
					for(Attachment a:accFiles){
						Date attDate = Date.newInstance(a.CreatedDate.year(), a.CreatedDate.month(),a.CreatedDate.day());
						if(wrap.history.ActivityDate<=attDate){
							if(alreadyAdded.contains(a.Id)) continue;
							else alreadyAdded.add(a.Id);
						
							ActivityWrapper w2=createAttachmentWrapper(a,attDate);
							accountActivities.add(w2);
						}
					
					}
					accountActivities.add(wrap);
				}
			}
		}

	*/
			
		//Init ShortCut Helper

		Id orgId=UserInfo.getOrganizationId();

		
			defaultCuts=[select id, name, Goto_First_Company__c,Company_Search_Result__c,Search_Term__c,Goto_Term_Result_List__c,Create_New_Company__c,Goto_Company_Website__c,Instantly_Log_an_activity__c,SetupOwnerId,Alexa__c,Send_email_using_template__c  ,Send_email_to_key_contact__c,Toggle_Ownership__c,Add_contact__c,Google_News__c,Edit_company_page__c,Logging_action__c,SF_Search_Bar__c,Notify_Company__c from Keyboard_Shortcuts__c where SetupOwnerId=:orgId];
     	List<Keyboard_Shortcuts__c> userCuts=[select id, name, Goto_First_Company__c,Company_Search_Result__c,Search_Term__c,Goto_Term_Result_List__c,Create_New_Company__c,Goto_Company_Website__c,Instantly_Log_an_activity__c,SetupOwnerId,Alexa__c,Send_email_using_template__c  ,Send_email_to_key_contact__c,Toggle_Ownership__c,Add_contact__c,Google_News__c,Edit_company_page__c,Logging_action__c,SF_Search_Bar__c,Notify_Company__c from Keyboard_Shortcuts__c where SetupOwnerId=:userId];
   	    if(Test.isRunningTest()) finalCut = new Keyboard_Shortcuts__c(); 
   	    	else
   		finalCut=userCuts.size()==0?defaultCuts.get(0):userCuts.get(0);
   		
   		//fillHelper(finalCut);
 		


 		// Blast all email
 		setAllEmails();
 	


	}


	public string mainURL{
		get {
			return 'https://'+ApexPages.currentPage().getHeaders().get('Host');
		}
	}

	public String sendEmailKeyContact{
		get {
			String ret='';
			if(finalCut.Send_email_to_key_contact__c== null)
				 ret=defaultCuts.get(0).Send_email_to_key_contact__c;
			else
				ret=finalCut.Send_email_to_key_contact__c;
			return ret;
		}
	}
	public String notifyCompany{
		get {
			String ret='';
			if(finalCut.Notify_Company__c== null)
				 ret=defaultCuts.get(0).Notify_Company__c;
			else
				ret=finalCut.Notify_Company__c;
			return ret;
		}
	}

	public void setAllEmails(){
		AllEmails='';
			if(KeyContact!=null) {
 			String sfirst='';
 			if(KeyContact.FirstName!=null)
 				sfirst=KeyContact.FirstName.replace('"','');

 			if(KeyContact.FirstName==null) return;
 			String slast=KeyContact.LastName.replace('"','');
 			List<String> slNames=slast.split(' ');
 			if(slNames.size()>1){
 				if(sfirst.length()==0){
 					sfirst=slNames.get(0);
 				}
 				if(slNames.size()==2){
 					sLast=slNames.get(1);
 				}
 				if(slNames.size()==3){
 					sLast=slNames.get(2);
 				}

 			}
	 		EmailFinder finder= new EmailFinder(Domain,slast,sfirst);
			List<String> emails=finder.getAllCombinations();
			
			FirstEmail='';
			if(emails.size()>0) FirstEmail=emails.get(0);
			Boolean first=true;
			for(String s:emails){
				if(first) {
					first=false;
					continue;
				}
				AllEmails+=s+';';
			}
		}
	}
public ActivityWrapper createAttachmentWrapper(Attachment a,Date attDate){
						ActivityWrapper w2= new ActivityWrapper();
							//ActivityHistory h= new ActivityHistory();
							//h.Type__c='File Upload';
							w2.Subject=a.Name;
							w2.OwnerId=a.OwnerId;
							w2.FirstName=a.Owner.FirstName;
							w2.LastName=a.Owner.LastName;
							w2.ActivityDate=attDate;
							w2.AccountId=a.ParentId;
							w2.Id=a.Id;
							w2.Type='Attachment';
							w2.Clean_Body= 'Download File:';
							w2.fileName=a.Name;
							w2.Custom_Color_Border=getActivityColorString(w2.ActivityDate,w2.Type, Double.valueOf(1.3));
							w2.Custom_Color=getActivityColorString(w2.ActivityDate,w2.Type, Double.valueOf(1));
							w2.maxWidthStyle='max-width:250px;';
				   			Datetime attdt=a.CreatedDate;
							w2.Long_Date=attdt.format('EEEE')+', '+attdt.format('MMMMM')+' '+attdt.format('dd')+', '+attdt.format('yyyy');

							w2.Display_Icon='';
							w2.iconName='files16.png';
							w2.Display_Icon2='display:none;';
							String pId=a.ParentId;
							if(pId.substring(0,3)==IOPrefix){
								w2.Display_Icon2='';
							}
							
							w2.fileLink=a.Id;
							return w2;
}
	public ActivityWrapper createWrapper(Task hist){
	ActivityWrapper wrap = new ActivityWrapper();
					wrap.history=hist;
					wrap.FirstName=hist.Owner.FirstName;
					wrap.LastName=hist.Owner.LastName;
					wrap.Type=hist.Type__c;
					Date sDate;
					if(hist.ActivityDate==null){
						sDate=Date.newInstance(hist.CreatedDate.year(), hist.CreatedDate.month(),hist.CreatedDate.day());		}
					else sDate=hist.ActivityDate;
					wrap.ActivityDate=sDate;
					wrap.AccountId=hist.Accountid;
					wrap.Id=hist.Id;
					wrap.Source=hist.Source__c;
					wrap.Subject=hist.Subject;
					wrap.OwnerId=hist.OwnerId;
					wrap.maxWidthStyle='';
					wrap.fileLink='';
					wrap.fileName='';
					wrap.Custom_Color_Border=getActivityColorString(sDate,hist.Type__c, Double.valueOf(1.3));
					wrap.Custom_Color=getActivityColorString(sDate,hist.Type__c, Double.valueOf(1));
					//wrap.Clean_Body=  hist.Description.replaceAll('<','').replaceAll('>','').replaceAll('\n', '<br/>');
					if(hist.Description!=null){

						
						Pattern brackets = Pattern.compile(
				      '(<[a-z0-9_\\.-]+\\@[\\da-z\\.-]+\\.[a-z\\.]{2,6}>)'//literal right bracket
				    	);
	    				Matcher matcher = brackets.matcher(hist.Description);
	   					String repl='';
	   					system.debug('find email:');
	   					if(matcher.find())
	   						    repl=matcher.group(0);

						
	   					if(repl.length()>0) system.debug(repl)	;		    
						wrap.Clean_Body=  hist.Description.replaceAll(repl,repl.replaceAll('<','<"').replaceAll('>','">')).replaceAll('\n', '<br/>');
					}
					else wrap.Clean_Body='';
					// Tuesday, June 09, 2015
				    Date actDate=sDate;
				   Datetime dt = datetime.newInstance(actDate.year(), actDate.month(),actDate.day());
					wrap.Long_Date=dt.format('EEEE')+', '+dt.format('MMMMM')+' '+dt.format('dd')+', '+dt.format('yyyy');
					/*

					1.  Can we have there be a bit more space between the  calendar icon on the "edit" button?  
					2.  Change terminolgy to last modified XXXX
					4.  If Thomas Lesnick is the last modifier but if it was created on a different date than the activitydate show "last modified on {createddate}".

					*/
					//   DatetDatetime dtMod =hist.LastModifiedDate;

					Datetime dtMod =hist.LastModifiedDate;
					Boolean isDiffDate=false;
					wrap.LogDate='Last modified '+dtMod.format('EEEE')+', '+dtMod.format('MMMMM')+' '+dtMod.format('dd')+', '+dtMod.format('yyyy');
					
					Date chkModDate = date.newinstance(dtMod.year(), dtMod.month(), dtMod.day());
					if(dtMod.date()==actDate || (hist.LastModifiedBy.LastName=='Lesnick' || hist.LastModifiedBy.LastName=='Wing')){
						wrap.HideLogDate='display:none;';

					}
					else isDiffDate=true;
				//	if(dtMod.addhours(-5).dateGMT()==actDate || (hist.LastModifiedBy.LastName=='Lesnick' || hist.LastModifiedBy.LastName=='Wing'))
						system.debug(chkModDate+'-'+actDate);
					Date chkCreatedDate = date.newinstance(hist.CreatedDate.year(), hist.CreatedDate.month(), hist.CreatedDate.day());	
					if(!isDiffDate && hist.CreatedDate.date()!=actDate && (hist.LastModifiedBy.LastName=='Lesnick' || hist.LastModifiedBy.LastName=='Wing')) {
					//if(hist.CreatedDate.addhours(-5).dateGMT()!=actDate && (hist.LastModifiedBy.LastName=='Lesnick' || hist.LastModifiedBy.LastName=='Wing')) {
						wrap.HideLogDate='';
						wrap.LogDate='Last modified  '+hist.CreatedDate.format('EEEE')+', '+hist.CreatedDate.format('MMMMM')+' '+hist.CreatedDate.format('dd')+', '+hist.CreatedDate.format('yyyy');
					
					}

						



					wrap.Display_Icon='';
					wrap.Display_Icon2='display:none;';
					if(hist.WhatId!=null){
						String whatId=hist.WhatId;
						//System.debug('SubString:'+whatId.substring(0,3));
						if(whatId.substring(0,3)==IOPrefix){
							wrap.Display_Icon='';
							wrap.iconName='cash16.png';

						}
						else {
							wrap.Display_Icon='display:none;';

							wrap.iconName='account.png';

						}
					}	
					return wrap;
	}
	public void fillColorDict(){
		colorDict.put('Call', new List<Integer> {0,232,23});
			colorDict.put('Meeting', new List<Integer> {0,193,245});
			colorDict.put('Meet', new List<Integer> {0,193,245});
			colorDict.put('Voicemail', new List<Integer> {0,3,255});
			colorDict.put('Vmail', new List<Integer> {0,3,255});
			colorDict.put('Email', new List<Integer> {250,50,200});
			colorDict.put('Unassign', new List<Integer> {245,51,0});
			colorDict.put('Pass', new List<Integer> {245,158,0});
			colorDict.put('Other', new List<Integer> {245,241,9});
			colorDict.put('Examine', new List<Integer> {245,241,9});
			colorDict.put('TermSheetProposed', new List<Integer> {0,193,245});
			colorDict.put('TermSheetSigned', new List<Integer> {0,193,245});
			colorDict.put('Article', new List<Integer> {71,237,198});
			colorDict.put('Attachment', new List<Integer> {71,237,198});
	}

	public String cn(string str){
		return str.replace('__c','').replace('_',' ');
	}
/*
	public void fillHelper(Keyboard_Shortcuts__c shortCuts){
			ShortCutHelper = new List<KeyboardShortcut>();
			KeyboardShortcut k = new KeyboardShortcut();
			k.Name=cn('Goto_First_Company__c');
			k.Keys=shortCuts.Goto_First_Company__c;
			ShortCutHelper.add(k);
			k = new KeyboardShortcut();
			k.Name=cn('Company_Search_Result__c');
			k.Keys=shortCuts.Company_Search_Result__c;
			ShortCutHelper.add(k);
			k = new KeyboardShortcut();
			k.Name=cn('Search_Term__c');
			k.Keys=shortCuts.Search_Term__c;
			ShortCutHelper.add(k);
			k = new KeyboardShortcut();
			k.Name=cn('Goto_Term_Result_List__c');
			k.Keys=shortCuts.Goto_Term_Result_List__c;
			ShortCutHelper.add(k);
			k = new KeyboardShortcut();
			k.Name=cn('Create_New_Company__c');
			k.Keys=shortCuts.Create_New_Company__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('Goto_Company_Website__c');
			k.Keys=shortCuts.Goto_Company_Website__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('Instantly_Log_an_activity__c');
			k.Keys=shortCuts.Instantly_Log_an_activity__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('Alexa__c');
			k.Keys=shortCuts.Alexa__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('Send_email_using_template__c');
			k.Keys=shortCuts.Send_email_using_template__c;
			ShortCutHelper.add(k);


			k = new KeyboardShortcut();
			k.Name=cn('Send_email_to_key_contact__c');
			k.Keys=shortCuts.Send_email_to_key_contact__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('Toggle_Ownership__c');
			k.Keys=shortCuts.Toggle_Ownership__c;
			ShortCutHelper.add(k);


			k = new KeyboardShortcut();
			k.Name=cn('Add_contact__c');
			k.Keys=shortCuts.Add_contact__c;
			ShortCutHelper.add(k);


			k = new KeyboardShortcut();
			k.Name=cn('Google_News__c');
			k.Keys=shortCuts.Google_News__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('Edit_company_page__c');
			k.Keys=shortCuts.Edit_company_page__c;
			ShortCutHelper.add(k);
			k = new KeyboardShortcut();
			k.Name=cn('Logging_action__c');
			k.Keys=shortCuts.Logging_action__c;
			ShortCutHelper.add(k);

			k = new KeyboardShortcut();
			k.Name=cn('SF_Search_Bar__c');
			k.Keys=shortCuts.SF_Search_Bar__c;
			ShortCutHelper.add(k);



	}
	public List<KeyboardShortcut> ShortCutHelper {get;set;}

	*/
	public void cleanTask(Task t){
		 
       
                
               System.debug('validateTemplate:'+t.CreatedDate);
               DateTime upperLimit=t.CreatedDate.addSeconds(2);
               EmailTemplate template;
               List<EmailTemplate> tmpls=[select id, Name, LastusedDate from EmailTemplate where LastusedDate >=:t.CreatedDate and LastusedDate<=:upperLimit];
               
               if(tmpls.size()>0){
                    template=tmpls.get(0);
               } 

               if(t.Subject.startsWith('Email')){
               	  	Integer index=t.Description.indexOf('Body:');
               	  	if(index<0) return;
	                t.Type__c='Email';
	                t.Source__c='Outbound';
	                t.Subject=t.Subject.replace('Email:','');
	                t.Description=t.Description.replace('Additional To:','To:');
	                t.Description=t.Description.replace('BCC: \n','');
	                t.Description=t.Description.replace('CC: \n','');
	               
	                t.Description=t.Description.replace('Attachment: \n','');
	                index=t.Description.indexOf('Body:');
	                Integer startBCC= t.Description.indexOf('BCC:');
	                if(startBCC>-1 && index>-1){
	                	String bccLine=t.Description.substring(startBCC,index-1);
	                	system.debug('bccLine:'+bccLine);
	                	Integer numAddresses=bccLine.split(';').size();
	                	if(numAddresses==0) numAddresses=bccLine.split(',').size();
	                	if(numAddresses>5){
	                		t.Description=t.Description.replace(bccLine,'BCC: Email blast all combination\n');	
	                	}
	                }
	                index=t.Description.indexOf('Body:');
	                String body='';
	                if(index>-1) body=t.Description.substring(index,t.Description.length());

	                Integer sIndex=t.Description.indexOf('Subject:');

	                if(sIndex>-1){ 
	                    t.Description=t.Description.left(sIndex-1);
	                    t.Description=t.Description+'\n'+body;
	                }
	                index=t.Description.indexOf('Body:');
	               
	                if(template!=null){
	                    
	                    if(index>-1){
	                        t.Description=t.Description.left(index-1);
	                    }
	                    t.Description=t.Description+'\nTemplate:'+template.Name;
	                }
	                
	          
	            	update t;
	            	
            	}
            	
	}

	
	 

	      
 
	@RemoteAction
	global static List<EmailFinder.EmailDefinition> findEmails(String domain, String first, String last,String emailGroup) {		
		//String retVal='';
		 
		EmailFinder finder= new EmailFinder(domain,last,first);
		// Search in batches of 5
		finder.findEmailInit();
		finder.getEmails(emailGroup);
		system.debug('startBatches');
		for(Integer i=0;i<finder.EmailSelection.size();i++){
			finder.CurrentBatch.add(finder.EmailSelection.get(i));
			if(math.mod(i+1, 2)==0 || i+1==finder.EmailSelection.size()){
				System.debug('Batch'+ String.valueOf((i+1)));
				finder.findEmail();
				finder.CurrentBatch.clear();
			}

		}
		system.debug('endBatches');

		 Integer okCount=0;
	    for(EmailFinder.EmailDefinition defn:finder.FinalList){
	    	if(defn.CheckerResults.status=='Ok') okCount++;
	    }

	    if(finder.FinalList.size()==okCount && okCount>1) {

	   	 System.debug('trydummyEmail');
	    	if(finder.tryDummyEmail())	{
	    		finder.FinalList.get(0).CheckerResults.additionalStatus='ServerIsCatchAll';
	    		//finder.FinalList.get(0).CheckerResults.status='artificial';
	    	}
	    	 System.debug(' end trydummyEmail');

	    }

	     System.debug('startMatches');
		List<Matching_Email_Template__c> matches = new List<Matching_Email_Template__c>();
		for(EmailFinder.EmailDefinition defn:finder.FinalList) {
			EmailFinder.EmailJson res= defn.CheckerResults;
			if( res.additionalStatus!='ServerIsCatchAll' && res.status=='Ok'){
             	Matching_Email_Template__c m = new Matching_Email_Template__c();
             	m.Name=defn.EmailTemplate;
             	m.Actual_Email_Address__c=defn.EmailAddress;
             	matches.add(m);
		    }
	    }
	    insert matches;
	 
	 System.debug('endMatches');
		return finder.FinalList;

	}

	@RemoteAction
	global static List<String> emailBlastList(String domain, String first, String last) {		
		//String retVal='';
		 
		EmailFinder finder= new EmailFinder(domain,last,first);
		List<String> emails=finder.getAllCombinations();
		
		//Include current users
		String currEmail =UserInfo.getUserEmail();
		emails.add(currEmail);
		return emails;

	}
	@RemoteAction 
	 global static String createKeyContact(String emailAddress, String firstName,  String lastName, String accountId){
		List<Contact> allConts= [select id,Email from Contact where AccountId=:accountId and Key_Contact__c=true];

	

		Contact newContact			= new Contact();
		newContact.FirstName		= firstName;
		newContact.LastName			= lastName;
		newContact.Key_Contact__c 	= true;
		newContact.AccountId 		= accountId;
		newContact.Email 			= emailAddress;

		insert newContact;


		if(allConts.size()>0){
			Contact cont=allConts[0];
			cont.Key_Contact__c=false;
			update cont;
		}

		return  newContact.Id;

	}


	@RemoteAction 
	 global static String updateKeyContact(String emailAddress, Id contactId){
		Contact cont= [select id,Email from Contact where Id=:contactId];
		if(cont!=null){
			cont.Email=emailaddress;
			update cont;
		}
		return 'success';

	}



	
	@RemoteAction
	global static List<String> toggleOwner(Id id) {		
		//String retVal='';
		Id userId=UserInfo.getUserId();
		Account acct=[select Id,OwnerId,Owner.Name,Name from Account where Id=:id];
		List<String> retVal= new List<String>();
		if(acct.Owner.Name==UNASSIGNED) {
			 acct.OwnerId=userId;
			 try {
				 update acct;
				 retVal.add('The owner of '+acct.Name+' has been changed to '+UserInfo.getName());
				 retVal.add(UserInfo.getName());
				 retVal.add('changed');
				} 
			catch(DmlException ex) {
				retVal.add('Sorry you cannot transfer ownership of this company.'+ex.getMessage());
				retVal.add('');
				retVal.add('cannot');
			}
		} 
		else {
			if(acct.OwnerId!=userId) {
				retVal.add('Sorry you cannot transfer ownership of this company.');
				retVal.add('');
				retVal.add('cannot');
			} else {

				retVal.add(acct.Owner.Name);
				retVal.add(acct.Owner.Name);
				retVal.add('unassign');
			}
		}

		return retVal;

	}

	@RemoteAction
	public static void sendErrorEmail(String subject, String body) {

     Messaging.SingleEmailMessage message = 
      new Messaging.SingleEmailMessage();
      
  
      message.setReplyTo('troy.wing@saasbydesign.com');
      message.setSaveAsActivity(false);
      message.setSubject(UserInfo.getName()+':'+subject);
      message.setPlainTextBody('User '+UserInfo.getName()+' generated this error:\n'+body);
      List<String> toAddresses= new List<String>();
      toAddresses.add('TLesnick@insightpartners.com');
      toAddresses.add('troy.wing@saasbydesign.com');
      message.setToAddresses(toAddresses);
      List<Messaging.SingleEmailMessage> messages= new List<Messaging.SingleEmailMessage>();
      messages.add(message);
      Messaging.sendEmail(messages);

}
	private String  getActivityColorString( Date actdate,String type,Double multiplier)
        {
            String ret = '';
            if(colorDict.containsKey(type))
            {
                Date d = actdate;
               
    
                Double numDays=d.daysBetween(Date.today());
                
                //cl("Days between: "+db);
                //cl(d);
                //cl(dateToday);
        		
                //var opacityScale = (Math.max(0,(90-db))/90.0)*0.3+0.2;
                Double tmp =2.71;
				//-1.0/90.0*numDays-0.2       *.3+.15          
                Double opacityScale = Math.pow(tmp,Double.valueOf(-1.0/90.0*numDays-0.2) )*.3+.15;
                opacityScale *= multiplier;
      
                        
                List<Integer> colorArray = colorDict.get(type);
    
                ret = 'rgba('+String.valueOf(colorArray.get(0))+','+String.valueOf(colorArray.get(1))+','+String.valueOf(colorArray.get(2))+','+opacityScale+')';                   
            }
            else
            {
                ret = '';
            }
            //cl("Color is: "+ret);
            return ret;
        }

	
}