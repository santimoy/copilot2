@isTest
public class GetValidationDetails_Test {
    
    public static testmethod void Method()
    {
        string jsonToTest = '{"Last_Validated":"2019-05-09 05:55:38 UTC","execution_time":"461 ms","validation_status":"Valid","Recent_Validation_Error":""}';
        
        test.startTest();
        // GetValidationDetails validationDetails = new GetValidationDetails();
        GetValidationDetails validationDetails = (GetValidationDetails)JSON.deserialize(jsonToTest, GetValidationDetails.class);

        // validationDetails = GetValidationDetails.parse(jsonToTest);
        system.assertEquals('2019-05-09 05:55:38 UTC', validationDetails.Last_Validated);
        system.assertEquals('', validationDetails.Recent_Validation_Error);
        system.assertEquals('Valid', validationDetails.validation_status);
        test.stopTest();
    }
}