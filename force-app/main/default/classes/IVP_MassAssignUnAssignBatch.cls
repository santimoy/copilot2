public class IVP_MassAssignUnAssignBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    @testvisible private boolean            		isMassAssign;
    @testvisible private Map<Id,Map<String,String>> mapOfFailRecords;
    @testvisible private Map<Id,String> 			mapOfPassRecords;
    @testvisible private String           			query;
    @testvisible private Set<Id>		            acctIdsSet;
    @testvisible private Id             		    unassignUserId;
    @testvisible private String             		lossDropReason;
    @testvisible private String             		lossDropReasonNote;
    @testvisible private boolean             		isMailSend;
    
    
    
    public IVP_MassAssignUnAssignBatch(boolean isMassAssign,Set<Id> acctIdsSet,String lossDropReason, String lossDropReasonNote){
        this.isMassAssign       = isMassAssign;
        this.acctIdsSet         = acctIdsSet; 
        this.mapOfPassRecords   = new Map<Id,String>();
        this.lossDropReason     = lossDropReason;
        this.mapOfFailRecords   = new Map<Id,Map<String,String>>();
        this.isMailSend			= false;
        this.lossDropReasonNote = lossDropReasonNote;
        
        List<User> listUser     = [SELECT Id,Name from User where Name = 'Unassigned' Limit 1];
        if(!listUser.isEmpty()){
            unassignUserId= listUser[0].Id;
        }
        query='SELECT Id,Name,OwnerId,Loss_Drop_Reason__c from Account where Id in : acctIdsSet';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Account> scope) {
        for(Account a : scope){
            //It should automatically done when a loss drop reason is set
            if(isMassAssign){
                a.OwnerId = UserInfo.getUserId();
            }else{
                // we need check as TRUE as not specifying Owneris to execute process builders
                TaskCreationController.UNASSIGN_MANUAL = TRUE;
            }
            a.Loss_Drop_Reason__c   = isMassAssign ? a.Loss_Drop_Reason__c  : lossDropReason;
            a.Loss_Drop_Notes__c = lossDropReasonNote == '' ? a.Loss_Drop_Notes__c : lossDropReasonNote;
        }
        Map<Id,Account> mapOfScope = new Map<Id,Account>(scope);
        
        System.debug('Before IVP Utils');
        Map<String,Map<Id,String>> updateSummary = IVPUtils.updateRecords(scope);
        TaskCreationController.UNASSIGN_MANUAL = FALSE;
        System.debug('After IVP Utils::'+updateSummary);
        for(String summaryKey : updateSummary.keySet()){
            if(summaryKey.equalsIgnoreCase('successRecords')){
                Map<Id,String> tempSuccessRecordMap = updateSummary.get(summaryKey);
                for(Id objId : tempSuccessRecordMap.keySet()){
                    mapOfPassRecords.put(objId,mapOfScope.get(objId).Name);
                }
            }else if(summaryKey.equalsIgnoreCase('failRecords')){
                Map<Id,String> tempErrorRecordMap = updateSummary.get('failRecords');
                for(Id objId : tempErrorRecordMap.keySet()){
                    mapOfFailRecords.put(objId,new Map<String,String>{
                        mapOfScope.get(objId).Name => tempErrorRecordMap.get(objId)
                            });
                }
            }
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        sendMail();
    }
    
    public void sendMail(){
        String isAssignOrUnassign = isMassAssign ? 'assign': 'unassign';
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(new List<String> {UserInfo.getUserEmail()});
        String ccIds = Label.IVP_AssignUnAssign_Notification_Emails;
        if(ccIds!= null && !ccIds.trim().equalsIgnoreCase('null') && ccIds.length()!=0){
            email.setCcAddresses(ccIds.split(','));
        }
        //email.setbccAddresses(new List<String> {});
        email.setSubject( 'Mass '+isAssignOrUnassign+' update');
        
        String mailBody = 'Dear '+UserInfo.getName() +',';
        mailBody += '<br/><br/>Mass '+isAssignOrUnassign+' process is completed. Please find details below:';   
        
        if(!mapOfFailRecords.isEmpty()) {   
            mailBody += '<br/><br/>Following companies are not '+isAssignOrUnassign+'ed:<br/>';
            Integer cnt = 1 ;
            for(String accId : mapOfFailRecords.keySet()){
                for(String accountName : mapOfFailRecords.get(accId).keySet()){
                    System.debug('mapOfFailRecords.get(accId).get(accountName)::'+mapOfFailRecords.get(accId).get(accountName));
                    mailBody+= cnt++ +'.<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+accId+'">'+accountName+'</a> - '+ extractErrorMessage(mapOfFailRecords.get(accId).get(accountName)) +'<br/>';  
                }
            }
        }
        
        if(!mapOfPassRecords.isEmpty()){
            mailBody+= '<br/><br/>Following companies are '+isAssignOrUnassign+'ed:<br/>';
            Integer cnt = 1;
            for(String accId : mapOfPassRecords.keySet()){
                mailBody+= cnt++ +'.<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+accId+'">'+mapOfPassRecords.get(accId)+'</a> <br/>';
            }
        }       
        mailBody+='<br/>Thank You';
        
        email.setHtmlBody(mailBody);
        
        //Setting up org wide email address in Email
        Id orgWideEmailId= getSysAdminOrgWideEmailId();
        
        if(orgWideEmailId != null){
            email.setOrgWideEmailAddressId(orgWideEmailId);
        }
        
        if(!isMailSend){
            isMailSend=true;
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
    }
    /**
* Name getSysAdminOrgWideEmailId
* Description This method will get the 'Salesforce Admin' org wide email address Id!
*/
    private Id getSysAdminOrgWideEmailId(){
        List<OrgWideEmailAddress> orgWideEmails =  [SELECT Id
                                                    FROM OrgWideEmailAddress 
                                                    WHERE DisplayName = 'Salesforce Admin'];
        if(orgWideEmails.isEmpty()){
            return null;
        }else{
            return orgWideEmails[0].Id;
        }
    }
    /***********************************************************************
     * @description This extract the message of an exception.
     * @return String
     * @author 10k-Expert
     * @param String error message
     ***********************************************************************/
    public static String extractErrorMessage(String errMsg){
        if(errMsg!=null){
            String findString = '_EXCEPTION: ';
            Integer strIndex = errMsg.indexOf(findString);
            if(strIndex>=0){
                Integer findStringLen = findString.length();
                Integer endIndex = errMsg.indexOf('.',strIndex);
                errMsg = endIndex > -1 ? errMsg.substring(strIndex+findStringLen, endIndex) : errMsg;
            }
        }
        return errMsg;
    }
}