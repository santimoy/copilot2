/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Krow_Resource_UtilizationTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Krow_Resource_UtilizationTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Krow__Resource_Utilization__c());
    }
}