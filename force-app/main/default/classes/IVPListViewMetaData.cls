public class IVPListViewMetaData {
    static String dateRegex = '(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)';
    static Pattern dateTime_Pattern = Pattern.compile('(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d) \\d\\d:\\d\\d');
    static Pattern date_Pattern = Pattern.compile(dateRegex);
	public static Map<String, String> fieldWithName = new Map<String,String> {
		
		'name' => 'ACCOUNT.NAME',
		'ownership' => 'ACCOUNT.OWNERSHIP',
		'accountnumber' => 'ACCOUNT.ACCOUNT_NUMBER',
		'site' => 'ACCOUNT.SITE',
		'recordtypeid' => 'ACCOUNT.RECORDTYPE',
		'accountsource' => 'ACCOUNT_SOURCE',
		'annualrevenue' => 'ACCOUNT.SALES',
		'billingstreet' => 'ACCOUNT.ADDRESS1_STREET',
		'billingcity' => 'ACCOUNT.ADDRESS1_CITY',
		'billingstatecode' => 'ACCOUNT.ADDRESS1_STATE_CODE',
		'billingpostalcode' => 'ACCOUNT.ADDRESS1_ZIP',
		'billingcountrycode' => 'ACCOUNT.ADDRESS1_COUNTRY_CODE',
		'billingstate' => 'ACCOUNT.ADDRESS1_STATE',
		'billingcountry' => 'ACCOUNT.ADDRESS1_COUNTRY',
		'createdbyid' => 'CREATEDBY_USER.ALIAS',
		
		'ownerid' => 'CORE.USERS.ALIAS',
		'owner.lastname'=>'CORE.USERS.LAST_NAME',
		'owner.firstname'=>'CORE.USERS.FIRST_NAME',
		'jigsaw' => 'JIGSAW_KEY',
		'numberofemployees' => 'ACCOUNT.EMPLOYEES',
		'fax' => 'ACCOUNT.PHONE2',
		'industry' => 'ACCOUNT.INDUSTRY',
		'lastmodifiedbyid' => 'UPDATEDBY_USER.ALIAS',
		'parentid' => 'PARENT_NAME',
		'shippingstreet' => 'ACCOUNT.ADDRESS2_STREET',
		'shippingcity' => 'ACCOUNT.ADDRESS2_CITY',
		'shippingstatecode' => 'ACCOUNT.ADDRESS2_STATE_CODE',
		'shippingpostalcode' => 'ACCOUNT.ADDRESS2_ZIP',
		'shippingcountrycode' => 'ACCOUNT.ADDRESS2_COUNTRY_CODE',
		'shippingstate' => 'ACCOUNT.ADDRESS2_STATE',
		'shippingcountry' => 'ACCOUNT.ADDRESS2_COUNTRY',
		
		'phone'=>'ACCOUNT.PHONE1',
		'rating'=>'ACCOUNT.RATING',
		'sic'=>'ACCOUNT.SIC',
		'tickersymbol'=>'ACCOUNT.TICKER',
		'type'=>'ACCOUNT.TYPE',
		'website'=>'ACCOUNT.URL',
		'sicdesc'=>'SIC_DESC'
	};
	
	// using from IVPMetadataServiceProvider class
	public static Map<String, Object> fieldWithLabel{
	    get{
	        if(fieldWithLabel == null){
	            fieldWithLabel = new Map<String,Object>();
	            for(string key: IVPListViewMetaData.accFieldMap.keyset()){
	                DescribeFieldResult field = IVPListViewMetaData.accFieldMap.get(key).getDescribe();
    	            Map<String, String> fieldDetail = new Map<String, String>();
    	            fieldDetail.put('fieldLabel',field.getLabel()+'');
    	            fieldDetail.put('fieldType',field.getType()+'');
                    if(IVPListViewMetaData.fieldWithName.containskey(key)){
                        fieldWithLabel.put(IVPListViewMetaData.fieldWithName.get(key), fieldDetail);
                    }else{
                        fieldWithLabel.put(key, fieldDetail);
                    }
                }
	        }
	        return fieldWithLabel;
	    }set;
	}
	public class FieldData {
	    public String fieldLabel;
	    public String fieldType;
	}
	/*public static Map<String, String> fieldWithType{
	    get{
	        if(fieldWithType == null){
	            fieldWithType = new Map<String,String>();
                Map<String, SObjectField> fieldMap = Account.getSobjectType().getDescribe().fields.getMap();
                for(String field: fieldMap.keySet()){
                    fieldWithType.put(field, (fieldMap.get(field).getDescribe().getType()+'').toLowerCase());
                }
	        }
	        return fieldWithType;
	    }set;
	    
	}*/
	public static Map<String, Schema.SObjectField> accFieldMap{
	    get{
            if(accFieldMap == NULL ){
                accFieldMap = new Map<String, Schema.SObjectField> ();
                Schema.DescribeSObjectResult dSob = Account.sObjectType.getDescribe();
                accFieldMap = dSob.fields.getMap();
            }
            return accFieldMap;
	    }set;
	}
    public class ColumnData {

        public String ascendingLabel;
        public String descendingLabel;
        public String fieldNameOrPath;
        public Boolean hidden;
        public String label;
        public String selectListItem;
        public String sortDirection;
        public Integer sortIndex;
        public Boolean sortable;
        public String type;
    }
    
    public class OrderByData {
        public String fieldNameOrPath;
        public String nullsPosition;
        public String sortDirection;
    }
    
    public class WhereConditionData {

        public List<Condition> conditions;
        public String conjunction;
    }
    
    public class Condition{
        public String field;
        public String operator;
        public List<String> values;
        public List<WhereConditionData> conditions;
    }
    
    public class WhereCondition{
        public String field;
        public String operator;
        public List<Object> values;
    }
    public class filterData{
    	public String filterLogic = '';
    	public Integer filterNumber = 1;
    }
    
    public List<ColumnData> columns;
    public List<OrderByData> orderBy;
    public String query;
   // public Object whereCondition;
    
	public IVPMetadataService.ListView parse(String jsonResponse){
	    
	    Map<String, Object> lvMap = (Map<String, Object> )JSON.deserializeUntyped(jsonResponse);
        IVPMetadataService.ListView lv = new IVPMetadataService.ListView();
        lv.filters = new IVPMetadataService.ListViewFilter[]{};
        filterData fD = new filterData();
        Map<String, IVPMetadataService.ListViewFilter> fieldWithFilter = new Map<String, IVPMetadataService.ListViewFilter> ();
        for(String key : lvMap.keySet()){
        	Object keyValue = lvMap.get(key);
        	if(key == 'columns'){
        		Set<String> columns = extractColumns(keyValue);
        		lv.columns = new List<String>(columns);
        		
        	}else if(key == 'whereCondition'){
        		fD = extractFilter(keyValue, fD, lv.filters,fieldWithFilter);
        		lv.booleanFilter = fd.filterLogic;
        		
        	}else if(key == 'query'){
        	    String scope = IVPUtils.extractSOQLScope(String.valueOf(keyValue));
	            lv.filterScope = scope=='' ? 'Everything': 'Mine';
        	}
        }
	    return lv;
	}
   

    String validFieldName(String fieldName, String lowerFieldName){
        
        Integer dotInd = fieldName.indexOf('.');
        if(dotInd > 0){
            String reltionName = fieldName.substring(0, dotInd);
            if(reltionName.endsWith('__r')){
                reltionName = reltionName.replace('__r','__c');
            }else{
                reltionName += 'Id';
            }
            fieldName = reltionName;
            lowerFieldName = reltionName.toLowerCase();
        }
        if(!fieldName.endsWithIgnoreCase('__c')){
            fieldName = '';
            if(lowerFieldName == 'recordtype.name'){
                lowerFieldName = 'recordtypeid';
            }else if(lowerFieldName == 'owner.alias'){
                lowerFieldName = 'ownerid';
            }else if (lowerFieldName == 'lastmodifiedby.alias'){
                lowerFieldName = 'lastmodifiedbyid';
            }else if (lowerFieldName =='createdby.alias'){
                lowerFieldName = 'createdbyid';
            }
            
            if(IVPListViewMetaData.fieldWithName.containsKey(lowerFieldName)){
                fieldName = IVPListViewMetaData.fieldWithName.get(lowerFieldName);
            }
        }
        return fieldName;
    }
    Set<String> extractColumns(Object columnObj){
    	Set<String> columnsSet = new Set<String> ();
    	for(Object column :(List<Object>)columnObj){
    		Map<String, Object> columnMap = (Map<String, Object>)column;
    		String fieldName = String.valueOf(columnMap.get('fieldNameOrPath'));
            String lowerFieldName = fieldName.toLowerCase();
            if(lowerFieldName=='id'){
                break;
            }
            
            fieldName = validFieldName(fieldName, lowerFieldName);
            if(!columnsSet.contains(fieldName) && fieldName != ''){
                columnsSet.add(fieldName);
            }
    	}
    	return columnsSet;
    }

    filterData extractFilter(Object filterObj, filterData fD, IVPMetadataService.ListViewFilter[] lvFilters,
    	Map<String, IVPMetadataService.ListViewFilter> fieldWithFilter){
		
    	Map<String, Object> filterMap = (Map<String, Object> )filterObj;
    	String conjunction = '';
    	if(!(filterMap.containsKey('conjunction') || filterMap.containsKey('conditions'))){
    	    IVPMetadataService.ListViewFilter newFilter = new IVPMetadataService.ListViewFilter();
	    	newFilter.operation = String.valueOf(filterMap.get('operator'));
	    	newFilter.field = String.valueOf(filterMap.get('field'));
            if(newFilter.field != null){
                Schema.DisplayType fieldType = extractFieldType(newFilter.field);
                
                newFilter.field = validFieldName(newFilter.field, newFilter.field.toLowerCase());
                if(newFilter.field != '' ){
                    
                    newFilter.value='';
                    for(Object valueObj : (List<Object>)filterMap.get('values')){
                        if(valueObj != NULL){
                            if(fieldType == Schema.DisplayType.Boolean){
                                valueObj = extractValue(valueObj);
                            }else if(fieldType == Schema.DisplayType.Date){
	    	    		        valueObj = extractDateValue(valueObj);
	    	    		    }
                            newFilter.value += String.valueOf(valueObj)+',';
                        }
                    }
                    newFilter.value = newFilter.value.removeEnd(',');
                    newFilter.value = newFilter.value == 'null' ? '' : newFilter.value;
                    fD.filterLogic+=fD.filterNumber++ + ' ' + conjunction+' ';
                    lvFilters.add(newFilter);
                }
            }
    	}
    	if(filterMap.containsKey('conjunction')){
    		conjunction = String.valueOf(filterMap.get('conjunction'));
    	}
    	if(filterMap.containsKey('conditions')){
    		Object condition = filterMap.get('conditions');
    		if(condition instanceof List<Object>){
    		 	fd = prepareFilter((List<Object>)condition, conjunction.toUpperCase(), fD, lvFilters, fieldWithFilter);
    		}
    	}
    	return fD;
    }

    filterData prepareFilter(List<Object> filters, String conjunction, filterData fD,
		IVPMetadataService.ListViewFilter[] lvFilters, Map<String, IVPMetadataService.ListViewFilter> fieldWithFilter){
			
		for(Object filter :filters){
			Map<String, Object> filterMap = (Map<String, Object> )filter;
			
			if(filterMap.containsKey('conditions')){
				Boolean hasFilter = fD.filterLogic != '';
				fD.filterLogic+= hasFilter ? ' (':'';
				extractFilter(filterMap, fD, lvFilters, fieldWithFilter);
				fD.filterLogic+= hasFilter ? ' )':'';
				
				
			}else if(filterMap.containsKey('condition')){
				
				Map<String, Object> conditionMap = (Map<String, Object>)filterMap.get('condition');
				IVPMetadataService.ListViewFilter newFilter = new IVPMetadataService.ListViewFilter();
		    	newFilter.operation = 'noContains';
		    	newFilter.field = String.valueOf(conditionMap.get('field'));
		    	Schema.DisplayType fieldType = extractFieldType(newFilter.field);
		    	
		    	newFilter.field = validFieldName(newFilter.field, newFilter.field.toLowerCase());
		    	if(newFilter.field != '' ){
		    	    
		    	    newFilter.value='';
	    	    	for(Object valueObj : (List<Object>)conditionMap.get('values')){
	    	    		if(valueObj != NULL){
	    	    		    if(fieldType == Schema.DisplayType.Boolean){
	    	    		        valueObj = extractValue(valueObj);
	    	    		    }else if(fieldType == Schema.DisplayType.Date){
	    	    		        valueObj = extractDateValue(valueObj);
	    	    		    }
	    	    			newFilter.value += String.valueOf(valueObj)+',';
	    	    		}
	    	    	}
	    	    	newFilter.value = newFilter.value.removeEnd(',');
	    	    	newFilter.value = newFilter.value == 'null' ? '' : newFilter.value;
	    	    	fD.filterLogic+=fD.filterNumber++ + ' ' + conjunction+' ';
    	    		lvFilters.add(newFilter);
		    	}
			}else{
				IVPMetadataService.ListViewFilter newFilter = new IVPMetadataService.ListViewFilter();
		    	newFilter.operation = String.valueOf(filterMap.get('operator'));
		    	newFilter.field = String.valueOf(filterMap.get('field'));
		    	Schema.DisplayType fieldType = extractFieldType(newFilter.field);
		    	
		    	newFilter.field = validFieldName(newFilter.field, newFilter.field.toLowerCase());
		    	if(newFilter.field != '' ){
		    		String key=newFilter.field+':'+newFilter.operation;
		    	    
		    	    newFilter.value='';
		    	    for(Object valueObj : (List<Object>)filterMap.get('values')){
	    	    		if(valueObj != NULL){
	    	    		    if(fieldType == Schema.DisplayType.Boolean){
	    	    		        valueObj = extractValue(valueObj);
	    	    		    }else if(fieldType == Schema.DisplayType.Date){
	    	    		        valueObj = extractDateValue(valueObj);
	    	    		    }
	    	    			newFilter.value += String.valueOf(valueObj)+',';
	    	    		}
	    	    	}
	    	    	newFilter.value = newFilter.value.removeEnd(',');
	    	    	newFilter.value = newFilter.value == 'null' ? '' : newFilter.value;
	    	    	
	    	    	if(!fieldWithFilter.containsKey(key)){
	        			fieldWithFilter.put(key, newFilter);
	        		}else{
	        			IVPMetadataService.ListViewFilter oldFilter = fieldWithFilter.get(key);
	        			oldFilter.value+=','+newFilter.value;
	        			fieldWithFilter.put(key, oldFilter);
	        			fD.filterLogic+=fD.filterNumber++ + ' ' + conjunction+' ';
	        		}
	        		
	    			lvFilters.add(newFilter);
		    	}
			}
		}
		fD.filterLogic = fD.filterLogic.removeEnd(conjunction +' '); 
		return fD;
	}
	/*Boolean checkFieldIsBoolean(String fieldName){
	    Schema.SObjectField sobjField = IVPListViewMetaData.accFieldMap.get(fieldName);
	    if(sobjField != NULL){
	        Schema.DescribeFieldResult fieldResult = sobjField.getDescribe();
	        return fieldResult.getType() == Schema.DisplayType.BOOLEAN;
	    }
	    return false;
	}*/
	public static Schema.DisplayType extractFieldType(String fieldName){
	    Schema.SObjectField sobjField = IVPListViewMetaData.accFieldMap.get(fieldName);
	    if(sobjField != NULL){
	        Schema.DescribeFieldResult fieldResult = sobjField.getDescribe();
	        return fieldResult.getType();
	    }
	    return null;
	}
	public static Integer extractValue(object valueObj){
	    Boolean value = valueObj instanceof Boolean;
	    if(value){
	        Boolean.valueOf(String.valueOf(valueObj));
	    }
	    if(value){
	        return 1;
	    }
	    return 0;
	}
	String extractDateValue(object value){
	    String val = String.valueOf(value);
	    Matcher myMatcher = date_Pattern.matcher(val);
	    if(myMatcher.matches())
            return val.replaceAll(dateRegex,'$2\\/$3\\/$1');
        return '';
	}
}