public class InvoiceTriggerHelper {

    //This method will create Invoice Line Items for Project Companies
    public static void CreateAdditionalInvoiceLineItem(List<Krow__Invoice__c> InvoiceList){
        if(InvoiceList != NULL && InvoiceList.size() > 0){
            for(Krow__Invoice__c Invoice : InvoiceList){
                if(Invoice.Krow__Invoice_Account__c != null && Invoice.Krow__Start_Date__c != null && Invoice.Krow__End_Date__c != null){
                    system.debug('>>>>> createInvoiveLineItemsForInvoices Called --> ');
                    AddInvoiceLineItem_Custom.createInvoiveLineItemsForInvoices(Invoice, '');
                }
            }
        }
    }
}