/**
* @Description LeadTriggerHandlerTest is used to perform unit tests on public methods within the LeadTriggerHandlerTest
*
* @Author: Daniel Llewellyn
* @Date:   Aug 20, 2018
* @Note: DL 08/20/2018 Initial Draft
**/

@isTest
private class LeadTriggerHandlerTest {

    /**
    * @Description Is run before each test to create test data that is needed across all methods in this class
    **/
    @testSetup static void setup() {
        Common_Config__c config = CommonConfigUtil.getConfig();
        System.debug(config);

        // can't use user-level custom settings, so let's use the profile-level
        config = Common_Config__c.getInstance(userinfo.getProfileId());
        upsert config;

        createLeadOwnerUser();
    }

    /**
    * @Description This unit test will cover all the possible outcomes of the LeadTriggerHandlerTest.ConvertAndMatchPardotLeads and the private methods it uses (createAccountFromLead, parseWebsiteDomain).
    **/
	@isTest static void testConvertAndMatchPardotLeads1() {
		
		Test.startTest();
		
        //The first check is that logic only kicks off when lead.pi__url__c is not empty, the trigger do any actual processing.
        Lead testLead1 = new Lead();
        testLead1.Firstname = 'Test1';
        testLead1.Lastname = 'Lead1';
        testLead1.Company = 'Test Company 1';
        testLead1.Website = 'http://www.myTestWebsite.com';
        testLead1.Status = 'Open';
        testLead1.Email = 'testEmail@myTestWebsite.com';

        insert testLead1;

		Test.stopTest();
        
        //we can check that nothing happened by checking that there is no account or contact automatically created
        list<Account> testAccounts = [select id from account limit 1];
        system.assertEquals(testAccounts.size(),0, 'No accounts should have been automatically created because lead was not inserted by a pardot user. Update this assert if other processess can automatically convert leads');

        list<Contact> testContacts = [select id from contact limit 1];
        system.assertEquals(testContacts.size(),0, 'No contacts should have been automatically created because lead was not inserted by a pardot user. Update this assert if other processess can automatically convert leads');
        
        //hooray if we made it here all of the above logic worked! That's a wrap on this test.
    }
    
    @isTest static void testConvertAndMatchPardotLeads2() {
        
		Test.startTest();
		
        //now lets create the lead with pardot url populated and see if an account gets created.
        Lead testLead2 = new Lead();
        testLead2.pi__url__c = 'http://www.pardot.com/test.html';
        testLead2.Company = 'http://www.myTestWebsite2.com';
        testLead2.Firstname = 'Test2';
        testLead2.Lastname =  'Lead2';
        testLead2.Email = 'test2.lead2@myTestWebsite2.com';

        insert testLead2;

		Test.stopTest();
		
        //now that a pardot url has a value in the lead there should be an account created (since one does not exist that matches website domain or name) and the lead should be converted.
        list<Account> testAccounts = [select id, name, website, recordType.name, Website_Domain__c from account];
        system.debug('\n\n\n----- Accounts in the system currently position 1');
        system.debug(testAccounts);
        system.assertEquals(1,testAccounts.size(), 'No account was created for the pardot lead. Automatic lead conversion did not work.');


        //for some reason I don't quite understand 2 contacts exist at this point. One for the first lead which should still just be in open status and not processed by
        //the trigger at all since it is only an after update. However there is a second contact that has the FirstName of 'Test' and a LastName of = 'Lead1'
        // WN note: there's 2 contacts because of a Proxy Contact is always created for each company (to be used by the Banking team)
        list<Contact> testContacts = [select id, firstname, lastname, account.name from contact];
        system.debug('\n\n\n----- Contacts in the system currently position 1');
        system.debug(testContacts);
        system.assertEquals(2,testContacts.size(), 'No contact was created for the pardot lead. Automatic lead conversion did not work. There should be ');

        //hooray if we made it here all of the above logic worked! That's a wrap on this test.
    }
    
    @isTest static void testConvertAndMatchPardotLeads3() {
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Company3';
        testAccount.RecordType = new RecordType(Name='Prospect');
        testAccount.Website = 'http://www.myTestWebsite3.com';
        insert testAccount;

		Test.startTest();
		
        //make another lead that is the same info (though the only really important part is the website and the firstname lastname)
        Lead testLead3 = new Lead();
        testLead3.pi__url__c = 'http://www.pardot.com/test.html';
        testLead3.Firstname = 'Test3';
        testLead3.Lastname =  'Lead3';
        testLead3.Company = 'Test Company3';
        testLead3.Website = 'http://www.myTestWebsite3.com';

        insert testLead3;

		Test.stopTest();
		
        //since the logic should have matched on the existing account there should still only be one account in the system at this point. Also the lead should have its accountId set to that existing account
        testLead3 = [select id, Firstname, LastName, Company, ConvertedAccountId, IsConverted from Lead where id = :testLead3.Id];
        system.assertEquals(testAccount.id, testLead3.ConvertedAccountId, 'Test Account for TestLead3 is not the same as TestLead2. This means it was not caught by the matching logic');
        system.assertEquals(true, testLead3.IsConverted);

        list<Account> testAccounts = [select id, name, website, recordType.name, Website_Domain__c  from account];
        system.debug('\n\n\n----- Accounts in the system currently position 2');
        system.debug(testAccounts);
        system.assertEquals(1,testAccounts.size(), 'An additional account was created during lead conversion. This should not happen as there is a duplicate account and it matched on Website_Domain__c');

        //hooray if we made it here all of the above logic worked! That's a wrap on this test.
    }
    
    @isTest static void testConvertAndMatchPardotLeads4() {
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Company3';
        testAccount.RecordType = new RecordType(Name='Prospect');
        testAccount.Website = 'http://www.myTestWebsite3.com';
        insert testAccount;
        
        Test.startTest();
        
        //now lets create a lead with a different website domain and name and such.
        Lead testLead4 = new Lead();
        testLead4.pi__url__c = 'http://www.pardot.com/test.html';
        testLead4.Firstname = 'Test4';
        testLead4.Lastname = 'Lead4';
        testLead4.Company = 'Test Company 4';
        testLead4.Website = 'http://www.myTestWebsite4.com';
        testLead4.Status = 'Open';

        insert testLead4;

		Test.stopTest();
		
        testLead4 =  [select id, Firstname, LastName, Company, Website, ConvertedAccountId, IsConverted from Lead where id = :testLead4.Id];
        //we should now have two accounts in the system
        list<Account> testAccounts = [select  id, name, website, recordType.name, Website_Domain__c from account];
        system.debug('\n\n\n----- Accounts in the system currently position 3');
        system.debug(testAccounts);
        system.assertEquals(2,testAccounts.size(), 'An additional account was NOT created during lead conversion. There should not have been a match as the most recently inserted lead shared no identical data with any exist lead in the test suite.');

        //hooray if we made it here all of the above logic worked! That's a wrap on this test.
    }
    
    @isTest static void testConvertAndMatchPardotLeads6() {
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Company6';
        testAccount.RecordType = new RecordType(Name='Prospect');
        testAccount.Website = 'http://www.myTestWebsite6.com';
        insert testAccount;
        
        Test.startTest();
        
        //if we create a lead that matches the account name but not the domain it should get matched to that.
        Lead testLead6 = new Lead();
        testLead6.pi__url__c = 'http://www.pardot.com/test.html';
        testLead6.Website = 'http://anotherDomainThatDoesntMatchButNameFieldsShould.com';
        testLead6.Firstname = 'Test';
        testLead6.LastName = 'Lead6';
        testLead6.Company = testAccount.Name;
        
        insert testLead6;

		Test.stopTest();
		
        testLead6 =  [select id, Firstname, LastName, Company, Website,  ConvertedAccountId, IsConverted from Lead where id = :testLead6.Id];
        system.assertEquals(true, testLead6.IsConverted, 'Test lead 6 did not convert on insert as it should have.');

        //because testAccount and testLead6 had different domain names but the same name they should be joined to the same account.
        system.assertEquals(testLead6.ConvertedAccountId, testAccount.Id, 'Test lead6 does not have the same account as testAccount. The name matching logic did not work or was skipped.');

        //hooray if we made it here all of the above logic worked! That's a wrap on this test.
    }
    
    @isTest static void testConvertAndMatchPardotLeads7() {
        
          String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(FirstName='LiquidHub',LastName='Admin',Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);
        System.runAs(u)
        {
             Account testAccount = new Account();
            testAccount.Name = 'Test Company7';
            testAccount.RecordType = new RecordType(Name='Prospect');
            testAccount.Website = 'http://www.myTestWebsite7.com';
            insert testAccount;
            List<Account> lstAcct = [Select Id,Website_Domain__c from Account where Id =:testAccount.id];
            System.assertEquals(lstAcct[0].Website_Domain__c,'mytestwebsite7.com');
            Test.startTest();
            
            //try making a lead with no website and only an email. This should also match existing company if it matches an accounts website. Lets match test4s domain
            Lead testLead7 = new Lead();
            testLead7.pi__url__c = 'http://www.pardot.com/test.html';
            testLead7.Firstname = 'Test7';
            testLead7.LastName = 'Lead7';
            testLead7.Company = 'Test 7 Lead company';
            testLead7.Email = 'testGuy@myTestWebsite7.com';
            System.assertEquals(LeadTriggerHandler.parseEmailDomain(testLead7.Email),'mytestwebsite7.com');
            insert testLead7;
            
            Test.stopTest();
            
            testLead7 =  [select id, Firstname, LastName, Company, Website, ConvertedAccountId, IsConverted from Lead where id = :testLead7.Id];
            system.assertEquals(testLead7.ConvertedAccountId, testAccount.Id, 'Test lead7 does not have the same account as testAccount. The name matching logic did not work or was skipped.');

            //hooray if we made it here all of the above logic worked! That's a wrap on this test.
        }
       
    }


    /**
    * @Description Creates a user that mimics the pardot user in the real org using the system administrator profile. Inserts and returns the user
    **/
    private static user createLeadOwnerUser()
    {
        string ownerName =  CommonConfigUtil.getConfig().Pardot_Converted_Owner_Full_Name__c;

        list<User> ownerQuery = [select name from user where name = :ownerName];

        if(!ownerQuery.isEmpty()) return ownerQuery[0];

        ownerName = ownerName.split(' ').size() < 2 ? ' ' + ownerName : ownerName;
        Profile p = [ SELECT Id FROM Profile WHERE Name = 'System Administrator' ];
        User u = new User( Alias = 'standt',
                           Email='standarduser@testorg.dev',
                           EmailEncodingKey = 'UTF-8',
                           Firstname = ownerName.split(' ')[0].trim(),
                           Lastname = ownerName.split(' ')[1].trim(),
                           LanguageLocaleKey='en_US',
                           LocaleSidKey = 'en_US',
                           ProfileId = p.Id,
                           TimeZoneSidKey = 'America/Los_Angeles',
                           UserName = 'pardotLeadOwnerUser@testorg.dev' );


        insert u;
        return u;
    }

}