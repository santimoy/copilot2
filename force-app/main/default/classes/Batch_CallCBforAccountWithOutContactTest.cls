/*
Class Name   :  Batch_CallCBforAccountWithOutContactTest
Description  :  Test Code for Calling clear bit for Key Contact Scope object where account does not have any contact
Author       :  Sukesh
Created On   :  August 20 2019
*/
@isTest
private class Batch_CallCBforAccountWithOutContactTest {

    @TestSetup
    static void createTestData(){

        List<Clearbit_Role_Settings__c> lstRoleSettings = new List<Clearbit_Role_Settings__c>();
        lstRoleSettings.add(new Clearbit_Role_Settings__c(Name = 'CEO', Active__c = TRUE, Ranking__c = 1, Role__c = 'CEO'));
        lstRoleSettings.add(new Clearbit_Role_Settings__c(Name = 'Founder', Active__c = TRUE, Ranking__c = 4, Role__c = 'Founder'));
        insert lstRoleSettings;

        List<DWH_Role_Settings__c> listDWHRoles = new List<DWH_Role_Settings__c>();
        listDWHRoles.add(new DWH_Role_Settings__c(Name = 'CEO', Active__c = TRUE, Ranking__c = 1, Role__c = 'CEO'));
        listDWHRoles.add(new DWH_Role_Settings__c(Name = 'Founder', Active__c = TRUE, Ranking__c = 4, Role__c = 'Founder'));
        insert listDWHRoles;

        List<Account> lstAccountToInsert = new List<Account>();
        List<Contact> lstContactToInsert = new List<Contact>();

        for(Integer intCount=0; intCount<25; intCount++) {

            lstAccountToInsert.add(new Account(RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId(),
                                                       Name = 'Test Account '+intCount));
        }

        insert lstAccountToInsert;

        /*for(Integer intAccountCount=0; intAccountCount<15; intAccountCount++) {

            for(Integer intContactCount = 0; intContactCount<2; intContactCount++) {

                lstContactToInsert.add(new Contact(LastName = 'Test Contact '+intAccountCount+' '+intContactCount,
                                                   AccountId = lstAccountToInsert[intAccountCount].Id));
            }
        }

        insert lstContactToInsert;*/

        List<Key_Contact_Scope__c> lstKeyContactScopeToInsert = new List<Key_Contact_Scope__c>();

        for(Integer intAccountCount=0; intAccountCount<25; intAccountCount++) {

            lstKeyContactScopeToInsert.add(new Key_Contact_Scope__c(Account__c = lstAccountToInsert[intAccountCount].Id,
                                                                        Account_Id__c = lstAccountToInsert[intAccountCount].Id));
        }

        insert lstKeyContactScopeToInsert;
    }

    @isTest
    static void testKeyContactScopeObjectCreationandCallToClearBit(){

        Test.startTest();
            Database.executeBatch(new Batch_CallCBforAccountWithOutContact(), 10);
        Test.stopTest();
    }
}