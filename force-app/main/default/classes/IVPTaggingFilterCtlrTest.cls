@isTest
private class IVPTaggingFilterCtlrTest {

private static testMethod void testGetInitData() {
    IVPTestFuel tFuel = new IVPTestFuel('init');
	// List<Topic> topicList = tFuel.topicList;
    Test.startTest();
	    Map<String,Object>  topicMap = (Map<String,Object> )IVPTaggingFilterCtlr.getInitData();
    Test.stopTest();
    System.assert(!topicMap.isEmpty());
}
private static testMethod void testingManagingTagsWithHTTPRequest(){
    IVPTestFuel tFuel = new IVPTestFuel('modify');
	List<Topic> topicList = tFuel.topicList;
    Test.startTest();
    	Map<String, Object> mapOfObject = IVPTaggingFilterCtlr.maitainTags('{"chkFilter" : "true", "fieldApi" : "Tags__c", "filterScope" : "Everything", "selectedTagId" : "", "selectedValue" : "GreatLeads", "source" : "tag", "sourceType" : "advanced"}');
    Test.stopTest();
    System.debug('mapOfObject'+mapOfObject);
	//System.assertEquals(true,b);
}
    /*
private static testMethod void testingListView(){
    insert new IVP_SD_ListView__c(List_View_Name__c = 'Company_Tags');
    Test.startTest();
    IVPTaggingFilterCtlr.mataintainListView('','');
    Test.stopTest();
}
private static testMethod void testingManagingTags(){
    Map<String, Object> valueMp = (Map<String, Object>)JSON.deserializeUntyped('{"refresh":"true", "source":"tag", "action":"action"}');
    Test.setMock(HttpCalloutMock.class, new IVPTaggingMock());
    
    IVPTaggingFilterCtlr.mataintainListView(valueMp);
    
    
}
private static testMethod void testingUpsertHandlingWithSuccess(){
    
   	IVPMetadataService.UpsertResult IVPMdS=new IVPMetadataService.UpsertResult();
    IVPMdS.success=true;
    IVPTaggingFilterCtlr.handleUpsertResults(IVPMdS);
}
private static testMethod void testingUpsertHandlingWithSuccessFalse(){
   	IVPMetadataService.UpsertResult IVPMdS=new IVPMetadataService.UpsertResult();
    IVPMdS.success=false;
    try{
    	IVPTaggingFilterCtlr.handleUpsertResults(IVPMdS);    
    }catch(Exception excp){
        System.assertEquals('Request failed with no specified error.', excp.getMessage());
    }
    
}
private static testMethod void testingUpsertHandlingWithErrors(){
    IVPMetadataService.UpsertResult IVPMdS=new IVPMetadataService.UpsertResult();
    IVPMetadataService.Error IVPMdSError=new IVPMetadataService.Error();
    IVPMdSError.message='Error!';
    IVPMdS.errors=new List<IVPMetadataService.Error>();
    IVPMdS.errors.add(IVPMdSError);
    IVPMdS.success=false;
    List<String> messages = new List<String>();
    messages.add((IVPMdS.errors.size()==1 ? 'Error ' : 'Errors ') +'occured processing component ' + IVPMdS.fullName + '.');
        for(IVPMetadataService.Error error : IVPMdS.errors)
            messages.add(
                error.message + ' (' + error.statusCode + ').' +
                ( error.fields!=null && error.fields.size()>0 ?
                    ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
    try{
    	IVPTaggingFilterCtlr.handleUpsertResults(IVPMdS);    
    }catch(Exception excp){
        System.assertEquals(String.join(messages, ' '), excp.getMessage());
    }
    
}*/
}