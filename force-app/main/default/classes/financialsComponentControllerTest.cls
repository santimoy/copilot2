/*
Created By  : Sukesh Kumar 
Created On  : 04/09/19
Description : Test Code for Controller of Financials Component
*/

@isTest
private class financialsComponentControllerTest {
	
	@isTest static void test_method_one() {
		
		Account objAccount = new Account(Name ='Testaccount');
		insert objAccount;

		String strResponse = financialsComponentController.fetchInitData(objAccount.Id);

		financialsComponentController.saveData(objAccount.Id,2018,
											   2019,2020,
											   2017,2018,
											   2020,5643,
											   2017,2017,
											   1,2,3,4,5,6,7,8,
											   'ARR');

		strResponse = financialsComponentController.fetchInitData(objAccount.Id);

		financialsComponentController.saveData(objAccount.Id,2018,
											   2019,2020,
											   2017,2018,
											   2020,5643,
											   2017,2017,
											   1,2,3,4,5,6,7,8,
											   'ARR');
	}
	
}