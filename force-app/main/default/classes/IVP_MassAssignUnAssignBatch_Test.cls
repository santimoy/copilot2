@isTest
public class IVP_MassAssignUnAssignBatch_Test {
    static Set<Id> accountIds(){
        List<Account> listAccount = new List<Account>();
        listAccount.add(new Account(Name='Test-Account-1'));
        listAccount.add(new Account(Name='Test-Account-2'));
        insert listAccount;
        
        Set<Id> ids = new Set<Id>();
        for(Account acc : listAccount){
            ids.add(acc.id);
        }
        return ids;
    } 
    @isTest static void testMassAssignBatch(){
        Test.startTest();
        Set<Id> ids = accountIds();
        IVP_MassAssignUnAssignBatch massAssignBatch = new IVP_MassAssignUnAssignBatch(true,ids,'','tet');
        massAssignBatch.mapOfFailRecords.put(new List<id>(ids)[0],new Map<String,String>{'TestAccount'=>'Some Error Occurs'});
        Database.executeBatch(massAssignBatch,200);
        Test.stopTest();
        
        System.assertEquals(true, massAssignBatch.isMassAssign);
        System.assertEquals(2, massAssignBatch.acctIdsSet.size());
        System.assertEquals('', massAssignBatch.lossDropReason);
    }
    @isTest static void testMassUnassignBatch(){
        Test.startTest();
        IVP_MassAssignUnAssignBatch massUnassignBatch = new IVP_MassAssignUnAssignBatch(false,accountIds(),'No opportunity given prior capital','test');
        Database.executeBatch(massUnassignBatch,200);
        Test.stopTest();
        
        System.assertEquals(false, massUnassignBatch.isMassAssign);
        System.assertEquals(2, massUnassignBatch.acctIdsSet.size());
        System.assertEquals('No opportunity given prior capital', massUnassignBatch.lossDropReason);
    }
}