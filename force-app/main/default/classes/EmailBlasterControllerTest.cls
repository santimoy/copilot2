@isTest
private class EmailBlasterControllerTest
{
	@testSetup static void setup()
	{
		Email_Format_Template__c template = new Email_Format_Template__c( Name = 'FInit.Last@webdomain.com', Enabled__c = true );
		Email_Format_Template__c template2 = new Email_Format_Template__c( Name = 'First.Last@webdomain.com', Enabled__c = true );
		insert new List<Email_Format_Template__c>{ template, template2 };

		Account testAccount = new Account( Name = 'Test Company' );
		testAccount.Website = 'www.test.com';
		insert testAccount;
	}

	@isTest static void testGetAllEmails_NoKeyContact()
	{
		Account testAccount = [ SELECT Id FROM Account ];

		Test.startTest();

			List<String> allEmails = EmailBlasterController.getAllEmails( testAccount.Id );

		Test.stopTest();

		System.assert( allEmails.isEmpty() );
	}

	@isTest static void testGetAllEmails_WithKeyContact_WithEmail()
	{
		final String EMAIL_ADDRESS = 'test.contact@test.com';
		Account testAccount = [ SELECT Id FROM Account ];
		Contact testContact = TestFactory.createContacts( 1, testAccount.Id, 'TestContact', false )[0];
		testContact.Key_Contact__c = true;
		testContact.Email = EMAIL_ADDRESS;
		testContact.FirstName = '';
		testContact.LastName = 'Jeanne Doe';
		insert testContact;

		Test.startTest();

			List<String> allEmails = EmailBlasterController.getAllEmails( testAccount.Id );

		Test.stopTest();

		Set<String> emailsSet = new Set<String>( allEmails );
		System.assert( emailsSet.contains( EMAIL_ADDRESS ) );
	}

	@isTest static void testGetAllEmails_WithKeyContact_Blaster()
	{
		Account testAccount = [ SELECT Id FROM Account ];
		Contact testContact = TestFactory.createContacts( 1, testAccount.Id, 'TestContact', false )[0];
		testContact.Key_Contact__c = true;
		testContact.FirstName = 'John';
		testContact.LastName = 'Jeanne Doe';
		insert testContact;

		Test.startTest();

			List<String> allEmails = EmailBlasterController.getAllEmails( testAccount.Id );

		Test.stopTest();

		Set<String> emailsSet = new Set<String>( allEmails );
		System.assert( emailsSet.contains( 'J.Doe@test.com' ) );
		System.assert( emailsSet.contains( 'John.Doe@test.com' ) );
	}
}