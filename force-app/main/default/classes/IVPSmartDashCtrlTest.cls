/**
 * @author 10k-Advisor
 * @date 2018-05-19
 * @className IVPSmartDashCtrlTest
 * @description : To Test the IVPSmartDashCtrl Class..
 */

@isTest
private class IVPSmartDashCtrlTest {
    private static testMethod void testGetTodayVisitedWatchDog() {
        insert new Intelligence_User_Preferences__c(User_Id__c = userInfo.getUserId(), Last_Visited_WatchDog__c = System.today());
        Test.startTest();
        Boolean status = IVPSmartDashCtrl.getTodayVisitedWatchDog(true);
        status = IVPSmartDashCtrl.getTodayVisitedWatchDog(true);
        System.assertEquals(TRUE, status);
        Test.stopTest();
    }
}