/***********************************************************************************
 * @author      10k-Expert
 * @name    IVPOwnershipServiceTest
 * @date    2018-11-19
 * @description  To test IVPOwnershipService
 ***********************************************************************************/
@isTest
public class IVPOwnershipServiceTest {
    @isTest
    public static void testCheckUnassignedValidation(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        
        Test.startTest();
        List<Company_Request__c> companyRequestLst = testData.companyRequests;
        Map<Id, ValidationWrapper> validationWrapperMap = IVPOwnershipService.checkUnassignedValidation(new List<Id>{UserInfo.getUserId()});
        Test.stopTest();
        
        //System.assertEquals(4, validationWrapperMap.get(UserInfo.getUserId()).requestBalance);
        System.assertEquals(3, validationWrapperMap.get(UserInfo.getUserId()).requestBalance);
        
    }
    
  @isTest
    public static void testProperties(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel(); 
        testdata.populateIVPGeneralConfig();
        ValidationWrapper wrapObj = new ValidationWrapper(true);
        
        Test.startTest();
        
        String requestBalance = IVPOwnershipService.getGeneralConfigStrValue('ownership-request-balance');
        System.debug('requestBalance '+requestBalance);
        Test.stopTest();
        
        System.assertEquals(5, Integer.valueOf(requestBalance));
        // System.assertEquals(5, IVPOwnershipService.getOwnershipRequestBalance());
        //System.assertEquals(150, IVPOwnershipService.getUntouchableCount());
        System.assertEquals(150, IVPOwnershipService.priorityBalance);
        // total-count value is defined 550 in IVPOwnershipTestFuel
        //System.assertEquals(500, IVPOwnershipService.getTotalCount());
        //System.assertEquals(550, IVPOwnershipService.getTotalCount());
        System.assertEquals(550, IVPOwnershipService.totalCompanyBalance);
        System.assertEquals(5, IVPOwnershipService.ownershipRequestBalance);
        System.assertEquals(30, IVPOwnershipService.ownershipRequestBalanceDays);
        System.assertEquals(3, IVPOwnershipService.requestProcessingWorkingHours);
        System.assertEquals(180, IVPOwnershipService.partialOwnerLastInterationDays);
        // check-open-opportunity-last-n-days value is defined -1 in IVPOwnershipTestFuel
        //System.assertEquals(7, IVPOwnershipService.openOpportunityLastInterationMonth);
        System.assertEquals(-1, IVPOwnershipService.openOpportunityLastInterationMonth);
        
        System.assertEquals(24, IVPOwnershipService.secondEmailNotifyHourGap);
        System.assertEquals(1, IVPOwnershipService.finalEmailNotifyHourGap);
        System.assertEquals(550, IVPOwnershipService.totalCompanyBalance);
        // to cover ValidationWrapper class 
        System.assert(wrapObj != null);
        // System.assertEquals(3, IVPOwnershipService.dropDemoteCheckUptoInterationDays);
    }
    
  /*@isTest
    public static void testReduceUserRequest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        List<User> users = [SELECT Id, Ownership_Request_Balance__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        users[0].Ownership_Request_Balance__c = 5;
        update users;
        
        Test.startTest();
        IVPOwnershipService.reduceUserRequest(UserInfo.getUserId());
        Test.stopTest();
        
        System.assertEquals([SELECT Id, Ownership_Request_Balance__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Ownership_Request_Balance__c, 4);
    }*/
    
  @isTest
    public static void testGetUserWithRequestCount(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        Account acc = new Account(Name='Test');
        insert acc;
        List<Company_Request__c> companyRequestLst = new List<Company_Request__c>();
        
        companyRequestLst.add(new Company_Request__c(Company__c=acc.Id,Request_By__c=UserInfo.getUserId()));
        
        insert companyRequestLst;
        
        Test.startTest();
        Map<String, Integer> userWithRequestCount = IVPOwnershipService.getUserWithRequestCount(new List<Id>{UserInfo.getUserId()});
        Test.stopTest();
        
        System.assertEquals(1, userWithRequestCount.get(UserInfo.getUserId()));
    }
}