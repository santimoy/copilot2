/**
 *  @date: May 22, 2018
 *  @author: Nazia Khanam
 *  @desciption: This class is used to cover the code of IVPLogActivityCtlr's
**/
@isTest
private class IVPLogActivityCtlrTest {
	@testSetup 
	static void setupData(){
		IVPTestFuel tFuel = new IVPTestFuel();
		tFuel.populateLVSettings();
	}
	private static testMethod void test1() {
        IVPTestFuel tFuel = new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        String inputJson = '{"accId":"'+ companies[0].Id+'"}';
        system.assertNotEquals(NULL, IVPLogActivityCtlr.getInitData(inputJson));
    }
    private static testMethod void test2(){
        IVPTestFuel tFuel = new IVPTestFuel();
        String inputJson = tFuel.inputJson;
        System.assertNotEquals(NULL, IVPLogActivityCtlr.prepareTask(inputJson));
    }
}