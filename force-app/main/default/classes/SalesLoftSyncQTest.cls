/**
 * @author  : 10k-Advisor
 * @name      : SalesloftMock
 * @date      : 2018-08-17
 * @description   : This test class is related to SalesLoftSyncQ class
**/
@isTest
private class SalesLoftSyncQTest {
    
    static void preapreAPIKey(){
        IVP_General_Config__c config = IVP_General_Config__c.getInstance('salesloft-api-key');
        if(config == NULL){
            config = new IVP_General_Config__c(Name='salesloft-api-key');
        }
        config.Value__c='test';
        upsert config;
    }
    
    private static testMethod void testSyncPeople() {
        IVPTestFuel tFuel=new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Contact> contacts = tFuel.contacts;
        
        preapreAPIKey();
        
        Test.startTest();
        	System.enqueueJob(new SalesLoftSyncQ('people', Salesloft.PEOPLE_END_POINT,1));
        Test.stopTest();
        
        List<Contact> con = new List<Contact>([SELECT  Salesloft_Id__c FROM Contact WHERE SalesLoft_Id__C!=NULL]);
        System.assert(con.size() == 2);
         
    }   
    private static testMethod void testSynccadence(){
        
        preapreAPIKey();
        
        Test.startTest();
        	System.enqueueJob(new SalesLoftSyncQ('cadence', Salesloft.CADENCE_END_POINT,1));
        Test.stopTest();
        
        List<Salesloft_Cadence__c> cadence = new List<Salesloft_Cadence__c>([SELECT Id from Salesloft_Cadence__c]);
        System.assert(cadence.size() == 2);
    }
    private static testMethod void testSynccadencememeber(){
        IVPTestFuel tFuel=new IVPTestFuel();
        List<Account> companies = tFuel.companies;
        List<Contact> contacts = tFuel.contacts;
        contacts[0].Salesloft_Id__c = SalesloftMock.people1Id;
        contacts[1].Salesloft_Id__c = SalesloftMock.people2Id;
        update contacts;
        preapreAPIKey();
        
        Test.startTest();
        	System.enqueueJob(new SalesLoftSyncQ());
        Test.stopTest();
        
        List<Salesloft_Cadence_Member__c> cadence = new List<Salesloft_Cadence_Member__c>([SELECT Id from Salesloft_Cadence_Member__c]);
        System.assert(cadence.size() == 2);
    }
    
    @isTest static void testException(){
        Test.startTest();
            System.enqueueJob(new SalesLoftSyncQ('people'));
        Test.stopTest();
    }
}