/**
*@name      	: CampaignCompanyAutomation_Test
*@developer 	: 10K developer
*@date      	: 2017-09-26
*@description   : The class used as test class of CampaignCompanyAutomation
**/
@isTest
Public class CampaignCompanyAutomation_Test {
    
    public static testMethod void allTest(){
        Integer numberOfContacts = 5;
         //Adding Campaings
        List<Campaign> campList =  new List<Campaign>();
        for(integer i=0;i<5;i++){
            campList.add(new Campaign(Name = 'Test Campaign '+i, IsActive = TRUE,
                                      Type=(i < 2 ? 'Portfolio Company Demand Gen' : 'Dinner / Roundtable'), StartDate=System.today()));
        }
        insert campList;
        
        List<Account> accList = new List<Account>();
        for(integer i=0; i<numberOfContacts; i++){
            accList.add(new Account(Name = 'TestAccount'+i));
        }
        insert accList;
        
        List<Contact> conList = new List<Contact>();
        for(integer i=0; i<numberOfContacts; i++){
            conList.add(new Contact(FirstName = 'TestContactFirst'+i,LastName = 'TestContactLast'+i, AccountId = accList[i].Id));
        }
        checkRecursive.runOnce();
        insert conList;
        
        List<CampaignMember> campMemberList = new List<CampaignMember>();
        for(integer i=0; i<5; i++){
            for(integer conCount=0; conCount<numberOfContacts; conCount++){
            	campMemberList.add(new CampaignMember(Attendee_Type__c = 'Executive Guest',Status = (i < 2 ? 'Follow up 1' : 'Attended'),campaignId = campList[i].Id,contactId = conList[conCount].Id));
            }
        }
        insert campMemberList;
        update campMemberList;
       
		CampaignMemberHandler cmHandler = new CampaignMemberHandler();
        CampaignHandler cHandler = new CampaignHandler();
        CampaignUtil cUtil = new CampaignUtil();
        CampaignCompanyAutomation campAutomation = new CampaignCompanyAutomation();
        CampaignCompanyHandler cmp = new CampaignCompanyHandler();
        
        
    }
    
    //static String AccountQuery = 'SELECT Id FROM Account';
     /*
     * This method will setup test data 
	*/

    /*
     * 1.When inserting a new company child Campaign and CampaignMembers should be create autometically for newly created company
     * 2.When deleting a  company child Campaign and CampaignMembers should be deleted autometically 
	*/
    /*static testMethod void campaignCompanyCheck() {
        Map<Id,Campaign> campaignsMap = new Map<Id,Campaign>([SELECT Id FROM Campaign]);
        
        List<Campaign_Company__c> campCompanies = new List<Campaign_Company__c>();
        List<Account> accounts = Database.query(AccountQuery + '  LIMIT 5');
        for(Campaign campaignObj : campaignsMap.values()){
            for(Account acc : accounts)
        		campCompanies.add(new Campaign_Company__c(Campaign__c = campaignObj.Id,Company__c = acc.Id));    
        }
        
        Test.startTest();
        /* When you insert the companies child campaigns and campaignmembers should be created  */
        /*insert campCompanies;
        Map<Id,Campaign> childCampaignsMap = new Map<Id,Campaign>([SELECT Id FROM Campaign WHERE parentId in :campaignsMap.keySet()]);
        integer countParentCampMember = [SELECT count() FROM CampaignMember WHERE CampaignId in :campaignsMap.keySet()];*/
        //System.assertEquals(campCompanies.size(), childCampaignsMap.size());
        //System.assertEquals(countParentCampMember*campaignsMap.size(), [SELECT Id FROM CampaignMember WHERE campaignId in :childCampaignsMap.keySet()].size());
        
        /*
         *  When you delete the companies child campaigns and campaignmembers should be delete 
         *	Note: If you delete the campaign child campaignmember will be delete   automatically so that here is no assert for 
         */
        /*delete campCompanies;
       	System.assertEquals(0, [SELECT Id FROM Campaign WHERE parentId in :campaignsMap.keySet()].size());
		Test.stopTest();
	}*/
    
    /*
     * 1.When deleting the parent campaigns child campaigns should be delete
     * 2.When updating the parent campaigns child campaigns should be updated
	*/
    /*static testMethod void checkCampaignUpdate() {
        
        Map<Id,Campaign> campaignsMap = new Map<Id,Campaign>([SELECT Id,Status From Campaign]);
        List<Campaign_Company__c> campCompanies = new List<Campaign_Company__c>();
        List<Account> accounts = Database.query(AccountQuery + '  LIMIT 5');
        for(Campaign campaignObj : campaignsMap.values()){
            for(Account acc : accounts)
                campCompanies.add(new Campaign_Company__c(Campaign__c = campaignObj.Id,Company__c = acc.Id));    
        }
        insert campCompanies;
        
        for(Campaign camp : campaignsMap.values()){
            camp.Status = 'Responded';
        }
        
        Test.startTest();
        //When update the parent campaigns child campaigns should be updated
        
        update campaignsMap.values();
        System.assertEquals(0, [SELECT Id FROM Campaign WHERE parentId in :campaignsMap.keySet() AND Status != 'Responded'].size());
        Test.stopTest();
    }*/
    /*If we delete the parent campaign child campaign should be delete*/
    /*public static testMethod void  checkCampaignDelete(){
        Map<Id,Campaign> campaignsMap = new Map<Id,Campaign>([SELECT Id,Status From Campaign]);
        List<Campaign_Company__c> campCompanies = new List<Campaign_Company__c>();
        List<Account> accounts = Database.query(AccountQuery + '  LIMIT 5');
        for(Campaign campaignObj : campaignsMap.values()){
            for(Account acc : accounts)
                campCompanies.add(new Campaign_Company__c(Campaign__c = campaignObj.Id,Company__c = acc.Id));    
        }
        insert campCompanies;
        delete campaignsMap.values();
        System.assertEquals(0, [SELECT Id FROM Campaign WHERE parentId in :campaignsMap.keySet()].size());
    }*/
    /*
     * 1. If we insert CampaignMember for parent campaign Need to create same member to all child Campaigns
     * 2. If we Delete CampaignMember for parent campaign Need to delete same member to all child Campaigns
	*/
    /*static testMethod void checkCampaignMemberInsertAndDelete() {
        
        Map<Id,Campaign> parentCampMap = new Map<Id,Campaign>([SELECT Id from Campaign limit 3]);
        
        List<Campaign_Company__c> campCompanies = new List<Campaign_Company__c>();
        List<Account> accounts = Database.query(AccountQuery + '  LIMIT 5');
        for(Campaign campaignObj : parentCampMap.values()){
            for(Account acc : accounts){
                campCompanies.add(new Campaign_Company__c(Campaign__c = campaignObj.Id,Company__c = acc.Id));    
            }
        }
        insert campCompanies;
        
        Map<Id,Campaign> childCampsMap = new Map<Id,Campaign>([SELECT Id from Campaign WHERE parentId in :parentCampMap.keySet()]);
        Integer allReadyCampMemberCount =  [SELECT Id from CampaignMember WHERE campaignId in :childCampsMap.keySet()].size(); 
        
        List<CampaignMember> campMemberList = new List<CampaignMember>();
        List<Contact> conList = new List<Contact>();
        List<Account> accList = Database.query(AccountQuery);
        for(integer i=0; i<numberOfContacts; i++){
            conList.add(new Contact(FirstName = 'Hello'+i,LastName = 'Teset'+i, AccountId = accList[i].Id));
        }
        checkRecursive.runOnce();
        insert conList;
        
        for(Campaign campaignObj : parentCampMap.values()){
            for(integer i=0; i<numberOfContacts; i++){
                campMemberList.add(new CampaignMember(Status = (i < 2 ? 'Follow up 1' : 'Attended'),campaignId = campaignObj.Id,contactId = conList[i].Id));
            }
        }
        Test.startTest();	
       	CampaignCompanyAutomation.AllowChildCampaignAndMemberUpdate = false;
        insert campMemberList;
        system.assertEquals(numberOfContacts * childCampsMap.size() + allReadyCampMemberCount, [SELECT Id from CampaignMember WHERE campaignId in :childCampsMap.keySet()].size());
        
        delete campMemberList;
        system.assertEquals(allReadyCampMemberCount, [SELECT Id from CampaignMember WHERE campaignId in :childCampsMap.keySet()].size());
        
        Test.stopTest();
    }*/
    /* When ever parent campaign's member update we should update child campaign's member as well*/
	/*static testMethod void checkCampaignMemberUpdate() {
        Test.startTest();
        
        Map<Id,Campaign> parentCampMap = new Map<Id,Campaign>([SELECT Id FROM Campaign LIMIT 3]);
        List<Account> accounts = Database.query(AccountQuery + '  LIMIT 5');
        List<Campaign_Company__c> campCompanies = new List<Campaign_Company__c>();
        for(Campaign campaignObj : parentCampMap.values()){
            for(Account acc : accounts){
                campCompanies.add(new Campaign_Company__c(Campaign__c = campaignObj.Id,Company__c = acc.Id));    
            }
        }
        insert campCompanies;
        
        Map<Id,Campaign> childCampsMap = new Map<Id,Campaign>([SELECT Id from Campaign WHERE parentId in :parentCampMap.keySet()]);
        List<CampaignMember> campMemberList = [SELECT Id from CampaignMember WHERE campaignId in :parentCampMap.keySet()];
        for(CampaignMember campMember : campMemberList){
            campMember.Status = 'Responded';
        }
        CampaignCompanyAutomation.AllowChildCampaignAndMemberUpdate = false;
        update campMemberList;
        //system.assertEquals((parentCampMap.size()*numberOfContacts)*accounts.size(), [SELECT Id from CampaignMember WHERE campaignId in :childCampsMap.keySet() AND Status = 'Responded'].size());
        Test.stopTest();
        
    }*/

}