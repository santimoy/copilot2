public with sharing class JustForYouSignals_Apex {
    public class IVPWatchDogCtlrException extends Exception {
    }

    public class InputData {
        public String viewId;
        public Boolean modifyMetaData;
        public Boolean init;
        public List<String> selClrsOps;
        public List<String> selCatsOps;
        public String preferenceId;
    }

    public class IVPWDData {
        @AuraEnabled
        public HerokuData signals;
        @AuraEnabled
        public Map<String, List<String>> filters;
        @AuraEnabled
        public List<Signal_Category__c> categories;

        public IVPWDData() {
            signals = new HerokuData();
            filters = new Map<String, List<String>> ();
        }
    }

    private static HerokuData fetchCompanySignals(List<String> categories, String PreferecneID,Boolean isLimit) {
        List<Just_For_You_Setting__c> JustForYouSettingList = [SELECT Signals_Last_N_Days__c, Records_Limit__c, Fields_List__c, 
                                                                JFY_Companies_Last_N_Days__c
                                                                FROM Just_For_You_Setting__c LIMIT 1];

        
        String TypeString = 'universe';
        String signal_days_requested = '30';
        String JFY_Companies_Last_N_Days = '30';
        String recordLimit = '200';
        List<String> fieldList = new List<String>{
                'id',
                'sf_id',
                'signal_date',
                'tag_name',
                'headline',
                'snippets',
                'source',
                'url',
                'df_id',
                'website',
                'ct_id',
                'ivp_name'
        };
        if(!JustForYouSettingList.isEmpty()){
            if(String.valueOf(JustForYouSettingList[0].JFY_Companies_Last_N_Days__c) != null) {
                JFY_Companies_Last_N_Days = String.valueOf(JustForYouSettingList[0].JFY_Companies_Last_N_Days__c);
            }
            if(String.valueOf(JustForYouSettingList[0].Signals_Last_N_Days__c) != null) {
                signal_days_requested = String.valueOf(JustForYouSettingList[0].Signals_Last_N_Days__c);
            }
            if(String.valueOf(JustForYouSettingList[0].Records_Limit__c) != null) {
                recordLimit = String.valueOf(JustForYouSettingList[0].Records_Limit__c);
            }
            if(String.valueOf(JustForYouSettingList[0].Fields_List__c) != null) {
                String AllFieldsString = String.valueOf(JustForYouSettingList[0].Fields_List__c);
                fieldList = AllFieldsString.split(',');
            }
        }

        if(String.valueOf(PreferecneID) != null) {
            TypeString = 'Preference';
        } else {
            // what we need to use here are now using snehil's user Id
            PreferecneID = Userinfo.getUserId();
        }

        //If isLimit variable is true
        System.debug(LoggingLevel.ERROR,'signal_days_requested>>>: '+signal_days_requested);
        System.debug(LoggingLevel.ERROR,'categories>>>: '+categories);

        Set<String> tempSet = new Set<String>();
        tempSet.addAll(fieldList);

        HerokuData HerokuDataWrapper = JustForYouSignals_Apex.fetchHerokuData(tempSet, recordLimit, signal_days_requested, categories, JFY_Companies_Last_N_Days, TypeString, PreferecneID);
        System.debug(LoggingLevel.ERROR, 'HerokuDataWrapper>>>>: ' + HerokuDataWrapper);
        return HerokuDataWrapper;

    }

    public static HerokuData fetchHerokuData(Set<String> fieldList, String limits, String signal_days_requested, List<String> categories, String JFY_Companies_Last_N_Days, String TypeString, String PreferecneID) {
        Logger.push('fetchHerokuData','JustForYouSignals_Apex');
        if(categories==null){
            categories = new list<String>();
        }
        
        HttpRequest req = setHttpRequest(limits, fieldList, signal_days_requested, categories, JFY_Companies_Last_N_Days, TypeString, PreferecneID);
        List<Sourcing_Preference__c> SourcingPreferenceList = JustForYouCompaniesController.getSourcing();
        HerokuData hd = new HerokuData();
        if(SourcingPreferenceList.size() > 0) {
            Http http = new Http();
            HTTPResponse res;
            String result = '';
            System.debug('body>'+ req.getBody());
            try{
                res = http.send(req);
                if(res.getStatusCode() == 200) {
                    result = res.getBody();
                    result = result.replaceAll('null,', '\"\",');
                } else {
                    result = '{"errorMessage":\"API Error '+String.escapeSingleQuotes(res.getStatus()) +'\"}';
                }
            }catch(Exception excp){
                Datetime start = system.now();
                Map<String, Object> data = new Map<String, object>{
                    'body'=>req.getBody(),
                    'method'=>req.getMethod(),
                    'endpoint'=>req.getEndpoint(),
                    'message'=>excp.getMessage()
                };
                Logger.debugException(JSON.serialize(data));
                //Logger.debugException(excp);
                Logger.pop();
                Datetime ends = system.now();
                List<Log__c> logs = [SELECT Id,Name FROM Log__c
                                        WHERE CreatedById=:UserInfo.getUserId()
                                        AND CreatedDate>=:start AND CreatedDate<=:ends];

                result = System.Label.DWH_API_Time_Out ;
                if(!logs.isEmpty()){
                    result ='refId: '+String.escapeSingleQuotes(logs[0].Name)+',' +result;
                }
                hd.errorInfo = result;
                return hd;
            }
            try{
                hd = parse(result);
            }catch(Exception excp){
                hd.errorInfo = excp.getMessage();
            }
        }
        return hd;
    }

    public static HttpRequest setHttpRequest(String limits, Set<String> fieldList, String signal_days_requested, List<String> categories, String JFY_Companies_Last_N_Days, String TypeString, String PreferecneID) {
        HttpRequest req = new HttpRequest();
        BodyWrapper bw = new BodyWrapper();
        bw.limits = limits;
        bw.fields = fieldList;
        bw.id = PreferecneID;
        bw.type = TypeString;
        bw.companies_days_requested = JFY_Companies_Last_N_Days;
        bw.signal_days_requested = signal_days_requested;
        //bw.signal_days_requested = days_requested;
        bw.categories = new List<String>();
        for(String cat:categories){
            bw.categories.add(cat.trim());
        }
        bw.categories.sort();
        
        String endPoint = PreferenceCenterService.getPreferenceCenter().Preference_Center_Endpoint__c;
        endPoint += '/preference_user/'+ Userinfo.getUserId()+ '/signals';
        System.debug(LoggingLevel.ERROR, 'endPoint>>>: ' + endPoint);
        String body = JSON.serialize(bw);
        System.debug('body ' +body);
        body = body.replace('limits', 'limit');
        req.setEndpoint(endPoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        // need to keep max timeout is 30 sec
        req.setTimeout(120000);
        req.setBody(body);
        return req;
    }

    public class BodyWrapper {
        public Set<String> fields;
        public String order_by;
        public String limits;
        public String id;
        public String type;
        public String companies_days_requested;
        public String signal_days_requested;
        public List<String> categories;
    }

    public static HerokuData parse(String json) {
        return (HerokuData) System.JSON.deserialize(json, HerokuData.class);
    }

    public class HerokuData {
        @AuraEnabled public Integer total_pages { get; set; }
        @AuraEnabled public Boolean additional_data { get; set; }
        @AuraEnabled public String host { get; set; }
        @AuraEnabled public List<List<String>> data { get; set; }
        @AuraEnabled public List<String> data_fields { get; set; }
        @AuraEnabled public Response_time response_time { get; set; }
        @AuraEnabled public String sql_statement { get; set; }
        @AuraEnabled public Integer total_entries { get; set; }
        @AuraEnabled public String dialect { get; set; }
        @AuraEnabled public Set<String> fieldsLabel { get; set; }
        @AuraEnabled public Set<String> fieldsApi { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public Boolean apiException{get;set;}
        @AuraEnabled public String refId{get;set;}
        @AuraEnabled public String errorInfo{get;set;}
        @AuraEnabled public String error{get;set;}
        @AuraEnabled public apiResponse apiResponse {get;set;}
        public HerokuData(){
            apiException = false;
        }
    }

    public class fieldClass {
        @AuraEnabled public Set<String> fLabelList { get; set; }
        @AuraEnabled public Set<String> fApiList { get; set; }
    }

    public class Response_time {
        @AuraEnabled Integer query_execution { get; set; }
        @AuraEnabled Integer count_query_execution { get; set; }
        @AuraEnabled Integer total_execution_time { get; set; }
    }

    public class apiResponse {
        @AuraEnabled List<String> data_fields { get; set; }
    }

    @AuraEnabled
    public static IVPWDData getInitData(String inputJson) {
        Map<String, Object> inputMap = (Map<String, Object>) Json.deserializeUntyped(inputJson);
        Boolean isLimit = (Boolean) inputMap.get('isLimit');
System.debug('inputMap'+inputMap);
        HerokuData companySignalDataLst = new HerokuData();
        IVP_SD_ListView__c currentUserSetting = IVPUtils.currentUserSetting();
        Map<String, List<String> > filters = getWDFilters();
        List<String> cats = filters.get('myDashSignalPreferences');
        // gettting data using API
        companySignalDataLst = fetchCompanySignals(cats, null, isLimit);
        
        List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
        SignalCategoryList = [SELECT External_Id__c, Color_HEX__c, Image_URL__c  FROM Signal_Category__c WHERE External_Id__c != null];
        System.debug(LoggingLevel.ERROR,'SignalCategoryList>>>: '+SignalCategoryList);

        IVPWDData IVPWDDataObj = new IVPWDData();
        IVPWDDataObj.signals = companySignalDataLst;
        IVPWDDataObj.filters = filters;
        IVPWDDataObj.categories = SignalCategoryList;
        return IVPWDDataObj;
    }

    /*******************************************************************************************************
     * @description returning List of IVPWDData for specified list-view whose Id is being passed in the parameter
     * @return IVPWDData
     * @author Pritam
     * @param String inputJson	JSON value of input parameters
     * @note Don't change value of this property.
     */

    @AuraEnabled
    public static IVPWDData getCompanySignals(String inputJson) {

        InputData inputValues = (InputData) JSON.deserialize(inputJson, InputData.class);
        System.debug(LoggingLevel.ERROR,'234 inputValues>>>>: '+inputValues);
        HerokuData companySignalDataLst = new HerokuData();
        List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();

        if (inputValues.init != NULL && inputValues.init) {
            Map<String, List<String>> filters = getWDFilters();
        }
        System.debug(inputValues.selCatsOps);
        companySignalDataLst = fetchCompanySignals(inputValues.selCatsOps, inputValues.preferenceId, false);
        SignalCategoryList = [SELECT External_Id__c, Color_HEX__c, Image_URL__c FROM Signal_Category__c
        WHERE External_Id__c != null];
        System.debug(LoggingLevel.ERROR,'SignalCategoryList>>>: '+SignalCategoryList);

        IVPWDData data = new IVPWDData();
        data.signals = companySignalDataLst;
        data.categories = SignalCategoryList;
        return data;
    }

    @AuraEnabled
    public static Map<String, List<String>> getWDFilters() {
        Map<String, List<String>> wdFilterMap = new Map<String, List<String>>{
                'colours' => new List<String>(),
                /*'categories'=>new List<String>(),*/
                'myDashSignalPreferences' => new List<String>()
        };
        List<Intelligence_User_Preferences__c> uPres = [
                SELECT Id, Signal_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c, User_Id__c
                FROM Intelligence_User_Preferences__c
                WHERE User_Id__c = :UserInfo.getUserId()
        ];
        if(uPres.isEmpty()) {
            List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
            SignalCategoryList = [SELECT External_Id__c, Do_Not_Display__c FROM Signal_Category__c WHERE External_Id__c != null];
            String sourcingSignalStr;
            if(! SignalCategoryList.isEmpty()) {
                for (Signal_Category__c singleCategory : SignalCategoryList) {
                    if (singleCategory.External_Id__c != null) {
                        if (sourcingSignalStr != null) {
                            sourcingSignalStr = sourcingSignalStr + ',' + singleCategory.External_Id__c;
                        } else {
                            sourcingSignalStr = singleCategory.External_Id__c;
                        }
                    }
                }
            }
            Intelligence_User_Preferences__c IntelligenceUser = new Intelligence_User_Preferences__c();
            IntelligenceUser.Name = UserInfo.getName();
            IntelligenceUser.User_Id__c = UserInfo.getUserId();
            if(sourcingSignalStr != null){
                IntelligenceUser.Sourcing_Signal_Category_Preferences__c = sourcingSignalStr;
                IntelligenceUser.My_Dash_Signal_Preferences__c = sourcingSignalStr;
            }
            if(!Test.isRunningTest()){
                insert IntelligenceUser;
            }
            List<Intelligence_User_Preferences__c> uPresNewUser = [
                    SELECT Id, Signal_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c
                    FROM Intelligence_User_Preferences__c
                    WHERE User_Id__c = :UserInfo.getUserId()
            ];
            uPres.addAll(uPresNewUser);
        } else {
            List<Signal_Category__c> SignalCategoryList = new List<Signal_Category__c>();
            SignalCategoryList = [SELECT External_Id__c, Do_Not_display__c FROM Signal_Category__c WHERE External_Id__c != null];
            Set<String> defaults = new Set<String>();
            for (Signal_Category__c singleCategory : SignalCategoryList) {
                if(singleCategory.Do_Not_display__c == true){
                    defaults.add(singleCategory.External_Id__c);
                }
            }
            if (String.isEmpty(uPres[0].My_Dash_Signal_Preferences__c) || String.isEmpty(uPres[0].Sourcing_Signal_Category_Preferences__c)) {
                String sourcingSignalStr;
                if(! SignalCategoryList.isEmpty()) {
                    for (Signal_Category__c singleCategory : SignalCategoryList) {
                        if (singleCategory.External_Id__c != null) {
                            if (sourcingSignalStr != null) {
                                sourcingSignalStr = sourcingSignalStr + ',' + singleCategory.External_Id__c;
                            } else {
                                sourcingSignalStr = singleCategory.External_Id__c;
                            }
                        }
                    }
                }
                if(sourcingSignalStr != null){
                    Intelligence_User_Preferences__c IntelligenceUser = new Intelligence_User_Preferences__c();
                    IntelligenceUser.Id = uPres[0].Id;
                    IntelligenceUser.User_Id__c = uPres[0].User_Id__c;
                    if (String.isEmpty(uPres[0].My_Dash_Signal_Preferences__c)) {
                        IntelligenceUser.My_Dash_Signal_Preferences__c = sourcingSignalStr;
                    } else {
                        IntelligenceUser.My_Dash_Signal_Preferences__c = uPres[0].My_Dash_Signal_Preferences__c;
                    }
                    if (String.isEmpty(uPres[0].Sourcing_Signal_Category_Preferences__c)) {
                        IntelligenceUser.Sourcing_Signal_Category_Preferences__c = sourcingSignalStr;
                    } else {
                        IntelligenceUser.Sourcing_Signal_Category_Preferences__c = uPres[0].Sourcing_Signal_Category_Preferences__c;
                    }
                    upsert IntelligenceUser;
                }
                
                uPres.clear();
                List<Intelligence_User_Preferences__c> uPresNewUser = [
                        SELECT Id, Signal_Filter_Colour__c, WD_Filter_Category__c, My_Dash_Signal_Preferences__c, Sourcing_Signal_Category_Preferences__c
                        FROM Intelligence_User_Preferences__c
                        WHERE User_Id__c = :UserInfo.getUserId()
                ];
                uPres.addAll(uPresNewUser);
            }
            for(String signal: defaults){
                uPres[0].Sourcing_Signal_Category_Preferences__c = String.isNotEmpty(uPres[0].Sourcing_Signal_Category_Preferences__c) ? uPres[0].Sourcing_Signal_Category_Preferences__c :'';
                if(!uPres[0].Sourcing_Signal_Category_Preferences__c.contains(signal)){
                    uPres[0].Sourcing_Signal_Category_Preferences__c+=','+signal;
                }
                /* we don't need to add these into SmartDash signals
                uPres[0].My_Dash_Signal_Preferences__c = String.isNotEmpty(uPres[0].My_Dash_Signal_Preferences__c) ? uPres[0].My_Dash_Signal_Preferences__c :'';
                if(!uPres[0].My_Dash_Signal_Preferences__c.contains(signal)){
                    uPres[0].My_Dash_Signal_Preferences__c+=','+signal;
                }
                */
            }
        }
        if (!uPres.isEmpty()) {
            if (String.isNotBlank(uPres[0].Signal_Filter_Colour__c)) {
                wdFilterMap.put('colours', uPres[0].Signal_Filter_Colour__c.split(';'));
            }
            if (String.isNotBlank(uPres[0].Sourcing_Signal_Category_Preferences__c)) {
                wdFilterMap.put('myDashSignalPreferences', uPres[0].Sourcing_Signal_Category_Preferences__c.split(','));
            }
        }
        return wdFilterMap;
    }

    @AuraEnabled
    public static void setIVPSDSettings(String inputJson) {
        try {
            InputData inputValues = (InputData) JSON.deserialize(inputJson, InputData.class);

            if (inputValues.modifyMetaData != NULL && inputValues.modifyMetaData) {
                setWDFilters(inputValues.selClrsOps, inputValues.selCatsOps);
            }
        } catch (Exception ex) {
            System.debug(ex.getLineNumber());
            System.debug(ex.getLineNumber());
            System.debug(ex.getStackTraceString());
        }
    }

    @AuraEnabled
    public static void setWDFilters(List<String> colours, List<String> cats) {
        List<Intelligence_User_Preferences__c> IntelligenceUserPreferencesList = new List<Intelligence_User_Preferences__c>();
        IntelligenceUserPreferencesList = [
                SELECT Id, WD_Filter_Category__c, Signal_Filter_Colour__c
                FROM Intelligence_User_Preferences__c
                WHERE User_Id__c = :UserInfo.getUserId()
        ];
        Intelligence_User_Preferences__c usrPreference;
        if(IntelligenceUserPreferencesList.size() > 0) {
            for (Intelligence_User_Preferences__c uPre : IntelligenceUserPreferencesList) {
                usrPreference = uPre;
            }
        }
        if (usrPreference == null) {
            usrPreference = new Intelligence_User_Preferences__c(User_Id__c = UserInfo.getUserId(), Name = 'Smart Dash');
        }
        usrPreference.Signal_Filter_Colour__c = String.join(colours, ';');
        //usrPreference.WD_Filter_Category__c = String.join(cats, ';');
        upsert usrPreference;
    }
}