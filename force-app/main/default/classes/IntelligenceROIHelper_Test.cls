@isTest
private class IntelligenceROIHelper_Test {
	
    @testSetup
    public static void createTestData(){
        
        List<Account> lstAccount = new List<Account>{
            new Account(Name = 'Test Account 1', DWH_Name__c = 'Test DWH Name 1', KCName__c = 'Test KCName 1', Fax = 'Test Fax 1', Phone = '9709090909', Active__c = true),
            new Account(Name = 'Test Account 2', DWH_Name__c = 'Test DWH Name 2', KCName__c = 'Test KCName 2', Fax = 'Test Fax 2', Phone = '9709090910', Active__c = true)
        };
        insert lstAccount;
        insert new DWH_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Auth_Key__c='GXm03ylnnxALzFSnuoTd1up4qSeTwlge0d158Bmh8WFu9BQU9Cb8sNVlRhzZtQLXug3lozujHYoZlDMatOQIoWiDfL', Endpoint__c='https://dev-dwh-insightpartners-com-b1zp6mpwqb1l.curlhub.io/v1.6');
    }
    
    @isTest
    public static void testCreateIntelligenceROI() {
        
        List<Account> lstAccount = [SELECT Id, Name, DWH_Name__c, KCName__c, Fax, Phone, Active__c, Account_OwnerId__c FROM Account];
        for(integer i = 0; i < lstAccount.size(); i++) {
            IntelligenceROIHelper.createIntelligenceROI(IntelligenceROIHelper.STR_ROI_II_THESIS,
                                                        lstAccount[i].Id,
                                                        IntelligenceROIHelper.STR_ROI_INSIGHT_INTELLIGENCE_PAGE_OPENED,
                                                        '',
                                                        '',
                                                        'Queued',
                                                        '',
                                                        '');
        }
        system.assertEquals(true, [SELECT Id FROM Intelligence_ROI__c].size() != 0);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ROIMockCallout());
        Database.executeBatch(new IntelligenceROIBatch());
        Test.stopTest();
    }

    @isTest
    public static void testIntelligenceROIBatchScheduler() {
        
        List<Account> lstAccount = [SELECT Id, Name, DWH_Name__c, KCName__c, Fax, Phone, Active__c, Account_OwnerId__c FROM Account];
        for(integer i = 0; i < lstAccount.size(); i++) {
            IntelligenceROIHelper.createIntelligenceROI(IntelligenceROIHelper.STR_ROI_II_THESIS,
                                                        lstAccount[i].Id,
                                                        IntelligenceROIHelper.STR_ROI_INSIGHT_INTELLIGENCE_PAGE_OPENED,
                                                        '',
                                                        '',
                                                        'Queued',
                                                        '',
                                                        '');
        }

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ROIMockCallout());
        String jobId = System.schedule('IntelligenceROIBatch_Scheduler',
                                        IntelligenceROIBatch_Scheduler.CRON_EXP, 
                                        new IntelligenceROIBatch_Scheduler());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                                 NextFireTime
                                 FROM CronTrigger 
                                 WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(IntelligenceROIBatch_Scheduler.CRON_EXP, 
                            ct.CronExpression);
        Test.stopTest();
    }
}