/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Krow_Project_Resource_TeamTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Krow_Project_Resource_TeamTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Krow__Project_Resource_Team__c());
    }
}