/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Krow_Project_AssignmentTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Krow_Project_AssignmentTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Krow__Project_Assignment__c());
    }
}