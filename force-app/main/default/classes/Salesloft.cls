/**
 * @author	: 10k-Advisor
 * @name	: Salesloft 
 * @date	: 2018-08-10
 * @description   : The class will contain things related to Salesloft
**/
public class Salesloft{
    // we will replace all the following via Custom Setting values
    public static final String BASE_URL = 'https://api.salesloft.com'; //Base URL
    public static final String PEOPLE_END_POINT = BASE_URL + '/v2/people.json'; // endPoint of People
    public static final String CADENCE_END_POINT = BASE_URL + '/v2/cadences.json'; // endPoint of cadence
    public static final String CADENCE_ID_END_POINT = BASE_URL + '/v2/cadences/{id}.json';// endPoint of particuler cadence
    public static final String CADENCE_MEMBER_END_POINT = BASE_URL + '/v2/cadence_memberships.json'; // endPoint of cadence member
    
    // preparing Salesloft API_KEY with custom setting ang giving error if not found
    public static final String API_KEY{
        get{
            if(API_KEY == NULL){
                API_KEY = '';
                IVP_General_Config__c sfSetting = IVP_General_Config__c.getInstance('salesloft-api-key');
                if(sfSetting!=NULL && String.isNotBlank(sfSetting.value__c)){
                    API_KEY = sfSetting.value__c;
                }
                if(API_KEY==''){
                    throw new SalesloftException('Either Salesloft API_KEY is invalid or not exist in the custom IVP_SD_ListView setting with the same:salesloft-api-key');
                }
            }
            return API_KEY;
        }
        set;
    }
    // preparing Map of service provider with salesloft object name
    public static final Map<String, String> objectWithServiceName = new Map<String, String>{
        'people' => 'SalesloftPeopleSyncService',
        'cadence' => 'SalesloftCadenceSyncService',
        'cadencemember' => 'SalesloftCadenceMemberSyncService'
    };
    
    public class SalesloftException extends Exception{}
    
    /**
     *@description  : extracting endPoint for specified salesloft object name, if objectname not different from among(people, cadence, cadencemember) 
     *                  so throwing exception
     *@input: sfObjName -> that can the name of Salesloft's object like: people, cadence, cadencemember
     *@return-type  : String
    **/
    public static String getEndPoint(String sfObjName){
        String endPoint = '';
        if(String.isNotBlank(sfObjName)){
            sfObjName = sfObjName.toLowerCase();
            switch on sfObjName{
                when 'people'{
                    endPoint = PEOPLE_END_POINT;
                }
                when 'cadence'{
                    endPoint = CADENCE_END_POINT;
                }
                when 'cadencemember'{
                    endPoint = CADENCE_MEMBER_END_POINT;
                }
                when else{
                    endPoint = '';
                }
            }
        }
        if(endPoint == ''){
            throw new SalesloftException('Invalid Salesloft object name: '+sfObjName);
        }
        return endPoint;
    }
    
    /**
     *@description  : returning HttpResponse for specified inputs, in case of Unauthorization throwing error and sending email to users
     *@input: endPoint  -> that can the name of Salesloft's object like: people, cadence, cadencemember
     *      : method    -> API calling method
     *      : params    -> Parameters for API Call
     *@return-type  : HttpResponse
    **/
    public static HttpResponse getAPIResponse(String endPoint, String method, Map<String, String> params){
        
        try{
            HttpResponse response = SalesLoftAPI.getCalloutResponse(endPoint, method,
                            new Map<String, String>{'Authorization' =>'Bearer ' + Salesloft.API_KEY},
                            params,
                            null);
            system.debug('response body:'+response.getBody());
            if(response.getStatusCode() == 401 ){
                // throwing an error in case of Unauthorization
                throw new CalloutException('Something has wrong with Salesloft Syncing process:' + response.getBody());
            }
            return response;

        }catch(Exception ex){
            SalesloftUtility.notifyToUser('Salesloft', ex);
        }
        return null;
    }
    
    // Salesforce Object wrappers as following
    public class PageData{
        public Integer per_page;
        public Integer current_page;
        public Integer next_page;
        public Integer prev_page;
        public Integer total_pages;
        public Integer total_count;
    }
        
    public class Metadata{
        public PageData paging;
    }
    
    public class Owner{
        public String id;
    }
    
    public class People{
        public String id;
        public String email_address;
    }
    
    public class PeopleResponse {
        public Metadata metadata;
        public List<People> data;
    }
    
    public class Cadence{
        public String id;
        public Boolean team_cadence;
        public String name;
        public Owner owner;
    }
    
    public class CadenceResponse {
        public Metadata metadata;
        public List<Cadence> data;
    }
    
    public class CadenceData {
        public Cadence data;
    }
    
    public class CadenceMemberData {
        public CadenceMember data;
    }
    
    public class CadenceMember{
        public String id;
        public Cadence cadence;
        public People person;
    }
    
    public class CadenceMemberResponse {
        public Metadata metadata;
        public List<CadenceMember> data;
    }
    
}