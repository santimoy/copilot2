@isTest
private class WeeklyUpdateTriggerHandlerTest
{
	@isTest
    static void TestHandler()
    {
        Account acc= new Account();
        Date currentDate=Date.today();
        currentDate=currentDate.addDays(-30);
        Common_Config__c cconf = new Common_Config__c(Timesheet_Go_Live__c=(currentDate.addDays(-2000)));
        insert cconf;


        acc.Name='Test';
        insert acc;
        Project__c project= new Project__c();
        project.Company__c=acc.Id;
        project.Billable__c=true;
        project.Start_Date__c=currentDate;
        
        insert project;
        Working_Group_Member__c wm = new Working_Group_Member__c();
        wm.Project__c=project.id;

        wm.Working_Group_Member__c=UserInfo.getUserId();
        wm.Start_Date__c=currentDate;
        insert wm;
        
        Weekly_Update__c wu1 = new Weekly_Update__c();
        
        wu1.Project__c=project.Id;
        wu1.Resource__c=UserInfo.getUserId();
        wu1.Week_Ending__c=Date.newInstance(2015, 8, 8);
        Weekly_Update__c wu2= new Weekly_Update__c();
        
        wu2.Project__c=project.Id;
        wu2.Resource__c=UserInfo.getUserId();
        wu2.Week_Ending__c=Date.newInstance(2015, 8, 15);
        insert wu1;
        insert wu2;

        Id wu2Id=wu2.Id;

        test.StartTest();

        wu1.Snippets__c='This is my text';
        update wu1;
        test.StopTest();

        system.assertEquals('This is my text',wu1.Snippets__c);


    }
	/*
    @isTest
    static void TestSnippetUpdate()
    {
        Account acc= new Account();
        Date currentDate=Date.today();
        currentDate=currentDate.addDays(-30);
        Common_Config__c cconf = new Common_Config__c(Timesheet_Go_Live__c=(currentDate.addDays(-2000)));
        insert cconf;


        acc.Name='Test';
        insert acc;
        Project__c project= new Project__c();
        project.Company__c=acc.Id;
        project.Billable__c=true;
        project.Start_Date__c=currentDate;
        
        insert project;
        Working_Group_Member__c wm = new Working_Group_Member__c();
        wm.Project__c=project.id;

        wm.Working_Group_Member__c=UserInfo.getUserId();
        wm.Start_Date__c=currentDate;
        insert wm;
        
        Weekly_Update__c wu1 = new Weekly_Update__c();
        
        wu1.Project__c=project.Id;
        wu1.Resource__c=UserInfo.getUserId();
        wu1.Week_Ending__c=Date.newInstance(2015, 8, 8);
        Weekly_Update__c wu2= new Weekly_Update__c();
        
        wu2.Project__c=project.Id;
        wu2.Resource__c=UserInfo.getUserId();
        wu2.Week_Ending__c=Date.newInstance(2015, 8, 15);
        insert wu1;
        insert wu2;

        Id wu2Id=wu2.Id;

        test.StartTest();

        wu1.Snippets__c='This is my text';
        update wu1;
        test.StopTest();

        wu2=[select Last_week__c,id from Weekly_Update__c where id=:wu2Id];
        system.assertEquals('This is my text',wu2.Snippets__c);


    }
    */
}