/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Portfolio_Company_CustomersTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Portfolio_Company_CustomersTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Portfolio_Company_Customers__c());
    }
}