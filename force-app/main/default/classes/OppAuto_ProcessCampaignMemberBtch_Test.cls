/**
*@name      	: OppAuto_ProcessCampaignMemberBtch_Test
*@developer 	: 10K developer
*@date      	: 2018-10-19
*@description   : The class used as test class of OppAuto_ProcessCampaignMemberBtch
**/
@isTest
public class OppAuto_ProcessCampaignMemberBtch_Test {
    @testSetup static void testSetup() {
        //Creating custom setting
        List<IVP_General_Config__c> gcLst = new List<IVP_General_Config__c>();
        gcLst.add(new IVP_General_Config__c(Name='OppAuto_UserName',Value__c='alessandro@insightpartners.com'));
        gcLst.add(new IVP_General_Config__c(Name='Campaign Member Status',Value__c='Attended,Follow up 1,Follow up 2,Follow up 3,Call Set,Meeting Set,Connected via Email,No Response,Defer,Declined,Reschedule'));
        insert gcLst;

        //Creating account



    }
    public static testMethod void IB_RT_Case1(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Attended','Attended', 'IB_RT_Case1', false, 'Dinner / Roundtable', 'Open', 'Check with portfolio'
                                       , 0,1, contacts);
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        List<Opportunity> oppsToTest = [SELECT Id,Description,Name,NextStep,StageName FROM Opportunity];
        // System.debug('#TapoppsToTest'+oppsToTest[0].Description);
        // System.assertEquals(1, oppsToTest.size());
        // System.assertEquals('Check with portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(2, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void IB_RT_Case2(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Attended','Declined', 'IB_RT_Case2', false, 'Dinner / Roundtable', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        List<Opportunity> oppsToTest = [SELECT Id FROM Opportunity];

        // System.assertEquals(0, oppsToTest.size());
        // System.assertEquals(0, [SELECT Id FROM OpportunityContactRole].size());
        // System.assertEquals(0, [SELECT Id FROM CampaignInfluence].size());
    }
    public static testMethod void IB_RT_Case3(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Attended', 'IB_RT_Case3', false, 'Dinner / Roundtable', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);

        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        List<Opportunity> oppsToTest = [SELECT Id FROM Opportunity];

        // System.assertEquals(0, oppsToTest.size());
        // System.assertEquals(0, [SELECT Id FROM OpportunityContactRole].size());
        // System.assertEquals(0, [SELECT Id FROM CampaignInfluence].size());
    }
    public static testMethod void IB_RT_Case4(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'IB_RT_Case4', false, 'Dinner / Roundtable', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        List<Opportunity> oppsToTest = [SELECT Id FROM Opportunity];

        // System.assertEquals(0, oppsToTest.size());
        // System.assertEquals(0, [SELECT Id FROM OpportunityContactRole].size());
        // System.assertEquals(0, [SELECT Id FROM CampaignInfluence].size());
    }
    public static testMethod void IB_RT_Case5(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Attended','Attended', 'IB_RT_Case5', true, 'Dinner / Roundtable', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        System.debug('oppsToTest====' + oppsToTest);
        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // System.assertEquals('Attended '+ campObj.Name + ' ' +campObj.StartDate.format(), oppsToTest[0].Description);
        // System.assertEquals('Check with portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(2, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void IB_RT_Case6(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Attended','Attended', 'IB_RT_Case6', true, 'Dinner / Roundtable', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);



        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        OpportunityContactRole oppContactRole = new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id);
        insert oppContactRole;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        // System.assertEquals(1, oppsToTest.size(), oppsToTest +'');
        // System.assertEquals('Attended '+ campObj.Name + ' ' +campObj.StartDate.format(), oppsToTest[0].Description);
        // System.assertEquals('Check with portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(2, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void IB_RT_Case7(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Attended','Attended', 'IB_RT_Case7', true, 'Dinner / Roundtable', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Portfolio Company - Director'
                                                                   , ContactId = contacts[0].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        System.debug('oppsToTest===' + oppsToTest);
        // System.assertEquals(1, oppsToTest.size(), oppsToTest +'');
        // System.assertEquals('Attended '+ campObj.Name + ' ' +campObj.StartDate.format(), oppsToTest[0].Description);
        // System.assertEquals('Check with portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(2, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case1_2(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Follow up 1','Follow up 1', 'DG_Case1_2', false, 'Portfolio Company Demand Gen', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];


        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        //System.assertEquals(1, oppsToTest.size());
        // System.assertEquals('Lead', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case3(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Call Set','Call Set', 'DG_Case3', false, 'Portfolio Company Demand Gen', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];


        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        // System.assertEquals(1, oppsToTest.size());
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case4(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case4', false, 'Portfolio Company Demand Gen', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        //System.assertEquals(1, oppsToTest.size());
        //System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case5(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case5', true, 'Portfolio Company Demand Gen', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        System.debug('oppsToTest===' + oppsToTest);
        // System.assertEquals(1, oppsToTest.size());
        // //System.assertEquals('Declined '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Re-engage or find new target', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Stuck', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case6(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case6', true, 'Portfolio Company Demand Gen', 'Open'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(), oppsToTest +'');
        // //System.assertEquals('Declined '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Re-engage or find new target', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Stuck', oppsToTest[0].StageName);
        // System.debug([SELECT Id,Role,ContactId FROM OpportunityContactRole]);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case7(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Follow up 1','Follow up 1', 'DG_Case7', true, 'Portfolio Company Demand Gen', 'Ignite Qualified'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // //System.assertEquals('Follow up 1 '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Set a Call', oppsToTest[0].NextStep);
        // System.assertEquals('Lead', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case8(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Meeting Set','Meeting Set', 'DG_Case8', true, 'Portfolio Company Demand Gen'
                                       , 'Ignite Qualified', 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // System.assertEquals('Meeting Set '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Check with Portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case9(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case9', true, 'Portfolio Company Demand Gen', 'Ignite Qualified'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];


        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(), oppsToTest +'');
        // //System.assertEquals('Declined '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Re-engage or find new target', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Stuck', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case10(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Follow up 1','Follow up 1', 'DG_Case10', true, 'Portfolio Company Demand Gen'
                                       , 'Lead', 'Set a Call', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // //System.assertEquals('Follow up 1 '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Set a Call', oppsToTest[0].NextStep);
        // System.assertEquals('Lead', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case11(){
		List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Meeting Set','Meeting Set', 'DG_Case11', true, 'Portfolio Company Demand Gen', 'Lead'
                                       , 'Set a Call', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // System.assertEquals('Meeting Set '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Check with Portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case12(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case12', true, 'Portfolio Company Demand Gen', 'Lead'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // //System.assertEquals('Declined '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Re-engage or find new target', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Stuck', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case13(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Follow up 1','Follow up 1', 'DG_Case13', true, 'Portfolio Company Demand Gen'
                                       , 'Ignite Stuck', 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // //System.assertEquals('Follow up 1 '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Set a Call', oppsToTest[0].NextStep);
        // System.assertEquals('Lead', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case14(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Meeting Set','Meeting Set', 'DG_Case14', true, 'Portfolio Company Demand Gen', 'Ignite Stuck'
                                       , 'Check with portfolio', 0,1, contacts);
        List<Contact> conLst = [SELECT Id,AccountId FROM Contact];

        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // System.assertEquals('Meeting Set '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Check with Portfolio', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Qualified', oppsToTest[0].StageName);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case15(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case15', true, 'Portfolio Company Demand Gen', 'Ignite Stuck'
                                       , 'Re-engage or find new target', 0,1, contacts);
        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();

        oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        // System.debug('oppsToTest===' + oppsToTest);
        // System.assertEquals(1, oppsToTest.size(),oppsToTest +'');
        // //System.assertEquals('Declined '+System.today().format(), oppsToTest[0].Description);
        // System.assertEquals('Re-engage or find new target', oppsToTest[0].NextStep);
        // System.assertEquals('Ignite Stuck', oppsToTest[0].StageName);
        // System.debug('====>' + [SELECT Id,ContactId FROM OpportunityContactRole]);
        // System.assertEquals(1, [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppsToTest[0].Id].size());
        // System.assertEquals(1, [SELECT Id FROM CampaignInfluence WHERE OpportunityId = :oppsToTest[0].Id].size());
    }
    public static testMethod void DG_Case16(){

        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Attended','Attended', 'IB_RT_Case1', false, 'Portfolio Company Demand Gen', 'Open', 'Check with portfolio'
                                       , 0,1, contacts);
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(''));
        Test.stopTest();
    }
    public static testMethod void influencedOpportunityTest(){
        List<Contact> contacts = new List<Contact>();
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case15', true, 'Portfolio Company Demand Gen', 'Ignite Stuck'
                                       , 'Re-engage or find new target', 0,1, contacts);
        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];
        List<OpportunityContactRole> oppContactRoles = new List<OpportunityContactRole>();
        oppContactRoles.add(new OpportunityContactRole(OpportunityId = oppsToTest[0].Id
                                                                   , Role = 'Target-Director'
                                                                   , ContactId = contacts[1].Id));
        insert oppContactRoles;
        Test.startTest();
        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        //System.assertEquals(1, [SELECT Id,Influenced_Opportunities__c FROM Campaign WHERE Id = :campObj.Id][0].Influenced_Opportunities__c);
    }
    public static testMethod void callConstructors(){
        Test.startTest();
        	List<Contact> contacts = new List<Contact>();
        	Campaign campObj = prepareData('Declined','Declined', 'DG_Case15', true, 'Portfolio Company Demand Gen', 'Ignite Stuck'
                                       		, 'Re-engage or find new target', 0,1, contacts);
        	OppAuto_ProcessCampaignMemberBtch processCampBatch = new OppAuto_ProcessCampaignMemberBtch(true);
            OppAuto_ProcessCampaignMemberBtch sh1 = new OppAuto_ProcessCampaignMemberBtch();
            //String sch = '0 0 2 * * ?';

		Test.stopTest();

    }
	public static testMethod void insightIgniteTest(){
        Test.startTest();
        List<Contact> contacts = new List<Contact>();
        Account acc = new Account(Name = 'Insight Ignite');
        insert acc;
        Campaign campObj = prepareData('Declined','Declined', 'DG_Case15', false, 'Portfolio Company Demand Gen', 'Ignite Stuck'
                                       , 'Re-engage or find new target', 0,1, contacts);
        campObj.Portfolio_Company__c = acc.Id;
        update campObj;
        List<Opportunity> oppsToTest = [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity];

        Database.executeBatch(new OppAuto_ProcessCampaignMemberBtch(campObj.Id));
        Test.stopTest();
        //System.assertEquals(0, [SELECT Name,OppAuto_Opportunity_New_Name__c,Id, NextStep, StageName, Description FROM Opportunity].size());
    }
    public static Campaign prepareData(String portStatus, String excutiveStatus, String campName, Boolean isCreateOpp
                                        , String campType, String stageName, String nextStep
                                        , integer sourceIdx, integer destIdx, List<Contact> contacts){

		List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < 2; i++){
            accounts.add(new Account(Name = campName+'Test' + i));
        }
        insert accounts;

        //Creating contact

        for(Integer i = 0; i < 2; i++){
            contacts.add(new Contact(FirstName = campName+'Test'+i,LastName = campName+'Test'+i,AccountId = accounts[i].Id));
        }
        insert contacts;


        List<Campaign> campLst = new List<Campaign>();
		Campaign campObj = new Campaign(Name = campName, IsActive = TRUE, Type=campType, StartDate=System.today());

		if(campType == 'Portfolio Company Demand Gen'){
        	campObj.Portfolio_Company__c = contacts[sourceIdx].AccountId;
		}
		campLst.add(campObj);
        insert campLst;

        if(isCreateOpp){
            Id RecordTypeIdContact = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ignite Opportunity').getRecordTypeId();
            Opportunity oppObj = new Opportunity(Description= 'Attended Test Campaign1 11/12/2018', StageName=stageName, NextStep = nextStep
                                                 , Name=contacts[sourceIdx].Account.Name+' & '+ contacts[destIdx].Account.Name + ' 11/12/2018',RecordTypeId = RecordTypeIdContact, Target_Company__c = contacts[destIdx].AccountId
                                                 , AccountId = contacts[sourceIdx].AccountId, Amount=0, CloseDate=System.today(), CampaignId = campLst[0].Id);
            insert oppObj;
        }



        List<CampaignMemberStatus> campMemberStatus = new List<CampaignMemberStatus>();
        campMemberStatus.add(new CampaignMemberStatus(CampaignId=campLst[0].Id, HasResponded=true, Label='Attended'));
        campMemberStatus.add(new CampaignMemberStatus(CampaignId=campLst[0].Id, HasResponded=true, Label='Declined'));
        campMemberStatus.add(new CampaignMemberStatus(CampaignId=campLst[0].Id, HasResponded=true, Label='Follow up 1'));
        campMemberStatus.add(new CampaignMemberStatus(CampaignId=campLst[0].Id, HasResponded=true, Label='Call Set'));
        campMemberStatus.add(new CampaignMemberStatus(CampaignId=campLst[0].Id, HasResponded=true, Label='Meeting Set'));
        insert campMemberStatus;

        List<CampaignMember> campMemLst = new List<CampaignMember>();
        campMemLst.add(new CampaignMember(Status = portStatus, CampaignId = campLst[0].Id, ContactId = contacts[destIdx].Id, Attendee_Type__c = 'Executive Guest'));

        campMemLst.add(new CampaignMember(Status = excutiveStatus, CampaignId = campLst[0].Id, ContactId = contacts[sourceIdx].Id, Attendee_Type__c = 'Portfolio Executive'));
        insert campMemLst;

        return campLst[0];
    }

}