public class SalesloftUtility {
    
    public static Boolean checkSyncingRunning(){
        IVP_General_Config__c sfSetting = IVP_General_Config__c.getInstance('salesloft-job-running');
        if(sfSetting!=NULL && String.isNotBlank(sfSetting.value__c)){
            return sfSetting.value__c.toLowerCase().trim()=='true';
        }
        return false;
    }
    
    public static void prepareLastSyncDate(String startSyncDateTime){
        prepareIVPSetting('salesloft-last-sync',startSyncDateTime);
    }
    public static void prepareStartSyncDate(){
        prepareIVPSetting('salesloft-job-running', 'true');
    }
    public static void prepareIVPSetting(String settingName, String settingValue){
        If(String.isNotBlank(settingValue)){
            IVP_General_Config__c sfSetting = IVP_General_Config__c.getInstance(settingName);
            if(sfSetting == NULL){
                sfSetting = new IVP_General_Config__c(Name=settingName);
            }
            sfSetting.value__c = settingValue;
            upsert sfSetting;
        }
    }
    
    public static String extractLastSyncDate(){
        IVP_General_Config__c sfSetting = IVP_General_Config__c.getInstance('salesloft-last-sync');
        if(sfSetting!=NULL && String.isNotBlank(sfSetting.value__c)){
            return sfSetting.value__c;
        }
        return '';
    }
    
    static public void notifyToUser(String controllerName, Exception excp){
        SalesloftUtility.removeSetting(new Set<String>{'salesloft-job-running'});
        String emailIds = Label.IVP_User_Email_For_Salesloft_Notification;
        if(emailIds.toLowerCase() == '--none--'){
            return;
        }
        // if email Id(s) specified in custom setting comma separated then sending email
        List<String> toAddresses = emailIds.split(',',10);
        String htmlBody = 'Hello Team,'
                        + '<br/> Exception occurs in ' + controllerName + ' controller'
                        + '<br/> Exception is: ' + excp.getMessage()
                        + '<br/><br/> Exception StackTrace as follow:<br/>' + excp.getStackTraceString()
                        + '<br/><br/>Thanks <br/>IVP Developer Team';
        sendEmails(toAddresses, 'IVP Salesloft Syncing Exception',htmlBody);
    }
    /**
     * method will send an email based on the parameters values
    **/ 
    static private void sendEmails(List<String> toAddresses, String subject, String htmlBody){
        if(!toAddresses.isEmpty()){
            //Sending Mail
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
            
            // Assign the addresses for the To 
            mail.setToAddresses(toAddresses);
            
            //Email's subject 
            mail.setSubject(subject);
            
            //Body of email
            mail.setHtmlBody(htmlBody);
            
            //Text Body of email
            mail.setPlainTextBody(htmlBody.stripHtmlTags());
            
            //Sending the email
            try{
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                System.debug('email has been sent');
            }catch(Exception excp){
                // we should remove reset custom settings as well.
                System.debug(excp.getStackTraceString());
            }
        }
    }
    /**
     * method is used to remove value of custom setting
    **/
    static public void removeSetting(Set<String> configNames){
        List<IVP_General_Config__c> settings = [SELECT Id FROM IVP_General_Config__c WHERE Name in:configNames];
        if(!settings.isEmpty())
            DELETE settings;
    }
}