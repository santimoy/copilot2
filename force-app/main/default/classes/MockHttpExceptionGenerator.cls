@isTest
global class MockHttpExceptionGenerator implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if(req.getEndpoint().contains('/preferences')){
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Read Timed Out');
            throw e;
        }else if(req.getEndpoint().contains('/investors/search')){
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Read Timed Out');
            throw e;        
        }else if(req.getEndpoint().contains('/preference_user/ui_setup')){
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Read Timed Out');
            throw e;
        }
        return res;
    }
}