/*
    @description    CompanyLeadController
    @date           12th July 2016
    @author         Simon Coles
  	@revision		Mike Gill 18th July 2016 Added SystemModstamp
  					Troy Wing 3 September 2016 Added Quick Add Company Functionality and changes to Like queries.

    @todo
					Add NULLS First or LAST - see if that makes a difference
					Implement Separation of Concerns


*/
public with sharing class CompanyLeadController {
    // Initialise Our variables
    private list<CompanyLeadsSubClass> CompanyList {get; set;}
    private list<CompanyLeadsSubClass> SmartDashList {get; set;}
    private set<Id> CompanySelectSet;

    // Selection and Filter variables
    private String QueryCompanyLeads; // Used to build the SOQL
    private String CountCompanyLeads; // The number of Records in Filter LIMITED Used for Pagination
    public String SOQLQueryCompanyLeads {get;set;} // Final SOQL to query
    public String companyNameFilter {get; set;}
    public Integer webTrafficFilter_minVal {get; set;}
    public Integer webTrafficFilter_maxVal {get; set;}
    public Integer empCountFilter_minVal {get; set;}
    public Integer empCountFilter_maxVal {get; set;}
    public Integer empCount6MthGrowthFilter_minVal {get; set;}
    public Integer empCount6MthGrowthFilter_maxVal {get; set;}
    public String stageFilter {get; set;}
    public String ownerFilter {get; set;}
    public String locationFilter {get; set;}
    public String verticalFilter {get; set;}
    public String ssSourceFilter {get; set;}
    public String fundatingDateFromFilter {get; set;}
    public String fundatingDateToFilter {get; set;}
    public Decimal fundAmountFilter_minVal {get; set;}
    public Decimal fundAmountFilter_maxVal {get; set;}
    public String meetCallDateFromFilter {get; set;}
    public String meetCallDateToFilter {get; set;}
    public String fromDate;
    public String toDate;
    public List<String> soqlVerticals = new List<String>();
    public List<String> soqlStages = new List<String>();
    public List<String> soqlLocations = new List<String>();
    public List<String> soqlSources = new List<String>();

    // Pagination & Figures
    public Decimal totalFilteredRecords {get; set;}
    public Decimal pagTotalNumberPages {get; set;}
    public Integer currentPageNumber {get; set;}
    public String columnToggle { get; set; }
    public String sortColumn { get; set; }
    public String sortDirection { get; set; }
    
    public Boolean showSmartDash {get;set;}
	
	
	public Integer TargetCount {get;set;}
	public Integer ProspectCount {get;set;}
	public Decimal TotalIQ {get;set;}
	public Decimal TotalStrength {get;set;}
	// Set Limit for query CACHE TMW
	public String soqlLimit {get;set;}

    // display sort and number
    public String RecPerPage {get; set;}
    public list<SelectOption> RecPerPageOption {get; set;}

    public CompanyLeadController() {
        CompanyList = new list<CompanyLeadsSubClass>(); // Use the wrapper so we can add additional fields.
        CompanySelectSet = new set<Id>(); // Used to store the ID's of records that have been selected via the checkbox.

        // Setup the Page Drop Down Select
        RecPerPageOption = new list<SelectOption>();
        RecPerPageOption.add(new SelectOption('10', '10'));
        RecPerPageOption.add(new SelectOption('25', '25'));
        RecPerPageOption.add(new SelectOption('50', '50'));
        RecPerPageOption.add(new SelectOption('100', '100'));


        //RecPerPageOption.add(new SelectOption('200', '200'));
        RecPerPage = '10'; //default records per page
        currentPageNumber = 1;
        columnToggle = 'closed';
        sortColumn = '';
		soqlLimit='9500';
        // Clear the filters
        //2016/08/31 Prevent initial run of query 
        resetFilters();
        totalFilteredRecords = 0;
        //clearFilters();
        // Run the Query
        //BuildQuery();
       //  currentPageNumber = 1;
       
       Intelligence_Settings__c intelSettings= Intelligence_Settings__c.getInstance();
       showSmartDash=intelSettings.Smart_Dash__c;
       
    }

    // The Wrapper Class - Stores Company Details and The Checkbox Value
    public class CompanyLeadsSubClass {
        public Boolean cCheckBox {get; set;}
        public CompanyLead__c cCompanyDetails {get; set;}
		public String Name {get;set;}
		public Date Date_of_Last_Call_Meeting {get;set;}
		
		public Id Primary_Interested_Portfolio_Company {get;set;}
		// Target or Prospect
		public String CompanyType{
			get;
			set;
		}
		
		public Decimal IQ6MthDelta {get;set;}
        // sub-class initialization
        public CompanyLeadsSubClass(CompanyLead__c c, Boolean chk) {
            cCompanyDetails = c;
            cCheckBox = chk;
        }
    }
    
   

    // Return a default List of Companies
    public list<CompanyLeadsSubClass> getCurrentCompanyList() {
        // ***** Update the checkbox list before we query.
        // UpdateAccountSelectedSet();
        CompanyList = new list<CompanyLeadsSubClass>();
        if(SOQLQueryCompanyLeads!=''){
	        for (CompanyLead__c c : (list<CompanyLead__c>)StdSetControllerCompanies.getRecords()) {
	            // Remove the Quotes and Square Brackets
	            if(c.Mm_Industries__c != null) {
	                c.Mm_Industries__c = c.Mm_Industries__c.replace('[','').replace(']','').replace('"','').replace(',',', ');
	            }
	            CompanyList.add(new CompanyLeadsSubClass(c, null));
	        }
        }
        return CompanyList;
    }
    
    
    
    
      // Return a default List of Companies
    public list<CompanyLeadsSubClass> getSmartDashList() {
        // ***** Update the checkbox list before we query.
        // UpdateAccountSelectedSet();
        SmartDashList = new list<CompanyLeadsSubClass>();
       	List<Id> accs= new List<Id>();
	   
	    for (Account c : (list<Account>)StdSetControllerSmartDash.getRecords()) {
	 			accs.add(c.Id);		
		}
        	
    	List<CompanyLead__c> companies=([SELECT Id, Company_Strength__c,Conference_Exhibitor__c,Exited__c,IQ_Score_1_Month_Delta__c,IQ_Score_6_Month__c,IQ_Score_Last_Month__c,Leadership_Change__c,Likely_to_Raise__c,Needs_Contact__c,New_Location__c	,New_Raise__c,Partner_Flagged__c,Raised_Greater_Than_12_Months_Ago__c,Recent_Awards__c,CreatedDate, SystemModstamp, Name, Sh_CompanyName__c, Sh_Domain__c, Mm_Employees__c, Mm_Industries__c, Mm_EmployeesMoM1M__c, Mm_EmployeesMoM6M__c, Mm_6mEmployeeGrowthNumber__c, Mm_6mEmployeeGrowthPercent__c, Mm_FacebookHandle__c, Mm_LastFundingDate__c, Mm_LinkedInId__c, Mm_Location__c, Mm_MattermarkScore__c, Mm_Id__c, Mm_MobileDownloads__c, Mm_MobileScore__c, Sh_SharedId__c, Mm_Stage__c, Mm_TotalFunding__c, Mm_TwitterHandle__c, Sh_Account__r.Id, Sh_Account__r.Name, Sh_Account__r.Owner.FirstName, Sh_Account__r.Owner.LastName, Sh_Account__r.Date_of_Last_Call_Meeting__c, Mm_AlexaRank__c, Mm_AlexaRankMoM1M__c, Mm_AlexaRankMoM6M__c,IQ__c,Flags__c FROM CompanyLead__c where SH_Account__c in :accs]);
      
      	Map<Id,CompanyLead__c> m= new Map<Id,CompanyLead__c>();
      	for(CompanyLead__c c: companies){
      		m.put(c.SH_Account__c,c);
      	}
      	
      	TargetCount=0;
      	ProspectCount=0;
      	TotalIQ=0;
      	TotalStrength=0;
      	Integer avgCount=0;
        for (Account c : (list<Account>)StdSetControllerSmartDash.getRecords()) {
        	avgCount++;
        	CompanyLeadsSubClass sc;
 			if(m.containsKey(c.Id)){
 				CompanyLead__c existing=m.get(c.Id);
 				if(existing.IQ__c!=null) TotalIQ+=existing.IQ__c;
 				if(existing.Company_Strength__c!=null) TotalStrength+=existing.Company_Strength__c;
 				
 				if(existing.Mm_Industries__c != null) {
	                existing.Mm_Industries__c = existing.Mm_Industries__c.replace('[','').replace(']','').replace('"','').replace(',',', ');
	            }
	            sc=new CompanyLeadsSubClass(existing, null);
 			} else {
 				CompanyLead__c newLead= new CompanyLead__c();
 				newLead.Name=c.Name;
 				newLead.SH_Account__c=c.Id;
 				sc=new CompanyLeadsSubClass(newLead, null);
 			}
 			SmartDashList.add(sc);
 			sc.Name=c.Name;
 			sc.Date_of_Last_Call_Meeting=c.Date_of_Last_Call_Meeting__c;
 			sc.Primary_Interested_Portfolio_Company=c.Primary_Interested_Portfolio_Company__c;
 			if(c.Primary_Interested_Portfolio_Company__c==null){
 				sc.CompanyType='P';
 				ProspectCount++;
 			} 
 			else { 
 				sc.CompanyType='T';
 				TargetCount++;
 			}
		}
		if(avgCount>0){
			TotalIQ=(TotalIQ/avgCount).setScale(2);
			TotalStrength=(TotalStrength/avgCount).setScale(2);
			
		}
	        
	       
        
        return SmartDashList;
    }

    // Set and Get the Record List
    public ApexPages.StandardSetController StdSetControllerCompanies {
        get {
            if (StdSetControllerCompanies == null) {
                StdSetControllerCompanies = new ApexPages.StandardSetController(Database.getQueryLocator(SOQLQueryCompanyLeads));
                // sets the number of records in each page set
                StdSetControllerCompanies.setPageSize(Integer.valueOf(RecPerPage));
                StdSetControllerCompanies.setpageNumber(currentPageNumber);
            }
            return StdSetControllerCompanies;
        }
        set;
    }
    
    // Set and Get the Record List
    public ApexPages.StandardSetController StdSetControllerSmartDash {
        get {
         //   if (StdSetControllerSmartDash == null) {
            	String uid=UserInfo.getUserId();
            //	String soql=  'SELECT Company_Strength__c,Conference_Exhibitor__c,Exited__c,IQ_Score_1_Month_Delta__c,IQ_Score_6_Month__c,IQ_Score_Last_Month__c,Leadership_Change__c,Likely_to_Raise__c,Needs_Contact__c,New_Location__c	,New_Raise__c,Partner_Flagged__c,Raised_Greater_Than_12_Months_Ago__c,Recent_Awards__c,CreatedDate, SystemModstamp, Id, Name, Sh_CompanyName__c, Sh_Domain__c, Mm_Employees__c, Mm_Industries__c, Mm_EmployeesMoM1M__c, Mm_EmployeesMoM6M__c, Mm_6mEmployeeGrowthNumber__c, Mm_6mEmployeeGrowthPercent__c, Mm_FacebookHandle__c, Mm_LastFundingDate__c, Mm_LinkedInId__c, Mm_Location__c, Mm_MattermarkScore__c, Mm_Id__c, Mm_MobileDownloads__c, Mm_MobileScore__c, Sh_SharedId__c, Mm_Stage__c, Mm_TotalFunding__c, Mm_TwitterHandle__c, Sh_Account__r.Id, Sh_Account__r.Name, Sh_Account__r.Owner.FirstName, Sh_Account__r.Owner.LastName, Sh_Account__r.Date_of_Last_Call_Meeting__c, Mm_AlexaRank__c, Mm_AlexaRankMoM1M__c, Mm_AlexaRankMoM6M__c,IQ__c FROM CompanyLead__c where SH_Account__r.OwnerId=:uid ';
     	String soql=  'SELECT Name,Industry,Date_of_Last_Call_Meeting__c,Primary_Interested_Portfolio_Company__c FROM  Account where OwnerId=:uid ';
     
                StdSetControllerSmartDash = new ApexPages.StandardSetController(Database.getQueryLocator(soql));
                // sets the number of records in each page set
                StdSetControllerSmartDash.setPageSize(400);
                StdSetControllerSmartDash.setpageNumber(currentPageNumber);
            //}
            return StdSetControllerSmartDash;
        }
        set;
    }


	public List<String> getSourceItems() {
 	 	List<String> options = new List<String>();
 	 	List<SS_Source__c> ss=[select Name from SS_Source__c];
 	 	for(SS_Source__c s:ss){
 	 		options.add(s.Name);	
 	 	}
 	 	return options;
  	}
    public static String parseDate(String inDate) {
        // Used to convert the datestring to a useable format
        String[] dts = inDate.split('/');
        String myDate = dts[2] + '-' + dts[0] + '-' + dts[1];
        return myDate;
    }

    public void clearFilters() {
      	resetFilters();
        BuildQueryReset();
    }
    
    public void resetFilters(){
    	currentPageNumber = 1;
        companyNameFilter = '';
        webTrafficFilter_minVal = 0;
        webTrafficFilter_maxVal = 0;
        empCountFilter_minVal = 0;
        empCountFilter_maxVal = 0;
        empCount6MthGrowthFilter_minVal = 0;
        empCount6MthGrowthFilter_maxVal = 0;
        stageFilter = '';
        ownerFilter = '';
        locationFilter = '';
        verticalFilter = '';
		ssSourceFilter='';
        fundatingDateFromFilter = '';
        fundatingDateToFilter = '';
        fundAmountFilter_minVal = 0;
        fundAmountFilter_maxVal = 0;
        meetCallDateFromFilter = '';
        meetCallDateToFilter = '';
        sortColumn = '';
        sortDirection = '';
        
        SOQLQueryCompanyLeads='';
    }

    public void nextPage() {
        currentPageNumber = currentPageNumber + 1;
        BuildQuery();
    }

    public void prePage() {
        if (currentPageNumber > 1) {
            currentPageNumber = currentPageNumber - 1;
            BuildQuery();
        }
    }

    public void firstPage() {
        currentPageNumber = 1;
        BuildQuery();
    }

    public void lastPage() {
        currentPageNumber = pagTotalNumberPages.intValue();
        BuildQuery();
    }

    public void BuildQueryReset() {
        currentPageNumber = 1;
        BuildQuery();
    }
    
    
    public void unassignTask(){
    	String Id = Apexpages.currentPage().getParameters().get('companyId');
        String reason = Apexpages.currentPage().getParameters().get('punassignReason');
    	
	     Task t=new Task();
         t.Subject='Unassign';
         t.Description=reason;
         t.WhatId=Id;
         t.Type='Unassign';                  
         t.Type__c='Unassign';
         t.ActivityDate=Date.today();
         t.status='Completed';
         insert t;
    }

    // BuildQuery - build query command for list selection change
    public void BuildQuery() {

        StdSetControllerCompanies = null;
        totalFilteredRecords = 0;
        Boolean debug = true;
        Boolean filtersUsed = false;
        String soqlAnd = '';

        CountCompanyLeads = 'SELECT count() FROM CompanyLead__c ';
        QueryCompanyLeads = 'SELECT CreatedDate, SystemModstamp, Id, Name, Sh_CompanyName__c, Sh_Domain__c, Mm_Employees__c, Mm_Industries__c, Mm_EmployeesMoM1M__c, Mm_EmployeesMoM6M__c, Mm_6mEmployeeGrowthNumber__c, Mm_6mEmployeeGrowthPercent__c, Mm_FacebookHandle__c, Mm_LastFundingDate__c, Mm_LinkedInId__c, Mm_Location__c, Mm_MattermarkScore__c, Mm_Id__c, Mm_MobileDownloads__c, Mm_MobileScore__c, Sh_SharedId__c, Mm_Stage__c, Mm_TotalFunding__c, Mm_TwitterHandle__c, Sh_Account__r.Id, Sh_Account__r.Name, Sh_Account__r.Owner.FirstName, Sh_Account__r.Owner.LastName, Sh_Account__r.Date_of_Last_Call_Meeting__c, Mm_AlexaRank__c, Mm_AlexaRankMoM1M__c, Mm_AlexaRankMoM6M__c,IQ__c FROM CompanyLead__c ';
        SOQLQueryCompanyLeads = '';
 
        if (debug) {
            system.debug('companyNameFilter : ' + companyNameFilter);
            system.debug('webTrafficFilter_minVal : ' + webTrafficFilter_minVal);
            system.debug('webTrafficFilter_maxVal : ' + webTrafficFilter_maxVal);
            system.debug('empCountFilter_minVal : ' + empCountFilter_minVal);
            system.debug('empCountFilter_maxVal : ' + empCountFilter_maxVal);
            system.debug('empCount6MthGrowthFilter_minVal : ' + empCount6MthGrowthFilter_minVal);
            system.debug('empCount6MthGrowthFilter_maxVal : ' + empCount6MthGrowthFilter_maxVal);
            system.debug('stageFilter : ' + stageFilter);
            system.debug('locationFilter : ' + locationFilter);
            system.debug('verticalFilter : ' + verticalFilter);
            system.debug('fundatingDateFromFilter : ' + fundatingDateFromFilter);
            system.debug('fromDate : ' + fromDate);
            system.debug('fundatingDateToFilter : ' + fundatingDateToFilter);
            system.debug('toDate : ' + toDate);
            system.debug('meetCallDateFromFilter : ' + meetCallDateFromFilter);
            system.debug('meetCallDateFromFilter : ' + meetCallDateFromFilter);
            system.debug('fundAmountFilter_minVal : ' + fundAmountFilter_minVal);
            system.debug('fundAmountFilter_maxVal : ' + fundAmountFilter_maxVal);
            system.debug('totalFilteredRecords :' + totalFilteredRecords);
            system.debug('pagTotalNumberPages :' + pagTotalNumberPages);
            system.debug('ownerFilter :' + ownerFilter);
        }

        // Are we filtering or running a straight query?
        if ((companyNameFilter != '') || (webTrafficFilter_minVal != 0) || (webTrafficFilter_maxVal != 0) || (empCountFilter_minVal != 0) || (empCountFilter_maxVal != 0) || (empCount6MthGrowthFilter_minVal != 0) || (empCount6MthGrowthFilter_maxVal != 0) || (stageFilter != '') || (locationFilter != '') || (verticalFilter != '') || (fundatingDateFromFilter != '') || (fundatingDateToFilter != '') || (fundAmountFilter_minVal != 0) || (fundAmountFilter_maxVal != 0) || (ownerFilter != '') || (ssSourceFilter != '') || (meetCallDateFromFilter != '') || (meetCallDateToFilter != '')) {
            SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + 'WHERE ';

            if (companyNameFilter != '') {
             //   SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + 'Name LIKE \'%' + String.escapeSingleQuotes(companyNameFilter) + '%\'';
                  SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + 'Name LIKE \'' + String.escapeSingleQuotes(companyNameFilter) + '%\'';
               
                filtersUsed = true;
            }
            /*
            if (webTrafficFilter_minVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_WebsiteUniques__c >= ' + webTrafficFilter_minVal;
                filtersUsed = true;
            }
            if (webTrafficFilter_maxVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_WebsiteUniques__c <= ' + webTrafficFilter_maxVal;
                filtersUsed = true;
            }*/
            if (empCountFilter_minVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_Employees__c >= ' + empCountFilter_minVal;
                filtersUsed = true;
            }
            if (empCountFilter_maxVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_Employees__c <= ' + empCountFilter_maxVal;
                filtersUsed = true;
            }
            if (empCount6MthGrowthFilter_minVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_6mEmployeeGrowthNumber__c >= ' + empCount6MthGrowthFilter_minVal;
                filtersUsed = true;
            }
            if (empCount6MthGrowthFilter_maxVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_6mEmployeeGrowthNumber__c <= ' + empCount6MthGrowthFilter_maxVal;
                filtersUsed = true;
            }

            if (stageFilter != '') {
                // Split the Stage by Comma
                soqlStages.clear();
                List<String> stageParts = stageFilter.split(',');
                // Loop and Reconstruct MultiSelect List
                for (String stageName : stageParts) {
                    soqlStages.add('' + stageName + '');
                }
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_Stage__c IN :soqlStages';
                filtersUsed = true;
            }

            if (locationFilter != '') {
                system.debug('SPC IN HERE');
                if (filtersUsed) soqlAnd = ' AND ';
                soqlLocations.clear();
                // Allow multiple answers seperated with a comma
                if (locationFilter.contains(',')) {
                    List<String> locationParts = locationFilter.split(',');
                    // Loop and Reconstruct MultiSelect List
                    for (String locationName : locationParts) {
                        soqlLocations.add(locationName.trim());
                    }
                } else {
                    // Just add the single Entry
                    soqlLocations.add(locationFilter.trim());
                }
                System.debug('SPC soqlLocations ' + soqlLocations);
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_Location__c IN :soqlLocations';
                filtersUsed = true;
            }

            if (verticalFilter != '') {
                // Split the Stage by Comma
                soqlVerticals.clear();
                List<String> verticalParts = verticalFilter.split(',');
                // Loop and Reconstruct MultiSelect List
                for (String verticalName : verticalParts) {
                    soqlVerticals.add('%' + verticalName + '%');
                }
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_Industries__c LIKE :soqlVerticals';
                filtersUsed = true;
            }


            if (ssSourceFilter != '') {
                // Split the Stage by Comma
                soqlSources.clear();
                List<String> sourceParts = ssSourceFilter.split(';');
                // Loop and Reconstruct MultiSelect List
                for (String sPart : sourceParts) {
                    soqlSources.add(sPart);// 	Deloitte Fast 500 2016
                }
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' SS_SourceScrub_Sources__c in :soqlSources';
                filtersUsed = true;
            }



            if (fundatingDateFromFilter != '') {
                // Convert Date Strng to Date
                fromDate = parseDate(fundatingDateFromFilter);
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_LastFundingDate__c >= ' + fromDate;
                filtersUsed = true;
            }

            if (fundatingDateToFilter != '') {
                // Convert Date Strng to Date
                toDate = parseDate(fundatingDateToFilter);
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_LastFundingDate__c <= ' + toDate;
                filtersUsed = true;
            }

            if (fundAmountFilter_minVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_TotalFunding__c >= ' + fundAmountFilter_minVal;
                filtersUsed = true;
            }
            if (fundAmountFilter_maxVal != 0) {
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Mm_TotalFunding__c <= ' + fundAmountFilter_maxVal;
                filtersUsed = true;
            }
            if (ownerFilter != '') {
                if (filtersUsed) soqlAnd = ' AND ';
                // Split the Stage by Comma
                List<String> ownerParts = ownerFilter.split(',');
                // Setup the Counters
                Integer counter = 1;
                Boolean breakFlag = false;
                Integer ownerPartsLength = ownerParts.size();
                String soqlOwners = soqlAnd;

                System.debug('ownerPartsLength ' + ownerPartsLength);

                if(ownerPartsLength == 3) {
                    // If we have all three filters set then contruct the search manually.
                    soqlOwners = soqlOwners + ' (Sh_Account__r.Owner.LastName != \'\' OR Sh_Account__r.Id = \'\')';
                } else {
                    // Do we have Analyst and Unassigned?
                    if(ownerFilter.contains('Analyst,Unassigned')) {
                        soqlOwners = soqlOwners + ' Sh_Account__r.Owner.LastName != \'\'';
                    } else {
                        soqlOwners = soqlOwners + ' (';
                        // Loop and reconstruct our SOQL String
                        for (String ownerName : ownerParts) {
                            ownerName = ownerName.trim();
                            // We need to add different parameters depending on the values as we don't match these specifically.
                            if (ownerName == 'Analyst') {
                                soqlOwners = soqlOwners + ' (Sh_Account__r.Owner.LastName != \'Unassigned\' AND Sh_Account__r.Owner.LastName != \'\')';
                            }
                            else if (ownerName == 'Unassigned') {
                                soqlOwners = soqlOwners + ' Sh_Account__r.Owner.LastName = \'Unassigned\'';
                            }
                            else if (ownerName == 'notInSalesforce') {
                                soqlOwners = soqlOwners + ' Sh_Account__r.Id = \'\'';
                            }
                            // Add the AND if we have more
                            if(counter < ownerPartsLength) soqlOwners = soqlOwners + ' OR ';
                            counter = counter + 1;
                        }
                        soqlOwners = soqlOwners + ' )';
                    }
                }

                filtersUsed = true;
                // Update the main SOQL Query
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlOwners;
            }
            if (meetCallDateFromFilter != '') {
                // Convert Date Strng to Date
                fromDate = parseDate(meetCallDateFromFilter);
                if (filtersUsed) soqlAnd = ' AND ';
                SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Sh_Account__r.Id != \'\' AND Sh_Account__r.Date_of_Last_Call_Meeting__c >= ' + fromDate;
                filtersUsed = true;
            }
            if (meetCallDateToFilter != '') {
                // Convert Date Strng to Date
                toDate = parseDate(meetCallDateToFilter);
                if (filtersUsed) soqlAnd = ' AND ';
                if (meetCallDateFromFilter != '') {
                    SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Sh_Account__r.Date_of_Last_Call_Meeting__c <= ' + toDate;
                } else {
                    SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlAnd + ' Sh_Account__r.Id != \'\' AND Sh_Account__r.Date_of_Last_Call_Meeting__c <= ' + toDate;
                }
                filtersUsed = true;
            }

        system.debug('START' + SOQLQueryCompanyLeads);

        }
        // Add the Sort
        // Remove the prefix
        String soqlSortString = '';
        if (sortColumn != '') {
            string soqlSortColumn = sortColumn.replace('toSort_', '');
              string soqlSortDir = sortDirection;
            // Account for the Account Name as we can't pass a dot notation in an ID.
            if (soqlSortColumn == 'AccLastName') {
            	soqlSortColumn = 'Sh_Account__r.Owner.Name';
            	
            }
            else if (soqlSortColumn == 'AccLastCallDate') soqlSortColumn = 'Sh_Account__r.Date_of_Last_Call_Meeting__c';
          
            soqlSortString = ' ORDER BY ' + soqlSortColumn + ' ' + soqlSortDir + ' NULLS LAST ';
        }

        // Work out our Counts
        // First do the count as Count & Sort cannot be used together
        CountCompanyLeads = CountCompanyLeads + SOQLQueryCompanyLeads + ' LIMIT '+soqlLimit+' ';
        // Count the records before we search
        totalFilteredRecords = Database.countQuery(CountCompanyLeads);

        if (soqlSortString != '') {
            // Now set the Query
            SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + soqlSortString + ' LIMIT '+soqlLimit+' ';
        } else {
            // Add the limit
            SOQLQueryCompanyLeads = SOQLQueryCompanyLeads + ' LIMIT '+soqlLimit+' ';
        }
		system.debug('TMW:BuildQuery');
        system.debug(SOQLQueryCompanyLeads);

        // Pagination Data
        pagTotalNumberPages = totalFilteredRecords.divide(Integer.valueOf(RecPerPage), 2, System.RoundingMode.UP);
        if (pagTotalNumberPages < 1) pagTotalNumberPages = 1;
        pagTotalNumberPages = pagTotalNumberPages.setScale(0);

        // Create Actual SOQL Query
        SOQLQueryCompanyLeads = QueryCompanyLeads + SOQLQueryCompanyLeads ;
        system.debug('SOQL Query = ' + SOQLQueryCompanyLeads);

        if (debug) {
            system.debug('companyNameFilter : ' + companyNameFilter);
            system.debug('webTrafficFilter_minVal : ' + webTrafficFilter_minVal);
            system.debug('webTrafficFilter_maxVal : ' + webTrafficFilter_maxVal);
            system.debug('empCountFilter_minVal : ' + empCountFilter_minVal);
            system.debug('empCountFilter_maxVal : ' + empCountFilter_maxVal);
            system.debug('empCount6MthGrowthFilter_minVal : ' + empCount6MthGrowthFilter_minVal);
            system.debug('empCount6MthGrowthFilter_maxVal : ' + empCount6MthGrowthFilter_maxVal);
            system.debug('stageFilter : ' + stageFilter);
            system.debug('locationFilter : ' + locationFilter);
            system.debug('verticalFilter : ' + verticalFilter);
            system.debug('fundatingDateFromFilter : ' + fundatingDateFromFilter);
            system.debug('fromDate : ' + fromDate);
            system.debug('fundatingDateToFilter : ' + fundatingDateToFilter);
            system.debug('toDate : ' + toDate);
            system.debug('meetCallDateFromFilter : ' + meetCallDateFromFilter);
            system.debug('meetCallDateFromFilter : ' + meetCallDateFromFilter);
            system.debug('fundAmountFilter_minVal : ' + fundAmountFilter_minVal);
            system.debug('fundAmountFilter_maxVal : ' + fundAmountFilter_maxVal);
            system.debug('totalFilteredRecords :' + totalFilteredRecords);
            system.debug('pagTotalNumberPages :' + pagTotalNumberPages);
            system.debug('ownerFilter :' + ownerFilter);
        }

    }
    
    
    @RemoteAction
	public static String  quickAddCompany(String id) {		
		Set<String> sourceFields = Schema.SObjectType.CompanyLead__c.fields.getMap().keySet();
		/*
		String allSource='';
		for(String s:sourceFields)allSource+=s+',';
		System.debug(allSource);
		*/
		Set<String> targetFields = Schema.SObjectType.Account.fields.getMap().keySet();
		List<Quick_Add_Company_Mappings__c> mappings = Quick_Add_Company_Mappings__c.getall().values();
		CompanyLead__c companyLead=[select Mm_Country__c,Mm_City__c,MM_State__c,CreatedDate, SystemModstamp, Id, Name, Sh_CompanyName__c, Sh_Domain__c, Mm_Employees__c, Mm_Industries__c, Mm_EmployeesMoM1M__c, Mm_EmployeesMoM6M__c, Mm_6mEmployeeGrowthNumber__c, Mm_6mEmployeeGrowthPercent__c, Mm_FacebookHandle__c, Mm_LastFundingDate__c, Mm_LinkedInId__c, Mm_Location__c, Mm_MattermarkScore__c, Mm_Id__c, Mm_MobileDownloads__c, Mm_MobileScore__c, Sh_SharedId__c, Mm_Stage__c, Mm_TotalFunding__c, Mm_TwitterHandle__c, Sh_Account__r.Id, Sh_Account__r.Name, Sh_Account__r.Owner.FirstName, Sh_Account__r.Owner.LastName, Sh_Account__r.Date_of_Last_Call_Meeting__c, Mm_AlexaRank__c, Mm_AlexaRankMoM1M__c, Mm_AlexaRankMoM6M__c from CompanyLead__c where id=:id]; 
		Account a= new Account();
			
		sObject sTarget=(sObject)a;
		sObject sSource=(sObject)companyLead;
		
		for(Quick_Add_Company_Mappings__c m:mappings){
			String sourceField=m.Source__c.toLowerCase();
			String targetField=m.Target__c.toLowerCase();
			if(targetFields.contains(targetField) && sourceFields.contains(sourceField)) {
				Object sourceValue=sSource.get(sourceField);
				if(targetField=='website__c') sourceValue='http://www.'+(String)sourceValue;
				sTarget.put(targetField,sourceValue);
				system.debug(m.Source__c+':'+sSource.get(sourceField));
			}
			else 
			{ 
				if(!sourceFields.contains(sourceField))sTarget.addError('Invalid mapping for '+m.Source__c+'. Please check custom settings');
				if(!targetFields.contains(targetField))sTarget.addError('Invalid mapping for '+m.Target__c+'. Please check custom settings');
				return null;
				}
		}
		
		insert a;
		companyLead.SH_Account__c=a.Id;
		update companyLead;
		
		return a.Id;

	}
}