/**
 * @File Name          : HivebriteIntegrationCompanyHelperTest.cls
 * @Description        : 
 * @Author             : Anup Kage
 * @Group              : 
 * @Last Modified By   : Anup Kage
 * @Last Modified On   : 7/11/2019, 5:41:44 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/11/2019   Anup Kage     Initial Version
**/
@isTest(SeeAllData = false)
private with sharing class HivebriteIntegrationCompanyHelperTest {
  
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
   @IsTest
    static void createPartnerProject(){
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecord();
        List<Account> accountList =  getAccountRecord(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void updatePartnerProject(){       
        
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecordwithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();

    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertPartnerProjectOAuthError(){
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecordwithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponse401();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertPartnerProjectJSONExp(){
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecordwithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponseJSONException();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertPartnerProjectBadRequest(){
        Account Company =  HivebriteIntegrationTestFactory.createVendorRecordwithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponse500();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Partner-Project', 'Hivebrite API', 'PARTNER_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @param recId 
    * @return List<Account> 
    **/
    private static List<Account> getAccountRecord(String recId){
        String query = 'SELECT '+HivebriteIntegrationTestFactory.getSobjectRecords('Account')+' FROM Account WHERE Id =:recId ';
        return Database.query(query);
    }

    
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void createPortfolioCompany(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        List<Account> accountList =  getAccountRecord(Company.Id);
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', 'PORTFOLIO_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void updatePortfolioCompany(){       
        
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockResponse();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', 'PORTFOLIO_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();

    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertPortfolioCompanyOAuthError(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponse401();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', 'PORTFOLIO_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertPortfolioCompanyJSONExp(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponseJSONException();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', 'PORTFOLIO_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void insertPortfolioCompanyBadRequest(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecordWithHivebriteId();
        List<Account> accountList = getAccountRecord(Company.Id); 
        Test.startTest();
        HivebriteIntegrationTestFactory.setupMockBadResponse500();
        HivebriteIntegrationCompanyHelper objHelper = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', 'PORTFOLIO_SF_TO_HB', accountList);
        objHelper.startCalloutProcess();
        Test.stopTest();
    }
    /**
    * @description 
    * @author Anup Kage | 5/11/2019 
    * @return void 
    **/
    @IsTest
    static void coverUncalledMethods(){
        Account Company =  HivebriteIntegrationTestFactory.createCompanyRecord();
        List<Account> accountList =  getAccountRecord(Company.Id);
        Test.startTest();
            HivebriteIntegrationCompanyHelper obj =  new HivebriteIntegrationCompanyHelper();
            HivebriteIntegrationCompanyHelper obj1 = new HivebriteIntegrationCompanyHelper('Portfolio-Company', 'Hivebrite API', accountList);
            obj1.enqueueNextJob('PORTFOLIO_SF_TO_HB',accountList);
        Test.stopTest();
        
    }
}