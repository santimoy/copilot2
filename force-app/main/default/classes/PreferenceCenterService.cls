/**
 * @author Dharmendra Karamchandani
 * @date 2nd June 2019
 * @className PreferenceCenterService
 * @group PreferenceCenter
 */
public class PreferenceCenterService {

    /***********************************************************************
     * @author Dharmendra Karamchandani
     * @description prepare map of Preference_Center__c for specific ids
     * @return Map of Preference_Center__c
     * @param set of userIds
     ***********************************************************************/
    public static Map<Id, Preference_Center__c> getPreferenceCenterMap(Set<Id> userIds){
        Map<Id, Preference_Center__c> preferenceCenterMap = new Map<Id, Preference_Center__c>();
        for(Id userId : userIds){
            Preference_Center__c pre = Preference_Center__c.getInstance(userId);
            pre = maintainPreCenter(pre);
            preferenceCenterMap.put(userId, pre);
        }
        return preferenceCenterMap;
    }
    /***********************************************************************
     * @author Dharmendra Karamchandani
     * @description maintain endpoint value if not found for input
     * @return Preference_Center__c
     * @param Preference_Center__c
     ***********************************************************************/
    static Preference_Center__c maintainPreCenter(Preference_Center__c preferenceCenter){
        if(preferenceCenter == null){
            preferenceCenter = new Preference_Center__c();
        }
        if(String.isEmpty(preferenceCenter.Preference_Center_Endpoint__c)){
            Organization og = PreferenceCenterService.curOrg;
            preferenceCenter.Preference_Center_Endpoint__c = curOrg.isSandbox ? System.Label.DW_API_ENDPOINT_TEST : System.Label.DW_API_ENDPOINT;
        }
        return preferenceCenter;
    }
    /***********************************************************************
     * @author Dharmendra Karamchandani
     * @description prepare instance Preference_Center__c for loggedin user
     * @return Preference_Center__c
     ***********************************************************************/
    public static Preference_Center__c getPreferenceCenter(){
        return getPreferenceCenterMap(new Set<Id>{UserInfo.getUserId()} ).get(UserInfo.getUserId());
    }
    /***********************************************************************
     * @author Dharmendra Karamchandani
     * @description prepare instance Preference_Center__c of the Org
     * @return Preference_Center__c
     ***********************************************************************/
    public static Preference_Center__c getPreferenceCenterOrgDefault(){
        Preference_Center__c pre = Preference_Center__c.getOrgDefaults();
        pre = maintainPreCenter(pre);
        return pre;
    }
    /***********************************************************************
     * @author Dharmendra Karamchandani
     * @description get the instance Organization
     * @return Organization
     ***********************************************************************/
    public static Organization curOrg{
        get{
            if(curOrg==null){
                curOrg = [SELECT isSandbox FROM Organization];
            }
            return curOrg;
        }set;
    }
}