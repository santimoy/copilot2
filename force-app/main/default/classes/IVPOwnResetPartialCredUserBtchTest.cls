/***********************************************************************************
* @author      10k-Expert
* @name		IVPOwnResetPartialCredUserBtchTest
* @date		2019-01-21
* @description	To test IVPOwnResetPartialCredUserBtch
***********************************************************************************/
@isTest
public class IVPOwnResetPartialCredUserBtchTest {
    @isTest
    public static void unitTest(){
        IVPOwnershipTestFuel testdata = new IVPOwnershipTestFuel();
        testdata.populateIVPGeneralConfig();
        
        RecordType rType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType='ACCOUNT' AND DeveloperName='Prospect' LIMIT 1];
        
        Account acc = new Account();
        acc.Name='Test Account 1';
        acc.RecordTypeId=rType.Id;
        acc.Ownership_Status__c='Watch';
        acc.Partial_Credit_User__c = UserInfo.getUserId();
        insert acc;
        
        acc.Partial_Credit_User_Changed_On__c = System.now().addYears(-1);
        update acc;
        
        Test.startTest();
        System.schedule('test-IVP Reset Partial Credit User', '00 00 * * * ?', new IVPOwnResetPartialCredUserBtch());
        IVPOwnResetPartialCredUserBtch batchObj = new IVPOwnResetPartialCredUserBtch();
        Database.executeBatch(batchObj);
        Test.stopTest();
        Account updatedAcc = [Select Id,Name,Partial_Credit_User__c,Partial_Credit_User_Changed_On__c From Account Limit 1];
        
        System.assert(updatedAcc.Partial_Credit_User__c == null);
        System.assert(updatedAcc.Partial_Credit_User_Changed_On__c == null);
        
    }
}