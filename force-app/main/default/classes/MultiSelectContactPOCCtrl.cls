public with sharing class MultiSelectContactPOCCtrl {
    public MultiSelectContactPOCCtrl() {

    }
    
	@AuraEnabled
    public static TypeAheadRes[] typeAheadFuncLtng(String rName, String sObjName,String filter, String recordData)
    {
        return TypeaheadFunction.srch(rName,sObjName,filter,'', recordData);
    }
}