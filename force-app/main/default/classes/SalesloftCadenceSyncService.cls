/**
 * @author  : 10k-Advisor
 * @name    : SalesloftCadenceSyncService 
 * @date    : 2018-08-10
 * @description   : The class used as service to handle the process related to Salesloft Cadence object
**/
public class SalesloftCadenceSyncService implements SalesLoftProcessDomain{
    
    public void processHTTPResponse(HttpResponse response){
        if (response == NULL){
            return;
        }
        Salesloft.CadenceResponse cadenceData = (Salesloft.CadenceResponse)JSON.deserialize(
                                                    response.getBody(), 
                                                    Salesloft.CadenceResponse.Class);
        if(cadenceData!=NULL){
            Integer next_page;
            if(cadenceData.metadata!=NULL){
                if(cadenceData.metadata.paging!=NULL){
                    next_page = cadenceData.metadata.paging.next_page = cadenceData.metadata.paging.next_page;
                }
            }
            if(cadenceData.data!=NULL){
                processData(cadenceData.data);
            }
            if(next_page!=null){
                System.enqueueJob(new SalesLoftSyncQ('cadence', Salesloft.CADENCE_END_POINT, next_page));
            }else{
                SalesloftUtility.removeSetting(new Set<String>{'salesloft-job-running'});
                if(!Test.isRunningTest()){
                    //initiating CadenceMember Syncing.
                    System.enqueueJob(new SalesLoftSyncQ('cadencemember', Salesloft.CADENCE_MEMBER_END_POINT, 1, SalesLoftSyncQ.startSyncDateTime));
                }
            }
        }
    }
    
    public static Map<String, String> processData(List<Salesloft.Cadence> cadences){
        
        Set<String> personIds = new Set<String>();
        Set<String> cadenceIds = new Set<String>();
        Map<String, Salesloft_Cadence__c> cadenceMap = new Map<String, Salesloft_Cadence__c>();
        
        for(Salesloft.Cadence cadence : cadences){
            if(cadence.id != NULL){
                if(String.isBlank(cadence.name)){
                    cadence.name = cadence.id;
                }
                cadenceMap.put(cadence.id, new Salesloft_Cadence__c(Salesloft_Id__c = cadence.id, Name = cadence.name));
            }
        }
        
        Map<String, String> sfConIdWithId= new Map<String, String>();
        for(List<Salesloft_Cadence__c> existingCadences :[SELECT Salesloft_Id__c FROM Salesloft_Cadence__c 
                                                    WHERE Salesloft_Id__c in : cadenceMap.keySet()]){
            for(Salesloft_Cadence__c cadence : existingCadences){
                sfConIdWithId.put(cadence.Salesloft_Id__c, cadence.Id);
                cadenceMap.remove(cadence.Salesloft_Id__c);
            }
        }
        try{
            if(!cadenceMap.isEmpty()){
                upsert cadenceMap.values() Salesloft_Id__c;
            }
            
            for(String cid : cadenceMap.keySet()){
                sfConIdWithId.put(cid, cadenceMap.get(cid).Id);
            }
        }catch(Exception ex){
            SalesloftUtility.notifyToUser('Salesloft', ex);
        }
        return sfConIdWithId;
    }

}