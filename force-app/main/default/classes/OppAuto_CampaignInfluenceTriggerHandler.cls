/**
*@name      	: CampaignInfluenceTrigger
*@developer 	: 10K developer
*@date      	: 2018-10-12
*@description   : This class is use as Handler of Campaign Influence Trigger 
**/
public class OppAuto_CampaignInfluenceTriggerHandler {
    public static void stopAutometicInfluenceRecordCreate(List<CampaignInfluence> campInfluenceRecords){
        Set<Id> opportunityIds = new Set<ID>(); 
        Map<Id, String> oppIdsWithRecordTypeNames = new Map<Id,String>();
        
        for(CampaignInfluence campInflu : campInfluenceRecords){
            opportunityIds.add(campInflu.OpportunityId);
        }
        
        for(Opportunity oppRec : [SELECT Id,RecordType.Name FROM Opportunity WHERE Id IN :opportunityIds]){
            oppIdsWithRecordTypeNames.put(oppRec.Id, oppRec.RecordType.Name);    
        }
        
        for(CampaignInfluence campInflu : campInfluenceRecords){
            if(campInflu.Opp_Auto_Is_Calling_Manual__c == false 
               && oppIdsWithRecordTypeNames.containsKey(campInflu.OpportunityId) 
               && oppIdsWithRecordTypeNames.get(campInflu.OpportunityId) == 'Ignite Opportunity'){
                   
                   campInflu.addError('Can not process');
               }
        }
    }
    
}