public class ConfigureColumnsController {
    @AuraEnabled
    public static fieldsWrapper getFieldList(){
        fieldsWrapper fw = new fieldsWrapper();
        List<String> fieldList = new List<String>();
        List<String> selectedList = new List<String>();
        List<String> requiredList = new List<String>();
        List<Intelligence_User_Preferences__c> intelligenceUserList = getIntelligenceField();
        String columnValue = '';
        if(intelligenceUserList.size() > 0 && intelligenceUserList[0].Display_Columns__c != null){
            columnValue = intelligenceUserList[0].Display_Columns__c.replaceAll(';',',');
        }
        List<Intelligence_Field__c> nameFiledsList = [SELECT Id, Name, DB_Field_Name__c,Default_display_field__c ,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c 
                                                             FROM Intelligence_Field__c 
                                                             Where  UI_Label__c='Company Name' order by Order__c];
        
        for(Intelligence_Field__c names : nameFiledsList){
            
            if(names.DB_Field_Name__c != null && names.UI_Label__c != null){
            	String field = names.UI_Label__c;
                requiredList.add(field);
                if(!fieldList.contains(field)){
                    fieldList.add(field);
                }
                if(!selectedList.contains(field) && names.Default_display_field__c){
                    selectedList.add(field);
                }
            }
        }
        List<Intelligence_Field__c> intelligenceFieldList = [SELECT Id, Name, DB_Field_Name__c,Default_display_field__c ,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c 
                                                             FROM Intelligence_Field__c 
                                                             Where Active__c = true and Target_Field__c != 'Name' and UI_Label__c != null and DB_Column__c != null and Optional_display_Field__c=true order by UI_Label__c asc];
        for(Intelligence_Field__c intelligenceField : intelligenceFieldList){
            if(intelligenceField.DB_Field_Name__c != null && intelligenceField.UI_Label__c != null && intelligenceField.UI_Label__c != 'Name'){
            	String field = intelligenceField.UI_Label__c+' ('+intelligenceField.DB_Field_Name__c+')';
                if(!fieldList.contains(field)){
                    fieldList.add(field);
                }
                if(!selectedList.contains(field) && intelligenceField.Default_display_field__c){
                    selectedList.add(field);
                }
            }
        }
        if(columnValue == '' && selectedList.size() > 0){
            String displayCol = '';
            for(String str : selectedList){
                displayCol += str+';';
            }
            displayCol = displayCol.substring(0, displayCol.length() - 1);
            if(intelligenceUserList.size()>0){
                intelligenceUserList[0].Display_Columns__c = displayCol;
                update intelligenceUserList;
                columnValue = intelligenceUserList[0].Display_Columns__c.replaceAll(';',',');
            }
            fw.selectedList = selectedList;
        }
        
        

        fw.isWrapColn = checkWrapColn();
        fw.fieldList = fieldList;
        fw.selectedList = columnValue.split(',');
        fw.selectedList = columnValue.split(',');
        if(requiredList.size() > 0){
            fw.requiredList = requiredList;
        }
        
        return fw;
    }
    
    public static boolean checkWrapColn(){
        List<Intelligence_User_Preferences__c> intUserPref = [SELECT Id, Wrap_Large_Columns__c, User_Id__c from Intelligence_User_Preferences__c where User_Id__c =: UserInfo.getUserId() limit 1];
        if(intUserPref.size()> 0)
        {
            return intUserPref[0].Wrap_Large_Columns__c;
        }else{
            return false;
        }
    }
    
    @AuraEnabled
    public static void saveColumns(String columns, boolean isWrapColn){
       List<Intelligence_Field__c> intelligenceFieldList = [SELECT Id, Name, DB_Field_Name__c,Default_display_field__c ,UI_Label__c, Order__c, Active__c, Static_Selection__c, DB_Column__c 
                                                             FROM Intelligence_Field__c 
                                                             Where Active__c = true and Target_Field__c != 'Name' and UI_Label__c != null and DB_Column__c != null and Optional_display_Field__c=true order by UI_Label__c asc];
        
        
        if(columns != null && columns != ''){
            columns = columns.replaceAll(',',';'); 
        }else{
           columns=''; 
        }
        
        if (columns == 'Company Name'){
            for(Intelligence_Field__c intelligenceField : intelligenceFieldList){
                if(intelligenceField.DB_Field_Name__c != null && intelligenceField.UI_Label__c != null && intelligenceField.UI_Label__c != 'Name'){
                    String field = intelligenceField.UI_Label__c+' ('+intelligenceField.DB_Field_Name__c+')';
                    if(intelligenceField.Default_display_field__c){
                        columns +=  ','+field;
                    }
                }
            }
            columns = columns.replaceAll(',',';'); 
        }
        
        List<Intelligence_User_Preferences__c> intelligenceUserList = getIntelligenceField();
        if(intelligenceUserList.size()>0){
            intelligenceUserList[0].Display_Columns__c = columns;
            intelligenceUserList[0].Wrap_Large_Columns__c = isWrapColn;
            update intelligenceUserList;
        }
    }
    public static List<Intelligence_User_Preferences__c> getIntelligenceField(){
        List<Intelligence_User_Preferences__c> intelligenceUserList = [SELECT Display_Columns__c, Id,Wrap_Large_Columns__c  FROM Intelligence_User_Preferences__c Where User_Id__c=:userInfo.getUserId() limit 1];
        return intelligenceUserList;
    }
    public class fieldsWrapper{
        @AuraEnabled public  List<String> fieldList {get;set;}
        @AuraEnabled public  List<String> selectedList {get;set;}
        @AuraEnabled public  List<String> requiredList {get;set;}
        @AuraEnabled public  boolean isWrapColn {get;set;}
    }
    @AuraEnabled
    public static void resetDefault(){

        List<Intelligence_Field__c> fields =[Select DB_Column__c,UI_Label__c,Default_display_field__c from Intelligence_Field__c where  Active__c = true and Target_Field__c != 'Name'  order by Order__c asc, UI_Label__c desc];
        String defaultSelectedColumnName =  '';
        if(!fields.isEmpty()){
            for(Intelligence_Field__c field:fields){
                if(field.Default_display_field__c){
                    String dbColumnName = field.DB_Column__c;
                    dbColumnName = String.isNotEmpty(dbColumnName)  ? dbColumnName : '';
                    dbColumnName = dbColumnName.substring(dbColumnName.lastIndexOf('.')+1);
                    
                    String colLabel = field.UI_Label__c;
                    if(colLabel == null){
                        colLabel = dbColumnName;
                    }else{
                        if(colLabel != ''){
                            colLabel = colLabel +' ('+dbColumnName + ')';
                        }else{
                            colLabel = dbColumnName;
                        }
                    }
                    
                    if(defaultSelectedColumnName == ''){
                        defaultSelectedColumnName += colLabel;
                    }else{
                        defaultSelectedColumnName += ';'+colLabel;
                    }
                }
            }
        }
        List<Intelligence_User_Preferences__c> intelligenceUserList = getIntelligenceField();
        if(intelligenceUserList.size()>0){
            intelligenceUserList[0].Display_Columns__c = defaultSelectedColumnName;
            update intelligenceUserList;
        }
    }
}