/**
 * @author Dharmendra Karamchandani
 * @date 2018-05-10
 * @className IVPSDLVMaintainVisibilityService
 * @group IVPCompanyListView
 *
 * @description contains methods to handle IVPCompanyListView component
 * 				This is used as service class for IVPSDLVMaintainVisibilitySch
 */
public class IVPSDLVMaintainVisibilityService {
	public static String IVP_SDLV_NAME_INITIAL{
		get{
			if(IVP_SDLV_NAME_INITIAL == NULL){
				IVP_SDLV_NAME_INITIAL = IVPUtils.IVP_SD_NAME_INITIAL+'lv_';
			}
			return IVP_SDLV_NAME_INITIAL;
		}set;
	} 
	public static String IVP_SDLV_LABEL_INITIAL{
		get{
			if(IVP_SDLV_LABEL_INITIAL == NULL){
				IVP_SDLV_LABEL_INITIAL = IVPUtils.IVP_SD_LABEL_INITIAL+'LV ';
			}
			return IVP_SDLV_LABEL_INITIAL;
		}set;
	}
	
	static Datetime dateTimeFilter{
		get{
			if( dateTimeFilter == NULL){
				IVP_General_Config__c ivpGeneralConfig = IVP_General_Config__c.getValues('last-sync-time');
				
				if(ivpGeneralConfig!= NULL && String.isNotEmpty(ivpGeneralConfig.Value__c) && ivpGeneralConfig.Value__c.isNumeric()){
					dateTimeFilter = DateTime.newInstance(Long.valueOf(ivpGeneralConfig.Value__c));
				}
			}
			return dateTimeFilter;
		}set;
	}
	
	static String soqlFilter {
		get{
			if(soqlFilter == null){
				soqlFilter = ' WHERE SObjectType=\'Account\' AND (not Name like \'%public%\') ';
				if(dateTimeFilter != NULL){
					soqlFilter += 'AND LastModifiedDate >=:dateTimeFilter ';
				}
			}
			return soqlFilter;
		}set;
	}
	
	public static String soqlQeury {
		get{
			if(soqlQeury == null){
				soqlQeury = ' Select Id,Name, DeveloperName, CreatedById, CreatedBy.Name FROM ListView ' + soqlFilter;
			}
            
			return soqlQeury;
		}set;
	}
	
	public static Map<Id, String>  prepareUserWithGroupNameMap(List<ListView> listViews){
		Map<Id, ListView> listViewMap= new Map<Id, ListView>();
    	Set<Id> userIds = new Set<Id>();
        System.debug('list'+listviews.size());
    	for(ListView lv : listViews){
    		if(lv.DeveloperName.startsWithIgnoreCase(IVP_SDLV_NAME_INITIAL)){
    			String lsViewUserId = lv.DeveloperName.subString(lv.DeveloperName.indexOf(IVP_SDLV_NAME_INITIAL));
    			if(lsViewUserId.startsWithIgnoreCase('005')){
    				userIds.add(lv.CreatedById);
    			}
    		}else{
    			userIds.add(lv.CreatedById);
    		}
    		listViewMap.put(lv.Id, lv);
    	}
        
    	Map<Id, String> userIdWithGroupName = new Map<Id, String>();
    	List<GroupMember> groupMembers = new List<GroupMember>();
    	String soql = 'SELECT Id, GroupId, Group.DeveloperName, UserOrGroupId FROM GroupMember WHERE UserOrGroupId in : userIds '
    					+'AND Group.DeveloperName like \'%'+ IVP_SDLV_NAME_INITIAL + '%\' LIMIT 40000';
		System.debug(IVP_SDLV_NAME_INITIAL);
   		// System.debug(Database.query('SELECT Id, GroupId, Group.DeveloperName, UserOrGroupId FROM GroupMember WHERE  Group.DeveloperName like \'%'+ IVP_SDLV_NAME_INITIAL + '%\''));
    	for(GroupMember groupMember: Database.query(soql)){
			userIdWithGroupName.put(groupMember.UserOrGroupId, groupMember.Group.DeveloperName);
			userIds.remove(groupMember.UserOrGroupId);
    	}
    	
    	if(!userIds.isEmpty()){
	    	Map<Id, Group> userIdGroupsToCreate = new Map<Id, Group> ();
	    	
	    	for(Id lvId : listViewMap.keySet()){
	    		ListView lv = listViewMap.get(lvId);
	    		if(userIds.contains(lv.CreatedById) && !userIdGroupsToCreate.containsKey(lv.CreatedById)){
	    			Group groupObj = new Group(Name=IVP_SDLV_LABEL_INITIAL+lv.CreatedBy.Name, DeveloperName=IVP_SDLV_NAME_INITIAL+lv.CreatedById);
	    			
	    			if(groupObj.Name.length() >40){
		            	groupObj.Name = groupObj.Name.substring(0, 37)+'...';
		            }
	    			userIdGroupsToCreate.put(lv.CreatedById, groupObj);
	    			userIds.remove(lv.CreatedById);
	    		}
	    	}
            
            if(!listViewMap.isEmpty()){
	    		insert userIdGroupsToCreate.values();
	    		for(Id userId : userIdGroupsToCreate.keySet()){
	    			Group grpObj = userIdGroupsToCreate.get(userId);
	    			groupMembers.add(new GroupMember(GroupId = grpObj.Id, UserOrGroupId = userId));
	    			userIdWithGroupName.put(userId, grpObj.DeveloperName);
	    		}
	    		insert groupMembers;
	    	}
    	}
        
        return userIdWithGroupName;
	}
	
	public static void shareListViews(Map<Id, String> userIdWithGroupId, List<ListView> listViews, String sObjectName){
		for(ListView lv :listViews){
			if(userIdWithGroupId.containsKey(lv.CreatedById)){
				try{
					IVPMetadataServiceProvider.shareListViewiWithGroup(sObjectName.toUpperCase()+'.'+lv.DeveloperName, userIdWithGroupId.get(lv.CreatedById));
				}catch(Exception excp){
					System.debug(excp.getStacktraceString() + '='+ excp.getMessage());
				}
			}
		}
	}
	
	public static String checkAndCreateIVPGroupForUser(Id userId, String userName, Boolean isInsert){
		List<Profile> adminProfiles = [SELECT Id FROM Profile WHERE Id=: UserInfo.getProfileId() AND Name='System Administrator' ];
		
		String groupName = IVP_SDLV_NAME_INITIAL+userId;
		List<Group> groups = [SELECT Id, Name, (SELECT UserOrGroupId FROM GroupMembers WHERE UserOrGroupId=:userId) 
								FROM Group 
								WHERE DeveloperName =:groupName];
		
		List<GroupMember> membersToCreate = new List<GroupMember>();
		if(groups.isEmpty()){
			if(adminProfiles.isEmpty() || !isInsert)
				return '';
			Group ivpGroup = new Group(Name = IVP_SDLV_LABEL_INITIAL+userName, 
			                            DeveloperName=groupName);
            insert ivpGroup;
            membersToCreate.add(new GroupMember(GroupId = ivpGroup.Id, UserOrGroupId = userId));
		}else{
		    if(groups[0].GroupMembers.isEmpty()){
		        membersToCreate.add( new GroupMember(GroupId = groups[0].Id, UserOrGroupId = userId));
		    }
		}
		
		if(!membersToCreate.isEmpty()){
		    insert membersToCreate;
		}
        
		return groupName;
	}
	
	public static String getIVPUserGroupName(Id userId, String userName){
		return checkAndCreateIVPGroupForUser(userId, userName, false);
	}
}