/*
*	ActivityTimelineController class : It contains logics to display Activity Timeline for Mobile in SF1 app.
* 									   It allows Users to display Open Activities and Activity Histories
* 									   and allows them to apply filters for each types to display specific Activities.
*
*	Author: Philip
*	Date:   April 9, 2018
*
*	Last Modified:
*		5/31/2018 PC Added type to color and icon mappings
*		6/08/2018 WN Fixed exception when activity has a null activitydate
*/
global with sharing class ActivityTimelineController
{
    static Integer NUM_PAST_ACTIVITY = ( CommonConfigUtil.getConfig().Activity_Viewer_Mobile_Num_Past_Activity__c != NULL && CommonConfigUtil.getConfig().Activity_Viewer_Mobile_Num_Past_Activity__c <= 500 ) ? Integer.valueOf( CommonConfigUtil.getConfig().Activity_Viewer_Mobile_Num_Past_Activity__c ) : 500;
    static Integer NUM_OF_CHARACTER_DESC = ( CommonConfigUtil.getConfig().Activity_Viewer_Mobile_Num_Chars_Desc__c != NULL ) ? Integer.valueOf( CommonConfigUtil.getConfig().Activity_Viewer_Mobile_Num_Chars_Desc__c ) : 500;
 
    public static Map<String, List<Integer>> TYPE_TO_COLOR_MAP = new Map<String, List<Integer>>{ 'Call' => new List<Integer> {0,232,23},
        'Meeting' => new List<Integer> {0,193,245},
        'Meet' => new List<Integer> {0,193,245},
        'Voicemail' => new List<Integer> {0,3,255},
        'Vmail' => new List<Integer> {0,3,255},
        'Email' => new List<Integer> {250,50,200},
        'Unassign' => new List<Integer> {245,51,0},
        'Pass' => new List<Integer> {245,158,0},
        'Other' => new List<Integer> {245,241,9},
        'Examine' => new List<Integer> {245,241,9},
        'TermSheetProposed' => new List<Integer> {0,193,245},
        'TermSheetSigned' => new List<Integer> {0,193,245},
        'Article' => new List<Integer> {71,237,198},
        'Attachment' => new List<Integer> {71,237,198},
        'Task' => new List<Integer> {136,136,136},
        'Companyrequest'=> new List<Integer>{0, 95, 178}
        };
                                                                                                   
    public static Map<String, String> TYPE_TO_ICON_MAP = new Map<String, String>{ 'Call' => 'utility:log_a_call',
        'Meeting' => 'utility:groups',
        'Meet' => 'utility:groups',
        'Voicemail' => 'utility:voicemail_drop',
        'Vmail' => 'utility:voicemail_drop',
        'Email' => 'utility:email',
        'Unassign' => 'utility:announcement',
        'Pass' => 'utility:pin',
        'Other' => 'utility:pin',
        'Examine' => 'utility:pin',
        'TermSheetProposed' => 'utility:attach',
        'TermSheetSigned' => 'utility:attach',
        'Article' => 'utility:attach',
        'Attachment' => 'utility:attach',
        'Task' => 'utility:task',
        'Companyrequest' => 'utility:priority'
        };
                                                                                                           
    public static final Map<Id, User> mapMyTeamUserIds = new Map<Id, User>([SELECT Id FROM User WHERE Id = :UserInfo.getUserId() OR ManagerId = :UserInfo.getUserId()]);
    public static final  Set<Id> filterIdSet = new Set<Id>(mapMyTeamUserIds.keySet());

    @AuraEnabled
    public static ActivityTimelineWrapper getAccountWithActivities( Id recordId, String activityFilter, String dateFilter, String selectedTalentGroupFilter )
    {

        System.debug('selectedTalentGroupFilter====> 60'+selectedTalentGroupFilter);

        String queryString = buildActivitiesQueryString( recordId , selectedTalentGroupFilter);
        sObject  obj = Database.query( queryString );
        System.debug( 'Acc obj:::activityFilter : ' + activityFilter );
        
        List<sObject> openActivitiesFiltered = filterActivities(recordId, obj, 'OpenActivities', activityFilter, dateFilter , selectedTalentGroupFilter);
        List<sObject> pastActivitiesFiltered = filterActivities(recordId, obj, 'ActivityHistories', activityFilter, dateFilter , selectedTalentGroupFilter);
        System.debug('openActivities>>>'+openActivitiesFiltered);
        System.debug('pastActitvity>>>>'+pastActivitiesFiltered); 
        List<sObject> lstAllFilteredActivities = new List<sObject>();
        if(!openActivitiesFiltered.isEmpty())
            lstAllFilteredActivities.addAll(openActivitiesFiltered);

        if(!pastActivitiesFiltered.isEmpty())
            lstAllFilteredActivities.addAll(pastActivitiesFiltered);

            System.debug( 'lstAllFilteredActivities::: : ' + lstAllFilteredActivities );

        Map<Id, String> mapTaskIdToComment = new Map<Id, String>();
        if(!lstAllFilteredActivities.isEmpty())
            mapTaskIdToComment = getRichTextTaskComments(lstAllFilteredActivities);
		System.debug('NUM_PAST_ACTIVITY'+NUM_PAST_ACTIVITY);
        ActivityTimelineWrapper ret = new ActivityTimelineWrapper();
        ret.openActivities = buildActivityWrappers( openActivitiesFiltered, 0, 500 );
        ret.pastActivities = buildActivityWrappers( pastActivitiesFiltered, 0, NUM_PAST_ACTIVITY );
        ret.loadedNumOfPastActivities = ret.pastActivities.size();
        ret.totalCountOfPastActivities = pastActivitiesFiltered.size();
        ret.mapTaskIdToRichTextComment = mapTaskIdToComment;
        System.debug('ret==total===>88'+ret.totalCountOfPastActivities);
      
        return ret;

    }
    
    @AuraEnabled
    public static List<ActivityWrapper> loadPastActivities( Id recordId, String activityFilter, String dateFilter, Integer offsetValue )
    {
        String queryString = buildActivitiesQueryString( recordId , null);
        sObject  obj = Database.query( queryString );
        
        List<sObject> pastActivitiesFiltered = filterActivities(recordId, obj, 'ActivityHistories', activityFilter, dateFilter, null);
        List<ActivityWrapper> ret = buildActivityWrappers( pastActivitiesFiltered, offsetValue, NUM_PAST_ACTIVITY );
        System.debug('ret====>101 '+ret);
        return ret;
    }

    private static Map<String, String> teamProfileNamebyProfileName {get {
        if(teamProfileNamebyProfileName == null) {
                teamProfileNamebyProfileName = new Map<String, String>();
                for(Activity_Timeline_Team__mdt objActivityTimeline: [SELECT Id,Profile_Name__c, Team_Profile_Name__c 
                                                                        FROM Activity_Timeline_Team__mdt]) {
                    if(objActivityTimeline.Profile_Name__c != null 
                        &&  objActivityTimeline.Team_Profile_Name__c != null){
                        teamProfileNamebyProfileName.put(objActivityTimeline.Profile_Name__c,objActivityTimeline.Team_Profile_Name__c);
                    }
                }
            }
        return teamProfileNamebyProfileName;
    } set;}
    

    private static Map<string, PermissionSetAssignment> currentUserPermissionsetMap {get {
        if(currentUserPermissionsetMap == null) {
                currentUserPermissionsetMap = new Map<string, PermissionSetAssignment>();
                for(PermissionSetAssignment objPermsnSet: [SELECT Id, PermissionSet.Name 
                                                 FROM PermissionSetAssignment  
                                                 WHERE Assignee.Id =: UserInfo.getUserId()]) {
                    currentUserPermissionsetMap.put(objPermsnSet.PermissionSet.Name,objPermsnSet);
                }
            }
        return currentUserPermissionsetMap;
    } set;}

    private static List<sObject> reverseList( List<sObject> initialList )
    {
        List<sObject> reversedList = new List<sObject>();
        if( initialList != NULL && !initialList.isEmpty() )
            for( Integer i = initialList.size()-1; i >= 0; i-- )
            reversedList.add( initialList[i] );
        
        return reversedList;
    }
    
    private static String TSK_TALENT_RT = [SELECT Id,Name,DeveloperName 
                                           FROM RecordType 
                                           WHERE DeveloperName = 'Talent'].Id;
    
    private static String profileName = [SELECT Id,Name FROM Profile WHERE Id =: UserInfo.getProfileId()].Name;

    private static List<sObject> filterActivities(Id recordId, sObject obj, String activityType, String typeFilter, String dateFilter , String selectedTalentGroupFilter)
    {
        
        List<sObject> filteredRows = new List<sObject>();

        List<sObject> taskList = new List<sObject>();

        Set<Id> activityHistoriesIdSet = new Set<Id>();
        
         System.debug('activityType:----158 '+activityType);
         System.debug('typeFilter: ---159'+typeFilter);
        // System.debug('dateFilter: '+dateFilter);
        System.debug('selectedTalentGroupFilter111: '+selectedTalentGroupFilter);

        System.debug('activityTypelist: '+obj);
        List<sObject> activityRows = new List<sObject>();
        for(Sobject objAc:obj.getSobjects(activityType)){
            
                activityRows.add(objAc);
        }
        
        //List<sObject> activityRows = obj.getSObjects(activityType);
        System.debug('activityRows: '+activityRows);
        
        String taskQueryString;
        if( activityRows != NULL && !activityRows.isEmpty() )
        {
            System.debug(activityType +'size: '+activityRows.size());
            // we need to reverse the list if openactivities
            if( activityType.containsIgnoreCase( 'Open' ) )
                activityRows = reverseList( activityRows );
            
            // enforce sharing rules visibility
            // not needed?
            //activityRows = enforceSharingVisibility(activityRows);
            
            for( sObject o : activityRows )
            {	 
                system.debug('o----->'+o); 
                system.debug('o---get value-->'+String.valueOf( o.get( 'ActivityType' ))); 
                // filter type:  ActivityType = :typeFilter 
                System.debug('String.valueOf ActivityType: '+String.valueOf( o.get( 'ActivityType' )));
                // filter type: ActivitySubtype = :typeFilter 
                if( String.isNotBlank( typeFilter ) && !typeFilter.equalsIgnoreCase( String.valueOf( o.get( 'Type__c' ) ) ) )
                    continue;
                
                // date filter: ActivityDate in Last_Seven, Next_Seven, Last_Thirty
                if( String.isNotBlank( dateFilter ) && o.get( 'ActivityDate' ) != NULL )
                {
                    Date tDate = Date.today();
                    Date aDate = Date.valueOf( o.get( 'ActivityDate' ) );
                    System.debug('aDate====>207 '+aDate);
                    Integer daysBetween = tDate.daysBetween( aDate );
                    System.debug('daysBetween====>209 '+daysBetween);
                   //Below Date  filter condition only for past activites
                    if( !( activityType.containsIgnoreCase( 'Open' ) && ( daysBetween < 0 ) ) &&
                       ( ( dateFilter.equalsIgnoreCase( 'Last_Seven' ) && !( daysBetween > -7 && daysBetween <= 0 ) ) ||
                        ( dateFilter.equalsIgnoreCase( 'Last_Thirty' ) && !( daysBetween > -30 && daysBetween <= 0 ) ) ||
                        ( dateFilter.equalsIgnoreCase( 'Next_Seven' ) && !( daysBetween >=0 && daysBetween < 7 ) ) ) )
                        continue;
                    //Below Date  filter condition only for Next Step
                    if( ( activityType.containsIgnoreCase( 'Open' ) && ( daysBetween < 0 ) ) &&
                    ( ( dateFilter.equalsIgnoreCase( 'Last_Seven' ) && !( daysBetween > -7 && daysBetween <= 0 ) ) ||
                     ( dateFilter.equalsIgnoreCase( 'Last_Thirty' ) && !( daysBetween > -30 && daysBetween < 0 ) ) ||
                     ( dateFilter.equalsIgnoreCase( 'Next_Seven' ) && !( daysBetween > 0 && daysBetween < 7 ) ) ) )
                     continue;
                        
                    
                }
                System.debug('activityHistoriesIdSetB$>>>>>'+activityHistoriesIdSet);
                activityHistoriesIdSet.add(o.Id);
                System.debug('activityHistoriesIdSet>>>>>'+activityHistoriesIdSet);
                filteredRows.add(o);
            }

            taskQueryString = ' SELECT Id, Type, Type__c, ActivityDate, Subject, OwnerId, Owner.Name, TaskSubtype, Status, WhoId, Who.Name, WhatId, Description FROM Task  WHERE AccountId = \'' + recordId + '\' AND Id IN: activityHistoriesIdSet ';

            System.debug('taskQueryString=====>223 '+taskQueryString);
            System.debug('My Team teamProfileNamebyProfileName: '+teamProfileNamebyProfileName);
            System.debug('My Team profileName: '+profileName);
            
            //For Talent Permission Set view Starts here
            
            /*Set<string> permissionSetName = new Set<string>(currentUserPermissionsetMap.keySet());
            if(!permissionSetName.contains('Talent_Activities')){
               if(!permissionSetName.contains('Talent_Activities_View')){
                	taskQueryString += ' AND RecordTypeID != \''+TSK_TALENT_RT+'\' ';
            	} 
            }*/
            if(!FeatureManagement.checkPermission('Talent_Activities')){
                if(!FeatureManagement.checkPermission('hasTalentTaskViewPermission')){
                system.debug('>>>>>>>');
				taskQueryString += ' AND RecordTypeID != \''+TSK_TALENT_RT+'\' ';
            }
            }
            
            
            
			// Talent Permission Set Ends Here
            String teamProfileName = teamProfileNamebyProfileName.get(profileName);
            System.debug('My Team get Team Profiles: '+teamProfileName);

            Map<Id,Profile> teamProfileIdMap;
            // String teamProfileQueryString
            if(teamProfileName != null){
                teamProfileIdMap = new Map<Id,Profile>([SELECT Id,Name 
                                                                    FROM Profile
                                                                    WHERE Name =: teamProfileName OR
                                                                            Name =: profileName]);
            }else{
                teamProfileIdMap = new Map<Id,Profile>([SELECT Id,Name 
                                                                    FROM Profile
                                                                    WHERE Name =: profileName]);
            }

            Map<Id,User> teamUserMap;
                if(teamProfileIdMap != null){
                    teamUserMap = new Map<Id,User>([SELECT Id,Name 
                                                                FROM User
                                                                WHERE ProfileId IN: teamProfileIdMap.keySet()]);
            }

            if(teamUserMap != null){
                Set<Id> teamUserIdSet = new Set<Id>(teamUserMap.keySet());
                System.debug('My Team  teamUserIdSet: '+teamUserIdSet);
            }
            
            if(selectedTalentGroupFilter == 'mytasks'){
                taskQueryString+=' AND OwnerId = \''+UserInfo.getUserId()+'\' ';
            }else if(selectedTalentGroupFilter == 'myteamtasks'){
                // taskQueryString+=' AND OwnerId IN : filterIdSet ';
                taskQueryString+=' AND OwnerId IN: teamUserIdSet ';
            }
            String currentUserRoleName ='';
            Id currentUserRoleId = UserInfo.getUserRoleId();
            if(currentUserRoleId != null){
                currentUserRoleName = [SELECT ID, DeveloperName FROM UserRole WHERE ID =: currentUserRoleId LIMIT 1].DeveloperName;
            }
            
            taskQueryString+=' ORDER BY ActivityDate DESC ALL Rows ';

            System.debug(' taskQueryString: '+taskQueryString);
            List<sObject> TaskLIstFileters = Database.query( taskQueryString );
            System.debug('taskQueryString Results: '+TaskLIstFileters);
            System.debug('taskQueryString size: '+TaskLIstFileters.size());

            return Database.query( taskQueryString );
        }else{
            return new List<sObject>();
        }
        
        // return filteredRows;
    }
    
    private static List<ActivityWrapper> buildActivityWrappers( List<sObject> activityRows, Integer offsetValue, Integer numberOfRecs )
    {
        List<ActivityWrapper> activityWrappers = new List<ActivityWrapper>();
        Set<Id> alternateDetailIds = new Set<Id>();
        if( activityRows != NULL && !activityRows.isEmpty() )
        {
            Integer skipCount = 0;
            Integer offsetVal = Integer.valueOf( offsetValue );
            Date todaysDate = Date.today();
            System.debug('activityRows---->'+activityRows);
            for( sObject o : activityRows )
            {
                // skip offset records
                if( skipCount < offsetVal )
                {
                    skipCount++;
                    continue;
                }
                
                // construct activitywrapper instance
                ActivityWrapper wrapper = new ActivityWrapper();
                wrapper.activityId = (Id)o.get( 'Id' );
                // wrapper.activityType = String.valueOf( o.get( 'ActivityType' ) );
                wrapper.activityType = String.valueOf( o.get( 'Type' ) );
                // wrapper.activitySubtype = String.valueOf( o.get( 'ActivitySubtype' ) ).toLowerCase();
                wrapper.activitySubtype = String.valueOf( o.get( 'TaskSubtype' ) ).toLowerCase();
                wrapper.subject = String.valueOf( o.get( 'Subject' ) );
                wrapper.activityStatus = String.valueOf( o.get( 'Status' ) );
                System.debug('wrapper.activityStatus=====336 '+wrapper.activityStatus);
                if( String.isNotBlank( (String)o.get( 'Type__c' ) ) )
                {
                    wrapper.typeC = String.valueOf( o.get( 'Type__c' ) ).toLowerCase().deleteWhitespace();
                    wrapper.iconName = ( TYPE_TO_ICON_MAP.containsKey( wrapper.typeC.capitalize() ) ) ? TYPE_TO_ICON_MAP.get( wrapper.typeC.capitalize() ) : '';
                }
                System.debug('typeC==327====> '+wrapper.typeC );
                System.debug('typeC==328====> '+wrapper.iconName );

                // if( o.get( 'AlternateDetailId' ) != NULL )
                // {
                //     wrapper.alternateDetailId = (Id)o.get( 'AlternateDetailId' );
                //     alternateDetailIds.add( (Id)o.get( 'AlternateDetailId' ) );
                // }
                
                String description = String.valueOf( o.get( 'Description' ) );
                if( String.isNotBlank( description ) && description.length() >= NUM_OF_CHARACTER_DESC )
                    wrapper.description = description.substring(0, NUM_OF_CHARACTER_DESC) + ' ...';
                else
                    wrapper.description = description;
                
                // Date activityDate = Date.valueOf( o.get( 'ActivityDate' ) );
                Date activityDate = Date.valueOf( o.get( 'ActivityDate' ) );
                if( activityDate != NULL )
                {
                    wrapper.activityDateStr = '';
                    // wrapper.activityDate = ( wrapper.activitySubtype == 'event' ) ? DateTime.valueOf( o.get( 'StartDateTime' ) ) : activityDate;
                    wrapper.activityDate = activityDate;
                    if( activityDate.year() == todaysDate.year() )
                    {
                        wrapper.activityDateFormat = ( wrapper.activitySubtype == 'email' || wrapper.activitySubtype == 'event' ) ? 'HH:mm a | MMM d' : 'MMM d';
                        wrapper.activityDateFormat = 'MMM d';
                        wrapper.isActivityCurrentYear = true;
                        if( activityDate == todaysDate )
                            wrapper.activityDateStr = 'Today';
                        else if( activityDate == todaysDate.addDays(-1) )
                            wrapper.activityDateStr = 'Yesterday';
                        else if( activityDate == todaysDate.addDays(1) )
                            wrapper.activityDateStr = 'Tomorrow';
                
                        if( activityDate > todaysDate )
                        //System.debug('wrapper.activityStatus===== '+wrapper.activityStatus);
                        wrapper.isActivityDateIsOverDue = true;
                        
                        else if( activityDate < todaysDate ){
                            wrapper.isActivityDateIsOverDue = false;
                        }
                    }
                    else
                    {
                        wrapper.activityDateFormat = 'MMM d, yyyy';
                        wrapper.isActivityCurrentYear = false;
                    }
                }
                else
                {
                    wrapper.activityDate = null;
                    wrapper.activityDateStr = 'No due date';
                }
                
                if( String.isNotBlank( wrapper.typeC ) )
                {
                    System.debug('wrapper.typeC.capitalize()'+wrapper.typeC.capitalize());
                    wrapper.customColorBorder = getActivityColorString( activityDate, wrapper.typeC.capitalize(), Double.valueOf(1.3) );
                    wrapper.customColor = getActivityColorString( activityDate, wrapper.typeC.capitalize(), Double.valueOf(1.0) );
                }
                
                // wrapper.startDateTime = DateTime.valueOf( o.get( 'StartDateTime' ) );
                // wrapper.endDateTime = DateTime.valueOf( o.get( 'EndDateTime' ) );
                
                Id ownerId = (Id)o.get( 'OwnerId' );
                wrapper.assignedToId = ownerId;
                if( String.valueOf( UserInfo.getUserId() ).containsIgnoreCase( ownerId ) )
                    wrapper.assignedToName = 'You';
                else
                    wrapper.assignedToName = String.valueOf( o.getSObject( 'Owner' ).get( 'Name' ) );
                
                // if( o.getSObject( 'PrimaryWho' ) != NULL )
                // {
                //     wrapper.primaryWhoName = String.valueOf( o.getSObject( 'PrimaryWho' ).get( 'Name' ) );
                //     wrapper.primaryWhoId = (Id)o.get( 'PrimaryWhoId' );
                // }

                if( o.getSObject( 'Who' ) != NULL )
                {
                    wrapper.primaryWhoName = String.valueOf( o.getSObject( 'Who' ).get( 'Name' ) );
                    wrapper.primaryWhoId = (Id)o.get( 'WhoId' );
                }

                activityWrappers.add( wrapper );
                
                // loop until array size >= number of recs count
                if( activityWrappers.size() >= numberOfRecs )
                    break;
            }
            
            /**
* Commented out by Phil Choi - 7/12/2018
* Requested by Insight Team to display generic Message and make it consistent for all Tasks
if( !alternateDetailIds.isEmpty() )
{
Map<Id, EmailMessage> emailMessageIdToEmailMessage = new Map<Id, EmailMessage>();
for( EmailMessage em : [ SELECT Id, FromAddress, ToAddress, MessageDate, RelatedToId, RelatedTo.Name, TextBody FROM EmailMessage WHERE Id IN :alternateDetailIds ] )
{
emailMessageIdToEmailMessage.put( em.Id, em );
}

for( ActivityWrapper aw : activityWrappers )
{
if( String.isNotBlank( aw.alternateDetailId ) )
{
EmailMessage em = emailMessageIdToEmailMessage.get( aw.alternateDetailId );
aw.activityToAddress = em.ToAddress;
aw.activityFromAddress = em.FromAddress;
aw.activityRelatedToId = em.RelatedToId;
aw.activityRelatedToName = em.RelatedTo.Name;
aw.activityTextBody = em.TextBody;
aw.activityDate = em.MessageDate;
if( aw.activityDate.year() == todaysDate.year() )
aw.activityDateformat = 'HH:mm a | MMM d';
else
aw.activityDateformat = 'HH:mm a | MMM d, yyyy';
}
}
}
*/
        }
        
        return activityWrappers;
    }

    private static String buildActivitiesQueryString( Id recordId, String  selectedTalentGroupFilter)
    {

        // System.debug( 'recordId : ' + recordId );
		Id currentUserRoleId = UserInfo.getUserRoleId();
		String currentUserRoleName ='';
		if(currentUserRoleId != null){
			currentUserRoleName = [SELECT ID, DeveloperName FROM UserRole WHERE ID =: currentUserRoleId LIMIT 1].DeveloperName;
        }

        String queryString = 'SELECT Id, Name,';
        
        // SOQL security requirements: No WHERE clause, LIMIT 500, fixed ORDER BY
        // Filters and offset are performed post SOQL query
        
        // OpenActivities
        queryString += ' ( SELECT Id, AccountId, AlternateDetailId, ActivityDate, ActivitySubtype, ActivityType, EndDateTime, IsTask, OwnerId, Owner.Name, PrimaryAccountId, PrimaryWho.Name, PrimaryWhoId, ReminderDateTime, StartDateTime, Status, Subject, WhatId, WhoId, Type__c ' +
            'FROM OpenActivities ORDER BY ActivityDate ASC NULLS FIRST, LastModifiedDate DESC LIMIT 500), ';

        // ActivityHistories
        queryString += ' ( SELECT Id, AccountId, AlternateDetailId, ActivityDate, ActivitySubtype, ActivityType, EndDateTime, OwnerId, IsTask, Owner.Name, PrimaryAccountId, PrimaryWho.Name, PrimaryWhoId, ReminderDateTime, StartDateTime, Status, Subject, WhatId, WhoId, Type__c ' +
            'FROM ActivityHistories ORDER BY ActivityDate DESC NULLS FIRST, LastModifiedDate DESC LIMIT 500) ';   
        	queryString += ' FROM ' + recordId.getSObjectType().getDescribe().getName() + ' WHERE Id = \'' + recordId + '\'';
        
        System.debug( 'queryString Building Method : ' + queryString );
        return queryString; 
    }
    @TestVisible
    private static String getActivityColorString( Date actdate, String type, Double multiplier )
    {
        String colorString = '';
        if( TYPE_TO_COLOR_MAP.containsKey( type ) )
        {
            // WN - fixed if activitydate is null
           
            System.debug('type--->'+type);
            Date d = ( actdate != NULL ? actdate : Date.today() );
            Double numDays = d.daysBetween( Date.today() );
            
            Double tmp = 2.71;
            Double opacityScale = Math.pow( tmp, Double.valueOf( -1.0 / 90.0 * numDays - 0.2 ) ) * .3 + .15;
            opacityScale *= multiplier;
            
            List<Integer> colorArray = TYPE_TO_COLOR_MAP.get( type );
            System.debug('colorArray'+colorArray);
           
            if(type != 'Companyrequest'){
                colorString = 'rgba(' + String.valueOf( colorArray.get(0) ) + ',' + String.valueOf( colorArray.get(1) ) + ',' + String.valueOf( colorArray.get(2) ) + ',' + opacityScale + ')';
            }
            else{
                colorString = 'rgba(' + String.valueOf( colorArray.get(0) ) + ',' + String.valueOf( colorArray.get(1) ) + ',' + String.valueOf( colorArray.get(2) );
            }

           
        }
        else
            colorString = '';
                
        return colorString;
    }
    
    global class ActivityTimelineWrapper
    {
        @AuraEnabled
        public List<ActivityWrapper> openActivities { get; set; }
        
        @AuraEnabled
        public List<ActivityWrapper> pastActivities { get; set; }
        
        @AuraEnabled
        public Integer totalCountOfPastActivities { get; set; }
        
        @AuraEnabled
        public Integer loadedNumOfPastActivities { get; set; }
        
        @AuraEnabled
        public Map<Id, String> mapTaskIdToRichTextComment { get; set; }
    }
    
    global class ActivityWrapper
    {
        @AuraEnabled
        public Id activityId { get; set; }
        
        @AuraEnabled
        public Id alternateDetailId { get; set; }
        
        @AuraEnabled
        public String activityStatus { get; set; }
        
        @AuraEnabled
        public String activityType { get; set; }
        
        @AuraEnabled
        public String activitySubType { get; set; }
        
        @AuraEnabled
        public String assignedToName { get; set; }
        
        @AuraEnabled
        public Id assignedToId { get; set; }
        
        @AuraEnabled
        public String primaryWhoName { get; set; }
        
        @AuraEnabled
        public Id primaryWhoId { get; set; }
        
        @AuraEnabled
        public String subject { get; set; }
        
        @AuraEnabled
        public String description { get; set; }
        
        @AuraEnabled
        public Boolean isActivityCurrentYear { get; set; }
        
        @AuraEnabled
        public String activityDateFormat { get; set; }
        
        @AuraEnabled
        public DateTime activityDate { get; set; }
        
        @AuraEnabled
        public String activityDateStr { get; set; }
        
        @AuraEnabled
        public DateTime startDateTime { get; set; }
        
        @AuraEnabled
        public DateTime endDateTime { get; set; }
        
        @AuraEnabled
        public String activityToAddress { get; set; }
        
        @AuraEnabled
        public String activityFromAddress { get; set; }
        
        @AuraEnabled
        public String activityRelatedToId { get; set; }
        
        @AuraEnabled
        public String activityRelatedToName { get; set; }
        
        @AuraEnabled
        public String activityTextBody { get; set; }
        
        @AuraEnabled
        public String typeC { get; set; }
        
        @AuraEnabled
        public String customColor { get; set; }
        
        @AuraEnabled
        public String customColorBorder { get; set; }
        
        @AuraEnabled
        public String iconName { get; set; }
        @AuraEnabled
        public Boolean isActivityDateIsOverDue { get; set; }
    }
    
    @AuraEnabled
    public static Map<Id, String> getRichTextTaskComments( List<sObject> lstFilteredActivities) {
        
        Map<Id, String> mapTaskIdToComment = new Map<Id, String>();
        Map<Id, String> mapTaskIdToTaskComment = new Map<Id, String>();
        
        for(sObject objActivity : lstFilteredActivities) {
            
            // if(objActivity.get('IsTask') == true) {
                
                mapTaskIdToTaskComment.put((Id)objActivity.get('Id'), (String)objActivity.get('Description'));
            // }
        }
        
        for(Task_Comment__c objTaskComment : [SELECT Id, Task_Id__c, Comment__c 
                                              FROM Task_Comment__c
                                              WHERE Task_Id__c IN: mapTaskIdToTaskComment.keySet()]) {
                                                  
                                                  mapTaskIdToComment.put(objTaskComment.Task_Id__c, objTaskComment.Comment__c); 
                                              }
        
        for(Id taskId : mapTaskIdToTaskComment.keySet()){
            
            if((mapTaskIdToComment.containsKey(taskId) 
                && String.isBlank(mapTaskIdToComment.get(taskId)))
               || !mapTaskIdToComment.containsKey(taskId))
                mapTaskIdToComment.put(taskId, mapTaskIdToTaskComment.get(taskId)); 
        }
        return mapTaskIdToComment;
    }
    
    @AuraEnabled 
    public static Boolean isRichTextCommentExist(String strTaskId) {
        
        List<Task_Comment__c> lstTaskComment = new List<Task_Comment__c>();
        
        lstTaskComment = [SELECT Id
                          FROM Task_Comment__c
                          WHERE Task_Id__c=:strTaskId];
         
        return (!lstTaskComment.isEmpty()) ? TRUE : FALSE;
    }
    
	//Added sukesh code
    @AuraEnabled
    public static List<String> getButtonMenuItems(sObject objectType, string field) {
        
        List<String> allOpts = new List<String>();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objectType.getSObjectType();
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        // Get a map of fields for the SObject
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        // Get the list of picklist values for this field.
        List<Schema.PicklistEntry> values = fieldMap.get(field).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }
}