/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Krow_Project_Assignment_Schea2bTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Krow_Project_Assignment_Sa2bTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Krow__Project_Assignment_Schedule__c());
    }
}