public with sharing class AddInvoiceLineItem_Custom {

    public static Map<Id, List<Krow__Timesheet_Split__c>> timesheetSplitsByProjectId ;
    /* Project Billing types */
    public final static String PROJECT_BILLING_TYPE_INSTALLMENT = 'Installment';
    public final static String PROJECT_BILLING_TYPE_RECURRING = 'Recurring';
    public final static String PROJECT_BILLING_TYPE_IMMEDIATE = 'Immediate';
    public final static String PROJECT_BILLING_TYPE_TIME_AND_EXPENSE = 'Time & Expense';
    public final static String PROJECT_BILLING_TYPE_FIXED_FEE = 'Fixed Fee';
    public static String ApprovalStatus = '';

    public static void createInvoiveLineItemsForInvoices(Krow__Invoice__c invoice, String Approval_Status){
        try{
            if(Approval_Status != NULL && !String.isBlank(Approval_Status)){
                ApprovalStatus = Approval_Status;
            }
            List<Krow__Invoice_Line_Item__c> invoiceLineItemsToInsert = new List<Krow__Invoice_Line_Item__c>();
            timesheetSplitsByProjectId = new Map<Id, List<Krow__Timesheet_Split__c>>();

            if(invoice != NULL ){
                if(invoice.Id != null){
                    List<Krow__Timesheet_Split__c> invoiceTimesheetSplitList =  new List<Krow__Timesheet_Split__c>();
                    if(invoice.Krow__Invoice_Account__c != null && invoice.Krow__Start_Date__c != null && invoice.Krow__End_Date__c != null){
                        List<Krow__Project__c>  accountProjectList = AddInvoiceLineItem_Custom.GetprojectsByAccountId(invoice.Krow__Invoice_Account__c, invoice.Krow__Start_Date__c, invoice.Krow__End_Date__c);
                        if(accountProjectList != null){
                            for(Krow__Project__c project : accountProjectList){
                                if(project.Krow__Billing_Type__c == null || project.Krow__Billing_Type__r.Krow__Billing_Method__c == PROJECT_BILLING_TYPE_TIME_AND_EXPENSE){
                                    List<Krow__Timesheet_Split__c> accountInvoiceTimesheetSplitList = timesheetSplitsByProjectId.get(project.Id);
                                    if(accountInvoiceTimesheetSplitList != null && !accountInvoiceTimesheetSplitList.isEmpty()){
                                        invoiceTimesheetSplitList.addAll(accountInvoiceTimesheetSplitList);
                                    }
                                }
                            }
                        }
                    }

                    system.debug('invoiceTimesheetSplitList Size-->'+invoiceTimesheetSplitList.size());
                    if(invoiceTimesheetSplitList != null && !invoiceTimesheetSplitList.isEmpty()){
                        for(Krow__Timesheet_Split__c split : invoiceTimesheetSplitList){
                            if(split.Krow__Hours__c != null && split.Krow__Hours__c != 0){
                                Krow__Invoice_Line_Item__c newInvoiceLineItem = new Krow__Invoice_Line_Item__c(Krow__Invoice__c = invoice.Id);
                                newInvoiceLineItem.Krow__Timesheet_Split__c = split.Id;
                                newInvoiceLineItem.Krow__Include__c = true;
                                newInvoiceLineItem.Krow__Resource__c = split.Krow__User__c;
                                newInvoiceLineItem.Krow__Project_Resource__c = split.Krow__Project_Resource__c;
                                newInvoiceLineItem.Krow__Date__c = split.Krow__Date__c;
                                newInvoiceLineItem.Krow__Task__c = split.Krow__Krow_Task__c;
                                newInvoiceLineItem.Krow__Hours__c = split.Krow__Hours__c;
                                newInvoiceLineItem.Krow__Bill_Rate__c = split.Krow__Bill_Rate_New__c;
                                newInvoiceLineItem.Krow__Timesheet_Notes__c = split.Krow__Time_Comments__c;
                                if(UserInfo.isMultiCurrencyOrganization()){
                                    newInvoiceLineItem.put('CurrencyIsoCode',(String)split.get('CurrencyIsoCode'));
                                }
                                invoiceLineItemsToInsert.add(newInvoiceLineItem);
                            }
                        }
                    }
                }
            }
            if(invoiceLineItemsToInsert != NULL && invoiceLineItemsToInsert.size() > 0){
                insert invoiceLineItemsToInsert;
            }

        } catch(Exception e){
            system.debug('>>>>> Error message --> '+e.getMessage()+ ' On Line Number --> '+e.getLineNumber()+' In Class AddInvoiceLineItem_Custom');
        }
    }

    public static List<Krow__Project__c> GetprojectsByAccountId(Id AccountId, Date startDate, Date endDate){
        try{
            //Testing
            /*Date startDate = Date.today().addYears(-10);
            Date endDate = Date.today().addYears(10);*/
            //Testing
            List<Krow__Project__c> KPList = new List<Krow__Project__c>();
            if(AccountId != NULL){
                List<Project_Company__c> ProjectCompanyList = new List<Project_Company__c>();
                ProjectCompanyList = [Select Id, Account__c, Krow_Project__c From Project_Company__c Where Account__c =: AccountId];
                if(ProjectCompanyList.size() > 0){
                    Set<Id> KPIDs = new Set<Id>();
                    for(Project_Company__c ProCompany : ProjectCompanyList){
                        if(ProCompany.Krow_Project__c != NULL){
                            KPIDs.add(ProCompany.Krow_Project__c);
                        }
                    }
                    if(KPIDs.size() > 0){
                        KPList = [Select Id,Krow__Billing_Type__c, Krow__Billing_Type__r.Krow__Billing_Method__c From Krow__Project__c Where Id In : KPIDs];
                        List<Krow__Timesheet_Split__c> TimesheetSplitList = new List<Krow__Timesheet_Split__c>();
                        String KTSQuery = 'Select Id, Krow__Date__c, Krow__Hours__c, Krow__User__c, Krow__Project_Resource__c, Krow__Krow_Project__c, Krow__Krow_Task__c, ';
                        KTSQuery += 'Krow__Bill_Amount__c, Krow__Bill_Rate_New__c, Krow__Invoiced__c, Krow__Time_Comments__c, Krow__Approval_Status__c, ';
                        KTSQuery += 'Krow__Timesheet__r.Krow__Approval_Status__c ';
                        if(UserInfo.isMultiCurrencyOrganization()){
                            KTSQuery += ', CurrencyIsoCode ';
                        }
                        KTSQuery +=' From Krow__Timesheet_Split__c ';
                        KTSQuery +=' where Krow__Invoiced__c = false and Krow__Billable__c = true ';
                        KTSQuery +=' and Krow__Krow_Project__c != null and Krow__Krow_Project__c In : KPIDs';
                        KTSQuery +=' and Krow__Krow_Project__r.Krow__Billable__c = true ';
                        KTSQuery +=' and (Krow__Krow_Project__r.Krow__Billing_Type__c = null ';
                        Boolean IsTEBillingMethodExist = AddInvoiceLineItem_Custom.FindTEBillingMethod();
                        if(IsTEBillingMethodExist){
                            KTSQuery +=' OR Krow__Krow_Project__r.Krow__Billing_Type__r.Krow__Billing_Method__c = \''+PROJECT_BILLING_TYPE_TIME_AND_EXPENSE+'\' ';
                        }
                        KTSQuery +=')';
                        Krow__Invoice_Integration__c invoiceIntegrationConfig = AddInvoiceLineItem_Custom.GetinvoiceIntegrationConfig();
                        if(invoiceIntegrationConfig != null && invoiceIntegrationConfig.Krow__Enable_Hold_for_Billing__c != null && invoiceIntegrationConfig.Krow__Enable_Hold_for_Billing__c){
                            KTSQuery +=' and Krow__Hold_for_Billing__c = false ';
                        }
                        KTSQuery +=' and Krow__Date__c >= :startDate and Krow__Date__c <= :endDate';
                        if(ApprovalStatus != NULL && !String.isBlank(ApprovalStatus)){
                            if(ApprovalStatus.toLowerCase() == 'approved'){
                                KTSQuery +=' and Krow__Approval_Status__c = ' + '\'' + ApprovalStatus + '\' ';
                            }
                            else {
                                KTSQuery +=' and Krow__Approval_Status__c != ' + '\'' + ApprovalStatus + '\' ';
                            }
                        }
                        TimesheetSplitList = Database.query(KTSQuery);
                        system.debug('>>>>> TimesheetSplitList Size --> '+TimesheetSplitList.size());
                        if(TimesheetSplitList.size() > 0){
                            for(Krow__Timesheet_Split__c kts : TimesheetSplitList){
                                if(kts.Krow__Krow_Project__c != NULL){
                                    if(!timesheetSplitsByProjectId.containsKey(kts.Krow__Krow_Project__c)){
                                        List<Krow__Timesheet_Split__c> EmptyList = new List<Krow__Timesheet_Split__c>();
                                        timesheetSplitsByProjectId.put(kts.Krow__Krow_Project__c, EmptyList);
                                    }
                                    timesheetSplitsByProjectId.get(kts.Krow__Krow_Project__c).add(kts);
                                }
                            }
                        }
                    }
                }
            }
            return KPList;
        } catch (Exception e){
            system.debug('>>>>> Error message --> '+e.getMessage()+ ' On Line Number --> '+e.getLineNumber()+' In Class AddInvoiceLineItem_Custom');
        }
        return null;
    }

    //TEBillingMethod = Time & Expense Billing Method
    public static Boolean FindTEBillingMethod(){
        Boolean IsTEBillingMethodExist = false;
        List<Krow__Billing_Type__c> billingTypeList = new List<Krow__Billing_Type__c>();
        billingTypeList =   [   select id,Name,Krow__Billing_Method__c
                                from Krow__Billing_Type__c
                                where Krow__Billing_Method__c != null and Krow__Billing_Method__c =: PROJECT_BILLING_TYPE_TIME_AND_EXPENSE
                            ];
        if(billingTypeList.size() > 0){
            IsTEBillingMethodExist = true;
        }
        return IsTEBillingMethodExist;
    }

    public static Krow__Invoice_Integration__c GetinvoiceIntegrationConfig(){
        List<Krow__Invoice_Integration__c> invoiceIntegrationConfigs = Krow__Invoice_Integration__c.getall().values();
        Krow__Invoice_Integration__c invoiceIntegrationConfig = new Krow__Invoice_Integration__c();
        if(!invoiceIntegrationConfigs.isEmpty()){
            invoiceIntegrationConfig = invoiceIntegrationConfigs.get(0);
        } else{
            invoiceIntegrationConfig = new Krow__Invoice_Integration__c(Name = 'Default');
        }
        return invoiceIntegrationConfig;
    }

    /*//Testing only
    public static void AddTimeSheetSplitForTesting(){
        Map<String,Schema.SObjectField> mapOfFields = Schema.SObjectType.Krow__Timesheet_Split__c.fields.getMap();
        Set<String> All_Fields = mapOfFields.keySet();
        String Fieldz_To_Query = 'Select ';
        for(String Fld : All_Fields){
            Schema.DescribeFieldResult Obj_Fld = mapOfFields.get(Fld).getDescribe();
            if(Obj_Fld.isAccessible()){
                Fieldz_To_Query += mapOfFields.get(Fld) +',';
            }
        }
        Fieldz_To_Query = Fieldz_To_Query.removeEnd(',');
        system.debug('>>>>>Fieldz_To_Query -->'+Fieldz_To_Query);
        List<Krow__Timesheet_Split__c> TimeSheetSplitList = new List<Krow__Timesheet_Split__c>();
        String QueryString = Fieldz_To_Query + ' From Krow__Timesheet_Split__c Where Id = ' + '\'' + 'a2Y0W000003aqQu' + '\' Limit 1' ;
        TimeSheetSplitList = Database.query(QueryString);
        if(TimeSheetSplitList.size() > 0){
            List<Krow__Timesheet_Split__c> TimeSheetSplitListToInsert = new List<Krow__Timesheet_Split__c>();
            Krow__Timesheet_Split__c SobjTSS = new Krow__Timesheet_Split__c ();
            SobjTSS = TimeSheetSplitList[0].clone();
            SobjTSS.Krow__Invoiced__c = false;
            SobjTSS.Krow__Billable__c = true;
            SobjTSS.Krow__Hold_for_Billing__c = false;
            SobjTSS.Krow__Date__c = Date.newInstance(2017, 12, 10);
            SobjTSS.Krow__Approval_Status__c = 'Approved';
            system.debug('>>>>> SobjTSS --> '+SobjTSS);
            TimeSheetSplitListToInsert.add(SobjTSS);
            List<Database.saveResult> Results = Database.insert(TimeSheetSplitListToInsert, false);
            for(Database.saveResult result : Results){
                if(!result.isSuccess()){
                    system.debug('>>>>> Error in Inserting Record --> '+result.getErrors());
                }
                else {
                    system.debug('>>>>> New Record Id --> '+result.getId());
                }
            }
        }
    }

    //Testing
    public static void DebugallFields(){
        Map<String,Schema.SObjectField> mapOfFields = Schema.SObjectType.Krow__Timesheet_Detail__c.fields.getMap();
        Set<String> All_Fields = mapOfFields.keySet();
        String Fieldz_To_Query = 'Select ';
        for(String Fld : All_Fields){
            Schema.DescribeFieldResult Obj_Fld = mapOfFields.get(Fld).getDescribe();
            if(Obj_Fld.isAccessible()){
                Fieldz_To_Query += mapOfFields.get(Fld) +',';
            }
        }
        Fieldz_To_Query = Fieldz_To_Query.removeEnd(',');
        system.debug('>>>>>Fieldz_To_Query -->'+Fieldz_To_Query);
        Fieldz_To_Query += ' ' + 'From Krow__Timesheet_Detail__c Where Id = \'a2Z210000005q2m\'';
        system.debug('>>>>> Time Sheet Detail --> '+Database.query(Fieldz_To_Query));
    }*/
}