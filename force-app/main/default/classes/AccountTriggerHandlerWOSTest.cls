/*
*	AccountTriggerHandlerWOSTest class : It contains test methods that tests Company Rating trigger logics.
*
*	Author: Philip
*	Date:   March 14, 2018
*
*/
@isTest
private class AccountTriggerHandlerWOSTest
{
	static final String COMPANY_QUALITY = 'Thumbs Up';
	static final String DEAL_TYPE = 'Majority';
	static final String DEAL_IMMINENCE = 'Imminent';
	static final String DEAL_SEC_IMMINENCE = 'Short To Medium';
	static final String DEAL_SIZE = 'S ($15-$35)';
	static final String DEAL_NOTES = 'TEST NOTES';
	static final Boolean RESPONSIVE = true;

	static final String CHANGED_COMPANY_QUALITY = 'Neutral';
	static final String CHANGED_DEAL_TYPE = 'Minority';
	static final String CHANGED_DEAL_IMMINENCE = 'Short to Medium';
	static final String CHANGED_DEAL_SIZE = 'M ($35-$75)';
	static final String CHANGED_DEAL_NOTES = 'TEST NOTES CHANGED';
	static final Boolean CHANGED_RESPONSIVE = false;
    
    public static Id ownershipPSetId{
        get{
            if(ownershipPSetId == NULL){
                List<PermissionSet> sets = [SELECT Id, Name FROM PermissionSet WHERE Name in ('IVP_Ownership') ORDER BY Name];
                if(!sets.isEmpty()){
                    ownershipPSetId = sets[0].Id;
                }
            }
            return ownershipPSetId;
        }set;
    }
	private static User setupTestUser( String profileName, String alias )
	{
		Profile p = [ SELECT Id FROM Profile WHERE Name = :profileName ];

		User u = new User( Alias = alias,
						   Email = alias + '@testorg.dev',
						   EmailEncodingKey = 'UTF-8',
						   LastName = 'Testing ' + alias,
						   LanguageLocaleKey='en_US',
						   LocaleSidKey = 'en_US',
						   ProfileId = p.Id, 
						   TimeZoneSidKey = 'America/Los_Angeles',
						   UserName = alias + '@testorg.dev' );

		insert u;
		if(ownershipPSetId != NULL){
		    System.debug('->assign');
            List<PermissionSetAssignment> setAssignments = new List<PermissionSetAssignment>{
                new PermissionSetAssignment(PermissionSetId=ownershipPSetId, AssigneeId=u.Id)
            };
            insert setAssignments;
        }
		return u;
	}

	@isTest static void testTrackCompanyRatings_UNASSIGNED_OWNER()
	{
		User testUser = setupTestUser( 'Analyst Full', 'standt' );
		User unassignedUser = setupTestUser( 'Analyst Full', 'unassign' );

		System.runAs(unassignedUser) {
		}
		
		System.runAs(testUser) {
		    System.debug('======'+userinfo.getUserName());
			Common_Config__c config = CommonConfigUtil.getConfig();
			Set<String> accountDealFields = new Set<String>();
			String accountDealFieldQueryStr = '';
			for( String dealField : config.Company_Rating_Fieldnames__c.split(',') )
			{
				accountDealFields.add( dealField.deleteWhitespace() );
				accountDealFieldQueryStr += dealField + ',';
			}
			accountDealFieldQueryStr = accountDealFieldQueryStr.removeEnd( ',' );
	
			Account a = new Account();
			a.Name = 'TestAccount';
			a.Website = 'http://www.testwebsite.com';
			a.Company_Quality__c = COMPANY_QUALITY;
//			a.Deal_Type__c = DEAL_TYPE;
			a.Deal_Imminence__c = DEAL_IMMINENCE;
			a.Deal_Size__c = DEAL_SIZE;
			a.Deal_Notes__c = DEAL_NOTES;
			a.Responsive__c = RESPONSIVE;
			a.OwnerId = testUser.Id;
			
			Test.startTest();
	
				insert a;
				List<Company_Rating__c> relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, Id FROM Company_Rating__c' );
				System.assertEquals( 1, relatedCompanyRatings.size() );
				System.assert( relatedCompanyRatings[0].Current_Rating__c );
				System.assertEquals( COMPANY_QUALITY, relatedCompanyRatings[0].Company_Quality__c );
//				System.assertEquals( DEAL_TYPE, relatedCompanyRatings[0].Deal_Type__c );
				System.assertEquals( DEAL_IMMINENCE, relatedCompanyRatings[0].Deal_Imminence__c );
				System.assertEquals( DEAL_SIZE, relatedCompanyRatings[0].Deal_Size__c );
				System.assertEquals( DEAL_NOTES, relatedCompanyRatings[0].Deal_Notes__c );
				System.assertEquals( RESPONSIVE, relatedCompanyRatings[0].Responsive__c );
	
				// a.OwnerId = [select id from User where alias='unassign' limit 1].Id;
				a.Loss_Drop_Reason__c ='Other';
				a.Loss_Drop_Notes__c='Tetsing';
				update a;
	
			Test.stopTest();
		}
	
		List<Company_Rating__c> updatedRatings = Database.query( 'SELECT Id, Current_Rating__c FROM Company_Rating__c' );
		System.assertEquals( 1, updatedRatings.size() );
		
		List<Account> updatedAccounts = Database.query( 'SELECT Id, Company_Quality__c, Deal_Type__c, Deal_Imminence__c, Deal_Size__c, Deal_Notes__c, Responsive__c FROM Account' );
		System.assertEquals( 1, updatedAccounts.size() );
		System.assert( String.isBlank( updatedAccounts[0].Company_Quality__c ) );
//		System.assert( String.isBlank( updatedAccounts[0].Deal_Type__c ) );
		System.assert( String.isBlank( updatedAccounts[0].Deal_Imminence__c ) );
		System.assert( String.isBlank( updatedAccounts[0].Deal_Size__c ) );
		System.assert( String.isBlank( updatedAccounts[0].Deal_Notes__c ) );
		System.assert( !updatedAccounts[0].Responsive__c );
	}

	@isTest static void testTrackCompanyRatings_NEW_OWNER()
	{
		User testUser = setupTestUser( 'Analyst Full', 'standt' );
		User newTestUser = setupTestUser( 'Analyst Full', 'standtwo' );

		System.runAs(newTestUser) {
		}
		
		System.runAs(testUser) {
		    
		    System.debug('======'+userinfo.getUserName());
			Common_Config__c config = CommonConfigUtil.getConfig();
			Set<String> accountDealFields = new Set<String>();
			String accountDealFieldQueryStr = '';
			for( String dealField : config.Company_Rating_Fieldnames__c.split(',') )
			{
				accountDealFields.add( dealField.deleteWhitespace() );
				accountDealFieldQueryStr += dealField + ',';
			}
			accountDealFieldQueryStr = accountDealFieldQueryStr.removeEnd( ',' );
	
			Account a = new Account();
			a.Name = 'TestAccount';
			a.Website = 'http://www.testwebsite.com';
			a.Company_Quality__c = COMPANY_QUALITY;
//			a.Deal_Type__c = DEAL_TYPE;
// 			a.Deal_Imminence__c = DEAL_IMMINENCE;
			a.Deal_Size__c = DEAL_SIZE;
			a.Deal_Notes__c = DEAL_NOTES;
			a.Responsive__c = RESPONSIVE;
			a.OwnerId = testUser.Id;
			
			Test.startTest();
	
				insert a;
				List<Company_Rating__c> relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, Id FROM Company_Rating__c' );
				System.assertEquals( 1, relatedCompanyRatings.size() );
				System.assert( relatedCompanyRatings[0].Current_Rating__c );
				System.assertEquals( COMPANY_QUALITY, relatedCompanyRatings[0].Company_Quality__c );
//				System.assertEquals( DEAL_TYPE, relatedCompanyRatings[0].Deal_Type__c );
				// System.assertEquals( DEAL_IMMINENCE, relatedCompanyRatings[0].Deal_Imminence__c );
				System.assertEquals( DEAL_SIZE, relatedCompanyRatings[0].Deal_Size__c );
				System.assertEquals( DEAL_NOTES, relatedCompanyRatings[0].Deal_Notes__c );
				System.assertEquals( RESPONSIVE, relatedCompanyRatings[0].Responsive__c );
	
				// a.OwnerId = newTestUser.Id;
				system.debug([select id, recordtype.Name, recordtypeId from Account where Id=:a.Id limit 1]);
				// a.Trigger_Update__c = System.now();
				a.Deal_Imminence__c = DEAL_IMMINENCE;
				update a;
				relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, Id FROM Company_Rating__c' );
	            System.assertEquals( DEAL_IMMINENCE, relatedCompanyRatings[0].Deal_Imminence__c );
				relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, Id FROM Company_Rating__c' );
				System.assertEquals( 1, relatedCompanyRatings.size() );
				
				List<Opportunity> oppoties = new List<Opportunity>();
				oppoties.add(new Opportunity(AccountId =a.Id, Name='test:'+a.Name, StageName ='Deferred' , CloseDate = System.today()));
				insert oppoties;
				// populating another value as need to opopulate the same on opportunity
			    a.Deal_Imminence__c = DEAL_SEC_IMMINENCE;
				update a;
	
			Test.stopTest();
	
			relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, OwnerId, Id FROM Company_Rating__c WHERE Current_Rating__c = true' );
			System.assertEquals( 1, relatedCompanyRatings.size() );
// 			System.assertEquals( newTestUser.Id, relatedCompanyRatings[0].OwnerId );
			System.assertEquals( COMPANY_QUALITY, relatedCompanyRatings[0].Company_Quality__c );
//			System.assertEquals( DEAL_TYPE, relatedCompanyRatings[0].Deal_Type__c );
			System.assertEquals( DEAL_IMMINENCE, relatedCompanyRatings[0].Deal_Imminence__c );
			System.assertEquals( DEAL_SIZE, relatedCompanyRatings[0].Deal_Size__c );
			System.assertEquals( DEAL_NOTES, relatedCompanyRatings[0].Deal_Notes__c );
			System.assertEquals( RESPONSIVE, relatedCompanyRatings[0].Responsive__c );
		}
	}

	@isTest static void testTrackCompanyRatings_DEAL_FIELD_CHANGED()
	{
		User testUser = setupTestUser( 'Analyst Full', 'standt' );
		System.runAs(testUser) {
			Common_Config__c config = CommonConfigUtil.getConfig();
			Set<String> accountDealFields = new Set<String>();
			String accountDealFieldQueryStr = '';
			for( String dealField : config.Company_Rating_Fieldnames__c.split(',') )
			{
				accountDealFields.add( dealField.deleteWhitespace() );
				accountDealFieldQueryStr += dealField + ',';
			}
			accountDealFieldQueryStr = accountDealFieldQueryStr.removeEnd( ',' );
	
			Account a = new Account();
			a.Name = 'TestAccount';
			a.Website = 'http://www.testwebsite.com';
			a.Company_Quality__c = COMPANY_QUALITY;
//			a.Deal_Type__c = DEAL_TYPE;
			a.Deal_Imminence__c = DEAL_IMMINENCE;
			a.Deal_Size__c = DEAL_SIZE;
			a.Deal_Notes__c = DEAL_NOTES;
			a.Responsive__c = RESPONSIVE;
			a.OwnerId = testUser.Id;
			
			Test.startTest();
	
				insert a;
				List<Company_Rating__c> relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, Id FROM Company_Rating__c' );
				System.assertEquals( 1, relatedCompanyRatings.size() );
				System.assert( relatedCompanyRatings[0].Current_Rating__c );
				System.assertEquals( COMPANY_QUALITY, relatedCompanyRatings[0].Company_Quality__c );
//				System.assertEquals( DEAL_TYPE, relatedCompanyRatings[0].Deal_Type__c );
				System.assertEquals( DEAL_IMMINENCE, relatedCompanyRatings[0].Deal_Imminence__c );
				System.assertEquals( DEAL_SIZE, relatedCompanyRatings[0].Deal_Size__c );
				System.assertEquals( DEAL_NOTES, relatedCompanyRatings[0].Deal_Notes__c );
				System.assertEquals( RESPONSIVE, relatedCompanyRatings[0].Responsive__c );
	
				a.Company_Quality__c = CHANGED_COMPANY_QUALITY;
//				a.Deal_Type__c = CHANGED_DEAL_TYPE;
				a.Deal_Imminence__c = CHANGED_DEAL_IMMINENCE;
				a.Deal_Size__c = CHANGED_DEAL_SIZE;
				a.Deal_Notes__c = CHANGED_DEAL_NOTES;
				a.Responsive__c = CHANGED_RESPONSIVE;
				update a;
	
				relatedCompanyRatings = Database.query( 'SELECT ' + accountDealFieldQueryStr + ', Current_Rating__c, Id FROM Company_Rating__c' );
				System.assertEquals( 1, relatedCompanyRatings.size() );
				System.assert( relatedCompanyRatings[0].Current_Rating__c );
				System.assertEquals( CHANGED_COMPANY_QUALITY, relatedCompanyRatings[0].Company_Quality__c );
//				System.assertEquals( CHANGED_DEAL_TYPE, relatedCompanyRatings[0].Deal_Type__c );
				System.assertEquals( CHANGED_DEAL_IMMINENCE, relatedCompanyRatings[0].Deal_Imminence__c );
				System.assertEquals( CHANGED_DEAL_SIZE, relatedCompanyRatings[0].Deal_Size__c );
				System.assertEquals( CHANGED_DEAL_NOTES, relatedCompanyRatings[0].Deal_Notes__c );
				System.assertEquals( CHANGED_RESPONSIVE, relatedCompanyRatings[0].Responsive__c );
	
			Test.stopTest();
		}
	}
}