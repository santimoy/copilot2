/**
 * Created by Ranjeet Kumar on 11/5/2017.
 */

public with sharing class InvoiceLItemTriggerHelper {

    public static void HandleAfterInsert(List<Krow__Invoice_Line_Item__c> InvoiceLIList){
        if(InvoiceLIList.size() > 0){
            List<Project_Hours__c> ProjectHourListToInsert = InvoiceLItemTriggerHelper.GenerateProjectHours(InvoiceLIList);
            if(ProjectHourListToInsert.size() > 0){
                insert ProjectHourListToInsert;
                system.debug('>>>>> Total number of Project Hour Inserted --> '+ProjectHourListToInsert.size());
            }
            List<Resource_Hours__c> ResHrList = InvoiceLItemTriggerHelper.GenerateResourceHours(InvoiceLIList);
            system.debug('>>>>> ResHrList size --> '+ResHrList.size());
            if(ResHrList.size() > 0){
                upsert ResHrList;
            }
        }
    }

    public static void HandleAfterUpdate(Map<Id,Krow__Invoice_Line_Item__c> OldMap, List<Krow__Invoice_Line_Item__c> InvoiceLIList){
        if(InvoiceLIList.size() > 0){
            //Below list is used to store those Invoice Lineitems whose Timesheet Split has been changed
            Set<Id> InvoiceIds = new Set<Id>();
            for(Krow__Invoice_Line_Item__c ili : InvoiceLIList){
                if( (OldMap.get(ili.Id).Krow__Hours__c != ili.Krow__Hours__c) ||
                    (OldMap.get(ili.Id).Krow__Project_Resource__c != ili.Krow__Project_Resource__c) ||
                    (OldMap.get(ili.Id).Krow__Timesheet_Split__c != ili.Krow__Timesheet_Split__c) ){

                    InvoiceIds.add(ili.Krow__Invoice__c);
                }
            }
            if(InvoiceIds.size() > 0){
                InvoiceLItemTriggerHelper.MatchRealtedRecords(InvoiceIds);
            }
        }
    }

    public static void HandleAfterDelete(List<Krow__Invoice_Line_Item__c> OldInvoiceLIList){
        if(OldInvoiceLIList.size() > 0){
            Set<Id> InvoiceIds = new Set<Id>();
            for(Krow__Invoice_Line_Item__c ili : OldInvoiceLIList){
                if(ili.Krow__Invoice__c != NULL){
                    InvoiceIds.add(ili.Krow__Invoice__c);
                }
            }
            if(InvoiceIds.size() > 0){
                InvoiceLItemTriggerHelper.MatchRealtedRecords(InvoiceIds);
            }
        }
    }

    public static List<Project_Hours__c> GenerateProjectHours(List<Krow__Invoice_Line_Item__c> InvoiceLIList){
        List<Project_Hours__c> ProjectHourListToInsert = new List<Project_Hours__c>();
        if(InvoiceLIList.size() > 0){
            Set<Id> TimeSheetSplitIds = new Set<Id>();
            Map<Id, List<Krow__Invoice_Line_Item__c>> InvoiceVsLineItemsList = new Map<Id, List<Krow__Invoice_Line_Item__c>>();
            Map<Id, Id> TssIdVsInvoiceId = new Map<Id, Id>();
            for(Krow__Invoice_Line_Item__c ili : InvoiceLIList){
                if(ili.Krow__Invoice__c != NULL){
                    if(!InvoiceVsLineItemsList.containsKey(ili.Krow__Invoice__c)){
                        List<Krow__Invoice_Line_Item__c> EmptyList = new List<Krow__Invoice_Line_Item__c>();
                        InvoiceVsLineItemsList.put(ili.Krow__Invoice__c, EmptyList);
                    }
                    InvoiceVsLineItemsList.get(ili.Krow__Invoice__c).add(ili);
                    if(ili.Krow__Timesheet_Split__c != NULL){
                        TimeSheetSplitIds.add(ili.Krow__Timesheet_Split__c);
                        TssIdVsInvoiceId.put(ili.Krow__Timesheet_Split__c, ili.Krow__Invoice__c);
                    }
                }
            }
            if(TimeSheetSplitIds.size() > 0){
                List<Project_Hours__c> ProjectHourList = new List<Project_Hours__c>();
                Set<String> UniquePHIds = new Set<String>();
                ProjectHourList = [Select Id, Krow_Project__c, Invoice__c From Project_Hours__c Where Invoice__c In : InvoiceVsLineItemsList.keySet()];
                if(ProjectHourList.size() > 0){
                    for(Project_Hours__c ProHour : ProjectHourList){
                        String PhUniqueId = ProHour.Invoice__c ;
                        PhUniqueId += ProHour.Krow_Project__c;
                        UniquePHIds.add(PhUniqueId);
                    }
                }
                List<Krow__Timesheet_Split__c> TimeSheetSplitList = new List<Krow__Timesheet_Split__c>();
                TimeSheetSplitList = [Select Id, Krow__Krow_Project__c From Krow__Timesheet_Split__c Where Id In : TimeSheetSplitIds ];
                if(TimeSheetSplitList.size() > 0){
                    for(Krow__Timesheet_Split__c TSS : TimeSheetSplitList){
                        if(TSS.Krow__Krow_Project__c != NULL && TssIdVsInvoiceId.containsKey(TSS.Id)){
                            Project_Hours__c ProHour = new Project_Hours__c ();
                            ProHour.Invoice__c = TssIdVsInvoiceId.get(TSS.Id);
                            ProHour.Krow_Project__c = TSS.Krow__Krow_Project__c;
                            String PhUniqueId = ProHour.Invoice__c ;
                            PhUniqueId += ProHour.Krow_Project__c;
                            if(!UniquePHIds.contains(PhUniqueId)){
                                ProjectHourListToInsert.add(ProHour);
                                UniquePHIds.add(PhUniqueId);
                            }
                        }
                    }
                }
            }
        }
        return ProjectHourListToInsert;
    }

    public static List<Resource_Hours__c> GenerateResourceHours(List<Krow__Invoice_Line_Item__c> InvoiceLIList){
        List<Resource_Hours__c> ResHourListToReturn = new List<Resource_Hours__c>();
        if(InvoiceLIList.size() > 0) {
            system.debug('>>>>> InvoiceLIList Size --> '+InvoiceLIList.size());
            Map<Id, List<Krow__Invoice_Line_Item__c>> InvoiceVsLineItemsList = new Map<Id, List<Krow__Invoice_Line_Item__c>>();
            Map<Id, Krow__Invoice_Line_Item__c> IdVSILISobj = new Map<Id, Krow__Invoice_Line_Item__c>();
            for (Krow__Invoice_Line_Item__c ili : InvoiceLIList) {
                //system.debug('>>>>> ili --> '+ili);
                if (ili.Krow__Invoice__c != NULL) {
                    if (!InvoiceVsLineItemsList.containsKey(ili.Krow__Invoice__c)) {
                        List<Krow__Invoice_Line_Item__c> EmptyList = new List<Krow__Invoice_Line_Item__c>();
                        InvoiceVsLineItemsList.put(ili.Krow__Invoice__c, EmptyList);
                    }
                    InvoiceVsLineItemsList.get(ili.Krow__Invoice__c).add(ili);
                    IdVSILISobj.put(ili.Id, ili);
                }
            }
            //Need to query becuse I need its related Krow Project
            List<Krow__Invoice_Line_Item__c> QueriedLILIList = new List<Krow__Invoice_Line_Item__c>();
            Map<Id, Krow__Invoice_Line_Item__c> IdVSILISobj_Queried = new Map<Id, Krow__Invoice_Line_Item__c>();
            QueriedLILIList =   [   Select Id, Krow__Timesheet_Split__c, Krow__Timesheet_Split__r.Krow__Krow_Project__c
                                    From Krow__Invoice_Line_Item__c Where Id In : IdVSILISobj.keySet()
                                ];
            if(QueriedLILIList.size() > 0){
                for (Krow__Invoice_Line_Item__c Q_ili : QueriedLILIList) {
                    //system.debug('>>>>> Q_ili --> '+Q_ili);
                    if(Q_ili.Krow__Timesheet_Split__c != NULL && Q_ili.Krow__Timesheet_Split__r.Krow__Krow_Project__c != NULL){
                        IdVSILISobj_Queried.put(Q_ili.Id, Q_ili);
                    }
                }
            }
            system.debug('>>>>> QueriedLILIList Size--> '+QueriedLILIList.size());
            system.debug('>>>>> IdVSILISobj_Queried Size--> '+IdVSILISobj_Queried.size());
            if (IdVSILISobj_Queried.size() > 0) {
                List<Project_Hours__c> ProjectHourList = new List<Project_Hours__c>();
                Map<String, Id> UniqueKeyVsProHour = new Map<String, Id>();
                ProjectHourList = [Select Id, Invoice__c, Krow_Project__c From Project_Hours__c Where Invoice__c In : InvoiceVsLineItemsList.keySet()];
                if(ProjectHourList.size() > 0){
                    for(Project_Hours__c proHour : ProjectHourList){
                        String UniqueKey = proHour.Invoice__c ;
                        UniqueKey += proHour.Krow_Project__c;
                        UniqueKeyVsProHour.put(UniqueKey, proHour.Id);
                    }
                }
                List<Resource_Hours__c> ResourceHourList = new List<Resource_Hours__c>();
                Map<String, Resource_Hours__c> UniqueKeyVsExistingResHour = new Map<String, Resource_Hours__c>();
                ResourceHourList = [    Select Id, Hours__c, Total_Amount__c, Project_Hours__c, Project_Resource__c, Project_Hours__r.Invoice__c, Project_Hours__r.Krow_Project__c
                                        From Resource_Hours__c
                                        Where Project_Hours__r.Invoice__c In :InvoiceVsLineItemsList.keySet()
                                    ];
                if(ResourceHourList.size() > 0){
                    for(Resource_Hours__c ResHour : ResourceHourList){
                        String UniqueKey = ResHour.Project_Hours__r.Invoice__c ;
                        UniqueKey += ResHour.Project_Hours__r.Krow_Project__c ;
                        UniqueKey += ResHour.Project_Resource__c;
                        UniqueKeyVsExistingResHour.put(UniqueKey, ResHour);
                    }
                }
                system.debug('>>>>> ResourceHourList Size --> ' + ResourceHourList.size());

                for(Krow__Invoice_Line_Item__c InvoiceLI : InvoiceLIList){
                    if(IdVSILISobj_Queried.containsKey(InvoiceLI.Id) && InvoiceLI.Krow__Project_Resource__c != NULL){
                        String UniqueKey = InvoiceLI.Krow__Invoice__c;
                        UniqueKey += IdVSILISobj_Queried.get(InvoiceLI.Id).Krow__Timesheet_Split__r.Krow__Krow_Project__c;
                        UniqueKey += InvoiceLI.Krow__Project_Resource__c;
                        if(UniqueKeyVsExistingResHour.containsKey(UniqueKey)){
                            system.debug('>>>>> Resource Exist --> ');
                            Resource_Hours__c ExistResHr = UniqueKeyVsExistingResHour.get(UniqueKey);
                            if(ExistResHr.Hours__c == NULL){
                                ExistResHr.Hours__c = 0.00;
                            }
                            if(ExistResHr.Total_Amount__c == NULL){
                                ExistResHr.Total_Amount__c = 0.00;
                            }
                            if(InvoiceLI.Krow__Hours__c == NULL){
                                InvoiceLI.Krow__Hours__c = 0.00;
                            }
                            if(InvoiceLI.Krow__Amount_Total__c != NULL){
                                ExistResHr.Total_Amount__c += InvoiceLI.Krow__Amount_Total__c;
                            }
                            ExistResHr.Hours__c += InvoiceLI.Krow__Hours__c;
                            UniqueKeyVsExistingResHour.put(UniqueKey, ExistResHr);
                        }
                        else {
                            system.debug('>>>>> Resource don\'t Exist --> ');
                            String UniqueKeyForPH = InvoiceLI.Krow__Invoice__c;
                            UniqueKeyForPH += IdVSILISobj_Queried.get(InvoiceLI.Id).Krow__Timesheet_Split__r.Krow__Krow_Project__c;
                            if(UniqueKeyVsProHour.containsKey(UniqueKeyForPH)){
                                Resource_Hours__c NewResHr = new Resource_Hours__c ();
                                NewResHr.Project_Hours__c = UniqueKeyVsProHour.get(UniqueKeyForPH);
                                NewResHr.Project_Resource__c = InvoiceLI.Krow__Project_Resource__c;
                                if(NewResHr.Hours__c == NULL){
                                    NewResHr.Hours__c = 0.00;
                                }
                                if(InvoiceLI.Krow__Hours__c == NULL){
                                    InvoiceLI.Krow__Hours__c = 0.00;
                                }
                                if(InvoiceLI.Krow__Amount_Total__c != NULL){
                                    NewResHr.Total_Amount__c = InvoiceLI.Krow__Amount_Total__c;
                                }
                                NewResHr.Hours__c += InvoiceLI.Krow__Hours__c;
                                UniqueKeyVsExistingResHour.put(UniqueKey, NewResHr);
                            }
                        }
                        system.debug('>>>>> Invoice Line Item Id --> '+InvoiceLI.Id);
                        system.debug('>>>>> RH Amount --> '+UniqueKeyVsExistingResHour.get(UniqueKey).Total_Amount__c);
                    }
                }
                system.debug('>>>>> UniqueKeyVsExistingResHour Size--> '+UniqueKeyVsExistingResHour.size());
                if(UniqueKeyVsExistingResHour.size() > 0){
                    ResHourListToReturn = UniqueKeyVsExistingResHour.values();
                }

            }
        }
        return ResHourListToReturn;
    }

    public static void MatchRealtedRecords(Set<Id> InvoiceIds){
        if(InvoiceIds.size() > 0){
            system.debug('>>>>> InvoiceIds Size --> '+InvoiceIds.size());
            List<Krow__Invoice_Line_Item__c> ILItemsList = new List<Krow__Invoice_Line_Item__c>();
            ILItemsList =   [   Select Id, Krow__Hours__c, Krow__Amount_Total__c, Krow__Project_Resource__c, Krow__Invoice__c, Krow__Timesheet_Split__c,
                                Krow__Timesheet_Split__r.Krow__Krow_Project__c From Krow__Invoice_Line_Item__c
                                Where Krow__Invoice__c In : InvoiceIds
                            ];
            List<Project_Hours__c> ProjectHourList = new List<Project_Hours__c>();
            Map<String, Project_Hours__c> UniqueKeyVsProHrs = new Map<String, Project_Hours__c>();
            ProjectHourList = [Select Id, Krow_Project__c, Invoice__c From Project_Hours__c Where Invoice__c In : InvoiceIds];
            if(ProjectHourList.size() > 0){
                for(Project_Hours__c proHrs : ProjectHourList){
                    String UniqueKey = proHrs.Invoice__c;
                    UniqueKey += proHrs.Krow_Project__c;
                    UniqueKeyVsProHrs.put(UniqueKey, proHrs);
                }
            }

            List<Resource_Hours__c> ResourceHourList = new List<Resource_Hours__c>();
            Map<String, Resource_Hours__c> UniqueKeyVsResHrs = new Map<String, Resource_Hours__c>();
            ResourceHourList =  [   Select Id, Project_Hours__c, Project_Hours__r.Invoice__c, Project_Hours__r.Krow_Project__c, Project_Resource__c,
                                    Hours__c,Total_Amount__c From Resource_Hours__c Where Project_Hours__r.Invoice__c In : InvoiceIds
                                ];
            if(ResourceHourList.size() > 0){
                for(Resource_Hours__c ResHour : ResourceHourList){
                    String UniqueKey = ResHour.Project_Hours__r.Invoice__c;
                    UniqueKey += ResHour.Project_Hours__r.Krow_Project__c;
                    UniqueKey += ResHour.Project_Resource__c;
                    ResHour.Hours__c = 0.00;
                    ResHour.Total_Amount__c = 0.00;
                    UniqueKeyVsResHrs.put(UniqueKey, ResHour);
                }
            }
            List<Krow__Invoice_Line_Item__c> ILIListForPHInsertion = new List<Krow__Invoice_Line_Item__c>();
            List<Krow__Invoice_Line_Item__c> ILIListForRHInsertion = new List<Krow__Invoice_Line_Item__c>();
            Set<String> PHUniqueKeySet = new Set<String>();
            Set<String> RHUniqueKeySet = new Set<String>();

            for(Krow__Invoice_Line_Item__c ili : ILItemsList){
                String KeyForPH = ili.Krow__Invoice__c;
                KeyForPH += ili.Krow__Timesheet_Split__r.Krow__Krow_Project__c;

                String KeyForRH = ili.Krow__Invoice__c;
                KeyForRH += ili.Krow__Timesheet_Split__r.Krow__Krow_Project__c;
                KeyForRH += ili.Krow__Project_Resource__c;

                Boolean PHExist = false; //PH - Project Hour
                Boolean RHExist = false; //RH - Resource Hour

                if(UniqueKeyVsProHrs.containsKey(KeyForPH)){
                    PHUniqueKeySet.add(KeyForPH);
                    PHExist = true;
                }
                if(UniqueKeyVsResHrs.containsKey(KeyForRH)){
                    RHUniqueKeySet.add(KeyForRH);
                    RHExist = true;
                    Resource_Hours__c ExistingRH = UniqueKeyVsResHrs.get(KeyForRH);
                    if(ili.Krow__Hours__c == NULL){
                        ili.Krow__Hours__c = 0.00;
                    }
                    ExistingRH.Hours__c += ili.Krow__Hours__c;
                    if(ili.Krow__Amount_Total__c != NULL){
                        ExistingRH.Total_Amount__c += ili.Krow__Amount_Total__c;
                    }
                    UniqueKeyVsResHrs.put(KeyForRH, ExistingRH);
                }
                if(!PHExist){
                    ILIListForPHInsertion.add(ili);
                }
                if(!RHExist){
                    ILIListForRHInsertion.add(ili);
                }
            }
            system.debug('>>>>> UniqueKeyVsResHrs Size --> '+UniqueKeyVsResHrs.size());
            if(UniqueKeyVsResHrs.size() > 0){
                update UniqueKeyVsResHrs.values();
            }
            system.debug('>>>>> ILIListForPHInsertion Size --> '+ILIListForPHInsertion.size());
            system.debug('>>>>> ILIListForRHInsertion Size --> '+ILIListForRHInsertion.size());
            if(UniqueKeyVsProHrs.size() != PHUniqueKeySet.size()){
                List<Project_Hours__c> PHListToDelete = new List<Project_Hours__c>();
                for(String PhCustomKey : UniqueKeyVsProHrs.keySet()){
                    if(!PHUniqueKeySet.contains(PhCustomKey)){
                        PHListToDelete.add(UniqueKeyVsProHrs.get(PhCustomKey));
                    }
                }
                if(PHListToDelete.size() > 0){
                    delete PHListToDelete;
                }
            }
            if(UniqueKeyVsResHrs.size() != RHUniqueKeySet.size()){
                List<Resource_Hours__c> RHListToDelete = new List<Resource_Hours__c>();
                for(String RHCustomKey : UniqueKeyVsResHrs.keySet()){
                    if(!RHUniqueKeySet.contains(RHCustomKey)){
                        RHListToDelete.add(UniqueKeyVsResHrs.get(RHCustomKey));
                    }
                }
                if(RHListToDelete.size() > 0){
                    Database.delete(RHListToDelete, false);
                }
            }
            if(ILIListForPHInsertion.size() > 0){
                List<Project_Hours__c> ProjectHourListToInsert = new List<Project_Hours__c>();
                ProjectHourListToInsert = InvoiceLItemTriggerHelper.GenerateProjectHours(ILIListForPHInsertion);
                if(ProjectHourListToInsert.size() > 0){
                    insert ProjectHourListToInsert;
                }
            }
            if(ILIListForRHInsertion.size() > 0){
                List<Resource_Hours__c> ResourceHourListAll = new List<Resource_Hours__c>();
                ResourceHourListAll = InvoiceLItemTriggerHelper.GenerateResourceHours(ILIListForRHInsertion);
                if(ResourceHourListAll.size() > 0){
                    upsert ResourceHourListAll;
                }
            }
        }
    }

    public static void DebugLimits(){
        system.debug('>>>>> SOQL Queries USed --> '+Limits.getQueries());
        system.debug('>>>>> DMLs Used --> '+Limits.getDMLStatements());
        system.debug('>>>>> Aggregate Queries Used--> '+Limits.getAggregateQueries());
    }
}