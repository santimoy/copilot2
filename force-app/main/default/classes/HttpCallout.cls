public class HttpCallout {
    public static Boolean TEST_BYPASS = false;
    public static LoggerWrapper makeCallout(String endPoint, String method, String reqBody, Integer successStatus, String className, String methodName) {
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(endPoint);
        req1.setMethod(method);
        req1.setHeader('Content-Type','application/json');
        // need to keep max timeout is 30 sec
        req1.setTimeout(30000);
        if(method == 'POST') {
            req1.setBody(reqBody);
        }
        Http http1 = new Http();
        if(TEST_BYPASS){
            LoggerWrapper logWrap = new LoggerWrapper();
            logWrap.isApiError = false ;
            logWrap.responseBody = '{"Last_Validated":"2019-05-09 05:55:38 UTC","execution_time":"461 ms","validation_status":"Invalid","Recent_Validation_Error":""}';
            logWrap.responseCode = 200;
            return logWrap;
        }
        return HttpCallout.getLoggerResponse(http1, req1, methodName, className, successStatus);
    }
     private static LoggerWrapper getLoggerResponse(Http http1, HttpRequest req1, String methodName, String className, Integer successStatus) {
        Logger.push(methodName , className);
        LoggerWrapper logWrap = new LoggerWrapper();
        logWrap.isApiError = false ;
        logWrap.responseBody = '';
        HTTPResponse res1;
        try{
            res1 = http1.send(req1);
            logWrap.responseCode = res1.getStatusCode();
            if(res1 != null && res1.getStatusCode() == successStatus){
                logWrap.responseBody = res1.getBody();
                System.debug('--->'+logWrap.responseBody);
            }else{
                // in case of expected status code, didn't match
                // service unavailable
                System.debug('-wrong status');
                logWrap.isApiError = true;
                logWrap.apiError = Label.DWH_API_Time_Out;
                logWrap.readTimeout = false;
                String errorMsg = logDetails(req1, res1.getStatus());
                if(errorMsg!=''){
                    logWrap.apiError = errorMsg;
                }
            }
        }catch(Exception excp){
            // read time out.
            logWrap.isApiError = true ;
            logWrap.apiError = Label.DWH_API_Time_Out;
            logWrap.readTimeout = true;
            string status = '';
            if(res1 != null){
               status = res1.getStatus();
            } else {
                status = excp.getMessage();
            }
            String errorMsg = logDetails(req1, status);

        }
        return logWrap;
    }
    static String logDetails(HttpRequest req, String status){
        Datetime start = System.now();
        status = String.isNotEmpty(status) ? status : '';
        Map<String, Object> data = new Map<String, object>{
            'body'=>req.getBody(),
                'method'=>req.getMethod(),
                'endpoint'=>req.getEndpoint(),
                'status'=>status
                };
        Logger.debugException(JSON.serialize(data));
        //Logger.debugException(excp);
        Logger.pop();
        Datetime ends = System.now();
        List<Log__c> logs = [SELECT Id,Name FROM Log__c 
                             WHERE CreatedById=:UserInfo.getUserId()
                             AND CreatedDate>=:start AND CreatedDate<=:ends];
        if(!logs.isEmpty()){
            String customStatus = 'Read timed out';
            if(String.isNotEmpty(status) && status.toLowerCase() != customStatus.toLowerCase()){
                return '"refId":\"'+String.escapeSingleQuotes(logs[0].Name)+'\", '+status;
            }else{
                return '"refId":\"'+String.escapeSingleQuotes(logs[0].Name)+'\", '+Label.DWH_API_Time_Out;
            }
        }
        return String.isNotEmpty(status) ? status : '';
    } 
}