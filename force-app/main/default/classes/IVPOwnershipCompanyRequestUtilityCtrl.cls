/***********************************************************************************
 * @author 10k-Expert
 * @name IVPOwnershipCompanyRequestUtilityCtrl
 * @date 2018-11-19
 * @description Controller of IVPOwnershipCompanyRequestUtility component
 ***********************************************************************************/
public class IVPOwnershipCompanyRequestUtilityCtrl {
    
    /******************************************************************************
     * @description returning list of company requests those are realted to current user
     * @return List of holiday 
     * @author 10K-Expert
     * @param nothing
     *******************************************************************************/
    @AuraEnabled
    public static ValidationWrapper getCompanyRequests(){
        ValidationWrapper validationWrapper = new ValidationWrapper();
        try{
            validationWrapper = IVPOwnershipService.checkUnassignedValidation(new List<Id>{UserInfo.getUserId()}).get(UserInfo.getUserId());
            List<Company_Request__c> companyRequestLst = [SELECT Id, CreatedDate, Request_Processing_Hours__c, Company__c, Company__r.Name, Request_By__c, Request_By__r.Name, Request_To__c, Request_To__r.Name, Request_Expire_On__c, Status__c, Comment_By_Owner__c, Comment_By_Requester__c
                                                          FROM Company_Request__c
                                                          WHERE Request_By__c = :UserInfo.getUserId() OR Request_To__c = :UserInfo.getUserId() 
                                                          ORDER BY CreatedDate DESC
                                                          LIMIT 5000];
            if(!companyRequestLst.isEmpty()){
                validationWrapper.requests = companyRequestLst;
            }
        }catch(Exception excp){
            // throwing from here as we need to handle and extract message over loghtning component.
            throw new AuraHandledException(excp.getMessage());
        }
        return validationWrapper;
    }
    
    /******************************************************************************
     * @description returning list of holidays upto 50k in the order of latest holidays from the last 1 months as need to calculte time over lighting compnoent
     * @return List of holiday 
     * @author 10K-Expert
     * @param nothing
     *******************************************************************************/
    //We don't need it now
    /*@AuraEnabled
    public static List<Holiday> getHolidays(){
        Date oneMonthsAgo = System.today().addMonths(-1);
        return [SELECT Id, ActivityDate, StartTimeInMinutes, EndTimeInMinutes, IsAllDay FROM Holiday
                WHERE ActivityDate >=: oneMonthsAgo
                ORDER BY ActivityDate DESC
                LIMIT 50000];
    }*/
    
    /******************************************************************************
     * @description Changing Company_Request's status
     * @return ValidationWrapper data
     * @author 10K-Expert
     * @param request's Id, new status, note related to that request
     *******************************************************************************/
    @AuraEnabled
    public static ValidationWrapper changeRequestStatus(Id recordId, String status, String note){
        //update new Company_Request__c(Id=recordId, Comment_By_Owner__c=note, Status__c=status);
        ValidationWrapper validationWrapper1 = new ValidationWrapper();
        try{
            if(status == 'Accepted'){
                validationWrapper1 = TaskCreationController.acceptRequest(recordId, status, note);
            }
            else if(status == 'Rejected'){
                validationWrapper1 = TaskCreationController.rejectRequest(recordId, note);
            }
            ValidationWrapper validationWrapper2 = IVPOwnershipService.checkUnassignedValidation(new List<Id>{UserInfo.getUserId()}).get(UserInfo.getUserId());
            validationWrapper1.untouchableCount = validationWrapper2.untouchableCount;
        }catch(AuraHandledException auraExcp){
            throw auraExcp;
        }catch(Exception excp){
            throw new AuraHandledException(excp.getMessage());
        }
        return validationWrapper1;
    }
    
    @AuraEnabled
    public static Boolean getByPassValue()
    {
       /************
        * Checking if the user having bypass and if yes then we are bypassing the validation checks
    ************/
       Map<Id,boolean> usersWithBypass = IVPOwnershipService.checkUserHavingBypass(new Set<Id>{UserInfo.getUserId()});        
       Boolean byPassOwnershipValidation = usersWithBypass.containsKey(UserInfo.getUserId()) && usersWithBypass.get(UserInfo.getUserId())!=Null?usersWithBypass.get(UserInfo.getUserId()):false;
       System.debug(byPassOwnershipValidation);
       return byPassOwnershipValidation;
    }
    
    /******************************************************************************
     * @description returning list of Company_Request records
     * @return List Company_Request__c
     * @author 10K-Expert
     * @param request's Id
     *******************************************************************************/
    @AuraEnabled
    public static ValidationWrapper getCompanyRequest(Id recordId){
        ValidationWrapper validationWrapper = new ValidationWrapper();
        try{
            validationWrapper = IVPOwnershipService.checkUnassignedValidation(new List<Id>{UserInfo.getUserId()}).get(UserInfo.getUserId());
            List<Company_Request__c> companyRequestLst = [SELECT Id, CreatedDate, Request_Processing_Hours__c, Company__c, Company__r.Name, Request_By__c, Request_By__r.Name, Request_To__c, Request_To__r.Name, Request_Expire_On__c, 
                                                          Status__c, Comment_By_Owner__c, Comment_By_Requester__c
                                                          FROM Company_Request__c
                                                          WHERE Id =:recordId];
            if(!companyRequestLst.isEmpty()){
                validationWrapper.requests = companyRequestLst;
            }
        }catch(Exception excp){
            // throwing from here as we need to handle and extract message over loghtning component.
            throw new AuraHandledException(excp.getMessage());
        }
        return validationWrapper;
    }
    
    /******************************************************************************
     * @description preparing details related to Company request balance, date
     * @return ValidationWrapper
     * @author 10K-Expert
     * @param nothing
     *******************************************************************************/
    @AuraEnabled
    public static ValidationWrapper getRemainingBalance(){
        ValidationWrapper validationWrapper = new ValidationWrapper();
        try{
            Integer ownershipRequestBalanceDays = IVPOwnershipService.ownershipRequestBalanceDays;
            DateTime last30daysDateTime = DateTime.now().addDays(-ownershipRequestBalanceDays);
            List<Company_Request__c> companyRequestLst = [SELECT Id, Request_By__c, CreatedDate
                                                          FROM Company_Request__c
                                                          WHERE CreatedDate >= :last30daysDateTime AND Request_By__c = :UserInfo.getUserId()
                                                          ORDER BY CreatedDate
                                                          LIMIT 1];

      Integer requestBal = IVPOwnershipService.ownershipRequestBalance - IVPOwnershipService.getUserWithRequestCount(new List<Id>{UserInfo.getUserId()}).get(UserInfo.getUserId());            
            System.debug('requestBalance::'+requestBal);
            validationWrapper.requestBalance = requestBal < 0 ? 0 : requestBal;
            // this cannot be negative value. 16-JAN meeting
            ownershipRequestBalanceDays = ownershipRequestBalanceDays<0 ? 0 : ownershipRequestBalanceDays;
            if(!companyRequestLst.isEmpty()){
                validationWrapper.requestBalanceUpdateDate = companyRequestLst[0].CreatedDate.addDays(ownershipRequestBalanceDays).format('M/d/YYYY');
            }
            else{
                validationWrapper.requestBalanceUpdateDate = System.now().addDays(ownershipRequestBalanceDays).format('M/d/YYYY');
            }
            /*may be we need in future
            validationWrapper.ownershipRequestBalance = IVPOwnershipService.ownershipRequestBalance;
            validationWrapper.ownershipRequestBalanceDays = IVPOwnershipService.ownershipRequestBalanceDays;*/
        }catch(Exception excp){
            throw new AuraHandledException(excp.getMessage());
        }
        return validationWrapper;
    }
}