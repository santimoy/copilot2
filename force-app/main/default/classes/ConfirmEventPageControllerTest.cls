@isTest
public class ConfirmEventPageControllerTest {
    @testsetup
    public static void testData() {
        SL_LIGHT_RSVP__SL_Event_For_RSVP__c event = new SL_LIGHT_RSVP__SL_Event_For_RSVP__c();
        event.Name = 'Test Event';
        event.SL_LIGHT_RSVP__Capacity__c = 1;
        event.SL_LIGHT_RSVP__Start_DateTime__c = DateTime.now();
        event.SL_LIGHT_RSVP__End_DateTime__c = DateTime.now();
        insert event;
    }
    
    @isTest
    public static void test_InitPage() {
        SL_LIGHT_RSVP__SL_Event_For_RSVP__c event = [SELECT Id FROM SL_LIGHT_RSVP__SL_Event_For_RSVP__c];
        
        PageReference pageRef = Page.ConfirmEventPage;
        pageRef.getParameters().put('ObjectId', event.Id);
        pageRef.getParameters().put('UserEmail', UserInfo.getUserEmail());
        pageRef.getParameters().put('ResponseCode', 'True');
        Test.setCurrentPage(pageRef);
        
        ConfirmEventPageController controller = new ConfirmEventPageController();
        
        controller.InitPage();
        List<SL_LIGHT_RSVP__SL_Event_Invitees__c> invites = [Select Id from SL_LIGHT_RSVP__SL_Event_Invitees__c];
        system.assertEquals(1, invites.size());
        system.assertEquals('You are now registered for the event.', controller.ResponseMessage);
        
        controller.InitPage();
        invites = [Select Id from SL_LIGHT_RSVP__SL_Event_Invitees__c];
        system.assertEquals(1, invites.size());
        system.assertEquals('The event is at capacity.', controller.ResponseMessage);
    }
}