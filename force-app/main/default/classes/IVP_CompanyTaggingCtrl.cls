/**
* @author 10k advisor
* @date 2018-04-23
* @className CompanyTaggingCtrl
* @group IVPSmartDash
*
* @description contains methods to handle IVPTagging functionality
*/

public class IVP_CompanyTaggingCtrl {
    public class TagResponse{
        public String topicName;
        public String topicId;
    }

    
    @AuraEnabled
    public static Map<String,Object> 		getTopicsList(String accountIds){
        Map<String,Object> mapOfTopics 		= new Map<String,Object>();
        Map<String,Object> mapOfResult      = new Map<String,Object>();
        mapOfTopics.put('exception','false');
       
            List<Id> lstOfAcctIds 				= (List<Id>)accountIds.split(',');
            System.debug('lstOfAcctIds'+lstOfAcctIds);
            List<Account> selectedAccounts 		= [SELECT 
                                                   Id,OwnerId,Tags__c,Name 
                                                   FROM Account 
                                                   WHERE Id in :lstOfAcctIds];
            List<Account> lstOfFilterAccounts  	= filterAccountByOwner(selectedAccounts,false); 
            
            if(lstOfFilterAccounts.isEmpty()){
                mapOfTopics.put('canApplyTag','false');         
            }else{
                mapOfTopics.put('canApplyTag','true');
            }
            mapOfResult                         = getFilterTopics('');
            mapOfTopics.put('lstOfTopics',		mapOfResult.get('lstOfTopics'));
            mapOfTopics.put('lstOfTopicsOthers',mapOfResult.get('lstOfTopicsOthers'));
            mapOfTopics.put('lstOfTopicsRemove',getTopicsListForRemove(accountIds));
        
        return mapOfTopics;
    }
    
    @AuraEnabled
    public static List<Topic> getTopicsListForRemove(String accountIds){
        List<Id> lstOfAcctIds 				= (List<Id>)accountIds.split(',');
        Map<String,Object> mapOFObject 		= new Map<String,Object>();
        Set<Id> topicsId 					= new Set<Id>();
        List<Topic> lstOfTopics 			= new List<Topic>();
        List<Account> selectedAccounts 		= [SELECT 
                                               Id,OwnerId,Tags__c,Name 
                                               FROM Account 
                                               WHERE Id in :lstOfAcctIds];
        List<Account> lstOfFilterAccounts  	= filterAccountByOwner(selectedAccounts,true);       
        
        if(!lstOfFilterAccounts.isEmpty()){
            Set<Id> accountIdsSet = new Map<Id,Account>(lstOfFilterAccounts).keySet();
            List<TopicAssignment> listTopicAssignment = [SELECT 
                                                         Id,EntityId,TopicId,Topic.Name 
                                                         FROM TopicAssignment 
                                                         WHERE EntityId in :accountIdsSet];
            if(!listTopicAssignment.isEmpty()){
                for(TopicAssignment tpcAssignment : listTopicAssignment){
                    if(!topicsId.contains(tpcAssignment.TopicId)){
                        topicsId.add(tpcAssignment.TopicId);
                        lstOfTopics.add(new Topic(Id=tpcAssignment.TopicId,
                                                  Name=tpcAssignment.Topic.Name));        
                    }
                }
            }
        }
        
        return lstOfTopics;
    }
    
    @AuraEnabled
    public static String addTagToSelectedAccounts(String accountIds,Topic selectedTopicToTag){
        List<Id> lstOfAcctIds 							= (List<Id>)accountIds.split(',');
        Map<Id,String> mapOfPassRecords 				= new Map<Id,String>();
        Map<Id,Map<String,String>> mapOfFailRecords  	= new Map<Id,Map<String,String>>();
        List<TopicAssignment> lstOfTopicAssignment 		= new List<TopicAssignment>();
       
        Map<Id,Account> selectedAccount 				= new Map<Id,Account>([SELECT
                                                                               Id,OwnerId,Tags__c,Name 
                                                                               FROM Account 
                                                                               WHERE Id in :lstOfAcctIds]);
        Map<Id,Account> filterAccounts  				= new Map<Id,Account>(filterAccountByOwner(selectedAccount.values(),false));
        
        for(Id compId : selectedAccount.keySet()){
            if(!filterAccounts.containsKey(compId)){
                mapOfFailRecords.put(compId,new Map<String,String>{
                    	selectedAccount.get(compId).Name => 'You are not the owner of this company or not owned by unassigned user.'
                        });
            }
        }
        
        Integer countFailCompanies 						= selectedAccount.size()-filterAccounts.size();
        Set<Id> setAccountIds 							= new Set<Id>();
        String limitExceedAccounts = '';
        
        for(Account acctObj : filterAccounts.values()){
            acctObj.Tags__c = acctObj.Tags__c == null ? '' :acctObj.Tags__c;
            if( acctObj.Tags__c != '' && selectedTopicToTag.Name.length()+acctObj.Tags__c.length()<=255 ){
                List<String> lstOfTagsInAccount = acctObj.Tags__c.split(';');
                if(! lstOfTagsInAccount.contains(selectedTopicToTag.Name)){
                    lstOfTopicAssignment.add(new TopicAssignment( EntityId=acctObj.Id,
                                                                  TopicId=selectedTopicToTag.Id ) );
                }
            }else if(acctObj.Tags__c == ''){
                lstOfTopicAssignment.add(new TopicAssignment( EntityId=acctObj.Id,TopicId=selectedTopicToTag.Id ) );
            }else{
                limitExceedAccounts +=acctObj.Name + '&';
            }
        }
        
        if(!lstOfTopicAssignment.isEmpty()){
            insert lstOfTopicAssignment;     
        }
        return '{"isSuccess":"true","isLimitExceed":"false","limitExceedCompanies":"","countFailCompanies":"'+
            	countFailCompanies+'","countSuccessCompanies":"'+(lstOfAcctIds.size()-countFailCompanies)+'"}';           
    }
    
    @AuraEnabled
    public static String insertNewTopic(String newTopicName,String accountIds){
        Integer countFailCompanies						= 0;
        Map<Id,String> mapOfPassRecords 				= new Map<Id,String>();
        Map<Id,Map<String,String>> mapOfFailRecords   	= new Map<Id,Map<String,String>>();
        List<Id> lstOfAcctIds 							= (List<Id>)accountIds.split(',');
        Map<Id,Account> selectedAccount 				= new Map<Id,Account>([SELECT 
                                                                               Id,OwnerId,Tags__c,Name 
                                                                               FROM Account 
                                                                               WHERE Id in :lstOfAcctIds]);
        Map<Id,Account> filterAccounts  				= new Map<Id,Account>(filterAccountByOwner(selectedAccount.values(),false));
        
        for(Id compId : selectedAccount.keySet()){
            if(!filterAccounts.containsKey(compId)){
                mapOfFailRecords.put(compId,new Map<String,String>{
                    	selectedAccount.get(compId).Name => 'You are not the owner of this company or not owned by unassigned user.'
                        });
            }
        }
        countFailCompanies = selectedAccount.size()-filterAccounts.size();
        List<TopicAssignment> lstOfTopicAssignment 		= new List<TopicAssignment>();
        List<Topic> checkTopic 							= [SELECT 
                                                          Id,Name 
                                                          FROM Topic 
                                                          WHERE Name = :newTopicName.trim()];
        Topic newTopic ;
        if(checkTopic.size()==0){
            newTopic = new Topic(Name=newTopicName.trim());
            insert newTopic;
        }else{
            newTopic = checkTopic[0];
        }
        
        for(Account acctObject : filterAccounts.values()){
            lstOfTopicAssignment.add(new TopicAssignment( EntityId=acctObject.Id,TopicId=newTopic.Id ) );
        }
        insert lstOfTopicAssignment;       
        //sendMail(true,mapOfFailRecords,mapOfPassRecords);
        return '{"isSuccess":"true","isLimitExceed":"false","limitExceedCompanies":"","countFailCompanies":"'+
            	countFailCompanies+'","countSuccessCompanies":"'+(lstOfAcctIds.size()-countFailCompanies)+'"}';                 
    }
    
    @AuraEnabled
    public static Map<String,Object> getFilterTopics(String searchString){
        List<Topic> lstOfTopicsSelf 		= new List<Topic>();
        List<Topic> lstOfTopicsOthers 		= new List<Topic>();
        Integer selfSize 					= 1000;
        Integer otherSize 					= 0;
        searchString 						= '%'+searchString+'%';        
        Id userId							= UserInfo.getUserId();
	        
        List<Id> lstOfTopicIds 				= new List<Id>();
        Set<Id> topicsIds 					= new Set<Id>();
        
        AggregateResult[] groupedResults  = [SELECT count(Id) topicCount, TopicId,Topic.Name tname
                                                     FROM TopicAssignment 
                                                     WHERE CreatedById =:userId  
                                                     GROUP BY TopicId,Topic.Name 
                                                     ORDER BY count(TopicId) DESC
                                                     Limit :selfSize ];
        
        for (AggregateResult ar : groupedResults)  {
            lstOfTopicsSelf.add(new Topic(Id = (Id)ar.get('TopicId'), 
                                      Name=(String)ar.get('tname')) );
            topicsIds.add( (Id)ar.get('TopicId'));
        }
        
        if(groupedResults.size() < selfSize){
            otherSize = 1000 - topicsIds.size();
            AggregateResult[] groupedResultsOther  = [SELECT count(Id) topicCount, TopicId,Topic.Name tname
                                                      FROM TopicAssignment 
                                                      WHERE CreatedById !=:userId  AND TopicId not in : topicsIds
                                                      GROUP BY TopicId,Topic.Name 
                                                      ORDER BY count(TopicId) DESC
                                                      Limit  :otherSize];
            for (AggregateResult ar : groupedResultsOther)  {
                lstOfTopicsOthers.add(new Topic(Id = (Id)ar.get('TopicId'), 
                                          Name=(String)ar.get('tname') ) );
                topicsIds.add((Id)ar.get('TopicId'));
            }
            
            if(groupedResultsOther.size() < (selfSize+groupedResults.size() ) ){
                List<Topic> topicList = [SELECT
                                         Id,Name 
                                         FROM Topic 
                                         WHERE Name =:searchString AND id not in :topicsIds ];
                if(!topicList.isEmpty()){
                    lstOfTopicsOthers.addAll(topicList);  
                }
            }
        }
        Map<String,Object> mapOFObject = new Map<String,Object>();
        mapOFObject.put('lstOfTopics',lstOfTopicsSelf);
        mapOFObject.put('lstOfTopicsOthers',lstOfTopicsOthers);
        
        return mapOFObject;
    }
    
    public static List<Account> filterAccountByOwner(List<Account> lstOfAccounts,boolean isRemove){
        Id  loggedInUserId    		= UserInfo.getUserId();
        Id  unassignedUserId;
        List<Account> filterAccounts = new List<Account>();        
        List<User> UnassignedUser= [SELECT
                                    Id,Name 
                                    FROM User 
                                    WHERE name ='Unassigned' 
                                    LIMIT 1];
        
        if(!UnassignedUser.isEmpty()){
            unassignedUserId = UnassignedUser[0].Id;
        }
        
        if(!lstOfAccounts.isEmpty()){
            for(Account acctObj : lstOfAccounts){
                if(isRemove ){
                    if(acctObj.OwnerId == loggedInUserId ){
                        filterAccounts.add(acctObj);
                    } 
                }else{
                    if(acctObj.OwnerId == loggedInUserId || acctObj.OwnerId == unassignedUserId ){
                        filterAccounts.add(acctObj);
                    }    
                }
            }
        }
        return filterAccounts;
    }
    
    @AuraEnabled
    public static String unTagAccounts(String selectedTags,String accountIds){
        Integer countFailCompanies 						= 0;
        Map<Id,String> mapOfPassRecords 				= new Map<Id,String>();
        Map<Id,Map<String,String>> mapOfFailRecords   	= new Map<Id,Map<String,String>>();
        Id currentUserId 								= UserInfo.getUserId();
        List<User> UnassignedUser						= [SELECT 
                                                           Id,Name 
                                                           FROM User 
                                                           WHERE name ='Unassigned' 
                                                           LIMIT 1];
        Id unassignedUserId 							= UnassignedUser[0].Id;
        List<TagResponse> tagLst 						= (List<TagResponse>)Json.deserialize(selectedTags,
                                                                                              List<TagResponse>.class);
        Map<String, String> mapOfSelectedToRemove 		= new Map<String, String>();
        
        for(TagResponse tag :tagLst){
            String topicId= tag.topicId;
            String topicName=tag.topicName;
            mapOfSelectedToRemove.put(topicId,topicName);
        }
        
        Set<String> selectedTopicsIds = mapOfSelectedToRemove.keySet();
        List<Id> lstOfAcctIds = (List<Id>)accountIds.split(',');
        Map<Id,Account> selectedAccount = new Map<Id,Account>([Select Id,OwnerId,Tags__c,Name from Account where Id in :lstOfAcctIds]);
        Map<Id,Account> filterAccounts  = new Map<Id,Account>(filterAccountByOwner(selectedAccount.values(),true));
        
        List<Account> accountList = filterAccounts.values();
        countFailCompanies = selectedAccount.size()-filterAccounts.size();
        
        for(Id compId : selectedAccount.keySet()){
            if(!filterAccounts.containsKey(compId)){
                mapOfFailRecords.put(compId,new Map<String,String>{
                    selectedAccount.get(compId).Name => 'You are not the owner of this company or not owned by unassigned user.'
                        });
            }
        }
        
        if(!accountList.isEmpty()){
            Set<Id> acctIds = new Map<Id,Account> (accountList).keySet();    
            List<TopicAssignment> lstOfTopicAssignment = [SELECT
                                                          Id,EntityId,TopicId,Topic.Name,CreatedById 
                                                          FROM TopicAssignment 
                                                          WHERE TopicId in :selectedTopicsIds 
                                                          AND  EntityId in :acctIds];
            delete lstOfTopicAssignment;            
            //sendMail(false,mapOfFailRecords,mapOfPassRecords);
        }
        return '{"isSuccess":"true","countFailCompanies":"'+countFailCompanies+'","countSuccessCompanies":"'+(selectedAccount.size()-countFailCompanies)+'"}';                 
    }
}