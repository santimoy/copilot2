@isTest
private class TestClassContact { 
    public static  testMethod void testContactSum() {
        Account a = new Account(Name='Test Account');
            insert a;
              Contact c1 = new Contact(LastName='Last', AccountId = a.Id);
              Contact c2 = new Contact(LastName='Last2', AccountId = a.Id);
              Contact c3 = new Contact(LastName='Last3', AccountId = a.Id);
              checkRecursive.resetAll();
            insert c1;
              a = [Select Number_of_Contacts__c from Account where Id=:a.Id];
          System.assertEquals(1, a.Number_of_Contacts__c);
              checkRecursive.resetAll();
            insert c2;
              a = [Select Number_of_Contacts__c from Account where Id=:a.Id];
          System.assertEquals(2, a.Number_of_Contacts__c);
              checkRecursive.resetAll();
            insert c3;
                a = [Select Number_of_Contacts__c from Account where Id=:a.Id];
          System.assertEquals(3, a.Number_of_Contacts__c);
              checkRecursive.resetAll();
            delete c2;
                a = [Select Number_of_Contacts__c from Account where Id=:a.Id];
          System.assertEquals(2, a.Number_of_Contacts__c);
}
}